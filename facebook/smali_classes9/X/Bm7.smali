.class public LX/Bm7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Bm7;


# instance fields
.field public final a:Landroid/content/ContentResolver;

.field public final b:LX/Bky;

.field public final c:LX/0tX;

.field public final d:LX/0TD;

.field public final e:LX/0hB;

.field private final f:LX/1Ck;

.field private final g:LX/Bm1;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;LX/Bky;LX/Bm1;LX/0tX;LX/0TD;LX/0hB;LX/1Ck;)V
    .locals 0
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1817843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1817844
    iput-object p1, p0, LX/Bm7;->a:Landroid/content/ContentResolver;

    .line 1817845
    iput-object p2, p0, LX/Bm7;->b:LX/Bky;

    .line 1817846
    iput-object p3, p0, LX/Bm7;->g:LX/Bm1;

    .line 1817847
    iput-object p4, p0, LX/Bm7;->c:LX/0tX;

    .line 1817848
    iput-object p5, p0, LX/Bm7;->d:LX/0TD;

    .line 1817849
    iput-object p6, p0, LX/Bm7;->e:LX/0hB;

    .line 1817850
    iput-object p7, p0, LX/Bm7;->f:LX/1Ck;

    .line 1817851
    return-void
.end method

.method public static a(LX/0QB;)LX/Bm7;
    .locals 11

    .prologue
    .line 1817852
    sget-object v0, LX/Bm7;->h:LX/Bm7;

    if-nez v0, :cond_1

    .line 1817853
    const-class v1, LX/Bm7;

    monitor-enter v1

    .line 1817854
    :try_start_0
    sget-object v0, LX/Bm7;->h:LX/Bm7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1817855
    if-eqz v2, :cond_0

    .line 1817856
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1817857
    new-instance v3, LX/Bm7;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v4

    check-cast v4, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v5

    check-cast v5, LX/Bky;

    invoke-static {v0}, LX/Bm1;->a(LX/0QB;)LX/Bm1;

    move-result-object v6

    check-cast v6, LX/Bm1;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v9

    check-cast v9, LX/0hB;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v10

    check-cast v10, LX/1Ck;

    invoke-direct/range {v3 .. v10}, LX/Bm7;-><init>(Landroid/content/ContentResolver;LX/Bky;LX/Bm1;LX/0tX;LX/0TD;LX/0hB;LX/1Ck;)V

    .line 1817858
    move-object v0, v3

    .line 1817859
    sput-object v0, LX/Bm7;->h:LX/Bm7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1817860
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1817861
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1817862
    :cond_1
    sget-object v0, LX/Bm7;->h:LX/Bm7;

    return-object v0

    .line 1817863
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1817864
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 1817865
    new-instance v0, LX/Bm5;

    invoke-direct {v0, p0, p2, p1}, LX/Bm5;-><init>(LX/Bm7;Ljava/lang/String;I)V

    .line 1817866
    new-instance v1, LX/Bm6;

    invoke-direct {v1, p0, p2}, LX/Bm6;-><init>(LX/Bm7;Ljava/lang/String;)V

    .line 1817867
    iget-object v2, p0, LX/Bm7;->f:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-fetchEventCommonFragment:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1817868
    return-void
.end method
