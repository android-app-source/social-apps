.class public LX/AnL;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0Zb;

.field public final c:LX/0Sh;

.field private final d:LX/17V;

.field private final e:LX/17Q;

.field public final f:LX/961;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/17Q;LX/17V;LX/0Sh;LX/0Zb;LX/961;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1712364
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1712365
    iput-object p1, p0, LX/AnL;->a:Landroid/content/Context;

    .line 1712366
    iput-object p3, p0, LX/AnL;->d:LX/17V;

    .line 1712367
    iput-object p4, p0, LX/AnL;->c:LX/0Sh;

    .line 1712368
    iput-object p5, p0, LX/AnL;->b:LX/0Zb;

    .line 1712369
    iput-object p2, p0, LX/AnL;->e:LX/17Q;

    .line 1712370
    iput-object p6, p0, LX/AnL;->f:LX/961;

    .line 1712371
    return-void
.end method

.method private static a(LX/AnL;Lcom/facebook/fbui/glyph/GlyphView;ZZ)V
    .locals 1

    .prologue
    .line 1712356
    if-eqz p2, :cond_1

    .line 1712357
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 1712358
    const v0, 0x7f081040

    invoke-direct {p0, p1, v0}, LX/AnL;->a(Landroid/view/View;I)V

    .line 1712359
    if-eqz p3, :cond_0

    .line 1712360
    invoke-static {p1}, LX/7kh;->a(Landroid/view/View;)V

    .line 1712361
    :cond_0
    :goto_0
    return-void

    .line 1712362
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setSelected(Z)V

    .line 1712363
    const v0, 0x7f08103f

    invoke-direct {p0, p1, v0}, LX/AnL;->a(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;I)V
    .locals 1

    .prologue
    .line 1712354
    iget-object v0, p0, LX/AnL;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1712355
    return-void
.end method

.method public static a$redex0(LX/AnL;Lcom/facebook/fbui/glyph/GlyphView;Lcom/facebook/graphql/model/GraphQLPage;Z)V
    .locals 1

    .prologue
    .line 1712349
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1712350
    :goto_0
    invoke-virtual {p2, v0}, Lcom/facebook/graphql/model/GraphQLPage;->a(Z)V

    .line 1712351
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    invoke-static {p0, p1, v0, p3}, LX/AnL;->a(LX/AnL;Lcom/facebook/fbui/glyph/GlyphView;ZZ)V

    .line 1712352
    return-void

    .line 1712353
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/AnL;
    .locals 7

    .prologue
    .line 1712347
    new-instance v0, LX/AnL;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v2

    check-cast v2, LX/17Q;

    invoke-static {p0}, LX/17V;->b(LX/0QB;)LX/17V;

    move-result-object v3

    check-cast v3, LX/17V;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    invoke-static {p0}, LX/961;->b(LX/0QB;)LX/961;

    move-result-object v6

    check-cast v6, LX/961;

    invoke-direct/range {v0 .. v6}, LX/AnL;-><init>(Landroid/content/Context;LX/17Q;LX/17V;LX/0Sh;LX/0Zb;LX/961;)V

    .line 1712348
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;LX/25E;Lcom/facebook/fbui/glyph/GlyphView;LX/AnC;)V
    .locals 7

    .prologue
    .line 1712343
    invoke-interface {p2}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    .line 1712344
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    const/4 v1, 0x0

    invoke-static {p0, p3, v0, v1}, LX/AnL;->a(LX/AnL;Lcom/facebook/fbui/glyph/GlyphView;ZZ)V

    .line 1712345
    new-instance v0, LX/AnK;

    move-object v1, p0

    move-object v2, p3

    move-object v4, p1

    move-object v5, p2

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/AnK;-><init>(LX/AnL;Lcom/facebook/fbui/glyph/GlyphView;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;LX/25E;LX/AnC;)V

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1712346
    return-void
.end method
