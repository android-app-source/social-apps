.class public LX/BWG;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/1jt;


# instance fields
.field public final b:LX/BWF;

.field public final c:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Set",
            "<",
            "LX/BWC;",
            ">;>;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/44w",
            "<",
            "Ljava/lang/String;",
            "LX/BWL;",
            ">;>;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1791814
    new-instance v0, LX/BWB;

    invoke-direct {v0}, LX/BWB;-><init>()V

    sput-object v0, LX/BWG;->a:LX/1jt;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Class;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1791815
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1791816
    iput-object p1, p0, LX/BWG;->c:Ljava/lang/Class;

    .line 1791817
    new-instance v0, LX/BWF;

    invoke-direct {v0, p0}, LX/BWF;-><init>(LX/BWG;)V

    iput-object v0, p0, LX/BWG;->b:LX/BWF;

    .line 1791818
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BWG;->d:Ljava/util/Map;

    .line 1791819
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/BWG;->e:Ljava/util/Map;

    .line 1791820
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/BWG;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 1791821
    const-string v0, "call_return"

    new-instance v1, LX/BWD;

    invoke-direct {v1, p0}, LX/BWD;-><init>(LX/BWG;)V

    invoke-virtual {p0, v0, v1}, LX/BWG;->a(Ljava/lang/String;LX/BWC;)V

    .line 1791822
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1791788
    const/16 v0, 0x9

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-gt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    .line 1791789
    :goto_0
    if-nez v0, :cond_1

    .line 1791790
    invoke-static {}, Landroid/webkit/WebStorage;->getInstance()Landroid/webkit/WebStorage;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/WebStorage;->deleteAllData()V

    .line 1791791
    :goto_1
    return-void

    .line 1791792
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1791793
    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 1791794
    new-instance v1, Lcom/facebook/webview/FacebookJsBridge$2;

    invoke-direct {v1, p0}, Lcom/facebook/webview/FacebookJsBridge$2;-><init>(Landroid/content/Context;)V

    const v2, 0x683d7461

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/BWL;)Ljava/lang/String;
    .locals 3
    .param p2    # LX/BWL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1791808
    iget-object v0, p0, LX/BWG;->f:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 1791809
    monitor-enter p0

    .line 1791810
    :try_start_0
    iget-object v1, p0, LX/BWG;->e:Ljava/util/Map;

    new-instance v2, LX/44w;

    invoke-direct {v2, p1, p2}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791811
    monitor-exit p0

    .line 1791812
    return-object v0

    .line 1791813
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Ljava/lang/String;LX/BWC;)V
    .locals 2

    .prologue
    .line 1791802
    iget-object v0, p0, LX/BWG;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1791803
    if-nez v0, :cond_0

    .line 1791804
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 1791805
    iget-object v1, p0, LX/BWG;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1791806
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1791807
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;LX/BWN;)Z
    .locals 2

    .prologue
    .line 1791795
    iget-object v0, p0, LX/BWG;->d:Ljava/util/Map;

    invoke-interface {p3}, LX/BWN;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1791796
    if-nez v0, :cond_0

    .line 1791797
    const/4 v0, 0x0

    .line 1791798
    :goto_0
    return v0

    .line 1791799
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWC;

    .line 1791800
    invoke-interface {v0, p1, p2, p3}, LX/BWC;->a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;LX/BWN;)V

    goto :goto_1

    .line 1791801
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
