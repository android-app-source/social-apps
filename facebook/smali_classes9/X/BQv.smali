.class public final LX/BQv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/base/fragment/FbFragment;

.field public final synthetic b:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

.field public final synthetic c:Lcom/facebook/content/SecureContextHelper;

.field public final synthetic d:LX/BQx;


# direct methods
.method public constructor <init>(LX/BQx;Lcom/facebook/base/fragment/FbFragment;Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1782862
    iput-object p1, p0, LX/BQv;->d:LX/BQx;

    iput-object p2, p0, LX/BQv;->a:Lcom/facebook/base/fragment/FbFragment;

    iput-object p3, p0, LX/BQv;->b:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    iput-object p4, p0, LX/BQv;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x6fb11507

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1782863
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/BQv;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/facebook/timeline/stagingground/ProfilePictureOverlayDefaultImageActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1782864
    const-string v2, "heisman_pivot_intent_data"

    iget-object v3, p0, LX/BQv;->b:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1782865
    iget-object v2, p0, LX/BQv;->c:Lcom/facebook/content/SecureContextHelper;

    const/4 v3, 0x3

    iget-object v4, p0, LX/BQv;->a:Lcom/facebook/base/fragment/FbFragment;

    invoke-interface {v2, v1, v3, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1782866
    const v1, -0x191afdc7

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
