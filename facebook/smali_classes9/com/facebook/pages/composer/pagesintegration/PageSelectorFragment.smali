.class public Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:Lcom/facebook/widget/listview/EmptyListViewItem;

.field public d:Landroid/view/View;

.field public e:LX/BEO;

.field public f:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0aG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/ARk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1764455
    const-class v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1764456
    const-string v0, "extra_go_to_composer_when_page_selected"

    sput-object v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1764453
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1764454
    return-void
.end method

.method public static synthetic a(Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 1764420
    iget-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->e:LX/BEO;

    invoke-virtual {v0, p3}, LX/BEO;->a(I)Lcom/facebook/ipc/pages/PageInfo;

    move-result-object v0

    .line 1764421
    new-instance v1, LX/89I;

    iget-wide v2, v0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    sget-object v4, LX/2rw;->PAGE:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    iget-object v2, v0, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    .line 1764422
    iput-object v2, v1, LX/89I;->c:Ljava/lang/String;

    .line 1764423
    move-object v1, v1

    .line 1764424
    iget-object v2, v0, Lcom/facebook/ipc/pages/PageInfo;->squareProfilePicUrl:Ljava/lang/String;

    .line 1764425
    iput-object v2, v1, LX/89I;->d:Ljava/lang/String;

    .line 1764426
    move-object v1, v1

    .line 1764427
    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    .line 1764428
    invoke-static {}, Lcom/facebook/auth/viewercontext/ViewerContext;->newBuilder()LX/0SK;

    move-result-object v2

    const/4 v3, 0x1

    .line 1764429
    iput-boolean v3, v2, LX/0SK;->d:Z

    .line 1764430
    move-object v2, v2

    .line 1764431
    iget-wide v4, v0, Lcom/facebook/ipc/pages/PageInfo;->pageId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    .line 1764432
    iput-object v3, v2, LX/0SK;->a:Ljava/lang/String;

    .line 1764433
    move-object v2, v2

    .line 1764434
    iget-object v3, v0, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    .line 1764435
    iput-object v3, v2, LX/0SK;->g:Ljava/lang/String;

    .line 1764436
    move-object v2, v2

    .line 1764437
    iget-object v3, v0, Lcom/facebook/ipc/pages/PageInfo;->accessToken:Ljava/lang/String;

    .line 1764438
    iput-object v3, v2, LX/0SK;->b:Ljava/lang/String;

    .line 1764439
    move-object v2, v2

    .line 1764440
    invoke-virtual {v2}, LX/0SK;->h()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v2

    .line 1764441
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/ipc/pages/PageInfo;->pageName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v3

    iget-object v0, v0, Lcom/facebook/ipc/pages/PageInfo;->squareProfilePicUrl:Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v3

    .line 1764442
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 1764443
    sget-object v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->a:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1764444
    const-string v0, "extra_composer_configuration"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1764445
    invoke-static {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    .line 1764446
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->i:LX/1Kf;

    const-string v2, "extra_composer_internal_session_id"

    invoke-virtual {v4, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x6dc

    invoke-interface {v1, v2, v0, v3, p0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    .line 1764447
    :goto_0
    return-void

    .line 1764448
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1764449
    const-string v4, "extra_composer_target_data"

    invoke-virtual {v0, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1764450
    const-string v1, "extra_composer_page_data"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1764451
    const-string v1, "extra_actor_viewer_context"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1764452
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->j:LX/ARk;

    invoke-interface {v1, v0}, LX/ARk;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static c(Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;)V
    .locals 4

    .prologue
    .line 1764414
    iget-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 1764415
    iget-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1764416
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1764417
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->g:LX/0aG;

    const-string v2, "fetch_all_pages"

    const v3, 0x2b14b74c

    invoke-static {v1, v2, v0, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1764418
    iget-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->h:LX/0Sh;

    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v2, LX/BEL;

    invoke-direct {v2, p0}, LX/BEL;-><init>(Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;)V

    invoke-virtual {v0, v1, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 1764419
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1764457
    const-string v0, "composer"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1764411
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1764412
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    invoke-static {v0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v2

    check-cast v2, LX/0aG;

    invoke-static {v0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object p1

    check-cast p1, LX/0Sh;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iput-object v2, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->g:LX/0aG;

    iput-object p1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->h:LX/0Sh;

    iput-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->i:LX/1Kf;

    .line 1764413
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1764407
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1764408
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1764409
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1764410
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x31cc900e

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1764385
    const v0, 0x7f030342

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 1764386
    const v0, 0x102000a

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 1764387
    new-instance v1, LX/BEK;

    invoke-direct {v1, p0}, LX/BEK;-><init>(Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1764388
    const v1, 0x7f0d0abe

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/listview/EmptyListViewItem;

    iput-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    .line 1764389
    const v1, 0x7f0d0abf

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->d:Landroid/view/View;

    .line 1764390
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->d:Landroid/view/View;

    new-instance v4, LX/BEM;

    invoke-direct {v4, p0}, LX/BEM;-><init>(Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1764391
    new-instance v1, LX/BEO;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, LX/BEO;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->e:LX/BEO;

    .line 1764392
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->e:LX/BEO;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1764393
    invoke-static {p0}, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c(Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;)V

    .line 1764394
    const/16 v0, 0x2b

    const v1, 0x113cf439

    invoke-static {v5, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v3
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x194ce4b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1764399
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1764400
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1764401
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1764402
    invoke-interface {v1, v3}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 1764403
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/EmptyListViewItem;->a(Z)V

    .line 1764404
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c:Lcom/facebook/widget/listview/EmptyListViewItem;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/EmptyListViewItem;->setVisibility(I)V

    .line 1764405
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1764406
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x1e776

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xd747959

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1764395
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1764396
    iget-object v1, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->f:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v1, :cond_0

    .line 1764397
    invoke-static {p0}, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->c(Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;)V

    .line 1764398
    :cond_0
    const/16 v1, 0x2b

    const v2, 0x328411e4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
