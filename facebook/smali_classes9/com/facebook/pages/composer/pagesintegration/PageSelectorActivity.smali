.class public Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;
.implements LX/10X;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static p:Ljava/lang/String;

.field private static final q:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private r:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1764317
    const-class v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->q:Lcom/facebook/common/callercontext/CallerContext;

    .line 1764318
    const-string v0, "extra_go_to_composer_when_page_selected"

    sput-object v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1764315
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1764316
    const-string v0, "composer"

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1764314
    const v0, 0x7f0824aa

    invoke-virtual {p0, v0}, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1764302
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1764303
    if-nez p1, :cond_0

    .line 1764304
    const v0, 0x7f031069

    invoke-virtual {p0, v0}, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->setContentView(I)V

    .line 1764305
    const v0, 0x7f0d274b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1764306
    new-instance v1, LX/BEI;

    invoke-direct {v1, p0}, LX/BEI;-><init>(Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1764307
    const v1, 0x7f081414

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1764308
    new-instance v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    invoke-direct {v0}, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->r:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    .line 1764309
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d1a78

    iget-object v2, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->r:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1764310
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->r:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    new-instance v1, LX/BEJ;

    invoke-direct {v1, p0}, LX/BEJ;-><init>(Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;)V

    .line 1764311
    iput-object v1, v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;->j:LX/ARk;

    .line 1764312
    return-void

    .line 1764313
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f0d1a78

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    iput-object v0, p0, Lcom/facebook/pages/composer/pagesintegration/PageSelectorActivity;->r:Lcom/facebook/pages/composer/pagesintegration/PageSelectorFragment;

    goto :goto_0
.end method
