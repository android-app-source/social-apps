.class public final Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1913747
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1913748
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1913749
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1913665
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1913666
    const/4 v2, 0x0

    .line 1913667
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1913668
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913669
    :goto_0
    move v1, v2

    .line 1913670
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1913671
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1913672
    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel;-><init>()V

    .line 1913673
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1913674
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1913675
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1913676
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1913677
    :cond_0
    return-object v1

    .line 1913678
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913679
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1913680
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1913681
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1913682
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1913683
    const-string v4, "commerce_store"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1913684
    const/4 v3, 0x0

    .line 1913685
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_a

    .line 1913686
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913687
    :goto_2
    move v1, v3

    .line 1913688
    goto :goto_1

    .line 1913689
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1913690
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1913691
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1913692
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913693
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_9

    .line 1913694
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1913695
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1913696
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_6

    if-eqz v6, :cond_6

    .line 1913697
    const-string v7, "commerce_merchant_settings"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1913698
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1913699
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_11

    .line 1913700
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913701
    :goto_4
    move v5, v6

    .line 1913702
    goto :goto_3

    .line 1913703
    :cond_7
    const-string v7, "ordered_collections"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1913704
    const/4 v6, 0x0

    .line 1913705
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_16

    .line 1913706
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913707
    :goto_5
    move v4, v6

    .line 1913708
    goto :goto_3

    .line 1913709
    :cond_8
    const-string v7, "url"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1913710
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 1913711
    :cond_9
    const/4 v6, 0x3

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1913712
    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1913713
    const/4 v3, 0x1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1913714
    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1913715
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_a
    move v1, v3

    move v4, v3

    move v5, v3

    goto :goto_3

    .line 1913716
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_e

    .line 1913717
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1913718
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1913719
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_b

    if-eqz v11, :cond_b

    .line 1913720
    const-string p0, "payment_provider"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 1913721
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v8

    move v10, v8

    move v8, v7

    goto :goto_6

    .line 1913722
    :cond_c
    const-string p0, "show_edit_interface"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 1913723
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v5

    move v9, v5

    move v5, v7

    goto :goto_6

    .line 1913724
    :cond_d
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_6

    .line 1913725
    :cond_e
    const/4 v11, 0x2

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1913726
    if-eqz v8, :cond_f

    .line 1913727
    invoke-virtual {v0, v6, v10, v6}, LX/186;->a(III)V

    .line 1913728
    :cond_f
    if-eqz v5, :cond_10

    .line 1913729
    invoke-virtual {v0, v7, v9}, LX/186;->a(IZ)V

    .line 1913730
    :cond_10
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_4

    :cond_11
    move v5, v6

    move v8, v6

    move v9, v6

    move v10, v6

    goto :goto_6

    .line 1913731
    :cond_12
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913732
    :cond_13
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_15

    .line 1913733
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1913734
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1913735
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_13

    if-eqz v7, :cond_13

    .line 1913736
    const-string v8, "nodes"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 1913737
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1913738
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_14

    .line 1913739
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_14

    .line 1913740
    invoke-static {p1, v0}, LX/CZ1;->b(LX/15w;LX/186;)I

    move-result v7

    .line 1913741
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1913742
    :cond_14
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1913743
    goto :goto_7

    .line 1913744
    :cond_15
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1913745
    invoke-virtual {v0, v6, v4}, LX/186;->b(II)V

    .line 1913746
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_5

    :cond_16
    move v4, v6

    goto :goto_7
.end method
