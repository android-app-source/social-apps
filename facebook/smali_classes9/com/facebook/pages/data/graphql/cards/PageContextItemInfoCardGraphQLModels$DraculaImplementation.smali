.class public final Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1913154
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1913155
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1913152
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1913153
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    .line 1913131
    if-nez p1, :cond_0

    .line 1913132
    const/4 v0, 0x0

    .line 1913133
    :goto_0
    return v0

    .line 1913134
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1913135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1913136
    :sswitch_0
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1913137
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1913138
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 1913139
    const/4 v0, 0x3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 1913140
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1913141
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1913142
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1913143
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1913144
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1913145
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1913146
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1913147
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1913148
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1913149
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->a(IZ)V

    .line 1913150
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1913151
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x60119901 -> :sswitch_0
        0x5e85f3e8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1913130
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1913127
    sparse-switch p0, :sswitch_data_0

    .line 1913128
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1913129
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x60119901 -> :sswitch_0
        0x5e85f3e8 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1913126
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1913124
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->b(I)V

    .line 1913125
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1913119
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1913120
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1913121
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1913122
    iput p2, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->b:I

    .line 1913123
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1913118
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1913093
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1913115
    iget v0, p0, LX/1vt;->c:I

    .line 1913116
    move v0, v0

    .line 1913117
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1913112
    iget v0, p0, LX/1vt;->c:I

    .line 1913113
    move v0, v0

    .line 1913114
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1913109
    iget v0, p0, LX/1vt;->b:I

    .line 1913110
    move v0, v0

    .line 1913111
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913106
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1913107
    move-object v0, v0

    .line 1913108
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1913097
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1913098
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1913099
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1913100
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1913101
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1913102
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1913103
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1913104
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1913105
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1913094
    iget v0, p0, LX/1vt;->c:I

    .line 1913095
    move v0, v0

    .line 1913096
    return v0
.end method
