.class public final Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1916007
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1916008
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1916028
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1916029
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1916015
    if-nez p1, :cond_0

    .line 1916016
    :goto_0
    return v0

    .line 1916017
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1916018
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1916019
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1916020
    const v2, 0x5844f38f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1916021
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1916022
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1916023
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1916024
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1916025
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1916026
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1916027
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3972b9c0 -> :sswitch_0
        0x5844f38f -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1916014
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1916009
    sparse-switch p2, :sswitch_data_0

    .line 1916010
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1916011
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1916012
    const v1, 0x5844f38f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1916013
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3972b9c0 -> :sswitch_0
        0x5844f38f -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1916001
    if-eqz p1, :cond_0

    .line 1916002
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1916003
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;

    .line 1916004
    if-eq v0, v1, :cond_0

    .line 1916005
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1916006
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1916000
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1915998
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1915999
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1916030
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1916031
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1916032
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1916033
    iput p2, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->b:I

    .line 1916034
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1915997
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1915996
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1915993
    iget v0, p0, LX/1vt;->c:I

    .line 1915994
    move v0, v0

    .line 1915995
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1915972
    iget v0, p0, LX/1vt;->c:I

    .line 1915973
    move v0, v0

    .line 1915974
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1915990
    iget v0, p0, LX/1vt;->b:I

    .line 1915991
    move v0, v0

    .line 1915992
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1915987
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1915988
    move-object v0, v0

    .line 1915989
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1915978
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1915979
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1915980
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1915981
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1915982
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1915983
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1915984
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1915985
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1915986
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1915975
    iget v0, p0, LX/1vt;->c:I

    .line 1915976
    move v0, v0

    .line 1915977
    return v0
.end method
