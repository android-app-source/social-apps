.class public final Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x682f3fc1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1913339
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1913348
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1913346
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1913347
    return-void
.end method

.method private a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913344
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    .line 1913345
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    return-object v0
.end method

.method private j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913342
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->f:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->f:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    .line 1913343
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->f:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913340
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1913341
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMapBoundingBox"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913333
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1913334
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getMenuInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913337
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1913338
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLPlaceType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913335
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->l:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPlaceType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPlaceType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->l:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    .line 1913336
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->l:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1913349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1913350
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1913351
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1913352
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1913353
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x60119901

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1913354
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->m()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    const v6, 0x5e85f3e8

    invoke-static {v5, v4, v6}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1913355
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->n()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1913356
    const/16 v6, 0x8

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1913357
    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1913358
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1913359
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1913360
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1913361
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1913362
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->j:I

    invoke-virtual {p1, v0, v1, v7}, LX/186;->a(III)V

    .line 1913363
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1913364
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1913365
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1913366
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1913287
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1913288
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1913289
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    .line 1913290
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1913291
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    .line 1913292
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultAddressFieldsModel;

    .line 1913293
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1913294
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    .line 1913295
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->j()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1913296
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    .line 1913297
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->f:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel;

    .line 1913298
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1913299
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1913300
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1913301
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    .line 1913302
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->h:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultLocationFieldsModel;

    .line 1913303
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 1913304
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x60119901

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1913305
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1913306
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    .line 1913307
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->i:I

    move-object v1, v0

    .line 1913308
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 1913309
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x5e85f3e8

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1913310
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1913311
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    .line 1913312
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->k:I

    move-object v1, v0

    .line 1913313
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1913314
    if-nez v1, :cond_5

    :goto_0
    return-object p0

    .line 1913315
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1913316
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_5
    move-object p0, v1

    .line 1913317
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1913318
    new-instance v0, LX/CYt;

    invoke-direct {v0, p1}, LX/CYt;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1913319
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1913320
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->g:Z

    .line 1913321
    const/4 v0, 0x4

    const v1, -0x60119901

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->i:I

    .line 1913322
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->j:I

    .line 1913323
    const/4 v0, 0x6

    const v1, 0x5e85f3e8

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;->k:I

    .line 1913324
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1913325
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1913326
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1913327
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1913328
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;-><init>()V

    .line 1913329
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1913330
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1913331
    const v0, 0x6d10573a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1913332
    const v0, 0x25d6af

    return v0
.end method
