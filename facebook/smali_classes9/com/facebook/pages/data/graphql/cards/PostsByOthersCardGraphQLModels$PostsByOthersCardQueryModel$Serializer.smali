.class public final Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1915020
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1915021
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1915022
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1915023
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1915024
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1915025
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1915026
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1915027
    if-eqz v2, :cond_1

    .line 1915028
    const-string p0, "admin_display_preference"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915029
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1915030
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1915031
    if-eqz p0, :cond_0

    .line 1915032
    const-string p2, "show_posts_by_others"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915033
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1915034
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1915035
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1915036
    if-eqz v2, :cond_2

    .line 1915037
    const-string p0, "can_viewer_post"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915038
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1915039
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1915040
    if-eqz v2, :cond_3

    .line 1915041
    const-string p0, "can_viewer_post_photo_to_timeline"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915042
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1915043
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1915044
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1915045
    check-cast p1, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel$Serializer;->a(Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
