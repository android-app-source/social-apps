.class public final Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7ac6d0fa
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1911752
    const-class v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1911751
    const-class v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1911749
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1911750
    return-void
.end method

.method private a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getByline"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1911747
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1911748
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getExternalImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911745
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1911746
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911743
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->g:Ljava/lang/String;

    .line 1911744
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPublishedOn"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911741
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1911742
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getReviewer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911753
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->i:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->i:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    .line 1911754
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->i:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    return-object v0
.end method

.method private n()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSummary"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911739
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1911740
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911737
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1911738
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 9

    .prologue
    .line 1911719
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1911720
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x424574dd

    invoke-static {v1, v0, v2}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1911721
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x406d6b79

    invoke-static {v2, v1, v3}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1911722
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1911723
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x23d76252

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1911724
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->m()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1911725
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->n()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, 0x7ac8856d

    invoke-static {v6, v5, v7}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1911726
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->o()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, 0x2a7ef9ef

    invoke-static {v7, v6, v8}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1911727
    const/4 v7, 0x7

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1911728
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1911729
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1911730
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1911731
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1911732
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1911733
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1911734
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1911735
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1911736
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1911680
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1911681
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1911682
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x424574dd

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1911683
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1911684
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;

    .line 1911685
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->e:I

    move-object v1, v0

    .line 1911686
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1911687
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x406d6b79

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1911688
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1911689
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;

    .line 1911690
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->f:I

    move-object v1, v0

    .line 1911691
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1911692
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x23d76252

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1911693
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1911694
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;

    .line 1911695
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->h:I

    move-object v1, v0

    .line 1911696
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->m()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1911697
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->m()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    .line 1911698
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->m()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1911699
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;

    .line 1911700
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->i:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel$ReviewerModel;

    .line 1911701
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->n()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 1911702
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->n()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7ac8856d

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1911703
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->n()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1911704
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;

    .line 1911705
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->j:I

    move-object v1, v0

    .line 1911706
    :cond_4
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 1911707
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2a7ef9ef

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1911708
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1911709
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;

    .line 1911710
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->k:I

    move-object v1, v0

    .line 1911711
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1911712
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    .line 1911713
    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 1911714
    :catchall_1
    move-exception v0

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v0

    .line 1911715
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0

    .line 1911716
    :catchall_3
    move-exception v0

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v0

    .line 1911717
    :catchall_4
    move-exception v0

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v0

    :cond_6
    move-object p0, v1

    .line 1911718
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1911673
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1911674
    const/4 v0, 0x0

    const v1, 0x424574dd

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->e:I

    .line 1911675
    const/4 v0, 0x1

    const v1, 0x406d6b79

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->f:I

    .line 1911676
    const/4 v0, 0x3

    const v1, -0x23d76252

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->h:I

    .line 1911677
    const/4 v0, 0x5

    const v1, 0x7ac8856d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->j:I

    .line 1911678
    const/4 v0, 0x6

    const v1, 0x2a7ef9ef

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;->k:I

    .line 1911679
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1911670
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel$NodesModel;-><init>()V

    .line 1911671
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1911672
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1911668
    const v0, 0x13148370

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1911669
    const v0, 0x548fc90c

    return v0
.end method
