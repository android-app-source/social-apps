.class public final Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6038e1ed
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1915854
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1915853
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1915851
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1915852
    return-void
.end method

.method private a()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAttribution"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1915849
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x3680f3dd

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;->e:LX/3Sb;

    .line 1915850
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1915843
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1915844
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;->a()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1915845
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1915846
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1915847
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1915848
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1915855
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1915856
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;->a()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1915857
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;->a()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1915858
    if-eqz v1, :cond_0

    .line 1915859
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;

    .line 1915860
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;->e:LX/3Sb;

    .line 1915861
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1915862
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1915842
    new-instance v0, LX/CZT;

    invoke-direct {v0, p1}, LX/CZT;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1915840
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1915841
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1915839
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1915836
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;-><init>()V

    .line 1915837
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1915838
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1915835
    const v0, 0x58bfae19

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1915834
    const v0, 0x25d6af

    return v0
.end method
