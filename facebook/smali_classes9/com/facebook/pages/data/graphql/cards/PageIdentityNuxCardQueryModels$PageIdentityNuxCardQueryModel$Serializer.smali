.class public final Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1914019
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1914020
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1914008
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1914010
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1914011
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1914012
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1914013
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1914014
    if-eqz p0, :cond_0

    .line 1914015
    const-string p0, "page_nux"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1914016
    invoke-virtual {v1, v0, p2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1914017
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1914018
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1914009
    check-cast p1, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel$Serializer;->a(Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
