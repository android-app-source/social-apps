.class public final Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7e361fa1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1915263
    const-class v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1915230
    const-class v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1915261
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1915262
    return-void
.end method

.method private a()Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSubtitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1915259
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->e:Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->e:Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    .line 1915260
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->e:Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1915257
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1915258
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1915249
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1915250
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->a()Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1915251
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1915252
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1915253
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1915254
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1915255
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1915256
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1915236
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1915237
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->a()Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1915238
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->a()Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    .line 1915239
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->a()Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1915240
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;

    .line 1915241
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->e:Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    .line 1915242
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1915243
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1915244
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->j()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1915245
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;

    .line 1915246
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1915247
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1915248
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1915233
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewNeedyPlaceCardQueryModel$ReviewNeedyPlaceCardModel;-><init>()V

    .line 1915234
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1915235
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1915232
    const v0, 0x79e09f07

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1915231
    const v0, 0x412906ec

    return v0
.end method
