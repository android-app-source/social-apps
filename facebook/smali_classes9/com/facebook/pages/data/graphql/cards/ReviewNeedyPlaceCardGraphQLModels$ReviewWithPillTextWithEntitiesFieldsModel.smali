.class public final Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/174;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x575f03a1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1915358
    const-class v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1915359
    const-class v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1915339
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1915340
    return-void
.end method

.method private j()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImageRanges"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1915356
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, -0x10ee8716

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->e:LX/3Sb;

    .line 1915357
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1915348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1915349
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->j()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1915350
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1915351
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1915352
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1915353
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1915354
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1915355
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1915360
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1915361
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->j()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1915362
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->j()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1915363
    if-eqz v1, :cond_0

    .line 1915364
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    .line 1915365
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->e:LX/3Sb;

    .line 1915366
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1915367
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1915346
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->f:Ljava/lang/String;

    .line 1915347
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1915343
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$ReviewWithPillTextWithEntitiesFieldsModel;-><init>()V

    .line 1915344
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1915345
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1915342
    const v0, -0x41579b7d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1915341
    const v0, -0x726d476c

    return v0
.end method
