.class public final Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1915177
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1915178
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1915175
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1915176
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1915152
    if-nez p1, :cond_0

    .line 1915153
    :goto_0
    return v0

    .line 1915154
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1915155
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1915156
    :sswitch_0
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v0

    .line 1915157
    const v1, 0xb244c9b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1915158
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1915159
    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1915160
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1915161
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1915162
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1915163
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1915164
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1915165
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1915166
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1915167
    const v2, 0xac3c06e

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1915168
    invoke-virtual {p0, p1, v5, v0}, LX/15i;->a(III)I

    move-result v2

    .line 1915169
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1915170
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1915171
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1915172
    invoke-virtual {p3, v5, v2, v0}, LX/186;->a(III)V

    .line 1915173
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 1915174
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x10ee8716 -> :sswitch_2
        0xac3c06e -> :sswitch_0
        0xb244c9b -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1915143
    if-nez p0, :cond_0

    move v0, v1

    .line 1915144
    :goto_0
    return v0

    .line 1915145
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1915146
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1915147
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1915148
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1915149
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1915150
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1915151
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1915136
    const/4 v7, 0x0

    .line 1915137
    const/4 v1, 0x0

    .line 1915138
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1915139
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1915140
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1915141
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1915142
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1915135
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1915128
    sparse-switch p2, :sswitch_data_0

    .line 1915129
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1915130
    :sswitch_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1915131
    const v1, 0xb244c9b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1915132
    :goto_0
    :sswitch_1
    return-void

    .line 1915133
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1915134
    const v1, 0xac3c06e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x10ee8716 -> :sswitch_2
        0xac3c06e -> :sswitch_0
        0xb244c9b -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1915122
    if-eqz p1, :cond_0

    .line 1915123
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1915124
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;

    .line 1915125
    if-eq v0, v1, :cond_0

    .line 1915126
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1915127
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1915121
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1915119
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1915120
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1915088
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1915089
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1915090
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1915091
    iput p2, p0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->b:I

    .line 1915092
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1915118
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1915117
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1915114
    iget v0, p0, LX/1vt;->c:I

    .line 1915115
    move v0, v0

    .line 1915116
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1915111
    iget v0, p0, LX/1vt;->c:I

    .line 1915112
    move v0, v0

    .line 1915113
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1915108
    iget v0, p0, LX/1vt;->b:I

    .line 1915109
    move v0, v0

    .line 1915110
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1915105
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1915106
    move-object v0, v0

    .line 1915107
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1915096
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1915097
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1915098
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1915099
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1915100
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1915101
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1915102
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1915103
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/ReviewNeedyPlaceCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1915104
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1915093
    iget v0, p0, LX/1vt;->c:I

    .line 1915094
    move v0, v0

    .line 1915095
    return v0
.end method
