.class public final Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1913219
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1913220
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1913221
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;LX/0nX;LX/0my;)V
    .locals 12

    .prologue
    .line 1913222
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1913223
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v5, 0x7

    const/4 v4, 0x0

    .line 1913224
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1913225
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1913226
    if-eqz v2, :cond_0

    .line 1913227
    const-string v3, "address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913228
    invoke-static {v1, v2, p1}, LX/4aS;->a(LX/15i;ILX/0nX;)V

    .line 1913229
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1913230
    if-eqz v2, :cond_1

    .line 1913231
    const-string v3, "contextItemInfoCards"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913232
    invoke-static {v1, v2, p1, p2}, LX/5Ny;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1913233
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1913234
    if-eqz v2, :cond_2

    .line 1913235
    const-string v3, "is_place_map_hidden"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913236
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1913237
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1913238
    if-eqz v2, :cond_3

    .line 1913239
    const-string v3, "location"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913240
    invoke-static {v1, v2, p1}, LX/4aX;->a(LX/15i;ILX/0nX;)V

    .line 1913241
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1913242
    if-eqz v2, :cond_8

    .line 1913243
    const-string v3, "map_bounding_box"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913244
    const-wide/16 v10, 0x0

    .line 1913245
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1913246
    const/4 v6, 0x0

    invoke-virtual {v1, v2, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1913247
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_4

    .line 1913248
    const-string v8, "east"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913249
    invoke-virtual {p1, v6, v7}, LX/0nX;->a(D)V

    .line 1913250
    :cond_4
    const/4 v6, 0x1

    invoke-virtual {v1, v2, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1913251
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_5

    .line 1913252
    const-string v8, "north"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913253
    invoke-virtual {p1, v6, v7}, LX/0nX;->a(D)V

    .line 1913254
    :cond_5
    const/4 v6, 0x2

    invoke-virtual {v1, v2, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1913255
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_6

    .line 1913256
    const-string v8, "south"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913257
    invoke-virtual {p1, v6, v7}, LX/0nX;->a(D)V

    .line 1913258
    :cond_6
    const/4 v6, 0x3

    invoke-virtual {v1, v2, v6, v10, v11}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1913259
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_7

    .line 1913260
    const-string v8, "west"

    invoke-virtual {p1, v8}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913261
    invoke-virtual {p1, v6, v7}, LX/0nX;->a(D)V

    .line 1913262
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1913263
    :cond_8
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1913264
    if-eqz v2, :cond_9

    .line 1913265
    const-string v3, "map_zoom_level"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913266
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1913267
    :cond_9
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1913268
    if-eqz v2, :cond_c

    .line 1913269
    const-string v3, "menu_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913270
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1913271
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1913272
    if-eqz v3, :cond_a

    .line 1913273
    const-string v4, "has_photo_menus"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913274
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 1913275
    :cond_a
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1913276
    if-eqz v3, :cond_b

    .line 1913277
    const-string v4, "has_structured_menu"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913278
    invoke-virtual {p1, v3}, LX/0nX;->a(Z)V

    .line 1913279
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1913280
    :cond_c
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1913281
    if-eqz v2, :cond_d

    .line 1913282
    const-string v2, "place_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913283
    invoke-virtual {v1, v0, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1913284
    :cond_d
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1913285
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1913286
    check-cast p1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Serializer;->a(Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
