.class public final Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1cd191cc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1914041
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1914040
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1914038
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1914039
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914036
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    .line 1914037
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1914030
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1914031
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLReactionRequestedUnit;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1914032
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1914033
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1914034
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1914035
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1914042
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1914043
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1914044
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1914029
    new-instance v0, LX/CZ3;

    invoke-direct {v0, p1}, LX/CZ3;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1914027
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1914028
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1914026
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1914023
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityNuxCardQueryModels$PageIdentityNuxCardQueryModel;-><init>()V

    .line 1914024
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1914025
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1914021
    const v0, 0x71af2231

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1914022
    const v0, 0x25d6af

    return v0
.end method
