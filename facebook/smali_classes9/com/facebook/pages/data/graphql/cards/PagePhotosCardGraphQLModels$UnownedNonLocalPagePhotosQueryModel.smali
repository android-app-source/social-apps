.class public final Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x43fd8be
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1914673
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1914672
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1914645
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1914646
    return-void
.end method

.method private a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhotosTakenOf"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914670
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    .line 1914671
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1914664
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1914665
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1914666
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1914667
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1914668
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1914669
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1914656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1914657
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1914658
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    .line 1914659
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1914660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;

    .line 1914661
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel$PhotosTakenOfModel;

    .line 1914662
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1914663
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1914655
    new-instance v0, LX/CZA;

    invoke-direct {v0, p1}, LX/CZA;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1914653
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1914654
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1914652
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1914649
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedNonLocalPagePhotosQueryModel;-><init>()V

    .line 1914650
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1914651
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1914648
    const v0, -0x4d499739

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1914647
    const v0, 0x25d6af

    return v0
.end method
