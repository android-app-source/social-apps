.class public final Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1912261
    const-class v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1912262
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1912293
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1912263
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1912264
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1912265
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_7

    .line 1912266
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1912267
    :goto_0
    move v1, v2

    .line 1912268
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1912269
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1912270
    new-instance v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;-><init>()V

    .line 1912271
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1912272
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1912273
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1912274
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1912275
    :cond_0
    return-object v1

    .line 1912276
    :cond_1
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_5

    .line 1912277
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1912278
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1912279
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_1

    if-eqz v7, :cond_1

    .line 1912280
    const-string p0, "events_calendar_can_viewer_subscribe"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1912281
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_1

    .line 1912282
    :cond_2
    const-string p0, "events_calendar_subscription_status"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1912283
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    goto :goto_1

    .line 1912284
    :cond_3
    const-string p0, "owned_events"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 1912285
    invoke-static {p1, v0}, LX/CYr;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1912286
    :cond_4
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1912287
    :cond_5
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1912288
    if-eqz v1, :cond_6

    .line 1912289
    invoke-virtual {v0, v2, v6}, LX/186;->a(IZ)V

    .line 1912290
    :cond_6
    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1912291
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1912292
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    goto :goto_1
.end method
