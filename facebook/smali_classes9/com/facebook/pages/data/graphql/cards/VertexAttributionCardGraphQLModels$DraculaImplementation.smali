.class public final Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1915580
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1915581
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1915651
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1915652
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1915599
    if-nez p1, :cond_0

    move v0, v1

    .line 1915600
    :goto_0
    return v0

    .line 1915601
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1915602
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1915603
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1915604
    const v2, 0x3955171d

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1915605
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v2

    .line 1915606
    const v3, 0x557cc1a1

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1915607
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    move-result-object v3

    .line 1915608
    invoke-virtual {p3, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1915609
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1915610
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1915611
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1915612
    invoke-virtual {p3, v5, v3}, LX/186;->b(II)V

    .line 1915613
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1915614
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1915615
    const v2, -0x38ac4d4

    const/4 v6, 0x0

    .line 1915616
    if-nez v0, :cond_1

    move v3, v6

    .line 1915617
    :goto_1
    move v0, v3

    .line 1915618
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1915619
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1915620
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1915621
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1915622
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1915623
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1915624
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1915625
    const v2, 0x7fc90bf7

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1915626
    invoke-virtual {p0, p1, v4, v1}, LX/15i;->a(III)I

    move-result v2

    .line 1915627
    invoke-virtual {p0, p1, v5, v1}, LX/15i;->a(III)I

    move-result v3

    .line 1915628
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1915629
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1915630
    invoke-virtual {p3, v4, v2, v1}, LX/186;->a(III)V

    .line 1915631
    invoke-virtual {p3, v5, v3, v1}, LX/186;->a(III)V

    .line 1915632
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1915633
    :sswitch_3
    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1915634
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1915635
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1915636
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1915637
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1915638
    :sswitch_4
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;

    .line 1915639
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1915640
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1915641
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1915642
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1915643
    :cond_1
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v7

    .line 1915644
    if-nez v7, :cond_2

    const/4 v3, 0x0

    .line 1915645
    :goto_2
    if-ge v6, v7, :cond_3

    .line 1915646
    invoke-virtual {p0, v0, v6}, LX/15i;->q(II)I

    move-result p2

    .line 1915647
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v6

    .line 1915648
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1915649
    :cond_2
    new-array v3, v7, [I

    goto :goto_2

    .line 1915650
    :cond_3
    const/4 v6, 0x1

    invoke-virtual {p3, v3, v6}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x38ac4d4 -> :sswitch_2
        0x3680f3dd -> :sswitch_0
        0x3955171d -> :sswitch_1
        0x557cc1a1 -> :sswitch_4
        0x7fc90bf7 -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1915590
    if-nez p0, :cond_0

    move v0, v1

    .line 1915591
    :goto_0
    return v0

    .line 1915592
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1915593
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1915594
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1915595
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1915596
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1915597
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1915598
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1915583
    const/4 v7, 0x0

    .line 1915584
    const/4 v1, 0x0

    .line 1915585
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1915586
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1915587
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1915588
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1915589
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1915582
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1915517
    if-eqz p0, :cond_0

    .line 1915518
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1915519
    if-eq v0, p0, :cond_0

    .line 1915520
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1915521
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1915560
    sparse-switch p2, :sswitch_data_0

    .line 1915561
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1915562
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1915563
    const v1, 0x3955171d

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1915564
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1915565
    const v1, 0x557cc1a1

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1915566
    :goto_0
    :sswitch_1
    return-void

    .line 1915567
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1915568
    const v1, -0x38ac4d4

    .line 1915569
    if-eqz v0, :cond_0

    .line 1915570
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1915571
    const/4 v2, 0x0

    :goto_1
    if-ge v2, p1, :cond_0

    .line 1915572
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1915573
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1915574
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1915575
    :cond_0
    goto :goto_0

    .line 1915576
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1915577
    const v1, 0x7fc90bf7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1915578
    :sswitch_4
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;

    .line 1915579
    invoke-static {v0, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x38ac4d4 -> :sswitch_3
        0x3680f3dd -> :sswitch_0
        0x3955171d -> :sswitch_2
        0x557cc1a1 -> :sswitch_4
        0x7fc90bf7 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1915554
    if-eqz p1, :cond_0

    .line 1915555
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1915556
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;

    .line 1915557
    if-eq v0, v1, :cond_0

    .line 1915558
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1915559
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1915553
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1915653
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1915654
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1915548
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1915549
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1915550
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1915551
    iput p2, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->b:I

    .line 1915552
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1915547
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1915546
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1915543
    iget v0, p0, LX/1vt;->c:I

    .line 1915544
    move v0, v0

    .line 1915545
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1915540
    iget v0, p0, LX/1vt;->c:I

    .line 1915541
    move v0, v0

    .line 1915542
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1915537
    iget v0, p0, LX/1vt;->b:I

    .line 1915538
    move v0, v0

    .line 1915539
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1915534
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1915535
    move-object v0, v0

    .line 1915536
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1915525
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1915526
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1915527
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1915528
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1915529
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1915530
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1915531
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1915532
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1915533
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1915522
    iget v0, p0, LX/1vt;->c:I

    .line 1915523
    move v0, v0

    .line 1915524
    return v0
.end method
