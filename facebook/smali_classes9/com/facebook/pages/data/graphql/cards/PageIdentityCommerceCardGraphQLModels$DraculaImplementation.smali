.class public final Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1913543
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1913544
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1913541
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1913542
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1913500
    if-nez p1, :cond_0

    .line 1913501
    :goto_0
    return v0

    .line 1913502
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1913503
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1913504
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1913505
    const v2, 0x77b2a100

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1913506
    invoke-virtual {p0, p1, v5}, LX/15i;->p(II)I

    move-result v2

    .line 1913507
    const v3, 0x8e61268

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1913508
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1913509
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1913510
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1913511
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1913512
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1913513
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1913514
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1913515
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1913516
    invoke-virtual {p0, p1, v5}, LX/15i;->b(II)Z

    move-result v2

    .line 1913517
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1913518
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1913519
    invoke-virtual {p3, v5, v2}, LX/186;->a(IZ)V

    .line 1913520
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1913521
    :sswitch_2
    const-class v1, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1913522
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1913523
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1913524
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1913525
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1913526
    :sswitch_3
    const-class v1, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1913527
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1913528
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1913529
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1913530
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1913531
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1913532
    const v2, 0x441d4310

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1913533
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1913534
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1913535
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1913536
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1913537
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1913538
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1913539
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1913540
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x8e61268 -> :sswitch_2
        0x1e5ee4fb -> :sswitch_0
        0x20d7d441 -> :sswitch_4
        0x441d4310 -> :sswitch_5
        0x6c50adee -> :sswitch_3
        0x77b2a100 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1913491
    if-nez p0, :cond_0

    move v0, v1

    .line 1913492
    :goto_0
    return v0

    .line 1913493
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1913494
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1913495
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1913496
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1913497
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1913498
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1913499
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1913484
    const/4 v7, 0x0

    .line 1913485
    const/4 v1, 0x0

    .line 1913486
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1913487
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1913488
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1913489
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1913490
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1913483
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1913479
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1913480
    if-eqz v0, :cond_0

    .line 1913481
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1913482
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1913466
    sparse-switch p2, :sswitch_data_0

    .line 1913467
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1913468
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1913469
    const v1, 0x77b2a100

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1913470
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1913471
    const v1, 0x8e61268

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1913472
    :goto_0
    :sswitch_1
    return-void

    .line 1913473
    :sswitch_2
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1913474
    invoke-static {v0, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1913475
    :sswitch_3
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1913476
    invoke-static {v0, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1913477
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1913478
    const v1, 0x441d4310

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x8e61268 -> :sswitch_2
        0x1e5ee4fb -> :sswitch_0
        0x20d7d441 -> :sswitch_4
        0x441d4310 -> :sswitch_1
        0x6c50adee -> :sswitch_3
        0x77b2a100 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1913454
    if-eqz p1, :cond_0

    .line 1913455
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1913456
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;

    .line 1913457
    if-eq v0, v1, :cond_0

    .line 1913458
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1913459
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1913465
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1913545
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1913546
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1913460
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1913461
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1913462
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1913463
    iput p2, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->b:I

    .line 1913464
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1913453
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1913452
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1913449
    iget v0, p0, LX/1vt;->c:I

    .line 1913450
    move v0, v0

    .line 1913451
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1913446
    iget v0, p0, LX/1vt;->c:I

    .line 1913447
    move v0, v0

    .line 1913448
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1913443
    iget v0, p0, LX/1vt;->b:I

    .line 1913444
    move v0, v0

    .line 1913445
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913440
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1913441
    move-object v0, v0

    .line 1913442
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1913431
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1913432
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1913433
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1913434
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1913435
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1913436
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1913437
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1913438
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1913439
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1913428
    iget v0, p0, LX/1vt;->c:I

    .line 1913429
    move v0, v0

    .line 1913430
    return v0
.end method
