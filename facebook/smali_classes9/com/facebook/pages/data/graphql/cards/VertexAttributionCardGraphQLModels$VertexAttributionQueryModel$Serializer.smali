.class public final Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1915785
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1915786
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1915833
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1915788
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1915789
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1915790
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1915791
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1915792
    if-eqz v2, :cond_8

    .line 1915793
    const-string v3, "attribution"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915794
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1915795
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_7

    .line 1915796
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    const/4 v7, 0x2

    .line 1915797
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1915798
    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, LX/15i;->g(II)I

    move-result v5

    .line 1915799
    if-eqz v5, :cond_3

    .line 1915800
    const-string v6, "attribution"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915801
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1915802
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1915803
    if-eqz v6, :cond_1

    .line 1915804
    const-string p0, "ranges"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915805
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1915806
    const/4 p0, 0x0

    :goto_1
    invoke-virtual {v1, v6}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_0

    .line 1915807
    invoke-virtual {v1, v6, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/CZV;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1915808
    add-int/lit8 p0, p0, 0x1

    goto :goto_1

    .line 1915809
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1915810
    :cond_1
    const/4 v6, 0x1

    invoke-virtual {v1, v5, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1915811
    if-eqz v6, :cond_2

    .line 1915812
    const-string p0, "text"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915813
    invoke-virtual {p1, v6}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1915814
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1915815
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {v1, v4, v5}, LX/15i;->g(II)I

    move-result v5

    .line 1915816
    if-eqz v5, :cond_5

    .line 1915817
    const-string v6, "icon"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915818
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1915819
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1915820
    if-eqz v6, :cond_4

    .line 1915821
    const-string p0, "icon_image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915822
    invoke-static {v1, v6, p1}, LX/CZW;->a(LX/15i;ILX/0nX;)V

    .line 1915823
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1915824
    :cond_5
    invoke-virtual {v1, v4, v7}, LX/15i;->g(II)I

    move-result v5

    .line 1915825
    if-eqz v5, :cond_6

    .line 1915826
    const-string v5, "source"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1915827
    invoke-virtual {v1, v4, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1915828
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1915829
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1915830
    :cond_7
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1915831
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1915832
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1915787
    check-cast p1, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Serializer;->a(Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
