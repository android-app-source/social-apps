.class public final Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1916136
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1916137
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1916102
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1916104
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1916105
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1916106
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1916107
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1916108
    if-eqz v2, :cond_0

    .line 1916109
    const-string p0, "featured_video"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1916110
    invoke-static {v1, v2, p1, p2}, LX/5i4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1916111
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1916112
    if-eqz v2, :cond_1

    .line 1916113
    const-string p0, "show_video_hub"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1916114
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1916115
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1916116
    if-eqz v2, :cond_2

    .line 1916117
    const-string p0, "uploaded_videos"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1916118
    invoke-static {v1, v2, p1, p2}, LX/CZa;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1916119
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1916120
    if-eqz v2, :cond_5

    .line 1916121
    const-string p0, "video_collection"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1916122
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1916123
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1916124
    if-eqz p0, :cond_4

    .line 1916125
    const-string v0, "video_lists"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1916126
    const/4 v0, 0x0

    .line 1916127
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1916128
    invoke-virtual {v1, p0, v0, v0}, LX/15i;->a(III)I

    move-result v0

    .line 1916129
    if-eqz v0, :cond_3

    .line 1916130
    const-string v2, "count"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1916131
    invoke-virtual {p1, v0}, LX/0nX;->b(I)V

    .line 1916132
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1916133
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1916134
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1916135
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1916103
    check-cast p1, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Serializer;->a(Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
