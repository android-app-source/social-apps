.class public final Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x61cd44e9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1913607
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1913604
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1913605
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1913606
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913608
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->e:Ljava/lang/String;

    .line 1913609
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913598
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->f:Ljava/lang/String;

    .line 1913599
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOrderedImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1913600
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->g:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x2

    const v4, 0x20d7d441

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->g:LX/3Sb;

    .line 1913601
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->g:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method private m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913602
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->h:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->h:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    .line 1913603
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->h:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1913567
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1913568
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1913569
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1913570
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->l()LX/2uF;

    move-result-object v2

    invoke-static {v2, p1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v2

    .line 1913571
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1913572
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1913573
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1913574
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1913575
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1913576
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1913577
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1913578
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1913579
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1913580
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->l()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1913581
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->l()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1913582
    if-eqz v1, :cond_2

    .line 1913583
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    .line 1913584
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->g:LX/3Sb;

    move-object v1, v0

    .line 1913585
    :goto_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1913586
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    .line 1913587
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1913588
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    .line 1913589
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->h:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    .line 1913590
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1913591
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1913592
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1913593
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$CommerceStoreModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;-><init>()V

    .line 1913594
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1913595
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1913596
    const v0, -0x24760fe9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1913597
    const v0, 0xa7c5482

    return v0
.end method
