.class public final Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1916035
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1916036
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1916037
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1916038
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1916039
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1916040
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1916041
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1916042
    :goto_0
    move v1, v2

    .line 1916043
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1916044
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1916045
    new-instance v1, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;-><init>()V

    .line 1916046
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1916047
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1916048
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1916049
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1916050
    :cond_0
    return-object v1

    .line 1916051
    :cond_1
    const-string v9, "show_video_hub"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1916052
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    .line 1916053
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_6

    .line 1916054
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1916055
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1916056
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_2

    if-eqz v8, :cond_2

    .line 1916057
    const-string v9, "featured_video"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1916058
    invoke-static {p1, v0}, LX/5i4;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1916059
    :cond_3
    const-string v9, "uploaded_videos"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 1916060
    invoke-static {p1, v0}, LX/CZa;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1916061
    :cond_4
    const-string v9, "video_collection"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1916062
    const/4 v8, 0x0

    .line 1916063
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v9, :cond_c

    .line 1916064
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1916065
    :goto_2
    move v4, v8

    .line 1916066
    goto :goto_1

    .line 1916067
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1916068
    :cond_6
    const/4 v8, 0x4

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1916069
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1916070
    if-eqz v1, :cond_7

    .line 1916071
    invoke-virtual {v0, v3, v6}, LX/186;->a(IZ)V

    .line 1916072
    :cond_7
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1916073
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1916074
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_1

    .line 1916075
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1916076
    :cond_a
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_b

    .line 1916077
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1916078
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1916079
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_a

    if-eqz v9, :cond_a

    .line 1916080
    const-string v10, "video_lists"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 1916081
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 1916082
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v11, :cond_11

    .line 1916083
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1916084
    :goto_4
    move v4, v9

    .line 1916085
    goto :goto_3

    .line 1916086
    :cond_b
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1916087
    invoke-virtual {v0, v8, v4}, LX/186;->b(II)V

    .line 1916088
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto :goto_2

    :cond_c
    move v4, v8

    goto :goto_3

    .line 1916089
    :cond_d
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_f

    .line 1916090
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1916091
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1916092
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_d

    if-eqz v12, :cond_d

    .line 1916093
    const-string p0, "count"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_e

    .line 1916094
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v4

    move v11, v4

    move v4, v10

    goto :goto_5

    .line 1916095
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_5

    .line 1916096
    :cond_f
    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1916097
    if-eqz v4, :cond_10

    .line 1916098
    invoke-virtual {v0, v9, v11, v9}, LX/186;->a(III)V

    .line 1916099
    :cond_10
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto :goto_4

    :cond_11
    move v4, v9

    move v11, v9

    goto :goto_5
.end method
