.class public final Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2730a79b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1911856
    const-class v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1911855
    const-class v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1911853
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1911854
    return-void
.end method

.method private a()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCriticReviewsReceived"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911851
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    .line 1911852
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1911845
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1911846
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1911847
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1911848
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1911849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1911850
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1911837
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1911838
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1911839
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    .line 1911840
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1911841
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;

    .line 1911842
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel$CriticReviewsReceivedModel;

    .line 1911843
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1911844
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1911836
    new-instance v0, LX/CYe;

    invoke-direct {v0, p1}, LX/CYe;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1911834
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1911835
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1911833
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1911828
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$CriticReviewsCardQueryModel;-><init>()V

    .line 1911829
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1911830
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1911832
    const v0, -0x13a0096e

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1911831
    const v0, 0x25d6af

    return v0
.end method
