.class public final Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1915701
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1915702
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1915703
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1915704
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1915705
    const/4 v2, 0x0

    .line 1915706
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1915707
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1915708
    :goto_0
    move v1, v2

    .line 1915709
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1915710
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1915711
    new-instance v1, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel;-><init>()V

    .line 1915712
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1915713
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1915714
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1915715
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1915716
    :cond_0
    return-object v1

    .line 1915717
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1915718
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1915719
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1915720
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1915721
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1915722
    const-string v4, "attribution"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1915723
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1915724
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_3

    .line 1915725
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1915726
    const/4 v4, 0x0

    .line 1915727
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_b

    .line 1915728
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1915729
    :goto_3
    move v3, v4

    .line 1915730
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1915731
    :cond_3
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1915732
    goto :goto_1

    .line 1915733
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1915734
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1915735
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_1

    .line 1915736
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1915737
    :cond_7
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_a

    .line 1915738
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1915739
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1915740
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_7

    if-eqz v7, :cond_7

    .line 1915741
    const-string v8, "attribution"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 1915742
    const/4 v7, 0x0

    .line 1915743
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v6, v8, :cond_10

    .line 1915744
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1915745
    :goto_5
    move v6, v7

    .line 1915746
    goto :goto_4

    .line 1915747
    :cond_8
    const-string v8, "icon"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1915748
    const/4 v7, 0x0

    .line 1915749
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_14

    .line 1915750
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1915751
    :goto_6
    move v5, v7

    .line 1915752
    goto :goto_4

    .line 1915753
    :cond_9
    const-string v8, "source"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1915754
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLAttributionSource;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLAttributionSource;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto :goto_4

    .line 1915755
    :cond_a
    const/4 v7, 0x3

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1915756
    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1915757
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1915758
    const/4 v4, 0x2

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1915759
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_3

    :cond_b
    move v3, v4

    move v5, v4

    move v6, v4

    goto :goto_4

    .line 1915760
    :cond_c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1915761
    :cond_d
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, p0, :cond_f

    .line 1915762
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1915763
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1915764
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_d

    if-eqz v9, :cond_d

    .line 1915765
    const-string p0, "ranges"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_e

    .line 1915766
    invoke-static {p1, v0}, LX/CZV;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_7

    .line 1915767
    :cond_e
    const-string p0, "text"

    invoke-virtual {v9, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1915768
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto :goto_7

    .line 1915769
    :cond_f
    const/4 v9, 0x2

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1915770
    invoke-virtual {v0, v7, v8}, LX/186;->b(II)V

    .line 1915771
    const/4 v7, 0x1

    invoke-virtual {v0, v7, v6}, LX/186;->b(II)V

    .line 1915772
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto/16 :goto_5

    :cond_10
    move v6, v7

    move v8, v7

    goto :goto_7

    .line 1915773
    :cond_11
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1915774
    :cond_12
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_13

    .line 1915775
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1915776
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1915777
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, p0, :cond_12

    if-eqz v8, :cond_12

    .line 1915778
    const-string v9, "icon_image"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_11

    .line 1915779
    invoke-static {p1, v0}, LX/CZW;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_8

    .line 1915780
    :cond_13
    const/4 v8, 0x1

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1915781
    invoke-virtual {v0, v7, v5}, LX/186;->b(II)V

    .line 1915782
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto/16 :goto_6

    :cond_14
    move v5, v7

    goto :goto_8
.end method
