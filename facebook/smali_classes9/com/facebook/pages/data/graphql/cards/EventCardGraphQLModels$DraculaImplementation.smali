.class public final Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1912211
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1912212
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1912213
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1912214
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const-wide/16 v4, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1912215
    if-nez p1, :cond_0

    .line 1912216
    :goto_0
    return v1

    .line 1912217
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1912218
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1912219
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1912220
    const v2, 0x173d9cd

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1912221
    const-class v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v8, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel$CoverPhotoModel$PhotoModel;

    .line 1912222
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1912223
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    .line 1912224
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1912225
    invoke-virtual {p3, v8, v0}, LX/186;->b(II)V

    .line 1912226
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1912227
    :sswitch_1
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1912228
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1912229
    invoke-virtual {p3, v9}, LX/186;->c(I)V

    move-object v0, p3

    .line 1912230
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1912231
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1912232
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    .line 1912233
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1912234
    invoke-virtual {p0, p1, v8}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1912235
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1912236
    invoke-virtual {p0, p1, v9, v1}, LX/15i;->a(III)I

    move-result v3

    .line 1912237
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1912238
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1912239
    invoke-virtual {p3, v8, v2}, LX/186;->b(II)V

    .line 1912240
    invoke-virtual {p3, v9, v3, v1}, LX/186;->a(III)V

    .line 1912241
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0xd3f7038 -> :sswitch_0
        0x173d9cd -> :sswitch_1
        0x41e3be30 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1912247
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1912242
    if-eqz p0, :cond_0

    .line 1912243
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1912244
    if-eq v0, p0, :cond_0

    .line 1912245
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1912246
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1912254
    sparse-switch p2, :sswitch_data_0

    .line 1912255
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1912256
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1912257
    const v1, 0x173d9cd

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1912258
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel$CoverPhotoModel$PhotoModel;

    .line 1912259
    invoke-static {v0, p3}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1912260
    :sswitch_1
    return-void

    nop

    :sswitch_data_0
    .sparse-switch
        -0xd3f7038 -> :sswitch_0
        0x173d9cd -> :sswitch_1
        0x41e3be30 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1912248
    if-eqz p1, :cond_0

    .line 1912249
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1912250
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;

    .line 1912251
    if-eq v0, v1, :cond_0

    .line 1912252
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1912253
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1912208
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1912209
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1912210
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1912203
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1912204
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1912205
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1912206
    iput p2, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->b:I

    .line 1912207
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1912202
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1912201
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1912198
    iget v0, p0, LX/1vt;->c:I

    .line 1912199
    move v0, v0

    .line 1912200
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1912195
    iget v0, p0, LX/1vt;->c:I

    .line 1912196
    move v0, v0

    .line 1912197
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1912192
    iget v0, p0, LX/1vt;->b:I

    .line 1912193
    move v0, v0

    .line 1912194
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912189
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1912190
    move-object v0, v0

    .line 1912191
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1912180
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1912181
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1912182
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1912183
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1912184
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1912185
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1912186
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1912187
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1912188
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1912177
    iget v0, p0, LX/1vt;->c:I

    .line 1912178
    move v0, v0

    .line 1912179
    return v0
.end method
