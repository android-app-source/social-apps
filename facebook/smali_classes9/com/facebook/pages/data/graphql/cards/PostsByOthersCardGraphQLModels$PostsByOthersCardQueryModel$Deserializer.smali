.class public final Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1914969
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1914970
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1915017
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1914971
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1914972
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1914973
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_8

    .line 1914974
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1914975
    :goto_0
    move v1, v2

    .line 1914976
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1914977
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1914978
    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PostsByOthersCardGraphQLModels$PostsByOthersCardQueryModel;-><init>()V

    .line 1914979
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1914980
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1914981
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1914982
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1914983
    :cond_0
    return-object v1

    .line 1914984
    :cond_1
    const-string v9, "can_viewer_post"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 1914985
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v6, v4

    move v4, v3

    .line 1914986
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, v9, :cond_5

    .line 1914987
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1914988
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1914989
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v9, v10, :cond_2

    if-eqz v8, :cond_2

    .line 1914990
    const-string v9, "admin_display_preference"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 1914991
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1914992
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_d

    .line 1914993
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1914994
    :goto_2
    move v7, v8

    .line 1914995
    goto :goto_1

    .line 1914996
    :cond_3
    const-string v9, "can_viewer_post_photo_to_timeline"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1914997
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v3

    goto :goto_1

    .line 1914998
    :cond_4
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1914999
    :cond_5
    const/4 v8, 0x3

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1915000
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1915001
    if-eqz v4, :cond_6

    .line 1915002
    invoke-virtual {v0, v3, v6}, LX/186;->a(IZ)V

    .line 1915003
    :cond_6
    if-eqz v1, :cond_7

    .line 1915004
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->a(IZ)V

    .line 1915005
    :cond_7
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto :goto_1

    .line 1915006
    :cond_9
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_b

    .line 1915007
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1915008
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1915009
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_9

    if-eqz v11, :cond_9

    .line 1915010
    const-string p0, "show_posts_by_others"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 1915011
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v7

    move v10, v7

    move v7, v9

    goto :goto_3

    .line 1915012
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1915013
    :cond_b
    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1915014
    if-eqz v7, :cond_c

    .line 1915015
    invoke-virtual {v0, v8, v10}, LX/186;->a(IZ)V

    .line 1915016
    :cond_c
    invoke-virtual {v0}, LX/186;->d()I

    move-result v8

    goto :goto_2

    :cond_d
    move v7, v8

    move v10, v8

    goto :goto_3
.end method
