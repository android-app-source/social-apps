.class public final Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/7oZ;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x18249296
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Z

.field private g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:J

.field private j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:J

.field private u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1912661
    const-class v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1912662
    const-class v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1912663
    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1912664
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V
    .locals 4

    .prologue
    .line 1912665
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->w:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1912666
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1912667
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1912668
    if-eqz v0, :cond_0

    .line 1912669
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x12

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1912670
    :cond_0
    return-void

    .line 1912671
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V
    .locals 4

    .prologue
    .line 1912672
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->y:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1912673
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1912674
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1912675
    if-eqz v0, :cond_0

    .line 1912676
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v3, 0x14

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1912677
    :cond_0
    return-void

    .line 1912678
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1912679
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->s:Ljava/lang/String;

    .line 1912680
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1912681
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1912682
    if-eqz v0, :cond_0

    .line 1912683
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1912684
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1912655
    iput-boolean p1, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->e:Z

    .line 1912656
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1912657
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1912658
    if-eqz v0, :cond_0

    .line 1912659
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1912660
    :cond_0
    return-void
.end method

.method private j()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1912685
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1912686
    iget-boolean v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->e:Z

    return v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912687
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1912688
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->g:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912689
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1912690
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912691
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1912692
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    return-object v0
.end method

.method private n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912693
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1912694
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    return-object v0
.end method

.method private o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912695
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1912696
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    return-object v0
.end method

.method private p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912697
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1912698
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    return-object v0
.end method

.method private q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912699
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    const/16 v1, 0x9

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1912700
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    return-object v0
.end method

.method private r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912701
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1912702
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    return-object v0
.end method

.method private s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912463
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1912464
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    return-object v0
.end method

.method private t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912703
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->q:Ljava/lang/String;

    const/16 v1, 0xc

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->q:Ljava/lang/String;

    .line 1912704
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->q:Ljava/lang/String;

    return-object v0
.end method

.method private u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912461
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->s:Ljava/lang/String;

    const/16 v1, 0xe

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->s:Ljava/lang/String;

    .line 1912462
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->s:Ljava/lang/String;

    return-object v0
.end method

.method private v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912465
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    const/16 v1, 0x10

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1912466
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    return-object v0
.end method

.method private w()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912467
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->v:Ljava/lang/String;

    const/16 v1, 0x11

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->v:Ljava/lang/String;

    .line 1912468
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->v:Ljava/lang/String;

    return-object v0
.end method

.method private x()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912469
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->w:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    const/16 v1, 0x12

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->w:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1912470
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->w:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    return-object v0
.end method

.method private y()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1912471
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->x:Ljava/util/List;

    const/16 v1, 0x13

    const-class v2, Lcom/facebook/events/graphql/EventsGraphQLModels$UserInEventFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->x:Ljava/util/List;

    .line 1912472
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->x:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private z()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912473
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->y:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    const/16 v1, 0x14

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->y:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    .line 1912474
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->y:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 22

    .prologue
    .line 1912475
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1912476
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->k()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1912477
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0xd3f7038

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1912478
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1912479
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1912480
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1912481
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1912482
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1912483
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1912484
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1912485
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->t()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    .line 1912486
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->u()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    .line 1912487
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1912488
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->w()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 1912489
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->x()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 1912490
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->y()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v20

    .line 1912491
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->z()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v21

    .line 1912492
    const/16 v4, 0x15

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1912493
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->e:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1912494
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->f:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1912495
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v2}, LX/186;->b(II)V

    .line 1912496
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1912497
    const/4 v3, 0x4

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->i:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1912498
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1912499
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1912500
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1912501
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1912502
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 1912503
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 1912504
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 1912505
    const/16 v2, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 1912506
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, LX/186;->a(IZ)V

    .line 1912507
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1912508
    const/16 v3, 0xf

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->t:J

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 1912509
    const/16 v2, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1912510
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1912511
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1912512
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1912513
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1912514
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1912515
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    return v2
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1912516
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1912517
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1912518
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0xd3f7038

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1912519
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1912520
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912521
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->h:I

    move-object v1, v0

    .line 1912522
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1912523
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1912524
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1912525
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912526
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1912527
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1912528
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1912529
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1912530
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912531
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1912532
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1912533
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1912534
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1912535
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912536
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1912537
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1912538
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1912539
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->p()Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1912540
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912541
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m:Lcom/facebook/events/graphql/EventsGraphQLModels$EventPlaceModel;

    .line 1912542
    :cond_4
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1912543
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1912544
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->q()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1912545
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912546
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMaybesFirst5Model;

    .line 1912547
    :cond_5
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1912548
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1912549
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->r()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1912550
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912551
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventMembersFirst5Model;

    .line 1912552
    :cond_6
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1912553
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1912554
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->s()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1912555
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912556
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->p:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$FriendEventWatchersFirst5Model;

    .line 1912557
    :cond_7
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1912558
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1912559
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->v()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1912560
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912561
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->u:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$SuggestedEventContextSentenceModel;

    .line 1912562
    :cond_8
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->y()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 1912563
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->y()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1912564
    if-eqz v2, :cond_9

    .line 1912565
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    .line 1912566
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->x:Ljava/util/List;

    move-object v1, v0

    .line 1912567
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1912568
    if-nez v1, :cond_a

    :goto_0
    return-object p0

    .line 1912569
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_a
    move-object p0, v1

    .line 1912570
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1912571
    new-instance v0, LX/CYl;

    invoke-direct {v0, p1}, LX/CYl;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912572
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1912573
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1912574
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->e:Z

    .line 1912575
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->f:Z

    .line 1912576
    const/4 v0, 0x3

    const v1, -0xd3f7038

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->h:I

    .line 1912577
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->i:J

    .line 1912578
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->r:Z

    .line 1912579
    const/16 v0, 0xf

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->t:J

    .line 1912580
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1912581
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1912582
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1912583
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1912584
    iput v2, p2, LX/18L;->c:I

    .line 1912585
    :goto_0
    return-void

    .line 1912586
    :cond_0
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1912587
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    .line 1912588
    if-eqz v0, :cond_6

    .line 1912589
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1912590
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1912591
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1912592
    :cond_1
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1912593
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    .line 1912594
    if-eqz v0, :cond_6

    .line 1912595
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1912596
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1912597
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1912598
    :cond_2
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1912599
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    .line 1912600
    if-eqz v0, :cond_6

    .line 1912601
    invoke-virtual {v0}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1912602
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1912603
    iput v2, p2, LX/18L;->c:I

    goto :goto_0

    .line 1912604
    :cond_3
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1912605
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->u()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1912606
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1912607
    const/16 v0, 0xe

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1912608
    :cond_4
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1912609
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->x()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1912610
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1912611
    const/16 v0, 0x12

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1912612
    :cond_5
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1912613
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->z()Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1912614
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1912615
    const/16 v0, 0x14

    iput v0, p2, LX/18L;->c:I

    goto/16 :goto_0

    .line 1912616
    :cond_6
    invoke-virtual {p2}, LX/18L;->a()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1912617
    const-string v0, "can_viewer_change_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1912618
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->a(Z)V

    .line 1912619
    :cond_0
    :goto_0
    return-void

    .line 1912620
    :cond_1
    const-string v0, "event_declines.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1912621
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->m()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    move-result-object v0

    .line 1912622
    if-eqz v0, :cond_0

    .line 1912623
    if-eqz p3, :cond_2

    .line 1912624
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    .line 1912625
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a(I)V

    .line 1912626
    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->j:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;

    goto :goto_0

    .line 1912627
    :cond_2
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventDeclinesModel;->a(I)V

    goto :goto_0

    .line 1912628
    :cond_3
    const-string v0, "event_maybes.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1912629
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->n()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    move-result-object v0

    .line 1912630
    if-eqz v0, :cond_0

    .line 1912631
    if-eqz p3, :cond_4

    .line 1912632
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    .line 1912633
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a(I)V

    .line 1912634
    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->k:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;

    goto :goto_0

    .line 1912635
    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMaybesModel;->a(I)V

    goto :goto_0

    .line 1912636
    :cond_5
    const-string v0, "event_members.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1912637
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->o()Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    move-result-object v0

    .line 1912638
    if-eqz v0, :cond_0

    .line 1912639
    if-eqz p3, :cond_6

    .line 1912640
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    .line 1912641
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a(I)V

    .line 1912642
    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->l:Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;

    goto :goto_0

    .line 1912643
    :cond_6
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/graphql/EventsGraphQLModels$EventSocialContextFieldsModel$EventMembersModel;->a(I)V

    goto/16 :goto_0

    .line 1912644
    :cond_7
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1912645
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1912646
    :cond_8
    const-string v0, "viewer_guest_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1912647
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-direct {p0, p2}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->a(Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;)V

    goto/16 :goto_0

    .line 1912648
    :cond_9
    const-string v0, "viewer_watch_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1912649
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-direct {p0, p2}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;->a(Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)V

    goto/16 :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1912650
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel$NodesModel;-><init>()V

    .line 1912651
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1912652
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1912653
    const v0, 0x466c3a72

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1912654
    const v0, 0x403827a

    return v0
.end method
