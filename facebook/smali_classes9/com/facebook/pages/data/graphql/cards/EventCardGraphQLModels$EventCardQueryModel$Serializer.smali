.class public final Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1912315
    const-class v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1912316
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1912296
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1912297
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1912298
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    .line 1912299
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1912300
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1912301
    if-eqz v2, :cond_0

    .line 1912302
    const-string v3, "events_calendar_can_viewer_subscribe"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912303
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1912304
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1912305
    if-eqz v2, :cond_1

    .line 1912306
    const-string v2, "events_calendar_subscription_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912307
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1912308
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1912309
    if-eqz v2, :cond_2

    .line 1912310
    const-string v3, "owned_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1912311
    invoke-static {v1, v2, p1, p2}, LX/CYr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1912312
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1912313
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1912314
    check-cast p1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Serializer;->a(Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
