.class public final Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x77cea5ca
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1914558
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1914557
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1914555
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1914556
    return-void
.end method

.method private a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhotosTakenHere"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914553
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    .line 1914554
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1914547
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1914548
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1914549
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1914550
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1914551
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1914552
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1914539
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1914540
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1914541
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    .line 1914542
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;->a()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1914543
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;

    .line 1914544
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;->e:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    .line 1914545
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1914546
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1914538
    new-instance v0, LX/CZ9;

    invoke-direct {v0, p1}, LX/CZ9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1914536
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1914537
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1914530
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1914533
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel;-><init>()V

    .line 1914534
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1914535
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1914532
    const v0, 0x155dfdc3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1914531
    const v0, 0x25d6af

    return v0
.end method
