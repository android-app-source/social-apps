.class public final Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1914490
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1914491
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1914489
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1914486
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1914487
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/CZH;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1914488
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1914485
    check-cast p1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel$Serializer;->a(Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$UnownedLocalPagePhotosQueryModel$PhotosTakenHereModel;LX/0nX;LX/0my;)V

    return-void
.end method
