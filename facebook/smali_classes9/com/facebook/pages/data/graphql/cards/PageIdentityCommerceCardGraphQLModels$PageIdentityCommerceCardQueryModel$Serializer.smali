.class public final Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1913752
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1913753
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1913754
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1913755
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1913756
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1913757
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1913758
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1913759
    if-eqz v2, :cond_7

    .line 1913760
    const-string v3, "commerce_store"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913761
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1913762
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1913763
    if-eqz v3, :cond_2

    .line 1913764
    const-string p0, "commerce_merchant_settings"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913765
    const/4 p0, 0x0

    .line 1913766
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1913767
    invoke-virtual {v1, v3, p0, p0}, LX/15i;->a(III)I

    move-result p0

    .line 1913768
    if-eqz p0, :cond_0

    .line 1913769
    const-string v0, "payment_provider"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913770
    invoke-virtual {p1, p0}, LX/0nX;->b(I)V

    .line 1913771
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v3, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1913772
    if-eqz p0, :cond_1

    .line 1913773
    const-string v0, "show_edit_interface"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913774
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1913775
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1913776
    :cond_2
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1913777
    if-eqz v3, :cond_5

    .line 1913778
    const-string p0, "ordered_collections"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913779
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1913780
    const/4 p0, 0x0

    invoke-virtual {v1, v3, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1913781
    if-eqz p0, :cond_4

    .line 1913782
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913783
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1913784
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 1913785
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v3

    invoke-static {v1, v3, p1, p2}, LX/CZ1;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1913786
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1913787
    :cond_3
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1913788
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1913789
    :cond_5
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1913790
    if-eqz v3, :cond_6

    .line 1913791
    const-string p0, "url"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1913792
    invoke-virtual {p1, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1913793
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1913794
    :cond_7
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1913795
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1913796
    check-cast p1, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel$Serializer;->a(Lcom/facebook/pages/data/graphql/cards/PageIdentityCommerceCardGraphQLModels$PageIdentityCommerceCardQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
