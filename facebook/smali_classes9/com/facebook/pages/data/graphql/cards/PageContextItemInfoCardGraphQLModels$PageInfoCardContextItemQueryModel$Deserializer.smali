.class public final Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1913215
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1913216
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1913214
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1913156
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1913157
    const/4 v10, 0x0

    .line 1913158
    const/4 v9, 0x0

    .line 1913159
    const/4 v8, 0x0

    .line 1913160
    const/4 v7, 0x0

    .line 1913161
    const/4 v6, 0x0

    .line 1913162
    const/4 v5, 0x0

    .line 1913163
    const/4 v4, 0x0

    .line 1913164
    const/4 v3, 0x0

    .line 1913165
    const/4 v2, 0x0

    .line 1913166
    const/4 v1, 0x0

    .line 1913167
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_OBJECT:LX/15z;

    if-eq v11, p0, :cond_2

    .line 1913168
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913169
    const/4 v1, 0x0

    .line 1913170
    :goto_0
    move v1, v1

    .line 1913171
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1913172
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1913173
    new-instance v1, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/data/graphql/cards/PageContextItemInfoCardGraphQLModels$PageInfoCardContextItemQueryModel;-><init>()V

    .line 1913174
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1913175
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1913176
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1913177
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1913178
    :cond_0
    return-object v1

    .line 1913179
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1913180
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_a

    .line 1913181
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1913182
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1913183
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1913184
    const-string p0, "address"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1913185
    invoke-static {p1, v0}, LX/4aS;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1913186
    :cond_3
    const-string p0, "contextItemInfoCards"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1913187
    invoke-static {p1, v0}, LX/5Ny;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1913188
    :cond_4
    const-string p0, "is_place_map_hidden"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1913189
    const/4 v2, 0x1

    .line 1913190
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 1913191
    :cond_5
    const-string p0, "location"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1913192
    invoke-static {p1, v0}, LX/4aX;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1913193
    :cond_6
    const-string p0, "map_bounding_box"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1913194
    invoke-static {p1, v0}, LX/CYv;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 1913195
    :cond_7
    const-string p0, "map_zoom_level"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1913196
    const/4 v1, 0x1

    .line 1913197
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v5

    goto :goto_1

    .line 1913198
    :cond_8
    const-string p0, "menu_info"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_9

    .line 1913199
    invoke-static {p1, v0}, LX/CYw;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1913200
    :cond_9
    const-string p0, "place_type"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1913201
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPlaceType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    goto/16 :goto_1

    .line 1913202
    :cond_a
    const/16 v11, 0x8

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1913203
    const/4 v11, 0x0

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 1913204
    const/4 v10, 0x1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 1913205
    if-eqz v2, :cond_b

    .line 1913206
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 1913207
    :cond_b
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1913208
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1913209
    if-eqz v1, :cond_c

    .line 1913210
    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v5, v2}, LX/186;->a(III)V

    .line 1913211
    :cond_c
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 1913212
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1913213
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
