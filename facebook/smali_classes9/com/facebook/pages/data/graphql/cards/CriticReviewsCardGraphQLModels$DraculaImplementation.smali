.class public final Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1911926
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1911927
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1911924
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1911925
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1911890
    if-nez p1, :cond_0

    .line 1911891
    :goto_0
    return v0

    .line 1911892
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1911893
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1911894
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1911895
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1911896
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1911897
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1911898
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1911899
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1911900
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1911901
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1911902
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1911903
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1911904
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1911905
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1911906
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1911907
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1911908
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1911909
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1911910
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1911911
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1911912
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1911913
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1911914
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1911915
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1911916
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1911917
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1911918
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1911919
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1911920
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1911921
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1911922
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1911923
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x23d76252 -> :sswitch_2
        0x9939334 -> :sswitch_3
        0x2a7ef9ef -> :sswitch_5
        0x406d6b79 -> :sswitch_1
        0x424574dd -> :sswitch_0
        0x7ac8856d -> :sswitch_4
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1911889
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1911886
    sparse-switch p0, :sswitch_data_0

    .line 1911887
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1911888
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x23d76252 -> :sswitch_0
        0x9939334 -> :sswitch_0
        0x2a7ef9ef -> :sswitch_0
        0x406d6b79 -> :sswitch_0
        0x424574dd -> :sswitch_0
        0x7ac8856d -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1911885
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1911883
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->b(I)V

    .line 1911884
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1911928
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1911929
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1911930
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1911931
    iput p2, p0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->b:I

    .line 1911932
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1911882
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1911857
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1911879
    iget v0, p0, LX/1vt;->c:I

    .line 1911880
    move v0, v0

    .line 1911881
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1911876
    iget v0, p0, LX/1vt;->c:I

    .line 1911877
    move v0, v0

    .line 1911878
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1911873
    iget v0, p0, LX/1vt;->b:I

    .line 1911874
    move v0, v0

    .line 1911875
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911870
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1911871
    move-object v0, v0

    .line 1911872
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1911861
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1911862
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1911863
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1911864
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1911865
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1911866
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1911867
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1911868
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/CriticReviewsCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1911869
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1911858
    iget v0, p0, LX/1vt;->c:I

    .line 1911859
    move v0, v0

    .line 1911860
    return v0
.end method
