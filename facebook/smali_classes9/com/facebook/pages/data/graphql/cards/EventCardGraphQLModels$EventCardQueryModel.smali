.class public final Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x31ab087d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1912366
    const-class v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1912365
    const-class v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1912363
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1912364
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912361
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->f:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->f:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 1912362
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->f:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;)V
    .locals 4

    .prologue
    .line 1912354
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->f:Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    .line 1912355
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1912356
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1912357
    if-eqz v0, :cond_0

    .line 1912358
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1912359
    :cond_0
    return-void

    .line 1912360
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwnedEvents"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1912352
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->g:Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->g:Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    .line 1912353
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->g:Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1912317
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1912318
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1912319
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->j()Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1912320
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1912321
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->e:Z

    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 1912322
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1912323
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1912324
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1912325
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1912344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1912345
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->j()Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1912346
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->j()Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    .line 1912347
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->j()Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1912348
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;

    .line 1912349
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->g:Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$PageEventsConnectionModel;

    .line 1912350
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1912351
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1912343
    new-instance v0, LX/CYk;

    invoke-direct {v0, p1}, LX/CYk;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1912340
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1912341
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->e:Z

    .line 1912342
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1912334
    const-string v0, "events_calendar_subscription_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1912335
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1912336
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1912337
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1912338
    :goto_0
    return-void

    .line 1912339
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1912331
    const-string v0, "events_calendar_subscription_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1912332
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;

    invoke-direct {p0, p2}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;->a(Lcom/facebook/graphql/enums/GraphQLEventsCalendarSubscriptionStatus;)V

    .line 1912333
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1912328
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/EventCardGraphQLModels$EventCardQueryModel;-><init>()V

    .line 1912329
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1912330
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1912327
    const v0, -0x60312964

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1912326
    const v0, 0x25d6af

    return v0
.end method
