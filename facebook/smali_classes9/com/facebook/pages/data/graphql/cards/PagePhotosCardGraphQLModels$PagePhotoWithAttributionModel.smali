.class public final Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/1U8;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x9f6bb49
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1914365
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1914366
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1914367
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1914368
    return-void
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914369
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1914370
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914371
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914372
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914375
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914376
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914373
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914374
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914351
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914352
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method

.method private p()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914440
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    .line 1914441
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    return-object v0
.end method

.method private q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrivacyScope"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1914442
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1914443
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 14

    .prologue
    .line 1914417
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1914418
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1914419
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1914420
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1914421
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 1914422
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1914423
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 1914424
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1914425
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->p()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1914426
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->q()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x2c963aff

    invoke-static {v2, v1, v3}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1914427
    const/16 v1, 0xa

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1914428
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1914429
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1914430
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1914431
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1914432
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1914433
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1914434
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1914435
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v11}, LX/186;->b(II)V

    .line 1914436
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v12}, LX/186;->b(II)V

    .line 1914437
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v13}, LX/186;->b(II)V

    .line 1914438
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1914439
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1914377
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1914378
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1914379
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1914380
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1914381
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;

    .line 1914382
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1914383
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1914384
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914385
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1914386
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;

    .line 1914387
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914388
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1914389
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914390
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1914391
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;

    .line 1914392
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->j:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914393
    :cond_2
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1914394
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914395
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1914396
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;

    .line 1914397
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914398
    :cond_3
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1914399
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914400
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1914401
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;

    .line 1914402
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1914403
    :cond_4
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->p()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1914404
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->p()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    .line 1914405
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->p()Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1914406
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;

    .line 1914407
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m:Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$OwnerModel;

    .line 1914408
    :cond_5
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 1914409
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2c963aff

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1914410
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1914411
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;

    .line 1914412
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->n:I

    move-object v1, v0

    .line 1914413
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1914414
    if-nez v1, :cond_7

    :goto_0
    return-object p0

    .line 1914415
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    move-object p0, v1

    .line 1914416
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1914363
    new-instance v0, LX/CZ7;

    invoke-direct {v0, p1}, LX/CZ7;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914364
    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1914339
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1914340
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->f:J

    .line 1914341
    const/16 v0, 0x9

    const v1, 0x2c963aff

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->n:I

    .line 1914342
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1914343
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1914344
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1914345
    return-void
.end method

.method public final synthetic ai_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914346
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic aj_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914347
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914348
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1914349
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1914350
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1914353
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;-><init>()V

    .line 1914354
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1914355
    return-object v0
.end method

.method public final synthetic c()LX/1f8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914356
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914357
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->h:Ljava/lang/String;

    .line 1914358
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1914359
    const v0, 0x582e0b0f

    return v0
.end method

.method public final synthetic e()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914360
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1914361
    const v0, 0x4984e12

    return v0
.end method

.method public final synthetic j()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914362
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method
