.class public final Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2b81edc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1915697
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1915696
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1915694
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1915695
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1915675
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;->f:Ljava/lang/String;

    .line 1915676
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1915686
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1915687
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1915688
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1915689
    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;->e:I

    invoke-virtual {p1, v2, v1, v2}, LX/186;->a(III)V

    .line 1915690
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1915691
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1915692
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1915693
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1915698
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1915699
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1915700
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1915682
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1915683
    invoke-virtual {p1, p2, v1, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;->e:I

    .line 1915684
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;->g:I

    .line 1915685
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1915679
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/VertexAttributionCardGraphQLModels$VertexAttributionQueryModel$AttributionModel$IconModel$IconImageModel;-><init>()V

    .line 1915680
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1915681
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1915678
    const v0, -0x727fed52

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1915677
    const v0, 0x437b93b

    return v0
.end method
