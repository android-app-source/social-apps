.class public final Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x21bec32a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1916183
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1916236
    const-class v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1916234
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1916235
    return-void
.end method

.method private a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1916232
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 1916233
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    return-object v0
.end method

.method private j()Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1916230
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->g:Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    iput-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->g:Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    .line 1916231
    iget-object v0, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->g:Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    return-object v0
.end method

.method private k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideoCollection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1916228
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1916229
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1916217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1916218
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1916219
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->j()Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1916220
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, -0x3972b9c0    # -18083.125f

    invoke-static {v3, v2, v4}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1916221
    const/4 v3, 0x4

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1916222
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1916223
    const/4 v0, 0x1

    iget-boolean v3, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->f:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1916224
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1916225
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1916226
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1916227
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1916197
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1916198
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1916199
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 1916200
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->a()Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1916201
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;

    .line 1916202
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->e:Lcom/facebook/photos/albums/protocols/VideosUploadedByUserGraphQLModels$VideoDetailFragmentModel;

    .line 1916203
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->j()Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1916204
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->j()Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    .line 1916205
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->j()Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1916206
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;

    .line 1916207
    iput-object v0, v1, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->g:Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel$UploadedVideosModel;

    .line 1916208
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1916209
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x3972b9c0    # -18083.125f

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1916210
    invoke-direct {p0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1916211
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;

    .line 1916212
    iput v3, v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->h:I

    move-object v1, v0

    .line 1916213
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1916214
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1916215
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 1916216
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1916196
    new-instance v0, LX/CZY;

    invoke-direct {v0, p1}, LX/CZY;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1916192
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1916193
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->f:Z

    .line 1916194
    const/4 v0, 0x3

    const v1, -0x3972b9c0    # -18083.125f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;->h:I

    .line 1916195
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1916190
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1916191
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1916189
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1916186
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;

    invoke-direct {v0}, Lcom/facebook/pages/data/graphql/cards/VideoCardGraphQLModels$VideoCardQueryModel;-><init>()V

    .line 1916187
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1916188
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1916185
    const v0, -0x41850892

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1916184
    const v0, 0x25d6af

    return v0
.end method
