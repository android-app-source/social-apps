.class public final Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1914105
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1914106
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1914103
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1914104
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1914094
    if-nez p1, :cond_0

    move v0, v1

    .line 1914095
    :goto_0
    return v0

    .line 1914096
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1914097
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1914098
    :pswitch_0
    const-class v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$PrivacyScopeModel$IconImageModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$PrivacyScopeModel$IconImageModel;

    .line 1914099
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1914100
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1914101
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1914102
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2c963aff
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1914093
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1914088
    if-eqz p0, :cond_0

    .line 1914089
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1914090
    if-eq v0, p0, :cond_0

    .line 1914091
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1914092
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1914050
    packed-switch p2, :pswitch_data_0

    .line 1914051
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1914052
    :pswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$PrivacyScopeModel$IconImageModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$PagePhotoWithAttributionModel$PrivacyScopeModel$IconImageModel;

    .line 1914053
    invoke-static {v0, p3}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1914054
    return-void

    :pswitch_data_0
    .packed-switch 0x2c963aff
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1914087
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1914085
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1914086
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1914080
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1914081
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1914082
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1914083
    iput p2, p0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->b:I

    .line 1914084
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1914049
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1914055
    new-instance v0, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1914056
    iget v0, p0, LX/1vt;->c:I

    .line 1914057
    move v0, v0

    .line 1914058
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1914059
    iget v0, p0, LX/1vt;->c:I

    .line 1914060
    move v0, v0

    .line 1914061
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1914062
    iget v0, p0, LX/1vt;->b:I

    .line 1914063
    move v0, v0

    .line 1914064
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1914065
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1914066
    move-object v0, v0

    .line 1914067
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1914068
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1914069
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1914070
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1914071
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1914072
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1914073
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1914074
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1914075
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/data/graphql/cards/PagePhotosCardGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1914076
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1914077
    iget v0, p0, LX/1vt;->c:I

    .line 1914078
    move v0, v0

    .line 1914079
    return v0
.end method
