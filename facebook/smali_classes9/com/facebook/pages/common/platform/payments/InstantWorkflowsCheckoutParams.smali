.class public Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/checkout/CheckoutParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "orderInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation
.end field

.field public final b:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

.field public final c:Lcom/facebook/payments/checkout/CheckoutCommonParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1896459
    new-instance v0, LX/CUJ;

    invoke-direct {v0}, LX/CUJ;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1896482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896483
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->a:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    .line 1896484
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->b:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    .line 1896485
    const-class v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/checkout/CheckoutCommonParams;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->c:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 1896486
    return-void
.end method

.method public constructor <init>(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1896466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896467
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896468
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1896469
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896470
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->jX_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896471
    invoke-virtual {p1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1896472
    invoke-virtual {p2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1896473
    iput-object p1, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->a:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    .line 1896474
    iput-object p2, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->b:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    .line 1896475
    iput-object p3, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->c:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    .line 1896476
    return-void

    :cond_0
    move v0, v2

    .line 1896477
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1896478
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1896479
    goto :goto_2

    :cond_3
    move v1, v2

    .line 1896480
    goto :goto_3
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/checkout/CheckoutCommonParams;
    .locals 1

    .prologue
    .line 1896481
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->c:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/checkout/CheckoutCommonParams;)Lcom/facebook/payments/checkout/CheckoutParams;
    .locals 3

    .prologue
    .line 1896465
    new-instance v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->a:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->b:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    invoke-direct {v0, v1, v2, p1}, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;-><init>(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;Lcom/facebook/payments/checkout/CheckoutCommonParams;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1896464
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1896460
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->a:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1896461
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->b:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1896462
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsCheckoutParams;->c:Lcom/facebook/payments/checkout/CheckoutCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1896463
    return-void
.end method
