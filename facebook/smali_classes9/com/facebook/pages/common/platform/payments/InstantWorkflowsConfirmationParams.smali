.class public Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/confirmation/ConfirmationParams;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1896515
    new-instance v0, LX/CUP;

    invoke-direct {v0}, LX/CUP;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0lB;Lcom/facebook/payments/confirmation/ConfirmationCommonParams;Ljava/lang/String;LX/0lF;)V
    .locals 2

    .prologue
    .line 1896516
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896517
    iput-object p2, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    .line 1896518
    iput-object p3, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->b:Ljava/lang/String;

    .line 1896519
    const-string v0, "title"

    invoke-virtual {p4, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->d:Ljava/lang/String;

    .line 1896520
    const-string v0, "message_with_email"

    invoke-virtual {p4, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->e:Ljava/lang/String;

    .line 1896521
    const-string v0, "receipt_url"

    invoke-virtual {p4, v0}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->c:Ljava/lang/String;

    .line 1896522
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    .line 1896523
    :try_start_0
    new-instance v0, LX/0lp;

    invoke-direct {v0}, LX/0lp;-><init>()V

    .line 1896524
    const-string v1, "email_ranges"

    invoke-virtual {p4, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0lp;->b(Ljava/lang/String;)LX/15w;

    move-result-object v0

    .line 1896525
    invoke-virtual {v0, p1}, LX/15w;->a(LX/0lD;)V

    .line 1896526
    invoke-virtual {v0}, LX/15w;->J()LX/0lG;

    move-result-object v0

    check-cast v0, LX/162;

    .line 1896527
    if-eqz v0, :cond_1

    .line 1896528
    invoke-virtual {v0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v1

    .line 1896529
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1896530
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    .line 1896531
    if-eqz v0, :cond_0

    const-string p2, "offset"

    invoke-virtual {v0, p2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    const-string p2, "length"

    invoke-virtual {v0, p2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p2

    if-eqz p2, :cond_0

    .line 1896532
    iget-object p2, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    const-string p3, "offset"

    invoke-virtual {v0, p3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p3

    invoke-static {p3}, LX/16N;->d(LX/0lF;)I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896533
    iget-object p2, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    const-string p3, "length"

    invoke-virtual {v0, p3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->d(LX/0lF;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1896534
    :cond_1
    :goto_1
    return-void

    :catch_0
    goto :goto_1
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    .line 1896535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1896536
    const-class v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    .line 1896537
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->b:Ljava/lang/String;

    .line 1896538
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->d:Ljava/lang/String;

    .line 1896539
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->e:Ljava/lang/String;

    .line 1896540
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->c:Ljava/lang/String;

    .line 1896541
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    .line 1896542
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 1896543
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1896544
    iget-object v2, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1896545
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1896546
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/payments/confirmation/ConfirmationCommonParams;
    .locals 1

    .prologue
    .line 1896547
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1896548
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1896549
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->a:Lcom/facebook/payments/confirmation/ConfirmationCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1896550
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1896551
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1896552
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1896553
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1896554
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1896555
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1896556
    iget-object v0, p0, Lcom/facebook/pages/common/platform/payments/InstantWorkflowsConfirmationParams;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1896557
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1896558
    :cond_0
    return-void
.end method
