.class public final Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/CT9;


# direct methods
.method public constructor <init>(LX/CT9;Ljava/util/ArrayList;LX/0Px;)V
    .locals 0

    .prologue
    .line 1895353
    iput-object p1, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->c:LX/CT9;

    iput-object p2, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->a:Ljava/util/ArrayList;

    iput-object p3, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->b:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 1895315
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    .line 1895316
    iget-object v0, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->c:LX/CT9;

    iget-object v0, v0, LX/CT9;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    .line 1895317
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 1895318
    check-cast v0, LX/CSW;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3rL;

    iget-object v1, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->b:Ljava/lang/Object;

    check-cast v2, Ljava/util/ArrayList;

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1895319
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v7

    .line 1895320
    iget-object v4, v0, LX/CSW;->j:LX/CUD;

    .line 1895321
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895322
    invoke-static {v4, v1}, LX/CUD;->a(LX/CUD;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v9

    .line 1895323
    if-nez v9, :cond_8

    .line 1895324
    const/4 v8, 0x0

    .line 1895325
    :goto_1
    move-object v8, v8

    .line 1895326
    if-nez v8, :cond_5

    .line 1895327
    :cond_0
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1895328
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->c:LX/CT9;

    iget-object v0, v0, LX/CT9;->a:LX/CTC;

    iget-object v0, v0, LX/CTC;->w:Landroid/support/v7/widget/RecyclerView;

    .line 1895329
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 1895330
    check-cast v0, LX/CSW;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/infra/PlatformSessionController$8$1;->b:LX/0Px;

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1895331
    iget-object v2, v0, LX/CSW;->j:LX/CUD;

    sget-object v3, LX/CU9;->UNSHIMMER:LX/CU9;

    invoke-virtual {v2, v3, v1}, LX/CUD;->a(LX/CU9;LX/0Px;)Ljava/util/ArrayList;

    move-result-object v7

    .line 1895332
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v6, v5

    :goto_3
    if-ge v6, v8, :cond_4

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 1895333
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ltz v3, :cond_2

    move v3, v4

    :goto_4
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 1895334
    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-lt v9, v3, :cond_3

    move v3, v4

    :goto_5
    invoke-static {v3}, LX/0PB;->checkState(Z)V

    .line 1895335
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v3, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sub-int v2, v3, v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v9, v2}, LX/1OM;->a(II)V

    .line 1895336
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_3

    :cond_2
    move v3, v5

    .line 1895337
    goto :goto_4

    :cond_3
    move v3, v5

    .line 1895338
    goto :goto_5

    .line 1895339
    :cond_4
    return-void

    .line 1895340
    :cond_5
    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ltz v4, :cond_6

    move v4, v5

    :goto_6
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 1895341
    iget-object v4, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v9

    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lt v9, v4, :cond_7

    :goto_7
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 1895342
    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget-object v4, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v6

    iget-object v4, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    sub-int v4, v6, v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v0, v5, v4}, LX/1OM;->a(II)V

    .line 1895343
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v4

    if-ge v4, v7, :cond_0

    .line 1895344
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v4

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v5

    sub-int v5, v7, v5

    invoke-virtual {v0, v4, v5}, LX/1OM;->d(II)V

    goto/16 :goto_2

    :cond_6
    move v4, v6

    .line 1895345
    goto :goto_6

    :cond_7
    move v5, v6

    .line 1895346
    goto :goto_7

    .line 1895347
    :cond_8
    iget-object v8, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 1895348
    iget-object v8, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 1895349
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/2addr v8, v10

    add-int/lit8 v11, v8, -0x1

    .line 1895350
    iget-object v8, v4, LX/CUD;->c:Ljava/util/ArrayList;

    add-int/lit8 v12, v9, 0x1

    invoke-virtual {v8, v10, v12}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->clear()V

    .line 1895351
    iget-object v8, v4, LX/CUD;->c:Ljava/util/ArrayList;

    invoke-virtual {v8, v10, v2}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    .line 1895352
    new-instance v8, Landroid/util/Pair;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v9, v11}, Ljava/lang/Math;->max(II)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v8, v10, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1
.end method
