.class public Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field private static final p:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private q:LX/0gc;

.field private r:Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;

.field public s:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Activity$OnActivityResultHandler;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1894567
    const/16 v0, 0x2b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->p:LX/0Rf;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1894574
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v0, -0x1

    .line 1894568
    if-eq p2, v0, :cond_0

    .line 1894569
    :goto_0
    return-void

    .line 1894570
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1894571
    const/4 v0, 0x0

    const-string v1, "Unhandled case"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    goto :goto_0

    .line 1894572
    :pswitch_0
    invoke-virtual {p0, v0, p3}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->setResult(ILandroid/content/Intent;)V

    .line 1894573
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2b
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1894575
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1894576
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->q:LX/0gc;

    .line 1894577
    const v0, 0x7f030fd7

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->setContentView(I)V

    .line 1894578
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->s:Ljava/util/HashMap;

    .line 1894579
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "cta_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "referrer"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "initial_input"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/CSQ;

    invoke-direct {v4, p0}, LX/CSQ;-><init>(Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;)V

    .line 1894580
    new-instance p1, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;

    invoke-direct {p1}, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;-><init>()V

    .line 1894581
    iput-object v0, p1, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->c:Ljava/lang/String;

    .line 1894582
    iput-object v1, p1, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->d:Ljava/lang/String;

    .line 1894583
    iput-object v2, p1, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->e:Ljava/lang/String;

    .line 1894584
    iput-object v3, p1, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->f:Ljava/lang/String;

    .line 1894585
    iput-object v4, p1, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->q:LX/CSQ;

    .line 1894586
    move-object v0, p1

    .line 1894587
    iput-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->r:Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;

    .line 1894588
    const v0, 0x7f0400c8

    const v1, 0x7f040030

    invoke-virtual {p0, v0, v1}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->overridePendingTransition(II)V

    .line 1894589
    iget-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->q:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->r:Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1894590
    iget-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->q:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1894591
    return-void
.end method

.method public final finish()V
    .locals 0

    .prologue
    .line 1894555
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1894556
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1894557
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1894558
    iget-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->s:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1894559
    iget-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->s:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CWg;

    invoke-virtual {v0, p2, p3}, LX/CWg;->a(ILandroid/content/Intent;)V

    .line 1894560
    iget-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->s:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1894561
    :cond_0
    :goto_0
    return-void

    .line 1894562
    :cond_1
    sget-object v0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->p:LX/0Rf;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1894563
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->a(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1894564
    iget-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->r:Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;

    instance-of v0, v0, LX/0fj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/platform/activity/PlatformFirstPartyFlowActivity;->r:Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->S_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1894565
    :goto_0
    return-void

    .line 1894566
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
