.class public Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public c:LX/1vv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1910487
    const-class v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/1vv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1910488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1910489
    iput-object p1, p0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b:Landroid/content/res/Resources;

    .line 1910490
    iput-object p2, p0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->c:LX/1vv;

    .line 1910491
    return-void
.end method

.method public static a(Landroid/text/SpannableStringBuilder;IILcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;)V
    .locals 2

    .prologue
    .line 1910492
    sget-object v0, LX/CXf;->b:[I

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1910493
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected text style!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1910494
    :pswitch_0
    const/4 v0, 0x1

    .line 1910495
    :goto_0
    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    const/16 v0, 0x11

    invoke-static {p0, v1, p1, p2, v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    .line 1910496
    return-void

    .line 1910497
    :pswitch_1
    const/4 v0, 0x2

    .line 1910498
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V
    .locals 1

    .prologue
    .line 1910499
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1910500
    if-ltz p2, :cond_0

    if-ge p2, v0, :cond_0

    if-lt p3, p2, :cond_0

    if-le p3, v0, :cond_1

    .line 1910501
    :cond_0
    :goto_0
    return-void

    .line 1910502
    :cond_1
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .locals 3

    .prologue
    .line 1910503
    new-instance v2, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1vv;->a(LX/0QB;)LX/1vv;

    move-result-object v1

    check-cast v1, LX/1vv;

    invoke-direct {v2, v0, v1}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;-><init>(Landroid/content/res/Resources;LX/1vv;)V

    .line 1910504
    return-object v2
.end method

.method public static b(Landroid/text/SpannableStringBuilder;IILcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;)V
    .locals 2

    .prologue
    .line 1910505
    sget-object v0, LX/CXf;->b:[I

    invoke-virtual {p3}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1910506
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not color text style!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1910507
    :pswitch_0
    const v0, -0x6f6b64

    .line 1910508
    :goto_0
    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v1, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v0, 0x11

    invoke-static {p0, v1, p1, p2, v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    .line 1910509
    return-void

    .line 1910510
    :pswitch_1
    const v0, -0x5c1c2

    .line 1910511
    goto :goto_0

    .line 1910512
    :pswitch_2
    const v0, -0xbd48d6

    .line 1910513
    goto :goto_0

    .line 1910514
    :pswitch_3
    const v0, -0xbd984e

    .line 1910515
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V
    .locals 12

    .prologue
    .line 1910516
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910517
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1910518
    iget-object v1, p2, LX/CU1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1910519
    const/16 v6, 0x11

    .line 1910520
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910521
    iget-object v1, p2, LX/CU1;->c:Ljava/util/ArrayList;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910522
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, p2, LX/CU1;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 1910523
    iget-object v1, p2, LX/CU1;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CTx;

    .line 1910524
    sget-object v3, LX/CXf;->b:[I

    iget-object v4, v1, LX/CTx;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 1910525
    :goto_1
    :pswitch_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1910526
    :pswitch_1
    iget v3, v1, LX/CTx;->a:I

    iget v4, v1, LX/CTx;->a:I

    iget v5, v1, LX/CTx;->b:I

    add-int/2addr v4, v5

    iget-object v1, v1, LX/CTx;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-static {v0, v3, v4, v1}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(Landroid/text/SpannableStringBuilder;IILcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;)V

    goto :goto_1

    .line 1910527
    :pswitch_2
    iget v3, v1, LX/CTx;->a:I

    iget v4, v1, LX/CTx;->a:I

    iget v5, v1, LX/CTx;->b:I

    add-int/2addr v4, v5

    iget-object v1, v1, LX/CTx;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    invoke-static {v0, v3, v4, v1}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Landroid/text/SpannableStringBuilder;IILcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;)V

    goto :goto_1

    .line 1910528
    :pswitch_3
    new-instance v3, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v3}, Landroid/text/style/StrikethroughSpan;-><init>()V

    iget v4, v1, LX/CTx;->a:I

    iget v5, v1, LX/CTx;->a:I

    iget v1, v1, LX/CTx;->b:I

    add-int/2addr v1, v5

    invoke-static {v0, v3, v4, v1, v6}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    goto :goto_1

    .line 1910529
    :pswitch_4
    new-instance v3, Landroid/text/style/UnderlineSpan;

    invoke-direct {v3}, Landroid/text/style/UnderlineSpan;-><init>()V

    iget v4, v1, LX/CTx;->a:I

    iget v5, v1, LX/CTx;->a:I

    iget v1, v1, LX/CTx;->b:I

    add-int/2addr v1, v5

    invoke-static {v0, v3, v4, v1, v6}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    goto :goto_1

    .line 1910530
    :pswitch_5
    new-instance v3, LX/CXe;

    invoke-direct {v3, p0}, LX/CXe;-><init>(Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    iget v4, v1, LX/CTx;->a:I

    iget v5, v1, LX/CTx;->a:I

    iget v1, v1, LX/CTx;->b:I

    add-int/2addr v1, v5

    invoke-static {v0, v3, v4, v1, v6}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    goto :goto_1

    .line 1910531
    :cond_0
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910532
    iget-object v1, p2, LX/CU1;->b:Ljava/util/ArrayList;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1910533
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    iget-object v1, p2, LX/CU1;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 1910534
    iget-object v1, p2, LX/CU1;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CU2;

    .line 1910535
    sget-object v3, LX/CXf;->a:[I

    iget-object v4, v1, LX/CU2;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    .line 1910536
    :cond_1
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 1910537
    :pswitch_6
    iget-object v3, v1, LX/CU2;->d:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1910538
    new-instance v3, Landroid/text/style/URLSpan;

    iget-object v4, v1, LX/CU2;->d:Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/text/style/URLSpan;-><init>(Ljava/lang/String;)V

    iget v4, v1, LX/CU2;->a:I

    iget v5, v1, LX/CU2;->a:I

    iget v1, v1, LX/CU2;->b:I

    add-int/2addr v1, v5

    const/16 v5, 0x11

    invoke-static {v0, v3, v4, v1, v5}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/Object;III)V

    goto :goto_3

    .line 1910539
    :cond_2
    move-object v0, v0

    .line 1910540
    invoke-virtual {p1, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910541
    iget-object v0, p2, LX/CU1;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    if-eqz v0, :cond_3

    iget-object v0, p2, LX/CU1;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    if-ne v0, v1, :cond_7

    .line 1910542
    :cond_3
    :goto_4
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1910543
    const/4 v4, -0x1

    .line 1910544
    new-instance v2, LX/2nQ;

    invoke-virtual {p1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v2, v1}, LX/2nQ;-><init>(Ljava/lang/CharSequence;)V

    .line 1910545
    new-instance v5, Ljava/util/TreeSet;

    sget-object v1, LX/1vv;->a:Ljava/util/Comparator;

    invoke-direct {v5, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 1910546
    iget-object v6, p2, LX/CU1;->b:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    const/4 v1, 0x0

    move v3, v1

    :goto_5
    if-ge v3, v7, :cond_5

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CU2;

    .line 1910547
    iget-object v8, v1, LX/CU2;->d:Ljava/lang/String;

    if-eqz v8, :cond_4

    iget-object v8, v1, LX/CU2;->c:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    sget-object v9, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->ICON:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    if-ne v8, v9, :cond_4

    .line 1910548
    new-instance v8, LX/34R;

    iget-object v9, v1, LX/CU2;->d:Ljava/lang/String;

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    new-instance v10, LX/1yN;

    iget v11, v1, LX/CU2;->a:I

    iget v1, v1, LX/CU2;->b:I

    invoke-direct {v10, v11, v1}, LX/1yN;-><init>(II)V

    invoke-direct {v8, v9, v10, v4, v4}, LX/34R;-><init>(Landroid/net/Uri;LX/1yN;II)V

    invoke-virtual {v5, v8}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 1910549
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5

    .line 1910550
    :cond_5
    invoke-virtual {v5}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_6
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v6, v1

    check-cast v6, LX/34R;

    .line 1910551
    iget-object v1, p0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->c:LX/1vv;

    iget-object v3, v6, LX/34R;->a:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->getLineHeight()I

    move-result v5

    iget-object v6, v6, LX/34R;->b:LX/1yN;

    const/4 v7, 0x2

    invoke-static {v7}, LX/34T;->a(I)I

    move-result v7

    sget-object v8, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {v1 .. v8}, LX/1vv;->a(LX/2nQ;Landroid/net/Uri;IILX/1yN;ILcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_6

    .line 1910552
    :cond_6
    invoke-virtual {p1, v2}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setDraweeSpanStringBuilder(LX/2nQ;)V

    .line 1910553
    return-void

    .line 1910554
    :cond_7
    sget-object v0, LX/CXf;->c:[I

    iget-object v1, p2, LX/CU1;->d:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_4

    .line 1910555
    :pswitch_7
    iget-object v0, p0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1910556
    :goto_7
    const/4 v1, 0x0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setTextSize(IF)V

    goto/16 :goto_4

    .line 1910557
    :pswitch_8
    iget-object v0, p0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_7

    .line 1910558
    :pswitch_9
    iget-object v0, p0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b:Landroid/content/res/Resources;

    const v1, 0x7f0b004e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
