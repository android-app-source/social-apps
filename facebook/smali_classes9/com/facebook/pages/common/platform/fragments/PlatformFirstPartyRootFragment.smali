.class public Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/CTD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/CTC;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field private g:Landroid/support/v7/widget/RecyclerView;

.field private h:Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;

.field private i:Lcom/facebook/fig/button/FigButton;

.field private j:Landroid/view/View;

.field private k:Landroid/widget/FrameLayout;

.field private l:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

.field private m:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

.field private n:Landroid/widget/LinearLayout;

.field private o:Landroid/widget/LinearLayout;

.field private p:Landroid/view/View;

.field public q:LX/CSQ;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1894649
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1894646
    iget-object v0, p0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->b:LX/CTC;

    .line 1894647
    iget-object p0, v0, LX/CTC;->i:LX/CT6;

    invoke-virtual {p0}, LX/CT6;->a()V

    .line 1894648
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1894621
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1894622
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;

    const-class v0, LX/CTD;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/CTD;

    iput-object p1, p0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->a:LX/CTD;

    .line 1894623
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xc9f88b4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1894645
    const v1, 0x7f030fd8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x563283cf

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 17
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1894624
    const v1, 0x7f0d263d

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->g:Landroid/support/v7/widget/RecyclerView;

    .line 1894625
    const v1, 0x7f0d2639

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->h:Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;

    .line 1894626
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->h:Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;

    const v1, 0x7f0d2638

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->a(Landroid/view/ViewGroup;)V

    .line 1894627
    const v1, 0x7f0d263f

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->i:Lcom/facebook/fig/button/FigButton;

    .line 1894628
    const v1, 0x7f0d263e

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->j:Landroid/view/View;

    .line 1894629
    const v1, 0x7f0d04ad

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->k:Landroid/widget/FrameLayout;

    .line 1894630
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->k:Landroid/widget/FrameLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 1894631
    const v1, 0x7f0d31d2

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->l:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    .line 1894632
    const v1, 0x7f0d31d3

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->m:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    .line 1894633
    const v1, 0x7f0d263c

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->n:Landroid/widget/LinearLayout;

    .line 1894634
    const v1, 0x7f0d263a

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->o:Landroid/widget/LinearLayout;

    .line 1894635
    const v1, 0x7f0d263b

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->p:Landroid/view/View;

    .line 1894636
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->getItemAnimator()LX/1Of;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/1Of;->a(Z)V

    .line 1894637
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->g:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, LX/1P0;

    invoke-virtual/range {p0 .. p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1894638
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->d:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->d:Ljava/lang/String;

    const-string v2, "unknown"

    invoke-static {v1, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->c:Ljava/lang/String;

    .line 1894639
    :goto_0
    const-string v1, "unknown"

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->e:Ljava/lang/String;

    invoke-static {v1, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1894640
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->e:Ljava/lang/String;

    .line 1894641
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->a:LX/CTD;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->f:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->g:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->h:Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->i:Lcom/facebook/fig/button/FigButton;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->j:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->k:Landroid/widget/FrameLayout;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->l:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->m:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->n:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->o:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->p:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->q:LX/CSQ;

    new-instance v15, LX/CST;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, LX/CST;-><init>(Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;)V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->e:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v1 .. v16}, LX/CTD;->a(Ljava/lang/String;Ljava/lang/String;Landroid/support/v7/widget/RecyclerView;LX/CXY;Lcom/facebook/fig/button/FigButton;Landroid/view/View;Landroid/widget/FrameLayout;Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;Landroid/widget/LinearLayout;Landroid/widget/LinearLayout;Landroid/view/View;LX/CSQ;LX/CST;Ljava/lang/String;)LX/CTC;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->b:LX/CTC;

    .line 1894642
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->b:LX/CTC;

    invoke-virtual {v1}, LX/CTC;->a()V

    .line 1894643
    return-void

    .line 1894644
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/common/platform/fragments/PlatformFirstPartyRootFragment;->d:Ljava/lang/String;

    goto :goto_0
.end method
