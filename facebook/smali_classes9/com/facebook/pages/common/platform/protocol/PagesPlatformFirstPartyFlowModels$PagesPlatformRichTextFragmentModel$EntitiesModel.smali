.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7bdc7f60
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1899760
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1899766
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1899764
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1899765
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1899761
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1899762
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1899763
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;
    .locals 9

    .prologue
    .line 1899705
    if-nez p0, :cond_0

    .line 1899706
    const/4 p0, 0x0

    .line 1899707
    :goto_0
    return-object p0

    .line 1899708
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    if-eqz v0, :cond_1

    .line 1899709
    check-cast p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    goto :goto_0

    .line 1899710
    :cond_1
    new-instance v0, LX/CV3;

    invoke-direct {v0}, LX/CV3;-><init>()V

    .line 1899711
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->a()Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    move-result-object v1

    iput-object v1, v0, LX/CV3;->a:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 1899712
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->b()I

    move-result v1

    iput v1, v0, LX/CV3;->b:I

    .line 1899713
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->c()I

    move-result v1

    iput v1, v0, LX/CV3;->c:I

    .line 1899714
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CV3;->d:Ljava/lang/String;

    .line 1899715
    const/4 v6, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 1899716
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1899717
    iget-object v3, v0, LX/CV3;->a:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    invoke-virtual {v2, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    .line 1899718
    iget-object v5, v0, LX/CV3;->d:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1899719
    const/4 v7, 0x4

    invoke-virtual {v2, v7}, LX/186;->c(I)V

    .line 1899720
    invoke-virtual {v2, v8, v3}, LX/186;->b(II)V

    .line 1899721
    iget v3, v0, LX/CV3;->b:I

    invoke-virtual {v2, v6, v3, v8}, LX/186;->a(III)V

    .line 1899722
    const/4 v3, 0x2

    iget v7, v0, LX/CV3;->c:I

    invoke-virtual {v2, v3, v7, v8}, LX/186;->a(III)V

    .line 1899723
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v5}, LX/186;->b(II)V

    .line 1899724
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1899725
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1899726
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1899727
    invoke-virtual {v3, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1899728
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1899729
    new-instance v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    invoke-direct {v3, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;-><init>(LX/15i;)V

    .line 1899730
    move-object p0, v3

    .line 1899731
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1899750
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1899751
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->a()Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1899752
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1899753
    const/4 v2, 0x4

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1899754
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1899755
    const/4 v0, 0x1

    iget v2, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->f:I

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 1899756
    const/4 v0, 0x2

    iget v2, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->g:I

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 1899757
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1899758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1899759
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1899747
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1899748
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1899749
    return-object p0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899767
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    .line 1899768
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->e:Lcom/facebook/graphql/enums/GraphQLPagesPlatformRichTextEntityType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1899743
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1899744
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->f:I

    .line 1899745
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->g:I

    .line 1899746
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 1899741
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899742
    iget v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->f:I

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1899738
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;-><init>()V

    .line 1899739
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1899740
    return-object v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 1899736
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899737
    iget v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->g:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899734
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->h:Ljava/lang/String;

    .line 1899735
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1899733
    const v0, 0x61e3fd54    # 5.2570814E20f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1899732
    const v0, 0x4e622983    # 9.4859283E8f

    return v0
.end method
