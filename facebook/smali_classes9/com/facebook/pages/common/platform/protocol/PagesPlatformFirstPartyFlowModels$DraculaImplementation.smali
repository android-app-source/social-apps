.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1898035
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1898036
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1898404
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1898405
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 8

    .prologue
    .line 1898063
    if-nez p1, :cond_0

    .line 1898064
    const/4 v0, 0x0

    .line 1898065
    :goto_0
    return v0

    .line 1898066
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1898067
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1898068
    :sswitch_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898069
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898070
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1898071
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1898072
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1898073
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1898074
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1898075
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1898076
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1898077
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1898078
    const/4 v5, 0x5

    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 1898079
    invoke-virtual {p3, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1898080
    const/4 v6, 0x6

    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v6

    .line 1898081
    invoke-virtual {p3, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1898082
    const/4 v7, 0x7

    invoke-virtual {p3, v7}, LX/186;->c(I)V

    .line 1898083
    const/4 v7, 0x0

    invoke-virtual {p3, v7, v0}, LX/186;->b(II)V

    .line 1898084
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898085
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1898086
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1898087
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1898088
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v5}, LX/186;->b(II)V

    .line 1898089
    const/4 v0, 0x6

    invoke-virtual {p3, v0, v6}, LX/186;->b(II)V

    .line 1898090
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1898091
    :sswitch_1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898092
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898093
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898094
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898095
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898096
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1898097
    const v1, 0x45f6ac0a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1898098
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1898099
    const v2, -0x4436c6b7

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1898100
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898101
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1898102
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898103
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898104
    :sswitch_3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898105
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898106
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898107
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898108
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898109
    :sswitch_4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898110
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898111
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898112
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898113
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898114
    :sswitch_5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898115
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898116
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1898117
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1898118
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1898119
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1898120
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1898121
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1898122
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1898123
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1898124
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898125
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1898126
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1898127
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898128
    :sswitch_6
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1898129
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1898130
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898131
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898132
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898133
    :sswitch_7
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1898134
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1898135
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1898136
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898137
    :sswitch_8
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1898138
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1898139
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1898140
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898141
    :sswitch_9
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1898142
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1898143
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1898144
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898145
    :sswitch_a
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1898146
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1898147
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1898148
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(III)I

    move-result v2

    .line 1898149
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1898150
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v0, v4}, LX/186;->a(III)V

    .line 1898151
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898152
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v2, v1}, LX/186;->a(III)V

    .line 1898153
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898154
    :sswitch_b
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1898155
    const v1, 0x5bae196f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1898156
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1898157
    const v2, -0x13145d37

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1898158
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898159
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1898160
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898161
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898162
    :sswitch_c
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898163
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898164
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898165
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898166
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898167
    :sswitch_d
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898168
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898169
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898170
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898171
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898172
    :sswitch_e
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898173
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898174
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1898175
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1898176
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898177
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1898178
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898179
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898180
    :sswitch_f
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1898181
    const v1, -0x34b87718    # -1.3076712E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1898182
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    move-result-object v1

    .line 1898183
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1898184
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898185
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1898186
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898187
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898188
    :sswitch_10
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898189
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898190
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898191
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898192
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898193
    :sswitch_11
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1898194
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898195
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1898196
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1898197
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898198
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1898199
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898200
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898201
    :sswitch_12
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898202
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1898203
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1898204
    const v2, 0x2ea003e7

    invoke-static {p0, v0, v2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1898205
    const/4 v0, 0x2

    const-class v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898206
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898207
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1898208
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v1}, LX/186;->b(II)V

    .line 1898209
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1898210
    const/4 v1, 0x2

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898211
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898212
    :sswitch_13
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1898213
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1898214
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1898215
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898216
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 1898217
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898218
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898219
    :sswitch_14
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v2

    .line 1898220
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1898221
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1898222
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898223
    :sswitch_15
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    .line 1898224
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898225
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    move-result-object v1

    .line 1898226
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1898227
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898228
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1898229
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898230
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898231
    :sswitch_16
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1898232
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1898233
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1898234
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1898235
    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v3, v4}, LX/15i;->a(III)I

    move-result v3

    .line 1898236
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1898237
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p3, v4, v0, v5}, LX/186;->a(III)V

    .line 1898238
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1898239
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1898240
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v3, v1}, LX/186;->a(III)V

    .line 1898241
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898242
    :sswitch_17
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    .line 1898243
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898244
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1898245
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1898246
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898247
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1898248
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898249
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898250
    :sswitch_18
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898251
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898252
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898253
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898254
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898255
    :sswitch_19
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1898256
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 1898257
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1898258
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1898259
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1898260
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->e(II)Ljava/util/Iterator;

    move-result-object v4

    invoke-static {v4}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v4

    .line 1898261
    invoke-virtual {p3, v4}, LX/186;->a(Ljava/util/List;)I

    move-result v4

    .line 1898262
    const/4 v5, 0x5

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1898263
    const/4 v5, 0x0

    invoke-virtual {p3, v5, v0}, LX/186;->a(IZ)V

    .line 1898264
    const/4 v0, 0x1

    const/4 v5, 0x0

    invoke-virtual {p3, v0, v1, v5}, LX/186;->a(III)V

    .line 1898265
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->a(IZ)V

    .line 1898266
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1898267
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1898268
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898269
    :sswitch_1a
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898270
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1898271
    const/4 v0, 0x1

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1898272
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1898273
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1898274
    const v3, -0x1c5238ac

    invoke-static {p0, v0, v3, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1898275
    const/4 v0, 0x3

    const-class v4, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898276
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898277
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1898278
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v1}, LX/186;->b(II)V

    .line 1898279
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1898280
    const/4 v1, 0x2

    invoke-virtual {p3, v1, v3}, LX/186;->b(II)V

    .line 1898281
    const/4 v1, 0x3

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898282
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898283
    :sswitch_1b
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1898284
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1898285
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1898286
    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->a(III)I

    move-result v2

    .line 1898287
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1898288
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v0, v4}, LX/186;->a(III)V

    .line 1898289
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898290
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v2, v1}, LX/186;->a(III)V

    .line 1898291
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898292
    :sswitch_1c
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1898293
    const/4 v0, 0x1

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    .line 1898294
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898295
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v2

    .line 1898296
    const v3, -0x2897e73b

    invoke-static {p0, v2, v3, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v2

    .line 1898297
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1898298
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1898299
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1898300
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p3, v4, v1, v5}, LX/186;->a(III)V

    .line 1898301
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898302
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1898303
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1898304
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898305
    :sswitch_1d
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898306
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898307
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesPlatformButtonGlyphType;

    move-result-object v1

    .line 1898308
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1898309
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1898310
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v3

    .line 1898311
    const v4, -0x40afefad

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1898312
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1898313
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1898314
    const/4 v5, 0x5

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1898315
    const/4 v5, 0x0

    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1898316
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898317
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->a(IZ)V

    .line 1898318
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1898319
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1898320
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898321
    :sswitch_1e
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898322
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898323
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898324
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898325
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898326
    :sswitch_1f
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898327
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1898328
    const/4 v0, 0x1

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;

    .line 1898329
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898330
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1898331
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1898332
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1898333
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1898334
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v1}, LX/186;->b(II)V

    .line 1898335
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898336
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1898337
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->a(IZ)V

    .line 1898338
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898339
    :sswitch_20
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    .line 1898340
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1898341
    const/4 v0, 0x1

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898342
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898343
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1898344
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v1}, LX/186;->b(II)V

    .line 1898345
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898346
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898347
    :sswitch_21
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1898348
    const v1, 0x2ea003e7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1898349
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898350
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1898351
    const/4 v0, 0x2

    const-class v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898352
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898353
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1898354
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v1}, LX/186;->b(II)V

    .line 1898355
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1898356
    const/4 v1, 0x2

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898357
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898358
    :sswitch_22
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898359
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898360
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898361
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898362
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898363
    :sswitch_23
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v1

    .line 1898364
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898365
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1898366
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1898367
    const v3, 0x26f30b65

    invoke-static {p0, v0, v3, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1898368
    const/4 v0, 0x4

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->a(III)I

    move-result v4

    .line 1898369
    const/4 v0, 0x5

    const-class v5, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {p0, p1, v0, v5}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    .line 1898370
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898371
    const/4 v5, 0x6

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1898372
    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p3, v5, v1, v6}, LX/186;->a(III)V

    .line 1898373
    const/4 v1, 0x2

    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1898374
    const/4 v1, 0x3

    invoke-virtual {p3, v1, v3}, LX/186;->b(II)V

    .line 1898375
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v4, v2}, LX/186;->a(III)V

    .line 1898376
    const/4 v1, 0x5

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898377
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898378
    :sswitch_24
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898379
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1898380
    const/4 v0, 0x1

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898381
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898382
    const/4 v2, 0x2

    const-class v3, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$TimeSlotSectionModel$TimeSlotGroupsModel;

    invoke-virtual {p0, p1, v2, v3}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    .line 1898383
    invoke-static {p3, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1898384
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1898385
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v1}, LX/186;->b(II)V

    .line 1898386
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898387
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1898388
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898389
    :sswitch_25
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1898390
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1898391
    const v2, 0x3ca4e33b

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1898392
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1898393
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1898394
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1898395
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->a(IZ)V

    .line 1898396
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1898397
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1898398
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1898399
    :sswitch_26
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1898400
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1898401
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, LX/186;->c(I)V

    .line 1898402
    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1898403
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7969ad84 -> :sswitch_8
        -0x696e62b7 -> :sswitch_23
        -0x67ea05b7 -> :sswitch_5
        -0x67d54b32 -> :sswitch_12
        -0x65666c3a -> :sswitch_2
        -0x64c54929 -> :sswitch_21
        -0x5f4401ec -> :sswitch_1a
        -0x4ac1f473 -> :sswitch_1f
        -0x4ac0d5b4 -> :sswitch_24
        -0x47e2e9de -> :sswitch_11
        -0x45f7548c -> :sswitch_25
        -0x4436c6b7 -> :sswitch_4
        -0x434f4abe -> :sswitch_e
        -0x40afefad -> :sswitch_1e
        -0x34b87718 -> :sswitch_10
        -0x30cf865f -> :sswitch_1d
        -0x2897e73b -> :sswitch_14
        -0x279ab4e9 -> :sswitch_15
        -0x21aeca47 -> :sswitch_0
        -0x1c5238ac -> :sswitch_1b
        -0x13145d37 -> :sswitch_d
        -0x3ed3a02 -> :sswitch_1
        -0x347c35b -> :sswitch_19
        0x4d6717c -> :sswitch_a
        0x9787328 -> :sswitch_17
        0x15d473dd -> :sswitch_18
        0x1b46b811 -> :sswitch_9
        0x26f30b65 -> :sswitch_13
        0x2a5a73a7 -> :sswitch_b
        0x2c67c036 -> :sswitch_6
        0x2ea003e7 -> :sswitch_16
        0x3ca4e33b -> :sswitch_26
        0x41f00cd4 -> :sswitch_20
        0x45f6ac0a -> :sswitch_3
        0x4e3af34f -> :sswitch_f
        0x593eb926 -> :sswitch_22
        0x5bae196f -> :sswitch_c
        0x6565cba5 -> :sswitch_1c
        0x6cd307e8 -> :sswitch_7
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1898054
    if-nez p0, :cond_0

    move v0, v1

    .line 1898055
    :goto_0
    return v0

    .line 1898056
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1898057
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1898058
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1898059
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1898060
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1898061
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1898062
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1898047
    const/4 v7, 0x0

    .line 1898048
    const/4 v1, 0x0

    .line 1898049
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1898050
    invoke-static {v2, v3, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1898051
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1898052
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1898053
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1898046
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1898042
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1898043
    if-eqz v0, :cond_0

    .line 1898044
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1898045
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1898037
    if-eqz p0, :cond_0

    .line 1898038
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1898039
    if-eq v0, p0, :cond_0

    .line 1898040
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1898041
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 6

    .prologue
    const v5, 0x2ea003e7

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1897972
    sparse-switch p2, :sswitch_data_0

    .line 1897973
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1897974
    :sswitch_0
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1897975
    const v1, 0x45f6ac0a

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1897976
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1897977
    const v1, -0x4436c6b7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1897978
    :goto_0
    :sswitch_1
    return-void

    .line 1897979
    :sswitch_2
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$ContainerModel$ElementsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1897980
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    .line 1897981
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1897982
    const v1, 0x5bae196f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1897983
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1897984
    const v1, -0x13145d37

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1897985
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1897986
    const v1, -0x34b87718    # -1.3076712E7f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1897987
    :sswitch_5
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1897988
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1897989
    :sswitch_6
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1897990
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1897991
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1897992
    invoke-static {p0, v0, v5, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1897993
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1897994
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1897995
    :sswitch_7
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformEventListenersFragmentModel$EventHandlerModel;

    .line 1897996
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1897997
    :sswitch_8
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFragmentModel;

    .line 1897998
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1897999
    :sswitch_9
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898000
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1898001
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1898002
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1898003
    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v0

    .line 1898004
    const v1, -0x1c5238ac

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1898005
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898006
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    .line 1898007
    :sswitch_a
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformTimeSlotFragmentModel$ProductModel;

    .line 1898008
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1898009
    invoke-virtual {p0, p1, v3}, LX/15i;->p(II)I

    move-result v0

    .line 1898010
    const v1, -0x2897e73b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1898011
    :sswitch_b
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v0

    .line 1898012
    const v1, -0x40afefad

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    .line 1898013
    :sswitch_c
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDataFragmentModel$NavbarActionModel$ActionEventHandlerModel;

    .line 1898014
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    .line 1898015
    :sswitch_d
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    .line 1898016
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1898017
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898018
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    .line 1898019
    :sswitch_e
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1898020
    invoke-static {p0, v0, v5, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1898021
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898022
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    .line 1898023
    :sswitch_f
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v0

    .line 1898024
    const v1, 0x26f30b65

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1898025
    const/4 v0, 0x5

    const-class v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    .line 1898026
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto/16 :goto_0

    .line 1898027
    :sswitch_10
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898028
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1898029
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1898030
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1898031
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$TimeSlotSectionModel$TimeSlotGroupsModel;

    invoke-virtual {p0, p1, v3, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1898032
    invoke-static {v0, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto/16 :goto_0

    .line 1898033
    :sswitch_11
    invoke-virtual {p0, p1, v2}, LX/15i;->p(II)I

    move-result v0

    .line 1898034
    const v1, 0x3ca4e33b

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7969ad84 -> :sswitch_1
        -0x696e62b7 -> :sswitch_f
        -0x67ea05b7 -> :sswitch_1
        -0x67d54b32 -> :sswitch_6
        -0x65666c3a -> :sswitch_0
        -0x64c54929 -> :sswitch_e
        -0x5f4401ec -> :sswitch_9
        -0x4ac1f473 -> :sswitch_c
        -0x4ac0d5b4 -> :sswitch_10
        -0x47e2e9de -> :sswitch_5
        -0x45f7548c -> :sswitch_11
        -0x4436c6b7 -> :sswitch_1
        -0x434f4abe -> :sswitch_1
        -0x40afefad -> :sswitch_1
        -0x34b87718 -> :sswitch_1
        -0x30cf865f -> :sswitch_b
        -0x2897e73b -> :sswitch_1
        -0x279ab4e9 -> :sswitch_7
        -0x21aeca47 -> :sswitch_1
        -0x1c5238ac -> :sswitch_1
        -0x13145d37 -> :sswitch_1
        -0x3ed3a02 -> :sswitch_1
        -0x347c35b -> :sswitch_1
        0x4d6717c -> :sswitch_1
        0x9787328 -> :sswitch_8
        0x15d473dd -> :sswitch_1
        0x1b46b811 -> :sswitch_1
        0x26f30b65 -> :sswitch_1
        0x2a5a73a7 -> :sswitch_3
        0x2c67c036 -> :sswitch_2
        0x2ea003e7 -> :sswitch_1
        0x3ca4e33b -> :sswitch_1
        0x41f00cd4 -> :sswitch_d
        0x45f6ac0a -> :sswitch_1
        0x4e3af34f -> :sswitch_4
        0x593eb926 -> :sswitch_1
        0x5bae196f -> :sswitch_1
        0x6565cba5 -> :sswitch_a
        0x6cd307e8 -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1897966
    if-eqz p1, :cond_0

    .line 1897967
    invoke-static {p0, p1, p2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v1

    .line 1897968
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    .line 1897969
    if-eq v0, v1, :cond_0

    .line 1897970
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1897971
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1898406
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1897933
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1897934
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1897936
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1897937
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1897938
    :cond_0
    iput-object p1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a:LX/15i;

    .line 1897939
    iput p2, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->b:I

    .line 1897940
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1897935
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1897941
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1897942
    iget v0, p0, LX/1vt;->c:I

    .line 1897943
    move v0, v0

    .line 1897944
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1897945
    iget v0, p0, LX/1vt;->c:I

    .line 1897946
    move v0, v0

    .line 1897947
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1897948
    iget v0, p0, LX/1vt;->b:I

    .line 1897949
    move v0, v0

    .line 1897950
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1897951
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1897952
    move-object v0, v0

    .line 1897953
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1897954
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1897955
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1897956
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1897957
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1897958
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1897959
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1897960
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1897961
    invoke-static {v3, v9, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1897962
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1897963
    iget v0, p0, LX/1vt;->c:I

    .line 1897964
    move v0, v0

    .line 1897965
    return v0
.end method
