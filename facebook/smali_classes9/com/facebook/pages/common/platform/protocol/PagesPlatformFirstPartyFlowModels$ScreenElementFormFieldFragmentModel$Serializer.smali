.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1900726
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1900727
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1900862
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1900729
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1900730
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x19

    const/16 v7, 0x17

    const/16 v6, 0xc

    const/16 v5, 0xa

    const/4 v4, 0x0

    .line 1900731
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1900732
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1900733
    if-eqz v2, :cond_0

    .line 1900734
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900735
    invoke-static {v1, v0, v4, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1900736
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900737
    if-eqz v2, :cond_1

    .line 1900738
    const-string v3, "additional_fees"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900739
    invoke-static {v1, v2, p1, p2}, LX/CW7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900740
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1900741
    if-eqz v2, :cond_2

    .line 1900742
    const-string v3, "allow_multi_select"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900743
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1900744
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900745
    if-eqz v2, :cond_3

    .line 1900746
    const-string v3, "available_time_slots"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900747
    invoke-static {v1, v2, p1, p2}, LX/CVf;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900748
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900749
    if-eqz v2, :cond_4

    .line 1900750
    const-string v3, "available_times"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900751
    invoke-static {v1, v2, p1, p2}, LX/CVG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900752
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900753
    if-eqz v2, :cond_5

    .line 1900754
    const-string v3, "default_value"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900755
    invoke-static {v1, v2, p1}, LX/CVm;->a(LX/15i;ILX/0nX;)V

    .line 1900756
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900757
    if-eqz v2, :cond_6

    .line 1900758
    const-string v3, "description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900759
    invoke-static {v1, v2, p1}, LX/CVn;->a(LX/15i;ILX/0nX;)V

    .line 1900760
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1900761
    if-eqz v2, :cond_7

    .line 1900762
    const-string v3, "disable_autofill"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900763
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1900764
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900765
    if-eqz v2, :cond_8

    .line 1900766
    const-string v3, "event_listeners"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900767
    invoke-static {v1, v2, p1, p2}, LX/CVU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900768
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900769
    if-eqz v2, :cond_9

    .line 1900770
    const-string v3, "fee"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900771
    invoke-static {v1, v2, p1}, LX/CVR;->a(LX/15i;ILX/0nX;)V

    .line 1900772
    :cond_9
    invoke-virtual {v1, v0, v5}, LX/15i;->g(II)I

    move-result v2

    .line 1900773
    if-eqz v2, :cond_a

    .line 1900774
    const-string v2, "fields"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900775
    invoke-virtual {v1, v0, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1900776
    :cond_a
    const/16 v2, 0xb

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1900777
    if-eqz v2, :cond_b

    .line 1900778
    const-string v3, "form_field_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900779
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1900780
    :cond_b
    invoke-virtual {v1, v0, v6}, LX/15i;->g(II)I

    move-result v2

    .line 1900781
    if-eqz v2, :cond_c

    .line 1900782
    const-string v2, "form_field_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900783
    invoke-virtual {v1, v0, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1900784
    :cond_c
    const/16 v2, 0xd

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1900785
    if-eqz v2, :cond_d

    .line 1900786
    const-string v3, "heading"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900787
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1900788
    :cond_d
    const/16 v2, 0xe

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1900789
    if-eqz v2, :cond_e

    .line 1900790
    const-string v3, "is_optin_on"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900791
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1900792
    :cond_e
    const/16 v2, 0xf

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1900793
    if-eqz v2, :cond_f

    .line 1900794
    const-string v3, "is_optional"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900795
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1900796
    :cond_f
    const/16 v2, 0x10

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1900797
    if-eqz v2, :cond_10

    .line 1900798
    const-string v3, "is_weekly_view"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900799
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1900800
    :cond_10
    const/16 v2, 0x11

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900801
    if-eqz v2, :cond_11

    .line 1900802
    const-string v3, "items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900803
    invoke-static {v1, v2, p1, p2}, LX/CVo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900804
    :cond_11
    const/16 v2, 0x12

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->a(III)I

    move-result v2

    .line 1900805
    if-eqz v2, :cond_12

    .line 1900806
    const-string v3, "max_selected"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900807
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1900808
    :cond_12
    const/16 v2, 0x13

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1900809
    if-eqz v2, :cond_13

    .line 1900810
    const-string v3, "optin_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900811
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1900812
    :cond_13
    const/16 v2, 0x14

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900813
    if-eqz v2, :cond_14

    .line 1900814
    const-string v3, "prefill_values"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900815
    invoke-static {v1, v2, p1, p2}, LX/CVp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900816
    :cond_14
    const/16 v2, 0x15

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900817
    if-eqz v2, :cond_15

    .line 1900818
    const-string v3, "product_items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900819
    invoke-static {v1, v2, p1, p2}, LX/CVu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900820
    :cond_15
    const/16 v2, 0x16

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900821
    if-eqz v2, :cond_16

    .line 1900822
    const-string v3, "product_selection_description"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900823
    invoke-static {v1, v2, p1, p2}, LX/CVa;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900824
    :cond_16
    invoke-virtual {v1, v0, v7}, LX/15i;->g(II)I

    move-result v2

    .line 1900825
    if-eqz v2, :cond_17

    .line 1900826
    const-string v2, "semantic_tag"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900827
    invoke-virtual {v1, v0, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1900828
    :cond_17
    const/16 v2, 0x18

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1900829
    if-eqz v2, :cond_18

    .line 1900830
    const-string v3, "show_subtotal"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900831
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1900832
    :cond_18
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1900833
    if-eqz v2, :cond_19

    .line 1900834
    const-string v2, "style"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900835
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1900836
    :cond_19
    const/16 v2, 0x1a

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900837
    if-eqz v2, :cond_1a

    .line 1900838
    const-string v3, "time_end"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900839
    invoke-static {v1, v2, p1}, LX/CVH;->a(LX/15i;ILX/0nX;)V

    .line 1900840
    :cond_1a
    const/16 v2, 0x1b

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900841
    if-eqz v2, :cond_1b

    .line 1900842
    const-string v3, "time_selected"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900843
    invoke-static {v1, v2, p1}, LX/CVI;->a(LX/15i;ILX/0nX;)V

    .line 1900844
    :cond_1b
    const/16 v2, 0x1c

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900845
    if-eqz v2, :cond_1c

    .line 1900846
    const-string v3, "time_slot_section"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900847
    invoke-static {v1, v2, p1, p2}, LX/CVx;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1900848
    :cond_1c
    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900849
    if-eqz v2, :cond_1d

    .line 1900850
    const-string v3, "time_start"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900851
    invoke-static {v1, v2, p1}, LX/CVJ;->a(LX/15i;ILX/0nX;)V

    .line 1900852
    :cond_1d
    const/16 v2, 0x1e

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1900853
    if-eqz v2, :cond_1e

    .line 1900854
    const-string v3, "tip"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900855
    invoke-static {v1, v2, p1}, LX/CVb;->a(LX/15i;ILX/0nX;)V

    .line 1900856
    :cond_1e
    const/16 v2, 0x1f

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1900857
    if-eqz v2, :cond_1f

    .line 1900858
    const-string v3, "transactional_text"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1900859
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1900860
    :cond_1f
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1900861
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1900728
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$Serializer;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
