.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1901849
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1901850
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1901851
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1901852
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1901853
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1901854
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 1901855
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1901856
    :goto_0
    move v1, v2

    .line 1901857
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1901858
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1901859
    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel;-><init>()V

    .line 1901860
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1901861
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1901862
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1901863
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1901864
    :cond_0
    return-object v1

    .line 1901865
    :cond_1
    const-string p0, "is_optional"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1901866
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v3

    .line 1901867
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_9

    .line 1901868
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1901869
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1901870
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1901871
    const-string p0, "additional_fees"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1901872
    invoke-static {p1, v0}, LX/CW7;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 1901873
    :cond_3
    const-string p0, "event_listeners"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1901874
    invoke-static {p1, v0}, LX/CVU;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 1901875
    :cond_4
    const-string p0, "fee"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1901876
    invoke-static {p1, v0}, LX/CVR;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 1901877
    :cond_5
    const-string p0, "product_items"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1901878
    invoke-static {p1, v0}, LX/CVu;->a(LX/15w;LX/186;)I

    move-result v7

    goto :goto_1

    .line 1901879
    :cond_6
    const-string p0, "show_subtotal"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_7

    .line 1901880
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v6, v1

    move v1, v3

    goto :goto_1

    .line 1901881
    :cond_7
    const-string p0, "tip"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 1901882
    invoke-static {p1, v0}, LX/CVb;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 1901883
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1901884
    :cond_9
    const/4 v12, 0x7

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1901885
    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1901886
    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1901887
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1901888
    if-eqz v4, :cond_a

    .line 1901889
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 1901890
    :cond_a
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1901891
    if-eqz v1, :cond_b

    .line 1901892
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v6}, LX/186;->a(IZ)V

    .line 1901893
    :cond_b
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1901894
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
