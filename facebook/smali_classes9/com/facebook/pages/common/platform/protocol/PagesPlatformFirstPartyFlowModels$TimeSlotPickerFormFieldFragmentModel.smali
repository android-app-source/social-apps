.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/CUr;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x517d689d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Z

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1902180
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1902179
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1902177
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1902178
    return-void
.end method

.method private a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1902175
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->f:Ljava/util/List;

    .line 1902176
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1902173
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 1902174
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1902171
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->h:Ljava/lang/String;

    .line 1902172
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method private l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeSlotSection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1902169
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1902170
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->j:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1902135
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1902136
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1902137
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1902138
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1902139
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x4ac0d5b4

    invoke-static {v4, v3, v5}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1902140
    const/4 v4, 0x6

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1902141
    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->e:Z

    invoke-virtual {p1, v4, v5}, LX/186;->a(IZ)V

    .line 1902142
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1902143
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1902144
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1902145
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1902146
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1902147
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1902148
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1902159
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1902160
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1902161
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4ac0d5b4

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1902162
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1902163
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;

    .line 1902164
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->j:I

    .line 1902165
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1902166
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1902167
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1902168
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1902154
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1902155
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->e:Z

    .line 1902156
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->i:Z

    .line 1902157
    const/4 v0, 0x5

    const v1, -0x4ac0d5b4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;->j:I

    .line 1902158
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1902151
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$TimeSlotPickerFormFieldFragmentModel;-><init>()V

    .line 1902152
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1902153
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1902150
    const v0, -0x43939ba4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1902149
    const v0, -0x5a0ea063

    return v0
.end method
