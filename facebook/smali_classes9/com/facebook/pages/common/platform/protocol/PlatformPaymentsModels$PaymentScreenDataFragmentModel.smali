.class public final Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x19d2c07a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1907467
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1907466
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1907464
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1907465
    return-void
.end method

.method private a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdditionalInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1907462
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1907463
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907460
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->f:Ljava/lang/String;

    .line 1907461
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOrderInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907458
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->j:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->j:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    .line 1907459
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->j:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    return-object v0
.end method

.method private l()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907456
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->k:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->k:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    .line 1907457
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->k:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907454
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->l:Ljava/lang/String;

    .line 1907455
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method private n()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907398
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    .line 1907399
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->m:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    return-object v0
.end method

.method private o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907452
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->n:Ljava/lang/String;

    .line 1907453
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->n:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    .line 1907431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1907432
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x2a6a78e6

    invoke-static {v1, v0, v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1907433
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1907434
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->k()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1907435
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->l()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1907436
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1907437
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenType;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 1907438
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1907439
    const/16 v7, 0xa

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 1907440
    const/4 v7, 0x0

    invoke-virtual {p1, v7, v0}, LX/186;->b(II)V

    .line 1907441
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1907442
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1907443
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1907444
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1907445
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1907446
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1907447
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1907448
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1907449
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1907450
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1907451
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1907411
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1907412
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1907413
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2a6a78e6

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1907414
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1907415
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;

    .line 1907416
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->e:I

    move-object v1, v0

    .line 1907417
    :cond_0
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->k()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1907418
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->k()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    .line 1907419
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->k()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1907420
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;

    .line 1907421
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->j:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    .line 1907422
    :cond_1
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->l()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1907423
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->l()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    .line 1907424
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->l()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1907425
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;

    .line 1907426
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->k:Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    .line 1907427
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1907428
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1907429
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    move-object p0, v1

    .line 1907430
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1907405
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1907406
    const/4 v0, 0x0

    const v1, 0x2a6a78e6

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->e:I

    .line 1907407
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->g:Z

    .line 1907408
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->h:Z

    .line 1907409
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;->i:Z

    .line 1907410
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1907402
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;-><init>()V

    .line 1907403
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1907404
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1907401
    const v0, 0x2b047f9b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1907400
    const v0, -0x70eff344

    return v0
.end method
