.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1897673
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1897674
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1897693
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1897676
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1897677
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x1

    .line 1897678
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1897679
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1897680
    if-eqz v2, :cond_0

    .line 1897681
    const-string v3, "container"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1897682
    invoke-static {v1, v2, p1, p2}, LX/CVF;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1897683
    :cond_0
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1897684
    if-eqz v2, :cond_1

    .line 1897685
    const-string v2, "element_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1897686
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1897687
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1897688
    if-eqz v2, :cond_2

    .line 1897689
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1897690
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1897691
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1897692
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1897675
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel$Serializer;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ContainerElementFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
