.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1899484
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1899485
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1899486
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1899487
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1899488
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1899489
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1899490
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1899491
    if-eqz v2, :cond_3

    .line 1899492
    const-string v3, "items"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1899493
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1899494
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v4

    if-ge v3, v4, :cond_2

    .line 1899495
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result v4

    .line 1899496
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1899497
    const/4 p0, 0x0

    invoke-virtual {v1, v4, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1899498
    if-eqz p0, :cond_0

    .line 1899499
    const-string v0, "screen_element"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1899500
    invoke-static {v1, p0, p1, p2}, LX/CVr;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1899501
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v4, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1899502
    if-eqz p0, :cond_1

    .line 1899503
    const-string v0, "target_screen_element_id"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1899504
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1899505
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1899506
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1899507
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1899508
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1899509
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1899510
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel$Serializer;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
