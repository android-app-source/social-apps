.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/CUy;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x484e00f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1899933
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1899932
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1899930
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1899931
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1899927
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1899928
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1899929
    return-void
.end method

.method public static a(LX/CUy;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1899890
    if-nez p0, :cond_0

    .line 1899891
    const/4 p0, 0x0

    .line 1899892
    :goto_0
    return-object p0

    .line 1899893
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    if-eqz v0, :cond_1

    .line 1899894
    check-cast p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    goto :goto_0

    .line 1899895
    :cond_1
    new-instance v3, LX/CV2;

    invoke-direct {v3}, LX/CV2;-><init>()V

    .line 1899896
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1899897
    :goto_1
    invoke-interface {p0}, LX/CUy;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1899898
    invoke-interface {p0}, LX/CUy;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1899899
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1899900
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CV2;->a:LX/0Px;

    .line 1899901
    invoke-interface {p0}, LX/CUy;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v0

    iput-object v0, v3, LX/CV2;->b:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    .line 1899902
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1899903
    :goto_2
    invoke-interface {p0}, LX/CUy;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1899904
    invoke-interface {p0}, LX/CUy;->c()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1899905
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1899906
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CV2;->c:LX/0Px;

    .line 1899907
    invoke-interface {p0}, LX/CUy;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CV2;->d:Ljava/lang/String;

    .line 1899908
    const/4 v9, 0x1

    const/4 v13, 0x0

    const/4 v7, 0x0

    .line 1899909
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 1899910
    iget-object v6, v3, LX/CV2;->a:LX/0Px;

    invoke-static {v5, v6}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v6

    .line 1899911
    iget-object v8, v3, LX/CV2;->b:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    invoke-virtual {v5, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1899912
    iget-object v10, v3, LX/CV2;->c:LX/0Px;

    invoke-static {v5, v10}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v10

    .line 1899913
    iget-object v11, v3, LX/CV2;->d:Ljava/lang/String;

    invoke-virtual {v5, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 1899914
    const/4 v12, 0x4

    invoke-virtual {v5, v12}, LX/186;->c(I)V

    .line 1899915
    invoke-virtual {v5, v13, v6}, LX/186;->b(II)V

    .line 1899916
    invoke-virtual {v5, v9, v8}, LX/186;->b(II)V

    .line 1899917
    const/4 v6, 0x2

    invoke-virtual {v5, v6, v10}, LX/186;->b(II)V

    .line 1899918
    const/4 v6, 0x3

    invoke-virtual {v5, v6, v11}, LX/186;->b(II)V

    .line 1899919
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 1899920
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 1899921
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 1899922
    invoke-virtual {v6, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1899923
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1899924
    new-instance v6, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-direct {v6, v5}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;-><init>(LX/15i;)V

    .line 1899925
    move-object p0, v6

    .line 1899926
    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1899878
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1899879
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1899880
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1899881
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->c()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1899882
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1899883
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1899884
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1899885
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1899886
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1899887
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1899888
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1899889
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899934
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->e:Ljava/util/List;

    .line 1899935
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1899865
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1899866
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1899867
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1899868
    if-eqz v1, :cond_0

    .line 1899869
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1899870
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->e:Ljava/util/List;

    .line 1899871
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1899872
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1899873
    if-eqz v1, :cond_1

    .line 1899874
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1899875
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->g:Ljava/util/List;

    .line 1899876
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1899877
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899863
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    .line 1899864
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1899860
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;-><init>()V

    .line 1899861
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1899862
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899858
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->g:Ljava/util/List;

    .line 1899859
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899856
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->h:Ljava/lang/String;

    .line 1899857
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1899854
    const v0, -0x6c1f749f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1899855
    const v0, 0x7d407980

    return v0
.end method
