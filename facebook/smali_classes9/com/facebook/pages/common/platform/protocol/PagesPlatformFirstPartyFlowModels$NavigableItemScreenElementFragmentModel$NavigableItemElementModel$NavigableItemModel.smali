.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/CUt;
.implements LX/CUo;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x51b67de
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel$Serializer;
.end annotation


# instance fields
.field private A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private D:Z

.field private E:Z

.field private F:Z

.field private G:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:I

.field private I:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private K:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private L:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private M:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private O:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private P:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private R:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private S:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private T:Z

.field private U:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private V:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private W:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private X:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Y:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private Z:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private aa:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Z

.field private k:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private o:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Z

.field private s:Lcom/facebook/graphql/enums/GraphQLScreenElementType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private w:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1899316
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1899300
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1899298
    const/16 v0, 0x32

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1899299
    return-void
.end method

.method private ab()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899296
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->p:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->p:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    .line 1899297
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->p:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    return-object v0
.end method

.method private ac()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899294
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    .line 1899295
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    return-object v0
.end method

.method private ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899292
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    const/16 v1, 0x22

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1899293
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    return-object v0
.end method

.method private ae()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProduct"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899290
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    const/16 v1, 0x24

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    .line 1899291
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    return-object v0
.end method

.method private af()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899288
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    const/16 v1, 0x26

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1899289
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    return-object v0
.end method

.method private ag()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899271
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    const/16 v1, 0x2d

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    .line 1899272
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    return-object v0
.end method


# virtual methods
.method public final synthetic A()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899287
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ag()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v0

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899285
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ab:Ljava/lang/String;

    const/16 v1, 0x31

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ab:Ljava/lang/String;

    .line 1899286
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ab:Ljava/lang/String;

    return-object v0
.end method

.method public final C()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAdditionalFees"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899283
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->f:Ljava/util/List;

    .line 1899284
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final D()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddressInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899281
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899282
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final E()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAddressLabel"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899279
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899280
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final F()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAlertBarContent"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899277
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899278
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->i:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final G()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAvailableTimeSlots"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899275
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->k:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x6

    const v4, 0x6565cba5

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->k:LX/3Sb;

    .line 1899276
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->k:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final H()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAvailableTimes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899273
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->l:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x7

    const v4, 0x6cd307e8

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->l:LX/3Sb;

    .line 1899274
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->l:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final I()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getConfirmationItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 1899317
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899318
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->n:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final J()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getContactInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899303
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899304
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->o:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final K()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEmbeddedItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899329
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899330
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->t:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final L()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getError"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899327
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899328
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->u:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final M()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventListeners"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899325
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->v:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x11

    const v4, -0x279ab4e9

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->v:LX/3Sb;

    .line 1899326
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->v:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final N()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getFee"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x2

    .line 1899323
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899324
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->w:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final O()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getHeadingItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899321
    const/4 v0, 0x2

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899322
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->B:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final P()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899319
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G:Ljava/util/List;

    const/16 v1, 0x1c

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$ItemsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G:Ljava/util/List;

    .line 1899320
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final Q()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPagesPlatformDate"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899331
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899332
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final R()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPagesPlatformImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899314
    const/4 v0, 0x4

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899315
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final S()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPrefillValues"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899312
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x23

    const v4, -0x434f4abe

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N:LX/3Sb;

    .line 1899313
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final synthetic T()LX/CUx;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProduct"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899311
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ae()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final U()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProductItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899309
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/16 v3, 0x25

    const v4, -0x696e62b7

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P:LX/3Sb;

    .line 1899310
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final V()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getSeparator"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899307
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899308
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->S:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final W()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTextItem"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899305
    const/4 v0, 0x5

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899306
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final X()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeEnd"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899301
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899302
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final Y()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeSlotSection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899236
    const/4 v0, 0x5

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899237
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final Z()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTimeStart"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899234
    const/4 v0, 0x5

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899235
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/186;)I
    .locals 47

    .prologue
    .line 1899137
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1899138
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1899139
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->C()LX/0Px;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1899140
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    const v7, -0x21aeca47

    invoke-static {v6, v5, v7}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1899141
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E()LX/1vs;

    move-result-object v6

    iget-object v7, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    const v8, -0x67d54b32

    invoke-static {v7, v6, v8}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1899142
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0x3ed3a02

    invoke-static {v8, v7, v9}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1899143
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G()LX/2uF;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-static {v8, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v8

    .line 1899144
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->H()LX/2uF;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-static {v9, v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v9

    .line 1899145
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->d()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1899146
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I()LX/1vs;

    move-result-object v11

    iget-object v12, v11, LX/1vs;->a:LX/15i;

    iget v11, v11, LX/1vs;->b:I

    const v13, -0x65666c3a

    invoke-static {v12, v11, v13}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-static {v0, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 1899147
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J()LX/1vs;

    move-result-object v12

    iget-object v13, v12, LX/1vs;->a:LX/15i;

    iget v12, v12, LX/1vs;->b:I

    const v14, -0x67ea05b7

    invoke-static {v13, v12, v14}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 1899148
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ab()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-static {v0, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 1899149
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ac()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    move-result-object v14

    move-object/from16 v0, p1

    invoke-static {v0, v14}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 1899150
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v15

    .line 1899151
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K()LX/1vs;

    move-result-object v16

    move-object/from16 v0, v16

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    iget v0, v0, LX/1vs;->b:I

    move/from16 v16, v0

    const v18, 0x4d6717c

    move-object/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 1899152
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L()LX/1vs;

    move-result-object v17

    move-object/from16 v0, v17

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, v17

    iget v0, v0, LX/1vs;->b:I

    move/from16 v17, v0

    const v19, 0x2a5a73a7

    move-object/from16 v0, v18

    move/from16 v1, v17

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 1899153
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M()LX/2uF;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v18

    .line 1899154
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N()LX/1vs;

    move-result-object v19

    move-object/from16 v0, v19

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, LX/1vs;->b:I

    move/from16 v19, v0

    const v21, 0x26f30b65

    move-object/from16 v0, v20

    move/from16 v1, v19

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v19

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v19

    .line 1899155
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->k()LX/0Px;

    move-result-object v20

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v20

    .line 1899156
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->l()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    .line 1899157
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-result-object v22

    move-object/from16 v0, p1

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v22

    .line 1899158
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->n()Ljava/lang/String;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v23

    .line 1899159
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O()LX/1vs;

    move-result-object v24

    move-object/from16 v0, v24

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v25, v0

    move-object/from16 v0, v24

    iget v0, v0, LX/1vs;->b:I

    move/from16 v24, v0

    const v26, 0x4e3af34f    # 7.8412691E8f

    move-object/from16 v0, v25

    move/from16 v1, v24

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v24

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v24

    .line 1899160
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->o()Ljava/lang/String;

    move-result-object v25

    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v25

    .line 1899161
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P()LX/0Px;

    move-result-object v26

    move-object/from16 v0, p1

    move-object/from16 v1, v26

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v26

    .line 1899162
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->t()Ljava/lang/String;

    move-result-object v27

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v27

    .line 1899163
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->u()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v28

    .line 1899164
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q()LX/1vs;

    move-result-object v29

    move-object/from16 v0, v29

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v30, v0

    move-object/from16 v0, v29

    iget v0, v0, LX/1vs;->b:I

    move/from16 v29, v0

    const v31, -0x2897e73b

    move-object/from16 v0, v30

    move/from16 v1, v29

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v29

    move-object/from16 v0, p1

    move-object/from16 v1, v29

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v29

    .line 1899165
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R()LX/1vs;

    move-result-object v30

    move-object/from16 v0, v30

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v31, v0

    move-object/from16 v0, v30

    iget v0, v0, LX/1vs;->b:I

    move/from16 v30, v0

    const v32, 0x2ea003e7

    move-object/from16 v0, v31

    move/from16 v1, v30

    move/from16 v2, v32

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v30

    move-object/from16 v0, p1

    move-object/from16 v1, v30

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v30

    .line 1899166
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v31

    move-object/from16 v0, p1

    move-object/from16 v1, v31

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v31

    .line 1899167
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->S()LX/2uF;

    move-result-object v32

    move-object/from16 v0, v32

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v32

    .line 1899168
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ae()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    move-result-object v33

    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v33

    .line 1899169
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->U()LX/2uF;

    move-result-object v34

    move-object/from16 v0, v34

    move-object/from16 v1, p1

    invoke-static {v0, v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v34

    .line 1899170
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->af()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v35

    move-object/from16 v0, p1

    move-object/from16 v1, v35

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v35

    .line 1899171
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->x()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    move-result-object v36

    move-object/from16 v0, p1

    move-object/from16 v1, v36

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v36

    .line 1899172
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V()LX/1vs;

    move-result-object v37

    move-object/from16 v0, v37

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v38, v0

    move-object/from16 v0, v37

    iget v0, v0, LX/1vs;->b:I

    move/from16 v37, v0

    const v39, 0x593eb926

    move-object/from16 v0, v38

    move/from16 v1, v37

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v37

    move-object/from16 v0, p1

    move-object/from16 v1, v37

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v37

    .line 1899173
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    move-result-object v38

    move-object/from16 v0, p1

    move-object/from16 v1, v38

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v38

    .line 1899174
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W()LX/1vs;

    move-result-object v39

    move-object/from16 v0, v39

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v40, v0

    move-object/from16 v0, v39

    iget v0, v0, LX/1vs;->b:I

    move/from16 v39, v0

    const v41, -0x5f4401ec

    move-object/from16 v0, v40

    move/from16 v1, v39

    move/from16 v2, v41

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v39

    move-object/from16 v0, p1

    move-object/from16 v1, v39

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v39

    .line 1899175
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X()LX/1vs;

    move-result-object v40

    move-object/from16 v0, v40

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v41, v0

    move-object/from16 v0, v40

    iget v0, v0, LX/1vs;->b:I

    move/from16 v40, v0

    const v42, -0x7969ad84

    move-object/from16 v0, v41

    move/from16 v1, v40

    move/from16 v2, v42

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v40

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v40

    .line 1899176
    invoke-direct/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ag()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v41

    move-object/from16 v0, p1

    move-object/from16 v1, v41

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v41

    .line 1899177
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y()LX/1vs;

    move-result-object v42

    move-object/from16 v0, v42

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v43, v0

    move-object/from16 v0, v42

    iget v0, v0, LX/1vs;->b:I

    move/from16 v42, v0

    const v44, -0x4ac0d5b4

    move-object/from16 v0, v43

    move/from16 v1, v42

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v42

    move-object/from16 v0, p1

    move-object/from16 v1, v42

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v42

    .line 1899178
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z()LX/1vs;

    move-result-object v43

    move-object/from16 v0, v43

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v44, v0

    move-object/from16 v0, v43

    iget v0, v0, LX/1vs;->b:I

    move/from16 v43, v0

    const v45, 0x1b46b811

    move-object/from16 v0, v44

    move/from16 v1, v43

    move/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v43

    move-object/from16 v0, p1

    move-object/from16 v1, v43

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v43

    .line 1899179
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa()LX/1vs;

    move-result-object v44

    move-object/from16 v0, v44

    iget-object v0, v0, LX/1vs;->a:LX/15i;

    move-object/from16 v45, v0

    move-object/from16 v0, v44

    iget v0, v0, LX/1vs;->b:I

    move/from16 v44, v0

    const v46, -0x347c35b

    move-object/from16 v0, v45

    move/from16 v1, v44

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v44

    move-object/from16 v0, p1

    move-object/from16 v1, v44

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v44

    .line 1899180
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->B()Ljava/lang/String;

    move-result-object v45

    move-object/from16 v0, p1

    move-object/from16 v1, v45

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v45

    .line 1899181
    const/16 v46, 0x32

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 1899182
    const/16 v46, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 1899183
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->b(II)V

    .line 1899184
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v5}, LX/186;->b(II)V

    .line 1899185
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1899186
    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 1899187
    const/4 v3, 0x5

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1899188
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 1899189
    const/4 v3, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 1899190
    const/16 v3, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->b(II)V

    .line 1899191
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, LX/186;->b(II)V

    .line 1899192
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v12}, LX/186;->b(II)V

    .line 1899193
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 1899194
    const/16 v3, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 1899195
    const/16 v3, 0xd

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->r:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1899196
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v15}, LX/186;->b(II)V

    .line 1899197
    const/16 v3, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899198
    const/16 v3, 0x10

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899199
    const/16 v3, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899200
    const/16 v3, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899201
    const/16 v3, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899202
    const/16 v3, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899203
    const/16 v3, 0x15

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899204
    const/16 v3, 0x16

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899205
    const/16 v3, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899206
    const/16 v3, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899207
    const/16 v3, 0x19

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1899208
    const/16 v3, 0x1a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1899209
    const/16 v3, 0x1b

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1899210
    const/16 v3, 0x1c

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899211
    const/16 v3, 0x1d

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->H:I

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v5}, LX/186;->a(III)V

    .line 1899212
    const/16 v3, 0x1e

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899213
    const/16 v3, 0x1f

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899214
    const/16 v3, 0x20

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899215
    const/16 v3, 0x21

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899216
    const/16 v3, 0x22

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899217
    const/16 v3, 0x23

    move-object/from16 v0, p1

    move/from16 v1, v32

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899218
    const/16 v3, 0x24

    move-object/from16 v0, p1

    move/from16 v1, v33

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899219
    const/16 v3, 0x25

    move-object/from16 v0, p1

    move/from16 v1, v34

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899220
    const/16 v3, 0x26

    move-object/from16 v0, p1

    move/from16 v1, v35

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899221
    const/16 v3, 0x27

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899222
    const/16 v3, 0x28

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899223
    const/16 v3, 0x29

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->T:Z

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4}, LX/186;->a(IZ)V

    .line 1899224
    const/16 v3, 0x2a

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899225
    const/16 v3, 0x2b

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899226
    const/16 v3, 0x2c

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899227
    const/16 v3, 0x2d

    move-object/from16 v0, p1

    move/from16 v1, v41

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899228
    const/16 v3, 0x2e

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899229
    const/16 v3, 0x2f

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899230
    const/16 v3, 0x30

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899231
    const/16 v3, 0x31

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1899232
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1899233
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    return v3
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1898966
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1898967
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->C()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1e

    .line 1898968
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->C()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1898969
    if-eqz v1, :cond_1e

    .line 1898970
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1898971
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1898972
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1898973
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x21aeca47

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1898974
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1898975
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1898976
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->g:I

    move-object v1, v0

    .line 1898977
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1898978
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x67d54b32

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1898979
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1898980
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1898981
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->h:I

    move-object v1, v0

    .line 1898982
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1898983
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x3ed3a02

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1898984
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1898985
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1898986
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->i:I

    move-object v1, v0

    .line 1898987
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1898988
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1898989
    if-eqz v2, :cond_3

    .line 1898990
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1898991
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->k:LX/3Sb;

    move-object v1, v0

    .line 1898992
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->H()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1898993
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->H()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1898994
    if-eqz v2, :cond_4

    .line 1898995
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1898996
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->l:LX/3Sb;

    move-object v1, v0

    .line 1898997
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 1898998
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x65666c3a

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 1898999
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1899000
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899001
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->n:I

    move-object v1, v0

    .line 1899002
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 1899003
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x67ea05b7

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 1899004
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1899005
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899006
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->o:I

    move-object v1, v0

    .line 1899007
    :cond_6
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ab()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1899008
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ab()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    .line 1899009
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ab()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v2

    if-eq v2, v0, :cond_7

    .line 1899010
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899011
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->p:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    .line 1899012
    :cond_7
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ac()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1899013
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ac()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    .line 1899014
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ac()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    move-result-object v2

    if-eq v2, v0, :cond_8

    .line 1899015
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899016
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    .line 1899017
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_9

    .line 1899018
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4d6717c

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 1899019
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1899020
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899021
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->t:I

    move-object v1, v0

    .line 1899022
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_a

    .line 1899023
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2a5a73a7

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_6
    monitor-exit v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    .line 1899024
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1899025
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899026
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->u:I

    move-object v1, v0

    .line 1899027
    :cond_a
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_b

    .line 1899028
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1899029
    if-eqz v2, :cond_b

    .line 1899030
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899031
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->v:LX/3Sb;

    move-object v1, v0

    .line 1899032
    :cond_b
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_c

    .line 1899033
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x26f30b65

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_7
    monitor-exit v4
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    .line 1899034
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1899035
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899036
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->w:I

    move-object v1, v0

    .line 1899037
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_d

    .line 1899038
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4e3af34f    # 7.8412691E8f

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_8
    monitor-exit v4
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 1899039
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_d

    .line 1899040
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899041
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->B:I

    move-object v1, v0

    .line 1899042
    :cond_d
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_e

    .line 1899043
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1899044
    if-eqz v2, :cond_e

    .line 1899045
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899046
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->G:Ljava/util/List;

    move-object v1, v0

    .line 1899047
    :cond_e
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_f

    .line 1899048
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x2897e73b

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_9
    monitor-exit v4
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    .line 1899049
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1899050
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899051
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K:I

    move-object v1, v0

    .line 1899052
    :cond_f
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_10

    .line 1899053
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2ea003e7

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_a
    monitor-exit v4
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    .line 1899054
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_10

    .line 1899055
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899056
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L:I

    move-object v1, v0

    .line 1899057
    :cond_10
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1899058
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1899059
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_11

    .line 1899060
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899061
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->M:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1899062
    :cond_11
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->S()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_12

    .line 1899063
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->S()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1899064
    if-eqz v2, :cond_12

    .line 1899065
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899066
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->N:LX/3Sb;

    move-object v1, v0

    .line 1899067
    :cond_12
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ae()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_13

    .line 1899068
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ae()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    .line 1899069
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ae()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_13

    .line 1899070
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899071
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->O:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformProductFragmentModel;

    .line 1899072
    :cond_13
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->U()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_14

    .line 1899073
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->U()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v2

    .line 1899074
    if-eqz v2, :cond_14

    .line 1899075
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899076
    invoke-virtual {v2}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->P:LX/3Sb;

    move-object v1, v0

    .line 1899077
    :cond_14
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->af()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_15

    .line 1899078
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->af()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1899079
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->af()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_15

    .line 1899080
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899081
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Q:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    .line 1899082
    :cond_15
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_16

    .line 1899083
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x593eb926

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_b
    monitor-exit v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_b

    .line 1899084
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_16

    .line 1899085
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899086
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->S:I

    move-object v1, v0

    .line 1899087
    :cond_16
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_17

    .line 1899088
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5f4401ec

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_c
    monitor-exit v4
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_c

    .line 1899089
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_17

    .line 1899090
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899091
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V:I

    move-object v1, v0

    .line 1899092
    :cond_17
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_18

    .line 1899093
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x7969ad84

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_d
    monitor-exit v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_d

    .line 1899094
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_18

    .line 1899095
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899096
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W:I

    move-object v1, v0

    .line 1899097
    :cond_18
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ag()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v0

    if-eqz v0, :cond_19

    .line 1899098
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ag()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    .line 1899099
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ag()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    move-result-object v2

    if-eq v2, v0, :cond_19

    .line 1899100
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899101
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->X:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$TimeSelectedModel;

    .line 1899102
    :cond_19
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1a

    .line 1899103
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x4ac0d5b4

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_e
    monitor-exit v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_e

    .line 1899104
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1a

    .line 1899105
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899106
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y:I

    move-object v1, v0

    .line 1899107
    :cond_1a
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1b

    .line 1899108
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x1b46b811

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_f
    monitor-exit v4
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_f

    .line 1899109
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 1899110
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899111
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z:I

    move-object v1, v0

    .line 1899112
    :cond_1b
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1c

    .line 1899113
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x347c35b

    invoke-static {v2, v0, v3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_10
    monitor-exit v4
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_10

    .line 1899114
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1c

    .line 1899115
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    .line 1899116
    iput v3, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa:I

    move-object v1, v0

    .line 1899117
    :cond_1c
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1899118
    if-nez v1, :cond_1d

    :goto_1
    return-object p0

    .line 1899119
    :catchall_0
    move-exception v0

    :try_start_11
    monitor-exit v4
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    throw v0

    .line 1899120
    :catchall_1
    move-exception v0

    :try_start_12
    monitor-exit v4
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_1

    throw v0

    .line 1899121
    :catchall_2
    move-exception v0

    :try_start_13
    monitor-exit v4
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_2

    throw v0

    .line 1899122
    :catchall_3
    move-exception v0

    :try_start_14
    monitor-exit v4
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    throw v0

    .line 1899123
    :catchall_4
    move-exception v0

    :try_start_15
    monitor-exit v4
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    throw v0

    .line 1899124
    :catchall_5
    move-exception v0

    :try_start_16
    monitor-exit v4
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_5

    throw v0

    .line 1899125
    :catchall_6
    move-exception v0

    :try_start_17
    monitor-exit v4
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_6

    throw v0

    .line 1899126
    :catchall_7
    move-exception v0

    :try_start_18
    monitor-exit v4
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_7

    throw v0

    .line 1899127
    :catchall_8
    move-exception v0

    :try_start_19
    monitor-exit v4
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_8

    throw v0

    .line 1899128
    :catchall_9
    move-exception v0

    :try_start_1a
    monitor-exit v4
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_9

    throw v0

    .line 1899129
    :catchall_a
    move-exception v0

    :try_start_1b
    monitor-exit v4
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_a

    throw v0

    .line 1899130
    :catchall_b
    move-exception v0

    :try_start_1c
    monitor-exit v4
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_b

    throw v0

    .line 1899131
    :catchall_c
    move-exception v0

    :try_start_1d
    monitor-exit v4
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_c

    throw v0

    .line 1899132
    :catchall_d
    move-exception v0

    :try_start_1e
    monitor-exit v4
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_d

    throw v0

    .line 1899133
    :catchall_e
    move-exception v0

    :try_start_1f
    monitor-exit v4
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_e

    throw v0

    .line 1899134
    :catchall_f
    move-exception v0

    :try_start_20
    monitor-exit v4
    :try_end_20
    .catchall {:try_start_20 .. :try_end_20} :catchall_f

    throw v0

    .line 1899135
    :catchall_10
    move-exception v0

    :try_start_21
    monitor-exit v4
    :try_end_21
    .catchall {:try_start_21 .. :try_end_21} :catchall_10

    throw v0

    :cond_1d
    move-object p0, v1

    .line 1899136
    goto :goto_1

    :cond_1e
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1898965
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1898939
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1898940
    const/4 v0, 0x2

    const v1, -0x21aeca47

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->g:I

    .line 1898941
    const/4 v0, 0x3

    const v1, -0x67d54b32

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->h:I

    .line 1898942
    const/4 v0, 0x4

    const v1, -0x3ed3a02

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->i:I

    .line 1898943
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j:Z

    .line 1898944
    const/16 v0, 0x9

    const v1, -0x65666c3a

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->n:I

    .line 1898945
    const/16 v0, 0xa

    const v1, -0x67ea05b7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->o:I

    .line 1898946
    const/16 v0, 0xd

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->r:Z

    .line 1898947
    const/16 v0, 0xf

    const v1, 0x4d6717c

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->t:I

    .line 1898948
    const/16 v0, 0x10

    const v1, 0x2a5a73a7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->u:I

    .line 1898949
    const/16 v0, 0x12

    const v1, 0x26f30b65

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->w:I

    .line 1898950
    const/16 v0, 0x17

    const v1, 0x4e3af34f    # 7.8412691E8f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->B:I

    .line 1898951
    const/16 v0, 0x19

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D:Z

    .line 1898952
    const/16 v0, 0x1a

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E:Z

    .line 1898953
    const/16 v0, 0x1b

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F:Z

    .line 1898954
    const/16 v0, 0x1d

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->H:I

    .line 1898955
    const/16 v0, 0x20

    const v1, -0x2897e73b

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->K:I

    .line 1898956
    const/16 v0, 0x21

    const v1, 0x2ea003e7

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->L:I

    .line 1898957
    const/16 v0, 0x28

    const v1, 0x593eb926

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->S:I

    .line 1898958
    const/16 v0, 0x29

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->T:Z

    .line 1898959
    const/16 v0, 0x2b

    const v1, -0x5f4401ec

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->V:I

    .line 1898960
    const/16 v0, 0x2c

    const v1, -0x7969ad84

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->W:I

    .line 1898961
    const/16 v0, 0x2e

    const v1, -0x4ac0d5b4

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Y:I

    .line 1898962
    const/16 v0, 0x2f

    const v1, 0x1b46b811

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->Z:I

    .line 1898963
    const/16 v0, 0x30

    const v1, -0x347c35b

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa:I

    .line 1898964
    return-void
.end method

.method public final aa()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTip"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1898937
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1898938
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->aa:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899240
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1899241
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1899242
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1898934
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;-><init>()V

    .line 1898935
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1898936
    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 1898932
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1898933
    iget-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->j:Z

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1898930
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->m:Ljava/lang/String;

    .line 1898931
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1898929
    const v0, 0x5d35dea5

    return v0
.end method

.method public final synthetic e()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1898928
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ab()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DefaultValueModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1898927
    const v0, 0x1edc14d0

    return v0
.end method

.method public final j()Lcom/facebook/graphql/enums/GraphQLScreenElementType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1898925
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->s:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    const/16 v1, 0xe

    const-class v2, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->s:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    .line 1898926
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->s:Lcom/facebook/graphql/enums/GraphQLScreenElementType;

    return-object v0
.end method

.method public final jP_()Z
    .locals 2

    .prologue
    .line 1898923
    const/4 v0, 0x1

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1898924
    iget-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->r:Z

    return v0
.end method

.method public final synthetic jQ_()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1898922
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ac()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenElementFormFieldFragmentModel$DescriptionModel;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1899255
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->x:Ljava/util/List;

    const/16 v1, 0x13

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->x:Ljava/util/List;

    .line 1899256
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->x:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899269
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->y:Ljava/lang/String;

    const/16 v1, 0x14

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->y:Ljava/lang/String;

    .line 1899270
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899267
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->z:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    const/16 v1, 0x15

    const-class v2, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->z:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    .line 1899268
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->z:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899265
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->A:Ljava/lang/String;

    const/16 v1, 0x16

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->A:Ljava/lang/String;

    .line 1899266
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899263
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->C:Ljava/lang/String;

    const/16 v1, 0x18

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->C:Ljava/lang/String;

    .line 1899264
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->C:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 1899261
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899262
    iget-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->D:Z

    return v0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 1899259
    const/4 v0, 0x3

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899260
    iget-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->E:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    const/4 v0, 0x3

    .line 1899257
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899258
    iget-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->F:Z

    return v0
.end method

.method public final s()I
    .locals 2

    .prologue
    .line 1899238
    const/4 v0, 0x3

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899239
    iget v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->H:I

    return v0
.end method

.method public final t()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899253
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I:Ljava/lang/String;

    const/16 v1, 0x1e

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I:Ljava/lang/String;

    .line 1899254
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->I:Ljava/lang/String;

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899251
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J:Ljava/lang/String;

    const/16 v1, 0x1f

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J:Ljava/lang/String;

    .line 1899252
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->J:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic v()LX/CUy;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899250
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->ad()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic w()LX/CUy;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899249
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->af()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final x()Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899247
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    const/16 v1, 0x27

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    .line 1899248
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->R:Lcom/facebook/graphql/enums/GraphQLPagesPlatformSemanticTag;

    return-object v0
.end method

.method public final y()Z
    .locals 2

    .prologue
    .line 1899245
    const/4 v0, 0x5

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1899246
    iget-boolean v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->T:Z

    return v0
.end method

.method public final z()Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1899243
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->U:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    const/16 v1, 0x2a

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->U:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    .line 1899244
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$NavigableItemScreenElementFragmentModel$NavigableItemElementModel$NavigableItemModel;->U:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenSelectionStyle;

    return-object v0
.end method
