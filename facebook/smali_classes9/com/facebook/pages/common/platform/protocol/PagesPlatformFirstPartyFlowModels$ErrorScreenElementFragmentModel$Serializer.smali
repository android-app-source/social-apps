.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1898527
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1898528
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1898508
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1898510
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1898511
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1898512
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1898513
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1898514
    if-eqz v2, :cond_0

    .line 1898515
    const-string p0, "call_link"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1898516
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1898517
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1898518
    if-eqz v2, :cond_1

    .line 1898519
    const-string p0, "error"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1898520
    invoke-static {v1, v2, p1, p2}, LX/CVL;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1898521
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1898522
    if-eqz v2, :cond_2

    .line 1898523
    const-string p0, "message_link"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1898524
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1898525
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1898526
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1898509
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel$Serializer;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ErrorScreenElementFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
