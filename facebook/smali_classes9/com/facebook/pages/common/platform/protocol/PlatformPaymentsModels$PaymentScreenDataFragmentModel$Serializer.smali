.class public final Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1907351
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1907352
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1907350
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1907353
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1907354
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x8

    .line 1907355
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1907356
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1907357
    if-eqz v2, :cond_0

    .line 1907358
    const-string v3, "additional_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907359
    invoke-static {v1, v2, p1}, LX/CW8;->a(LX/15i;ILX/0nX;)V

    .line 1907360
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1907361
    if-eqz v2, :cond_1

    .line 1907362
    const-string v3, "component_flow_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907363
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1907364
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1907365
    if-eqz v2, :cond_2

    .line 1907366
    const-string v3, "need_delivery_address"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907367
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1907368
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1907369
    if-eqz v2, :cond_3

    .line 1907370
    const-string v3, "need_phone_number"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907371
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1907372
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1907373
    if-eqz v2, :cond_4

    .line 1907374
    const-string v3, "need_verified_email"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907375
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1907376
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1907377
    if-eqz v2, :cond_5

    .line 1907378
    const-string v3, "order_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907379
    invoke-static {v1, v2, p1, p2}, LX/CW6;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1907380
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1907381
    if-eqz v2, :cond_6

    .line 1907382
    const-string v3, "provider_info"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907383
    invoke-static {v1, v2, p1, p2}, LX/CW5;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1907384
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1907385
    if-eqz v2, :cond_7

    .line 1907386
    const-string v3, "screen_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907387
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1907388
    :cond_7
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1907389
    if-eqz v2, :cond_8

    .line 1907390
    const-string v2, "screen_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907391
    invoke-virtual {v1, v0, p0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1907392
    :cond_8
    const/16 v2, 0x9

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1907393
    if-eqz v2, :cond_9

    .line 1907394
    const-string v3, "title"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1907395
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1907396
    :cond_9
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1907397
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1907349
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel$Serializer;->a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PaymentScreenDataFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
