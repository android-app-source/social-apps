.class public final Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xa7a47e7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1907100
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1907099
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1907097
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1907098
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1907094
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1907095
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1907096
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;
    .locals 12

    .prologue
    .line 1907062
    if-nez p0, :cond_0

    .line 1907063
    const/4 p0, 0x0

    .line 1907064
    :goto_0
    return-object p0

    .line 1907065
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    if-eqz v0, :cond_1

    .line 1907066
    check-cast p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    goto :goto_0

    .line 1907067
    :cond_1
    new-instance v0, LX/CW1;

    invoke-direct {v0}, LX/CW1;-><init>()V

    .line 1907068
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CW1;->a:Ljava/lang/String;

    .line 1907069
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CW1;->b:Ljava/lang/String;

    .line 1907070
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CW1;->c:Ljava/lang/String;

    .line 1907071
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CW1;->d:Ljava/lang/String;

    .line 1907072
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->e()LX/3Ab;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;->a(LX/3Ab;)Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v1

    iput-object v1, v0, LX/CW1;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1907073
    const/4 v6, 0x1

    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 1907074
    new-instance v2, LX/186;

    const/16 v3, 0x80

    invoke-direct {v2, v3}, LX/186;-><init>(I)V

    .line 1907075
    iget-object v3, v0, LX/CW1;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1907076
    iget-object v5, v0, LX/CW1;->b:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1907077
    iget-object v7, v0, LX/CW1;->c:Ljava/lang/String;

    invoke-virtual {v2, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1907078
    iget-object v8, v0, LX/CW1;->d:Ljava/lang/String;

    invoke-virtual {v2, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1907079
    iget-object v9, v0, LX/CW1;->e:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-static {v2, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1907080
    const/4 v10, 0x5

    invoke-virtual {v2, v10}, LX/186;->c(I)V

    .line 1907081
    invoke-virtual {v2, v11, v3}, LX/186;->b(II)V

    .line 1907082
    invoke-virtual {v2, v6, v5}, LX/186;->b(II)V

    .line 1907083
    const/4 v3, 0x2

    invoke-virtual {v2, v3, v7}, LX/186;->b(II)V

    .line 1907084
    const/4 v3, 0x3

    invoke-virtual {v2, v3, v8}, LX/186;->b(II)V

    .line 1907085
    const/4 v3, 0x4

    invoke-virtual {v2, v3, v9}, LX/186;->b(II)V

    .line 1907086
    invoke-virtual {v2}, LX/186;->d()I

    move-result v3

    .line 1907087
    invoke-virtual {v2, v3}, LX/186;->d(I)V

    .line 1907088
    invoke-virtual {v2}, LX/186;->e()[B

    move-result-object v2

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 1907089
    invoke-virtual {v3, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1907090
    new-instance v2, LX/15i;

    move-object v5, v4

    move-object v7, v4

    invoke-direct/range {v2 .. v7}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1907091
    new-instance v3, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    invoke-direct {v3, v2}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;-><init>(LX/15i;)V

    .line 1907092
    move-object p0, v3

    .line 1907093
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907060
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1907061
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1907046
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1907047
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1907048
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1907049
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1907050
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1907051
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1907052
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1907053
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1907054
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1907055
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1907056
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1907057
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1907058
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1907059
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1907101
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1907102
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1907103
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1907104
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1907105
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    .line 1907106
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->i:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 1907107
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1907108
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907044
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->e:Ljava/lang/String;

    .line 1907045
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1907041
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;-><init>()V

    .line 1907042
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1907043
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907039
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->f:Ljava/lang/String;

    .line 1907040
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907037
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->g:Ljava/lang/String;

    .line 1907038
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907035
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->h:Ljava/lang/String;

    .line 1907036
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1907034
    const v0, 0x607d396a

    return v0
.end method

.method public final synthetic e()LX/3Ab;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907033
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformCheckOutProviderInfoFragmentModel;->j()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1907032
    const v0, 0x38f4f95c

    return v0
.end method
