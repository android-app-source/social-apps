.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1897771
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1897772
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1897773
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1897774
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1897775
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1897776
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1897777
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1897778
    if-eqz v2, :cond_0

    .line 1897779
    const-string p0, "available_times"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1897780
    invoke-static {v1, v2, p1, p2}, LX/CVG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1897781
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1897782
    if-eqz v2, :cond_1

    .line 1897783
    const-string p0, "event_listeners"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1897784
    invoke-static {v1, v2, p1, p2}, LX/CVU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1897785
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1897786
    if-eqz v2, :cond_2

    .line 1897787
    const-string p0, "time_end"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1897788
    invoke-static {v1, v2, p1}, LX/CVH;->a(LX/15i;ILX/0nX;)V

    .line 1897789
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1897790
    if-eqz v2, :cond_3

    .line 1897791
    const-string p0, "time_selected"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1897792
    invoke-static {v1, v2, p1}, LX/CVI;->a(LX/15i;ILX/0nX;)V

    .line 1897793
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1897794
    if-eqz v2, :cond_4

    .line 1897795
    const-string p0, "time_start"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1897796
    invoke-static {v1, v2, p1}, LX/CVJ;->a(LX/15i;ILX/0nX;)V

    .line 1897797
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1897798
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1897799
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel$Serializer;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$DatePickerFormFieldFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
