.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/CUy;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x484e00f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1900311
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1900310
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1900308
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1900309
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1900296
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1900297
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1900298
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1900299
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->c()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1900300
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1900301
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1900302
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1900303
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1900304
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1900305
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1900306
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1900307
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1900294
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$EntitiesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->e:Ljava/util/List;

    .line 1900295
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1900312
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1900313
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1900314
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1900315
    if-eqz v1, :cond_0

    .line 1900316
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    .line 1900317
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->e:Ljava/util/List;

    .line 1900318
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->c()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1900319
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->c()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1900320
    if-eqz v1, :cond_1

    .line 1900321
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    .line 1900322
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->g:Ljava/util/List;

    .line 1900323
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1900324
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1900292
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    .line 1900293
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->f:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextSize;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1900289
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;-><init>()V

    .line 1900290
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1900291
    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1900283
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformRichTextFragmentModel$InlineStylesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->g:Ljava/util/List;

    .line 1900284
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1900287
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->h:Ljava/lang/String;

    .line 1900288
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ScreenDisclaimerFragmentModel$CustomizedDisclaimerModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1900286
    const v0, 0x6ba7d652

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1900285
    const v0, 0x7d407980

    return v0
.end method
