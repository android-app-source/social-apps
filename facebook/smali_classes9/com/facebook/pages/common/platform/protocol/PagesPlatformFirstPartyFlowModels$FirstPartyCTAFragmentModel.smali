.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4993e4d1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1898806
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1898782
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1898804
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1898805
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1898798
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1898799
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1898800
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1898801
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1898802
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1898803
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1898790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1898791
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1898792
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    .line 1898793
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1898794
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;

    .line 1898795
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->e:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    .line 1898796
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1898797
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getScreenUnion"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1898788
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->e:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->e:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    .line 1898789
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;->e:Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel$ScreenUnionModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1898785
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$FirstPartyCTAFragmentModel;-><init>()V

    .line 1898786
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1898787
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1898784
    const v0, -0x3c917d0c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1898783
    const v0, -0x1bba1006

    return v0
.end method
