.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1899482
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1899483
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1899481
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1899436
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1899437
    const/4 v2, 0x0

    .line 1899438
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1899439
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1899440
    :goto_0
    move v1, v2

    .line 1899441
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1899442
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1899443
    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$PagesPlatformPartialScreenQueryModel;-><init>()V

    .line 1899444
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1899445
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1899446
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1899447
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1899448
    :cond_0
    return-object v1

    .line 1899449
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1899450
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 1899451
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1899452
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1899453
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1899454
    const-string v4, "items"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1899455
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1899456
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_3

    .line 1899457
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1899458
    const/4 v4, 0x0

    .line 1899459
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 1899460
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1899461
    :goto_3
    move v3, v4

    .line 1899462
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1899463
    :cond_3
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1899464
    goto :goto_1

    .line 1899465
    :cond_4
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1899466
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1899467
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_5
    move v1, v2

    goto :goto_1

    .line 1899468
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1899469
    :cond_7
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_9

    .line 1899470
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1899471
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1899472
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v6, :cond_7

    .line 1899473
    const-string p0, "screen_element"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1899474
    invoke-static {p1, v0}, LX/CVr;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_4

    .line 1899475
    :cond_8
    const-string p0, "target_screen_element_id"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1899476
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_4

    .line 1899477
    :cond_9
    const/4 v6, 0x2

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1899478
    invoke-virtual {v0, v4, v5}, LX/186;->b(II)V

    .line 1899479
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1899480
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_3

    :cond_a
    move v3, v4

    move v5, v4

    goto :goto_4
.end method
