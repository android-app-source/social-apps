.class public final Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1e7cdda4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1907252
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1907251
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1907249
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1907250
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1907165
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1907166
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1907167
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromInterface"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1907224
    if-nez p0, :cond_0

    .line 1907225
    const/4 p0, 0x0

    .line 1907226
    :goto_0
    return-object p0

    .line 1907227
    :cond_0
    instance-of v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    if-eqz v0, :cond_1

    .line 1907228
    check-cast p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    goto :goto_0

    .line 1907229
    :cond_1
    new-instance v3, LX/CW2;

    invoke-direct {v3}, LX/CW2;-><init>()V

    .line 1907230
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->a:Ljava/lang/String;

    .line 1907231
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1907232
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1907233
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1907234
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1907235
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->b:LX/0Px;

    .line 1907236
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->c:Ljava/lang/String;

    .line 1907237
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->d:Ljava/lang/String;

    .line 1907238
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1907239
    :goto_2
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1907240
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;->a(Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;)Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1907241
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1907242
    :cond_3
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->e:LX/0Px;

    .line 1907243
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->f:Ljava/lang/String;

    .line 1907244
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->g:Ljava/lang/String;

    .line 1907245
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->jX_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->h:Ljava/lang/String;

    .line 1907246
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->jY_()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->i:Ljava/lang/String;

    .line 1907247
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, LX/CW2;->j:Ljava/lang/String;

    .line 1907248
    invoke-virtual {v3}, LX/CW2;->a()Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    move-result-object p0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 11

    .prologue
    .line 1907200
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1907201
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1907202
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1907203
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1907204
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1907205
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1907206
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1907207
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1907208
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->jX_()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1907209
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->jY_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1907210
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 1907211
    const/16 v10, 0xa

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 1907212
    const/4 v10, 0x0

    invoke-virtual {p1, v10, v0}, LX/186;->b(II)V

    .line 1907213
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1907214
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1907215
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1907216
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1907217
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1907218
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1907219
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1907220
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1907221
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1907222
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1907223
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1907187
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1907188
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1907189
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1907190
    if-eqz v1, :cond_0

    .line 1907191
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    .line 1907192
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->f:Ljava/util/List;

    .line 1907193
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1907194
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1907195
    if-eqz v1, :cond_1

    .line 1907196
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    .line 1907197
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->i:Ljava/util/List;

    .line 1907198
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1907199
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907185
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->e:Ljava/lang/String;

    .line 1907186
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1907182
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;-><init>()V

    .line 1907183
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1907184
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907180
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->g:Ljava/lang/String;

    .line 1907181
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907178
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->h:Ljava/lang/String;

    .line 1907179
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907176
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->j:Ljava/lang/String;

    .line 1907177
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1907175
    const v0, -0x611a45a2

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907173
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k:Ljava/lang/String;

    .line 1907174
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1907172
    const v0, 0x5652bc0d

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907170
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->n:Ljava/lang/String;

    .line 1907171
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final jX_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907168
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l:Ljava/lang/String;

    const/4 v1, 0x7

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l:Ljava/lang/String;

    .line 1907169
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final jY_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1907163
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->m:Ljava/lang/String;

    .line 1907164
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getExtraPriceItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1907161
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->f:Ljava/util/List;

    .line 1907162
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPriceItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1907159
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentPriceItemFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->i:Ljava/util/List;

    .line 1907160
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/PlatformPaymentsModels$PagesPlatformPaymentOrderInfoFragmentModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
