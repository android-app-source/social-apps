.class public final Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1901895
    const-class v0, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel;

    new-instance v1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1901896
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1901897
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1901898
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1901899
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1901900
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1901901
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1901902
    if-eqz v2, :cond_0

    .line 1901903
    const-string p0, "additional_fees"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1901904
    invoke-static {v1, v2, p1, p2}, LX/CW7;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1901905
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1901906
    if-eqz v2, :cond_1

    .line 1901907
    const-string p0, "event_listeners"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1901908
    invoke-static {v1, v2, p1, p2}, LX/CVU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1901909
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1901910
    if-eqz v2, :cond_2

    .line 1901911
    const-string p0, "fee"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1901912
    invoke-static {v1, v2, p1}, LX/CVR;->a(LX/15i;ILX/0nX;)V

    .line 1901913
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1901914
    if-eqz v2, :cond_3

    .line 1901915
    const-string p0, "is_optional"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1901916
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1901917
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1901918
    if-eqz v2, :cond_4

    .line 1901919
    const-string p0, "product_items"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1901920
    invoke-static {v1, v2, p1, p2}, LX/CVu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1901921
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->b(II)Z

    move-result v2

    .line 1901922
    if-eqz v2, :cond_5

    .line 1901923
    const-string p0, "show_subtotal"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1901924
    invoke-virtual {p1, v2}, LX/0nX;->a(Z)V

    .line 1901925
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1901926
    if-eqz v2, :cond_6

    .line 1901927
    const-string p0, "tip"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1901928
    invoke-static {v1, v2, p1}, LX/CVb;->a(LX/15i;ILX/0nX;)V

    .line 1901929
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1901930
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1901931
    check-cast p1, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel$Serializer;->a(Lcom/facebook/pages/common/platform/protocol/PagesPlatformFirstPartyFlowModels$ShoppingCartFormFieldFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
