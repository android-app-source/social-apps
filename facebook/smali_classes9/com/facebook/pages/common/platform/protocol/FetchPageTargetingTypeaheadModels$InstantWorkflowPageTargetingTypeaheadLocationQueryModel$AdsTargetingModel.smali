.class public final Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xcf5fc65
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1896757
    const-class v0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1896756
    const-class v0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1896754
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1896755
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1896748
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1896749
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;->a()Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1896750
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1896751
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1896752
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1896753
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1896733
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1896734
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;->a()Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1896735
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;->a()Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    .line 1896736
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;->a()Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1896737
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;

    .line 1896738
    iput-object v0, v1, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;->e:Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    .line 1896739
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1896740
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1896746
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;->e:Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;->e:Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    .line 1896747
    iget-object v0, p0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;->e:Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1896743
    new-instance v0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel;-><init>()V

    .line 1896744
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1896745
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1896742
    const v0, -0x572f4925

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1896741
    const v0, 0x21443201

    return v0
.end method
