.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/widget/LinearLayout;

.field private final c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

.field private final d:Landroid/widget/LinearLayout;

.field private final e:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

.field private final f:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

.field private final g:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

.field private final h:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

.field public i:LX/CXT;

.field private j:LX/CTm;

.field private k:LX/CSY;

.field private l:LX/CT7;

.field public m:LX/CT6;

.field private n:LX/CT1;

.field public o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ">;"
        }
    .end annotation
.end field

.field private q:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ">;"
        }
    .end annotation
.end field

.field private r:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909177
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909178
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909213
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909214
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1909215
    const v0, 0x7f030fa2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909216
    const v0, 0x7f0d25ca

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->d:Landroid/widget/LinearLayout;

    .line 1909217
    const v0, 0x7f0d25c6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->b:Landroid/widget/LinearLayout;

    .line 1909218
    const v0, 0x7f0d25c7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    .line 1909219
    const v0, 0x7f0d25c9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->e:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    .line 1909220
    const v0, 0x7f0d25c8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->f:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    .line 1909221
    const v0, 0x7f0d25cb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->g:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    .line 1909222
    const v0, 0x7f0d25cc    # 1.876174E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->h:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    .line 1909223
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->e:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a94

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setTitle(Ljava/lang/String;)V

    .line 1909224
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->e:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    sget-object v1, LX/CXJ;->OTHER_FEE:LX/CXJ;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValueStyle(LX/CXJ;)V

    .line 1909225
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->f:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a98

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setTitle(Ljava/lang/String;)V

    .line 1909226
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->f:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    sget-object v1, LX/CXJ;->OTHER_FEE:LX/CXJ;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValueStyle(LX/CXJ;)V

    .line 1909227
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->g:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a99

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setTitle(Ljava/lang/String;)V

    .line 1909228
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->g:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    sget-object v1, LX/CXJ;->OTHER_FEE:LX/CXJ;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValueStyle(LX/CXJ;)V

    .line 1909229
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->h:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081d45

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setTitle(Ljava/lang/String;)V

    .line 1909230
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->h:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    sget-object v1, LX/CXJ;->TOTAL_FEE:LX/CXJ;

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValueStyle(LX/CXJ;)V

    .line 1909231
    return-void
.end method

.method private static a(LX/CSY;LX/0Px;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CSY;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ")",
            "LX/CXT;"
        }
    .end annotation

    .prologue
    .line 1909183
    const-string v0, "tip"

    .line 1909184
    iget-object v1, p0, LX/CSY;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CSZ;

    invoke-virtual {v1}, LX/CSZ;->e()Lorg/json/JSONObject;

    move-result-object v1

    move-object v0, v1

    .line 1909185
    :try_start_0
    const-string v6, "use_cash"

    invoke-virtual {v0, v6}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 1909186
    new-instance v6, Ljava/math/BigDecimal;

    const-string v8, "amount"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    invoke-direct {v6, v8, v9}, Ljava/math/BigDecimal;-><init>(D)V

    .line 1909187
    const-string v8, "currency"

    invoke-virtual {v0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1909188
    new-instance v9, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-direct {v9, v8, v6}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    .line 1909189
    new-instance v6, LX/3rL;

    invoke-direct {v6, v9, v7}, LX/3rL;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1909190
    :goto_0
    move-object v1, v6

    .line 1909191
    iget-object v0, v1, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 1909192
    iget-object v0, v1, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1909193
    if-eqz v2, :cond_0

    .line 1909194
    iget-object v0, p2, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1909195
    invoke-static {v0}, LX/CXT;->a(Ljava/lang/String;)LX/CXT;

    move-result-object v0

    .line 1909196
    :goto_1
    return-object v0

    .line 1909197
    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 1909198
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-ge v3, v4, :cond_1

    invoke-virtual {p1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 1909199
    invoke-static {p2, v1}, LX/CXT;->a(Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/Integer;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v5

    .line 1909200
    const/4 v9, 0x4

    .line 1909201
    invoke-static {v0, v5}, Lcom/facebook/payments/currency/CurrencyAmount;->d(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 1909202
    iget-object v6, v0, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    invoke-static {v6}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->numberOfTrailingZeros(I)I

    move-result v6

    .line 1909203
    iget-object v7, v0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v7, v7

    .line 1909204
    invoke-virtual {v7, v6, v9}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v7

    .line 1909205
    iget-object v8, v5, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v8, v8

    .line 1909206
    invoke-virtual {v8, v6, v9}, Ljava/math/BigDecimal;->setScale(II)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v6

    move v5, v6

    .line 1909207
    if-nez v5, :cond_3

    .line 1909208
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1909209
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_2

    .line 1909210
    :cond_1
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1909211
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-static {v1, v0}, LX/CXT;->a(Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v0

    goto :goto_1

    .line 1909212
    :cond_2
    invoke-static {v0}, LX/CXT;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v1, v2

    goto :goto_3

    :catch_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private static a(LX/CSY;LX/CTl;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;
    .locals 1

    .prologue
    .line 1909179
    if-nez p0, :cond_0

    .line 1909180
    invoke-static {p1, p2}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a(LX/CTl;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v0

    .line 1909181
    :goto_0
    return-object v0

    .line 1909182
    :cond_0
    iget-object v0, p1, LX/CTl;->d:LX/0Px;

    invoke-static {p0, v0, p2}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a(LX/CSY;LX/0Px;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/CTl;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;
    .locals 3

    .prologue
    .line 1909165
    iget-object v0, p0, LX/CTl;->c:LX/CXU;

    iget v1, p0, LX/CTl;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1909166
    sget-object v2, LX/CXU;->CASH:LX/CXU;

    if-ne v0, v2, :cond_0

    .line 1909167
    iget-object v2, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1909168
    invoke-static {v2}, LX/CXT;->a(Ljava/lang/String;)LX/CXT;

    move-result-object v2

    .line 1909169
    :goto_0
    move-object v0, v2

    .line 1909170
    return-object v0

    .line 1909171
    :cond_0
    sget-object v2, LX/CXU;->DEFAULT_PERCENTAGE:LX/CXU;

    if-ne v0, v2, :cond_1

    .line 1909172
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1909173
    invoke-static {p1, v2}, LX/CXT;->a(Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/Integer;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object p0

    .line 1909174
    invoke-static {v2, p0}, LX/CXT;->a(Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v2

    goto :goto_0

    .line 1909175
    :cond_1
    iget-object v2, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1909176
    invoke-static {v2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v2

    invoke-static {v2}, LX/CXT;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v2

    goto :goto_0
.end method

.method private a()Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;
    .locals 4

    .prologue
    .line 1909232
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03165a

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->b:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;

    return-object v0
.end method

.method private a(LX/CSY;)V
    .locals 6

    .prologue
    .line 1909154
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v4, v0, LX/CTm;->d:LX/CTl;

    .line 1909155
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->p:Ljava/util/HashMap;

    invoke-static {v0, v1}, LX/CXE;->a(Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v5

    .line 1909156
    if-eqz v4, :cond_0

    if-nez v5, :cond_1

    .line 1909157
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->i:LX/CXT;

    .line 1909158
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->setVisibility(I)V

    .line 1909159
    :goto_0
    return-void

    .line 1909160
    :cond_1
    invoke-static {p1, v4, v5}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a(LX/CSY;LX/CTl;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v1

    .line 1909161
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->setVisibility(I)V

    .line 1909162
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    iget-object v2, v4, LX/CTl;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->setTipTitle(Ljava/lang/String;)V

    .line 1909163
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    new-instance v2, LX/CWv;

    invoke-direct {v2, p0}, LX/CWv;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;)V

    invoke-virtual {v0, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->setOnTipSelectionChangedListener(LX/CWu;)V

    .line 1909164
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    new-instance v2, LX/CWx;

    invoke-direct {v2, p0}, LX/CWx;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;)V

    iget-boolean v3, v4, LX/CTl;->b:Z

    iget-object v4, v4, LX/CTl;->d:LX/0Px;

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(LX/CXT;LX/CWw;ZLX/0Px;Lcom/facebook/payments/currency/CurrencyAmount;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v0

    check-cast v0, LX/5fv;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a:LX/5fv;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1909148
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909149
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->k:LX/CSY;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    invoke-static {v2}, LX/CXE;->a(Ljava/util/HashMap;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CSY;->a(Ljava/lang/String;Lorg/json/JSONArray;)V

    .line 1909150
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->l:LX/CT7;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v2, v2, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->k:LX/CSY;

    invoke-virtual {v0, v1, v2, v3}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1909151
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v0, v0, LX/CTJ;->p:Ljava/util/HashMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_REMOVE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1909152
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->n:LX/CT1;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_REMOVE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v0, v0, LX/CTJ;->p:Ljava/util/HashMap;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_ITEM_REMOVE:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTv;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v3, v3, LX/CTJ;->o:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, LX/CT1;->a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;LX/CTv;Ljava/lang/String;)V

    .line 1909153
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/16 v12, 0x8

    const/4 v5, 0x0

    .line 1909085
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->q:Ljava/util/HashMap;

    invoke-static {v0, v1}, LX/CXE;->a(Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1909086
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v1, v1, LX/CTm;->c:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-static {v1, v0}, LX/CXh;->a(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v6

    .line 1909087
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->p:Ljava/util/HashMap;

    invoke-static {v0, v1}, LX/CXE;->a(Ljava/util/HashMap;Ljava/util/HashMap;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v7

    .line 1909088
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-boolean v0, v0, LX/CTm;->a:Z

    if-eqz v0, :cond_0

    if-nez v7, :cond_4

    .line 1909089
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->f:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v0, v12}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setVisibility(I)V

    .line 1909090
    :goto_0
    if-nez v6, :cond_5

    .line 1909091
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->e:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v0, v12}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setVisibility(I)V

    .line 1909092
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v0, v0, LX/CTm;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1909093
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    move-object v0, v2

    .line 1909094
    :goto_2
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->i:LX/CXT;

    if-nez v1, :cond_8

    .line 1909095
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->g:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v1, v12}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setVisibility(I)V

    .line 1909096
    :goto_3
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->i:LX/CXT;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->i:LX/CXT;

    .line 1909097
    iget-object v2, v1, LX/CXT;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v2, v2

    .line 1909098
    :cond_1
    invoke-static {v7, v2}, LX/CXh;->a(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    invoke-static {v6, v0}, LX/CXh;->a(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v3

    invoke-static {v1, v3}, LX/CXh;->a(Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    move-object v0, v1

    .line 1909099
    if-nez v0, :cond_9

    .line 1909100
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->h:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v0, v12}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setVisibility(I)V

    .line 1909101
    :goto_4
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->k:LX/CSY;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->r:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    invoke-static {v2}, LX/CXE;->a(Ljava/util/HashMap;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CSY;->a(Ljava/lang/String;Lorg/json/JSONArray;)V

    .line 1909102
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->i:LX/CXT;

    if-eqz v0, :cond_2

    .line 1909103
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->k:LX/CSY;

    const-string v1, "tip"

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->i:LX/CXT;

    .line 1909104
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 1909105
    :try_start_0
    const-string v4, "use_cash"

    .line 1909106
    iget-object v5, v2, LX/CXT;->c:LX/CXU;

    move-object v5, v5

    .line 1909107
    sget-object v6, LX/CXU;->CASH:LX/CXU;

    if-ne v5, v6, :cond_b

    const/4 v5, 0x1

    :goto_5
    move v5, v5

    .line 1909108
    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 1909109
    const-string v4, "amount"

    .line 1909110
    iget-object v5, v2, LX/CXT;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v5, v5

    .line 1909111
    iget-object v6, v5, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v5, v6

    .line 1909112
    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 1909113
    const-string v4, "currency"

    .line 1909114
    iget-object v5, v2, LX/CXT;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v5, v5

    .line 1909115
    iget-object v6, v5, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v5, v6

    .line 1909116
    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1909117
    :goto_6
    move-object v2, v3

    .line 1909118
    iget-object v3, v0, LX/CSY;->c:Ljava/util/HashMap;

    new-instance v4, LX/CSZ;

    invoke-direct {v4, v2}, LX/CSZ;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v3, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909119
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->l:LX/CT7;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    .line 1909120
    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1909121
    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-lez v2, :cond_3

    .line 1909122
    const/4 v2, 0x1

    .line 1909123
    :goto_7
    move v1, v2

    .line 1909124
    iget-object v2, v0, LX/CT7;->a:LX/CTC;

    iget-object v3, v2, LX/CTC;->y:Lcom/facebook/fig/button/FigButton;

    if-eqz v1, :cond_d

    iget-object v2, v0, LX/CT7;->a:LX/CTC;

    iget-object v2, v2, LX/CTC;->s:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->a:Ljava/lang/Object;

    check-cast v2, LX/CUD;

    iget-object v2, v2, LX/CUD;->e:LX/CU8;

    iget-boolean v2, v2, LX/CU8;->d:Z

    if-nez v2, :cond_d

    const/4 v2, 0x1

    :goto_8
    invoke-virtual {v3, v2}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 1909125
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->l:LX/CT7;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v2, v2, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->k:LX/CSY;

    invoke-virtual {v0, v1, v2, v3}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1909126
    return-void

    .line 1909127
    :cond_4
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->f:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v0, v5}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setVisibility(I)V

    .line 1909128
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->f:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v0, v7}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValue(Lcom/facebook/payments/currency/CurrencyAmount;)V

    goto/16 :goto_0

    .line 1909129
    :cond_5
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->e:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v0, v5}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setVisibility(I)V

    .line 1909130
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->e:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v0, v6}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValue(Lcom/facebook/payments/currency/CurrencyAmount;)V

    goto/16 :goto_1

    .line 1909131
    :cond_6
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1909132
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1909133
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v8, v0, LX/CTm;->b:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v4, v5

    move-object v3, v2

    :goto_9
    if-ge v4, v9, :cond_a

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTi;

    .line 1909134
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v10, 0x7f031659

    iget-object v11, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v10, v11, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    .line 1909135
    iget-object v10, v0, LX/CTi;->b:Ljava/lang/String;

    invoke-virtual {v1, v10}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setTitle(Ljava/lang/String;)V

    .line 1909136
    iget-object v10, v0, LX/CTi;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v10}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValue(Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 1909137
    iget-object v10, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v10, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1909138
    if-nez v3, :cond_7

    iget-object v0, v0, LX/CTi;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 1909139
    :goto_a
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v3, v0

    goto :goto_9

    .line 1909140
    :cond_7
    iget-object v0, v0, LX/CTi;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v3, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Lcom/facebook/payments/currency/CurrencyAmount;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    goto :goto_a

    .line 1909141
    :cond_8
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->g:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v1, v5}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setVisibility(I)V

    .line 1909142
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->g:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->i:LX/CXT;

    .line 1909143
    iget-object v4, v3, LX/CXT;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v3, v4

    .line 1909144
    invoke-virtual {v1, v3}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValue(Lcom/facebook/payments/currency/CurrencyAmount;)V

    goto/16 :goto_3

    .line 1909145
    :cond_9
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->h:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v1, v5}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setVisibility(I)V

    .line 1909146
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->h:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->setValue(Lcom/facebook/payments/currency/CurrencyAmount;)V

    goto/16 :goto_4

    :cond_a
    move-object v0, v3

    goto/16 :goto_2

    :catch_0
    goto/16 :goto_6

    :cond_b
    :try_start_1
    const/4 v5, 0x0

    goto/16 :goto_5
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 1909147
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_8
.end method


# virtual methods
.method public final a(LX/CTm;LX/CT7;LX/CT6;LX/CT1;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 1909026
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    .line 1909027
    iput-object p2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->l:LX/CT7;

    .line 1909028
    iput-object p3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->m:LX/CT6;

    .line 1909029
    iput-object p4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->n:LX/CT1;

    .line 1909030
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->l:LX/CT7;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v2, v2, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {v0, v1, v2}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v2

    .line 1909031
    if-eqz v2, :cond_3

    move-object v0, v2

    :goto_0
    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->k:LX/CSY;

    .line 1909032
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    invoke-static {v0}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->r:Ljava/lang/String;

    .line 1909033
    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->r:Ljava/lang/String;

    .line 1909034
    iget-object v1, v2, LX/CSY;->c:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CSZ;

    invoke-virtual {v1}, LX/CSZ;->d()Lorg/json/JSONArray;

    move-result-object v1

    move-object v0, v1

    .line 1909035
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1909036
    const/4 v1, 0x0

    :goto_1
    :try_start_0
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 1909037
    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v5

    .line 1909038
    const-string v6, "id"

    invoke-virtual {v5, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "qty"

    invoke-virtual {v5, v7}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v6, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1909039
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1909040
    :catch_0
    :cond_0
    move-object v0, v4

    .line 1909041
    :goto_2
    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    .line 1909042
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->p:Ljava/util/HashMap;

    .line 1909043
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->q:Ljava/util/HashMap;

    .line 1909044
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1909045
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v5, v0, LX/CTm;->k:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_7

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTj;

    .line 1909046
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a()Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;

    move-result-object v7

    .line 1909047
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1909048
    iget-object v8, v0, LX/CTj;->f:Ljava/lang/String;

    .line 1909049
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    invoke-virtual {v1, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    invoke-virtual {v1, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 1909050
    :goto_4
    iget-object v9, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v9, v8, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909051
    iget-object v9, v0, LX/CTj;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v9, :cond_1

    .line 1909052
    iget-object v9, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->p:Ljava/util/HashMap;

    iget-object v10, v0, LX/CTj;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v9, v8, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909053
    :cond_1
    iget-object v9, v0, LX/CTj;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v9, :cond_2

    .line 1909054
    iget-object v9, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->q:Ljava/util/HashMap;

    iget-object v10, v0, LX/CTj;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v9, v8, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909055
    :cond_2
    iget v9, v0, LX/CTj;->c:I

    invoke-virtual {v7, v3, v9}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->a(II)V

    .line 1909056
    invoke-virtual {v7, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->setQuantity(I)V

    .line 1909057
    new-instance v1, LX/CWr;

    invoke-direct {v1, p0, v8}, LX/CWr;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->setQuantityChangedListener(LX/CWq;)V

    .line 1909058
    iget-object v0, v0, LX/CTj;->e:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->setItemTitle(Ljava/lang/String;)V

    .line 1909059
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->p:Ljava/util/HashMap;

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1909060
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a:LX/5fv;

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->p:Ljava/util/HashMap;

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v1, v0}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->a(Ljava/lang/String;)V

    .line 1909061
    :goto_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    .line 1909062
    :cond_3
    new-instance v0, LX/CSY;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v1, v1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-boolean v4, v4, LX/CTK;->i:Z

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, v1, v4, v5}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto/16 :goto_0

    .line 1909063
    :cond_4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto/16 :goto_2

    .line 1909064
    :cond_5
    iget v1, v0, LX/CTj;->d:I

    goto :goto_4

    .line 1909065
    :cond_6
    invoke-virtual {v7}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->a()V

    goto :goto_5

    .line 1909066
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->j:LX/CTm;

    iget-object v4, v0, LX/CTm;->l:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v3

    :goto_6
    if-ge v1, v5, :cond_9

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTk;

    .line 1909067
    iget-object v3, v0, LX/CTk;->a:LX/CU0;

    iget-object v3, v3, LX/CU0;->a:Ljava/lang/String;

    .line 1909068
    iget-object v6, v0, LX/CTk;->c:Ljava/lang/String;

    .line 1909069
    new-instance v7, LX/CXN;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-direct {v7, v8}, LX/CXN;-><init>(Landroid/content/Context;)V

    .line 1909070
    iget-object v8, v0, LX/CTk;->a:LX/CU0;

    .line 1909071
    iget-object v9, v7, LX/CXN;->a:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    invoke-virtual {v9, v8}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(LX/CU0;)V

    .line 1909072
    const/4 v9, 0x0

    invoke-virtual {v7, v9}, LX/CXN;->setVisibility(I)V

    .line 1909073
    new-instance v8, LX/CWs;

    invoke-direct {v8, p0, v6}, LX/CWs;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;Ljava/lang/String;)V

    .line 1909074
    iget-object v6, v7, LX/CXN;->b:Lcom/facebook/fig/button/FigButton;

    new-instance v9, LX/CXM;

    invoke-direct {v9, v7, v8}, LX/CXM;-><init>(LX/CXN;LX/CWs;)V

    invoke-virtual {v6, v9}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909075
    new-instance v6, LX/CWt;

    invoke-direct {v6, p0, v3}, LX/CWt;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;Ljava/lang/String;)V

    .line 1909076
    iget-object v8, v7, LX/CXN;->c:Lcom/facebook/fig/button/FigButton;

    new-instance v9, LX/CXL;

    invoke-direct {v9, v7, v6}, LX/CXL;-><init>(LX/CXN;LX/CWt;)V

    invoke-virtual {v8, v9}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909077
    iget-object v6, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v6, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1909078
    iget-object v6, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->o:Ljava/util/HashMap;

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v3, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909079
    iget-object v6, v0, LX/CTk;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v6, :cond_8

    .line 1909080
    iget-object v6, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->p:Ljava/util/HashMap;

    iget-object v0, v0, LX/CTk;->b:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v6, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909081
    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 1909082
    :cond_9
    invoke-direct {p0, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->a(LX/CSY;)V

    .line 1909083
    invoke-static {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;->b$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldShoppingCartSimpleView;)V

    .line 1909084
    return-void
.end method
