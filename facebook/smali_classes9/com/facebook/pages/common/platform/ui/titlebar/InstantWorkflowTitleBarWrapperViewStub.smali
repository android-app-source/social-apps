.class public Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;
.super Landroid/view/View;
.source ""

# interfaces
.implements LX/CXY;


# instance fields
.field public a:LX/63V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:LX/0h5;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1910447
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1910448
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b()V

    .line 1910449
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1910419
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910420
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b()V

    .line 1910421
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1910450
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910451
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b()V

    .line 1910452
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;

    invoke-static {v0}, LX/63V;->a(LX/0QB;)LX/63V;

    move-result-object v0

    check-cast v0, LX/63V;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->a:LX/63V;

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1910453
    const-class v0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910454
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1910455
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    instance-of v0, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    if-eqz v0, :cond_1

    .line 1910456
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->d()V

    .line 1910457
    :cond_0
    :goto_0
    return-void

    .line 1910458
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    instance-of v0, v0, LX/63h;

    if-eqz v0, :cond_0

    .line 1910459
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    goto :goto_0
.end method

.method public final a(LX/CT3;)V
    .locals 2

    .prologue
    .line 1910460
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    instance-of v0, v0, LX/63h;

    if-eqz v0, :cond_0

    .line 1910461
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1910462
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    new-instance v1, LX/CXZ;

    invoke-direct {v1, p0, p1}, LX/CXZ;-><init>(Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;LX/CT3;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnBackPressedListener(LX/63J;)V

    .line 1910463
    :goto_0
    return-void

    .line 1910464
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    new-instance v1, LX/CXa;

    invoke-direct {v1, p0, p1}, LX/CXa;-><init>(Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;LX/CT3;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1910465
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Try showUpButton(InstantWorkflowNavUpClickListener)"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1910438
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1910439
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->a:LX/63V;

    invoke-virtual {v1}, LX/63V;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1910440
    const v1, 0x7f030023

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    .line 1910441
    new-instance v1, LX/63h;

    invoke-direct {v1, v0}, LX/63h;-><init>(Landroid/support/v7/widget/Toolbar;)V

    iput-object v1, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    .line 1910442
    :goto_0
    invoke-static {p1, p0, v0}, LX/478;->a(Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V

    .line 1910443
    return-void

    .line 1910444
    :cond_0
    const v1, 0x7f03095b

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1910445
    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    goto :goto_0
.end method

.method public final d_(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 1910446
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->d_(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public setButtonSpecs(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1910436
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1910437
    return-void
.end method

.method public setCustomTitleView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1910434
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setCustomTitleView(Landroid/view/View;)V

    .line 1910435
    return-void
.end method

.method public setHasBackButton(Z)V
    .locals 1

    .prologue
    .line 1910432
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setHasBackButton(Z)V

    .line 1910433
    return-void
.end method

.method public setHasFbLogo(Z)V
    .locals 0

    .prologue
    .line 1910431
    return-void
.end method

.method public setOnBackPressedListener(LX/63J;)V
    .locals 1

    .prologue
    .line 1910429
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setOnBackPressedListener(LX/63J;)V

    .line 1910430
    return-void
.end method

.method public setOnToolbarButtonListener(LX/63W;)V
    .locals 1

    .prologue
    .line 1910427
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1910428
    return-void
.end method

.method public setShowDividers(Z)V
    .locals 0

    .prologue
    .line 1910426
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 1910424
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(I)V

    .line 1910425
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1910422
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/titlebar/InstantWorkflowTitleBarWrapperViewStub;->b:LX/0h5;

    invoke-interface {v0, p1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1910423
    return-void
.end method

.method public setTitlebarAsModal(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 1910418
    return-void
.end method
