.class public Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909876
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909877
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909878
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909879
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909880
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909881
    const v0, 0x7f030fad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909882
    const v0, 0x7f0d25e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1909883
    const v0, 0x7f0d25e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1909884
    const v0, 0x7f0d25e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;

    .line 1909885
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1909886
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1909887
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1909888
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->setMinimumQuantity(I)V

    .line 1909889
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;

    invoke-virtual {v0, p2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->setMaximumQuantity(I)V

    .line 1909890
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1909891
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1909892
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909893
    return-void
.end method

.method public setItemTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1909894
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909895
    return-void
.end method

.method public setQuantity(I)V
    .locals 1

    .prologue
    .line 1909896
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;

    invoke-virtual {v0, p1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->setCurrentQuantity(I)V

    .line 1909897
    return-void
.end method

.method public setQuantityChangedListener(LX/CWq;)V
    .locals 1

    .prologue
    .line 1909898
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartItemView;->c:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;

    .line 1909899
    iput-object p1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->i:LX/CWq;

    .line 1909900
    return-void
.end method
