.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/SwitchCompat;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908554
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908555
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908556
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908557
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908558
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908559
    const v0, 0x7f030f99

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908560
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->setOrientation(I)V

    .line 1908561
    const v0, 0x7f0d25be

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->a:Lcom/facebook/widget/SwitchCompat;

    .line 1908562
    const v0, 0x7f0d25bd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1908563
    const v0, 0x7f0d25bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1908564
    return-void
.end method


# virtual methods
.method public final a(LX/CTR;LX/CT7;)V
    .locals 7

    .prologue
    .line 1908565
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v0

    .line 1908566
    if-eqz v0, :cond_0

    move-object v2, v0

    .line 1908567
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v3

    .line 1908568
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, p1, LX/CTR;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908569
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->c:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, p1, LX/CTR;->c:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908570
    if-eqz v0, :cond_1

    .line 1908571
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->a:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, v3}, LX/CSY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 1908572
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->a:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908573
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1, v2}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908574
    iget-object v6, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->a:Lcom/facebook/widget/SwitchCompat;

    new-instance v0, LX/CWe;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/CWe;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTR;)V

    invoke-virtual {v6, v0}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1908575
    return-void

    .line 1908576
    :cond_0
    new-instance v2, LX/CSY;

    iget-object v1, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v3, p1, LX/CTK;->i:Z

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v2, v1, v3, v4}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908577
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldMarketOptinView;->a:Lcom/facebook/widget/SwitchCompat;

    iget-boolean v1, p1, LX/CTR;->a:Z

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    goto :goto_1
.end method
