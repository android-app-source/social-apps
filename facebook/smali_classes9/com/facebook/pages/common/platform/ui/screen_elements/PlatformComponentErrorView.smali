.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final d:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final e:Lcom/facebook/fig/button/FigButton;

.field private final f:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910296
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910297
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910294
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910295
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910285
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910286
    const-class v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910287
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030f94

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1910288
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->setOrientation(I)V

    .line 1910289
    const v0, 0x7f0d25a6

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910290
    const v0, 0x7f0d25a7

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->d:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910291
    const v0, 0x7f0d25a8

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->e:Lcom/facebook/fig/button/FigButton;

    .line 1910292
    const v0, 0x7f0d25a9

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->f:Lcom/facebook/fig/button/FigButton;

    .line 1910293
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;Lcom/facebook/content/SecureContextHelper;LX/17W;)V
    .locals 0

    .prologue
    .line 1910298
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object p2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->b:LX/17W;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->a(Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;Lcom/facebook/content/SecureContextHelper;LX/17W;)V

    return-void
.end method


# virtual methods
.method public final a(LX/CTb;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1910274
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTb;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910275
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->d:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTb;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910276
    iget-object v0, p1, LX/CTb;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1910277
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->e:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1910278
    :goto_0
    iget-object v0, p1, LX/CTb;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1910279
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->f:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1910280
    :goto_1
    return-void

    .line 1910281
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->e:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1910282
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->e:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/CXW;

    invoke-direct {v1, p0, p1}, LX/CXW;-><init>(Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;LX/CTb;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1910283
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->f:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1910284
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;->f:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/CXX;

    invoke-direct {v1, p0, p1}, LX/CXX;-><init>(Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentErrorView;LX/CTb;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method
