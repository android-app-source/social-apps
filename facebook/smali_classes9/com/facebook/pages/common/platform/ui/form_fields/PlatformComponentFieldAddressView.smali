.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final b:Lcom/facebook/fig/textinput/FigEditText;

.field private final c:Lcom/facebook/fig/textinput/FigEditText;

.field private final d:Lcom/facebook/fig/textinput/FigEditText;

.field private final e:Lcom/facebook/fig/textinput/FigEditText;

.field private final f:Lcom/facebook/fig/textinput/FigEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908341
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908342
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908343
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908344
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908332
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908333
    const v0, 0x7f030f95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908334
    const v0, 0x7f0d25aa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908335
    const v0, 0x7f0d25ab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->b:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908336
    const v0, 0x7f0d25ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->c:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908337
    const v0, 0x7f0d25ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->d:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908338
    const v0, 0x7f0d25ae

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->e:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908339
    const v0, 0x7f0d25af

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->f:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908340
    return-void
.end method


# virtual methods
.method public final a(LX/CTL;LX/CT7;)V
    .locals 9

    .prologue
    .line 1908322
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908323
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v2

    .line 1908324
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 1908325
    :goto_0
    const-string v1, "address1"

    iget-object v4, p1, LX/CTL;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a88

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->b:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908326
    const-string v1, "address2"

    iget-object v4, p1, LX/CTL;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a89

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->c:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908327
    const-string v1, "city"

    iget-object v4, p1, LX/CTL;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a8a

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->d:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908328
    const-string v1, "state"

    iget-object v4, p1, LX/CTL;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a8b

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->e:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908329
    const-string v1, "zipcode"

    iget-object v4, p1, LX/CTL;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a8c

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldAddressView;->f:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908330
    return-void

    .line 1908331
    :cond_0
    new-instance v3, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v3, v0, v1, v4}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0
.end method
