.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;
.super Lcom/facebook/resources/ui/FbCheckBox;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1908349
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbCheckBox;-><init>(Landroid/content/Context;)V

    .line 1908350
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908351
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908352
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908353
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbCheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908354
    return-void
.end method


# virtual methods
.method public final a(LX/CTK;LX/CT7;)V
    .locals 7

    .prologue
    .line 1908355
    iget-object v0, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;->setText(Ljava/lang/CharSequence;)V

    .line 1908356
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v6

    .line 1908357
    if-eqz v6, :cond_0

    move-object v2, v6

    .line 1908358
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v3

    .line 1908359
    new-instance v0, LX/CWY;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/CWY;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTK;)V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908360
    if-eqz v6, :cond_1

    .line 1908361
    invoke-virtual {v6, v3}, LX/CSY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;->setChecked(Z)V

    .line 1908362
    :goto_1
    return-void

    .line 1908363
    :cond_0
    new-instance v2, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v2, v0, v1, v3}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908364
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldCheckboxView;->setChecked(Z)V

    goto :goto_1
.end method
