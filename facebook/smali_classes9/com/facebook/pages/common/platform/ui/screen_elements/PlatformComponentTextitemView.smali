.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final k:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final l:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final m:Lcom/facebook/drawee/span/DraweeSpanTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910388
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910389
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910390
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910391
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910392
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910393
    const-class v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910394
    const v0, 0x7f030faa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1910395
    const v0, 0x7f0d25da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->k:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910396
    const v0, 0x7f0d25db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->l:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910397
    const v0, 0x7f0d25dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->m:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910398
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method


# virtual methods
.method public final a(LX/CTh;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1910399
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget v0, p1, LX/CTh;->e:I

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v0, p1, LX/CTh;->d:LX/15i;

    iget v4, p1, LX/CTh;->e:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-virtual {v0, v4, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 1910400
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-object v0, p1, LX/CTh;->d:LX/15i;

    iget v3, p1, LX/CTh;->e:I

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-virtual {v0, v3, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1910401
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1910402
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->k:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v2, p1, LX/CTh;->a:LX/CU1;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1910403
    iget-object v0, p1, LX/CTh;->b:LX/CU1;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->l:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0, v1, v2}, LX/CXh;->a(LX/CU1;Lcom/facebook/drawee/span/DraweeSpanTextView;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    .line 1910404
    iget-object v0, p1, LX/CTh;->c:LX/CU1;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->m:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0, v1, v2}, LX/CXh;->a(LX/CU1;Lcom/facebook/drawee/span/DraweeSpanTextView;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    .line 1910405
    return-void

    .line 1910406
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 1910407
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    .line 1910408
    :cond_2
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_1
.end method

.method public final iW_()LX/1aX;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1aX",
            "<",
            "LX/1af;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1910409
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentTextitemView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1910410
    new-instance v1, LX/1Uo;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v2, LX/1Up;->c:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    .line 1910411
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-static {v1, v0}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    return-object v0
.end method
