.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final k:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final l:Lcom/facebook/drawee/span/DraweeSpanTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910181
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910182
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910179
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910180
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910151
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910152
    const-class v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910153
    const v0, 0x7f030f91

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1910154
    const v0, 0x7f0d259a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->k:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910155
    const v0, 0x7f0d259b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->l:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910156
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method


# virtual methods
.method public final a(LX/CTV;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1910157
    iget-object v0, p1, LX/CTV;->c:LX/CTw;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/CTV;->c:LX/CTw;

    .line 1910158
    iget-object v1, v0, LX/CTw;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1910159
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p1, LX/CTV;->c:LX/CTw;

    .line 1910160
    iget-object v1, v0, LX/CTw;->d:Ljava/lang/Integer;

    move-object v0, v1

    .line 1910161
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p1, LX/CTV;->c:LX/CTw;

    .line 1910162
    iget-object v1, v0, LX/CTw;->c:Ljava/lang/Integer;

    move-object v0, v1

    .line 1910163
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_0

    .line 1910164
    iget-object v0, p1, LX/CTV;->c:LX/CTw;

    .line 1910165
    iget-object v1, v0, LX/CTw;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1910166
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1910167
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1910168
    iget-object v2, v0, LX/CTw;->c:Ljava/lang/Integer;

    move-object v2, v2

    .line 1910169
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v1, v2, v3}, LX/471;->a(Landroid/content/res/Resources;FI)I

    move-result v1

    .line 1910170
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1910171
    iget-object v4, v0, LX/CTw;->d:Ljava/lang/Integer;

    move-object v0, v4

    .line 1910172
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    invoke-static {v2, v0, v3}, LX/471;->a(Landroid/content/res/Resources;FI)I

    move-result v0

    .line 1910173
    invoke-virtual {p0, v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 1910174
    invoke-virtual {p0, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1910175
    :goto_0
    iget-object v0, p1, LX/CTV;->a:LX/CU1;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->k:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0, v1, v2}, LX/CXh;->a(LX/CU1;Lcom/facebook/drawee/span/DraweeSpanTextView;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    .line 1910176
    iget-object v0, p1, LX/CTV;->b:LX/CU1;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->l:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressLabelView;->j:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0, v1, v2}, LX/CXh;->a(LX/CU1;Lcom/facebook/drawee/span/DraweeSpanTextView;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    .line 1910177
    return-void

    .line 1910178
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_0
.end method
