.class public Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphButton;

.field private b:Lcom/facebook/fbui/glyph/GlyphButton;

.field private c:Landroid/widget/TextView;

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/text/NumberFormat;

.field public i:LX/CWq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909693
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1909694
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    .line 1909695
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e:I

    .line 1909696
    const/16 v0, 0x14

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->f:I

    .line 1909697
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    .line 1909698
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909699
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909764
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909765
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    .line 1909766
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e:I

    .line 1909767
    const/16 v0, 0x14

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->f:I

    .line 1909768
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    .line 1909769
    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909770
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909757
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909758
    const/4 v0, 0x1

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    .line 1909759
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e:I

    .line 1909760
    const/16 v0, 0x14

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->f:I

    .line 1909761
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    .line 1909762
    invoke-direct {p0, p1, p2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909763
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1909743
    const v0, 0x7f03095c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909744
    invoke-static {}, Ljava/text/NumberFormat;->getIntegerInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->h:Ljava/text/NumberFormat;

    .line 1909745
    const v0, 0x7f0d1809

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1909746
    const v0, 0x7f0d1807

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphButton;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    .line 1909747
    const v0, 0x7f0d1808

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->c:Landroid/widget/TextView;

    .line 1909748
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/CXG;

    invoke-direct {v1, p0}, LX/CXG;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909749
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    new-instance v1, LX/CXH;

    invoke-direct {v1, p0}, LX/CXH;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909750
    sget-object v0, LX/03r;->InstantWorkflowQuantityPicker:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1909751
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 1909752
    if-eqz v1, :cond_0

    .line 1909753
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1909754
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1909755
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1909756
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;)V
    .locals 2

    .prologue
    .line 1909739
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->f:I

    if-ge v0, v1, :cond_0

    .line 1909740
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->c()V

    .line 1909741
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->f()V

    .line 1909742
    :cond_0
    return-void
.end method

.method public static b$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;)V
    .locals 2

    .prologue
    .line 1909735
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    if-le v0, v1, :cond_0

    .line 1909736
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d()V

    .line 1909737
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->f()V

    .line 1909738
    :cond_0
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1909731
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e:I

    if-ge v0, v1, :cond_0

    .line 1909732
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->setCurrentQuantity(I)V

    .line 1909733
    :goto_0
    return-void

    .line 1909734
    :cond_0
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->setCurrentQuantity(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1909727
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e:I

    if-le v0, v1, :cond_0

    .line 1909728
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->setCurrentQuantity(I)V

    .line 1909729
    :goto_0
    return-void

    .line 1909730
    :cond_0
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->setCurrentQuantity(I)V

    goto :goto_0
.end method

.method private e()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1909721
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->h:Ljava/text/NumberFormat;

    iget v4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    int-to-long v4, v4

    invoke-virtual {v3, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909722
    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->b:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    iget v4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    if-le v0, v4, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1909723
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->a:Lcom/facebook/fbui/glyph/GlyphButton;

    iget v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    iget v4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->f:I

    if-ge v3, v4, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphButton;->setEnabled(Z)V

    .line 1909724
    return-void

    :cond_0
    move v0, v2

    .line 1909725
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1909726
    goto :goto_1
.end method

.method private f()V
    .locals 2

    .prologue
    .line 1909718
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->i:LX/CWq;

    if-eqz v0, :cond_0

    .line 1909719
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->i:LX/CWq;

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    invoke-interface {v0, v1}, LX/CWq;->a(I)V

    .line 1909720
    :cond_0
    return-void
.end method


# virtual methods
.method public getCurrentQuantity()I
    .locals 1

    .prologue
    .line 1909717
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    return v0
.end method

.method public setCurrentQuantity(I)V
    .locals 0

    .prologue
    .line 1909714
    iput p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    .line 1909715
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e()V

    .line 1909716
    return-void
.end method

.method public setListener(LX/CWq;)V
    .locals 0

    .prologue
    .line 1909712
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->i:LX/CWq;

    .line 1909713
    return-void
.end method

.method public setMaximumQuantity(I)V
    .locals 1

    .prologue
    .line 1909707
    iput p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->f:I

    .line 1909708
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    if-le v0, p1, :cond_0

    .line 1909709
    iput p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    .line 1909710
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e()V

    .line 1909711
    :cond_0
    return-void
.end method

.method public setMinimumQuantity(I)V
    .locals 1

    .prologue
    .line 1909702
    iput p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->d:I

    .line 1909703
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    if-ge v0, p1, :cond_0

    .line 1909704
    iput p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->g:I

    .line 1909705
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e()V

    .line 1909706
    :cond_0
    return-void
.end method

.method public setMinimumSelectedQuantity(I)V
    .locals 0

    .prologue
    .line 1909700
    iput p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/InstantWorkflowQuantityPicker;->e:I

    .line 1909701
    return-void
.end method
