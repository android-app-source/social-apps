.class public Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ai;

.field private final e:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private g:LX/CTw;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1910150
    const-class v0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910113
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910115
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910117
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910118
    new-instance v0, LX/CXV;

    invoke-direct {v0, p0}, LX/CXV;-><init>(Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->c:LX/1Ai;

    .line 1910119
    const-class v0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910120
    const v0, 0x7f030fda

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1910121
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->setGravity(I)V

    .line 1910122
    const v0, 0x7f0d2643

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->e:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910123
    const v0, 0x7f0d2644

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1910124
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;LX/1Ad;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V
    .locals 0

    .prologue
    .line 1910125
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->a:LX/1Ad;

    iput-object p2, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->b:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;

    invoke-static {v1}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-static {v1}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->a(Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;LX/1Ad;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    return-void
.end method


# virtual methods
.method public final a(LX/1ln;)V
    .locals 4
    .param p1    # LX/1ln;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910126
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/1ln;->h()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1910127
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->g:LX/CTw;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->g:LX/CTw;

    .line 1910128
    iget-object v2, v0, LX/CTw;->d:Ljava/lang/Integer;

    move-object v0, v2

    .line 1910129
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->g:LX/CTw;

    .line 1910130
    iget-object v2, v0, LX/CTw;->d:Ljava/lang/Integer;

    move-object v0, v2

    .line 1910131
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->g:LX/CTw;

    .line 1910132
    iget-object v3, v2, LX/CTw;->d:Ljava/lang/Integer;

    move-object v2, v3

    .line 1910133
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v2, v2

    const/4 v3, 0x1

    invoke-static {v0, v2, v3}, LX/471;->a(Landroid/content/res/Resources;FI)I

    move-result v0

    :goto_0
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1910134
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1910135
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, LX/1ln;->g()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, LX/1ln;->h()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1910136
    :cond_0
    return-void

    .line 1910137
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1dde

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    goto :goto_0
.end method

.method public final a(LX/CUF;)V
    .locals 3

    .prologue
    .line 1910138
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->b:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->e:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v2, p1, LX/CUF;->a:LX/CU1;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1910139
    iget-object v0, p1, LX/CUF;->b:LX/CTw;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/CUF;->b:LX/CTw;

    .line 1910140
    iget-object v1, v0, LX/CTw;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1910141
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1910142
    iget-object v0, p1, LX/CUF;->b:LX/CTw;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->g:LX/CTw;

    .line 1910143
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p1, LX/CUF;->b:LX/CTw;

    .line 1910144
    iget-object v2, v1, LX/CTw;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1910145
    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->c:LX/1Ai;

    invoke-virtual {v0, v1}, LX/1Ae;->a(LX/1Ai;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1910146
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1910147
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1910148
    :goto_0
    return-void

    .line 1910149
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutFooterView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method
