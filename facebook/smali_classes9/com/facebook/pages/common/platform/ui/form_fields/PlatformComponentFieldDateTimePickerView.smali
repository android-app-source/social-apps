.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field private final b:Lcom/facebook/events/ui/date/DatePickerView;

.field private final c:Lcom/facebook/events/ui/date/TimePickerView;

.field private final d:LX/Bm9;

.field private final e:LX/BmO;

.field public f:LX/CSY;

.field public g:LX/CTO;

.field public h:LX/CT7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1908465
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908466
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908463
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908464
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908467
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908468
    const v0, 0x7f030f98

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908469
    const v0, 0x7f0d25ba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1908470
    const v0, 0x7f0d25bb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/DatePickerView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->b:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1908471
    const v0, 0x7f0d25bc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/TimePickerView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->c:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1908472
    new-instance v0, LX/CWb;

    invoke-direct {v0, p0}, LX/CWb;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->d:LX/Bm9;

    .line 1908473
    new-instance v0, LX/CWc;

    invoke-direct {v0, p0}, LX/CWc;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->e:LX/BmO;

    .line 1908474
    return-void
.end method


# virtual methods
.method public final a(LX/CTO;LX/CT7;)V
    .locals 5

    .prologue
    .line 1908450
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908451
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v0

    .line 1908452
    if-eqz v0, :cond_0

    :goto_0
    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->f:LX/CSY;

    .line 1908453
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->g:LX/CTO;

    .line 1908454
    iput-object p2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->h:LX/CT7;

    .line 1908455
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->b:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->d:LX/Bm9;

    .line 1908456
    iput-object v1, v0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    .line 1908457
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->b:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    .line 1908458
    iput-wide v2, v0, Lcom/facebook/events/ui/date/DatePickerView;->f:J

    .line 1908459
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->c:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDateTimePickerView;->e:LX/BmO;

    .line 1908460
    iput-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 1908461
    return-void

    .line 1908462
    :cond_0
    new-instance v0, LX/CSY;

    iget-object v1, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v2, p1, LX/CTK;->i:Z

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, v1, v2, v3}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0
.end method
