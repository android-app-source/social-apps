.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final c:Landroid/widget/LinearLayout;

.field public final d:Lcom/facebook/widget/text/BetterTextView;

.field public final e:Ljava/text/SimpleDateFormat;

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908548
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908549
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908490
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908491
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908541
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908542
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1908543
    const v0, 0x7f030f97

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908544
    const v0, 0x7f0d25b9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->c:Landroid/widget/LinearLayout;

    .line 1908545
    const v0, 0x7f0d25b8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1908546
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEEE, MMMM d"

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-direct {v1, v2, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->e:Ljava/text/SimpleDateFormat;

    .line 1908547
    return-void
.end method

.method private a(LX/CTN;LX/CT7;LX/CT6;LX/CT1;Ljava/lang/String;LX/CSY;Ljava/util/Calendar;II)Landroid/view/View$OnClickListener;
    .locals 11

    .prologue
    .line 1908540
    new-instance v0, LX/CWd;

    move-object v1, p0

    move-object/from16 v2, p6

    move-object/from16 v3, p5

    move-object v4, p1

    move/from16 v5, p9

    move-object v6, p2

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object v9, p3

    move-object v10, p4

    invoke-direct/range {v0 .. v10}, LX/CWd;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;LX/CSY;Ljava/lang/String;LX/CTN;ILX/CT7;Ljava/util/Calendar;ILX/CT6;LX/CT1;)V

    return-object v0
.end method

.method private a()Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;
    .locals 4

    .prologue
    .line 1908539
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031658

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->c:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;

    return-object v0
.end method

.method public static a(LX/CTN;LX/CT1;)V
    .locals 3

    .prologue
    .line 1908536
    iget-object v0, p0, LX/CTJ;->p:Ljava/util/HashMap;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_TAP:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1908537
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_TAP:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    iget-object v0, p0, LX/CTJ;->p:Ljava/util/HashMap;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;->ON_TAP:Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CTv;

    iget-object v2, p0, LX/CTJ;->o:Ljava/lang/String;

    invoke-virtual {p1, v1, v0, v2}, LX/CT1;->a(Lcom/facebook/graphql/enums/GraphQLPagesPlatformScreenEvent;LX/CTv;Ljava/lang/String;)V

    .line 1908538
    :cond_0
    return-void
.end method

.method private a(LX/CTN;LX/CT7;LX/CT6;LX/CT1;Ljava/lang/String;LX/CSY;Ljava/util/HashSet;JJ)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CTN;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Storage$FormFieldValuesChangeListener;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$Navigation$NavigationChangeListener;",
            "Lcom/facebook/pages/common/platform/interfaces/PlatformInterfaces$ScreenEvent$ScreenEventHandler;",
            "Ljava/lang/String;",
            "LX/CSY;",
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Long;",
            ">;JJ)V"
        }
    .end annotation

    .prologue
    .line 1908512
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v12

    .line 1908513
    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-virtual {v12}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v2

    div-int/lit16 v10, v2, 0x3e8

    .line 1908514
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v9

    .line 1908515
    new-instance v2, Ljava/util/Date;

    iget-wide v4, p1, LX/CTN;->a:J

    int-to-long v6, v10

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v9, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908516
    const/4 v11, 0x0

    :goto_0
    int-to-long v2, v11

    const-wide/16 v4, 0x7

    move-wide/from16 v0, p8

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_5

    .line 1908517
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a()Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;

    move-result-object v13

    .line 1908518
    const/4 v2, 0x5

    invoke-virtual {v12, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    const/4 v3, 0x5

    invoke-virtual {v9, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    move v3, v2

    .line 1908519
    :goto_1
    int-to-long v4, v11

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 1908520
    int-to-long v6, v11

    cmp-long v2, v6, p10

    if-nez v2, :cond_3

    const/4 v2, 0x1

    move v4, v2

    .line 1908521
    :goto_2
    iget-object v6, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->b:LX/23P;

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v7, 0x7f08009b

    invoke-virtual {v2, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_3
    const/4 v7, 0x0

    invoke-virtual {v6, v2, v7}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v13, v3, v5, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a(ZZLjava/lang/CharSequence;)V

    .line 1908522
    const/4 v2, 0x5

    invoke-virtual {v9, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->setDayNumber(Ljava/lang/String;)V

    .line 1908523
    invoke-virtual {v13, v4, v5}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a(ZZ)V

    .line 1908524
    if-eqz v4, :cond_0

    .line 1908525
    iput v11, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->f:I

    .line 1908526
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->d:Lcom/facebook/widget/text/BetterTextView;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->e:Ljava/text/SimpleDateFormat;

    invoke-virtual {v9}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908527
    :cond_0
    if-eqz v5, :cond_1

    move-object v2, p0

    move-object v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    .line 1908528
    invoke-direct/range {v2 .. v11}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a(LX/CTN;LX/CT7;LX/CT6;LX/CT1;Ljava/lang/String;LX/CSY;Ljava/util/Calendar;II)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v13, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->setTapListener(Landroid/view/View$OnClickListener;)V

    .line 1908529
    :cond_1
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v13}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1908530
    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v9, v2, v3}, Ljava/util/Calendar;->add(II)V

    .line 1908531
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 1908532
    :cond_2
    const/4 v2, 0x0

    move v3, v2

    goto :goto_1

    .line 1908533
    :cond_3
    const/4 v2, 0x0

    move v4, v2

    goto :goto_2

    .line 1908534
    :cond_4
    const/4 v7, 0x7

    const/4 v8, 0x1

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Locale;

    invoke-virtual {v9, v7, v8, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 1908535
    :cond_5
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;LX/0Or;LX/23P;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;",
            "LX/0Or",
            "<",
            "Ljava/util/Locale;",
            ">;",
            "LX/23P;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1908511
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->b:LX/23P;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;

    const/16 v1, 0x1617

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    invoke-static {p0, v1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;LX/0Or;LX/23P;)V

    return-void
.end method


# virtual methods
.method public final a(LX/CTN;LX/CT7;LX/CT6;LX/CT1;)V
    .locals 20
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SimpleDateFormat"
        }
    .end annotation

    .prologue
    .line 1908492
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1908493
    invoke-static/range {p1 .. p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v7

    .line 1908494
    move-object/from16 v0, p1

    iget-object v2, v0, LX/CTJ;->o:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v2

    .line 1908495
    if-eqz v2, :cond_0

    move-object v8, v2

    .line 1908496
    :goto_0
    new-instance v9, Ljava/util/HashSet;

    invoke-direct {v9}, Ljava/util/HashSet;-><init>()V

    .line 1908497
    move-object/from16 v0, p1

    iget-wide v4, v0, LX/CTN;->b:J

    move-object/from16 v0, p1

    iget-wide v10, v0, LX/CTN;->a:J

    sub-long/2addr v4, v10

    const-wide/32 v10, 0x15180

    div-long v10, v4, v10

    .line 1908498
    if-eqz v2, :cond_1

    invoke-virtual {v2, v7}, LX/CSY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-wide v14, v2

    .line 1908499
    :goto_1
    move-object/from16 v0, p1

    iget-wide v2, v0, LX/CTN;->a:J

    sub-long v2, v14, v2

    const-wide/32 v4, 0x15180

    div-long v12, v2, v4

    .line 1908500
    move-object/from16 v0, p1

    iget-object v4, v0, LX/CTN;->d:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 1908501
    move-object/from16 v0, p1

    iget-wide v0, v0, LX/CTN;->a:J

    move-wide/from16 v18, v0

    sub-long v16, v16, v18

    const-wide/32 v18, 0x15180

    div-long v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1908502
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 1908503
    :cond_0
    new-instance v8, LX/CSY;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/CTJ;->o:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-boolean v4, v0, LX/CTK;->i:Z

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v8, v3, v4, v5}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908504
    :cond_1
    move-object/from16 v0, p1

    iget-wide v2, v0, LX/CTN;->c:J

    move-wide v14, v2

    goto :goto_1

    .line 1908505
    :cond_2
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_3
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    .line 1908506
    invoke-direct/range {v2 .. v13}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldDatepickerView;->a(LX/CTN;LX/CT7;LX/CT6;LX/CT1;Ljava/lang/String;LX/CSY;Ljava/util/HashSet;JJ)V

    .line 1908507
    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v7, v2}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908508
    move-object/from16 v0, p1

    iget-object v2, v0, LX/CTJ;->o:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v3, v0, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3, v8}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908509
    return-void

    .line 1908510
    :cond_3
    const-wide/16 v12, -0x1

    goto :goto_3
.end method
