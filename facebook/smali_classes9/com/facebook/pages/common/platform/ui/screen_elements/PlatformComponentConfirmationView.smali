.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910227
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910228
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910229
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910230
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910231
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910232
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f030f92

    invoke-static {v0, v1, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1910233
    const v0, 0x7f0d25a0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910234
    const v0, 0x7f0d25a1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910235
    return-void
.end method


# virtual methods
.method public final a(LX/CTX;)V
    .locals 2

    .prologue
    .line 1910236
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTX;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910237
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentConfirmationView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTX;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910238
    return-void
.end method
