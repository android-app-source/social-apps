.class public Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private e:Landroid/view/View;

.field private f:LX/CWu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910055
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910056
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910046
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910047
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910048
    const v0, 0x7f030fb0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1910049
    const v0, 0x7f0d25e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->c:Landroid/widget/TextView;

    .line 1910050
    const v0, 0x7f0d25ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 1910051
    const v0, 0x7f0d25eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    .line 1910052
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a:LX/5fv;

    .line 1910053
    iput-object v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->b:LX/5fv;

    .line 1910054
    return-void
.end method

.method private static a(II)I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1910036
    if-ltz p0, :cond_0

    if-lez p1, :cond_0

    if-ge p0, p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1910037
    if-ne p1, v1, :cond_1

    .line 1910038
    const v0, 0x7f0214a8

    .line 1910039
    :goto_1
    return v0

    .line 1910040
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1910041
    :cond_1
    if-nez p0, :cond_2

    .line 1910042
    const v0, 0x7f0214a0

    goto :goto_1

    .line 1910043
    :cond_2
    add-int/lit8 v0, p1, -0x1

    if-ne p0, v0, :cond_3

    .line 1910044
    const v0, 0x7f0214a5

    goto :goto_1

    .line 1910045
    :cond_3
    const v0, 0x7f0214a3

    goto :goto_1
.end method

.method private static a(Landroid/view/ViewGroup;)Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;
    .locals 3

    .prologue
    .line 1910035
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03165b

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1910029
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->e:Landroid/view/View;

    if-ne v0, p1, :cond_0

    .line 1910030
    :goto_0
    return-void

    .line 1910031
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    .line 1910032
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->e:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1910033
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1910034
    :cond_1
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->e:Landroid/view/View;

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v0

    check-cast v0, LX/5fv;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a:LX/5fv;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;)V
    .locals 2

    .prologue
    .line 1910025
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Landroid/view/View;)V

    .line 1910026
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->c()V

    .line 1910027
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->setSelection(I)V

    .line 1910028
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;LX/CXT;)V
    .locals 1

    .prologue
    .line 1910020
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Landroid/view/View;)V

    .line 1910021
    invoke-static {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;)V

    .line 1910022
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->f:LX/CWu;

    if-eqz v0, :cond_0

    .line 1910023
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->f:LX/CWu;

    invoke-interface {v0, p2}, LX/CWu;->a(LX/CXT;)V

    .line 1910024
    :cond_0
    return-void
.end method

.method public static b(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;)V
    .locals 2

    .prologue
    .line 1910018
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    invoke-static {v0, v1}, LX/2Na;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1910019
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1910016
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    invoke-static {v0, v1}, LX/2Na;->b(Landroid/content/Context;Landroid/view/View;)V

    .line 1910017
    return-void
.end method


# virtual methods
.method public final a(LX/CXT;LX/CWw;ZLX/0Px;Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CXT;",
            "LX/CWw;",
            "Z",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/facebook/payments/currency/CurrencyAmount;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1909972
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    .line 1909973
    if-eqz p3, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-virtual/range {p4 .. p4}, LX/0Px;->size()I

    move-result v2

    add-int v4, v1, v2

    .line 1909974
    const/4 v1, 0x0

    .line 1909975
    if-eqz p3, :cond_0

    .line 1909976
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-static {v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Landroid/view/ViewGroup;)Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;

    move-result-object v2

    .line 1909977
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f082a9b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;->setTitleOnly(Ljava/lang/String;)V

    .line 1909978
    invoke-virtual/range {p5 .. p5}, Lcom/facebook/payments/currency/CurrencyAmount;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CXT;->a(Ljava/lang/String;)LX/CXT;

    move-result-object v3

    .line 1909979
    new-instance v1, LX/CXO;

    invoke-direct {v1, p0, v2, v3}, LX/CXO;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;LX/CXT;)V

    invoke-virtual {v2, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909980
    const/4 v1, 0x0

    invoke-static {v1, v4}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(II)I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;->setBackgroundResource(I)V

    .line 1909981
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v6, -0x2

    const/4 v7, -0x1

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-direct {v5, v6, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v2, v5}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1909982
    const/4 v1, 0x1

    .line 1909983
    invoke-virtual {p1}, LX/CXT;->a()LX/CXU;

    move-result-object v5

    sget-object v6, LX/CXU;->CASH:LX/CXU;

    if-ne v5, v6, :cond_0

    .line 1909984
    invoke-direct {p0, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Landroid/view/View;)V

    .line 1909985
    invoke-interface {p2, v3}, LX/CWw;->a(LX/CXT;)V

    .line 1909986
    :cond_0
    invoke-virtual/range {p4 .. p4}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v1

    :goto_1
    if-ge v2, v5, :cond_3

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 1909987
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-static {v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Landroid/view/ViewGroup;)Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;

    move-result-object v7

    .line 1909988
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object/from16 v0, p5

    invoke-static {v0, v1}, LX/CXT;->a(Lcom/facebook/payments/currency/CurrencyAmount;Ljava/lang/Integer;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 1909989
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v8, v1}, LX/CXT;->a(Ljava/lang/Integer;Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v8

    .line 1909990
    invoke-virtual {v7, v6, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;->a(ILcom/facebook/payments/currency/CurrencyAmount;)V

    .line 1909991
    new-instance v1, LX/CXP;

    invoke-direct {v1, p0, v7, v8}, LX/CXP;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;LX/CXT;)V

    invoke-virtual {v7, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909992
    invoke-static {v3, v4}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(II)I

    move-result v1

    invoke-virtual {v7, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipSelectableItemView;->setBackgroundResource(I)V

    .line 1909993
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x2

    const/4 v11, -0x1

    const/high16 v12, 0x3f800000    # 1.0f

    invoke-direct {v9, v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v1, v7, v9}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1909994
    add-int/lit8 v3, v3, 0x1

    .line 1909995
    invoke-virtual {p1}, LX/CXT;->a()LX/CXU;

    move-result-object v1

    sget-object v9, LX/CXU;->DEFAULT_PERCENTAGE:LX/CXU;

    if-ne v1, v9, :cond_1

    invoke-virtual {p1}, LX/CXT;->c()LX/0am;

    move-result-object v1

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v6, v1, :cond_1

    .line 1909996
    invoke-direct {p0, v7}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Landroid/view/View;)V

    .line 1909997
    invoke-interface {p2, v8}, LX/CWw;->a(LX/CXT;)V

    .line 1909998
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1909999
    :cond_2
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 1910000
    :cond_3
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    new-instance v2, LX/CXQ;

    invoke-direct {v2, p0}, LX/CXQ;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1910001
    if-eqz p5, :cond_4

    .line 1910002
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    move-object/from16 v0, p5

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->a(Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 1910003
    :cond_4
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    new-instance v2, LX/CXR;

    invoke-direct {v2, p0}, LX/CXR;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1910004
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    new-instance v2, LX/CXS;

    invoke-direct {v2, p0}, LX/CXS;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1910005
    invoke-virtual {p1}, LX/CXT;->a()LX/CXU;

    move-result-object v1

    sget-object v2, LX/CXU;->CUSTOM:LX/CXU;

    if-ne v1, v2, :cond_6

    .line 1910006
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    invoke-direct {p0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->a(Landroid/view/View;)V

    .line 1910007
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    invoke-virtual {v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->getCustomTip()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 1910008
    if-nez v1, :cond_5

    .line 1910009
    invoke-virtual {p1}, LX/CXT;->d()Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    .line 1910010
    :cond_5
    invoke-static {v1}, LX/CXT;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v1

    .line 1910011
    invoke-interface {p2, v1}, LX/CWw;->a(LX/CXT;)V

    .line 1910012
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->setSelected(Z)V

    .line 1910013
    :cond_6
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 1910014
    int-to-float v2, v4

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1910015
    return-void
.end method

.method public setOnTipSelectionChangedListener(LX/CWu;)V
    .locals 2

    .prologue
    .line 1909966
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->f:LX/CWu;

    .line 1909967
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->b:Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->f:LX/CWu;

    .line 1909968
    iput-object v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->g:LX/CWu;

    .line 1909969
    return-void
.end method

.method public setTipTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1909970
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartTipView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909971
    return-void
.end method
