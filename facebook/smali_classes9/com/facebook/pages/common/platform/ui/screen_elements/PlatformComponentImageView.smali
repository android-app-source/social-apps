.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final d:Landroid/widget/FrameLayout$LayoutParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1910330
    const-class v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910328
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910329
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910326
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910327
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1910321
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910322
    const-class v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910323
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1910324
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->d:Landroid/widget/FrameLayout$LayoutParams;

    .line 1910325
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->a:LX/1Ad;

    return-void
.end method


# virtual methods
.method public final a(LX/CTd;)V
    .locals 3

    .prologue
    .line 1910310
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->removeAllViews()V

    .line 1910311
    iget-boolean v0, p1, LX/CTd;->b:Z

    if-eqz v0, :cond_0

    .line 1910312
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->d:Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1910313
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->d:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1ddc

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1910314
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->d:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1910315
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v1, p1, LX/CTd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1910316
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1910317
    return-void

    .line 1910318
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 1910319
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->d:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p1, LX/CTd;->d:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1910320
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentImageView;->d:Landroid/widget/FrameLayout$LayoutParams;

    iget v2, p1, LX/CTd;->c:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->width:I

    goto :goto_0
.end method
