.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910239
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910240
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910241
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910242
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910243
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910244
    const v0, 0x7f030f93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1910245
    const v0, 0x7f0d25a2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910246
    const v0, 0x7f0d25a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910247
    const v0, 0x7f0d25a4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910248
    return-void
.end method


# virtual methods
.method public final a(LX/CTY;)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1910249
    iget-object v0, p1, LX/CTY;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, LX/CTY;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1910250
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p1, LX/CTY;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, LX/CTY;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 1910251
    :goto_0
    iget-object v3, p1, LX/CTY;->c:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1910252
    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v4, p1, LX/CTY;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v1

    .line 1910253
    :goto_1
    iget-object v4, p1, LX/CTY;->d:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1910254
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v4, p1, LX/CTY;->d:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910255
    :goto_2
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910256
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910257
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentContactinfoView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910258
    return-void

    :cond_0
    move v1, v2

    goto :goto_2

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_0
.end method
