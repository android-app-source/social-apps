.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/CTH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/CXd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field public final d:Lcom/facebook/fig/textinput/FigEditText;

.field public final e:Lcom/facebook/widget/listview/BetterListView;

.field public final f:Landroid/view/View;

.field private final g:Landroid/view/View$OnFocusChangeListener;

.field private final h:Landroid/widget/AdapterView$OnItemClickListener;

.field private final i:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909314
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909315
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909316
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909317
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909322
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909323
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->j:Ljava/util/ArrayList;

    .line 1909324
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->k:Z

    .line 1909325
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1909326
    const v0, 0x7f030fa7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909327
    const v0, 0x7f0d25ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1909328
    const v0, 0x7f0d25cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->d:Lcom/facebook/fig/textinput/FigEditText;

    .line 1909329
    const v0, 0x7f0d25d0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->f:Landroid/view/View;

    .line 1909330
    const v0, 0x7f0d25d1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    .line 1909331
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f030fb4

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->j:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->i:Landroid/widget/ArrayAdapter;

    .line 1909332
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->i:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1909333
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->getFocusChangeListener()Landroid/view/View$OnFocusChangeListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->g:Landroid/view/View$OnFocusChangeListener;

    .line 1909334
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->h:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1909335
    return-void
.end method

.method private a(LX/CTo;LX/CT7;LX/CSY;Ljava/lang/String;)Landroid/text/TextWatcher;
    .locals 6

    .prologue
    .line 1909318
    new-instance v0, LX/CWz;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/CWz;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTo;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;)V
    .locals 1

    .prologue
    .line 1909319
    sget-object v0, LX/CTH;->a:LX/0Px;

    move-object v0, v0

    .line 1909320
    invoke-static {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;LX/0Px;)V

    .line 1909321
    return-void
.end method

.method private static a(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;LX/CTH;LX/CXd;)V
    .locals 0

    .prologue
    .line 1909313
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a:LX/CTH;

    iput-object p2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->b:LX/CXd;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;

    new-instance p1, LX/CTH;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-direct {p1, v0, v2}, LX/CTH;-><init>(LX/1Ck;LX/0tX;)V

    move-object v0, p1

    check-cast v0, LX/CTH;

    invoke-static {v1}, LX/CXd;->b(LX/0QB;)LX/CXd;

    move-result-object v1

    check-cast v1, LX/CXd;

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;LX/CTH;LX/CXd;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel$NodesModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1909303
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->j:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 1909304
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1909305
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1909306
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->i:Landroid/widget/ArrayAdapter;

    const v1, -0x6880eca5

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1909307
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->b()V

    .line 1909308
    return-void

    .line 1909309
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1909310
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel$NodesModel;

    .line 1909311
    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/facebook/pages/common/platform/protocol/FetchPageTargetingTypeaheadModels$InstantWorkflowPageTargetingTypeaheadLocationQueryModel$AdsTargetingModel$LocationsModel$NodesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1909312
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 1909282
    iget-boolean v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->k:Z

    if-eqz v0, :cond_0

    .line 1909283
    :goto_0
    return-void

    .line 1909284
    :cond_0
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1909285
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a:LX/CTH;

    .line 1909286
    iget-object v1, v0, LX/CTH;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1909287
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->f:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1909288
    invoke-static {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;)V

    goto :goto_0

    .line 1909289
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1909290
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a:LX/CTH;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CITY:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    new-instance v2, LX/CX1;

    invoke-direct {v2, p0}, LX/CX1;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;)V

    .line 1909291
    iget-object v3, v0, LX/CTH;->b:LX/1Ck;

    sget-object v4, LX/CTG;->FETCH_TYPEAHEAD_RESULTS:LX/CTG;

    .line 1909292
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909293
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1909294
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v8

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v8, :cond_2

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 1909295
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1909296
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 1909297
    :cond_2
    new-instance v5, LX/CUY;

    invoke-direct {v5}, LX/CUY;-><init>()V

    move-object v5, v5

    .line 1909298
    const-string v6, "query"

    invoke-virtual {v5, v6, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v8, "limit"

    const/16 p0, 0x8

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v6, v8, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v8, "types"

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v6, v8, v7}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1909299
    iget-object v6, v0, LX/CTH;->c:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    .line 1909300
    new-instance v6, LX/CTF;

    invoke-direct {v6, v0}, LX/CTF;-><init>(LX/CTH;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v7

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v5, v5

    .line 1909301
    new-instance v6, LX/CTE;

    invoke-direct {v6, v0, v2}, LX/CTE;-><init>(LX/CTH;LX/CX1;)V

    invoke-virtual {v3, v4, v5, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1909302
    goto/16 :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1909279
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getWidth()I

    move-result v1

    const v2, 0x1fffffff

    const/high16 v3, -0x80000000

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/listview/BetterListView;->measure(II)V

    .line 1909280
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getMeasuredHeight()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1909281
    return-void
.end method

.method private getFocusChangeListener()Landroid/view/View$OnFocusChangeListener;
    .locals 1

    .prologue
    .line 1909278
    new-instance v0, LX/CX0;

    invoke-direct {v0, p0}, LX/CX0;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;)V

    return-object v0
.end method

.method private getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 1909277
    new-instance v0, LX/CWy;

    invoke-direct {v0, p0}, LX/CWy;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/CTo;LX/CT7;)V
    .locals 9

    .prologue
    .line 1909263
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909264
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v2

    .line 1909265
    if-eqz v2, :cond_1

    move-object v3, v2

    .line 1909266
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v1

    .line 1909267
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1909268
    iget-object v0, p1, LX/CTo;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1909269
    iget-object v0, p1, LX/CTo;->b:Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909270
    :cond_0
    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    iget-object v6, p1, LX/CTo;->a:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->d:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/lang/String;Lcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1909271
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->d:Lcom/facebook/fig/textinput/FigEditText;

    invoke-direct {p0, p1, p2, v3, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a(LX/CTo;LX/CT7;LX/CSY;Ljava/lang/String;)Landroid/text/TextWatcher;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1909272
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->e:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->h:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1909273
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->d:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->g:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/textinput/FigEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1909274
    invoke-static {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;->a(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextCityTypeaheadView;)V

    .line 1909275
    return-void

    .line 1909276
    :cond_1
    new-instance v3, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v3, v0, v1, v4}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0
.end method
