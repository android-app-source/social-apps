.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final d:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final e:Lcom/facebook/drawee/span/DraweeSpanTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910183
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910185
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910186
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910187
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910188
    const v0, 0x7f030f90

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1910189
    const v0, 0x7f0d259a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910190
    const v0, 0x7f0d259b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910191
    const v0, 0x7f0d259c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910192
    const v0, 0x7f0d259d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->d:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910193
    const v0, 0x7f0d259e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->e:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910194
    return-void
.end method


# virtual methods
.method public final a(LX/CTU;)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1910195
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v3, p1, LX/CTU;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910196
    iget-object v0, p1, LX/CTU;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1910197
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v3, p1, LX/CTU;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    move v0, v1

    .line 1910198
    :goto_0
    iget-object v3, p1, LX/CTU;->c:Ljava/lang/String;

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 1910199
    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v4, p1, LX/CTU;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v1

    .line 1910200
    :goto_1
    iget-object v4, p1, LX/CTU;->e:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p1, LX/CTU;->f:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p1, LX/CTU;->d:Ljava/lang/String;

    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 1910201
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p1, LX/CTU;->e:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1910202
    iget-object v5, p1, LX/CTU;->f:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1910203
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 1910204
    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1910205
    :cond_1
    iget-object v5, p1, LX/CTU;->f:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1910206
    :cond_2
    iget-object v5, p1, LX/CTU;->d:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 1910207
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_3

    .line 1910208
    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1910209
    :cond_3
    iget-object v5, p1, LX/CTU;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1910210
    :cond_4
    iget-object v5, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->d:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    move v4, v1

    .line 1910211
    :goto_2
    iget-object v5, p1, LX/CTU;->g:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1910212
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->e:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v5, p1, LX/CTU;->g:Ljava/lang/String;

    invoke-virtual {v2, v5}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910213
    :goto_3
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910214
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910215
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->d:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910216
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAddressView;->e:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910217
    return-void

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    move v4, v2

    goto :goto_2

    :cond_7
    move v3, v2

    goto/16 :goto_1

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method
