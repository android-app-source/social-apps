.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private e:LX/CXD;

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908806
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908807
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908748
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908749
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908799
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908800
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1908801
    const v0, 0x7f030f9c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908802
    const v0, 0x7f0d25c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908803
    const v0, 0x7f0d25c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908804
    const v0, 0x7f0d25c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 1908805
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method

.method public static c(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1908788
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->g:I

    if-lt p1, v0, :cond_1

    move v1, v2

    :goto_0
    move v4, v3

    .line 1908789
    :goto_1
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->f:I

    if-ge v4, v0, :cond_3

    .line 1908790
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CX7;

    .line 1908791
    if-eqz v1, :cond_2

    .line 1908792
    iget-boolean v5, v0, LX/CX7;->f:Z

    move v5, v5

    .line 1908793
    if-nez v5, :cond_0

    .line 1908794
    invoke-virtual {v0, v3}, LX/CX7;->setEnabled(Z)V

    .line 1908795
    :cond_0
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_1
    move v1, v3

    .line 1908796
    goto :goto_0

    .line 1908797
    :cond_2
    invoke-virtual {v0, v2}, LX/CX7;->setEnabled(Z)V

    goto :goto_2

    .line 1908798
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(LX/CTT;LX/CT7;)V
    .locals 12

    .prologue
    const/4 v10, 0x0

    .line 1908750
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v7

    .line 1908751
    new-instance v0, LX/CXD;

    invoke-direct {v0, p1}, LX/CXD;-><init>(LX/CTT;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->e:LX/CXD;

    .line 1908752
    iget-object v0, p1, LX/CTT;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->f:I

    .line 1908753
    iget v0, p1, LX/CTT;->k:I

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->g:I

    .line 1908754
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v11

    .line 1908755
    if-eqz v11, :cond_0

    move-object v6, v11

    .line 1908756
    :goto_0
    if-nez v11, :cond_1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1908757
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CXD;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1908758
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTT;->b:LX/CU1;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0, v1, v2}, LX/CXD;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    .line 1908759
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    move v3, v10

    .line 1908760
    :goto_2
    iget-object v0, p1, LX/CTT;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 1908761
    new-instance v5, LX/CX7;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v5, v0}, LX/CX7;-><init>(Landroid/content/Context;)V

    .line 1908762
    iget-object v0, p1, LX/CTT;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/util/ArrayList;

    new-instance v0, LX/CWk;

    move-object v1, p0

    move-object v2, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, LX/CWk;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;LX/CTT;ILjava/util/ArrayList;LX/CX7;LX/CSY;Ljava/lang/String;LX/CT7;)V

    invoke-virtual {v5, v9, v0}, LX/CX7;->a(Ljava/util/ArrayList;LX/CWk;)V

    .line 1908763
    invoke-virtual {v5, v10}, LX/CX7;->setChecked(Z)V

    .line 1908764
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 1908765
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1908766
    :cond_0
    new-instance v6, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v6, v0, v1, v2}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908767
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, LX/CSY;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1

    .line 1908768
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->e:LX/CXD;

    invoke-virtual {v0, v7, v11}, LX/CXD;->a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1908769
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->e:LX/CXD;

    invoke-virtual {v1, v0}, LX/CXD;->a(I)Ljava/lang/String;

    move-result-object v3

    move v1, v10

    .line 1908770
    :goto_4
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->f:I

    if-ge v1, v0, :cond_3

    .line 1908771
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CX7;

    .line 1908772
    iget-object v5, v0, LX/CX7;->i:Ljava/util/Set;

    invoke-interface {v5, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    move v5, v5

    .line 1908773
    if-eqz v5, :cond_5

    .line 1908774
    const/4 v5, 0x1

    .line 1908775
    iput-boolean v5, v0, LX/CX7;->f:Z

    .line 1908776
    iget-object v1, v0, LX/CX7;->b:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1908777
    const/4 v1, 0x0

    move v5, v1

    :goto_5
    iget-object v1, v0, LX/CX7;->h:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v5, v1, :cond_4

    .line 1908778
    iget-object v1, v0, LX/CX7;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CU0;

    iget-object v1, v1, LX/CU0;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1908779
    iput v5, v0, LX/CX7;->j:I

    .line 1908780
    iget-object v5, v0, LX/CX7;->d:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    iget-object v1, v0, LX/CX7;->h:Ljava/util/ArrayList;

    iget v8, v0, LX/CX7;->j:I

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CU0;

    invoke-virtual {v5, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(LX/CU0;)V

    .line 1908781
    :cond_4
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1908782
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 1908783
    :cond_6
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->e:LX/CXD;

    invoke-virtual {v0, v7, v11}, LX/CXD;->a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;->c(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorMultiSelectView;I)V

    .line 1908784
    invoke-virtual {v6, v7, v4}, LX/CSY;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1908785
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1, v6}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908786
    return-void

    .line 1908787
    :cond_7
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_5
.end method
