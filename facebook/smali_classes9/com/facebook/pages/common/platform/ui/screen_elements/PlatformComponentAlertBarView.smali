.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;
.super Lcom/facebook/drawee/span/DraweeSpanTextView;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910225
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910226
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910223
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910224
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910220
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/span/DraweeSpanTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910221
    const-class v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910222
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method


# virtual methods
.method public final a(LX/CTW;)V
    .locals 1

    .prologue
    .line 1910218
    iget-object v0, p1, LX/CTW;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentAlertBarView;->setText(Ljava/lang/CharSequence;)V

    .line 1910219
    return-void
.end method
