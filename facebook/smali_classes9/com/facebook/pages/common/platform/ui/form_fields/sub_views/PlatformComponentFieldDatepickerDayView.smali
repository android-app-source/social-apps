.class public Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909852
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909853
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909850
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909851
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909839
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909840
    const v0, 0x7f030fab

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909841
    const v0, 0x7f0d25dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1909842
    const v0, 0x7f0d25de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1909843
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1909844
    const v1, 0x7f0a0098

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->c:I

    .line 1909845
    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->d:I

    .line 1909846
    const v1, 0x7f0a00a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->e:I

    .line 1909847
    const v1, 0x7f0a0095

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->f:I

    .line 1909848
    const v1, 0x7f0a00a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->g:I

    .line 1909849
    return-void
.end method


# virtual methods
.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 1909832
    invoke-virtual {p0, p1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->setSelected(Z)V

    .line 1909833
    if-eqz p1, :cond_0

    .line 1909834
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1909835
    :goto_0
    return-void

    .line 1909836
    :cond_0
    if-eqz p2, :cond_1

    .line 1909837
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->d:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    .line 1909838
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->e:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public final a(ZZLjava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1909820
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909821
    if-nez p2, :cond_0

    .line 1909822
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->e:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1909823
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1909824
    :goto_0
    return-void

    .line 1909825
    :cond_0
    if-eqz p1, :cond_1

    .line 1909826
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->f:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    .line 1909827
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->a:Lcom/facebook/widget/text/BetterTextView;

    iget v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->g:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public setDayNumber(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1909830
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909831
    return-void
.end method

.method public setTapListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1909828
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldDatepickerDayView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909829
    return-void
.end method
