.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentHeadingView;
.super Lcom/facebook/drawee/span/DraweeSpanTextView;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910299
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentHeadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910300
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910301
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentHeadingView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910302
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910303
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/span/DraweeSpanTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910304
    return-void
.end method


# virtual methods
.method public final a(LX/CTc;)V
    .locals 2

    .prologue
    .line 1910305
    iget-object v0, p1, LX/CTc;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentHeadingView;->setText(Ljava/lang/CharSequence;)V

    .line 1910306
    iget-object v0, p1, LX/CTc;->b:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;->ALIGN_CENTER:Lcom/facebook/graphql/enums/GraphQLPagesPlatformTextStyle;

    if-ne v0, v1, :cond_0

    .line 1910307
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentHeadingView;->setGravity(I)V

    .line 1910308
    :goto_0
    return-void

    .line 1910309
    :cond_0
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentHeadingView;->setGravity(I)V

    goto :goto_0
.end method
