.class public Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;
.super LX/6E7;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Lcom/facebook/widget/text/BetterTextView;

.field private final e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1907792
    const-class v0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;

    const-string v1, "pages_public_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1907790
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1907791
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1907775
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1907776
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1907782
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1907783
    const-class v0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1907784
    const v0, 0x7f03095e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1907785
    const v0, 0x7f0d180a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1907786
    const v0, 0x7f0d180b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1907787
    const v0, 0x7f0d180c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1907788
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x4

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1907789
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->a:LX/1Ad;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1907777
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1907778
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1907779
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->a:LX/1Ad;

    sget-object v1, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    .line 1907780
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowsConfirmationProductPurchaseSectionView;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1907781
    return-void
.end method
