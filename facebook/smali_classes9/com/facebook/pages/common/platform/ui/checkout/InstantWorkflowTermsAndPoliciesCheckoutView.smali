.class public Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;
.super LX/6E7;
.source ""


# instance fields
.field public a:LX/1nG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field private c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1907756
    invoke-direct {p0, p1}, LX/6E7;-><init>(Landroid/content/Context;)V

    .line 1907757
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->a()V

    .line 1907758
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1907753
    invoke-direct {p0, p1, p2}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1907754
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->a()V

    .line 1907755
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1907750
    invoke-direct {p0, p1, p2, p3}, LX/6E7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1907751
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->a()V

    .line 1907752
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1907759
    const-class v0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1907760
    const v0, 0x7f03095d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1907761
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->setOrientation(I)V

    .line 1907762
    const v0, 0x7f0d0ebb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1907763
    const v0, 0x7f0d0ebc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1907764
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;

    invoke-static {v0}, LX/1nG;->a(LX/0QB;)LX/1nG;

    move-result-object v0

    check-cast v0, LX/1nG;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->a:LX/1nG;

    return-void
.end method


# virtual methods
.method public setInfoMessage(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1907745
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1907746
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1907747
    :goto_0
    return-void

    .line 1907748
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1907749
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setTermsAndPolicies(LX/3Ab;)V
    .locals 2

    .prologue
    .line 1907743
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    new-instance v1, LX/CWC;

    invoke-direct {v1, p0}, LX/CWC;-><init>(Lcom/facebook/pages/common/platform/ui/checkout/InstantWorkflowTermsAndPoliciesCheckoutView;)V

    invoke-virtual {v0, p1, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->a(LX/3Ab;LX/7wC;)V

    .line 1907744
    return-void
.end method
