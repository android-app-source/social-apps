.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field public final b:Lcom/facebook/fig/textinput/FigEditText;

.field private final c:I

.field public d:LX/CXF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908936
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908937
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908938
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908939
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908940
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908941
    const v0, 0x7f030fa4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908942
    const v0, 0x7f0d25c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908943
    const v0, 0x7f0d25cd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->b:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908944
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->c:I

    .line 1908945
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;LX/CSY;Ljava/lang/String;ILX/CT7;LX/CTn;)V
    .locals 4

    .prologue
    .line 1908946
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->d:LX/CXF;

    invoke-virtual {v3, p3}, LX/CXF;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, p2, v0}, LX/CSY;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1908947
    iget-object v0, p5, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p5, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p4, v0, v1, p1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908948
    return-void
.end method


# virtual methods
.method public final a(LX/CTn;LX/CT7;LX/CT1;)V
    .locals 11

    .prologue
    .line 1908949
    new-instance v0, LX/CXF;

    invoke-direct {v0, p1}, LX/CXF;-><init>(LX/CTn;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->d:LX/CXF;

    .line 1908950
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v8

    .line 1908951
    if-eqz v8, :cond_0

    move-object v3, v8

    .line 1908952
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v4

    .line 1908953
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CXF;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1908954
    new-instance v9, LX/34b;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v9, v0}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 1908955
    const/4 v0, 0x1

    .line 1908956
    iput-boolean v0, v9, LX/34b;->d:Z

    .line 1908957
    iget-object v0, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-virtual {v9, v0}, LX/34b;->a(Ljava/lang/String;)V

    .line 1908958
    const/4 v2, 0x0

    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->d:LX/CXF;

    invoke-virtual {v0}, LX/CXF;->a()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1908959
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->d:LX/CXF;

    invoke-virtual {v0, v2}, LX/CXF;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v10

    .line 1908960
    new-instance v0, LX/CWn;

    move-object v1, p0

    move-object v5, p2

    move-object v6, p1

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, LX/CWn;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;ILX/CSY;Ljava/lang/String;LX/CT7;LX/CTn;LX/CT1;)V

    invoke-virtual {v10, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1908961
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1908962
    :cond_0
    new-instance v3, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v3, v0, v1, v2}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908963
    :cond_1
    new-instance v0, LX/3Af;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 1908964
    invoke-virtual {v0, v9}, LX/3Af;->a(LX/1OM;)V

    .line 1908965
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->b:Lcom/facebook/fig/textinput/FigEditText;

    new-instance v2, LX/CWo;

    invoke-direct {v2, p0, v0}, LX/CWo;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;LX/3Af;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908966
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->d:LX/CXF;

    invoke-virtual {v0, v4, v8}, LX/CXF;->a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 1908967
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->b:Lcom/facebook/fig/textinput/FigEditText;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->d:LX/CXF;

    invoke-virtual {v2, v5}, LX/CXF;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    move-object v2, p0

    move-object v6, p2

    move-object v7, p1

    .line 1908968
    invoke-static/range {v2 .. v7}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;->a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectDropdownView;LX/CSY;Ljava/lang/String;ILX/CT7;LX/CTn;)V

    goto :goto_2

    .line 1908969
    :cond_2
    return-void
.end method
