.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final d:Landroid/widget/LinearLayout;

.field private e:LX/CXD;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908642
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908643
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908644
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908645
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908646
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908647
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1908648
    const v0, 0x7f030f9b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908649
    const v0, 0x7f0d25c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908650
    const v0, 0x7f0d25c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908651
    const v0, 0x7f0d25c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->d:Landroid/widget/LinearLayout;

    .line 1908652
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1908653
    if-lt p2, p1, :cond_1

    move v1, v2

    .line 1908654
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1908655
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CX3;

    .line 1908656
    invoke-virtual {v0}, LX/CX3;->isChecked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1908657
    invoke-virtual {v0, v2}, LX/CX3;->setEnabled(Z)V

    .line 1908658
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1908659
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1908660
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CX3;

    .line 1908661
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/CX3;->setEnabled(Z)V

    .line 1908662
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1908663
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(LX/CTS;LX/CT7;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1908664
    new-instance v0, LX/CXD;

    invoke-direct {v0, p1}, LX/CXD;-><init>(LX/CTS;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->e:LX/CXD;

    .line 1908665
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v9

    .line 1908666
    if-eqz v9, :cond_0

    move-object v6, v9

    .line 1908667
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v7

    .line 1908668
    if-nez v9, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1908669
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CXD;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1908670
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTS;->b:LX/CU1;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0, v1, v2}, LX/CXD;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    .line 1908671
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v10, v11

    .line 1908672
    :goto_2
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->e:LX/CXD;

    invoke-virtual {v0}, LX/CXD;->a()I

    move-result v0

    if-ge v10, v0, :cond_2

    .line 1908673
    new-instance v2, LX/CX3;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, LX/CX3;-><init>(Landroid/content/Context;)V

    .line 1908674
    invoke-virtual {v2, v10}, LX/CX3;->setId(I)V

    .line 1908675
    iget-object v0, p1, LX/CTS;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU0;

    .line 1908676
    iget-object v1, v2, LX/CX3;->c:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    invoke-virtual {v1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(LX/CU0;)V

    .line 1908677
    invoke-virtual {v2, v11}, LX/CX3;->setChecked(Z)V

    .line 1908678
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->e:LX/CXD;

    invoke-virtual {v0, v10}, LX/CXD;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 1908679
    new-instance v0, LX/CWh;

    move-object v1, p0

    move-object v5, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, LX/CWh;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;LX/CX3;Ljava/util/ArrayList;Ljava/lang/String;LX/CTS;LX/CSY;Ljava/lang/String;LX/CT7;)V

    invoke-virtual {v2, v0}, LX/CX3;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908680
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1908681
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_2

    .line 1908682
    :cond_0
    new-instance v6, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v6, v0, v1, v2}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908683
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, LX/CSY;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1

    .line 1908684
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->e:LX/CXD;

    invoke-virtual {v0, v7, v9}, LX/CXD;->a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1908685
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CX3;

    .line 1908686
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/CX3;->setChecked(Z)V

    .line 1908687
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->e:LX/CXD;

    invoke-virtual {v0, v2}, LX/CXD;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1908688
    :cond_3
    iget v0, p1, LX/CTS;->d:I

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;->a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionMultiView;II)V

    .line 1908689
    invoke-virtual {v6, v7, v3}, LX/CSY;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1908690
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1, v6}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908691
    return-void
.end method
