.class public Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;

.field private final d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909874
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909875
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909867
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909868
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1909869
    const v0, 0x7f030fac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909870
    const v0, 0x7f0d25df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1909871
    const v0, 0x7f0d25e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1909872
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->d:Landroid/content/res/Resources;

    .line 1909873
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v0

    check-cast v0, LX/5fv;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->a:LX/5fv;

    return-void
.end method


# virtual methods
.method public setTitle(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1909865
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909866
    return-void
.end method

.method public setValue(Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 2

    .prologue
    .line 1909863
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->a:LX/5fv;

    invoke-virtual {v1, p1}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909864
    return-void
.end method

.method public setValueStyle(LX/CXJ;)V
    .locals 4

    .prologue
    .line 1909860
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentFieldShoppingCartFeesItemView;->d:Landroid/content/res/Resources;

    sget-object v0, LX/CXJ;->TOTAL_FEE:LX/CXJ;

    if-ne p1, v0, :cond_0

    const v0, 0x7f0b0057

    :goto_0
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextSize(IF)V

    .line 1909861
    return-void

    .line 1909862
    :cond_0
    const v0, 0x7f0b0050

    goto :goto_0
.end method
