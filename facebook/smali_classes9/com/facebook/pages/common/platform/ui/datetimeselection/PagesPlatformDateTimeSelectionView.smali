.class public Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/CWP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/LinearLayout;

.field private final d:[LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "LX/0zw",
            "<",
            "Landroid/widget/LinearLayout;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/fbui/glyph/GlyphView;

.field private final f:Lcom/facebook/fbui/glyph/GlyphView;

.field private final g:Landroid/widget/LinearLayout;

.field private final h:Landroid/widget/TextView;

.field public i:LX/CWW;

.field private final j:Landroid/view/View$OnClickListener;

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:Landroid/view/View$OnClickListener;

.field private final m:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908294
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908295
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908292
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908293
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908269
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908270
    const-class v0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1908271
    const v0, 0x7f030fd2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908272
    const v0, 0x7f0d0843

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->b:Landroid/widget/TextView;

    .line 1908273
    const v0, 0x7f0d0844

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->c:Landroid/widget/LinearLayout;

    .line 1908274
    const/4 v0, 0x6

    new-array v0, v0, [LX/0zw;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    .line 1908275
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    const/4 v2, 0x0

    new-instance v3, LX/0zw;

    const v0, 0x7f0d262d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    aput-object v3, v1, v2

    .line 1908276
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    const/4 v2, 0x1

    new-instance v3, LX/0zw;

    const v0, 0x7f0d262e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    aput-object v3, v1, v2

    .line 1908277
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    const/4 v2, 0x2

    new-instance v3, LX/0zw;

    const v0, 0x7f0d262f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    aput-object v3, v1, v2

    .line 1908278
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    const/4 v2, 0x3

    new-instance v3, LX/0zw;

    const v0, 0x7f0d2630

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    aput-object v3, v1, v2

    .line 1908279
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    const/4 v2, 0x4

    new-instance v3, LX/0zw;

    const v0, 0x7f0d2631

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    aput-object v3, v1, v2

    .line 1908280
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    const/4 v2, 0x5

    new-instance v3, LX/0zw;

    const v0, 0x7f0d2632

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v3, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    aput-object v3, v1, v2

    .line 1908281
    const v0, 0x7f0d262c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1908282
    const v0, 0x7f0d0602

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1908283
    const v0, 0x7f0d2634

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->g:Landroid/widget/LinearLayout;

    .line 1908284
    const v0, 0x7f0d2633

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->h:Landroid/widget/TextView;

    .line 1908285
    new-instance v0, LX/CWR;

    invoke-direct {v0, p0}, LX/CWR;-><init>(Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->j:Landroid/view/View$OnClickListener;

    .line 1908286
    new-instance v0, LX/CWS;

    invoke-direct {v0, p0}, LX/CWS;-><init>(Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->k:Landroid/view/View$OnClickListener;

    .line 1908287
    new-instance v0, LX/CWT;

    invoke-direct {v0, p0}, LX/CWT;-><init>(Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->l:Landroid/view/View$OnClickListener;

    .line 1908288
    new-instance v0, LX/CWU;

    invoke-direct {v0, p0}, LX/CWU;-><init>(Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->m:Landroid/view/View$OnClickListener;

    .line 1908289
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908290
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908291
    return-void
.end method

.method public static a(Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;)V
    .locals 8

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 1908257
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->b:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v3, v3, LX/CWW;->a:LX/CWF;

    iget-object v3, v3, LX/CWF;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v3, v1

    .line 1908258
    :goto_0
    int-to-long v4, v3

    const-wide/16 v6, 0x7

    cmp-long v0, v4, v6

    if-gez v0, :cond_0

    .line 1908259
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1908260
    iget-object v4, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v4, v4, LX/CWW;->a:LX/CWF;

    iget-object v4, v4, LX/CWF;->a:[Ljava/lang/String;

    aget-object v4, v4, v3

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908261
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 1908262
    :cond_0
    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v0, v0, LX/CWW;->a:LX/CWF;

    iget-boolean v0, v0, LX/CWF;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1908263
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v3, v3, LX/CWW;->a:LX/CWF;

    iget-boolean v3, v3, LX/CWF;->g:Z

    if-eqz v3, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1908264
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->b()V

    .line 1908265
    invoke-direct {p0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->c()V

    .line 1908266
    return-void

    :cond_1
    move v0, v2

    .line 1908267
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1908268
    goto :goto_2
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;

    new-instance v3, LX/CWP;

    const/16 v1, 0x1617

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v1, 0x259

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    new-instance v1, LX/CWJ;

    const/16 v2, 0x2b8b

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p1, 0x2b8a

    invoke-static {v0, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    invoke-direct {v1, v2, p1}, LX/CWJ;-><init>(LX/0Ot;LX/0Ot;)V

    move-object v1, v1

    check-cast v1, LX/CWJ;

    const-class v2, Landroid/content/Context;

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-direct {v3, v4, v5, v1, v2}, LX/CWP;-><init>(LX/0Or;LX/0Ot;LX/CWJ;Landroid/content/Context;)V

    move-object v0, v3

    check-cast v0, LX/CWP;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->a:LX/CWP;

    return-void
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1908224
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v0, v0, LX/CWW;->a:LX/CWF;

    iget-object v0, v0, LX/CWF;->b:[Ljava/lang/String;

    array-length v3, v0

    .line 1908225
    int-to-float v0, v3

    const/high16 v1, 0x40e00000    # 7.0f

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v4, v0

    move v1, v2

    .line 1908226
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 1908227
    if-ge v1, v4, :cond_0

    .line 1908228
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    aget-object v0, v0, v1

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1908229
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1908230
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    aget-object v0, v0, v1

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_1

    :cond_1
    move v0, v2

    .line 1908231
    :goto_2
    if-ge v0, v3, :cond_2

    .line 1908232
    invoke-direct {p0, v0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->c(I)Landroid/widget/TextView;

    move-result-object v1

    .line 1908233
    iget-object v4, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v4, v4, LX/CWW;->a:LX/CWF;

    iget-object v4, v4, LX/CWF;->b:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908234
    iget-object v4, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v4, v4, LX/CWW;->a:LX/CWF;

    iget-object v4, v4, LX/CWF;->c:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1908235
    sget-object v4, LX/CWV;->a:[I

    iget-object v5, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v5, v5, LX/CWW;->a:LX/CWF;

    iget-object v5, v5, LX/CWF;->d:[LX/CWE;

    aget-object v5, v5, v0

    invoke-virtual {v5}, LX/CWE;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1908236
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1908237
    :pswitch_0
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1908238
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1908239
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0097

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1908240
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 1908241
    :pswitch_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1908242
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1908243
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00ab

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1908244
    iget-object v4, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 1908245
    :pswitch_2
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1908246
    sget-object v4, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1908247
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a00a4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1908248
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 1908249
    :cond_2
    add-int/lit8 v0, v3, -0x1

    .line 1908250
    :goto_4
    add-int/lit8 v1, v0, 0x1

    rem-int/lit8 v1, v1, 0x7

    if-eqz v1, :cond_3

    .line 1908251
    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v1}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->c(I)Landroid/widget/TextView;

    move-result-object v1

    .line 1908252
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908253
    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908254
    add-int/lit8 v0, v0, 0x1

    .line 1908255
    goto :goto_4

    .line 1908256
    :cond_3
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private c(I)Landroid/widget/TextView;
    .locals 3

    .prologue
    .line 1908220
    div-int/lit8 v0, p1, 0x7

    .line 1908221
    rem-int/lit8 v1, p1, 0x7

    .line 1908222
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->d:[LX/0zw;

    aget-object v0, v2, v0

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1908223
    const v1, 0x7f0d2635

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1908158
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1908159
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-boolean v1, v1, LX/CWW;->b:Z

    if-nez v1, :cond_1

    .line 1908160
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1908161
    :cond_0
    return-void

    .line 1908162
    :cond_1
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1908163
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->h:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v2, v2, LX/CWW;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908164
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v1, v1, LX/CWW;->d:LX/0Px;

    if-eqz v1, :cond_0

    .line 1908165
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    iget-object v2, v1, LX/CWW;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CWQ;

    .line 1908166
    new-instance v4, Lcom/facebook/fig/listitem/FigListItem;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 1908167
    iget-object v5, v0, LX/CWQ;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 1908168
    iget-object v5, v0, LX/CWQ;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 1908169
    iget-object v0, v0, LX/CWQ;->c:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTag(Ljava/lang/Object;)V

    .line 1908170
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v0}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908171
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1908172
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CTQ;LX/CT7;LX/CT6;)V
    .locals 10

    .prologue
    .line 1908173
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->a:LX/CWP;

    const/4 v9, 0x0

    .line 1908174
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1908175
    iget-object v1, p1, LX/CTQ;->b:Ljava/util/TreeSet;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1908176
    iget-object v1, v0, LX/CWP;->c:LX/CWJ;

    iget-boolean v2, p1, LX/CTQ;->a:Z

    .line 1908177
    if-eqz v2, :cond_8

    .line 1908178
    iget-object v3, v1, LX/CWJ;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CWI;

    .line 1908179
    :goto_0
    move-object v1, v3

    .line 1908180
    iput-object v1, v0, LX/CWP;->n:LX/CWI;

    .line 1908181
    iput-object p2, v0, LX/CWP;->o:LX/CT7;

    .line 1908182
    iput-object p3, v0, LX/CWP;->p:LX/CT6;

    .line 1908183
    iput-object p1, v0, LX/CWP;->q:LX/CTQ;

    .line 1908184
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/CWP;->r:Ljava/lang/String;

    .line 1908185
    iget-object v1, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v1, v2}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v3

    .line 1908186
    if-eqz v3, :cond_1

    move-object v1, v3

    :goto_1
    iput-object v1, v0, LX/CWP;->s:LX/CSY;

    .line 1908187
    iget-object v1, v0, LX/CWP;->l:Ljava/util/TreeMap;

    invoke-virtual {v1}, Ljava/util/TreeMap;->clear()V

    .line 1908188
    iget-object v1, p1, LX/CTQ;->b:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 1908189
    iget-object v1, v0, LX/CWP;->g:Ljava/util/Calendar;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908190
    iget-object v1, v0, LX/CWP;->h:Ljava/util/Calendar;

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908191
    :cond_0
    new-instance v1, LX/CWX;

    invoke-direct {v1}, LX/CWX;-><init>()V

    .line 1908192
    iget-object v2, v0, LX/CWP;->j:Ljava/util/Calendar;

    iget-object v3, v0, LX/CWP;->n:LX/CWI;

    iget-object v4, v0, LX/CWP;->g:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    iget-object v5, v0, LX/CWP;->k:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-interface {v3, v4, v5}, LX/CWI;->a(Ljava/util/Date;Ljava/util/Date;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908193
    iget-object v2, v0, LX/CWP;->n:LX/CWI;

    iget-object v3, v0, LX/CWP;->j:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    iget-object v4, v0, LX/CWP;->l:Ljava/util/TreeMap;

    iget-object v5, v0, LX/CWP;->g:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    iget-object v6, v0, LX/CWP;->h:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v6

    invoke-interface {v2, v3, v4, v5, v6}, LX/CWI;->a(Ljava/util/Date;Ljava/util/TreeMap;Ljava/util/Date;Ljava/util/Date;)LX/CWF;

    move-result-object v2

    .line 1908194
    iput-object v2, v1, LX/CWX;->a:LX/CWF;

    .line 1908195
    iget-object v2, v0, LX/CWP;->k:Ljava/util/Calendar;

    if-eqz v2, :cond_7

    .line 1908196
    invoke-virtual {v1}, LX/CWX;->a()LX/CWW;

    move-result-object v1

    iget-object v2, v0, LX/CWP;->n:LX/CWI;

    iget-object v3, v0, LX/CWP;->j:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    iget-object v4, v0, LX/CWP;->k:Ljava/util/Calendar;

    invoke-virtual {v4}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/CWI;->b(Ljava/util/Date;Ljava/util/Date;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/CWP;->a(LX/CWW;Ljava/lang/String;)LX/CWW;

    move-result-object v1

    .line 1908197
    :goto_2
    move-object v0, v1

    .line 1908198
    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->i:LX/CWW;

    .line 1908199
    invoke-static {p0}, Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;->a(Lcom/facebook/pages/common/platform/ui/datetimeselection/PagesPlatformDateTimeSelectionView;)V

    .line 1908200
    return-void

    .line 1908201
    :cond_1
    new-instance v1, LX/CSY;

    iget-object v2, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v4, p1, LX/CTK;->i:Z

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v1, v2, v4, v5}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto/16 :goto_1

    .line 1908202
    :cond_2
    iget-object v1, p1, LX/CTQ;->b:Ljava/util/TreeSet;

    invoke-virtual {v1}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CU5;

    .line 1908203
    iget-object v2, v0, LX/CWP;->i:Ljava/util/Calendar;

    iget-object v5, v1, LX/CU5;->b:LX/CTu;

    iget-wide v5, v5, LX/CTu;->a:J

    const-wide/16 v7, 0x3e8

    mul-long/2addr v5, v7

    invoke-virtual {v2, v5, v6}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1908204
    iget-object v2, v0, LX/CWP;->i:Ljava/util/Calendar;

    const/16 v5, 0xa

    invoke-virtual {v2, v5, v9}, Ljava/util/Calendar;->set(II)V

    .line 1908205
    iget-object v2, v0, LX/CWP;->i:Ljava/util/Calendar;

    const/16 v5, 0xc

    invoke-virtual {v2, v5, v9}, Ljava/util/Calendar;->set(II)V

    .line 1908206
    iget-object v2, v0, LX/CWP;->i:Ljava/util/Calendar;

    const/16 v5, 0xd

    invoke-virtual {v2, v5, v9}, Ljava/util/Calendar;->set(II)V

    .line 1908207
    iget-object v2, v0, LX/CWP;->i:Ljava/util/Calendar;

    const/16 v5, 0xe

    invoke-virtual {v2, v5, v9}, Ljava/util/Calendar;->set(II)V

    .line 1908208
    iget-object v2, v0, LX/CWP;->g:Ljava/util/Calendar;

    iget-object v5, v0, LX/CWP;->i:Ljava/util/Calendar;

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->after(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1908209
    iget-object v2, v0, LX/CWP;->g:Ljava/util/Calendar;

    iget-object v5, v0, LX/CWP;->i:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908210
    :cond_3
    iget-object v2, v0, LX/CWP;->h:Ljava/util/Calendar;

    iget-object v5, v0, LX/CWP;->i:Ljava/util/Calendar;

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1908211
    iget-object v2, v0, LX/CWP;->h:Ljava/util/Calendar;

    iget-object v5, v0, LX/CWP;->i:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1908212
    :cond_4
    iget-object v2, v0, LX/CWP;->d:Ljava/text/SimpleDateFormat;

    iget-object v5, v0, LX/CWP;->i:Ljava/util/Calendar;

    invoke-virtual {v5}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    .line 1908213
    iget-object v5, v0, LX/CWP;->l:Ljava/util/TreeMap;

    invoke-virtual {v5, v2}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1908214
    iget-object v5, v0, LX/CWP;->l:Ljava/util/TreeMap;

    invoke-virtual {v5, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1908215
    :goto_4
    if-eqz v3, :cond_6

    iget-object v2, v0, LX/CWP;->s:LX/CSY;

    iget-object v5, v0, LX/CWP;->r:Ljava/lang/String;

    invoke-virtual {v2, v5}, LX/CSY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, LX/CU5;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1908216
    iget-object v1, v0, LX/CWP;->k:Ljava/util/Calendar;

    iget-object v2, v0, LX/CWP;->i:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    goto/16 :goto_3

    .line 1908217
    :cond_5
    iget-object v5, v0, LX/CWP;->l:Ljava/util/TreeMap;

    new-instance v6, Ljava/util/ArrayList;

    const/4 v7, 0x1

    new-array v7, v7, [LX/CU5;

    aput-object v1, v7, v9

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v5, v2, v6}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 1908218
    :cond_6
    iget-object v1, v0, LX/CWP;->k:Ljava/util/Calendar;

    iget-object v2, v0, LX/CWP;->g:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    goto/16 :goto_3

    .line 1908219
    :cond_7
    invoke-virtual {v1}, LX/CWX;->a()LX/CWW;

    move-result-object v1

    goto/16 :goto_2

    :cond_8
    iget-object v3, v1, LX/CWJ;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/CWI;

    goto/16 :goto_0
.end method
