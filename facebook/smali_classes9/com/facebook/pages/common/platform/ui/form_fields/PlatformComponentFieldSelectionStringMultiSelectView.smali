.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final b:Landroid/widget/LinearLayout;

.field private final c:I

.field private d:LX/CXF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908880
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908881
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908882
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908883
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908884
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908885
    const v0, 0x7f030fa3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908886
    const v0, 0x7f0d25c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908887
    const v0, 0x7f0d25c3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->b:Landroid/widget/LinearLayout;

    .line 1908888
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->c:I

    .line 1908889
    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1908890
    if-lt p2, p1, :cond_1

    move v1, v2

    .line 1908891
    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1908892
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    .line 1908893
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1908894
    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbCheckBox;->setEnabled(Z)V

    .line 1908895
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1908896
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1908897
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    .line 1908898
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbCheckBox;->setEnabled(Z)V

    .line 1908899
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1908900
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(LX/CTn;LX/CT7;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 1908901
    new-instance v0, LX/CXF;

    invoke-direct {v0, p1}, LX/CXF;-><init>(LX/CTn;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->d:LX/CXF;

    .line 1908902
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v9

    .line 1908903
    if-eqz v9, :cond_0

    move-object v6, v9

    .line 1908904
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v7

    .line 1908905
    if-nez v9, :cond_1

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1908906
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CXF;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1908907
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v10, v11

    .line 1908908
    :goto_2
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->d:LX/CXF;

    invoke-virtual {v0}, LX/CXF;->a()I

    move-result v0

    if-ge v10, v0, :cond_2

    .line 1908909
    new-instance v2, Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/facebook/resources/ui/FbCheckBox;-><init>(Landroid/content/Context;)V

    .line 1908910
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->d:LX/CXF;

    invoke-virtual {v0, v10}, LX/CXF;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbCheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1908911
    invoke-virtual {v2, v10}, Lcom/facebook/resources/ui/FbCheckBox;->setId(I)V

    .line 1908912
    iget v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->c:I

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbCheckBox;->setTextColor(I)V

    .line 1908913
    invoke-virtual {v2, v11}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 1908914
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->d:LX/CXF;

    invoke-virtual {v0, v10}, LX/CXF;->a(I)Ljava/lang/String;

    move-result-object v4

    .line 1908915
    new-instance v0, LX/CWm;

    move-object v1, p0

    move-object v5, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, LX/CWm;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;Lcom/facebook/resources/ui/FbCheckBox;Ljava/util/ArrayList;Ljava/lang/String;LX/CTn;LX/CSY;Ljava/lang/String;LX/CT7;)V

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbCheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908916
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1908917
    add-int/lit8 v0, v10, 0x1

    move v10, v0

    goto :goto_2

    .line 1908918
    :cond_0
    new-instance v6, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v6, v0, v1, v2}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908919
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v9, v7}, LX/CSY;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1

    .line 1908920
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->d:LX/CXF;

    invoke-virtual {v0, v7, v9}, LX/CXF;->a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;

    move-result-object v1

    .line 1908921
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1908922
    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckBox;

    .line 1908923
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    goto :goto_3

    .line 1908924
    :cond_3
    iget v0, p1, LX/CTn;->c:I

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {p0, v0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;->a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringMultiSelectView;II)V

    .line 1908925
    return-void
.end method
