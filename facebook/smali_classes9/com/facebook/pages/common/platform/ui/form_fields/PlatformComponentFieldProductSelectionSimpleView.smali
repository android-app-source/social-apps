.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

.field public e:LX/CXD;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908700
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908701
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908702
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908703
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908704
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908705
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1908706
    const v0, 0x7f030f9e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908707
    const v0, 0x7f0d25c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908708
    const v0, 0x7f0d25c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908709
    const v0, 0x7f0d25c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 1908710
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method


# virtual methods
.method public final a(LX/CTS;LX/CT7;LX/CT1;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 1908711
    new-instance v0, LX/CXD;

    invoke-direct {v0, p1}, LX/CXD;-><init>(LX/CTS;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->e:LX/CXD;

    .line 1908712
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v6

    .line 1908713
    new-instance v2, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v2, v0, v1, v3}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    .line 1908714
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v3

    .line 1908715
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CXD;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1908716
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTS;->b:LX/CU1;

    iget-object v4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0, v1, v4}, LX/CXD;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    .line 1908717
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->removeAllViews()V

    .line 1908718
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    .line 1908719
    iput-object v5, v0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 1908720
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0, v5}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 1908721
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a()V

    .line 1908722
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->e:LX/CXD;

    invoke-virtual {v0}, LX/CXD;->a()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1908723
    new-instance v4, LX/CX8;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v4, v0}, LX/CX8;-><init>(Landroid/content/Context;)V

    .line 1908724
    invoke-virtual {v4, v1}, LX/CX8;->setId(I)V

    .line 1908725
    iget-object v0, p1, LX/CTS;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CU0;

    .line 1908726
    iget-object v5, v4, LX/CX8;->e:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    invoke-virtual {v5, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(LX/CU0;)V

    .line 1908727
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, LX/CX8;->setVisibility(I)V

    .line 1908728
    new-instance v0, LX/CWi;

    invoke-direct {v0, p0, p1, p3}, LX/CWi;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;LX/CTS;LX/CT1;)V

    invoke-virtual {v4, v0}, LX/CX8;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908729
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v0, v4}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->addView(Landroid/view/View;)V

    .line 1908730
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1908731
    :cond_0
    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    new-instance v0, LX/CWj;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/CWj;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTS;)V

    .line 1908732
    iput-object v0, v7, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 1908733
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->e:LX/CXD;

    invoke-virtual {v0, v3, v6}, LX/CXD;->a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1908734
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionSimpleView;->d:Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;

    invoke-virtual {v2, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    goto :goto_1

    .line 1908735
    :cond_1
    return-void
.end method
