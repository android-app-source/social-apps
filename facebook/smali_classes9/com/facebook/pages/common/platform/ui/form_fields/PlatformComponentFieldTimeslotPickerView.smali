.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final d:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909406
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909407
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909404
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909405
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909360
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909361
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1909362
    const v0, 0x7f030fa8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909363
    const v0, 0x7f0d25d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1909364
    const v0, 0x7f0d25d3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1909365
    const v0, 0x7f0d25d4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->d:Landroid/widget/LinearLayout;

    .line 1909366
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method


# virtual methods
.method public final a(LX/CTr;LX/CT7;LX/CT6;)V
    .locals 19

    .prologue
    .line 1909367
    new-instance v3, LX/CSY;

    move-object/from16 v0, p1

    iget-object v1, v0, LX/CTJ;->o:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-boolean v2, v0, LX/CTK;->i:Z

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v3, v1, v2, v4}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    .line 1909368
    invoke-static/range {p1 .. p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v4

    .line 1909369
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/CTr;->a:LX/CU1;

    invoke-virtual {v1, v2, v5}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1909370
    move-object/from16 v0, p1

    iget-object v1, v0, LX/CTr;->b:LX/CU1;

    if-eqz v1, :cond_1

    move-object/from16 v0, p1

    iget-object v1, v0, LX/CTr;->b:LX/CU1;

    iget-object v1, v1, LX/CU1;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1909371
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    move-object/from16 v0, p1

    iget-object v5, v0, LX/CTr;->b:LX/CU1;

    invoke-virtual {v1, v2, v5}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1909372
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1909373
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1909374
    move-object/from16 v0, p1

    iget-object v15, v0, LX/CTr;->c:Ljava/util/ArrayList;

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v16

    const/4 v1, 0x0

    move v14, v1

    :goto_1
    move/from16 v0, v16

    if-ge v14, v0, :cond_5

    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CTq;

    .line 1909375
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v5, 0x7f030fb2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->d:Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    invoke-virtual {v2, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 1909376
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1909377
    const v2, 0x7f0d25ed

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1909378
    const v6, 0x7f0d25ee

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    move-object v9, v5

    check-cast v9, Landroid/widget/LinearLayout;

    .line 1909379
    iget-object v5, v1, LX/CTq;->a:LX/CU1;

    if-eqz v5, :cond_2

    iget-object v5, v1, LX/CTq;->a:LX/CU1;

    iget-object v5, v5, LX/CU1;->a:Ljava/lang/String;

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1909380
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iget-object v6, v1, LX/CTq;->a:LX/CU1;

    invoke-virtual {v5, v2, v6}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1909381
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1909382
    :goto_2
    invoke-virtual {v9}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1909383
    const/4 v2, 0x0

    .line 1909384
    const/4 v5, 0x0

    .line 1909385
    iget-object v0, v1, LX/CTq;->b:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/4 v1, 0x0

    move v12, v1

    move v6, v2

    :goto_3
    move/from16 v0, v18

    if-ge v12, v0, :cond_4

    move-object/from16 v0, v17

    invoke-virtual {v0, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, LX/CTp;

    .line 1909386
    rem-int/lit8 v7, v6, 0x4

    .line 1909387
    if-nez v7, :cond_6

    .line 1909388
    invoke-virtual {v9}, Landroid/widget/LinearLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v5, 0x7f030fb1

    const/4 v8, 0x0

    invoke-virtual {v1, v5, v9, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1909389
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object v11, v1

    .line 1909390
    :goto_4
    invoke-virtual {v11, v7}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1909391
    add-int/lit8 v13, v6, 0x1

    .line 1909392
    iget-object v1, v2, LX/CTp;->b:Ljava/lang/String;

    invoke-virtual {v10, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909393
    const/4 v1, 0x0

    invoke-virtual {v10, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1909394
    iget-boolean v1, v2, LX/CTp;->a:Z

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_5
    invoke-virtual {v10, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setEnabled(Z)V

    .line 1909395
    iget-boolean v1, v2, LX/CTp;->a:Z

    if-nez v1, :cond_0

    .line 1909396
    iget-object v5, v2, LX/CTp;->c:Ljava/lang/String;

    .line 1909397
    new-instance v1, LX/CX2;

    move-object/from16 v2, p0

    move-object/from16 v6, p2

    move-object/from16 v7, p1

    move-object/from16 v8, p3

    invoke-direct/range {v1 .. v8}, LX/CX2;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;LX/CSY;Ljava/lang/String;Ljava/lang/String;LX/CT7;LX/CTr;LX/CT6;)V

    invoke-virtual {v10, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1909398
    :cond_0
    add-int/lit8 v1, v12, 0x1

    move v12, v1

    move-object v5, v11

    move v6, v13

    goto :goto_3

    .line 1909399
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTimeslotPickerView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1909400
    :cond_2
    const/16 v5, 0x8

    invoke-virtual {v2, v5}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1909401
    :cond_3
    const/4 v1, 0x0

    goto :goto_5

    .line 1909402
    :cond_4
    add-int/lit8 v1, v14, 0x1

    move v14, v1

    goto/16 :goto_1

    .line 1909403
    :cond_5
    return-void

    :cond_6
    move-object v11, v5

    goto :goto_4
.end method
