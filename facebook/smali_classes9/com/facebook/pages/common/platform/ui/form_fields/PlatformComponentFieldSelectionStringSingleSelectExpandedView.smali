.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final b:Landroid/widget/RadioGroup;

.field private final c:I

.field public d:LX/CXF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909000
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909001
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909002
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909003
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908994
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908995
    const v0, 0x7f030fa5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908996
    const v0, 0x7f0d25c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908997
    const v0, 0x7f0d25c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->b:Landroid/widget/RadioGroup;

    .line 1908998
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->c:I

    .line 1908999
    return-void
.end method


# virtual methods
.method public final a(LX/CTn;LX/CT7;)V
    .locals 8

    .prologue
    .line 1908974
    new-instance v0, LX/CXF;

    invoke-direct {v0, p1}, LX/CXF;-><init>(LX/CTn;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->d:LX/CXF;

    .line 1908975
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v6

    .line 1908976
    if-eqz v6, :cond_0

    move-object v2, v6

    .line 1908977
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v3

    .line 1908978
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CXF;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1908979
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 1908980
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->b:Landroid/widget/RadioGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1908981
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->clearCheck()V

    .line 1908982
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->d:LX/CXF;

    invoke-virtual {v1}, LX/CXF;->a()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1908983
    new-instance v1, Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Lcom/facebook/resources/ui/FbRadioButton;-><init>(Landroid/content/Context;)V

    .line 1908984
    iget-object v4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->d:LX/CXF;

    invoke-virtual {v4, v0}, LX/CXF;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbRadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 1908985
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbRadioButton;->setId(I)V

    .line 1908986
    iget v4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->c:I

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbRadioButton;->setTextColor(I)V

    .line 1908987
    iget-object v4, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v4, v1}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 1908988
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1908989
    :cond_0
    new-instance v2, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v2, v0, v1, v3}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908990
    :cond_1
    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->b:Landroid/widget/RadioGroup;

    new-instance v0, LX/CWp;

    move-object v1, p0

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/CWp;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTn;)V

    invoke-virtual {v7, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1908991
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->d:LX/CXF;

    invoke-virtual {v0, v3, v6}, LX/CXF;->a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1908992
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldSelectionStringSingleSelectExpandedView;->b:Landroid/widget/RadioGroup;

    invoke-virtual {v2, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_2

    .line 1908993
    :cond_2
    return-void
.end method
