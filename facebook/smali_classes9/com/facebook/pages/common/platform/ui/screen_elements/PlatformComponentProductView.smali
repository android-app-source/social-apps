.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/5fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final d:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final e:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final f:Lcom/facebook/drawee/span/DraweeSpanTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910373
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910374
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910375
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910376
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910363
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910364
    const-class v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910365
    const v0, 0x7f030fa9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1910366
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->setOrientation(I)V

    .line 1910367
    const v0, 0x7f0d25d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->f:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910368
    const v0, 0x7f0d25d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->e:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910369
    const v0, 0x7f0d25d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910370
    const v0, 0x7f0d25d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->d:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910371
    const v0, 0x7f0d25d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910372
    return-void
.end method

.method private static a(Lcom/facebook/drawee/span/DraweeSpanTextView;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1910361
    invoke-virtual {p0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0, v1, v0, v1, p1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setPadding(IIII)V

    .line 1910362
    return-void
.end method

.method private static a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1910377
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1910378
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910379
    invoke-virtual {p0, p1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1910380
    :goto_0
    return-void

    .line 1910381
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    invoke-static {v0}, LX/5fv;->b(LX/0QB;)LX/5fv;

    move-result-object v0

    check-cast v0, LX/5fv;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a:LX/5fv;

    return-void
.end method

.method private b(LX/CU0;)Lcom/facebook/drawee/span/DraweeSpanTextView;
    .locals 1

    .prologue
    .line 1910343
    iget-object v0, p1, LX/CU0;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1910344
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->d:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910345
    :goto_0
    return-object v0

    .line 1910346
    :cond_0
    iget-object v0, p1, LX/CU0;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1910347
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    goto :goto_0

    .line 1910348
    :cond_1
    iget-object v0, p1, LX/CU0;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1910349
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->e:Lcom/facebook/drawee/span/DraweeSpanTextView;

    goto :goto_0

    .line 1910350
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->f:Lcom/facebook/drawee/span/DraweeSpanTextView;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CU0;)V
    .locals 3

    .prologue
    .line 1910351
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->f:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CU0;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1910352
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->e:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CU0;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1910353
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CU0;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1910354
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->d:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CU0;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1910355
    iget-object v0, p1, LX/CU0;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    if-eqz v0, :cond_0

    .line 1910356
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a:LX/5fv;

    iget-object v1, p1, LX/CU0;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v0, v1}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v0

    .line 1910357
    :goto_0
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-static {v1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1910358
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->b(LX/CU0;)Lcom/facebook/drawee/span/DraweeSpanTextView;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->f:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0060

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-static {v0, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;I)V

    .line 1910359
    return-void

    .line 1910360
    :cond_0
    iget-object v0, p1, LX/CU0;->c:Ljava/lang/String;

    goto :goto_0
.end method
