.class public Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;
.super Lcom/facebook/drawee/span/DraweeSpanTextView;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910331
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910332
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910333
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910334
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910335
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/span/DraweeSpanTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910336
    const-class v0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910337
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method


# virtual methods
.method public final a(LX/CTf;)V
    .locals 2

    .prologue
    .line 1910338
    iget-object v0, p1, LX/CTf;->a:LX/CU1;

    iget-object v0, v0, LX/CU1;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/CTf;->a:LX/CU1;

    iget-object v0, v0, LX/CU1;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1910339
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;->setVisibility(I)V

    .line 1910340
    :goto_0
    return-void

    .line 1910341
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;->setVisibility(I)V

    .line 1910342
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentParagraphView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iget-object v1, p1, LX/CTf;->a:LX/CU1;

    invoke-virtual {v0, p0, v1}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    goto :goto_0
.end method
