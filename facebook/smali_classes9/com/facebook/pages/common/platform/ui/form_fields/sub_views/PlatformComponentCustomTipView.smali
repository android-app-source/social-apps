.class public Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""


# instance fields
.field public b:LX/5fv;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:LX/CXI;

.field private f:Ljava/math/BigDecimal;

.field public g:LX/CWu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909812
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909813
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1909806
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909807
    const-string v0, "USD"

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->d:Ljava/lang/String;

    .line 1909808
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->f:Ljava/math/BigDecimal;

    .line 1909809
    new-instance v0, LX/CXI;

    invoke-direct {v0, p0}, LX/CXI;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->e:LX/CXI;

    .line 1909810
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->e:LX/CXI;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1909811
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 3

    .prologue
    .line 1909801
    invoke-static {p1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->b(Ljava/lang/String;)J

    move-result-wide v0

    .line 1909802
    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->d:Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/payments/currency/CurrencyAmount;->c(Ljava/lang/String;)I

    move-result v2

    .line 1909803
    invoke-static {v0, v1, v2}, Lcom/facebook/payments/currency/CurrencyAmount;->a(JI)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1909804
    invoke-direct {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->a(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 1909805
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    iget-object v2, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    return-object v1
.end method

.method private a(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 1909780
    sget-object v0, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 1909781
    sget-object p1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    .line 1909782
    :cond_0
    :goto_0
    return-object p1

    .line 1909783
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->f:Ljava/math/BigDecimal;

    invoke-virtual {p1, v0}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1909784
    iget-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->f:Ljava/math/BigDecimal;

    goto :goto_0
.end method

.method private static b(Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 1909796
    if-nez p0, :cond_0

    .line 1909797
    const-wide/16 v0, 0x0

    .line 1909798
    :goto_0
    return-wide v0

    .line 1909799
    :cond_0
    const-string v0, "[^\\d]"

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1909800
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1909814
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f082a9a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1909815
    :cond_0
    :goto_0
    return-void

    .line 1909816
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1909817
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->b:LX/5fv;

    invoke-virtual {v1, v0}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    .line 1909818
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->setText(Ljava/lang/CharSequence;)V

    .line 1909819
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->setSelection(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1909793
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->g:LX/CWu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1909794
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->g:LX/CWu;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v1

    invoke-static {v1}, LX/CXT;->a(Lcom/facebook/payments/currency/CurrencyAmount;)LX/CXT;

    move-result-object v1

    invoke-interface {v0, v1}, LX/CWu;->a(LX/CXT;)V

    .line 1909795
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/payments/currency/CurrencyAmount;)V
    .locals 2

    .prologue
    .line 1909785
    iget-object v0, p1, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object v0, v0

    .line 1909786
    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->f:Ljava/math/BigDecimal;

    .line 1909787
    iget-object v0, p1, Lcom/facebook/payments/currency/CurrencyAmount;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1909788
    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->d:Ljava/lang/String;

    .line 1909789
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1909790
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    .line 1909791
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->b:LX/5fv;

    invoke-virtual {v1, v0}, LX/5fv;->a(Lcom/facebook/payments/currency/CurrencyAmount;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c(Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;Ljava/lang/String;)V

    .line 1909792
    :cond_0
    return-void
.end method

.method public getCustomTip()Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 1

    .prologue
    .line 1909779
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->a(Ljava/lang/String;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 1909776
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1909777
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->a()V

    .line 1909778
    :cond_0
    invoke-super {p0, p2}, Lcom/facebook/resources/ui/FbEditText;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnTipSelectionChangedListener(LX/CWu;)V
    .locals 0

    .prologue
    .line 1909774
    iput-object p1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/sub_views/PlatformComponentCustomTipView;->g:LX/CWu;

    .line 1909775
    return-void
.end method
