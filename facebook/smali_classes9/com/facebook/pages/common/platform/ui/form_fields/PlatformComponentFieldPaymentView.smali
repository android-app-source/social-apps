.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;
.super Lcom/facebook/payments/ui/FloatingLabelTextView;
.source ""


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908591
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908592
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908593
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908594
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908595
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/payments/ui/FloatingLabelTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908596
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1908597
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->a:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;Landroid/view/View;LX/CSQ;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTK;Lcom/facebook/payments/ui/FloatingLabelTextView;)V
    .locals 10

    .prologue
    .line 1908598
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v7

    .line 1908599
    sget-object v0, LX/6xZ;->SELECT_PAYMENT_METHOD:LX/6xZ;

    sget-object v1, LX/6xY;->CHECKOUT:LX/6xY;

    invoke-static {v1}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object v1

    invoke-virtual {v1}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v0

    const-string v1, "Native Component Flow"

    invoke-virtual {v0, v1}, LX/718;->a(Ljava/lang/String;)LX/718;

    move-result-object v0

    invoke-virtual {v0}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v0

    .line 1908600
    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;->newBuilder()LX/707;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/707;->a(Z)LX/707;

    move-result-object v1

    invoke-virtual {v1}, LX/707;->e()Lcom/facebook/payments/paymentmethods/picker/model/PaymentMethodsPickerScreenFetcherParams;

    move-result-object v1

    .line 1908601
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/71A;->a(Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;)LX/71A;

    move-result-object v0

    sget-object v2, LX/71C;->PAYMENT_METHODS:LX/71C;

    invoke-virtual {v0, v2}, LX/71A;->a(LX/71C;)LX/71A;

    move-result-object v0

    sget-object v2, LX/6xg;->MOR_NONE:LX/6xg;

    invoke-virtual {v0, v2}, LX/71A;->a(LX/6xg;)LX/71A;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/71A;->a(Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;)LX/71A;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080c7a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/71A;->a(Ljava/lang/String;)LX/71A;

    move-result-object v0

    invoke-virtual {v0}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v0

    .line 1908602
    invoke-static {}, Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;->newBuilder()LX/6zf;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6zf;->a(Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;)LX/6zf;

    move-result-object v0

    invoke-virtual {v0}, LX/6zf;->e()Lcom/facebook/payments/paymentmethods/picker/PaymentMethodsPickerScreenConfig;

    move-result-object v0

    .line 1908603
    invoke-static {v7, v0}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Context;Lcom/facebook/payments/picker/model/PickerScreenConfig;)Landroid/content/Intent;

    move-result-object v8

    .line 1908604
    const-string v0, "request_code"

    const/16 v1, 0x2a

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1908605
    const/16 v9, 0x2a

    new-instance v0, LX/CWg;

    move-object v1, p0

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p7

    invoke-direct/range {v0 .. v6}, LX/CWg;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTK;Lcom/facebook/payments/ui/FloatingLabelTextView;)V

    invoke-virtual {p2, v9, v0}, LX/CSQ;->a(ILX/CWg;)V

    .line 1908606
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->a:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2a

    move-object v0, v7

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v8, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1908607
    return-void
.end method

.method public static b(Landroid/content/Intent;)Lcom/facebook/payments/paymentmethods/model/CreditCard;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1908608
    const-string v0, "selected_payment_method"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;

    .line 1908609
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/facebook/payments/paymentmethods/model/PaymentMethod;->b()LX/6zU;

    move-result-object v1

    sget-object v4, LX/6zU;->CREDIT_CARD:LX/6zU;

    if-ne v1, v4, :cond_0

    instance-of v1, v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    if-nez v1, :cond_1

    .line 1908610
    :cond_0
    const/4 v0, 0x0

    .line 1908611
    :goto_0
    return-object v0

    .line 1908612
    :cond_1
    check-cast v0, Lcom/facebook/payments/paymentmethods/model/CreditCard;

    .line 1908613
    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->g()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getHumanReadableName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1908614
    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_2
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1908615
    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_3
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1908616
    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    :goto_4
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1908617
    invoke-virtual {v0}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    :goto_5
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 1908618
    goto :goto_1

    :cond_3
    move v1, v3

    .line 1908619
    goto :goto_2

    :cond_4
    move v1, v3

    .line 1908620
    goto :goto_3

    :cond_5
    move v1, v3

    .line 1908621
    goto :goto_4

    :cond_6
    move v2, v3

    .line 1908622
    goto :goto_5
.end method


# virtual methods
.method public final a(LX/CTK;LX/CSQ;LX/CT7;)V
    .locals 9

    .prologue
    .line 1908623
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p3, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v8

    .line 1908624
    if-eqz v8, :cond_1

    move-object v3, v8

    .line 1908625
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v4

    .line 1908626
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080c79

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908627
    new-instance v0, LX/CWf;

    move-object v1, p0

    move-object v2, p2

    move-object v5, p3

    move-object v6, p1

    move-object v7, p0

    invoke-direct/range {v0 .. v7}, LX/CWf;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;LX/CSQ;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTK;Lcom/facebook/payments/ui/FloatingLabelTextView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908628
    if-eqz v8, :cond_0

    .line 1908629
    iget-object v0, v8, LX/CSY;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSZ;

    invoke-virtual {v0}, LX/CSZ;->c()Lcom/facebook/payments/paymentmethods/model/CreditCard;

    move-result-object v0

    move-object v0, v0

    .line 1908630
    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldPaymentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/model/CreditCard;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908631
    :cond_0
    return-void

    .line 1908632
    :cond_1
    new-instance v3, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v3, v0, v1, v2}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0
.end method
