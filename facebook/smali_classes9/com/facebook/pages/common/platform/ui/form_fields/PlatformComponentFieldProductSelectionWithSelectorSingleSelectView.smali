.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private e:LX/CXD;

.field public f:LX/CXC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908862
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908863
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908860
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908861
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908864
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908865
    const-class v0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1908866
    const v0, 0x7f030f9f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908867
    const v0, 0x7f0d25c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908868
    const v0, 0x7f0d25c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908869
    const v0, 0x7f0d25c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 1908870
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method


# virtual methods
.method public final a(LX/CTT;LX/CT7;)V
    .locals 12

    .prologue
    const/4 v10, 0x0

    .line 1908821
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v5

    .line 1908822
    new-instance v0, LX/CXD;

    invoke-direct {v0, p1}, LX/CXD;-><init>(LX/CTT;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->e:LX/CXD;

    .line 1908823
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v11

    .line 1908824
    if-eqz v11, :cond_0

    move-object v4, v11

    .line 1908825
    :goto_0
    if-nez v11, :cond_1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1908826
    :goto_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-static {v0, v1}, LX/CXD;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;Ljava/lang/String;)V

    .line 1908827
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTT;->b:LX/CU1;

    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    invoke-static {v0, v1, v3}, LX/CXD;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;)V

    .line 1908828
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    move v9, v10

    .line 1908829
    :goto_2
    iget-object v0, p1, LX/CTT;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v9, v0, :cond_2

    .line 1908830
    new-instance v3, LX/CXC;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, LX/CXC;-><init>(Landroid/content/Context;)V

    .line 1908831
    iget-object v0, p1, LX/CTT;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/ArrayList;

    new-instance v0, LX/CWl;

    move-object v1, p0

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, LX/CWl;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;Ljava/util/ArrayList;LX/CXC;LX/CSY;Ljava/lang/String;LX/CT7;LX/CTT;)V

    invoke-virtual {v3, v8, v0}, LX/CXC;->a(Ljava/util/ArrayList;LX/CWl;)V

    .line 1908832
    invoke-virtual {v3, v10}, LX/CXC;->setChecked(Z)V

    .line 1908833
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;)V

    .line 1908834
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_2

    .line 1908835
    :cond_0
    new-instance v4, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v4, v0, v1, v2}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0

    .line 1908836
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v11, v5}, LX/CSY;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_1

    .line 1908837
    :cond_2
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->e:LX/CXD;

    invoke-virtual {v0, v5, v11}, LX/CXD;->a(Ljava/lang/String;LX/CSY;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v10

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1908838
    iget-object v3, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->e:LX/CXD;

    invoke-virtual {v3, v0}, LX/CXD;->a(I)Ljava/lang/String;

    move-result-object v7

    move v3, v10

    .line 1908839
    :goto_4
    iget-object v0, p1, LX/CTT;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_6

    .line 1908840
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->d:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/CXC;

    .line 1908841
    iget-object v8, v0, LX/CXC;->h:Ljava/util/Set;

    invoke-interface {v8, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    move v8, v8

    .line 1908842
    if-eqz v8, :cond_4

    .line 1908843
    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldProductSelectionWithSelectorSingleSelectView;->f:LX/CXC;

    .line 1908844
    const/4 v3, 0x1

    .line 1908845
    iput-boolean v3, v0, LX/CXC;->f:Z

    .line 1908846
    iget-object v1, v0, LX/CXC;->b:Lcom/facebook/resources/ui/FbImageButton;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbImageButton;->setSelected(Z)V

    .line 1908847
    const/4 v1, 0x0

    move v3, v1

    :goto_5
    iget-object v1, v0, LX/CXC;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v3, v1, :cond_3

    .line 1908848
    iget-object v1, v0, LX/CXC;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CU0;

    iget-object v1, v1, LX/CU0;->a:Ljava/lang/String;

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1908849
    iput v3, v0, LX/CXC;->i:I

    .line 1908850
    iget-object v3, v0, LX/CXC;->d:Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;

    iget-object v1, v0, LX/CXC;->g:Ljava/util/ArrayList;

    iget v8, v0, LX/CXC;->i:I

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CU0;

    invoke-virtual {v3, v1}, Lcom/facebook/pages/common/platform/ui/screen_elements/PlatformComponentProductView;->a(LX/CU0;)V

    .line 1908851
    :cond_3
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1908852
    const/4 v0, 0x1

    .line 1908853
    :goto_6
    if-nez v0, :cond_5

    move v1, v0

    .line 1908854
    goto :goto_3

    .line 1908855
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 1908856
    :cond_5
    invoke-virtual {v4, v5, v2}, LX/CSY;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1908857
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1, v4}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908858
    return-void

    :cond_6
    move v0, v1

    goto :goto_6

    .line 1908859
    :cond_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5
.end method
