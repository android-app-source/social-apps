.class public Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final c:Lcom/facebook/drawee/span/DraweeSpanTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1910090
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910091
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910092
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910093
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910094
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910095
    const-class v0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910096
    const v0, 0x7f030fd9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1910097
    const v0, 0x7f0d2641

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910098
    const v0, 0x7f0d2642

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1910099
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;

    invoke-static {v0}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->b(LX/0QB;)Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    return-void
.end method


# virtual methods
.method public final a(LX/CUE;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 1910100
    iget-object v0, p1, LX/CUE;->a:LX/CU1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/CUE;->a:LX/CU1;

    iget-object v0, v0, LX/CU1;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1910101
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v2, p1, LX/CUE;->a:LX/CU1;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1910102
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910103
    :goto_0
    iget-object v0, p1, LX/CUE;->b:LX/CU1;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/CUE;->b:LX/CU1;

    iget-object v0, v0, LX/CU1;->a:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1910104
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->a:Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;

    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v2, p1, LX/CUE;->b:LX/CU1;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/common/platform/util/PagesPlatformRichTextConverter;->a(Lcom/facebook/drawee/span/DraweeSpanTextView;LX/CU1;)V

    .line 1910105
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    .line 1910106
    :goto_1
    return-void

    .line 1910107
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->b:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    goto :goto_0

    .line 1910108
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/layout_elements/PlatformLayoutDisclaimerView;->c:Lcom/facebook/drawee/span/DraweeSpanTextView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setVisibility(I)V

    goto :goto_1
.end method
