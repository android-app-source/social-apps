.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final b:Lcom/facebook/fig/textinput/FigEditText;

.field private final c:Lcom/facebook/fig/textinput/FigEditText;

.field private final d:Lcom/facebook/fig/textinput/FigEditText;

.field private final e:Lcom/facebook/fig/textinput/FigEditText;

.field private final f:Landroid/widget/LinearLayout;

.field public final g:Lcom/facebook/fig/textinput/FigEditText;

.field private final h:Lcom/facebook/fbui/glyph/GlyphView;

.field public final i:LX/34b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1908376
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1908377
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908378
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908379
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1908380
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1908381
    const v0, 0x7f030f96

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1908382
    const v0, 0x7f0d25b0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1908383
    const v0, 0x7f0d25b1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->b:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908384
    const v0, 0x7f0d25b2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->c:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908385
    const v0, 0x7f0d25b3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->d:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908386
    const v0, 0x7f0d25b4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->e:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908387
    const v0, 0x7f0d25b5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->f:Landroid/widget/LinearLayout;

    .line 1908388
    const v0, 0x7f0d25b6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->g:Lcom/facebook/fig/textinput/FigEditText;

    .line 1908389
    const v0, 0x7f0d25b7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->h:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1908390
    new-instance v0, LX/34b;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/34b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->i:LX/34b;

    .line 1908391
    return-void
.end method

.method private a(LX/CTM;LX/CT7;LX/CSY;LX/CSY;)V
    .locals 11
    .param p3    # LX/CSY;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x1

    const/4 v8, 0x0

    .line 1908392
    iget-object v0, p1, LX/CTK;->h:Ljava/util/ArrayList;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1908393
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1908394
    :goto_0
    return-void

    .line 1908395
    :cond_0
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1908396
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->g:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0, v3}, Lcom/facebook/fig/textinput/FigEditText;->setClickable(Z)V

    .line 1908397
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->g:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0, v8}, Lcom/facebook/fig/textinput/FigEditText;->setFocusable(Z)V

    .line 1908398
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->g:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0, v8}, Lcom/facebook/fig/textinput/FigEditText;->setInputType(I)V

    .line 1908399
    if-eqz p3, :cond_3

    .line 1908400
    const-string v0, "verified_email"

    invoke-virtual {p3, v0}, LX/CSY;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1908401
    :goto_1
    const-string v1, "verified_email"

    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4, v1, v2}, LX/CSY;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1908402
    iget-object v1, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v2, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v1, v2, p4}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;LX/CSY;)V

    .line 1908403
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->g:Lcom/facebook/fig/textinput/FigEditText;

    invoke-static {v0}, LX/CXh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1908404
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->g:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/support/design/widget/TextInputLayout;

    if-eqz v0, :cond_1

    .line 1908405
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->g:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/support/design/widget/TextInputLayout;

    invoke-virtual {p0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082a91

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/design/widget/TextInputLayout;->setHint(Ljava/lang/CharSequence;)V

    .line 1908406
    :cond_1
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->i:LX/34b;

    invoke-virtual {v0}, LX/34c;->clear()V

    .line 1908407
    iget-object v0, p1, LX/CTK;->j:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p1, LX/CTK;->j:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v3, :cond_7

    .line 1908408
    iget-object v0, p1, LX/CTK;->j:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/0Px;

    .line 1908409
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v9

    move v7, v8

    :goto_2
    if-ge v7, v9, :cond_6

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1908410
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1908411
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->i:LX/34b;

    invoke-virtual {v0, v2}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v10

    .line 1908412
    new-instance v0, LX/CWZ;

    move-object v1, p0

    move-object v3, p4

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/CWZ;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;Ljava/lang/String;LX/CSY;LX/CT7;LX/CTM;)V

    invoke-virtual {v10, v0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1908413
    :cond_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2

    .line 1908414
    :cond_3
    iget-object v0, p1, LX/CTM;->a:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p1, LX/CTM;->a:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1908415
    iget-object v0, p1, LX/CTM;->a:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_1

    .line 1908416
    :cond_4
    iget-object v0, p1, LX/CTK;->j:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/CTK;->j:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1908417
    iget-object v0, p1, LX/CTK;->j:Ljava/util/HashMap;

    const-string v1, "verified_email"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto/16 :goto_1

    .line 1908418
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1908419
    :cond_6
    new-instance v0, LX/CWa;

    invoke-direct {v0, p0, p1}, LX/CWa;-><init>(Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;LX/CTM;)V

    .line 1908420
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908421
    iget-object v1, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->g:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1, v0}, Lcom/facebook/fig/textinput/FigEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1908422
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v8}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1908423
    :cond_7
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->h:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(LX/CTM;LX/CT7;)V
    .locals 9

    .prologue
    .line 1908424
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908425
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v2

    .line 1908426
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 1908427
    :goto_0
    const-string v1, "first_name"

    iget-object v4, p1, LX/CTM;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a8d

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->b:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908428
    const-string v1, "last_name"

    iget-object v4, p1, LX/CTM;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a8e

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->c:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908429
    const-string v1, "phone"

    iget-object v4, p1, LX/CTM;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a8f

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->d:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908430
    const-string v1, "email"

    iget-object v4, p1, LX/CTM;->a:Ljava/util/HashMap;

    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    const v6, 0x7f082a90

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->e:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;ILcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1908431
    invoke-direct {p0, p1, p2, v2, v3}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldContactinfoView;->a(LX/CTM;LX/CT7;LX/CSY;LX/CSY;)V

    .line 1908432
    return-void

    .line 1908433
    :cond_0
    new-instance v3, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v3, v0, v1, v4}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0
.end method
