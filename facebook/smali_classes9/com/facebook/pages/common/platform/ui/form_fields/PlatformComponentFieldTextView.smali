.class public Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/drawee/span/DraweeSpanTextView;

.field private final b:Lcom/facebook/fig/textinput/FigEditText;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1909336
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1909337
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909338
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909339
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1909340
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1909341
    const v0, 0x7f030fa6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1909342
    const v0, 0x7f0d25ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/span/DraweeSpanTextView;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    .line 1909343
    const v0, 0x7f0d25cf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/textinput/FigEditText;

    iput-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;->b:Lcom/facebook/fig/textinput/FigEditText;

    .line 1909344
    return-void
.end method


# virtual methods
.method public final a(LX/CTo;LX/CT7;)V
    .locals 9

    .prologue
    .line 1909345
    iget-object v0, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;->a:Lcom/facebook/drawee/span/DraweeSpanTextView;

    iget-object v1, p1, LX/CTK;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/span/DraweeSpanTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1909346
    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-object v1, p1, LX/CTK;->e:Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;

    invoke-virtual {p2, v0, v1}, LX/CT7;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLScreenElementFormFieldType;)LX/CSY;

    move-result-object v2

    .line 1909347
    if-eqz v2, :cond_1

    move-object v3, v2

    .line 1909348
    :goto_0
    invoke-static {p1}, LX/CSR;->a(LX/CTK;)Ljava/lang/String;

    move-result-object v1

    .line 1909349
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1909350
    iget-object v0, p1, LX/CTo;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1909351
    iget-object v0, p1, LX/CTo;->b:Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1909352
    :cond_0
    iget-object v5, p1, LX/CTK;->j:Ljava/util/HashMap;

    iget-object v6, p1, LX/CTo;->a:Ljava/lang/String;

    iget-object v7, p0, Lcom/facebook/pages/common/platform/ui/form_fields/PlatformComponentFieldTextView;->b:Lcom/facebook/fig/textinput/FigEditText;

    move-object v0, p1

    move-object v8, p2

    invoke-static/range {v0 .. v8}, LX/CXh;->a(LX/CTK;Ljava/lang/String;LX/CSY;LX/CSY;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/lang/String;Lcom/facebook/fig/textinput/FigEditText;LX/CT7;)V

    .line 1909353
    return-void

    .line 1909354
    :cond_1
    new-instance v3, LX/CSY;

    iget-object v0, p1, LX/CTJ;->o:Ljava/lang/String;

    iget-boolean v1, p1, LX/CTK;->i:Z

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v3, v0, v1, v4}, LX/CSY;-><init>(Ljava/lang/String;ZLjava/util/HashMap;)V

    goto :goto_0
.end method
