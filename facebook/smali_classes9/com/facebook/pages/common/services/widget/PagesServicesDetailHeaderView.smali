.class public Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1910724
    const-class v0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

    const-string v1, "unknown"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1910721
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1910722
    invoke-direct {p0}, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->a()V

    .line 1910723
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910715
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1910716
    invoke-direct {p0}, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->a()V

    .line 1910717
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1910718
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1910719
    invoke-direct {p0}, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->a()V

    .line 1910720
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1910710
    const-class v0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

    invoke-static {v0, p0}, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1910711
    const v0, 0x7f030ee2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1910712
    const v0, 0x7f0d244d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1910713
    const v0, 0x7f0d244c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1910714
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->b:LX/1Ad;

    return-void
.end method


# virtual methods
.method public setImageURI(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 1910702
    iget-object v0, p0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1910703
    new-instance v0, LX/4em;

    const/16 v1, 0x1e

    invoke-direct {v0, v1}, LX/4em;-><init>(I)V

    .line 1910704
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    .line 1910705
    iput-object v0, v1, LX/1bX;->j:LX/33B;

    .line 1910706
    move-object v0, v1

    .line 1910707
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1910708
    iget-object v1, p0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->b:LX/1Ad;

    sget-object v3, Lcom/facebook/pages/common/services/widget/PagesServicesDetailHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1910709
    return-void
.end method
