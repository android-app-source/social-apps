.class public Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1911055
    new-instance v0, LX/CYN;

    invoke-direct {v0}, LX/CYN;-><init>()V

    sput-object v0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1911056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1911057
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->a:Z

    .line 1911058
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 1911059
    return-void
.end method

.method public constructor <init>(ZLcom/facebook/graphql/enums/GraphQLPageCallToActionType;)V
    .locals 0

    .prologue
    .line 1911060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1911061
    iput-boolean p1, p0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->a:Z

    .line 1911062
    iput-object p2, p0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    .line 1911063
    return-void
.end method

.method public static a()Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;
    .locals 3

    .prologue
    .line 1911064
    new-instance v0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;

    const/4 v1, 0x1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-direct {v0, v1, v2}, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;-><init>(ZLcom/facebook/graphql/enums/GraphQLPageCallToActionType;)V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1911065
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1911066
    iget-boolean v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->a:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1911067
    iget-object v0, p0, Lcom/facebook/pages/common/surface/calltoaction/ipc/PageAdminCallToActionFlowControlParam;->b:Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageCallToActionType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1911068
    return-void
.end method
