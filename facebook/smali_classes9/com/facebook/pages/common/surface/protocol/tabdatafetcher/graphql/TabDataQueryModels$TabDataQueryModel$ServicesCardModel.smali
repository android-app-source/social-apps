.class public final Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6c112f5a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1911338
    const-class v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1911339
    const-class v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1911340
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1911341
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1911342
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1911343
    invoke-virtual {p0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1911344
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1911345
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1911346
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1911347
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1911348
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1911349
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1911350
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1911351
    iget-object v0, p0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->e:Ljava/lang/String;

    .line 1911352
    iget-object v0, p0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1911353
    new-instance v0, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;

    invoke-direct {v0}, Lcom/facebook/pages/common/surface/protocol/tabdatafetcher/graphql/TabDataQueryModels$TabDataQueryModel$ServicesCardModel;-><init>()V

    .line 1911354
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1911355
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1911356
    const v0, 0x2b33b4f6

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1911357
    const v0, -0x5c14c323

    return v0
.end method
