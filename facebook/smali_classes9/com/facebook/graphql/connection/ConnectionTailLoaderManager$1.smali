.class public final Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/2nf;

.field public final synthetic b:LX/B0O;


# direct methods
.method public constructor <init>(LX/B0O;LX/2nf;)V
    .locals 0

    .prologue
    .line 1733717
    iput-object p1, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$1;->b:LX/B0O;

    iput-object p2, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$1;->a:LX/2nf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1733718
    iget-object v0, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$1;->b:LX/B0O;

    iget-object v1, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$1;->a:LX/2nf;

    const/4 p0, 0x1

    .line 1733719
    invoke-static {v0, v1}, LX/B0O;->c(LX/B0O;LX/2nf;)I

    move-result v2

    .line 1733720
    packed-switch v2, :pswitch_data_0

    .line 1733721
    :goto_0
    :pswitch_0
    iget-object v2, v0, LX/B0O;->j:LX/0Sh;

    invoke-virtual {v2}, LX/0Sh;->a()V

    .line 1733722
    iget-object v2, v0, LX/B0O;->c:LX/B0L;

    invoke-interface {v2, v1}, LX/B0L;->a(LX/2nf;)V

    .line 1733723
    invoke-static {v0}, LX/B0O;->i$redex0(LX/B0O;)V

    .line 1733724
    :cond_0
    :goto_1
    return-void

    .line 1733725
    :pswitch_1
    if-eqz v1, :cond_0

    .line 1733726
    invoke-interface {v1}, LX/2nf;->close()V

    goto :goto_1

    .line 1733727
    :pswitch_2
    iput-boolean p0, v0, LX/B0O;->m:Z

    goto :goto_0

    .line 1733728
    :pswitch_3
    iget-object v2, v0, LX/B0O;->b:LX/B0U;

    invoke-interface {v2}, LX/B0U;->b()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    .line 1733729
    iput-boolean p0, v0, LX/B0O;->m:Z

    goto :goto_0

    .line 1733730
    :cond_1
    if-eqz v1, :cond_0

    .line 1733731
    invoke-interface {v1}, LX/2nf;->close()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
