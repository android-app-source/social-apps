.class public final Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/B0O;


# direct methods
.method public constructor <init>(LX/B0O;)V
    .locals 0

    .prologue
    .line 1733732
    iput-object p1, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1733733
    iget-object v0, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->d:LX/B0N;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1733734
    iget-object v0, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->i:LX/2k0;

    iget-object v1, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    iget-object v1, v1, LX/B0O;->l:LX/2kb;

    iget-object v2, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    iget-object v2, v2, LX/B0O;->d:LX/B0N;

    invoke-interface {v2}, LX/B0N;->a()Ljava/nio/ByteBuffer;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    iget-object v3, v3, LX/B0O;->d:LX/B0N;

    invoke-interface {v3}, LX/B0N;->b()LX/95b;

    move-result-object v3

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    iget-object v6, v6, LX/B0O;->d:LX/B0N;

    invoke-interface {v6}, LX/B0N;->c()Lcom/facebook/graphql/cursor/edgestore/PageInfo;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual/range {v0 .. v7}, LX/2k0;->b(LX/2kb;Ljava/nio/ByteBuffer;LX/95b;JLcom/facebook/graphql/cursor/edgestore/PageInfo;Z)Landroid/os/Bundle;

    .line 1733735
    iget-object v0, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    const/4 v1, 0x0

    .line 1733736
    iput-object v1, v0, LX/B0O;->d:LX/B0N;

    .line 1733737
    iget-object v0, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    invoke-static {v0}, LX/B0O;->g(LX/B0O;)V

    .line 1733738
    iget-object v0, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    iget-object v0, v0, LX/B0O;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 1733739
    iget-object v0, p0, Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$2;->a:LX/B0O;

    invoke-virtual {v0}, LX/B0O;->c()V

    .line 1733740
    return-void
.end method
