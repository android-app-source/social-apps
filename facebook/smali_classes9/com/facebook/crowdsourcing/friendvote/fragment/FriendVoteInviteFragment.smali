.class public Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""


# instance fields
.field public u:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1805108
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    .line 1805109
    const-class v0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->y:Ljava/lang/String;

    .line 1805110
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p0

    check-cast p0, LX/0kL;

    iput-object v1, p1, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->u:LX/03V;

    iput-object v2, p1, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->v:LX/0tX;

    iput-object v3, p1, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->w:LX/1Ck;

    iput-object p0, p1, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->x:LX/0kL;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1805085
    const-class v0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;

    invoke-static {v0, p0}, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1805086
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 1805087
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1805088
    if-eqz v0, :cond_0

    .line 1805089
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->z:Ljava/lang/String;

    .line 1805090
    :cond_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->z:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1805091
    :cond_1
    iget-object v0, p0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->u:LX/03V;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->y:Ljava/lang/String;

    const-string v2, "FriendVote inviter launched without specifying Page ID"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805092
    :cond_2
    return-void
.end method

.method public final y()V
    .locals 8

    .prologue
    .line 1805093
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v0

    .line 1805094
    new-instance v1, LX/Beb;

    invoke-direct {v1}, LX/Beb;-><init>()V

    move-object v1, v1

    .line 1805095
    new-instance v2, LX/4Dv;

    invoke-direct {v2}, LX/4Dv;-><init>()V

    iget-object v3, p0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->z:Ljava/lang/String;

    .line 1805096
    const-string v4, "place_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805097
    move-object v2, v2

    .line 1805098
    const-string v3, "friend_ids"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1805099
    move-object v2, v2

    .line 1805100
    const-string v3, "input"

    invoke-virtual {v1, v3, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1805101
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 1805102
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 1805103
    new-instance v2, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1805104
    new-instance v3, LX/BeZ;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0f0125

    invoke-virtual {v5, v6, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f0126

    invoke-virtual {v6, v7, v0}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, LX/BeZ;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v3

    const v4, 0x7f080016

    new-instance v5, LX/BeY;

    invoke-direct {v5, p0}, LX/BeY;-><init>(Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;)V

    invoke-virtual {v3, v4, v5}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1805105
    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    invoke-virtual {v2}, LX/2EJ;->show()V

    .line 1805106
    iget-object v0, p0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->w:LX/1Ck;

    const-string v2, "key_send_invite"

    iget-object v3, p0, Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;->v:LX/0tX;

    sget-object v4, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v3, v1, v4}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v3, LX/BeX;

    invoke-direct {v3, p0}, LX/BeX;-><init>(Lcom/facebook/crowdsourcing/friendvote/fragment/FriendVoteInviteFragment;)V

    invoke-virtual {v0, v2, v1, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1805107
    return-void
.end method
