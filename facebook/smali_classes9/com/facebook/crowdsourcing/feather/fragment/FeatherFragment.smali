.class public Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/31f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/BeH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1804839
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x70d06c42

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1804874
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 1804875
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v3, p0

    check-cast v3, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v1}, LX/31f;->a(LX/0QB;)LX/31f;

    move-result-object v8

    check-cast v8, LX/31f;

    invoke-static {v1}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v9

    check-cast v9, LX/1Ck;

    invoke-static {v1}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object p3

    check-cast p3, LX/0if;

    invoke-static {v1}, LX/BeH;->a(LX/0QB;)LX/BeH;

    move-result-object v1

    check-cast v1, LX/BeH;

    iput-object v5, v3, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->a:LX/0wM;

    iput-object v6, v3, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->b:LX/03V;

    iput-object v7, v3, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->c:LX/0tX;

    iput-object v8, v3, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->d:LX/31f;

    iput-object v9, v3, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->e:LX/1Ck;

    iput-object p3, v3, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->f:LX/0if;

    iput-object v1, v3, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->g:LX/BeH;

    .line 1804876
    const v1, 0x7f03061e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3dbc6050

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1804840
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1804841
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->f:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    const-string v2, "feather_overlay_start"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804842
    const v0, 0x7f0d10f4

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, LX/BeK;

    invoke-direct {v1, p0}, LX/BeK;-><init>(Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1804843
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 1804844
    const-string v0, "page_id"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "page_name"

    invoke-virtual {v3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "questions"

    invoke-static {v3, v2}, LX/4By;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Collection;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    const-string v2, "entry_point"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1804845
    const v3, 0x7f0d10f5

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1804846
    const v3, 0x7f0d10f7

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/resources/ui/FbTextView;

    const/16 p2, 0x21

    const/4 p1, 0x0

    .line 1804847
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f082932

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1804848
    new-instance v6, Landroid/text/SpannableString;

    invoke-direct {v6, v5}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1804849
    iget-object v5, p0, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->a:LX/0wM;

    const v7, 0x7f0208ed

    const v8, -0x6e685d

    invoke-virtual {v5, v7, v8}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 1804850
    if-eqz v5, :cond_0

    .line 1804851
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0050

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 1804852
    invoke-virtual {v5, p1, p1, v7, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1804853
    new-instance v7, LX/34T;

    const/4 v8, 0x2

    invoke-direct {v7, v5, v8}, LX/34T;-><init>(Landroid/graphics/drawable/Drawable;I)V

    .line 1804854
    const/4 v5, 0x1

    invoke-virtual {v6, v7, p1, v5, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1804855
    :cond_0
    new-instance v5, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f0e0a1a

    invoke-direct {v5, v7, v8}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v6}, Landroid/text/SpannableString;->length()I

    move-result v7

    invoke-virtual {v6, v5, p1, v7, p2}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1804856
    move-object v5, v6

    .line 1804857
    invoke-virtual {v3, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1804858
    const v3, 0x7f0d10f6

    invoke-virtual {p0, v3}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    invoke-virtual {v3, v0, v1, v4}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V

    .line 1804859
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v3

    .line 1804860
    iget-object v5, p0, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->f:LX/0if;

    sget-object v6, LX/0ig;->au:LX/0ih;

    const-string v7, "feather_overlay_end"

    invoke-virtual {v5, v6, v7}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804861
    iget-object v5, p0, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->d:LX/31f;

    new-instance v6, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    const-string v7, "android_feather"

    invoke-direct {v6, v2, v7}, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, v0}, LX/31f;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)V

    .line 1804862
    new-instance v5, LX/96C;

    invoke-direct {v5}, LX/96C;-><init>()V

    move-object v5, v5

    .line 1804863
    new-instance v6, LX/4Eh;

    invoke-direct {v6}, LX/4Eh;-><init>()V

    .line 1804864
    const-string v7, "android_feather"

    .line 1804865
    const-string v8, "endpoint"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1804866
    const-string v7, "entry_point"

    invoke-virtual {v6, v7, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1804867
    const-string v7, "page_id"

    invoke-virtual {v6, v7, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1804868
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    .line 1804869
    const-string v8, "num_questions"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1804870
    const-string v7, "input"

    invoke-virtual {v5, v7, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1804871
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 1804872
    iget-object v6, p0, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->e:LX/1Ck;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "task_overlay_shown"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;->c:LX/0tX;

    sget-object p1, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v8, v5, p1}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    new-instance v8, LX/BeL;

    invoke-direct {v8, p0}, LX/BeL;-><init>(Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;)V

    invoke-virtual {v6, v7, v5, v8}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1804873
    return-void
.end method
