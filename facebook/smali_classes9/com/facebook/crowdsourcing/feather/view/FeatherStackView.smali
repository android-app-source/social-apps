.class public Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;
.super LX/BeU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BeU",
        "<",
        "LX/BeP;",
        ">;"
    }
.end annotation


# static fields
.field private static final m:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/31f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0iA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/BeF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field private q:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1805059
    new-instance v0, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    const-string v1, "android_feather_post_compose"

    const-string v2, "android_feather"

    invoke-direct {v0, v1, v2}, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->m:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1805057
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1805058
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1805017
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1805018
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1805053
    invoke-direct {p0, p1, p2, p3}, LX/BeU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1805054
    const-class v0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    invoke-static {v0, p0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1805055
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    .line 1805056
    return-void
.end method

.method private static a(Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;LX/03V;LX/0if;Lcom/facebook/content/SecureContextHelper;LX/31f;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;LX/17Y;LX/0iA;LX/BeF;LX/0ad;LX/0Uh;)V
    .locals 0

    .prologue
    .line 1805052
    iput-object p1, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->b:LX/0if;

    iput-object p3, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->c:Lcom/facebook/content/SecureContextHelper;

    iput-object p4, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->d:LX/31f;

    iput-object p5, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->e:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iput-object p6, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->f:LX/17Y;

    iput-object p7, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->g:LX/0iA;

    iput-object p8, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->h:LX/BeF;

    iput-object p9, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->i:LX/0ad;

    iput-object p10, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->j:LX/0Uh;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    invoke-static {v10}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {v10}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v2

    check-cast v2, LX/0if;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v10}, LX/31f;->a(LX/0QB;)LX/31f;

    move-result-object v4

    check-cast v4, LX/31f;

    invoke-static {v10}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b(LX/0QB;)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    move-result-object v5

    check-cast v5, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    invoke-static {v10}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v6

    check-cast v6, LX/17Y;

    invoke-static {v10}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v7

    check-cast v7, LX/0iA;

    invoke-static {v10}, LX/BeF;->b(LX/0QB;)LX/BeF;

    move-result-object v8

    check-cast v8, LX/BeF;

    invoke-static {v10}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {v10}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v10

    check-cast v10, LX/0Uh;

    invoke-static/range {v0 .. v10}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->a(Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;LX/03V;LX/0if;Lcom/facebook/content/SecureContextHelper;LX/31f;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;LX/17Y;LX/0iA;LX/BeF;LX/0ad;LX/0Uh;)V

    return-void
.end method

.method private e(I)LX/BeS;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1805042
    iget-boolean v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->q:Z

    if-eqz v2, :cond_0

    if-nez p1, :cond_0

    .line 1805043
    sget-object v0, LX/BeS;->INTRO:LX/BeS;

    .line 1805044
    :goto_0
    return-object v0

    .line 1805045
    :cond_0
    iget-object v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    iget-boolean v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->q:Z

    if-eqz v2, :cond_1

    move v2, v0

    :goto_1
    add-int/2addr v2, v3

    if-ne p1, v2, :cond_2

    .line 1805046
    sget-object v0, LX/BeS;->THANK_YOU:LX/BeS;

    goto :goto_0

    :cond_1
    move v2, v1

    .line 1805047
    goto :goto_1

    .line 1805048
    :cond_2
    iget-object v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    iget-boolean v3, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->q:Z

    if-eqz v3, :cond_3

    :goto_2
    add-int/2addr v0, v2

    if-le p1, v0, :cond_4

    .line 1805049
    sget-object v0, LX/BeS;->UNDEFINED:LX/BeS;

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1805050
    goto :goto_2

    .line 1805051
    :cond_4
    sget-object v0, LX/BeS;->QUESTION:LX/BeS;

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/BeP;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1805032
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v2, 0x7f0d10f7

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1805033
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->j:LX/0Uh;

    const/16 v3, 0x31b

    invoke-virtual {v0, v3, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1805034
    sget-object v0, LX/BeR;->a:[I

    invoke-direct {p0, p1}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->e(I)LX/BeS;

    move-result-object v2

    invoke-virtual {v2}, LX/BeS;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1805035
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    :goto_1
    return-object v0

    .line 1805036
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 1805037
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->h:LX/BeF;

    .line 1805038
    iget-object v1, v0, LX/BeF;->c:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    const-string v2, "4246"

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1805039
    new-instance v0, LX/BeQ;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$1;

    invoke-direct {v2, p0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$1;-><init>(Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;)V

    invoke-direct {v0, v1, v2}, LX/BeQ;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 1805040
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    iget-boolean v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->q:Z

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    :cond_1
    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->e:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p0}, LX/Bg1;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Landroid/content/Context;LX/BeT;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 1805041
    :pswitch_2
    new-instance v0, LX/BeW;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;

    invoke-direct {v2, p0}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;-><init>(Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;)V

    invoke-direct {v0, v1, v2}, LX/BeW;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1805023
    iput-object p1, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->o:Ljava/lang/String;

    .line 1805024
    iput-object p2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->p:Ljava/lang/String;

    .line 1805025
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1805026
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1805027
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->g:LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v3, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->FEATHER_OVERLAY_SHOWN:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v2, v3}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v3, LX/BeF;

    invoke-virtual {v0, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/BeF;

    .line 1805028
    iget-object v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->i:LX/0ad;

    sget-short v3, LX/BeA;->a:S

    invoke-interface {v2, v3, v1}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->q:Z

    .line 1805029
    invoke-virtual {p0}, LX/BeU;->a()V

    .line 1805030
    return-void

    :cond_0
    move v0, v1

    .line 1805031
    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1805019
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    .line 1805020
    iget v1, p0, LX/BeU;->b:I

    move v1, v1

    .line 1805021
    invoke-interface {v0, v1, p1}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    .line 1805022
    return-void
.end method

.method public final b(I)V
    .locals 6

    .prologue
    .line 1805005
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->b:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    const-string v2, "next_card"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1805006
    sget-object v0, LX/BeR;->a:[I

    invoke-direct {p0, p1}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->e(I)LX/BeS;

    move-result-object v1

    invoke-virtual {v1}, LX/BeS;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1805007
    :goto_0
    return-void

    .line 1805008
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->b:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    const-string v2, "intro"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    goto :goto_0

    .line 1805009
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->d:LX/31f;

    sget-object v2, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->m:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-object v3, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->o:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    iget-boolean v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    sub-int v0, p1, v0

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1805010
    iget-object v4, v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->a:Ljava/lang/String;

    const-string v5, "expanded_question_impression"

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p0

    invoke-static {v1, v4, v5, p0}, LX/31f;->a(LX/31f;Ljava/lang/String;Ljava/lang/String;LX/0am;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 1805011
    const-string v5, "endpoint"

    new-instance p0, LX/0mD;

    iget-object p1, v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;->b:Ljava/lang/String;

    invoke-direct {p0, p1}, LX/0mD;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1805012
    const-string v5, "question_id"

    invoke-virtual {v4, v5, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1805013
    iget-object v5, v1, LX/31f;->a:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1805014
    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 1805015
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->b:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    const-string v2, "thank_you"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1805016
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->d:LX/31f;

    const-string v1, "android_feather_suggest_edits_upsell"

    iget-object v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->o:Ljava/lang/String;

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/31f;->a(Ljava/lang/String;LX/0am;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getNumQuestions()I
    .locals 2

    .prologue
    .line 1805004
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v1, v0, 0x1

    iget-boolean v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
