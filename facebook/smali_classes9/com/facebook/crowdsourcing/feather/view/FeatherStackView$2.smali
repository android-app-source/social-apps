.class public final Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;


# direct methods
.method public constructor <init>(Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;)V
    .locals 0

    .prologue
    .line 1804901
    iput-object p1, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1804902
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->b:LX/0if;

    sget-object v1, LX/0ig;->au:LX/0ih;

    const-string v2, "suggest_edits_upsell_clicked"

    invoke-virtual {v0, v1, v2}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1804903
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->f:LX/17Y;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->aL:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    iget-object v5, v5, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->o:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "android_feather_suggest_edits_upsell"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1804904
    if-nez v0, :cond_0

    .line 1804905
    iget-object v0, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    iget-object v0, v0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->a:LX/03V;

    const-string v1, "feather"

    const-string v2, "Failed to resolve Suggest Edits intent!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1804906
    :goto_0
    return-void

    .line 1804907
    :cond_0
    const-string v1, "profile_name"

    iget-object v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    iget-object v2, v2, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1804908
    iget-object v1, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    iget-object v1, v1, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView$2;->a:Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/feather/view/FeatherStackView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
