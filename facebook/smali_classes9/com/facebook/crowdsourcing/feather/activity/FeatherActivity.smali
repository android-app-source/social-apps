.class public Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public p:LX/BeH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1804828
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;

    invoke-static {v0}, LX/BeH;->a(LX/0QB;)LX/BeH;

    move-result-object v0

    check-cast v0, LX/BeH;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->p:LX/BeH;

    return-void
.end method

.method private c(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 1804814
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "page_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1804815
    const-string v1, "questions"

    invoke-static {p1, v1}, LX/4By;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 1804816
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1804817
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1804818
    invoke-static {p0, p0}, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1804819
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1804820
    invoke-direct {p0, v1}, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->c(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1804821
    iget-object v2, p0, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->p:LX/BeH;

    new-instance v3, LX/BeI;

    const-string v0, "page_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v4, "entry_point"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/03g;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v0, v1}, LX/BeI;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "url_clicked"

    invoke-virtual {v2, v3, v0, p0}, LX/BeH;->a(LX/BeI;Ljava/lang/String;Landroid/content/Context;)V

    .line 1804822
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->onBackPressed()V

    .line 1804823
    :goto_0
    return-void

    .line 1804824
    :cond_0
    const v0, 0x7f03061d

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->setContentView(I)V

    .line 1804825
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    new-instance v2, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;

    invoke-direct {v2}, Lcom/facebook/crowdsourcing/feather/fragment/FeatherFragment;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 1804826
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    const v1, 0x1020002

    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/feather/activity/FeatherActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1804827
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
