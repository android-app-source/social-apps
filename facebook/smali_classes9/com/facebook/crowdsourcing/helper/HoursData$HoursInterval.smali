.class public final Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1805575
    new-instance v0, LX/Bey;

    invoke-direct {v0}, LX/Bey;-><init>()V

    sput-object v0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 1805580
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805581
    iput-wide p1, p0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    .line 1805582
    iput-wide p3, p0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    .line 1805583
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1805584
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805585
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    .line 1805586
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    .line 1805587
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1805579
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1805576
    iget-wide v0, p0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1805577
    iget-wide v0, p0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1805578
    return-void
.end method
