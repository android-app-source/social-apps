.class public Lcom/facebook/crowdsourcing/helper/HoursData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/crowdsourcing/helper/HoursData;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:[LX/Bex;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    .line 1805588
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v6, 0x7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static/range {v0 .. v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    .line 1805589
    new-instance v0, LX/Bew;

    invoke-direct {v0}, LX/Bew;-><init>()V

    sput-object v0, Lcom/facebook/crowdsourcing/helper/HoursData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 1805590
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805591
    sget-object v0, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [LX/Bex;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    .line 1805592
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 1805593
    iget-object v1, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    new-instance v2, LX/Bex;

    invoke-direct {v2}, LX/Bex;-><init>()V

    aput-object v2, v1, v0

    .line 1805594
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1805595
    :cond_0
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1805596
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1805597
    sget-object v0, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [LX/Bex;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    move v0, v1

    .line 1805598
    :goto_0
    iget-object v2, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1805599
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1805600
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    .line 1805601
    :goto_1
    sget-object v4, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1805602
    iget-object v4, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    new-instance v5, LX/Bex;

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-direct {v5, v3}, LX/Bex;-><init>(LX/0Px;)V

    aput-object v5, v4, v0

    .line 1805603
    iget-object v3, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    aget-object v3, v3, v0

    .line 1805604
    iput-boolean v2, v3, LX/Bex;->b:Z

    .line 1805605
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 1805606
    goto :goto_1

    .line 1805607
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(I)LX/Bex;
    .locals 1

    .prologue
    .line 1805608
    iget-object v0, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a(ILX/Bex;)V
    .locals 1

    .prologue
    .line 1805609
    iget-object v0, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    aput-object p2, v0, p1

    .line 1805610
    return-void
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1805611
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1805612
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1805613
    iget-object v2, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    aget-object v2, v2, v0

    .line 1805614
    iget-boolean p2, v2, LX/Bex;->b:Z

    move v2, p2

    .line 1805615
    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    int-to-byte v2, v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 1805616
    iget-object v2, p0, Lcom/facebook/crowdsourcing/helper/HoursData;->b:[LX/Bex;

    aget-object v2, v2, v0

    iget-object v2, v2, LX/Bex;->a:LX/0Px;

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1805617
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 1805618
    goto :goto_1

    .line 1805619
    :cond_1
    return-void
.end method
