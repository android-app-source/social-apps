.class public Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;
.super LX/BeU;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BeU",
        "<",
        "LX/BeP;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Beq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/Bep;

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1805553
    invoke-direct {p0, p1}, LX/BeU;-><init>(Landroid/content/Context;)V

    .line 1805554
    const-class v0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-static {v0, p0}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1805555
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1805556
    invoke-direct {p0, p1, p2}, LX/BeU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1805557
    const-class v0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-static {v0, p0}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1805558
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1805550
    invoke-direct {p0, p1, p2, p3}, LX/BeU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1805551
    const-class v0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-static {v0, p0}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1805552
    return-void
.end method

.method private static a(Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/util/concurrent/Executor;LX/Beq;)V
    .locals 0

    .prologue
    .line 1805549
    iput-object p1, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->a:Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    iput-object p2, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->b:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->c:LX/Beq;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;

    invoke-static {v2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b(LX/0QB;)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    const-class v3, LX/Beq;

    invoke-interface {v2, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Beq;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->a(Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/util/concurrent/Executor;LX/Beq;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V
    .locals 2

    .prologue
    .line 1805545
    invoke-virtual {p1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1805546
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->d:LX/Bep;

    new-instance v1, LX/Beu;

    invoke-direct {v1, p0, p1}, LX/Beu;-><init>(Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    .line 1805547
    invoke-static {v0}, LX/Bep;->e(LX/Bep;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    iget-object p1, v0, LX/Bep;->d:Ljava/util/concurrent/Executor;

    invoke-static {p0, v1, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1805548
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/BeP;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1805531
    iget v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Attempted to access questions out of order"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1805532
    iget v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    if-ne p1, v0, :cond_2

    .line 1805533
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->d:LX/Bep;

    .line 1805534
    iget-object v1, v0, LX/Bep;->f:LX/Ber;

    .line 1805535
    iget-boolean v2, v1, LX/Ber;->d:Z

    move v1, v2

    .line 1805536
    if-eqz v1, :cond_3

    .line 1805537
    const/4 v1, 0x0

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 1805538
    :goto_1
    move-object v0, v1

    .line 1805539
    :goto_2
    new-instance v1, LX/Bev;

    invoke-direct {v1, p0, p1}, LX/Bev;-><init>(Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;I)V

    iget-object v2, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 1805540
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1805541
    :cond_2
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->d:LX/Bep;

    invoke-virtual {v0}, LX/Bep;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_2

    .line 1805542
    :cond_3
    iget-object v1, v0, LX/Bep;->f:LX/Ber;

    invoke-virtual {v1}, LX/Ber;->c()I

    move-result v1

    if-nez v1, :cond_4

    .line 1805543
    invoke-virtual {v0}, LX/Bep;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_1

    .line 1805544
    :cond_4
    iget-object v1, v0, LX/Bep;->f:LX/Ber;

    invoke-virtual {v1}, LX/Ber;->e()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V
    .locals 9

    .prologue
    .line 1805517
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->c:LX/Beq;

    const/4 v1, 0x3

    .line 1805518
    new-instance v2, LX/Bep;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v2, v3, p1, v1}, LX/Bep;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    .line 1805519
    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    .line 1805520
    new-instance p1, LX/Ber;

    invoke-direct {p1}, LX/Ber;-><init>()V

    .line 1805521
    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    .line 1805522
    iput-object v8, p1, LX/Ber;->a:LX/03V;

    .line 1805523
    move-object v8, p1

    .line 1805524
    check-cast v8, LX/Ber;

    .line 1805525
    iput-object v3, v2, LX/Bep;->a:LX/1Ck;

    iput-object v4, v2, LX/Bep;->b:LX/0tX;

    iput-object v5, v2, LX/Bep;->c:LX/03V;

    iput-object v6, v2, LX/Bep;->d:Ljava/util/concurrent/Executor;

    iput-object v7, v2, LX/Bep;->e:LX/0Uh;

    iput-object v8, v2, LX/Bep;->f:LX/Ber;

    .line 1805526
    move-object v0, v2

    .line 1805527
    iput-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->d:LX/Bep;

    .line 1805528
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    .line 1805529
    invoke-static {p0, p2}, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->a$redex0(Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    .line 1805530
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1805511
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->d:LX/Bep;

    .line 1805512
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    .line 1805513
    iget-object v2, v0, LX/Bep;->j:Ljava/util/LinkedList;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    invoke-static {p0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p0

    invoke-virtual {v2, p0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 1805514
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1805515
    :cond_0
    iget v1, v0, LX/Bep;->m:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, LX/Bep;->m:I

    .line 1805516
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 1805503
    iget v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Top card changed out of order"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1805504
    iget v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    add-int/lit8 v0, v0, 0x1

    if-ne p1, v0, :cond_1

    .line 1805505
    iget v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->e:I

    .line 1805506
    :cond_1
    return-void

    .line 1805507
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getNumQuestions()I
    .locals 2

    .prologue
    .line 1805508
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/view/GraphEditorStackView;->d:LX/Bep;

    .line 1805509
    iget-object v1, v0, LX/Bep;->f:LX/Ber;

    invoke-virtual {v1}, LX/Ber;->c()I

    move-result v1

    iget p0, v0, LX/Bep;->m:I

    add-int/2addr v1, p0

    move v0, v1

    .line 1805510
    add-int/lit8 v0, v0, 0x1

    return v0
.end method
