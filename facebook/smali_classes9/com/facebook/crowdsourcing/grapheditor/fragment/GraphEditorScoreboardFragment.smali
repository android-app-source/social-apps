.class public Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:Landroid/widget/ListView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1805318
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x318efb6a    # -1.0109024E9f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1805319
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 1805320
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v3, p0

    check-cast v3, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;

    invoke-static {v10}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v10}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v10}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v10}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v8

    check-cast v8, Ljava/util/concurrent/Executor;

    new-instance p0, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;

    const-class v9, Landroid/content/Context;

    invoke-interface {v10, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/content/Context;

    invoke-direct {p0, v9}, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;-><init>(Landroid/content/Context;)V

    invoke-static {v10}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v9

    check-cast v9, Landroid/view/LayoutInflater;

    const/16 p3, 0x19e

    invoke-static {v10, p3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {v10}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object p3

    check-cast p3, LX/17Y;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    iput-object v9, p0, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->a:Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->c:LX/17Y;

    iput-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->d:Lcom/facebook/content/SecureContextHelper;

    move-object v9, p0

    check-cast v9, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;

    invoke-static {v10}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v10

    check-cast v10, Landroid/view/LayoutInflater;

    iput-object v5, v3, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->a:LX/0tX;

    iput-object v6, v3, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->b:LX/1Ck;

    iput-object v7, v3, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->c:LX/03V;

    iput-object v8, v3, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->d:Ljava/util/concurrent/Executor;

    iput-object v9, v3, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->e:Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;

    iput-object v10, v3, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->f:Landroid/view/LayoutInflater;

    .line 1805321
    const v1, 0x7f0307e4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x639f6eb5

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1805322
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1805323
    const v0, 0x7f0d14d2

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->g:Landroid/widget/ListView;

    .line 1805324
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->f:Landroid/view/LayoutInflater;

    const v2, 0x7f0307e5

    iget-object v3, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->g:Landroid/widget/ListView;

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    .line 1805325
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->g:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->e:Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1805326
    new-instance v0, LX/96m;

    invoke-direct {v0}, LX/96m;-><init>()V

    move-object v0, v0

    .line 1805327
    const-string v1, "profile_picture_size"

    const/4 v2, 0x1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1c86

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v2, v3, v4}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1805328
    const-string v1, "count"

    const/16 v2, 0xf

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1805329
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1805330
    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1805331
    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->b:LX/1Ck;

    const-string v2, "graph_editor_task_fetch_scoreboard"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1805332
    move-object v0, v0

    .line 1805333
    new-instance v1, LX/Bel;

    invoke-direct {v1, p0}, LX/Bel;-><init>(Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;)V

    iget-object v2, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorScoreboardFragment;->d:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1805334
    return-void
.end method
