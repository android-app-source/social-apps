.class public Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/48V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0WJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Lcom/facebook/webview/FacebookWebView;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1805371
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 1805372
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->d:Lcom/facebook/webview/FacebookWebView;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->e:Ljava/lang/String;

    .line 1805373
    iget-object v2, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->a:LX/48V;

    invoke-virtual {v2, v0, v1}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1805374
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x6d689dc2

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1805368
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 1805369
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;

    invoke-static {v1}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v3

    check-cast v3, LX/48V;

    invoke-static {v1}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object p3

    check-cast p3, LX/0WJ;

    invoke-static {v1}, LX/0WJ;->a(LX/0QB;)LX/0WJ;

    move-result-object v1

    check-cast v1, LX/0WJ;

    iput-object v3, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->a:LX/48V;

    iput-object p3, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->b:LX/0WJ;

    iput-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->c:LX/0WJ;

    .line 1805370
    const v1, 0x7f0307e8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x43fdb62a

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1805344
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1805345
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1805346
    if-eqz v0, :cond_1

    .line 1805347
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1805348
    const-string v3, "url"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 1805349
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1805350
    const-string v3, "url"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->e:Ljava/lang/String;

    .line 1805351
    const v0, 0x7f0d14e0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/webview/FacebookWebView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->d:Lcom/facebook/webview/FacebookWebView;

    .line 1805352
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->d:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, v1}, Lcom/facebook/webview/FacebookWebView;->setFocusable(Z)V

    .line 1805353
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->d:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, v1}, Lcom/facebook/webview/FacebookWebView;->setFocusableInTouchMode(Z)V

    .line 1805354
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->e:Ljava/lang/String;

    .line 1805355
    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->b:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->a()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    .line 1805356
    iget-object v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->c:Ljava/lang/String;

    move-object v1, v3

    .line 1805357
    if-eqz v1, :cond_0

    .line 1805358
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/facebook/auth/credentials/SessionCookie;->a(LX/0lC;Ljava/lang/String;)LX/0Px;

    move-result-object v1

    .line 1805359
    if-eqz v1, :cond_0

    .line 1805360
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/facebook/webview/FacebookWebView;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1805361
    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->c:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->i()V

    .line 1805362
    :cond_0
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->d:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, v2}, Lcom/facebook/webview/FacebookWebView;->setVerticalScrollBarEnabled(Z)V

    .line 1805363
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->d:Lcom/facebook/webview/FacebookWebView;

    invoke-virtual {v0, v2}, Lcom/facebook/webview/FacebookWebView;->setHorizontalScrollBarEnabled(Z)V

    .line 1805364
    iget-object v0, p0, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->d:Lcom/facebook/webview/FacebookWebView;

    new-instance v1, Landroid/webkit/WebViewClient;

    invoke-direct {v1}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v0, v1}, Lcom/facebook/webview/FacebookWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1805365
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/grapheditor/fragment/GraphEditorWebViewFragment;->b()V

    .line 1805366
    return-void

    :cond_1
    move v0, v2

    .line 1805367
    goto :goto_0
.end method
