.class public Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;
.super Landroid/widget/ArrayAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field public a:Landroid/view/LayoutInflater;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1805198
    const/4 v0, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 1805199
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1805200
    invoke-virtual {p0, p1}, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;

    .line 1805201
    if-nez p2, :cond_0

    .line 1805202
    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->a:Landroid/view/LayoutInflater;

    const v4, 0x7f0307e6

    invoke-virtual {v1, v4, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1805203
    :cond_0
    iget-object v1, p0, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1805204
    iget-object v4, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v4

    .line 1805205
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    .line 1805206
    if-nez v4, :cond_3

    .line 1805207
    const v1, 0x7f0d14d5

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1805208
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->j()I

    move-result v6

    .line 1805209
    :cond_1
    add-int/lit8 p1, p1, -0x1

    .line 1805210
    if-ltz p1, :cond_2

    invoke-virtual {p0, p1}, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;

    invoke-virtual {v5}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->j()I

    move-result v5

    if-eq v6, v5, :cond_1

    .line 1805211
    :cond_2
    add-int/lit8 v5, p1, 0x2

    move v5, v5

    .line 1805212
    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1805213
    :goto_0
    const v1, 0x7f0d14d6

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    if-eqz v4, :cond_4

    move v1, v2

    :goto_1
    invoke-virtual {v5, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1805214
    const v1, 0x7f0d14d5

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v4, :cond_5

    :goto_2
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1805215
    const v1, 0x7f0d14d9

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1805216
    const v1, 0x7f0d14da

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p2}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f0124

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->j()I

    move-result v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->j()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1805217
    const v1, 0x7f0d14d8

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;->m()Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const-class v3, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1805218
    new-instance v1, LX/Bef;

    invoke-direct {v1, p0, v0}, LX/Bef;-><init>(Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;Lcom/facebook/crowdsourcing/protocol/graphql/ScoreboardFragmentsModels$ScoreboardFriendFieldsModel;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805219
    return-object p2

    .line 1805220
    :cond_3
    const v1, 0x7f0d14d9

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00d2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1805221
    const v1, 0x7f0d14da

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/grapheditor/adapter/GraphEditorScoreboardAdapter;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0a00d2

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_4
    move v1, v3

    .line 1805222
    goto/16 :goto_1

    :cond_5
    move v3, v2

    .line 1805223
    goto/16 :goto_2
.end method
