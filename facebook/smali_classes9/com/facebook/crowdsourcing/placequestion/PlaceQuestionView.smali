.class public Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;
.super LX/BeP;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0hL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/view/View;

.field private k:LX/Bfd;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1806788
    invoke-direct {p0, p1}, LX/BeP;-><init>(Landroid/content/Context;)V

    .line 1806789
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->b()V

    .line 1806790
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1806785
    invoke-direct {p0, p1, p2}, LX/BeP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1806786
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->b()V

    .line 1806787
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1806782
    invoke-direct {p0, p1, p2, p3}, LX/BeP;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1806783
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->b()V

    .line 1806784
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/view/View$OnClickListener;ZI)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1806775
    iget v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030f79

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;

    .line 1806776
    :goto_0
    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->setAnswerLabel(Ljava/lang/String;)V

    .line 1806777
    invoke-virtual {v0, p2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1806778
    invoke-direct {p0, v0, p3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;Z)V

    .line 1806779
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1, v0, p4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->addView(Landroid/view/View;I)V

    .line 1806780
    return-object v0

    .line 1806781
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030f76

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;

    goto :goto_0
.end method

.method private a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;Z)V
    .locals 3

    .prologue
    .line 1806772
    invoke-direct {p0, p1, p2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->b(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;Z)V

    .line 1806773
    new-instance v0, LX/Bfu;

    iget v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->g:I

    iget v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->h:I

    invoke-direct {v0, v1, v2}, LX/Bfu;-><init>(II)V

    invoke-static {p1, v0}, LX/1r0;->a(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1806774
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v0

    check-cast v0, LX/0hL;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a:LX/0hL;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1806759
    const-class v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    invoke-static {v0, p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1806760
    invoke-virtual {p0, v2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->setFocusableInTouchMode(Z)V

    .line 1806761
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->g:I

    .line 1806762
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00ea

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->h:I

    .line 1806763
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c7e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->i:I

    .line 1806764
    invoke-virtual {p0, v2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->setOrientation(I)V

    .line 1806765
    const v0, 0x7f030f7a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1806766
    const v0, 0x7f0d2553

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->b:Landroid/view/View;

    .line 1806767
    const v0, 0x7f0d2554

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->c:Landroid/view/View;

    .line 1806768
    const v0, 0x7f0d2555

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->d:Landroid/view/View;

    .line 1806769
    const v0, 0x7f0d2556

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 1806770
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->c(I)V

    .line 1806771
    return-void
.end method

.method private b(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1806701
    iget v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->f:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1806702
    :goto_0
    return-void

    .line 1806703
    :cond_0
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->getChildCount()I

    move-result v1

    if-nez v1, :cond_2

    iget v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->i:I

    .line 1806704
    :goto_1
    if-eqz p2, :cond_1

    iget v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->i:I

    .line 1806705
    :cond_1
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v3

    .line 1806706
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->getPaddingLeft()I

    move-result v4

    if-eqz v3, :cond_3

    move v1, v0

    :goto_2
    add-int/2addr v1, v4

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->getPaddingRight()I

    move-result v5

    if-eqz v3, :cond_4

    :goto_3
    add-int v0, v5, v2

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->getPaddingBottom()I

    move-result v2

    invoke-virtual {p1, v1, v4, v0, v2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->setPadding(IIII)V

    goto :goto_0

    :cond_2
    move v2, v0

    .line 1806707
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1806708
    goto :goto_2

    :cond_4
    move v2, v0

    goto :goto_3
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1806754
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1806755
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->j:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->removeView(Landroid/view/View;)V

    .line 1806756
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->j:Landroid/view/View;

    .line 1806757
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1806758
    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 1806750
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    if-eqz v0, :cond_0

    .line 1806751
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->removeView(Landroid/view/View;)V

    .line 1806752
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    .line 1806753
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/view/View$OnClickListener;Z)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;
    .locals 1

    .prologue
    .line 1806749
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;ZI)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1806746
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    if-eqz v0, :cond_0

    .line 1806747
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    invoke-virtual {v0}, LX/Bfd;->a()V

    .line 1806748
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/view/View$OnClickListener;I)V
    .locals 2

    .prologue
    .line 1806742
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, p2, v0, p3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;ZI)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;

    move-result-object v0

    .line 1806743
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->setCheckBoxVisibility(I)V

    .line 1806744
    return-void

    .line 1806745
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 1806730
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->c()V

    .line 1806731
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->d()V

    .line 1806732
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->removeAllViews()V

    .line 1806733
    iput p1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->f:I

    .line 1806734
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->d:Landroid/view/View;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1806735
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setOrientation(I)V

    .line 1806736
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    if-nez p1, :cond_1

    const v0, 0x7f0b1c7d

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1806737
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->e:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {v1, v0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->b_(II)V

    .line 1806738
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1806739
    return-void

    .line 1806740
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 1806741
    :cond_1
    const v0, 0x7f0b1c7e

    goto :goto_1
.end method

.method public getSuggestionText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1806722
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 1806723
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    instance-of v0, v0, LX/Bft;

    invoke-static {v0}, LX/0Tp;->a(Z)V

    .line 1806724
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    check-cast v0, LX/Bft;

    .line 1806725
    iget-object p0, v0, LX/Bft;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1806726
    const-string p0, "<<not-applicable>>"

    .line 1806727
    :goto_1
    move-object v0, p0

    .line 1806728
    return-object v0

    .line 1806729
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object p0, v0, LX/Bft;->f:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {p0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1
.end method

.method public setContentPaddingTop(Z)V
    .locals 2

    .prologue
    .line 1806719
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1806720
    return-void

    .line 1806721
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setContentView(LX/Bfd;)V
    .locals 1

    .prologue
    .line 1806714
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->d()V

    .line 1806715
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->j:Landroid/view/View;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->addView(Landroid/view/View;I)V

    .line 1806716
    iput-object p1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->k:LX/Bfd;

    .line 1806717
    return-void

    .line 1806718
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public setHeaderView(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1806709
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->c()V

    .line 1806710
    invoke-virtual {p0, p1, v1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->addView(Landroid/view/View;I)V

    .line 1806711
    iput-object p1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->j:Landroid/view/View;

    .line 1806712
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1806713
    return-void
.end method
