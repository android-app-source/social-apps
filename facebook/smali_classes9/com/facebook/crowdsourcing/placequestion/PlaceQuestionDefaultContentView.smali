.class public Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;
.super LX/Bfd;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6Zi;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public g:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field public h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public i:Lcom/facebook/maps/FbStaticMapView;

.field public j:Lcom/facebook/android/maps/MapView;

.field public k:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field public l:Landroid/widget/TextView;

.field public m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public n:Landroid/widget/TextView;

.field public o:Landroid/widget/TextView;

.field public p:Landroid/view/View;

.field public q:Landroid/widget/ProgressBar;

.field private r:Landroid/animation/ValueAnimator;

.field public s:LX/BeT;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1806579
    const-class v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1806558
    invoke-direct {p0, p1}, LX/Bfd;-><init>(Landroid/content/Context;)V

    .line 1806559
    const/4 p1, 0x1

    .line 1806560
    const-class v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;

    invoke-static {v0, p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1806561
    invoke-virtual {p0, p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->setFocusableInTouchMode(Z)V

    .line 1806562
    const v0, 0x7f030f75

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1806563
    invoke-virtual {p0, p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->setOrientation(I)V

    .line 1806564
    const v0, 0x7f0d2540

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1806565
    const v0, 0x7f0d2541

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1806566
    const v0, 0x7f0d2542

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->g:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 1806567
    const v0, 0x7f0d2543

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1806568
    const v0, 0x7f0d2544

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->i:Lcom/facebook/maps/FbStaticMapView;

    .line 1806569
    const v0, 0x7f0d2545

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/android/maps/MapView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->j:Lcom/facebook/android/maps/MapView;

    .line 1806570
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->j:Lcom/facebook/android/maps/MapView;

    const/4 p1, 0x0

    invoke-virtual {v0, p1}, Lcom/facebook/android/maps/MapView;->a(Landroid/os/Bundle;)V

    .line 1806571
    const v0, 0x7f0d2546

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->k:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1806572
    const v0, 0x7f0d2548

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->l:Landroid/widget/TextView;

    .line 1806573
    const v0, 0x7f0d2547

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1806574
    const v0, 0x7f0d2549

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->n:Landroid/widget/TextView;

    .line 1806575
    const v0, 0x7f0d254a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->o:Landroid/widget/TextView;

    .line 1806576
    const v0, 0x7f0d254b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->p:Landroid/view/View;

    .line 1806577
    const v0, 0x7f0d254c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->q:Landroid/widget/ProgressBar;

    .line 1806578
    return-void
.end method

.method private static a(Ljava/lang/String;)LX/69B;
    .locals 10
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1806546
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    .line 1806547
    invoke-virtual {v1}, LX/0lF;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v0

    if-lez v0, :cond_1

    .line 1806548
    new-instance v2, LX/69B;

    invoke-direct {v2}, LX/69B;-><init>()V

    .line 1806549
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, LX/0lF;->e()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 1806550
    invoke-virtual {v1, v0}, LX/0lF;->a(I)LX/0lF;

    move-result-object v3

    .line 1806551
    invoke-virtual {v3}, LX/0lF;->i()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1806552
    new-instance v4, Lcom/facebook/android/maps/model/LatLng;

    const-string v5, "lat"

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-virtual {v5}, LX/0lF;->E()D

    move-result-wide v6

    const-string v5, "long"

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    invoke-virtual {v3}, LX/0lF;->E()D

    move-result-wide v8

    invoke-direct {v4, v6, v7, v8, v9}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    .line 1806553
    iget-object v3, v2, LX/69B;->c:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1806554
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1806555
    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid street points JSON "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1806556
    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid street points JSON "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1806557
    :cond_2
    return-object v2
.end method

.method private static a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1806539
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->q:Landroid/widget/ProgressBar;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 1806540
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->q:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 1806541
    const/4 v0, 0x2

    new-array v0, v0, [I

    aput v2, v0, v2

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->q:Landroid/widget/ProgressBar;

    invoke-virtual {v2}, Landroid/widget/ProgressBar;->getMax()I

    move-result v2

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->r:Landroid/animation/ValueAnimator;

    .line 1806542
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->r:Landroid/animation/ValueAnimator;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1806543
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->r:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Bff;

    invoke-direct {v1, p0}, LX/Bff;-><init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1806544
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->r:Landroid/animation/ValueAnimator;

    new-instance v1, LX/Bfg;

    invoke-direct {v1, p0, p2}, LX/Bfg;-><init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1806545
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;

    const/16 v2, 0x262e

    invoke-static {v1, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x259

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v1}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    iput-object v2, p1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a:LX/0Ot;

    iput-object p0, p1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->b:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->c:LX/0Uh;

    return-void
.end method

.method public static a(LX/175;)Z
    .locals 1
    .param p0    # LX/175;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1806538
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setPlacePositionMapVisible(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;Z)V
    .locals 2

    .prologue
    .line 1806535
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->i:Lcom/facebook/maps/FbStaticMapView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/maps/FbStaticMapView;->setVisibility(I)V

    .line 1806536
    return-void

    .line 1806537
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;
    .locals 13

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1806454
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->o()LX/175;

    move-result-object v0

    .line 1806455
    if-eqz v0, :cond_0

    .line 1806456
    :try_start_0
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_1

    .line 1806457
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a(LX/175;)Z

    move-result v1

    if-eqz v1, :cond_f

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1806458
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->gr_()LX/175;

    move-result-object v0

    .line 1806459
    if-eqz v0, :cond_1

    .line 1806460
    :try_start_1
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_2

    .line 1806461
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->f:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a(LX/175;)Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x0

    :goto_3
    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1806462
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->m()LX/175;

    move-result-object v0

    .line 1806463
    if-eqz v0, :cond_2

    .line 1806464
    :try_start_2
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->g:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_2
    .catch LX/47A; {:try_start_2 .. :try_end_2} :catch_3

    .line 1806465
    :cond_2
    :goto_4
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->g:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a(LX/175;)Z

    move-result v1

    if-eqz v1, :cond_11

    const/4 v1, 0x0

    :goto_5
    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1806466
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->gq_()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionImageModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1806467
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->gq_()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionImageModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1806468
    if-eqz v0, :cond_9

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_6
    invoke-static {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z

    move-result v1

    const/16 v6, 0x8

    const v3, 0x3faaaaab

    const/4 v5, 0x0

    .line 1806469
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v4, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1806470
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->c:LX/0Uh;

    const/16 v10, 0x31c

    invoke-virtual {v2, v10, v5}, LX/0Uh;->a(IZ)Z

    move-result v10

    .line 1806471
    iget-object v11, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v10, :cond_12

    if-nez v1, :cond_12

    move v2, v3

    :goto_7
    invoke-virtual {v11, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1806472
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    if-eqz v10, :cond_13

    if-nez v1, :cond_13

    move v2, v4

    :goto_8
    iput v2, v11, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1806473
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    if-eqz v10, :cond_14

    if-nez v1, :cond_14

    int-to-float v2, v4

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    :goto_9
    iput v2, v11, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1806474
    iget-object v3, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->p:Landroid/view/View;

    if-nez v10, :cond_3

    if-eqz v1, :cond_15

    :cond_3
    move v2, v6

    :goto_a
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1806475
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v3, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1806476
    iget-object v2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_16

    :goto_b
    invoke-virtual {v2, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1806477
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->k()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;

    move-result-object v10

    .line 1806478
    if-eqz v10, :cond_c

    .line 1806479
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e()LX/1k1;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1806480
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e()LX/1k1;

    move-result-object v0

    invoke-interface {v0}, LX/1k1;->a()D

    move-result-wide v2

    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e()LX/1k1;

    move-result-object v0

    invoke-interface {v0}, LX/1k1;->b()D

    move-result-wide v4

    const/16 v6, 0xd

    move-object v1, p0

    .line 1806481
    iget-object v0, v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->i:Lcom/facebook/maps/FbStaticMapView;

    new-instance v11, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v12, "places_feed_place_question"

    invoke-direct {v11, v12}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2, v3, v4, v5}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v11

    invoke-virtual {v11, v6}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 1806482
    new-instance v0, LX/Bfe;

    invoke-direct {v0, p0, v10}, LX/Bfe;-><init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;)V

    .line 1806483
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->i:Lcom/facebook/maps/FbStaticMapView;

    invoke-virtual {v1, v0}, Lcom/facebook/maps/FbStaticMapView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1806484
    :cond_5
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->e()LX/1k1;

    move-result-object v0

    if-eqz v0, :cond_a

    move v0, v8

    :goto_c
    invoke-static {p0, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->setPlacePositionMapVisible(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;Z)V

    .line 1806485
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 1806486
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->l:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806487
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->gs_()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 1806488
    if-eqz v0, :cond_6

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 1806489
    :cond_6
    if-nez v7, :cond_17

    .line 1806490
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020966

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1806491
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1806492
    :goto_d
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, ""

    .line 1806493
    :goto_e
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->n:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806494
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 1806495
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806496
    invoke-virtual {p0, v8}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->setPlaceDetailsVisible(Z)V

    .line 1806497
    :goto_f
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->l()Ljava/lang/String;

    move-result-object v2

    .line 1806498
    if-eqz v2, :cond_7

    .line 1806499
    :try_start_3
    invoke-static {v2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a(Ljava/lang/String;)LX/69B;

    move-result-object v0

    .line 1806500
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->j:Lcom/facebook/android/maps/MapView;

    new-instance v3, LX/Bfi;

    invoke-direct {v3, v0}, LX/Bfi;-><init>(LX/69B;)V

    invoke-virtual {v1, v3}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 1806501
    const/4 v0, 0x1

    .line 1806502
    iget-object v3, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->j:Lcom/facebook/android/maps/MapView;

    if-eqz v0, :cond_18

    const/4 v1, 0x0

    :goto_10
    invoke-virtual {v3, v1}, Lcom/facebook/android/maps/MapView;->setVisibility(I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 1806503
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->n()I

    move-result v0

    if-lez v0, :cond_d

    .line 1806504
    :goto_11
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->q:Landroid/widget/ProgressBar;

    if-eqz v8, :cond_e

    :goto_12
    invoke-virtual {v0, v9}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1806505
    if-eqz v8, :cond_8

    .line 1806506
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->n()I

    move-result v0

    invoke-static {p0, v0, p2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;ILjava/lang/String;)V

    .line 1806507
    :cond_8
    return-object p0

    :cond_9
    move-object v0, v7

    .line 1806508
    goto/16 :goto_6

    :cond_a
    move v0, v9

    .line 1806509
    goto/16 :goto_c

    .line 1806510
    :cond_b
    invoke-virtual {v10}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel$PlaceQuestionPlaceModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_e

    .line 1806511
    :cond_c
    invoke-static {p0, v9}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->setPlacePositionMapVisible(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;Z)V

    .line 1806512
    invoke-virtual {p0, v9}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->setPlaceDetailsVisible(Z)V

    goto :goto_f

    .line 1806513
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1806514
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v3, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception when processing street points JSON "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1806515
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_d
    move v8, v9

    .line 1806516
    goto :goto_11

    .line 1806517
    :cond_e
    const/16 v9, 0x8

    goto :goto_12

    .line 1806518
    :catch_1
    move-exception v1

    .line 1806519
    const-string v2, "PlaceQuestionView"

    invoke-virtual {v1}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1806520
    :cond_f
    const/16 v1, 0x8

    goto/16 :goto_1

    .line 1806521
    :catch_2
    move-exception v1

    .line 1806522
    const-string v2, "PlaceQuestionView"

    invoke-virtual {v1}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 1806523
    :cond_10
    const/16 v1, 0x8

    goto/16 :goto_3

    .line 1806524
    :catch_3
    move-exception v1

    .line 1806525
    const-string v2, "PlaceQuestionView"

    invoke-virtual {v1}, LX/47A;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    .line 1806526
    :cond_11
    const/16 v1, 0x8

    goto/16 :goto_5

    .line 1806527
    :cond_12
    const/high16 v2, 0x3f800000    # 1.0f

    goto/16 :goto_7

    .line 1806528
    :cond_13
    div-int/lit8 v2, v4, 0x2

    goto/16 :goto_8

    .line 1806529
    :cond_14
    div-int/lit8 v2, v4, 0x2

    goto/16 :goto_9

    :cond_15
    move v2, v5

    .line 1806530
    goto/16 :goto_a

    :cond_16
    move v5, v6

    .line 1806531
    goto/16 :goto_b

    .line 1806532
    :cond_17
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-class v1, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, v7, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1806533
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto/16 :goto_d

    .line 1806534
    :cond_18
    const/16 v1, 0x8

    goto/16 :goto_10
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1806448
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->r:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 1806449
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->r:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1806450
    :cond_0
    return-void
.end method

.method public setPlaceDetailsVisible(Z)V
    .locals 2

    .prologue
    .line 1806451
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->k:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 1806452
    return-void

    .line 1806453
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
