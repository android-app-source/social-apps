.class public Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0tX;

.field private d:LX/BeT;

.field private final e:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/concurrent/Executor;

.field public final g:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1806950
    const-class v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0tX;LX/1Ck;Ljava/util/concurrent/Executor;LX/03V;)V
    .locals 1
    .param p4    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/6Zi;",
            ">;",
            "LX/0tX;",
            "LX/1Ck;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1806943
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1806944
    iput-object p2, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->c:LX/0tX;

    .line 1806945
    iput-object p3, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->e:LX/1Ck;

    .line 1806946
    iput-object p4, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->f:Ljava/util/concurrent/Executor;

    .line 1806947
    iput-object p5, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->g:LX/03V;

    .line 1806948
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b:Ljava/util/Set;

    .line 1806949
    return-void
.end method

.method private a(LX/399;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/399",
            "<TT;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 1806940
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->c:LX/0tX;

    sget-object v1, LX/3Fz;->b:LX/3Fz;

    invoke-virtual {v0, p1, v1}, LX/0tX;->a(LX/399;LX/3Fz;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1806941
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->e:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "task_key_submit_answer"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1806942
    return-object v0
.end method

.method private a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionMutationsModels$PlaceQuestionAnswerSubmitModel;",
            ">;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1806939
    new-instance v0, LX/Bfz;

    invoke-direct {v0, p0}, LX/Bfz;-><init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;)V

    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->f:Ljava/util/concurrent/Executor;

    invoke-static {p1, v0, v1}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILjava/lang/String;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1806928
    new-instance v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2, p3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->d:LX/BeT;

    .line 1806929
    iput-object v1, v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionDefaultContentView;->s:LX/BeT;

    .line 1806930
    move-object v0, v0

    .line 1806931
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->setContentView(LX/Bfd;)V

    move v1, v2

    .line 1806932
    :goto_0
    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1806933
    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;

    .line 1806934
    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_0

    if-nez p4, :cond_0

    const/4 v3, 0x1

    .line 1806935
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel$PlaceQuestionAnswerLabelModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel$PlaceQuestionAnswerLabelModel;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v5, LX/Bfx;

    invoke-direct {v5, p0, p3, p5, v0}, LX/Bfx;-><init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;)V

    invoke-virtual {p1, v4, v5, v3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;Z)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;

    .line 1806936
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v3, v2

    .line 1806937
    goto :goto_1

    .line 1806938
    :cond_1
    return-void
.end method

.method private static a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;)Z
    .locals 1

    .prologue
    .line 1806927
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel$PlaceQuestionAnswerLabelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel$PlaceQuestionAnswerLabelModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel$PlaceQuestionAnswerLabelModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z
    .locals 5
    .param p0    # Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1806920
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->o()LX/175;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->o()LX/175;

    move-result-object v0

    invoke-interface {v0}, LX/175;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 1806921
    :goto_0
    return v0

    .line 1806922
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;

    .line 1806923
    invoke-static {v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionAnswerFieldsModel;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1806924
    goto :goto_0

    .line 1806925
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1806926
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;)Z
    .locals 1
    .param p0    # Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1806951
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->d()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->d()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1806909
    new-instance v0, LX/4IL;

    invoke-direct {v0}, LX/4IL;-><init>()V

    .line 1806910
    const-string v1, "place_question_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806911
    move-object v0, v0

    .line 1806912
    const-string v1, "REACTION"

    .line 1806913
    const-string v2, "surface"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806914
    move-object v0, v0

    .line 1806915
    new-instance v1, LX/97O;

    invoke-direct {v1}, LX/97O;-><init>()V

    move-object v1, v1

    .line 1806916
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1806917
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(LX/399;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1806918
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->d:LX/BeT;

    invoke-interface {v0}, LX/BeT;->kD_()V

    .line 1806919
    return-void
.end method

.method public static a$redex0(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1806896
    new-instance v0, LX/4IM;

    invoke-direct {v0}, LX/4IM;-><init>()V

    .line 1806897
    const-string v1, "place_question_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806898
    move-object v0, v0

    .line 1806899
    const-string v1, "place_question_answer_value"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806900
    move-object v0, v0

    .line 1806901
    const-string v1, "REACTION"

    .line 1806902
    const-string v2, "surface"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1806903
    move-object v0, v0

    .line 1806904
    new-instance v1, LX/97N;

    invoke-direct {v1}, LX/97N;-><init>()V

    move-object v1, v1

    .line 1806905
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1806906
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1806907
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->d:LX/BeT;

    invoke-direct {p0, v0, p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(LX/399;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-interface {v1, v0}, LX/BeT;->b(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 1806908
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;
    .locals 6

    .prologue
    .line 1806894
    new-instance v0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;

    const/16 v1, 0x262e

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;-><init>(LX/0Ot;LX/0tX;LX/1Ck;Ljava/util/concurrent/Executor;LX/03V;)V

    .line 1806895
    return-object v0
.end method

.method private b(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1806890
    if-nez p4, :cond_0

    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->d()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 1806891
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Bfy;

    invoke-direct {v2, p0, p3, p5}, LX/Bfy;-><init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1, v1, v2, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;I)V

    .line 1806892
    return-void

    .line 1806893
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static b(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z
    .locals 2
    .param p0    # Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1806889
    invoke-static {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->GRAPH_EDITOR_INFO_CARD:Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    invoke-virtual {v0, v1}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->n()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z
    .locals 1
    .param p0    # Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1806888
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILX/BeT;)V
    .locals 6

    .prologue
    .line 1806839
    new-instance v0, LX/Bfv;

    invoke-direct {v0, p0, p5}, LX/Bfv;-><init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;LX/BeT;)V

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->d:LX/BeT;

    .line 1806840
    invoke-virtual {p1, p4}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->c(I)V

    .line 1806841
    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 1806842
    sget-object v0, LX/Bg0;->a:[I

    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLPlaceQuestionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 1806843
    invoke-direct/range {v0 .. v5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILjava/lang/String;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 1806844
    invoke-direct/range {v0 .. v5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILjava/lang/String;)V

    .line 1806845
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 1806846
    invoke-direct/range {v0 .. v5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0

    .line 1806847
    :pswitch_1
    new-instance v0, LX/Bft;

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Bft;-><init>(Landroid/content/Context;)V

    .line 1806848
    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->e()LX/175;

    move-result-object v1

    .line 1806849
    if-eqz v1, :cond_0

    .line 1806850
    iget-object v2, v0, LX/Bft;->d:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-interface {v1}, LX/175;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbCheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 1806851
    :cond_0
    iget-object v3, v0, LX/Bft;->d:Lcom/facebook/resources/ui/FbCheckBox;

    if-eqz v1, :cond_3

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbCheckBox;->setVisibility(I)V

    .line 1806852
    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->m()LX/175;

    move-result-object v1

    .line 1806853
    if-eqz v1, :cond_1

    .line 1806854
    :try_start_0
    iget-object v2, v0, LX/Bft;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_0
    .catch LX/47A; {:try_start_0 .. :try_end_0} :catch_0

    .line 1806855
    :cond_1
    :goto_2
    iget-object v3, v0, LX/Bft;->e:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v1}, LX/Bft;->a(LX/175;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1806856
    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->o()LX/175;

    move-result-object v1

    .line 1806857
    if-eqz v1, :cond_2

    .line 1806858
    :try_start_1
    iget-object v2, v0, LX/Bft;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setTextWithEntities(LX/175;)V
    :try_end_1
    .catch LX/47A; {:try_start_1 .. :try_end_1} :catch_1

    .line 1806859
    :cond_2
    :goto_4
    iget-object v3, v0, LX/Bft;->c:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-static {v1}, LX/Bft;->a(LX/175;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    :goto_5
    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 1806860
    move-object v0, v0

    .line 1806861
    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080035

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Bfw;

    invoke-direct {v2, p0, p3, v5, p1}, LX/Bfw;-><init>(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;)V

    const/4 v3, 0x1

    invoke-virtual {p1, v1, v2, v3}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;Z)Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;

    move-result-object v1

    .line 1806862
    invoke-static {v0, v1}, LX/Bft;->b(LX/Bft;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;)V

    .line 1806863
    iget-object v2, v0, LX/Bft;->d:Lcom/facebook/resources/ui/FbCheckBox;

    new-instance v3, LX/Bfr;

    invoke-direct {v3, v0, v1}, LX/Bfr;-><init>(LX/Bft;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbCheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1806864
    iget-object v2, v0, LX/Bft;->f:Lcom/facebook/resources/ui/FbEditText;

    new-instance v3, LX/Bfs;

    invoke-direct {v3, v0, v1}, LX/Bfs;-><init>(LX/Bft;Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;)V

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1806865
    move-object v0, v0

    .line 1806866
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->setContentView(LX/Bfd;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    .line 1806867
    invoke-direct/range {v0 .. v5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->b(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILjava/lang/String;)V

    goto/16 :goto_0

    .line 1806868
    :cond_3
    const/16 v2, 0x8

    goto :goto_1

    .line 1806869
    :catch_0
    move-exception v2

    .line 1806870
    iget-object v3, v0, LX/Bft;->a:LX/03V;

    sget-object v4, LX/Bft;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 1806871
    :cond_4
    const/16 v2, 0x8

    goto :goto_3

    .line 1806872
    :catch_1
    move-exception v2

    .line 1806873
    iget-object v3, v0, LX/Bft;->a:LX/03V;

    sget-object v4, LX/Bft;->b:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_4

    .line 1806874
    :cond_5
    const/16 v2, 0x8

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILX/BeT;)V
    .locals 2

    .prologue
    .line 1806875
    invoke-virtual/range {p0 .. p5}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionViewController;->a(Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;Ljava/lang/String;ILX/BeT;)V

    .line 1806876
    new-instance v0, LX/Bfl;

    invoke-virtual {p1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Bfl;-><init>(Landroid/content/Context;)V

    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionFieldsModel;->c()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;

    move-result-object v1

    .line 1806877
    iget-object p0, v0, LX/Bfl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->b()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PlaceProfilePictureModel;->a()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    const-class p3, LX/Bfl;

    invoke-static {p3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p3

    invoke-virtual {p0, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1806878
    iget-object p0, v0, LX/Bfl;->m:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->d()LX/174;

    move-result-object p2

    invoke-interface {p2}, LX/174;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806879
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->c()LX/174;

    move-result-object p0

    if-eqz p0, :cond_1

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->c()LX/174;

    move-result-object p0

    invoke-interface {p0}, LX/174;->a()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 1806880
    iget-object p0, v0, LX/Bfl;->n:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->c()LX/174;

    move-result-object p2

    invoke-interface {p2}, LX/174;->a()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806881
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;->a()Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel$PageModel;->b()Ljava/lang/String;

    move-result-object p0

    if-eqz p0, :cond_0

    .line 1806882
    new-instance p0, LX/Bfj;

    invoke-direct {p0, v0, v1}, LX/Bfj;-><init>(LX/Bfl;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;)V

    invoke-virtual {v0, p0}, LX/Bfl;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1806883
    iget-object p0, v0, LX/Bfl;->o:Landroid/view/View;

    new-instance p2, LX/Bfk;

    invoke-direct {p2, v0, v1}, LX/Bfk;-><init>(LX/Bfl;Lcom/facebook/crowdsourcing/protocol/graphql/PlaceQuestionFragmentsModels$PlaceQuestionPlaceInfoFieldsModel;)V

    invoke-virtual {p0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1806884
    :cond_0
    move-object v0, v0

    .line 1806885
    invoke-virtual {p1, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionView;->setHeaderView(Landroid/view/View;)V

    .line 1806886
    return-void

    .line 1806887
    :cond_1
    iget-object p0, v0, LX/Bfl;->n:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p0, p2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
