.class public Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1806321
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1806322
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->a(Landroid/util/AttributeSet;)V

    .line 1806323
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1806315
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1806316
    invoke-direct {p0, p2}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->a(Landroid/util/AttributeSet;)V

    .line 1806317
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1806324
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, LX/03r;->PlaceQuestionAnswerView:[I

    invoke-virtual {v0, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1806325
    const/16 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    .line 1806326
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1806327
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->setOrientation(I)V

    .line 1806328
    const v0, 0x7f030f73

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1806329
    const v0, 0x7f0d253c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->a:Landroid/widget/TextView;

    .line 1806330
    const v0, 0x7f0d253d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->b:Landroid/widget/CheckBox;

    .line 1806331
    if-eqz v2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->setCheckBoxVisibility(I)V

    .line 1806332
    return-void

    .line 1806333
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 1806318
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setAnswerLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1806319
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806320
    return-void
.end method

.method public setCheckBoxVisibility(I)V
    .locals 2

    .prologue
    .line 1806302
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1806303
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->a:Landroid/widget/TextView;

    if-nez p1, :cond_0

    const v0, 0x800003

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 1806304
    return-void

    .line 1806305
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1806300
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1806301
    return-void
.end method

.method public setEnabled(Z)V
    .locals 3

    .prologue
    .line 1806306
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->setEnabled(Z)V

    .line 1806307
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1806308
    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1806309
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->a:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a010e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1806310
    return-void

    .line 1806311
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00e8

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0
.end method

.method public final toggle()V
    .locals 2

    .prologue
    .line 1806312
    iget-object v1, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->b:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/facebook/crowdsourcing/placequestion/PlaceQuestionAnswerView;->b:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1806313
    return-void

    .line 1806314
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
