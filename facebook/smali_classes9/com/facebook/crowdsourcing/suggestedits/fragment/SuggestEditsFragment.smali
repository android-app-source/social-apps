.class public Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public a:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/31f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/BgK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:Z

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private h:Landroid/widget/LinearLayout;

.field private i:Landroid/widget/ProgressBar;

.field private j:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1807898
    const-class v0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1807899
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v1

    check-cast v1, LX/03V;

    invoke-static {p0}, LX/31f;->a(LX/0QB;)LX/31f;

    move-result-object v2

    check-cast v2, LX/31f;

    invoke-static {p0}, LX/BgK;->b(LX/0QB;)LX/BgK;

    move-result-object p0

    check-cast p0, LX/BgK;

    iput-object v1, p1, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->a:LX/03V;

    iput-object v2, p1, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->b:LX/31f;

    iput-object p0, p1, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->c:LX/BgK;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 4

    .prologue
    .line 1807792
    iget-boolean v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->e:Z

    if-eqz v0, :cond_0

    .line 1807793
    const/4 v0, 0x0

    .line 1807794
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->c:LX/BgK;

    const/4 v1, 0x1

    .line 1807795
    iget-boolean v2, v0, LX/BgK;->B:Z

    if-nez v2, :cond_1

    .line 1807796
    const/4 v1, 0x0

    .line 1807797
    :goto_1
    move v0, v1

    .line 1807798
    goto :goto_0

    .line 1807799
    :cond_1
    new-instance v2, LX/31Y;

    iget-object v3, v0, LX/BgK;->d:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/31Y;-><init>(Landroid/content/Context;)V

    const v3, 0x7f082918

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f082919

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v2

    const v3, 0x7f08291a

    new-instance p0, LX/BgI;

    invoke-direct {p0, v0}, LX/BgI;-><init>(LX/BgK;)V

    invoke-virtual {v2, v3, p0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const v3, 0x7f08291b

    new-instance p0, LX/BgH;

    invoke-direct {p0, v0}, LX/BgH;-><init>(LX/BgK;)V

    invoke-virtual {v2, v3, p0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    invoke-virtual {v2}, LX/0ju;->a()LX/2EJ;

    move-result-object v2

    invoke-virtual {v2}, LX/2EJ;->show()V

    goto :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1807890
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1807891
    const-class v0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    invoke-static {v0, p0}, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1807892
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1807893
    const-string p1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->f:Ljava/lang/String;

    .line 1807894
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1807895
    const-string p1, "profile_name"

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->g:Ljava/lang/String;

    .line 1807896
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->e:Z

    .line 1807897
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1807885
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    .line 1807886
    if-eqz v0, :cond_0

    .line 1807887
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->e:Z

    .line 1807888
    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 1807889
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    .line 1807853
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->c:LX/BgK;

    .line 1807854
    iget-object v1, v0, LX/BgK;->b:LX/BfJ;

    iget-object v2, v0, LX/BgK;->x:LX/BgW;

    iget-object v3, v0, LX/BgK;->y:LX/0Px;

    iget-object p0, v0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    move v4, p1

    move v5, p2

    move-object v6, p3

    .line 1807855
    const/4 v0, -0x1

    if-eq v5, v0, :cond_0

    .line 1807856
    :goto_0
    return-void

    .line 1807857
    :cond_0
    invoke-static {v4}, LX/BeD;->fromOrdinal(I)LX/BeD;

    move-result-object p2

    .line 1807858
    if-eqz p2, :cond_1

    iget-object v0, v1, LX/BfJ;->d:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1807859
    :cond_1
    iget-object v0, v1, LX/BfJ;->b:LX/03V;

    sget-object p1, LX/BfJ;->a:Ljava/lang/String;

    const-string p2, "No picker matches the field type"

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1807860
    :cond_2
    iget-object v0, v1, LX/BfJ;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 1807861
    iget-object v0, v1, LX/BfJ;->b:LX/03V;

    sget-object p1, LX/BfJ;->a:Ljava/lang/String;

    const-string p2, "There was no caller stored when the picker activity returned"

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1807862
    :cond_3
    iget-object v0, v1, LX/BfJ;->e:Ljava/lang/String;

    .line 1807863
    if-nez v3, :cond_7

    .line 1807864
    iget-object p1, v1, LX/BfJ;->b:LX/03V;

    sget-object p3, LX/BfJ;->a:Ljava/lang/String;

    const-string v4, "Could not handle picker activity result because field holders are not set."

    invoke-virtual {p1, p3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1807865
    const/4 p1, 0x0

    .line 1807866
    :goto_1
    move-object p3, p1

    .line 1807867
    invoke-virtual {p3}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1807868
    invoke-virtual {p3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BgT;

    .line 1807869
    iget-object p1, v0, LX/BgT;->i:LX/97f;

    move-object p1, p1

    .line 1807870
    invoke-virtual {p3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BgS;

    move-object v2, v0

    .line 1807871
    :goto_2
    iget-object v0, v1, LX/BfJ;->d:LX/0P1;

    invoke-virtual {v0, p2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BfC;

    invoke-interface {v0, v6, p1}, LX/BfC;->a(Landroid/content/Intent;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 1807872
    sget-object p1, LX/BeD;->PHOTO_PICKER:LX/BeD;

    invoke-virtual {p2, p1}, LX/BeD;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    .line 1807873
    new-instance p1, LX/31Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, LX/31Y;-><init>(Landroid/content/Context;)V

    const p2, 0x7f081700

    invoke-virtual {p1, p2}, LX/0ju;->b(I)LX/0ju;

    move-result-object p1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object p1

    const p2, 0x7f081701

    new-instance p3, LX/BfI;

    invoke-direct {p3, v1, v2, v0}, LX/BfI;-><init>(LX/BfJ;LX/BgS;Ljava/lang/Object;)V

    invoke-virtual {p1, p2, p3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p1

    const p2, 0x7f081702

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p1

    invoke-virtual {p1}, LX/0ju;->a()LX/2EJ;

    move-result-object p1

    invoke-virtual {p1}, LX/2EJ;->show()V

    .line 1807874
    goto/16 :goto_0

    .line 1807875
    :cond_4
    if-eqz v2, :cond_5

    iget-object v0, v1, LX/BfJ;->e:Ljava/lang/String;

    invoke-virtual {v2}, LX/BgW;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1807876
    invoke-virtual {v2}, LX/BgW;->c()Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    move-result-object v0

    move-object p1, v0

    .line 1807877
    goto :goto_2

    .line 1807878
    :cond_5
    iget-object v0, v1, LX/BfJ;->b:LX/03V;

    sget-object p1, LX/BfJ;->a:Ljava/lang/String;

    const-string p2, "Unable to determine correct field to handle picker activity result"

    invoke-virtual {v0, p1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1807879
    :cond_6
    invoke-interface {v2, v0}, LX/BgS;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1807880
    :cond_7
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 p1, 0x0

    move p3, p1

    :goto_3
    if-ge p3, v4, :cond_9

    invoke-virtual {v3, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/BgT;

    .line 1807881
    invoke-virtual {p1}, LX/BgT;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 1807882
    invoke-static {p1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object p1

    goto/16 :goto_1

    .line 1807883
    :cond_8
    add-int/lit8 p1, p3, 0x1

    move p3, p1

    goto :goto_3

    .line 1807884
    :cond_9
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object p1

    goto/16 :goto_1
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x56964d86

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1807900
    const v0, 0x7f031418

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1807901
    const v0, 0x7f0d2e23

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->h:Landroid/widget/LinearLayout;

    .line 1807902
    const v0, 0x7f0d2e24

    invoke-static {v2, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->i:Landroid/widget/ProgressBar;

    .line 1807903
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "entry_point"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1807904
    if-nez v0, :cond_0

    .line 1807905
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->a:LX/03V;

    sget-object v3, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->d:Ljava/lang/String;

    const-string v4, "Suggest Edits launched without entrypoint"

    invoke-virtual {v0, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1807906
    const-string v0, "unknown"

    .line 1807907
    :cond_0
    new-instance v3, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    const-string v4, "android_suggest_edits_v2"

    invoke-direct {v3, v0, v4}, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->j:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 1807908
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->b:LX/31f;

    iget-object v3, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->j:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-object v4, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->f:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, LX/31f;->a(Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;)V

    .line 1807909
    const/16 v0, 0x2b

    const v3, 0x11771ffe

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3a8669cc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1807849
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1807850
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->b:LX/31f;

    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->j:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-object v3, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->f:Ljava/lang/String;

    .line 1807851
    const-string p0, "session_ended"

    invoke-static {v1, v2, p0, v3}, LX/31f;->b(LX/31f;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;Ljava/lang/String;Ljava/lang/String;)V

    .line 1807852
    const/16 v1, 0x2b

    const v2, -0x657ffd40

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7f3ed648

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1807847
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1807848
    const/16 v1, 0x2b

    const v2, 0x395d8218

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x59f94355

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1807836
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1807837
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1807838
    if-eqz v0, :cond_0

    .line 1807839
    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->c:LX/BgK;

    .line 1807840
    iput-object v0, v2, LX/BgK;->q:LX/1ZF;

    .line 1807841
    const/4 v4, 0x0

    invoke-interface {v0, v4}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 1807842
    iget-object v4, v2, LX/BgK;->q:LX/1ZF;

    const p0, 0x7f082902

    invoke-interface {v4, p0}, LX/1ZF;->x_(I)V

    .line 1807843
    iget-object v4, v2, LX/BgK;->q:LX/1ZF;

    const/4 p0, 0x1

    invoke-interface {v4, p0}, LX/1ZF;->k_(Z)V

    .line 1807844
    iget-object v4, v2, LX/BgK;->q:LX/1ZF;

    new-instance p0, LX/BgC;

    invoke-direct {p0, v2}, LX/BgC;-><init>(LX/BgK;)V

    invoke-interface {v4, p0}, LX/1ZF;->a(LX/63W;)V

    .line 1807845
    invoke-static {v2}, LX/BgK;->e(LX/BgK;)V

    .line 1807846
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x6dcc6330

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1807820
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1807821
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->c:LX/BgK;

    .line 1807822
    iget-object v1, v0, LX/BgK;->b:LX/BfJ;

    .line 1807823
    const-string v2, "state_caller_field_type"

    iget-object v3, v1, LX/BfJ;->e:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1807824
    const-string v1, "state_header"

    iget-object v2, v0, LX/BgK;->x:LX/BgW;

    .line 1807825
    iget-object v3, v2, LX/BgW;->e:Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;

    move-object v2, v3

    .line 1807826
    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1807827
    const-string v1, "state_original_sections"

    iget-object v2, v0, LX/BgK;->w:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsSectionsModel;

    invoke-static {p1, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1807828
    iget-object v1, v0, LX/BgK;->y:LX/0Px;

    if-eqz v1, :cond_1

    .line 1807829
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1807830
    iget-object v1, v0, LX/BgK;->y:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v1, v0, LX/BgK;->y:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BgT;

    .line 1807831
    iget-object p0, v1, LX/BgT;->i:LX/97f;

    move-object v1, p0

    .line 1807832
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1807833
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 1807834
    :cond_0
    const-string v1, "state_sections"

    invoke-static {p1, v1, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 1807835
    :cond_1
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1807805
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1807806
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->c:LX/BgK;

    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->j:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    iget-object v3, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->g:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->h:Landroid/widget/LinearLayout;

    iget-object v6, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->i:Landroid/widget/ProgressBar;

    move-object v1, p2

    move-object v7, p0

    .line 1807807
    iput-object v2, v0, LX/BgK;->p:Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 1807808
    iput-object v3, v0, LX/BgK;->r:Ljava/lang/String;

    .line 1807809
    iput-object v4, v0, LX/BgK;->s:Ljava/lang/String;

    .line 1807810
    iput-object v7, v0, LX/BgK;->t:Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;

    .line 1807811
    iput-object v5, v0, LX/BgK;->u:Landroid/widget/LinearLayout;

    .line 1807812
    iput-object v6, v0, LX/BgK;->v:Landroid/widget/ProgressBar;

    .line 1807813
    new-instance p0, LX/BgA;

    invoke-direct {p0, v0}, LX/BgA;-><init>(LX/BgK;)V

    iput-object p0, v0, LX/BgK;->z:LX/Bg9;

    .line 1807814
    new-instance p0, LX/BgB;

    invoke-direct {p0, v0}, LX/BgB;-><init>(LX/BgK;)V

    iput-object p0, v0, LX/BgK;->A:LX/Bg9;

    .line 1807815
    iget-object p0, v0, LX/BgK;->b:LX/BfJ;

    .line 1807816
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, LX/BfJ;->e:Ljava/lang/String;

    .line 1807817
    const/4 p0, 0x0

    iput-boolean p0, v0, LX/BgK;->B:Z

    .line 1807818
    return-void

    .line 1807819
    :cond_0
    const-string v2, "state_caller_field_type"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x859d93a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1807800
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 1807801
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/fragment/SuggestEditsFragment;->c:LX/BgK;

    .line 1807802
    invoke-static {v1, p1}, LX/BgK;->c(LX/BgK;Landroid/os/Bundle;)V

    .line 1807803
    invoke-static {v1, p1}, LX/BgK;->d(LX/BgK;Landroid/os/Bundle;)V

    .line 1807804
    const/16 v1, 0x2b

    const v2, -0xa4ad60e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
