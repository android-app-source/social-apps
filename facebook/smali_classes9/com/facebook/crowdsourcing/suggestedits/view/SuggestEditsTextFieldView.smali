.class public Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:Landroid/widget/EditText;

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/widget/ImageView;

.field private d:I

.field private e:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1808493
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1808494
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808495
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1808490
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808491
    invoke-direct {p0, p1, p2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808492
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1808436
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1808437
    invoke-direct {p0, p1, p2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808438
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1808477
    const v0, 0x7f031427

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1808478
    invoke-virtual {p0, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setOrientation(I)V

    .line 1808479
    const v0, 0x7f0d2e36

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    .line 1808480
    const v0, 0x7f0d2e2c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1808481
    const v0, 0x7f0d2e37

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    .line 1808482
    sget-object v0, LX/03r;->SuggestEditsTextFieldView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1808483
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1808484
    invoke-virtual {p0, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIconVisibility(I)V

    .line 1808485
    :goto_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonVisible(Z)V

    .line 1808486
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1808487
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c6f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->d:I

    .line 1808488
    return-void

    .line 1808489
    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIconVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1808467
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808468
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808469
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808470
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1808471
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808472
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIconVisibility(I)V

    .line 1808473
    invoke-virtual {p0, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonVisible(Z)V

    .line 1808474
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808475
    invoke-virtual {p0, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFocusable(Z)V

    .line 1808476
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1808464
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1808465
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1808466
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808461
    if-eqz p1, :cond_0

    .line 1808462
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1808463
    :cond_0
    return-void
.end method

.method public getActionButtonView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1808460
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    return-object v0
.end method

.method public getFieldValueView()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 1808459
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1808458
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 1808452
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomLinearLayout;->onLayout(ZIIII)V

    .line 1808453
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1808454
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 1808455
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingStart()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->d:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/EditText;->setPaddingRelative(IIII)V

    .line 1808456
    :cond_0
    :goto_0
    return-void

    .line 1808457
    :cond_1
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    iget v4, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->d:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/EditText;->setPadding(IIII)V

    goto :goto_0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1808450
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808451
    return-void
.end method

.method public setActionButtonVisible(Z)V
    .locals 2

    .prologue
    .line 1808447
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1808448
    return-void

    .line 1808449
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setFieldHintText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808414
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    .line 1808415
    return-void
.end method

.method public setFieldOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808416
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFocusable(Z)V

    .line 1808417
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808418
    return-void

    .line 1808419
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFieldText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808420
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-static {p1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1808421
    return-void
.end method

.method public setFieldTextFocusable(Z)V
    .locals 1

    .prologue
    .line 1808422
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1808423
    return-void
.end method

.method public setFieldTextLongClickable(Z)V
    .locals 1

    .prologue
    .line 1808424
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setLongClickable(Z)V

    .line 1808425
    return-void
.end method

.method public setFocusable(Z)V
    .locals 2

    .prologue
    .line 1808426
    if-eqz p1, :cond_0

    .line 1808427
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 1808428
    :goto_0
    return-void

    .line 1808429
    :cond_0
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    goto :goto_0
.end method

.method public setIcon(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808430
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v1, "crowdsourcing_edit"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1808431
    return-void
.end method

.method public setIconVisibility(I)V
    .locals 1

    .prologue
    .line 1808432
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1808433
    return-void
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 1808434
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1808435
    return-void
.end method

.method public setTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 2
    .param p1    # Landroid/text/TextWatcher;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808439
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->e:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 1808440
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808441
    :cond_0
    iput-object p1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->e:Landroid/text/TextWatcher;

    .line 1808442
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->e:Landroid/text/TextWatcher;

    if-eqz v0, :cond_1

    .line 1808443
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->e:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808444
    :cond_1
    return-void
.end method

.method public setTextFieldTag(I)V
    .locals 2

    .prologue
    .line 1808445
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a:Landroid/widget/EditText;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTag(Ljava/lang/Object;)V

    .line 1808446
    return-void
.end method
