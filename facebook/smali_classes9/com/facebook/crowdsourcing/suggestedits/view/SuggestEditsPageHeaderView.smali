.class public Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field private e:Landroid/view/View;

.field public f:Landroid/widget/EditText;

.field private g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private h:Landroid/widget/FrameLayout;

.field private i:LX/Bgn;

.field private j:Landroid/text/TextWatcher;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1808349
    const-class v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1808346
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1808347
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->c()V

    .line 1808348
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1808409
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808410
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->c()V

    .line 1808411
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1808406
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1808407
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->c()V

    .line 1808408
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1808395
    const v0, 0x7f031419

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1808396
    const v0, 0x7f0d2e28

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1808397
    const v0, 0x7f0d2e25

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->c:Landroid/widget/TextView;

    .line 1808398
    const v0, 0x7f0d2e2b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->d:Landroid/widget/TextView;

    .line 1808399
    const v0, 0x7f0d2e2a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->e:Landroid/view/View;

    .line 1808400
    const v0, 0x7f0d2e29

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    .line 1808401
    const v0, 0x7f0d2e26

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1808402
    const v0, 0x7f0d2e27

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->h:Landroid/widget/FrameLayout;

    .line 1808403
    new-instance v0, LX/Bgn;

    invoke-direct {v0}, LX/Bgn;-><init>()V

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->i:LX/Bgn;

    .line 1808404
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->i:LX/Bgn;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1808405
    return-void
.end method

.method public static d$redex0(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V
    .locals 2

    .prologue
    .line 1808392
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->j:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 1808393
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->j:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808394
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V
    .locals 2

    .prologue
    .line 1808389
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->j:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    .line 1808390
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->j:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808391
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1808386
    invoke-static {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->e(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    .line 1808387
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->j:Landroid/text/TextWatcher;

    .line 1808388
    return-void
.end method

.method public final a(Landroid/text/Spanned;Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1    # Landroid/text/Spanned;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808380
    if-nez p1, :cond_0

    .line 1808381
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1808382
    :goto_0
    return-void

    .line 1808383
    :cond_0
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1808384
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1808385
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final a(Landroid/text/TextWatcher;)V
    .locals 0

    .prologue
    .line 1808377
    iput-object p1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->j:Landroid/text/TextWatcher;

    .line 1808378
    invoke-static {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->d$redex0(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    .line 1808379
    return-void
.end method

.method public final a(Landroid/view/View$OnFocusChangeListener;)V
    .locals 1

    .prologue
    .line 1808412
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->i:LX/Bgn;

    invoke-virtual {v0, p1}, LX/Bgn;->a(Landroid/view/View$OnFocusChangeListener;)V

    .line 1808413
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1808375
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->i:LX/Bgn;

    invoke-virtual {v0}, LX/Bgn;->a()V

    .line 1808376
    return-void
.end method

.method public getPageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1808374
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAddPhotoImageResource(I)V
    .locals 1

    .prologue
    .line 1808372
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1808373
    return-void
.end method

.method public setCameraButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1808370
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808371
    return-void
.end method

.method public setPageName(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808368
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    invoke-static {p1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 1808369
    return-void
.end method

.method public setPageNameEditable(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1808359
    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->d:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1808360
    if-nez p1, :cond_1

    .line 1808361
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 1808362
    :goto_1
    return-void

    .line 1808363
    :cond_0
    const/16 v0, 0x8

    goto :goto_0

    .line 1808364
    :cond_1
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->f:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    .line 1808365
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 1808366
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->d:Landroid/widget/TextView;

    new-instance v1, LX/Bgl;

    invoke-direct {v1, p0}, LX/Bgl;-><init>(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808367
    new-instance v0, LX/Bgm;

    invoke-direct {v0, p0}, LX/Bgm;-><init>(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->a(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_1
.end method

.method public setPhoto(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808355
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 1808356
    :goto_0
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->g:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1808357
    return-void

    .line 1808358
    :cond_0
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public setPhotoEditable(Z)V
    .locals 2

    .prologue
    .line 1808352
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1808353
    return-void

    .line 1808354
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setPhotoGradientVisibility(I)V
    .locals 1

    .prologue
    .line 1808350
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsPageHeaderView;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1808351
    return-void
.end method
