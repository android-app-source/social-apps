.class public Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1808076
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1808077
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->b()V

    .line 1808078
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1808079
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808080
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->b()V

    .line 1808081
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1808061
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1808062
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->b()V

    .line 1808063
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1808072
    const v0, 0x7f03142d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1808073
    const v0, 0x7f0d2e3c    # 1.876612E38f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->a:Landroid/widget/TextView;

    .line 1808074
    const v0, 0x7f0d2e3b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->b:Landroid/widget/TextView;

    .line 1808075
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1808069
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08290d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1808070
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1808071
    return-void
.end method

.method public setDayLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1808067
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1808068
    return-void
.end method

.method public setHours(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1808064
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1808065
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->a:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a010d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1808066
    return-void
.end method
