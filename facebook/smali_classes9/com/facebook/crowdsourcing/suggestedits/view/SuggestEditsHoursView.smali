.class public Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/LinearLayout;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1808082
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1808083
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->c()V

    .line 1808084
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1808130
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808131
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->c()V

    .line 1808132
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1808127
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1808128
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->c()V

    .line 1808129
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1808122
    const v0, 0x7f03141c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1808123
    const v0, 0x7f0d2e2e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->a:Landroid/widget/TextView;

    .line 1808124
    const v0, 0x7f0d2e2d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b:Landroid/widget/LinearLayout;

    .line 1808125
    const v0, 0x7f0d2e2c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1808126
    return-void
.end method

.method private d()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;
    .locals 3

    .prologue
    .line 1808120
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808121
    const v1, 0x7f03141d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;
    .locals 2

    .prologue
    .line 1808113
    new-instance v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;-><init>(Landroid/content/Context;)V

    .line 1808114
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonVisible(Z)V

    .line 1808115
    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808116
    invoke-virtual {p0, p2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->setFieldIcon(Ljava/lang/String;)V

    .line 1808117
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIconVisibility(I)V

    .line 1808118
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1808119
    return-object v0
.end method

.method public final a()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1808107
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 1808108
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1808109
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1808110
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1808111
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808112
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 1808102
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808103
    const v1, 0x7f031426

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 1808104
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808105
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1808106
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1808097
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->d()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;

    move-result-object v0

    .line 1808098
    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->setDayLabel(Ljava/lang/String;)V

    .line 1808099
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->a()V

    .line 1808100
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1808101
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1808094
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1808095
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1808096
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1808089
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->d()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;

    move-result-object v0

    .line 1808090
    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->setDayLabel(Ljava/lang/String;)V

    .line 1808091
    invoke-virtual {v0, p2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursForSingleDayView;->setHours(Ljava/lang/String;)V

    .line 1808092
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1808093
    return-void
.end method

.method public setFieldIcon(Ljava/lang/String;)V
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808085
    if-nez p1, :cond_0

    .line 1808086
    :goto_0
    return-void

    .line 1808087
    :cond_0
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1808088
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsHoursView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "crowdsourcing_edit"

    invoke-static {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method
