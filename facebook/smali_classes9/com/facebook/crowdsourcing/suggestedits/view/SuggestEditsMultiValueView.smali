.class public Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1808313
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1808314
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->b()V

    .line 1808315
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1808310
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808311
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->b()V

    .line 1808312
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1808307
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1808308
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->b()V

    .line 1808309
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1808298
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808299
    const v1, 0x7f031428

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808300
    invoke-virtual {v0, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldTextFocusable(Z)V

    .line 1808301
    invoke-virtual {v0, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldTextLongClickable(Z)V

    .line 1808302
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonVisible(Z)V

    .line 1808303
    invoke-virtual {v0, p2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808304
    invoke-virtual {v0, p2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808305
    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIcon(Ljava/lang/String;)V

    .line 1808306
    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1808265
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->addView(Landroid/view/View;I)V

    .line 1808266
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1808290
    const v0, 0x7f031422

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1808291
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->setOrientation(I)V

    .line 1808292
    const v0, 0x7f0d2e32

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a:Landroid/widget/TextView;

    .line 1808293
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0218ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->b:Landroid/graphics/drawable/Drawable;

    .line 1808294
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08291e    # 1.809885E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->d:Ljava/lang/String;

    .line 1808295
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0218d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->c:Landroid/graphics/drawable/Drawable;

    .line 1808296
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08291d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->e:Ljava/lang/String;

    .line 1808297
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1808287
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->removeViews(II)V

    .line 1808288
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808289
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 1808282
    invoke-direct {p0, p2, p3}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-result-object v0

    .line 1808283
    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808284
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->b:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 1808285
    invoke-direct {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a(Landroid/view/View;)V

    .line 1808286
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1808275
    invoke-direct {p0, p2, p3}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-result-object v0

    .line 1808276
    new-instance v1, Landroid/text/SpannableString;

    invoke-direct {v1, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1808277
    new-instance v2, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v2}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {v1}, Landroid/text/SpannableString;->length()I

    move-result v3

    invoke-virtual {v1, v2, v4, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1808278
    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808279
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->c:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 1808280
    invoke-direct {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a(Landroid/view/View;)V

    .line 1808281
    return-void
.end method

.method public setAddText(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808269
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1808270
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1808271
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a:Landroid/widget/TextView;

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1808272
    return-void

    .line 1808273
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1808274
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setAddValueListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1808267
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiValueView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808268
    return-void
.end method
