.class public Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

.field private b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

.field private c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

.field private d:Lcom/facebook/maps/FbStaticMapView;

.field private final e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field private f:Landroid/location/Location;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1808202
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1808203
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "suggest_edits"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1808204
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b()V

    .line 1808205
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1808198
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808199
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "suggest_edits"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1808200
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b()V

    .line 1808201
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1808194
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1808195
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "suggest_edits"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 1808196
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b()V

    .line 1808197
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1808185
    const v0, 0x7f03141f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1808186
    const v0, 0x7f0d2e2f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808187
    const v0, 0x7f0d2e30

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808188
    const v0, 0x7f0d2e31

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808189
    const v0, 0x7f0d1dac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/maps/FbStaticMapView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->d:Lcom/facebook/maps/FbStaticMapView;

    .line 1808190
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    const v1, 0x7f0d2e2f

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextFieldTag(I)V

    .line 1808191
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    const v1, 0x7f0d2e30

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextFieldTag(I)V

    .line 1808192
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    const v1, 0x7f0d2e31

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextFieldTag(I)V

    .line 1808193
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1808172
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808173
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808174
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808175
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808176
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808177
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808178
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808179
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setStreetAddressFieldFocusable(Z)V

    .line 1808180
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1808181
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808182
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808183
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808184
    return-void
.end method

.method public getActionButtonView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1808161
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808162
    iget-object p0, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->c:Landroid/widget/ImageView;

    move-object v0, p0

    .line 1808163
    return-object v0
.end method

.method public getStreetAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1808171
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getZip()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1808170
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1808168
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808169
    return-void
.end method

.method public setCity(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808166
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808167
    return-void
.end method

.method public setCityHintText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808164
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808165
    return-void
.end method

.method public setCityOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808206
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808207
    return-void
.end method

.method public setIcon(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808147
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "crowdsourcing_edit"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1808148
    return-void
.end method

.method public setMapLocation(Landroid/location/Location;)V
    .locals 3
    .param p1    # Landroid/location/Location;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808133
    if-nez p1, :cond_1

    .line 1808134
    :cond_0
    :goto_0
    return-void

    .line 1808135
    :cond_1
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->f:Landroid/location/Location;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->f:Landroid/location/Location;

    invoke-virtual {p1, v0}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v0

    const v1, 0x3a83126f    # 0.001f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 1808136
    :cond_2
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->d:Lcom/facebook/maps/FbStaticMapView;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->e:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a()Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(Landroid/location/Location;)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3BP;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 1808137
    iput-object p1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->f:Landroid/location/Location;

    goto :goto_0
.end method

.method public setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V
    .locals 2

    .prologue
    .line 1808138
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1808139
    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1808140
    invoke-virtual {v1, p1}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1808141
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1808142
    :cond_0
    return-void
.end method

.method public setStreetAddress(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808143
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808144
    return-void
.end method

.method public setStreetAddressFieldFocusable(Z)V
    .locals 1

    .prologue
    .line 1808145
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFocusable(Z)V

    .line 1808146
    return-void
.end method

.method public setStreetAddressHintText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808149
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808150
    return-void
.end method

.method public setStreetAddressOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1    # Landroid/view/View$OnClickListener;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808151
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808152
    return-void
.end method

.method public setTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1
    .param p1    # Landroid/text/TextWatcher;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808153
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808154
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->b:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808155
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808156
    return-void
.end method

.method public setZip(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808157
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808158
    return-void
.end method

.method public setZipHintText(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808159
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsLocationView;->c:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldHintText(Ljava/lang/CharSequence;)V

    .line 1808160
    return-void
.end method
