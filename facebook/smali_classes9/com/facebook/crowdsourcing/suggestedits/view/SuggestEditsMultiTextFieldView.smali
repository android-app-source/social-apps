.class public Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1808254
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1808255
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->b()V

    .line 1808256
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1808251
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1808252
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->b()V

    .line 1808253
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1808248
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1808249
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->b()V

    .line 1808250
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1808216
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->setOrientation(I)V

    .line 1808217
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0218ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->b:Landroid/graphics/drawable/Drawable;

    .line 1808218
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08291e    # 1.809885E38f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->c:Ljava/lang/String;

    .line 1808219
    return-void
.end method

.method private c()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;
    .locals 3

    .prologue
    .line 1808244
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1808245
    const v1, 0x7f031428

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808246
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonVisible(Z)V

    .line 1808247
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/view/View$OnClickListener;LX/BgK;Ljava/lang/String;)Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1808257
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->c()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-result-object v0

    .line 1808258
    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setFieldText(Ljava/lang/String;)V

    .line 1808259
    invoke-virtual {v0, p4}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setIcon(Ljava/lang/String;)V

    .line 1808260
    new-instance v1, LX/Bgi;

    invoke-direct {v1, p0, p3}, LX/Bgi;-><init>(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/BgK;)V

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1808261
    iget-object v1, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->b:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    .line 1808262
    invoke-virtual {v0, p2}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1808263
    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->addView(Landroid/view/View;)V

    .line 1808264
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1808239
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->clearFocus()V

    .line 1808240
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->removeAllViews()V

    .line 1808241
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->c()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808242
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->addView(Landroid/view/View;)V

    .line 1808243
    return-void
.end method

.method public getLastTextFieldView()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;
    .locals 2

    .prologue
    .line 1808233
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 1808234
    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    if-eqz v1, :cond_0

    .line 1808235
    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808236
    :goto_1
    return-object v0

    .line 1808237
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1808238
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getPrimaryTextField()Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;
    .locals 1

    .prologue
    .line 1808232
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->a:Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    return-object v0
.end method

.method public getValues()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1808225
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1808226
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1808227
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    if-eqz v0, :cond_0

    .line 1808228
    invoke-virtual {p0, v1}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;

    .line 1808229
    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsTextFieldView;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1808230
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1808231
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public setOnFocusChangeListener(LX/Bgk;)V
    .locals 3

    .prologue
    .line 1808220
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1808221
    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1808222
    new-instance v2, LX/Bgj;

    invoke-direct {v2, p0, p1, v0}, LX/Bgj;-><init>(Lcom/facebook/crowdsourcing/suggestedits/view/SuggestEditsMultiTextFieldView;LX/Bgk;I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1808223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1808224
    :cond_0
    return-void
.end method
