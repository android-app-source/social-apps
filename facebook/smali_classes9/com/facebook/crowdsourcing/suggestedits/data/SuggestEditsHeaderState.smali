.class public Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

.field public c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

.field public d:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1807710
    new-instance v0, LX/BgY;

    invoke-direct {v0}, LX/BgY;-><init>()V

    sput-object v0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1807704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1807705
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->a:Z

    .line 1807706
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    .line 1807707
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    .line 1807708
    return-void

    .line 1807709
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;Z)V
    .locals 0

    .prologue
    .line 1807699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1807700
    iput-object p1, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    .line 1807701
    iput-object p1, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    .line 1807702
    iput-boolean p2, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->a:Z

    .line 1807703
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1807693
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1807694
    iget-boolean v0, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1807695
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->b:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1807696
    iget-object v0, p0, Lcom/facebook/crowdsourcing/suggestedits/data/SuggestEditsHeaderState;->c:Lcom/facebook/crowdsourcing/protocol/graphql/SuggestEditsModels$SuggestEditsHeaderModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1807697
    return-void

    .line 1807698
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
