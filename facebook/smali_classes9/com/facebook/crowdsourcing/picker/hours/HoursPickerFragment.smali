.class public Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/BfX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Ljava/util/Locale;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/fbui/popover/PopoverSpinner;

.field private d:Landroid/widget/LinearLayout;

.field public e:[Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1806228
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1806229
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1806230
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

    invoke-static {v0}, LX/BfX;->a(LX/0QB;)LX/BfX;

    move-result-object p1

    check-cast p1, LX/BfX;

    invoke-static {v0}, LX/0eD;->b(LX/0QB;)Ljava/util/Locale;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    iput-object p1, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->b:Ljava/util/Locale;

    .line 1806231
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x2

    const/16 v0, 0x2a

    const v1, -0x45c53629

    invoke-static {v9, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1806232
    const v0, 0x7f0308a0

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 1806233
    const v0, 0x7f0d1649

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/popover/PopoverSpinner;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->c:Lcom/facebook/fbui/popover/PopoverSpinner;

    .line 1806234
    const v0, 0x7f0d164a

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->d:Landroid/widget/LinearLayout;

    .line 1806235
    const/16 v0, 0x8

    new-array v1, v0, [Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    new-instance v0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;-><init>(Landroid/content/Context;)V

    aput-object v0, v1, v3

    const/4 v2, 0x1

    const v0, 0x7f0d1651

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    aput-object v0, v1, v2

    const v0, 0x7f0d164b

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    aput-object v0, v1, v9

    const/4 v2, 0x3

    const v0, 0x7f0d164c

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    aput-object v0, v1, v2

    const/4 v2, 0x4

    const v0, 0x7f0d164d

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    aput-object v0, v1, v2

    const/4 v2, 0x5

    const v0, 0x7f0d164e

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    aput-object v0, v1, v2

    const/4 v2, 0x6

    const v0, 0x7f0d164f

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    aput-object v0, v1, v2

    const/4 v2, 0x7

    const v0, 0x7f0d1650    # 1.87537E38f

    invoke-static {v8, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    aput-object v0, v1, v2

    iput-object v1, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->e:[Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    .line 1806236
    new-instance v0, Ljava/text/DateFormatSymbols;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->b:Ljava/util/Locale;

    invoke-direct {v0, v1}, Ljava/text/DateFormatSymbols;-><init>(Ljava/util/Locale;)V

    invoke-virtual {v0}, Ljava/text/DateFormatSymbols;->getWeekdays()[Ljava/lang/String;

    move-result-object v2

    .line 1806237
    sget-object v3, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1806238
    iget-object v5, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->e:[Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    aget-object v5, v5, v0

    aget-object v0, v2, v0

    invoke-static {v0}, LX/0YN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setDayNameLabel(Ljava/lang/String;)V

    .line 1806239
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1806240
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1806241
    const-string v1, "extra_hours_selected_option"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1806242
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1806243
    const-string v1, "extra_hours_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/facebook/crowdsourcing/helper/HoursData;

    .line 1806244
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->a:LX/BfX;

    iget-object v1, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->c:Lcom/facebook/fbui/popover/PopoverSpinner;

    iget-object v2, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->d:Landroid/widget/LinearLayout;

    iget-object v3, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;->e:[Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;

    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    move-object v6, p0

    .line 1806245
    iput-object v2, v0, LX/BfX;->e:Landroid/view/ViewGroup;

    .line 1806246
    iput-object v6, v0, LX/BfX;->a:Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;

    .line 1806247
    iget-object v10, v0, LX/BfX;->d:Landroid/content/res/Resources;

    const p0, 0x7f100044

    invoke-virtual {v10, p0}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v10

    .line 1806248
    new-instance p0, Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Lcom/facebook/fbui/popover/PopoverSpinner;->getContext()Landroid/content/Context;

    move-result-object p1

    const p2, 0x7f0308a2

    invoke-direct {p0, p1, p2, v10}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 1806249
    iput v4, v0, LX/BfX;->f:I

    .line 1806250
    invoke-virtual {v1, p0}, Lcom/facebook/fbui/popover/PopoverSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1806251
    invoke-virtual {v1, v4}, Lcom/facebook/fbui/popover/PopoverSpinner;->setSelection(I)V

    .line 1806252
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/popover/PopoverSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 1806253
    invoke-static {v0, v4}, LX/BfX;->a(LX/BfX;I)V

    .line 1806254
    invoke-virtual {v5}, LX/0am;->isPresent()Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1806255
    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/crowdsourcing/helper/HoursData;

    iput-object v10, v0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    .line 1806256
    invoke-virtual {v5}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/facebook/crowdsourcing/helper/HoursData;

    const/4 v6, 0x1

    const/4 p2, 0x0

    .line 1806257
    sget-object p3, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v1

    move p1, p2

    :goto_1
    if-ge p1, v1, :cond_3

    invoke-virtual {p3, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/Integer;

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 1806258
    invoke-virtual {v10, v2}, Lcom/facebook/crowdsourcing/helper/HoursData;->a(I)LX/Bex;

    move-result-object v4

    .line 1806259
    iget-object p0, v4, LX/Bex;->a:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result p0

    if-eqz p0, :cond_2

    .line 1806260
    aget-object p0, v3, v2

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->a()V

    .line 1806261
    :cond_1
    :goto_2
    add-int/lit8 p0, p1, 0x1

    move p1, p0

    goto :goto_1

    .line 1806262
    :cond_2
    aget-object p0, v3, v2

    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->b()V

    .line 1806263
    aget-object v5, v3, v2

    iget-object p0, v4, LX/Bex;->a:LX/0Px;

    invoke-virtual {p0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    invoke-static {v0, v5, p0}, LX/BfX;->a$redex0(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V

    .line 1806264
    iget-object p0, v4, LX/Bex;->a:LX/0Px;

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p0

    if-le p0, v6, :cond_1

    .line 1806265
    aget-object v2, v3, v2

    iget-object p0, v4, LX/Bex;->a:LX/0Px;

    invoke-virtual {p0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;

    invoke-static {v0, v2, p0}, LX/BfX;->b(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;Lcom/facebook/crowdsourcing/helper/HoursData$HoursInterval;)V

    goto :goto_2

    .line 1806266
    :cond_3
    :goto_3
    sget-object p1, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    const/4 v10, 0x0

    move p0, v10

    :goto_4
    if-ge p0, p2, :cond_4

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 1806267
    aget-object p3, v3, v10

    .line 1806268
    aget-object v1, v3, v10

    new-instance v2, LX/BfR;

    invoke-direct {v2, v0, v10, p3}, LX/BfR;-><init>(LX/BfX;ILcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setHoursOnClickListener(LX/BfQ;)V

    .line 1806269
    add-int/lit8 v10, p0, 0x1

    move p0, v10

    goto :goto_4

    .line 1806270
    :cond_4
    sget-object p1, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    const/4 v10, 0x0

    move p0, v10

    :goto_5
    if-ge p0, p2, :cond_5

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 1806271
    aget-object p3, v3, v10

    .line 1806272
    new-instance v1, LX/BfU;

    invoke-direct {v1, v0, p3, v10}, LX/BfU;-><init>(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;I)V

    invoke-virtual {p3, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setCheckBoxOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1806273
    add-int/lit8 v10, p0, 0x1

    move p0, v10

    goto :goto_5

    .line 1806274
    :cond_5
    sget-object p1, Lcom/facebook/crowdsourcing/helper/HoursData;->a:LX/0Px;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    const/4 v10, 0x0

    move p0, v10

    :goto_6
    if-ge p0, p2, :cond_6

    invoke-virtual {p1, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 1806275
    aget-object p3, v3, v10

    .line 1806276
    new-instance v1, LX/BfV;

    invoke-direct {v1, v0, v10, p3}, LX/BfV;-><init>(LX/BfX;ILcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;)V

    invoke-virtual {p3, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setAddIntervalButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1806277
    new-instance v1, LX/BfW;

    invoke-direct {v1, v0, p3, v10}, LX/BfW;-><init>(LX/BfX;Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;I)V

    invoke-virtual {p3, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setRemoveIntervalButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1806278
    add-int/lit8 v10, p0, 0x1

    move p0, v10

    goto :goto_6

    .line 1806279
    :cond_6
    const/16 v0, 0x2b

    const v1, 0x2d371086

    invoke-static {v9, v0, v1, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v8

    .line 1806280
    :cond_7
    new-instance v10, Lcom/facebook/crowdsourcing/helper/HoursData;

    invoke-direct {v10}, Lcom/facebook/crowdsourcing/helper/HoursData;-><init>()V

    iput-object v10, v0, LX/BfX;->g:Lcom/facebook/crowdsourcing/helper/HoursData;

    goto :goto_3
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v0, 0x2a

    const v1, 0x5cf855b

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1806281
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1806282
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1806283
    const/4 v2, 0x0

    invoke-interface {v0, v2}, LX/1ZF;->setCustomTitle(Landroid/view/View;)V

    .line 1806284
    const v2, 0x7f08290e

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 1806285
    invoke-interface {v0, v4}, LX/1ZF;->k_(Z)V

    .line 1806286
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f082912

    invoke-virtual {p0, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1806287
    iput-object v3, v2, LX/108;->g:Ljava/lang/String;

    .line 1806288
    move-object v2, v2

    .line 1806289
    iput-boolean v4, v2, LX/108;->d:Z

    .line 1806290
    move-object v2, v2

    .line 1806291
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    invoke-interface {v0, v2}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 1806292
    new-instance v2, LX/BfY;

    invoke-direct {v2, p0}, LX/BfY;-><init>(Lcom/facebook/crowdsourcing/picker/hours/HoursPickerFragment;)V

    invoke-interface {v0, v2}, LX/1ZF;->a(LX/63W;)V

    .line 1806293
    const/16 v0, 0x2b

    const v2, 0x7e523a43

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
