.class public Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1806019
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1806020
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a(Landroid/util/AttributeSet;)V

    .line 1806021
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1806016
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1806017
    invoke-direct {p0, p2}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a(Landroid/util/AttributeSet;)V

    .line 1806018
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1806013
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1806014
    invoke-direct {p0, p2}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a(Landroid/util/AttributeSet;)V

    .line 1806015
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1805992
    const v0, 0x7f0308a1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1805993
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setOrientation(I)V

    .line 1805994
    const v0, 0x7f0d1652

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a:Landroid/widget/TextView;

    .line 1805995
    const v0, 0x7f0d1653

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->b:Landroid/widget/TextView;

    .line 1805996
    const v0, 0x7f0d1654

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->c:Landroid/widget/ImageView;

    .line 1805997
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->HoursIntervalView:[I

    invoke-virtual {v0, p1, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1805998
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1805999
    const/16 v0, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    .line 1806000
    const/4 v0, 0x0

    .line 1806001
    if-eqz v3, :cond_0

    .line 1806002
    invoke-virtual {p0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1806003
    :cond_0
    if-eqz v2, :cond_1

    .line 1806004
    iget-object v3, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->c:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1806005
    iget-object v2, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1806006
    :cond_1
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1806007
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1806010
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806011
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1806012
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1806022
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEndHoursOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1806008
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1806009
    return-void
.end method

.method public setIconOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1805990
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805991
    return-void
.end method

.method public setIconVisibility(I)V
    .locals 1

    .prologue
    .line 1805988
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1805989
    return-void
.end method

.method public setStartHoursOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1805986
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805987
    return-void
.end method
