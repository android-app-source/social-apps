.class public Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field public c:Landroid/widget/CheckBox;

.field private d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

.field private e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1805930
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1805931
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->f()V

    .line 1805932
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1805983
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1805984
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->f()V

    .line 1805985
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1805980
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1805981
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->f()V

    .line 1805982
    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 1805971
    const v0, 0x7f0308a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1805972
    const v0, 0x7f0d1659

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->a:Landroid/view/View;

    .line 1805973
    const v0, 0x7f0d1656

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->b:Landroid/widget/TextView;

    .line 1805974
    const v0, 0x7f0d1655

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->c:Landroid/widget/CheckBox;

    .line 1805975
    const v0, 0x7f0d1657

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    .line 1805976
    const v0, 0x7f0d1658

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    iput-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    .line 1805977
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setChildrenVisibility(Z)V

    .line 1805978
    invoke-direct {p0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->g()V

    .line 1805979
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1805967
    new-instance v0, LX/BfP;

    invoke-direct {v0, p0}, LX/BfP;-><init>(Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;)V

    .line 1805968
    iget-object v1, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805969
    iget-object v1, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805970
    return-void
.end method

.method private setPlaceOpenOrClosed(Z)V
    .locals 1

    .prologue
    .line 1805965
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1805966
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1805963
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setPlaceOpenOrClosed(Z)V

    .line 1805964
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1805960
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805961
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setChildrenVisibility(Z)V

    .line 1805962
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1805958
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setPlaceOpenOrClosed(Z)V

    .line 1805959
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1805955
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805956
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->setChildrenVisibility(Z)V

    .line 1805957
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1805954
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a()Z

    move-result v0

    return v0
.end method

.method public setAddIntervalButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1805952
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setIconOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805953
    return-void
.end method

.method public setCheckBoxOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1805950
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1805951
    return-void
.end method

.method public setChildrenVisibility(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1805942
    iget-object v3, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setVisibility(I)V

    .line 1805943
    iget-object v3, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    invoke-virtual {v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setVisibility(I)V

    .line 1805944
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->a:Landroid/view/View;

    if-eqz p1, :cond_3

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1805945
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    iget-object v2, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    invoke-virtual {v2}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    const/4 v1, 0x4

    :cond_0
    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setIconVisibility(I)V

    .line 1805946
    return-void

    :cond_1
    move v0, v2

    .line 1805947
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1805948
    goto :goto_1

    :cond_3
    move v2, v1

    .line 1805949
    goto :goto_2
.end method

.method public setDayNameLabel(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1805940
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1805941
    return-void
.end method

.method public setHoursOnClickListener(LX/BfQ;)V
    .locals 2

    .prologue
    .line 1805935
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    new-instance v1, LX/BfL;

    invoke-direct {v1, p0, p1}, LX/BfL;-><init>(Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;LX/BfQ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setStartHoursOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805936
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->d:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    new-instance v1, LX/BfM;

    invoke-direct {v1, p0, p1}, LX/BfM;-><init>(Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;LX/BfQ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setEndHoursOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805937
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    new-instance v1, LX/BfN;

    invoke-direct {v1, p0, p1}, LX/BfN;-><init>(Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;LX/BfQ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setStartHoursOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805938
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    new-instance v1, LX/BfO;

    invoke-direct {v1, p0, p1}, LX/BfO;-><init>(Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;LX/BfQ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setEndHoursOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805939
    return-void
.end method

.method public setRemoveIntervalButtonOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1805933
    iget-object v0, p0, Lcom/facebook/crowdsourcing/picker/hours/HoursForSingleDayView;->e:Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;

    invoke-virtual {v0, p1}, Lcom/facebook/crowdsourcing/picker/hours/HoursIntervalView;->setIconOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1805934
    return-void
.end method
