.class public Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;
.super Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final e:Ljava/lang/String;

.field public f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1794055
    new-instance v0, LX/BY1;

    invoke-direct {v0}, LX/BY1;-><init>()V

    sput-object v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1794050
    const-class v0, Lcom/facebook/user/model/Name;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/Name;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    const-class v1, Lcom/facebook/user/model/UserKey;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserKey;

    invoke-direct {p0, v0, v3, v1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/Name;Ljava/lang/String;Lcom/facebook/user/model/UserKey;)V

    .line 1794051
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->e:Ljava/lang/String;

    .line 1794052
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->f:Z

    .line 1794053
    return-void

    .line 1794054
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/facebook/user/model/User;)V
    .locals 1

    .prologue
    .line 1794048
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;-><init>(Lcom/facebook/user/model/User;Z)V

    .line 1794049
    return-void
.end method

.method private constructor <init>(Lcom/facebook/user/model/User;Z)V
    .locals 1

    .prologue
    .line 1794043
    invoke-direct {p0, p1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 1794044
    iget-object v0, p1, Lcom/facebook/user/model/User;->q:Ljava/lang/String;

    move-object v0, v0

    .line 1794045
    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->e:Ljava/lang/String;

    .line 1794046
    iput-boolean p2, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->f:Z

    .line 1794047
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1794042
    const/4 v0, 0x0

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1794032
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1794033
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->e:Lcom/facebook/user/model/Name;

    move-object v0, v0

    .line 1794034
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1794035
    invoke-virtual {p0}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1794036
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1794037
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1794038
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1794039
    iget-boolean v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1794040
    return-void

    .line 1794041
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
