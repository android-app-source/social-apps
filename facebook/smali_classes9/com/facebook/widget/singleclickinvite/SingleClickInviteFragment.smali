.class public abstract Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public b:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/BXz;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/3LP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/2RQ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Lcom/facebook/resources/ui/FbTextView;

.field public h:Landroid/view/View;

.field public i:Landroid/view/View;

.field public j:Lcom/facebook/widget/listview/BetterListView;

.field public k:Landroid/view/inputmethod/InputMethodManager;

.field public l:Landroid/view/View$OnClickListener;

.field public m:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public o:LX/8RL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:LX/8uo;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0zG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/BY0;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public s:LX/3Oq;
    .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final t:LX/3NY;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1697074
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1697075
    new-instance v0, LX/BXp;

    invoke-direct {v0, p0}, LX/BXp;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->t:LX/3NY;

    return-void
.end method

.method public static a(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;Ljava/lang/String;Ljava/util/Map;)LX/44w;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;)",
            "LX/44w",
            "<",
            "LX/0Rf",
            "<",
            "Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;",
            ">;",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1697053
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1697054
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1697055
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v4

    .line 1697056
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1697057
    :cond_0
    const/4 v0, 0x0

    .line 1697058
    :goto_0
    return-object v0

    .line 1697059
    :cond_1
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 1697060
    new-instance v6, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;

    invoke-direct {v6, v1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 1697061
    iget-object p2, v1, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v1, p2

    .line 1697062
    invoke-virtual {p0, v1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a(Ljava/lang/String;)Z

    move-result v1

    .line 1697063
    iput-boolean v1, v6, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->f:Z

    .line 1697064
    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1697065
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1697066
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1697067
    new-instance v1, LX/623;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->b(Ljava/lang/String;)I

    move-result v2

    .line 1697068
    if-lez v2, :cond_3

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1697069
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1697070
    :goto_2
    move-object v2, v3

    .line 1697071
    const/4 v3, 0x0

    move v3, v3

    .line 1697072
    invoke-direct {v1, v2, v0, v3}, LX/623;-><init>(Ljava/lang/String;LX/0Px;Z)V

    .line 1697073
    new-instance v0, LX/44w;

    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    invoke-direct {v0, v2, v1}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public static b(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1697045
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v0, :cond_0

    .line 1697046
    if-eqz p1, :cond_1

    .line 1697047
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1697048
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    .line 1697049
    :cond_0
    :goto_0
    return-void

    .line 1697050
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 1697051
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1697052
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    .line 1697042
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1697043
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/8RL;->b(LX/0QB;)LX/8RL;

    move-result-object v5

    check-cast v5, LX/8RL;

    invoke-static {v0}, LX/8uo;->a(LX/0QB;)LX/8uo;

    move-result-object v6

    check-cast v6, LX/8uo;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v7

    check-cast v7, LX/0zG;

    new-instance v10, LX/BY0;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    const-class v9, LX/8vM;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/8vM;

    invoke-direct {v10, v8, v9}, LX/BY0;-><init>(Landroid/view/inputmethod/InputMethodManager;LX/8vM;)V

    move-object v8, v10

    check-cast v8, LX/BY0;

    invoke-static {v0}, LX/6NS;->b(LX/0QB;)LX/3Oq;

    move-result-object v9

    check-cast v9, LX/3Oq;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v10

    check-cast v10, LX/0TD;

    new-instance v11, LX/BXz;

    invoke-direct {v11}, LX/BXz;-><init>()V

    move-object v11, v11

    move-object v11, v11

    check-cast v11, LX/BXz;

    invoke-static {v0}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object p1

    check-cast p1, LX/3LP;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v0

    check-cast v0, LX/2RQ;

    iput-object v3, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->m:Landroid/content/Context;

    iput-object v4, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->n:LX/1Ck;

    iput-object v5, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->o:LX/8RL;

    iput-object v6, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->p:LX/8uo;

    iput-object v7, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->q:LX/0zG;

    iput-object v8, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->r:LX/BY0;

    iput-object v9, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->s:LX/3Oq;

    iput-object v10, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->b:LX/0TD;

    iput-object v11, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->c:LX/BXz;

    iput-object p1, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->d:LX/3LP;

    iput-object v0, v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->e:LX/2RQ;

    .line 1697044
    return-void
.end method

.method public abstract a(Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;)V
.end method

.method public a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 1697039
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080039

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1697040
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1697041
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1697038
    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1697035
    const-string v0, "suggested_section"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1697036
    const v0, 0x7f082719

    .line 1697037
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1697028
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1697029
    if-eqz v0, :cond_0

    .line 1697030
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 1697031
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->q:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1697032
    new-instance v1, LX/BXu;

    invoke-direct {v1, p0}, LX/BXu;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 1697033
    const v1, 0x7f082716

    invoke-interface {v0, v1}, LX/0h5;->setTitle(I)V

    .line 1697034
    return-void
.end method

.method public c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1697076
    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1697027
    const-string v0, "suggested_section"

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 1697026
    const/4 v0, 0x1

    return v0
.end method

.method public k()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1697025
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->b:LX/0TD;

    new-instance v1, LX/BXx;

    invoke-direct {v1, p0}, LX/BXx;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1697006
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->e:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->s:LX/3Oq;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1697007
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 1697008
    move-object v0, v0

    .line 1697009
    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->n()Z

    move-result v1

    .line 1697010
    iput-boolean v1, v0, LX/2RR;->o:Z

    .line 1697011
    move-object v0, v0

    .line 1697012
    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->m()LX/2RS;

    move-result-object v1

    .line 1697013
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 1697014
    move-object v0, v0

    .line 1697015
    iget-object v1, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->d:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 1697016
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1697017
    if-nez v1, :cond_0

    .line 1697018
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1697019
    :goto_0
    return-object v0

    .line 1697020
    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1697021
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1697022
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    .line 1697023
    :cond_1
    :try_start_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1697024
    invoke-interface {v1}, LX/3On;->close()V

    goto :goto_0
.end method

.method public m()LX/2RS;
    .locals 1

    .prologue
    .line 1697005
    sget-object v0, LX/2RS;->NAME:LX/2RS;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 1697004
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3f4ade09

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1696982
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1696983
    const v0, 0x7f0d2c8e

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1696984
    const v0, 0x7f0d2c91

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    .line 1696985
    const v0, 0x7f0d2c8b

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->h:Landroid/view/View;

    .line 1696986
    const v0, 0x7f0d2c8d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1696987
    const v0, 0x7f0d2c90

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->i:Landroid/view/View;

    .line 1696988
    const v0, 0x7f0d2c8f

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    .line 1696989
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->m:Landroid/content/Context;

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->k:Landroid/view/inputmethod/InputMethodManager;

    .line 1696990
    new-instance v0, LX/BXw;

    invoke-direct {v0, p0}, LX/BXw;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->l:Landroid/view/View$OnClickListener;

    .line 1696991
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->c:LX/BXz;

    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->e()Z

    move-result v2

    .line 1696992
    iput-boolean v2, v0, LX/BXz;->a:Z

    .line 1696993
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->r:LX/BY0;

    iget-object v2, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->o:LX/8RL;

    iget-object v4, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->c:LX/BXz;

    iget-object p1, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2, v4, p1}, LX/BY0;->a(LX/8RK;LX/BXz;Landroid/view/View$OnClickListener;)V

    .line 1696994
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v2, LX/BXs;

    invoke-direct {v2, p0}, LX/BXs;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1696995
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v2, LX/BXt;

    invoke-direct {v2, p0}, LX/BXt;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1696996
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v2, LX/8us;->WHILE_EDITING:LX/8us;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setClearButtonMode(LX/8us;)V

    .line 1696997
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->i:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1696998
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->a:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->b(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;Z)V

    .line 1696999
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->h:Landroid/view/View;

    new-instance v2, LX/BXv;

    invoke-direct {v2, p0}, LX/BXv;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1697000
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->r:LX/BY0;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1697001
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1697002
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->n:LX/1Ck;

    sget-object v2, LX/BXy;->FETCH_INIT_ID:LX/BXy;

    new-instance v4, LX/BXq;

    invoke-direct {v4, p0}, LX/BXq;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    new-instance p1, LX/BXr;

    invoke-direct {p1, p0}, LX/BXr;-><init>(Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;)V

    invoke-virtual {v0, v2, v4, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1697003
    const/16 v0, 0x2b

    const v2, 0x633c2911

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x135200c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696981
    const v1, 0x7f03133a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x4de2d11f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x355d42a9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696978
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1696979
    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteFragment;->b()V

    .line 1696980
    const/16 v1, 0x2b

    const v2, 0x76765406

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
