.class public Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final e:Lcom/facebook/fig/button/FigButton;

.field public f:Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1793961
    const-class v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1793990
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1793991
    const v0, 0x7f03133b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1793992
    const v0, 0x7f0d2c95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->b:Landroid/widget/TextView;

    .line 1793993
    const v0, 0x7f0d2c96

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->c:Landroid/widget/TextView;

    .line 1793994
    const v0, 0x7f0d2c92

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1793995
    const v0, 0x7f0d2c93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    .line 1793996
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1793982
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f08271b

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1793983
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x44

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1793984
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 1793985
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f0207d6

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 1793986
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0100

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->LIGHTEN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1793987
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0760

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1793988
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0761

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1793989
    return-void
.end method

.method public final a(LX/8QL;)V
    .locals 3

    .prologue
    .line 1793963
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1793964
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->c:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/8QL;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1793965
    invoke-virtual {p1}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    .line 1793966
    iget-object v1, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v2, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1793967
    check-cast p1, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;

    iput-object p1, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->f:Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;

    .line 1793968
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f0d01cc

    invoke-virtual {v0, v1, p0}, Lcom/facebook/fig/button/FigButton;->setTag(ILjava/lang/Object;)V

    .line 1793969
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->f:Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;

    .line 1793970
    iget-boolean v1, v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->f:Z

    move v0, v1

    .line 1793971
    if-eqz v0, :cond_0

    .line 1793972
    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->a()V

    .line 1793973
    :goto_0
    return-void

    .line 1793974
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f08271a

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1793975
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1793976
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 1793977
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->e:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1793978
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->clearColorFilter()V

    .line 1793979
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a075e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1793980
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a075f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1793981
    goto :goto_0
.end method

.method public getUserId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1793962
    iget-object v0, p0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteItemRow;->f:Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
