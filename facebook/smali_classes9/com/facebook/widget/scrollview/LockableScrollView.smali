.class public Lcom/facebook/widget/scrollview/LockableScrollView;
.super Landroid/widget/ScrollView;
.source ""


# instance fields
.field public a:Z

.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1793828
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 1793829
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->a:Z

    .line 1793830
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->b:Z

    .line 1793831
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    .line 1793832
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1793792
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1793793
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->a:Z

    .line 1793794
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->b:Z

    .line 1793795
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    .line 1793796
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1793833
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1793834
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->a:Z

    .line 1793835
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->b:Z

    .line 1793836
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    .line 1793837
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1793819
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 1793820
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/MotionEvent;->setAction(I)V

    .line 1793821
    invoke-super {p0, v0}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1793822
    iput-boolean v2, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->b:Z

    .line 1793823
    invoke-super {p0, v0}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 1793824
    :goto_0
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1793825
    return-void

    .line 1793826
    :cond_0
    iput-boolean v2, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    goto :goto_0
.end method

.method public getLocked()Z
    .locals 1

    .prologue
    .line 1793827
    iget-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->a:Z

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1793811
    iget-boolean v2, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->b:Z

    if-eqz v2, :cond_0

    .line 1793812
    iput-boolean v1, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->b:Z

    .line 1793813
    :goto_0
    return v0

    .line 1793814
    :cond_0
    iget-boolean v2, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    if-eqz v2, :cond_1

    .line 1793815
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    .line 1793816
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->a:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 1793817
    goto :goto_0

    .line 1793818
    :cond_2
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x2

    const/4 v1, 0x0

    const v2, -0x3a8add60

    invoke-static {v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1793802
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->a:Z

    if-eqz v3, :cond_0

    .line 1793803
    const v0, -0x1966aa10

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1793804
    :goto_0
    return v1

    .line 1793805
    :cond_0
    iget-boolean v3, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    if-eqz v3, :cond_1

    .line 1793806
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    .line 1793807
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    if-nez v0, :cond_3

    .line 1793808
    invoke-super {p0, p1}, Landroid/widget/ScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    const v0, 0x81c9efe

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1793809
    goto :goto_1

    .line 1793810
    :cond_3
    const v0, -0x302d8331

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_0
.end method

.method public setScrollLock(Z)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1793797
    if-nez p1, :cond_0

    .line 1793798
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->c:Z

    .line 1793799
    iput-boolean v0, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->b:Z

    .line 1793800
    :cond_0
    iput-boolean p1, p0, Lcom/facebook/widget/scrollview/LockableScrollView;->a:Z

    .line 1793801
    return-void
.end method
