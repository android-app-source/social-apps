.class public Lcom/facebook/widget/verticalviewpager/VerticalViewPager;
.super Landroid/view/ViewGroup;
.source ""


# static fields
.field public static final a:[I

.field private static final ai:LX/BYD;

.field private static final e:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "LX/BY5;",
            ">;"
        }
    .end annotation
.end field

.field private static final f:Landroid/view/animation/Interpolator;


# instance fields
.field private A:Z

.field private B:Z

.field private C:Z

.field private D:I

.field private E:I

.field private F:I

.field private G:F

.field private H:F

.field private I:F

.field private J:F

.field private K:I

.field private L:Landroid/view/VelocityTracker;

.field private M:I

.field private N:I

.field private O:I

.field private P:I

.field private Q:F

.field private R:Z

.field private S:LX/0vj;

.field private T:LX/0vj;

.field private U:Z

.field private V:Z

.field private W:I

.field private aa:Z

.field public ab:LX/Avr;

.field private ac:LX/Avr;

.field private ad:LX/BY8;

.field private ae:LX/BY9;

.field private af:Ljava/lang/reflect/Method;

.field private ag:I

.field private ah:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final aj:Ljava/lang/Runnable;

.field private ak:I

.field private b:I

.field private c:I

.field private d:F

.field private final g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/BY5;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/BY5;

.field private final i:Landroid/graphics/Rect;

.field public j:LX/0gG;

.field public k:I

.field private l:I

.field private m:Landroid/os/Parcelable;

.field private n:Ljava/lang/ClassLoader;

.field private o:LX/BYC;

.field private p:LX/BYA;

.field private q:I

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:I

.field private t:I

.field private u:F

.field private v:F

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1795051
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100b3

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a:[I

    .line 1795052
    new-instance v0, LX/BY2;

    invoke-direct {v0}, LX/BY2;-><init>()V

    sput-object v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->e:Ljava/util/Comparator;

    .line 1795053
    new-instance v0, LX/BY3;

    invoke-direct {v0}, LX/BY3;-><init>()V

    sput-object v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->f:Landroid/view/animation/Interpolator;

    .line 1795054
    new-instance v0, LX/BYD;

    invoke-direct {v0}, LX/BYD;-><init>()V

    sput-object v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ai:LX/BYD;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 1795034
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 1795035
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    .line 1795036
    new-instance v0, LX/BY5;

    invoke-direct {v0}, LX/BY5;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->h:LX/BY5;

    .line 1795037
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->i:Landroid/graphics/Rect;

    .line 1795038
    iput v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->l:I

    .line 1795039
    iput-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->m:Landroid/os/Parcelable;

    .line 1795040
    iput-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->n:Ljava/lang/ClassLoader;

    .line 1795041
    const v0, -0x800001

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->u:F

    .line 1795042
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->v:F

    .line 1795043
    iput v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->z:I

    .line 1795044
    iput v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    .line 1795045
    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    .line 1795046
    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aa:Z

    .line 1795047
    new-instance v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$3;

    invoke-direct {v0, p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$3;-><init>(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;)V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aj:Ljava/lang/Runnable;

    .line 1795048
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ak:I

    .line 1795049
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->d()V

    .line 1795050
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x1

    .line 1795017
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1795018
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    .line 1795019
    new-instance v0, LX/BY5;

    invoke-direct {v0}, LX/BY5;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->h:LX/BY5;

    .line 1795020
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->i:Landroid/graphics/Rect;

    .line 1795021
    iput v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->l:I

    .line 1795022
    iput-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->m:Landroid/os/Parcelable;

    .line 1795023
    iput-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->n:Ljava/lang/ClassLoader;

    .line 1795024
    const v0, -0x800001

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->u:F

    .line 1795025
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->v:F

    .line 1795026
    iput v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->z:I

    .line 1795027
    iput v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    .line 1795028
    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    .line 1795029
    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aa:Z

    .line 1795030
    new-instance v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$3;

    invoke-direct {v0, p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$3;-><init>(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;)V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aj:Ljava/lang/Runnable;

    .line 1795031
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ak:I

    .line 1795032
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->d()V

    .line 1795033
    return-void
.end method

.method private static a(F)F
    .locals 4

    .prologue
    .line 1795014
    const/high16 v0, 0x3f000000    # 0.5f

    sub-float v0, p0, v0

    .line 1795015
    float-to-double v0, v0

    const-wide v2, 0x3fde28c7460698c7L    # 0.4712389167638204

    mul-double/2addr v0, v2

    double-to-float v0, v0

    .line 1795016
    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(IFII)I
    .locals 3

    .prologue
    .line 1795003
    invoke-static {p4}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->O:I

    if-le v0, v1, :cond_2

    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->M:I

    if-le v0, v1, :cond_2

    .line 1795004
    if-lez p3, :cond_1

    .line 1795005
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1795006
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1795007
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BY5;

    .line 1795008
    iget v0, v0, LX/BY5;->b:I

    iget v1, v1, LX/BY5;->b:I

    invoke-static {p1, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 1795009
    :cond_0
    return p1

    .line 1795010
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 1795011
    :cond_2
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-lt p1, v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->Q:F

    sub-float/2addr v0, v1

    .line 1795012
    :goto_1
    int-to-float v1, p1

    add-float/2addr v1, p2

    add-float/2addr v0, v1

    float-to-int p1, v0

    goto :goto_0

    .line 1795013
    :cond_3
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->Q:F

    goto :goto_1
.end method

.method private a(II)LX/BY5;
    .locals 2

    .prologue
    .line 1794995
    new-instance v0, LX/BY5;

    invoke-direct {v0}, LX/BY5;-><init>()V

    .line 1794996
    iput p1, v0, LX/BY5;->b:I

    .line 1794997
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v1, p0, p1}, LX/0gG;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, LX/BY5;->a:Ljava/lang/Object;

    .line 1794998
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v1, p1}, LX/0gG;->d(I)F

    move-result v1

    iput v1, v0, LX/BY5;->d:F

    .line 1794999
    if-ltz p2, :cond_0

    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p2, v1, :cond_1

    .line 1795000
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1795001
    :goto_0
    return-object v0

    .line 1795002
    :cond_1
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;)LX/BY5;
    .locals 4

    .prologue
    .line 1794989
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1794990
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1794991
    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget-object v3, v0, LX/BY5;->a:Ljava/lang/Object;

    invoke-virtual {v2, p1, v3}, LX/0gG;->a(Landroid/view/View;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1794992
    :goto_1
    return-object v0

    .line 1794993
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1794994
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1794971
    if-nez p1, :cond_2

    .line 1794972
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1794973
    :goto_0
    if-nez p2, :cond_0

    .line 1794974
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/graphics/Rect;->set(IIII)V

    move-object v0, v1

    .line 1794975
    :goto_1
    return-object v0

    .line 1794976
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1794977
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 1794978
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1794979
    invoke-virtual {p2}, Landroid/view/View;->getBottom()I

    move-result v0

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 1794980
    invoke-virtual {p2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1794981
    :goto_2
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    if-eq v0, p0, :cond_1

    .line 1794982
    check-cast v0, Landroid/view/ViewGroup;

    .line 1794983
    iget v2, v1, Landroid/graphics/Rect;->left:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLeft()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 1794984
    iget v2, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getRight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 1794985
    iget v2, v1, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getTop()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 1794986
    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBottom()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 1794987
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_2

    :cond_1
    move-object v0, v1

    .line 1794988
    goto :goto_1

    :cond_2
    move-object v1, p1

    goto :goto_0
.end method

.method private a(I)V
    .locals 20

    .prologue
    .line 1794857
    const/4 v3, 0x0

    .line 1794858
    const/4 v2, 0x2

    .line 1794859
    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move/from16 v0, p1

    if-eq v4, v0, :cond_23

    .line 1794860
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move/from16 v0, p1

    if-ge v2, v0, :cond_1

    const/16 v2, 0x42

    .line 1794861
    :goto_0
    move-object/from16 v0, p0

    iget v3, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b(I)LX/BY5;

    move-result-object v3

    .line 1794862
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move-object v4, v3

    move v3, v2

    .line 1794863
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-nez v2, :cond_2

    .line 1794864
    invoke-direct/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->f()V

    .line 1794865
    :cond_0
    :goto_2
    return-void

    .line 1794866
    :cond_1
    const/16 v2, 0x11

    goto :goto_0

    .line 1794867
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->y:Z

    if-eqz v2, :cond_3

    .line 1794868
    invoke-direct/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->f()V

    goto :goto_2

    .line 1794869
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1794870
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, LX/0gG;->a(Landroid/view/ViewGroup;)V

    .line 1794871
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->z:I

    .line 1794872
    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    sub-int/2addr v6, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v8

    .line 1794873
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v5}, LX/0gG;->b()I

    move-result v15

    .line 1794874
    add-int/lit8 v5, v15, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/2addr v2, v6

    invoke-static {v5, v2}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 1794875
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b:I

    if-eq v15, v2, :cond_4

    .line 1794876
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1794877
    :goto_3
    new-instance v3, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "The application\'s PagerAdapter changed the adapter\'s contents without calling PagerAdapter#notifyDataSetChanged! Expected adapter item count: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", found: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " Pager id: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Pager class: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " Problematic adapter: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1794878
    :catch_0
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 1794879
    :cond_4
    const/4 v6, 0x0

    .line 1794880
    const/4 v2, 0x0

    move v5, v2

    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v5, v2, :cond_22

    .line 1794881
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    .line 1794882
    iget v7, v2, LX/BY5;->b:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-lt v7, v9, :cond_6

    .line 1794883
    iget v7, v2, LX/BY5;->b:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ne v7, v9, :cond_22

    .line 1794884
    :goto_5
    if-nez v2, :cond_21

    if-lez v15, :cond_21

    .line 1794885
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(II)LX/BY5;

    move-result-object v2

    move-object v14, v2

    .line 1794886
    :goto_6
    if-eqz v14, :cond_19

    .line 1794887
    const/4 v13, 0x0

    .line 1794888
    add-int/lit8 v12, v5, -0x1

    .line 1794889
    if-ltz v12, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    .line 1794890
    :goto_7
    invoke-direct/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v16

    .line 1794891
    if-gtz v16, :cond_8

    const/4 v6, 0x0

    .line 1794892
    :goto_8
    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->A:Z

    if-eqz v7, :cond_9

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v9, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v9, v9, -0x1

    invoke-static {v7, v9}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 1794893
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->A:Z

    if-eqz v9, :cond_a

    add-int/lit8 v9, v15, -0x1

    move-object/from16 v0, p0

    iget v11, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v11, v11, 0x1

    invoke-static {v9, v11}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 1794894
    :goto_a
    move-object/from16 v0, p0

    iget v11, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v11, v11, -0x1

    move/from16 v18, v11

    move v11, v13

    move/from16 v13, v18

    move/from16 v19, v12

    move v12, v5

    move/from16 v5, v19

    :goto_b
    if-ltz v13, :cond_10

    .line 1794895
    cmpl-float v17, v11, v6

    if-ltz v17, :cond_c

    if-ge v13, v8, :cond_c

    .line 1794896
    if-eqz v2, :cond_10

    .line 1794897
    iget v0, v2, LX/BY5;->b:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v13, v0, :cond_5

    iget-boolean v0, v2, LX/BY5;->c:Z

    move/from16 v17, v0

    if-nez v17, :cond_5

    .line 1794898
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1794899
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    move-object/from16 v17, v0

    iget-object v2, v2, LX/BY5;->a:Ljava/lang/Object;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v13, v2}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1794900
    add-int/lit8 v5, v5, -0x1

    .line 1794901
    add-int/lit8 v12, v12, -0x1

    .line 1794902
    if-ltz v5, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    .line 1794903
    :cond_5
    :goto_c
    add-int/lit8 v13, v13, -0x1

    goto :goto_b

    .line 1794904
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_4

    .line 1794905
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_7

    .line 1794906
    :cond_8
    const/high16 v6, 0x40000000    # 2.0f

    iget v7, v14, LX/BY5;->d:F

    sub-float/2addr v6, v7

    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v7

    int-to-float v7, v7

    move/from16 v0, v16

    int-to-float v9, v0

    div-float/2addr v7, v9

    add-float/2addr v6, v7

    goto/16 :goto_8

    :cond_9
    move v7, v8

    .line 1794907
    goto/16 :goto_9

    :cond_a
    move v9, v10

    .line 1794908
    goto :goto_a

    .line 1794909
    :cond_b
    const/4 v2, 0x0

    goto :goto_c

    .line 1794910
    :cond_c
    if-eqz v2, :cond_e

    iget v0, v2, LX/BY5;->b:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v13, v0, :cond_e

    .line 1794911
    iget v2, v2, LX/BY5;->d:F

    add-float/2addr v11, v2

    .line 1794912
    add-int/lit8 v5, v5, -0x1

    .line 1794913
    if-ltz v5, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    goto :goto_c

    :cond_d
    const/4 v2, 0x0

    goto :goto_c

    .line 1794914
    :cond_e
    if-lt v13, v7, :cond_5

    .line 1794915
    add-int/lit8 v2, v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(II)LX/BY5;

    move-result-object v2

    .line 1794916
    iget v2, v2, LX/BY5;->d:F

    add-float/2addr v11, v2

    .line 1794917
    add-int/lit8 v12, v12, 0x1

    .line 1794918
    if-ltz v5, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    goto :goto_c

    :cond_f
    const/4 v2, 0x0

    goto :goto_c

    .line 1794919
    :cond_10
    iget v6, v14, LX/BY5;->d:F

    .line 1794920
    add-int/lit8 v8, v12, 0x1

    .line 1794921
    const/high16 v2, 0x40000000    # 2.0f

    cmpg-float v2, v6, v2

    if-gez v2, :cond_18

    .line 1794922
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v8, v2, :cond_11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    move-object v7, v2

    .line 1794923
    :goto_d
    if-gtz v16, :cond_12

    const/4 v2, 0x0

    move v5, v2

    .line 1794924
    :goto_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v2, v2, 0x1

    move/from16 v18, v2

    move-object v2, v7

    move v7, v8

    move/from16 v8, v18

    :goto_f
    if-ge v8, v15, :cond_18

    .line 1794925
    cmpl-float v11, v6, v5

    if-ltz v11, :cond_14

    if-le v8, v10, :cond_14

    .line 1794926
    if-eqz v2, :cond_18

    .line 1794927
    iget v11, v2, LX/BY5;->b:I

    if-ne v8, v11, :cond_20

    iget-boolean v11, v2, LX/BY5;->c:Z

    if-nez v11, :cond_20

    .line 1794928
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v11, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1794929
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget-object v2, v2, LX/BY5;->a:Ljava/lang/Object;

    move-object/from16 v0, p0

    invoke-virtual {v11, v0, v8, v2}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1794930
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_13

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    :goto_10
    move/from16 v18, v6

    move-object v6, v2

    move/from16 v2, v18

    .line 1794931
    :goto_11
    add-int/lit8 v8, v8, 0x1

    move/from16 v18, v2

    move-object v2, v6

    move/from16 v6, v18

    goto :goto_f

    .line 1794932
    :cond_11
    const/4 v7, 0x0

    goto :goto_d

    .line 1794933
    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v2

    int-to-float v2, v2

    move/from16 v0, v16

    int-to-float v5, v0

    div-float/2addr v2, v5

    const/high16 v5, 0x40000000    # 2.0f

    add-float/2addr v2, v5

    move v5, v2

    goto :goto_e

    .line 1794934
    :cond_13
    const/4 v2, 0x0

    goto :goto_10

    .line 1794935
    :cond_14
    if-eqz v2, :cond_16

    iget v11, v2, LX/BY5;->b:I

    if-ne v8, v11, :cond_16

    .line 1794936
    iget v2, v2, LX/BY5;->d:F

    add-float/2addr v6, v2

    .line 1794937
    add-int/lit8 v7, v7, 0x1

    .line 1794938
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    :goto_12
    move/from16 v18, v6

    move-object v6, v2

    move/from16 v2, v18

    goto :goto_11

    :cond_15
    const/4 v2, 0x0

    goto :goto_12

    .line 1794939
    :cond_16
    if-gt v8, v9, :cond_20

    .line 1794940
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v7}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(II)LX/BY5;

    move-result-object v2

    .line 1794941
    add-int/lit8 v7, v7, 0x1

    .line 1794942
    iget v2, v2, LX/BY5;->d:F

    add-float/2addr v6, v2

    .line 1794943
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v7, v2, :cond_17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    :goto_13
    move/from16 v18, v6

    move-object v6, v2

    move/from16 v2, v18

    goto :goto_11

    :cond_17
    const/4 v2, 0x0

    goto :goto_13

    .line 1794944
    :cond_18
    move-object/from16 v0, p0

    invoke-direct {v0, v14, v12, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(LX/BY5;ILX/BY5;)V

    .line 1794945
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-eqz v14, :cond_1b

    iget-object v2, v14, LX/BY5;->a:Ljava/lang/Object;

    :goto_14
    move-object/from16 v0, p0

    invoke-virtual {v4, v0, v5, v2}, LX/0gG;->b(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1794946
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 1794947
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v5

    .line 1794948
    const/4 v2, 0x0

    move v4, v2

    :goto_15
    if-ge v4, v5, :cond_1c

    .line 1794949
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1794950
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, LX/BY6;

    .line 1794951
    iput v4, v2, LX/BY6;->f:I

    .line 1794952
    iget-boolean v7, v2, LX/BY6;->a:Z

    if-nez v7, :cond_1a

    iget v7, v2, LX/BY6;->c:F

    const/4 v8, 0x0

    cmpl-float v7, v7, v8

    if-nez v7, :cond_1a

    .line 1794953
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;)LX/BY5;

    move-result-object v6

    .line 1794954
    if-eqz v6, :cond_1a

    .line 1794955
    iget v7, v6, LX/BY5;->d:F

    iput v7, v2, LX/BY6;->c:F

    .line 1794956
    iget v6, v6, LX/BY5;->b:I

    iput v6, v2, LX/BY6;->e:I

    .line 1794957
    :cond_1a
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_15

    .line 1794958
    :cond_1b
    const/4 v2, 0x0

    goto :goto_14

    .line 1794959
    :cond_1c
    invoke-direct/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->f()V

    .line 1794960
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1794961
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 1794962
    if-eqz v2, :cond_1f

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b(Landroid/view/View;)LX/BY5;

    move-result-object v2

    .line 1794963
    :goto_16
    if-eqz v2, :cond_1d

    iget v2, v2, LX/BY5;->b:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-eq v2, v4, :cond_0

    .line 1794964
    :cond_1d
    const/4 v2, 0x0

    :goto_17
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 1794965
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1794966
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;)LX/BY5;

    move-result-object v5

    .line 1794967
    if-eqz v5, :cond_1e

    iget v5, v5, LX/BY5;->b:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ne v5, v6, :cond_1e

    .line 1794968
    invoke-virtual {v4, v3}, Landroid/view/View;->requestFocus(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1794969
    :cond_1e
    add-int/lit8 v2, v2, 0x1

    goto :goto_17

    .line 1794970
    :cond_1f
    const/4 v2, 0x0

    goto :goto_16

    :cond_20
    move/from16 v18, v6

    move-object v6, v2

    move/from16 v2, v18

    goto/16 :goto_11

    :cond_21
    move-object v14, v2

    goto/16 :goto_6

    :cond_22
    move-object v2, v6

    goto/16 :goto_5

    :cond_23
    move-object v4, v3

    move v3, v2

    goto/16 :goto_1
.end method

.method private a(IFI)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    .line 1794821
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->W:I

    if-lez v0, :cond_1

    .line 1794822
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v5

    .line 1794823
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v1

    .line 1794824
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v2

    .line 1794825
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getHeight()I

    move-result v6

    .line 1794826
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v7

    move v4, v3

    .line 1794827
    :goto_0
    if-ge v4, v7, :cond_1

    .line 1794828
    invoke-virtual {p0, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1794829
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/BY6;

    .line 1794830
    iget-boolean v9, v0, LX/BY6;->a:Z

    if-eqz v9, :cond_4

    .line 1794831
    iget v0, v0, LX/BY6;->b:I

    and-int/lit8 v0, v0, 0x7

    .line 1794832
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    move v10, v2

    move v2, v1

    move v1, v10

    .line 1794833
    :goto_1
    add-int/2addr v0, v5

    .line 1794834
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v9

    sub-int/2addr v0, v9

    .line 1794835
    if-eqz v0, :cond_0

    .line 1794836
    invoke-virtual {v8, v0}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1794837
    :cond_0
    :goto_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v10, v1

    move v1, v2

    move v2, v10

    goto :goto_0

    .line 1794838
    :pswitch_1
    invoke-virtual {v8}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v1

    move v10, v1

    move v1, v2

    move v2, v0

    move v0, v10

    .line 1794839
    goto :goto_1

    .line 1794840
    :pswitch_2
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v6, v0

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v10, v2

    move v2, v1

    move v1, v10

    .line 1794841
    goto :goto_1

    .line 1794842
    :pswitch_3
    sub-int v0, v6, v2

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    sub-int/2addr v0, v9

    .line 1794843
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v2, v9

    move v10, v2

    move v2, v1

    move v1, v10

    goto :goto_1

    .line 1794844
    :cond_1
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->c:I

    .line 1794845
    iput p2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->d:F

    .line 1794846
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ae:LX/BY9;

    if-eqz v0, :cond_3

    .line 1794847
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    .line 1794848
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v2

    move v1, v3

    .line 1794849
    :goto_3
    if-ge v1, v2, :cond_3

    .line 1794850
    invoke-virtual {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1794851
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/BY6;

    .line 1794852
    iget-boolean v0, v0, LX/BY6;->a:Z

    if-nez v0, :cond_2

    .line 1794853
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    .line 1794854
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 1794855
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->V:Z

    .line 1794856
    return-void

    :cond_4
    move v10, v2

    move v2, v1

    move v1, v10

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private a(III)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 1794600
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 1794601
    invoke-direct {p0, v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollingCacheEnabled(Z)V

    .line 1794602
    :goto_0
    return-void

    .line 1794603
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollX()I

    move-result v1

    .line 1794604
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v2

    .line 1794605
    sub-int v3, p1, v1

    .line 1794606
    sub-int v4, p2, v2

    .line 1794607
    if-nez v3, :cond_1

    if-nez v4, :cond_1

    .line 1794608
    invoke-direct {p0, v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Z)V

    .line 1794609
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b()V

    .line 1794610
    invoke-static {p0, v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollState(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;I)V

    goto :goto_0

    .line 1794611
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollingCacheEnabled(Z)V

    .line 1794612
    const/4 v0, 0x2

    invoke-static {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollState(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;I)V

    .line 1794613
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v0

    .line 1794614
    div-int/lit8 v5, v0, 0x2

    .line 1794615
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v8

    int-to-float v7, v0

    div-float/2addr v6, v7

    invoke-static {v8, v6}, Ljava/lang/Math;->min(FF)F

    move-result v6

    .line 1794616
    int-to-float v7, v5

    int-to-float v5, v5

    invoke-static {v6}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(F)F

    move-result v6

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    .line 1794617
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 1794618
    if-lez v6, :cond_2

    .line 1794619
    const/high16 v0, 0x447a0000    # 1000.0f

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    .line 1794620
    :goto_1
    const/16 v5, 0x258

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 1794621
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual/range {v0 .. v5}, LX/BYC;->startScroll(IIIII)V

    .line 1794622
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    goto :goto_0

    .line 1794623
    :cond_2
    int-to-float v0, v0

    iget-object v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    invoke-virtual {v5, v6}, LX/0gG;->d(I)F

    move-result v5

    mul-float/2addr v0, v5

    .line 1794624
    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    iget v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    int-to-float v6, v6

    add-float/2addr v0, v6

    div-float v0, v5, v0

    .line 1794625
    add-float/2addr v0, v8

    const/high16 v5, 0x42c80000    # 100.0f

    mul-float/2addr v0, v5

    float-to-int v0, v0

    goto :goto_1
.end method

.method private a(IIII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1794789
    if-lez p2, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1794790
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v0

    sub-int v0, p1, v0

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v0, v2

    add-int/2addr v0, p3

    .line 1794791
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v2

    sub-int v2, p2, v2

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/2addr v2, p4

    .line 1794792
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v3

    .line 1794793
    int-to-float v3, v3

    int-to-float v2, v2

    div-float v2, v3, v2

    .line 1794794
    int-to-float v0, v0

    mul-float/2addr v0, v2

    float-to-int v2, v0

    .line 1794795
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollX()I

    move-result v0

    invoke-virtual {p0, v0, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->scrollTo(II)V

    .line 1794796
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1794797
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->getDuration()I

    move-result v0

    iget-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v3}, LX/BYC;->timePassed()I

    move-result v3

    sub-int v5, v0, v3

    .line 1794798
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b(I)LX/BY5;

    move-result-object v3

    .line 1794799
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    iget v3, v3, LX/BY5;->e:F

    int-to-float v4, p1

    mul-float/2addr v3, v4

    float-to-int v4, v3

    move v3, v1

    invoke-virtual/range {v0 .. v5}, LX/BYC;->startScroll(IIIII)V

    .line 1794800
    :cond_0
    :goto_0
    return-void

    .line 1794801
    :cond_1
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b(I)LX/BY5;

    move-result-object v0

    .line 1794802
    if-eqz v0, :cond_2

    iget v0, v0, LX/BY5;->e:F

    iget v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->v:F

    invoke-static {v0, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1794803
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v2

    sub-int v2, p1, v2

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1794804
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 1794805
    invoke-direct {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Z)V

    .line 1794806
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollX()I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->scrollTo(II)V

    goto :goto_0

    .line 1794807
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(IZ)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1794786
    iput-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->y:Z

    .line 1794787
    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZZ)V

    .line 1794788
    return-void
.end method

.method private a(IZIZ)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1794769
    invoke-direct {p0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b(I)LX/BY5;

    move-result-object v0

    .line 1794770
    if-eqz v0, :cond_5

    .line 1794771
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v2

    .line 1794772
    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->u:F

    iget v0, v0, LX/BY5;->e:F

    iget v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->v:F

    invoke-static {v0, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 1794773
    :goto_0
    if-eqz p2, :cond_2

    .line 1794774
    invoke-direct {p0, v1, v0, p3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(III)V

    .line 1794775
    if-eqz p4, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    if-eqz v0, :cond_0

    .line 1794776
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    invoke-interface {v0, p1}, LX/Avr;->a(I)V

    .line 1794777
    :cond_0
    if-eqz p4, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ac:LX/Avr;

    if-eqz v0, :cond_1

    .line 1794778
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ac:LX/Avr;

    invoke-interface {v0, p1}, LX/Avr;->a(I)V

    .line 1794779
    :cond_1
    :goto_1
    return-void

    .line 1794780
    :cond_2
    if-eqz p4, :cond_3

    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    if-eqz v2, :cond_3

    .line 1794781
    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    invoke-interface {v2, p1}, LX/Avr;->a(I)V

    .line 1794782
    :cond_3
    if-eqz p4, :cond_4

    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ac:LX/Avr;

    if-eqz v2, :cond_4

    .line 1794783
    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ac:LX/Avr;

    invoke-interface {v2, p1}, LX/Avr;->a(I)V

    .line 1794784
    :cond_4
    invoke-direct {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Z)V

    .line 1794785
    invoke-virtual {p0, v1, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->scrollTo(II)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method private a(IZZ)V
    .locals 1

    .prologue
    .line 1794767
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZZI)V

    .line 1794768
    return-void
.end method

.method private a(IZZI)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1794744
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-gtz v0, :cond_1

    .line 1794745
    :cond_0
    invoke-direct {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollingCacheEnabled(Z)V

    .line 1794746
    :goto_0
    return-void

    .line 1794747
    :cond_1
    if-nez p3, :cond_2

    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ne v0, p1, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1794748
    invoke-direct {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollingCacheEnabled(Z)V

    goto :goto_0

    .line 1794749
    :cond_2
    if-gez p1, :cond_5

    move p1, v1

    .line 1794750
    :cond_3
    :goto_1
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->z:I

    .line 1794751
    iget v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/2addr v2, v0

    if-gt p1, v2, :cond_4

    iget v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    sub-int v0, v2, v0

    if-ge p1, v0, :cond_6

    :cond_4
    move v2, v1

    .line 1794752
    :goto_2
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_6

    .line 1794753
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    iput-boolean v3, v0, LX/BY5;->c:Z

    .line 1794754
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 1794755
    :cond_5
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-lt p1, v0, :cond_3

    .line 1794756
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    goto :goto_1

    .line 1794757
    :cond_6
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-eq v0, p1, :cond_7

    move v1, v3

    .line 1794758
    :cond_7
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    if-eqz v0, :cond_a

    .line 1794759
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    .line 1794760
    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    if-eqz v0, :cond_8

    .line 1794761
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    invoke-interface {v0, p1}, LX/Avr;->a(I)V

    .line 1794762
    :cond_8
    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ac:LX/Avr;

    if-eqz v0, :cond_9

    .line 1794763
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ac:LX/Avr;

    invoke-interface {v0, p1}, LX/Avr;->a(I)V

    .line 1794764
    :cond_9
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->requestLayout()V

    goto :goto_0

    .line 1794765
    :cond_a
    invoke-direct {p0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(I)V

    .line 1794766
    invoke-direct {p0, p1, p2, p4, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZIZ)V

    goto :goto_0
.end method

.method private a(LX/BY5;ILX/BY5;)V
    .locals 11

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    .line 1794681
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v6

    .line 1794682
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v0

    .line 1794683
    if-lez v0, :cond_0

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    move v5, v0

    .line 1794684
    :goto_0
    if-eqz p3, :cond_4

    .line 1794685
    iget v0, p3, LX/BY5;->b:I

    .line 1794686
    iget v1, p1, LX/BY5;->b:I

    if-ge v0, v1, :cond_2

    .line 1794687
    const/4 v2, 0x0

    .line 1794688
    iget v1, p3, LX/BY5;->e:F

    iget v3, p3, LX/BY5;->d:F

    add-float/2addr v1, v3

    add-float/2addr v1, v5

    .line 1794689
    add-int/lit8 v0, v0, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    .line 1794690
    :goto_1
    iget v0, p1, LX/BY5;->b:I

    if-gt v1, v0, :cond_4

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 1794691
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1794692
    :goto_2
    iget v4, v0, LX/BY5;->b:I

    if-le v1, v4, :cond_e

    iget-object v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_e

    .line 1794693
    add-int/lit8 v3, v3, 0x1

    .line 1794694
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    goto :goto_2

    .line 1794695
    :cond_0
    const/4 v0, 0x0

    move v5, v0

    goto :goto_0

    .line 1794696
    :goto_3
    iget v4, v0, LX/BY5;->b:I

    if-ge v2, v4, :cond_1

    .line 1794697
    iget-object v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v4, v2}, LX/0gG;->d(I)F

    move-result v4

    add-float/2addr v4, v5

    add-float/2addr v4, v1

    .line 1794698
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v4

    goto :goto_3

    .line 1794699
    :cond_1
    iput v1, v0, LX/BY5;->e:F

    .line 1794700
    iget v0, v0, LX/BY5;->d:F

    add-float/2addr v0, v5

    add-float/2addr v1, v0

    .line 1794701
    add-int/lit8 v0, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    .line 1794702
    :cond_2
    iget v1, p1, LX/BY5;->b:I

    if-le v0, v1, :cond_4

    .line 1794703
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v2, v1, -0x1

    .line 1794704
    iget v1, p3, LX/BY5;->e:F

    .line 1794705
    add-int/lit8 v0, v0, -0x1

    move v3, v2

    move v2, v1

    move v1, v0

    .line 1794706
    :goto_4
    iget v0, p1, LX/BY5;->b:I

    if-lt v1, v0, :cond_4

    if-ltz v3, :cond_4

    .line 1794707
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1794708
    :goto_5
    iget v4, v0, LX/BY5;->b:I

    if-ge v1, v4, :cond_d

    if-lez v3, :cond_d

    .line 1794709
    add-int/lit8 v3, v3, -0x1

    .line 1794710
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    goto :goto_5

    .line 1794711
    :goto_6
    iget v4, v0, LX/BY5;->b:I

    if-le v2, v4, :cond_3

    .line 1794712
    iget-object v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v4, v2}, LX/0gG;->d(I)F

    move-result v4

    add-float/2addr v4, v5

    sub-float v4, v1, v4

    .line 1794713
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    move v1, v4

    goto :goto_6

    .line 1794714
    :cond_3
    iget v4, v0, LX/BY5;->d:F

    add-float/2addr v4, v5

    sub-float/2addr v1, v4

    .line 1794715
    iput v1, v0, LX/BY5;->e:F

    .line 1794716
    add-int/lit8 v0, v2, -0x1

    move v2, v1

    move v1, v0

    goto :goto_4

    .line 1794717
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 1794718
    iget v2, p1, LX/BY5;->e:F

    .line 1794719
    iget v0, p1, LX/BY5;->b:I

    add-int/lit8 v1, v0, -0x1

    .line 1794720
    iget v0, p1, LX/BY5;->b:I

    if-nez v0, :cond_5

    iget v0, p1, LX/BY5;->e:F

    :goto_7
    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->u:F

    .line 1794721
    iget v0, p1, LX/BY5;->b:I

    add-int/lit8 v3, v6, -0x1

    if-ne v0, v3, :cond_6

    iget v0, p1, LX/BY5;->e:F

    iget v3, p1, LX/BY5;->d:F

    add-float/2addr v0, v3

    sub-float/2addr v0, v9

    :goto_8
    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->v:F

    .line 1794722
    add-int/lit8 v0, p2, -0x1

    move v4, v0

    :goto_9
    if-ltz v4, :cond_9

    .line 1794723
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    move v3, v2

    .line 1794724
    :goto_a
    iget v2, v0, LX/BY5;->b:I

    if-le v1, v2, :cond_7

    .line 1794725
    iget-object v8, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v8, v1}, LX/0gG;->d(I)F

    move-result v1

    add-float/2addr v1, v5

    sub-float v1, v3, v1

    move v3, v1

    move v1, v2

    goto :goto_a

    .line 1794726
    :cond_5
    const v0, -0x800001

    goto :goto_7

    .line 1794727
    :cond_6
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_8

    .line 1794728
    :cond_7
    iget v2, v0, LX/BY5;->d:F

    add-float/2addr v2, v5

    sub-float v2, v3, v2

    .line 1794729
    iput v2, v0, LX/BY5;->e:F

    .line 1794730
    iget v0, v0, LX/BY5;->b:I

    if-nez v0, :cond_8

    iput v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->u:F

    .line 1794731
    :cond_8
    add-int/lit8 v0, v4, -0x1

    add-int/lit8 v1, v1, -0x1

    move v4, v0

    goto :goto_9

    .line 1794732
    :cond_9
    iget v0, p1, LX/BY5;->e:F

    iget v1, p1, LX/BY5;->d:F

    add-float/2addr v0, v1

    add-float v2, v0, v5

    .line 1794733
    iget v0, p1, LX/BY5;->b:I

    add-int/lit8 v1, v0, 0x1

    .line 1794734
    add-int/lit8 v0, p2, 0x1

    move v4, v0

    :goto_b
    if-ge v4, v7, :cond_c

    .line 1794735
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    move v3, v2

    .line 1794736
    :goto_c
    iget v2, v0, LX/BY5;->b:I

    if-ge v1, v2, :cond_a

    .line 1794737
    iget-object v8, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v8, v1}, LX/0gG;->d(I)F

    move-result v1

    add-float/2addr v1, v5

    add-float/2addr v1, v3

    move v3, v1

    move v1, v2

    goto :goto_c

    .line 1794738
    :cond_a
    iget v2, v0, LX/BY5;->b:I

    add-int/lit8 v8, v6, -0x1

    if-ne v2, v8, :cond_b

    .line 1794739
    iget v2, v0, LX/BY5;->d:F

    add-float/2addr v2, v3

    sub-float/2addr v2, v9

    iput v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->v:F

    .line 1794740
    :cond_b
    iput v3, v0, LX/BY5;->e:F

    .line 1794741
    iget v0, v0, LX/BY5;->d:F

    add-float/2addr v0, v5

    add-float v2, v3, v0

    .line 1794742
    add-int/lit8 v0, v4, 0x1

    add-int/lit8 v1, v1, 0x1

    move v4, v0

    goto :goto_b

    .line 1794743
    :cond_c
    return-void

    :cond_d
    move v10, v1

    move v1, v2

    move v2, v10

    goto/16 :goto_6

    :cond_e
    move v10, v1

    move v1, v2

    move v2, v10

    goto/16 :goto_3
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 3

    .prologue
    .line 1794671
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1794672
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    .line 1794673
    iget v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    if-ne v1, v2, :cond_0

    .line 1794674
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1794675
    :goto_0
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v1

    iput v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1794676
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    .line 1794677
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 1794678
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 1794679
    :cond_0
    return-void

    .line 1794680
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1794649
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ak:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    move v0, v4

    .line 1794650
    :goto_0
    if-eqz v0, :cond_1

    .line 1794651
    invoke-direct {p0, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollingCacheEnabled(Z)V

    .line 1794652
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v1}, LX/BYC;->abortAnimation()V

    .line 1794653
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollX()I

    move-result v1

    .line 1794654
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v3

    .line 1794655
    iget-object v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v5}, LX/BYC;->getCurrX()I

    move-result v5

    .line 1794656
    iget-object v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v6}, LX/BYC;->getCurrY()I

    move-result v6

    .line 1794657
    if-ne v1, v5, :cond_0

    if-eq v3, v6, :cond_1

    .line 1794658
    :cond_0
    invoke-virtual {p0, v5, v6}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->scrollTo(II)V

    .line 1794659
    :cond_1
    iput-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->y:Z

    move v1, v2

    move v3, v0

    .line 1794660
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1794661
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1794662
    iget-boolean v5, v0, LX/BY5;->c:Z

    if-eqz v5, :cond_2

    .line 1794663
    iput-boolean v2, v0, LX/BY5;->c:Z

    move v3, v4

    .line 1794664
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 1794665
    goto :goto_0

    .line 1794666
    :cond_4
    if-eqz v3, :cond_5

    .line 1794667
    if-eqz p1, :cond_6

    .line 1794668
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aj:Ljava/lang/Runnable;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1794669
    :cond_5
    :goto_2
    return-void

    .line 1794670
    :cond_6
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aj:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_2
.end method

.method private a(FF)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1794648
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->E:I

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    cmpl-float v0, p2, v2

    if-gtz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getHeight()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->E:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_2

    cmpg-float v0, p2, v2

    if-gez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1794637
    const/4 v0, 0x0

    .line 1794638
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    .line 1794639
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 1794640
    :cond_0
    :goto_0
    return v0

    .line 1794641
    :sswitch_0
    const/16 v0, 0x21

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->d(I)Z

    move-result v0

    goto :goto_0

    .line 1794642
    :sswitch_1
    const/16 v0, 0x82

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->d(I)Z

    move-result v0

    goto :goto_0

    .line 1794643
    :sswitch_2
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    .line 1794644
    invoke-static {p1}, LX/3rb;->a(Landroid/view/KeyEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1794645
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->d(I)Z

    move-result v0

    goto :goto_0

    .line 1794646
    :cond_1
    invoke-static {p1, v3}, LX/3rb;->a(Landroid/view/KeyEvent;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1794647
    invoke-direct {p0, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->d(I)Z

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x3d -> :sswitch_2
    .end sparse-switch
.end method

.method private a(Landroid/view/View;ZIII)Z
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 1794626
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    move-object v6, p1

    .line 1794627
    check-cast v6, Landroid/view/ViewGroup;

    .line 1794628
    invoke-virtual {p1}, Landroid/view/View;->getScrollX()I

    move-result v8

    .line 1794629
    invoke-virtual {p1}, Landroid/view/View;->getScrollY()I

    move-result v9

    .line 1794630
    invoke-virtual {v6}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 1794631
    add-int/lit8 v0, v0, -0x1

    move v7, v0

    :goto_0
    if-ltz v7, :cond_2

    .line 1794632
    invoke-virtual {v6, v7}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1794633
    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt v0, v3, :cond_1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v3

    if-ge v0, v3, :cond_1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt v0, v3, :cond_1

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v3

    if-ge v0, v3, :cond_1

    add-int v0, p4, v8

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v4, v0, v3

    add-int v0, p5, v9

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sub-int v5, v0, v3

    move-object v0, p0

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1794634
    :cond_0
    :goto_1
    return v2

    .line 1794635
    :cond_1
    add-int/lit8 v0, v7, -0x1

    move v7, v0

    goto :goto_0

    .line 1794636
    :cond_2
    if-eqz p2, :cond_3

    neg-int v0, p3

    invoke-static {p1, v0}, LX/0vv;->b(Landroid/view/View;I)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private b(I)LX/BY5;
    .locals 3

    .prologue
    .line 1795062
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1795063
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1795064
    iget v2, v0, LX/BY5;->b:I

    if-ne v2, p1, :cond_0

    .line 1795065
    :goto_1
    return-object v0

    .line 1795066
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1795067
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(Landroid/view/View;)LX/BY5;
    .locals 2

    .prologue
    .line 1795056
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eq v0, p0, :cond_2

    .line 1795057
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_1

    .line 1795058
    :cond_0
    const/4 v0, 0x0

    .line 1795059
    :goto_1
    return-object v0

    .line 1795060
    :cond_1
    check-cast v0, Landroid/view/View;

    move-object p1, v0

    goto :goto_0

    .line 1795061
    :cond_2
    invoke-direct {p0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;)LX/BY5;

    move-result-object v0

    goto :goto_1
.end method

.method private b(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1795165
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v3

    move v2, v1

    .line 1795166
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1795167
    if-eqz p1, :cond_0

    const/4 v0, 0x2

    .line 1795168
    :goto_1
    invoke-virtual {p0, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {v4, v0, v5}, LX/0vv;->a(Landroid/view/View;ILandroid/graphics/Paint;)V

    .line 1795169
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 1795170
    goto :goto_1

    .line 1795171
    :cond_1
    return-void
.end method

.method private b(F)Z
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1795172
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    sub-float/2addr v0, p1

    .line 1795173
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1795174
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v1

    int-to-float v1, v1

    .line 1795175
    add-float v5, v1, v0

    .line 1795176
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v7

    .line 1795177
    int-to-float v0, v7

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->u:F

    mul-float v4, v0, v1

    .line 1795178
    int-to-float v0, v7

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->v:F

    mul-float v6, v0, v1

    .line 1795179
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1795180
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    iget-object v8, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BY5;

    .line 1795181
    iget v8, v0, LX/BY5;->b:I

    if-eqz v8, :cond_5

    .line 1795182
    iget v0, v0, LX/BY5;->e:F

    int-to-float v4, v7

    mul-float/2addr v0, v4

    move v4, v0

    move v0, v2

    .line 1795183
    :goto_0
    iget v8, v1, LX/BY5;->b:I

    iget-object v9, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v9}, LX/0gG;->b()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-eq v8, v9, :cond_4

    .line 1795184
    iget v1, v1, LX/BY5;->e:F

    int-to-float v3, v7

    mul-float/2addr v1, v3

    move v3, v2

    .line 1795185
    :goto_1
    cmpg-float v6, v5, v4

    if-gez v6, :cond_1

    .line 1795186
    if-eqz v0, :cond_0

    .line 1795187
    sub-float v0, v4, v5

    .line 1795188
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->S:LX/0vj;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v2, v7

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, LX/0vj;->a(F)Z

    move-result v2

    .line 1795189
    :cond_0
    :goto_2
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    float-to-int v1, v4

    int-to-float v1, v1

    sub-float v1, v4, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1795190
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollX()I

    move-result v0

    float-to-int v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->scrollTo(II)V

    .line 1795191
    float-to-int v0, v4

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->c(I)Z

    .line 1795192
    return v2

    .line 1795193
    :cond_1
    cmpl-float v0, v5, v1

    if-lez v0, :cond_3

    .line 1795194
    if-eqz v3, :cond_2

    .line 1795195
    sub-float v0, v5, v1

    .line 1795196
    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->T:LX/0vj;

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    int-to-float v3, v7

    div-float/2addr v0, v3

    invoke-virtual {v2, v0}, LX/0vj;->a(F)Z

    move-result v2

    :cond_2
    move v4, v1

    .line 1795197
    goto :goto_2

    :cond_3
    move v4, v5

    goto :goto_2

    :cond_4
    move v1, v6

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_0
.end method

.method private c(I)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1795198
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1795199
    iput-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->V:Z

    .line 1795200
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IFI)V

    .line 1795201
    iget-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->V:Z

    if-nez v1, :cond_2

    .line 1795202
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPageScrolled did not call superclass implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1795203
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g()LX/BY5;

    move-result-object v1

    .line 1795204
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v2

    .line 1795205
    iget v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    add-int/2addr v3, v2

    .line 1795206
    iget v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    int-to-float v4, v4

    int-to-float v5, v2

    div-float/2addr v4, v5

    .line 1795207
    iget v5, v1, LX/BY5;->b:I

    .line 1795208
    int-to-float v6, p1

    int-to-float v2, v2

    div-float v2, v6, v2

    iget v6, v1, LX/BY5;->e:F

    sub-float/2addr v2, v6

    iget v1, v1, LX/BY5;->d:F

    add-float/2addr v1, v4

    div-float v1, v2, v1

    .line 1795209
    int-to-float v2, v3

    mul-float/2addr v2, v1

    float-to-int v2, v2

    .line 1795210
    iput-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->V:Z

    .line 1795211
    invoke-direct {p0, v5, v1, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IFI)V

    .line 1795212
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->V:Z

    if-nez v0, :cond_1

    .line 1795213
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "onPageScrolled did not call superclass implementation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1795214
    :cond_1
    const/4 v0, 0x1

    :cond_2
    return v0
.end method

.method private d()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1795215
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setWillNotDraw(Z)V

    .line 1795216
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setDescendantFocusability(I)V

    .line 1795217
    invoke-virtual {p0, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setFocusable(Z)V

    .line 1795218
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1795219
    new-instance v1, LX/BYC;

    sget-object v2, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->f:Landroid/view/animation/Interpolator;

    invoke-direct {v1, p0, v0, v2}, LX/BYC;-><init>(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;Landroid/content/Context;Landroid/view/animation/Interpolator;)V

    iput-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    .line 1795220
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    .line 1795221
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    .line 1795222
    invoke-static {v1}, LX/0iP;->a(Landroid/view/ViewConfiguration;)I

    move-result v3

    iput v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->F:I

    .line 1795223
    const/high16 v3, 0x43c80000    # 400.0f

    mul-float/2addr v3, v2

    float-to-int v3, v3

    iput v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->M:I

    .line 1795224
    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->N:I

    .line 1795225
    new-instance v1, LX/0vj;

    invoke-direct {v1, v0}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->S:LX/0vj;

    .line 1795226
    new-instance v1, LX/0vj;

    invoke-direct {v1, v0}, LX/0vj;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->T:LX/0vj;

    .line 1795227
    const/high16 v0, 0x41c80000    # 25.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->O:I

    .line 1795228
    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->P:I

    .line 1795229
    const/high16 v0, 0x41800000    # 16.0f

    mul-float/2addr v0, v2

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->D:I

    .line 1795230
    const v0, 0x3f19999a    # 0.6f

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->Q:F

    .line 1795231
    new-instance v0, LX/BY7;

    invoke-direct {v0, p0}, LX/BY7;-><init>(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;)V

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 1795232
    invoke-static {p0}, LX/0vv;->e(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_0

    .line 1795233
    invoke-static {p0, v4}, LX/0vv;->d(Landroid/view/View;I)V

    .line 1795234
    :cond_0
    return-void
.end method

.method private d(I)Z
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v8, 0x82

    const/16 v7, 0x21

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1795278
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 1795279
    if-ne v2, p0, :cond_1

    move-object v0, v1

    .line 1795280
    :goto_0
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v1

    invoke-virtual {v1, p0, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 1795281
    if-eqz v1, :cond_7

    if-eq v1, v0, :cond_7

    .line 1795282
    if-ne p1, v7, :cond_5

    .line 1795283
    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->i:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->top:I

    .line 1795284
    iget-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->i:Landroid/graphics/Rect;

    invoke-direct {p0, v3, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->top:I

    .line 1795285
    if-eqz v0, :cond_4

    if-lt v2, v3, :cond_4

    .line 1795286
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->i()Z

    move-result v0

    .line 1795287
    :goto_1
    if-eqz v0, :cond_0

    .line 1795288
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->playSoundEffect(I)V

    .line 1795289
    :cond_0
    return v0

    .line 1795290
    :cond_1
    if-eqz v2, :cond_c

    .line 1795291
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_2
    instance-of v5, v0, Landroid/view/ViewGroup;

    if-eqz v5, :cond_d

    .line 1795292
    if-ne v0, p0, :cond_2

    move v0, v4

    .line 1795293
    :goto_3
    if-nez v0, :cond_c

    .line 1795294
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1795295
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1795296
    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_4
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    .line 1795297
    const-string v2, " => "

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1795298
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_4

    .line 1795299
    :cond_2
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_2

    .line 1795300
    :cond_3
    const-string v0, "ViewPager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "arrowScroll tried to find focus based on non-child current focused view "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 1795301
    goto/16 :goto_0

    .line 1795302
    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto :goto_1

    .line 1795303
    :cond_5
    if-ne p1, v8, :cond_b

    .line 1795304
    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->i:Landroid/graphics/Rect;

    invoke-direct {p0, v2, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    .line 1795305
    iget-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->i:Landroid/graphics/Rect;

    invoke-direct {p0, v3, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/graphics/Rect;Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v3

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    .line 1795306
    if-eqz v0, :cond_6

    if-gt v2, v3, :cond_6

    .line 1795307
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j()Z

    move-result v0

    goto/16 :goto_1

    .line 1795308
    :cond_6
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move-result v0

    goto/16 :goto_1

    .line 1795309
    :cond_7
    if-eq p1, v7, :cond_8

    if-ne p1, v4, :cond_9

    .line 1795310
    :cond_8
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->i()Z

    move-result v0

    goto/16 :goto_1

    .line 1795311
    :cond_9
    if-eq p1, v8, :cond_a

    const/4 v0, 0x2

    if-ne p1, v0, :cond_b

    .line 1795312
    :cond_a
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j()Z

    move-result v0

    goto/16 :goto_1

    :cond_b
    move v0, v3

    goto/16 :goto_1

    :cond_c
    move-object v0, v2

    goto/16 :goto_0

    :cond_d
    move v0, v3

    goto/16 :goto_3
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1795235
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1795236
    invoke-virtual {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1795237
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/BY6;

    .line 1795238
    iget-boolean v0, v0, LX/BY6;->a:Z

    if-nez v0, :cond_0

    .line 1795239
    invoke-virtual {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->removeViewAt(I)V

    .line 1795240
    add-int/lit8 v1, v1, -0x1

    .line 1795241
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1795242
    :cond_1
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 1795243
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ag:I

    if-eqz v0, :cond_2

    .line 1795244
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ah:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 1795245
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ah:Ljava/util/ArrayList;

    .line 1795246
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v1

    .line 1795247
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    .line 1795248
    invoke-virtual {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1795249
    iget-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ah:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1795250
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1795251
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ah:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 1795252
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ah:Ljava/util/ArrayList;

    sget-object v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ai:LX/BYD;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1795253
    :cond_2
    return-void
.end method

.method private g()LX/BY5;
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1795254
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v1

    .line 1795255
    if-lez v1, :cond_3

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v0

    int-to-float v0, v0

    int-to-float v4, v1

    div-float/2addr v0, v4

    move v9, v0

    .line 1795256
    :goto_0
    if-lez v1, :cond_4

    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    move v1, v0

    .line 1795257
    :goto_1
    const/4 v5, -0x1

    .line 1795258
    const/4 v4, 0x1

    .line 1795259
    const/4 v0, 0x0

    move v6, v2

    move v7, v2

    move v8, v5

    move v2, v3

    move v5, v4

    move-object v4, v0

    .line 1795260
    :goto_2
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 1795261
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1795262
    if-nez v5, :cond_6

    iget v10, v0, LX/BY5;->b:I

    add-int/lit8 v11, v8, 0x1

    if-eq v10, v11, :cond_6

    .line 1795263
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->h:LX/BY5;

    .line 1795264
    add-float/2addr v6, v7

    add-float/2addr v6, v1

    iput v6, v0, LX/BY5;->e:F

    .line 1795265
    add-int/lit8 v6, v8, 0x1

    iput v6, v0, LX/BY5;->b:I

    .line 1795266
    iget-object v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget v7, v0, LX/BY5;->b:I

    invoke-virtual {v6, v7}, LX/0gG;->d(I)F

    move-result v6

    iput v6, v0, LX/BY5;->d:F

    .line 1795267
    add-int/lit8 v2, v2, -0x1

    move-object v12, v0

    move v0, v2

    move-object v2, v12

    .line 1795268
    :goto_3
    iget v6, v2, LX/BY5;->e:F

    .line 1795269
    iget v7, v2, LX/BY5;->d:F

    add-float/2addr v7, v6

    add-float/2addr v7, v1

    .line 1795270
    if-nez v5, :cond_0

    cmpl-float v5, v9, v6

    if-ltz v5, :cond_2

    .line 1795271
    :cond_0
    cmpg-float v4, v9, v7

    if-ltz v4, :cond_1

    iget-object v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_5

    :cond_1
    move-object v4, v2

    .line 1795272
    :cond_2
    return-object v4

    :cond_3
    move v9, v2

    .line 1795273
    goto :goto_0

    :cond_4
    move v1, v2

    .line 1795274
    goto :goto_1

    .line 1795275
    :cond_5
    iget v5, v2, LX/BY5;->b:I

    .line 1795276
    iget v4, v2, LX/BY5;->d:F

    .line 1795277
    add-int/lit8 v0, v0, 0x1

    move v7, v6

    move v8, v5

    move v5, v3

    move v6, v4

    move-object v4, v2

    move v2, v0

    goto :goto_2

    :cond_6
    move-object v12, v0

    move v0, v2

    move-object v2, v12

    goto :goto_3
.end method

.method private getClientHeight()I
    .locals 2

    .prologue
    .line 1795158
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private h()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1795159
    iput-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    .line 1795160
    iput-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->C:Z

    .line 1795161
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 1795162
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1795163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    .line 1795164
    :cond_0
    return-void
.end method

.method private i()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1795155
    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-lez v1, :cond_0

    .line 1795156
    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZ)V

    .line 1795157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1795152
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_0

    .line 1795153
    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZ)V

    .line 1795154
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setScrollState(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;I)V
    .locals 1

    .prologue
    .line 1795144
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ak:I

    if-ne v0, p1, :cond_1

    .line 1795145
    :cond_0
    :goto_0
    return-void

    .line 1795146
    :cond_1
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ak:I

    .line 1795147
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ae:LX/BY9;

    if-eqz v0, :cond_2

    .line 1795148
    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b(Z)V

    .line 1795149
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    if-eqz v0, :cond_0

    .line 1795150
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    invoke-interface {v0, p1}, LX/Avr;->b(I)V

    goto :goto_0

    .line 1795151
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private setScrollingCacheEnabled(Z)V
    .locals 1

    .prologue
    .line 1795141
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->x:Z

    if-eq v0, p1, :cond_0

    .line 1795142
    iput-boolean p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->x:Z

    .line 1795143
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1795105
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v8

    .line 1795106
    iput v8, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b:I

    .line 1795107
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->z:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, 0x1

    if-ge v0, v3, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v8, :cond_1

    move v0, v1

    .line 1795108
    :goto_0
    iget v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    move v4, v2

    move v5, v3

    move v6, v0

    move v3, v2

    .line 1795109
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 1795110
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1795111
    iget-object v7, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget-object v9, v0, LX/BY5;->a:Ljava/lang/Object;

    invoke-virtual {v7, v9}, LX/0gG;->a(Ljava/lang/Object;)I

    move-result v7

    .line 1795112
    const/4 v9, -0x1

    if-eq v7, v9, :cond_9

    .line 1795113
    const/4 v9, -0x2

    if-ne v7, v9, :cond_2

    .line 1795114
    iget-object v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1795115
    add-int/lit8 v3, v3, -0x1

    .line 1795116
    if-nez v4, :cond_0

    .line 1795117
    iget-object v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v4, p0}, LX/0gG;->a(Landroid/view/ViewGroup;)V

    move v4, v1

    .line 1795118
    :cond_0
    iget-object v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget v7, v0, LX/BY5;->b:I

    iget-object v9, v0, LX/BY5;->a:Ljava/lang/Object;

    invoke-virtual {v6, p0, v7, v9}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1795119
    iget v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    iget v0, v0, LX/BY5;->b:I

    if-ne v6, v0, :cond_a

    .line 1795120
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    add-int/lit8 v5, v8, -0x1

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    .line 1795121
    :goto_2
    add-int/lit8 v0, v0, 0x1

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 1795122
    goto :goto_0

    .line 1795123
    :cond_2
    iget v9, v0, LX/BY5;->b:I

    if-eq v9, v7, :cond_9

    .line 1795124
    iget v6, v0, LX/BY5;->b:I

    iget v9, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ne v6, v9, :cond_3

    move v5, v7

    .line 1795125
    :cond_3
    iput v7, v0, LX/BY5;->b:I

    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    .line 1795126
    goto :goto_2

    .line 1795127
    :cond_4
    if-eqz v4, :cond_5

    .line 1795128
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0, p0}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 1795129
    :cond_5
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    sget-object v3, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->e:Ljava/util/Comparator;

    invoke-static {v0, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1795130
    if-eqz v6, :cond_8

    .line 1795131
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v4

    move v3, v2

    .line 1795132
    :goto_3
    if-ge v3, v4, :cond_7

    .line 1795133
    invoke-virtual {p0, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1795134
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/BY6;

    .line 1795135
    iget-boolean v6, v0, LX/BY6;->a:Z

    if-nez v6, :cond_6

    .line 1795136
    const/4 v6, 0x0

    iput v6, v0, LX/BY6;->c:F

    .line 1795137
    :cond_6
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 1795138
    :cond_7
    invoke-direct {p0, v5, v2, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZZ)V

    .line 1795139
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->requestLayout()V

    .line 1795140
    :cond_8
    return-void

    :cond_9
    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v6

    goto :goto_2

    :cond_a
    move v0, v3

    move v3, v4

    move v4, v5

    move v5, v1

    goto :goto_2
.end method

.method public final addFocusables(Ljava/util/ArrayList;II)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)V"
        }
    .end annotation

    .prologue
    .line 1795090
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 1795091
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getDescendantFocusability()I

    move-result v2

    .line 1795092
    const/high16 v0, 0x60000

    if-eq v2, v0, :cond_1

    .line 1795093
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 1795094
    invoke-virtual {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1795095
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_0

    .line 1795096
    invoke-direct {p0, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;)LX/BY5;

    move-result-object v4

    .line 1795097
    if-eqz v4, :cond_0

    iget v4, v4, LX/BY5;->b:I

    iget v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ne v4, v5, :cond_0

    .line 1795098
    invoke-virtual {v3, p1, p2, p3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1795099
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1795100
    :cond_1
    const/high16 v0, 0x40000

    if-ne v2, v0, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v1, v0, :cond_3

    .line 1795101
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->isFocusable()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1795102
    :cond_3
    :goto_1
    return-void

    .line 1795103
    :cond_4
    and-int/lit8 v0, p3, 0x1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->isFocusableInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1795104
    :cond_5
    invoke-virtual {p1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public final addTouchables(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1795082
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1795083
    invoke-virtual {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1795084
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 1795085
    invoke-direct {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;)LX/BY5;

    move-result-object v2

    .line 1795086
    if-eqz v2, :cond_0

    iget v2, v2, LX/BY5;->b:I

    iget v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ne v2, v3, :cond_0

    .line 1795087
    invoke-virtual {v1, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 1795088
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1795089
    :cond_1
    return-void
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 4

    .prologue
    .line 1795071
    invoke-virtual {p0, p3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1795072
    invoke-virtual {p0, p3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 1795073
    check-cast v0, LX/BY6;

    .line 1795074
    iget-boolean v2, v0, LX/BY6;->a:Z

    instance-of v3, p1, LX/BY4;

    or-int/2addr v2, v3

    iput-boolean v2, v0, LX/BY6;->a:Z

    .line 1795075
    iget-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->w:Z

    if-eqz v2, :cond_1

    .line 1795076
    iget-boolean v2, v0, LX/BY6;->a:Z

    if-eqz v2, :cond_0

    .line 1795077
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add pager decor view during layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1795078
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/BY6;->d:Z

    .line 1795079
    invoke-virtual {p0, p1, p2, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 1795080
    :goto_1
    return-void

    .line 1795081
    :cond_1
    invoke-super {p0, p1, p2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_2
    move-object v1, p3

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1795069
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(I)V

    .line 1795070
    return-void
.end method

.method public final checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 1795068
    instance-of v0, p1, LX/BY6;

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final computeScroll()V
    .locals 4

    .prologue
    .line 1794808
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1794809
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollX()I

    move-result v0

    .line 1794810
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v1

    .line 1794811
    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v2}, LX/BYC;->getCurrX()I

    move-result v2

    .line 1794812
    iget-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v3}, LX/BYC;->getCurrY()I

    move-result v3

    .line 1794813
    if-ne v0, v2, :cond_0

    if-eq v1, v3, :cond_1

    .line 1794814
    :cond_0
    invoke-virtual {p0, v2, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->scrollTo(II)V

    .line 1794815
    invoke-direct {p0, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1794816
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->abortAnimation()V

    .line 1794817
    const/4 v0, 0x0

    invoke-virtual {p0, v2, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->scrollTo(II)V

    .line 1794818
    :cond_1
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 1794819
    :goto_0
    return-void

    .line 1794820
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Z)V

    goto :goto_0
.end method

.method public final dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 1795055
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1794204
    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v1

    const/16 v2, 0x1000

    if-ne v1, v2, :cond_1

    .line 1794205
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    .line 1794206
    :cond_0
    :goto_0
    return v0

    .line 1794207
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v2

    move v1, v0

    .line 1794208
    :goto_1
    if-ge v1, v2, :cond_0

    .line 1794209
    invoke-virtual {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1794210
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_2

    .line 1794211
    invoke-direct {p0, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;)LX/BY5;

    move-result-object v4

    .line 1794212
    if-eqz v4, :cond_2

    iget v4, v4, LX/BY5;->b:I

    iget v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ne v4, v5, :cond_2

    invoke-virtual {v3, p1}, Landroid/view/View;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1794213
    const/4 v0, 0x1

    goto :goto_0

    .line 1794214
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 1794324
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 1794325
    const/4 v0, 0x0

    .line 1794326
    invoke-static {p0}, LX/0vv;->a(Landroid/view/View;)I

    move-result v1

    .line 1794327
    if-eqz v1, :cond_0

    if-ne v1, v2, :cond_4

    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    if-le v1, v2, :cond_4

    .line 1794328
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->S:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1794329
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1794330
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getHeight()I

    move-result v0

    .line 1794331
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    .line 1794332
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->u:F

    int-to-float v5, v0

    mul-float/2addr v4, v5

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1794333
    iget-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->S:LX/0vj;

    invoke-virtual {v3, v2, v0}, LX/0vj;->a(II)V

    .line 1794334
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->S:LX/0vj;

    invoke-virtual {v0, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    or-int/lit8 v0, v0, 0x0

    .line 1794335
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1794336
    :cond_1
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->T:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1794337
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v1

    .line 1794338
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getHeight()I

    move-result v2

    .line 1794339
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 1794340
    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 1794341
    neg-int v4, v3

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingLeft()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->v:F

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    neg-float v5, v5

    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1794342
    iget-object v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->T:LX/0vj;

    invoke-virtual {v4, v3, v2}, LX/0vj;->a(II)V

    .line 1794343
    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->T:LX/0vj;

    invoke-virtual {v2, p1}, LX/0vj;->a(Landroid/graphics/Canvas;)Z

    move-result v2

    or-int/2addr v0, v2

    .line 1794344
    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 1794345
    :cond_2
    :goto_0
    if-eqz v0, :cond_3

    .line 1794346
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 1794347
    :cond_3
    return-void

    .line 1794348
    :cond_4
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->S:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->b()V

    .line 1794349
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->T:LX/0vj;

    invoke-virtual {v1}, LX/0vj;->b()V

    goto :goto_0
.end method

.method public final drawableStateChanged()V
    .locals 2

    .prologue
    .line 1794319
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 1794320
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->r:Landroid/graphics/drawable/Drawable;

    .line 1794321
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1794322
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1794323
    :cond_0
    return-void
.end method

.method public final generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1794318
    new-instance v0, LX/BY6;

    invoke-direct {v0}, LX/BY6;-><init>()V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 1794317
    new-instance v0, LX/BY6;

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, LX/BY6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public final generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 1794316
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()LX/0gG;
    .locals 1

    .prologue
    .line 1794315
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    return-object v0
.end method

.method public final getChildDrawingOrder(II)I
    .locals 2

    .prologue
    .line 1794313
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ag:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    add-int/lit8 v0, p1, -0x1

    sub-int p2, v0, p2

    .line 1794314
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ah:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/BY6;

    iget v0, v0, LX/BY6;->f:I

    return v0
.end method

.method public getCurrentItem()I
    .locals 1

    .prologue
    .line 1794312
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    return v0
.end method

.method public getFlingDistance()I
    .locals 1

    .prologue
    .line 1794311
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->O:I

    return v0
.end method

.method public getMinimumVelocity()I
    .locals 1

    .prologue
    .line 1794310
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->M:I

    return v0
.end method

.method public getOffscreenPageLimit()I
    .locals 1

    .prologue
    .line 1794309
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->z:I

    return v0
.end method

.method public getOnlyCreatePagesImmediatelyOffscreen()Z
    .locals 1

    .prologue
    .line 1794308
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->A:Z

    return v0
.end method

.method public getPageMargin()I
    .locals 1

    .prologue
    .line 1794307
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    return v0
.end method

.method public getScrollOffset()F
    .locals 1

    .prologue
    .line 1794306
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->d:F

    return v0
.end method

.method public getScrollPosition()I
    .locals 1

    .prologue
    .line 1794305
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->c:I

    return v0
.end method

.method public getTruncator()F
    .locals 1

    .prologue
    .line 1794304
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->Q:F

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x6322bfc7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1794301
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 1794302
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    .line 1794303
    const/16 v1, 0x2d

    const v2, 0x2c50921c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x4989d4c0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1794298
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aj:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1794299
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 1794300
    const/16 v1, 0x2d

    const v2, -0x69325154

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 17

    .prologue
    .line 1794272
    invoke-super/range {p0 .. p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 1794273
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    if-lez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v1, :cond_3

    .line 1794274
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v6

    .line 1794275
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getHeight()I

    move-result v7

    .line 1794276
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    int-to-float v1, v1

    int-to-float v2, v7

    div-float v8, v1, v2

    .line 1794277
    const/4 v5, 0x0

    .line 1794278
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BY5;

    .line 1794279
    iget v4, v1, LX/BY5;->e:F

    .line 1794280
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    .line 1794281
    iget v3, v1, LX/BY5;->b:I

    .line 1794282
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    add-int/lit8 v10, v9, -0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BY5;

    iget v10, v2, LX/BY5;->b:I

    move v2, v5

    move v5, v3

    .line 1794283
    :goto_0
    if-ge v5, v10, :cond_3

    .line 1794284
    :goto_1
    iget v3, v1, LX/BY5;->b:I

    if-le v5, v3, :cond_0

    if-ge v2, v9, :cond_0

    .line 1794285
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BY5;

    goto :goto_1

    .line 1794286
    :cond_0
    iget v3, v1, LX/BY5;->b:I

    if-ne v5, v3, :cond_2

    .line 1794287
    iget v3, v1, LX/BY5;->e:F

    iget v4, v1, LX/BY5;->d:F

    add-float/2addr v3, v4

    int-to-float v4, v7

    mul-float/2addr v3, v4

    .line 1794288
    iget v4, v1, LX/BY5;->e:F

    iget v11, v1, LX/BY5;->d:F

    add-float/2addr v4, v11

    add-float/2addr v4, v8

    .line 1794289
    :goto_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    int-to-float v11, v11

    add-float/2addr v11, v3

    int-to-float v12, v6

    cmpl-float v11, v11, v12

    if-lez v11, :cond_1

    .line 1794290
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->r:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->s:I

    float-to-int v13, v3

    move-object/from16 v0, p0

    iget v14, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->t:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    int-to-float v15, v15

    add-float/2addr v15, v3

    const/high16 v16, 0x3f000000    # 0.5f

    add-float v15, v15, v16

    float-to-int v15, v15

    invoke-virtual {v11, v12, v13, v14, v15}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1794291
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->r:Landroid/graphics/drawable/Drawable;

    move-object/from16 v0, p1

    invoke-virtual {v11, v0}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1794292
    :cond_1
    add-int v11, v6, v7

    int-to-float v11, v11

    cmpl-float v3, v3, v11

    if-gtz v3, :cond_3

    .line 1794293
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 1794294
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v3, v5}, LX/0gG;->d(I)F

    move-result v11

    .line 1794295
    add-float v3, v4, v11

    int-to-float v12, v7

    mul-float/2addr v3, v12

    .line 1794296
    add-float/2addr v11, v8

    add-float/2addr v4, v11

    goto :goto_2

    .line 1794297
    :cond_3
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/4 v3, -0x1

    const/4 v12, 0x0

    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1794215
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    .line 1794216
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    if-ne v0, v6, :cond_2

    .line 1794217
    :cond_0
    iput-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    .line 1794218
    iput-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->C:Z

    .line 1794219
    iput v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    .line 1794220
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 1794221
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 1794222
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    .line 1794223
    :cond_1
    :goto_0
    return v2

    .line 1794224
    :cond_2
    if-eqz v0, :cond_4

    .line 1794225
    iget-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    if-eqz v1, :cond_3

    move v2, v6

    .line 1794226
    goto :goto_0

    .line 1794227
    :cond_3
    iget-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->C:Z

    if-nez v1, :cond_1

    .line 1794228
    :cond_4
    sparse-switch v0, :sswitch_data_0

    .line 1794229
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    if-nez v0, :cond_6

    .line 1794230
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    .line 1794231
    :cond_6
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1794232
    iget-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    goto :goto_0

    .line 1794233
    :sswitch_0
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    .line 1794234
    if-eq v0, v3, :cond_5

    .line 1794235
    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1794236
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v7

    .line 1794237
    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    sub-float v8, v7, v1

    .line 1794238
    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v9

    .line 1794239
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v10

    .line 1794240
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->I:F

    sub-float v0, v10, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v11

    .line 1794241
    cmpl-float v0, v8, v12

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    invoke-direct {p0, v0, v8}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(FF)Z

    move-result v0

    if-nez v0, :cond_7

    float-to-int v3, v8

    float-to-int v4, v10

    float-to-int v5, v7

    move-object v0, p0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;ZIII)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1794242
    iput v7, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1794243
    iput v10, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->G:F

    .line 1794244
    iput-boolean v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->C:Z

    goto :goto_0

    .line 1794245
    :cond_7
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->F:I

    int-to-float v0, v0

    cmpl-float v0, v9, v0

    if-lez v0, :cond_a

    const/high16 v0, 0x3f000000    # 0.5f

    mul-float/2addr v0, v9

    cmpl-float v0, v0, v11

    if-lez v0, :cond_a

    .line 1794246
    iput-boolean v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    .line 1794247
    invoke-static {p0, v6}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollState(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;I)V

    .line 1794248
    cmpl-float v0, v8, v12

    if-lez v0, :cond_9

    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->J:F

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->F:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_2
    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1794249
    iput v7, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1794250
    invoke-direct {p0, v6}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollingCacheEnabled(Z)V

    .line 1794251
    :cond_8
    :goto_3
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    if-eqz v0, :cond_5

    .line 1794252
    invoke-direct {p0, v7}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b(F)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1794253
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    goto/16 :goto_1

    .line 1794254
    :cond_9
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->J:F

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->F:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    goto :goto_2

    .line 1794255
    :cond_a
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->F:I

    int-to-float v0, v0

    cmpl-float v0, v11, v0

    if-lez v0, :cond_8

    .line 1794256
    iput-boolean v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->C:Z

    goto :goto_3

    .line 1794257
    :sswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->I:F

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->G:F

    .line 1794258
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->J:F

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1794259
    invoke-static {p1, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    .line 1794260
    iput-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->C:Z

    .line 1794261
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->computeScrollOffset()Z

    .line 1794262
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ak:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_b

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->getFinalY()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v1}, LX/BYC;->getCurrY()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->P:I

    if-le v0, v1, :cond_b

    .line 1794263
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->abortAnimation()V

    .line 1794264
    iput-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->y:Z

    .line 1794265
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b()V

    .line 1794266
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aa:Z

    if-eqz v0, :cond_5

    .line 1794267
    iput-boolean v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    .line 1794268
    invoke-static {p0, v6}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollState(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;I)V

    goto/16 :goto_1

    .line 1794269
    :cond_b
    invoke-direct {p0, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Z)V

    .line 1794270
    iput-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    goto/16 :goto_1

    .line 1794271
    :sswitch_2
    invoke-direct {p0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x2 -> :sswitch_0
        0x6 -> :sswitch_2
    .end sparse-switch
.end method

.method public final onLayout(ZIIII)V
    .locals 17

    .prologue
    .line 1794148
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v9

    .line 1794149
    sub-int v10, p4, p2

    .line 1794150
    sub-int v11, p5, p3

    .line 1794151
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingLeft()I

    move-result v6

    .line 1794152
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v2

    .line 1794153
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingRight()I

    move-result v5

    .line 1794154
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v3

    .line 1794155
    invoke-virtual/range {p0 .. p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v12

    .line 1794156
    const/4 v4, 0x0

    .line 1794157
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    if-ge v8, v9, :cond_0

    .line 1794158
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    .line 1794159
    invoke-virtual {v13}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v7, 0x8

    if-eq v1, v7, :cond_5

    .line 1794160
    invoke-virtual {v13}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/BY6;

    .line 1794161
    iget-boolean v7, v1, LX/BY6;->a:Z

    if-eqz v7, :cond_5

    .line 1794162
    iget v7, v1, LX/BY6;->b:I

    and-int/lit8 v7, v7, 0x7

    .line 1794163
    iget v1, v1, LX/BY6;->b:I

    and-int/lit8 v14, v1, 0x70

    .line 1794164
    packed-switch v7, :pswitch_data_0

    :pswitch_0
    move v7, v6

    .line 1794165
    :goto_1
    sparse-switch v14, :sswitch_data_0

    move v1, v2

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 1794166
    :goto_2
    add-int/2addr v1, v12

    .line 1794167
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    add-int/2addr v14, v7

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    add-int/2addr v15, v1

    invoke-virtual {v13, v7, v1, v14, v15}, Landroid/view/View;->layout(IIII)V

    .line 1794168
    add-int/lit8 v1, v4, 0x1

    move v4, v3

    move v3, v2

    move v2, v5

    move v5, v6

    .line 1794169
    :goto_3
    add-int/lit8 v6, v8, 0x1

    move v8, v6

    move v6, v5

    move v5, v2

    move v2, v4

    move v4, v1

    goto :goto_0

    .line 1794170
    :pswitch_1
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v6

    move v7, v6

    move v6, v1

    .line 1794171
    goto :goto_1

    .line 1794172
    :pswitch_2
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v10, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    move v7, v1

    .line 1794173
    goto :goto_1

    .line 1794174
    :pswitch_3
    sub-int v1, v10, v5

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v1, v7

    .line 1794175
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v5, v7

    move v7, v1

    goto :goto_1

    .line 1794176
    :sswitch_0
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v1, v2

    move/from16 v16, v2

    move v2, v3

    move v3, v1

    move/from16 v1, v16

    .line 1794177
    goto :goto_2

    .line 1794178
    :sswitch_1
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v11, v1

    div-int/lit8 v1, v1, 0x2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    .line 1794179
    goto :goto_2

    .line 1794180
    :sswitch_2
    sub-int v1, v11, v3

    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    sub-int/2addr v1, v14

    .line 1794181
    invoke-virtual {v13}, Landroid/view/View;->getMeasuredHeight()I

    move-result v14

    add-int/2addr v3, v14

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    goto :goto_2

    .line 1794182
    :cond_0
    sub-int v1, v11, v2

    sub-int v7, v1, v3

    .line 1794183
    const/4 v1, 0x0

    move v3, v1

    :goto_4
    if-ge v3, v9, :cond_3

    .line 1794184
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v8

    .line 1794185
    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v11, 0x8

    if-eq v1, v11, :cond_2

    .line 1794186
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/BY6;

    .line 1794187
    iget-boolean v11, v1, LX/BY6;->a:Z

    if-nez v11, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;)LX/BY5;

    move-result-object v11

    if-eqz v11, :cond_2

    .line 1794188
    int-to-float v12, v7

    iget v11, v11, LX/BY5;->e:F

    mul-float/2addr v11, v12

    float-to-int v11, v11

    .line 1794189
    add-int/2addr v11, v2

    .line 1794190
    iget-boolean v12, v1, LX/BY6;->d:Z

    if-eqz v12, :cond_1

    .line 1794191
    const/4 v12, 0x0

    iput-boolean v12, v1, LX/BY6;->d:Z

    .line 1794192
    sub-int v12, v10, v6

    sub-int/2addr v12, v5

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 1794193
    int-to-float v13, v7

    iget v1, v1, LX/BY6;->c:F

    mul-float/2addr v1, v13

    float-to-int v1, v1

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v1, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1794194
    invoke-virtual {v8, v12, v1}, Landroid/view/View;->measure(II)V

    .line 1794195
    :cond_1
    invoke-virtual {v8}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v6

    invoke-virtual {v8}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v12, v11

    invoke-virtual {v8, v6, v11, v1, v12}, Landroid/view/View;->layout(IIII)V

    .line 1794196
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    .line 1794197
    :cond_3
    move-object/from16 v0, p0

    iput v6, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->s:I

    .line 1794198
    sub-int v1, v10, v5

    move-object/from16 v0, p0

    iput v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->t:I

    .line 1794199
    move-object/from16 v0, p0

    iput v4, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->W:I

    .line 1794200
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    if-eqz v1, :cond_4

    .line 1794201
    move-object/from16 v0, p0

    iget v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZIZ)V

    .line 1794202
    :cond_4
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    .line 1794203
    return-void

    :cond_5
    move v1, v4

    move v4, v2

    move v2, v5

    move v5, v6

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_1
        0x30 -> :sswitch_0
        0x50 -> :sswitch_2
    .end sparse-switch
.end method

.method public final onMeasure(II)V
    .locals 13

    .prologue
    .line 1794389
    const/4 v0, 0x0

    invoke-static {v0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getDefaultSize(II)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1, p2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getDefaultSize(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setMeasuredDimension(II)V

    .line 1794390
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getMeasuredHeight()I

    move-result v0

    .line 1794391
    div-int/lit8 v1, v0, 0xa

    .line 1794392
    iget v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->D:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->E:I

    .line 1794393
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingRight()I

    move-result v2

    sub-int v3, v1, v2

    .line 1794394
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getPaddingBottom()I

    move-result v1

    sub-int v5, v0, v1

    .line 1794395
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v9

    .line 1794396
    const/4 v0, 0x0

    move v8, v0

    :goto_0
    if-ge v8, v9, :cond_8

    .line 1794397
    invoke-virtual {p0, v8}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 1794398
    invoke-virtual {v10}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    .line 1794399
    invoke-virtual {v10}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/BY6;

    .line 1794400
    if-eqz v0, :cond_3

    iget-boolean v1, v0, LX/BY6;->a:Z

    if-eqz v1, :cond_3

    .line 1794401
    iget v1, v0, LX/BY6;->b:I

    and-int/lit8 v6, v1, 0x7

    .line 1794402
    iget v1, v0, LX/BY6;->b:I

    and-int/lit8 v4, v1, 0x70

    .line 1794403
    const/high16 v2, -0x80000000

    .line 1794404
    const/high16 v1, -0x80000000

    .line 1794405
    const/16 v7, 0x30

    if-eq v4, v7, :cond_0

    const/16 v7, 0x50

    if-ne v4, v7, :cond_4

    :cond_0
    const/4 v4, 0x1

    move v7, v4

    .line 1794406
    :goto_1
    const/4 v4, 0x3

    if-eq v6, v4, :cond_1

    const/4 v4, 0x5

    if-ne v6, v4, :cond_5

    :cond_1
    const/4 v4, 0x1

    move v6, v4

    .line 1794407
    :goto_2
    if-eqz v7, :cond_6

    .line 1794408
    const/high16 v2, 0x40000000    # 2.0f

    .line 1794409
    :cond_2
    :goto_3
    iget v4, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v11, -0x2

    if-eq v4, v11, :cond_d

    .line 1794410
    const/high16 v4, 0x40000000    # 2.0f

    .line 1794411
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v11, -0x1

    if-eq v2, v11, :cond_c

    .line 1794412
    iget v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1794413
    :goto_4
    iget v11, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v12, -0x2

    if-eq v11, v12, :cond_b

    .line 1794414
    const/high16 v1, 0x40000000    # 2.0f

    .line 1794415
    iget v11, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    const/4 v12, -0x1

    if-eq v11, v12, :cond_b

    .line 1794416
    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1794417
    :goto_5
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1794418
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1794419
    invoke-virtual {v10, v2, v0}, Landroid/view/View;->measure(II)V

    .line 1794420
    if-eqz v7, :cond_7

    .line 1794421
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sub-int/2addr v5, v0

    .line 1794422
    :cond_3
    :goto_6
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    .line 1794423
    :cond_4
    const/4 v4, 0x0

    move v7, v4

    goto :goto_1

    .line 1794424
    :cond_5
    const/4 v4, 0x0

    move v6, v4

    goto :goto_2

    .line 1794425
    :cond_6
    if-eqz v6, :cond_2

    .line 1794426
    const/high16 v1, 0x40000000    # 2.0f

    goto :goto_3

    .line 1794427
    :cond_7
    if-eqz v6, :cond_3

    .line 1794428
    invoke-virtual {v10}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    sub-int/2addr v3, v0

    goto :goto_6

    .line 1794429
    :cond_8
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1794430
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->w:Z

    .line 1794431
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b()V

    .line 1794432
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->w:Z

    .line 1794433
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v3

    .line 1794434
    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v3, :cond_a

    .line 1794435
    invoke-virtual {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1794436
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v6, 0x8

    if-eq v0, v6, :cond_9

    .line 1794437
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, LX/BY6;

    .line 1794438
    iget-boolean v6, v0, LX/BY6;->a:Z

    if-nez v6, :cond_9

    .line 1794439
    int-to-float v6, v5

    iget v0, v0, LX/BY6;->c:F

    mul-float/2addr v0, v6

    float-to-int v0, v0

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v0, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 1794440
    invoke-virtual {v4, v2, v0}, Landroid/view/View;->measure(II)V

    .line 1794441
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 1794442
    :cond_a
    return-void

    :cond_b
    move v0, v5

    goto :goto_5

    :cond_c
    move v2, v3

    goto :goto_4

    :cond_d
    move v4, v2

    move v2, v3

    goto :goto_4
.end method

.method public final onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    const/4 v1, -0x1

    .line 1794587
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildCount()I

    move-result v0

    .line 1794588
    and-int/lit8 v3, p1, 0x2

    if-eqz v3, :cond_0

    move v1, v2

    move v3, v4

    .line 1794589
    :goto_0
    if-eq v3, v0, :cond_2

    .line 1794590
    invoke-virtual {p0, v3}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1794591
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_1

    .line 1794592
    invoke-direct {p0, v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/View;)LX/BY5;

    move-result-object v6

    .line 1794593
    if-eqz v6, :cond_1

    iget v6, v6, LX/BY5;->b:I

    iget v7, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    if-ne v6, v7, :cond_1

    .line 1794594
    invoke-virtual {v5, p1, p2}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1794595
    :goto_1
    return v2

    .line 1794596
    :cond_0
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v0, v1

    .line 1794597
    goto :goto_0

    .line 1794598
    :cond_1
    add-int/2addr v3, v1

    goto :goto_0

    :cond_2
    move v2, v4

    .line 1794599
    goto :goto_1
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    .prologue
    .line 1794576
    instance-of v0, p1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;

    if-nez v0, :cond_0

    .line 1794577
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1794578
    :goto_0
    return-void

    .line 1794579
    :cond_0
    check-cast p1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;

    .line 1794580
    invoke-virtual {p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1794581
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v0, :cond_1

    .line 1794582
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget-object v1, p1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->b:Landroid/os/Parcelable;

    iget-object v2, p1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->c:Ljava/lang/ClassLoader;

    invoke-virtual {v0, v1, v2}, LX/0gG;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 1794583
    iget v0, p1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->a:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZZ)V

    goto :goto_0

    .line 1794584
    :cond_1
    iget v0, p1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->a:I

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->l:I

    .line 1794585
    iget-object v0, p1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->b:Landroid/os/Parcelable;

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->m:Landroid/os/Parcelable;

    .line 1794586
    iget-object v0, p1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->c:Ljava/lang/ClassLoader;

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->n:Ljava/lang/ClassLoader;

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1794570
    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1794571
    new-instance v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1794572
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    iput v0, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->a:I

    .line 1794573
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v0, :cond_0

    .line 1794574
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->lf_()Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, v1, Lcom/facebook/widget/verticalviewpager/VerticalViewPager$SavedState;->b:Landroid/os/Parcelable;

    .line 1794575
    :cond_0
    return-object v1
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7df6ee70

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1794566
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1794567
    if-eq p2, p4, :cond_0

    .line 1794568
    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    iget v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    invoke-direct {p0, p2, p4, v1, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IIII)V

    .line 1794569
    :cond_0
    const/16 v1, 0x2d

    const v2, 0xdcd53b3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    const v0, 0x728957a2

    invoke-static {v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 1794494
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->R:Z

    if-eqz v0, :cond_0

    .line 1794495
    const v0, -0x2f1e6d73

    invoke-static {v4, v4, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    move v0, v1

    .line 1794496
    :goto_0
    return v0

    .line 1794497
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v0

    if-eqz v0, :cond_1

    .line 1794498
    const v0, -0x567e752f

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v2

    goto :goto_0

    .line 1794499
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0}, LX/0gG;->b()I

    move-result v0

    if-nez v0, :cond_3

    .line 1794500
    :cond_2
    const v0, 0x392c830a

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v2

    goto :goto_0

    .line 1794501
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    if-nez v0, :cond_4

    .line 1794502
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    .line 1794503
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1794504
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1794505
    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    .line 1794506
    :cond_5
    :goto_1
    :pswitch_0
    if-eqz v2, :cond_6

    .line 1794507
    invoke-static {p0}, LX/0vv;->d(Landroid/view/View;)V

    .line 1794508
    :cond_6
    const v0, 0x72021333

    invoke-static {v0, v3}, LX/02F;->a(II)V

    move v0, v1

    goto :goto_0

    .line 1794509
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    invoke-virtual {v0}, LX/BYC;->abortAnimation()V

    .line 1794510
    iput-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->y:Z

    .line 1794511
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b()V

    .line 1794512
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aa:Z

    if-eqz v0, :cond_7

    .line 1794513
    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    .line 1794514
    invoke-static {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollState(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;I)V

    .line 1794515
    :cond_7
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->I:F

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->G:F

    .line 1794516
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->J:F

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1794517
    invoke-static {p1, v2}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    goto :goto_1

    .line 1794518
    :pswitch_2
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    if-nez v0, :cond_8

    .line 1794519
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1794520
    invoke-static {p1, v0}, LX/2xd;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 1794521
    iget v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->G:F

    sub-float v5, v4, v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    .line 1794522
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 1794523
    iget v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    sub-float v6, v0, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    .line 1794524
    iget v7, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->F:I

    int-to-float v7, v7

    cmpl-float v7, v6, v7

    if-lez v7, :cond_8

    cmpl-float v5, v6, v5

    if-lez v5, :cond_8

    .line 1794525
    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    .line 1794526
    iget v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->J:F

    sub-float/2addr v0, v5

    const/4 v5, 0x0

    cmpl-float v0, v0, v5

    if-lez v0, :cond_9

    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->J:F

    iget v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->F:I

    int-to-float v5, v5

    add-float/2addr v0, v5

    :goto_2
    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1794527
    iput v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->G:F

    .line 1794528
    invoke-static {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollState(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;I)V

    .line 1794529
    invoke-direct {p0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setScrollingCacheEnabled(Z)V

    .line 1794530
    :cond_8
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    if-eqz v0, :cond_5

    .line 1794531
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1794532
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    .line 1794533
    invoke-direct {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b(F)Z

    move-result v0

    or-int/lit8 v2, v0, 0x0

    .line 1794534
    goto/16 :goto_1

    .line 1794535
    :cond_9
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->J:F

    iget v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->F:I

    int-to-float v5, v5

    sub-float/2addr v0, v5

    goto :goto_2

    .line 1794536
    :pswitch_3
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    if-eqz v0, :cond_5

    .line 1794537
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->L:Landroid/view/VelocityTracker;

    .line 1794538
    const/16 v2, 0x3e8

    iget v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->N:I

    int-to-float v4, v4

    invoke-virtual {v0, v2, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1794539
    iget v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    invoke-static {v0, v2}, LX/2uG;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    float-to-int v0, v0

    .line 1794540
    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->y:Z

    .line 1794541
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getClientHeight()I

    move-result v2

    .line 1794542
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getScrollY()I

    move-result v4

    .line 1794543
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g()LX/BY5;

    move-result-object v5

    .line 1794544
    iget v6, v5, LX/BY5;->b:I

    .line 1794545
    int-to-float v4, v4

    int-to-float v2, v2

    div-float v2, v4, v2

    iget v4, v5, LX/BY5;->e:F

    sub-float/2addr v2, v4

    iget v4, v5, LX/BY5;->d:F

    div-float/2addr v2, v4

    .line 1794546
    iget v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    invoke-static {p1, v4}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v4

    .line 1794547
    invoke-static {p1, v4}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 1794548
    iget v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->I:F

    sub-float/2addr v4, v5

    float-to-int v4, v4

    .line 1794549
    invoke-direct {p0, v6, v2, v0, v4}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IFII)I

    move-result v2

    .line 1794550
    invoke-direct {p0, v2, v1, v1, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZZI)V

    .line 1794551
    iput v7, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    .line 1794552
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->h()V

    .line 1794553
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->S:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->c()Z

    move-result v0

    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->T:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->c()Z

    move-result v2

    or-int/2addr v2, v0

    .line 1794554
    goto/16 :goto_1

    .line 1794555
    :pswitch_4
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->B:Z

    if-eqz v0, :cond_5

    .line 1794556
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    invoke-direct {p0, v0, v1, v2, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZIZ)V

    .line 1794557
    iput v7, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    .line 1794558
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->h()V

    .line 1794559
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->S:LX/0vj;

    invoke-virtual {v0}, LX/0vj;->c()Z

    move-result v0

    iget-object v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->T:LX/0vj;

    invoke-virtual {v2}, LX/0vj;->c()Z

    move-result v2

    or-int/2addr v2, v0

    goto/16 :goto_1

    .line 1794560
    :pswitch_5
    invoke-static {p1}, LX/2xd;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1794561
    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v4

    .line 1794562
    iput v4, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    .line 1794563
    invoke-static {p1, v0}, LX/2xd;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    goto/16 :goto_1

    .line 1794564
    :pswitch_6
    invoke-direct {p0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(Landroid/view/MotionEvent;)V

    .line 1794565
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->K:I

    invoke-static {p1, v0}, LX/2xd;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    invoke-static {p1, v0}, LX/2xd;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->H:F

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final removeView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1794490
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->w:Z

    if-eqz v0, :cond_0

    .line 1794491
    invoke-virtual {p0, p1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->removeViewInLayout(Landroid/view/View;)V

    .line 1794492
    :goto_0
    return-void

    .line 1794493
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setAdapter(LX/0gG;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1794458
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v0, :cond_1

    .line 1794459
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->p:LX/BYA;

    invoke-virtual {v0, v1}, LX/0gG;->b(Landroid/database/DataSetObserver;)V

    .line 1794460
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0, p0}, LX/0gG;->a(Landroid/view/ViewGroup;)V

    move v1, v2

    .line 1794461
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1794462
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BY5;

    .line 1794463
    iget-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget v4, v0, LX/BY5;->b:I

    iget-object v0, v0, LX/BY5;->a:Ljava/lang/Object;

    invoke-virtual {v3, p0, v4, v0}, LX/0gG;->a(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 1794464
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1794465
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v0, p0}, LX/0gG;->b(Landroid/view/ViewGroup;)V

    .line 1794466
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1794467
    invoke-direct {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->e()V

    .line 1794468
    iput v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->k:I

    .line 1794469
    invoke-virtual {p0, v2, v2}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->scrollTo(II)V

    .line 1794470
    :cond_1
    iput-object p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    .line 1794471
    iput v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b:I

    .line 1794472
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    if-eqz v0, :cond_3

    .line 1794473
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->p:LX/BYA;

    if-nez v0, :cond_2

    .line 1794474
    new-instance v0, LX/BYA;

    invoke-direct {v0, p0}, LX/BYA;-><init>(Lcom/facebook/widget/verticalviewpager/VerticalViewPager;)V

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->p:LX/BYA;

    .line 1794475
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->p:LX/BYA;

    invoke-virtual {v0, v1}, LX/0gG;->a(Landroid/database/DataSetObserver;)V

    .line 1794476
    iput-boolean v2, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->y:Z

    .line 1794477
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    .line 1794478
    iput-boolean v5, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    .line 1794479
    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    iput v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b:I

    .line 1794480
    iget v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->l:I

    if-ltz v1, :cond_4

    .line 1794481
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->j:LX/0gG;

    iget-object v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->m:Landroid/os/Parcelable;

    iget-object v3, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->n:Ljava/lang/ClassLoader;

    invoke-virtual {v0, v1, v3}, LX/0gG;->a(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V

    .line 1794482
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->l:I

    invoke-direct {p0, v0, v2, v5}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZZ)V

    .line 1794483
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->l:I

    .line 1794484
    iput-object v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->m:Landroid/os/Parcelable;

    .line 1794485
    iput-object v6, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->n:Ljava/lang/ClassLoader;

    .line 1794486
    :cond_3
    :goto_1
    return-void

    .line 1794487
    :cond_4
    if-nez v0, :cond_5

    .line 1794488
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b()V

    goto :goto_1

    .line 1794489
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->requestLayout()V

    goto :goto_1
.end method

.method public setChildrenDrawingOrderEnabledCompat(Z)V
    .locals 5

    .prologue
    .line 1794449
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x7

    if-lt v0, v1, :cond_1

    .line 1794450
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->af:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 1794451
    :try_start_0
    const-class v0, Landroid/view/ViewGroup;

    const-string v1, "setChildrenDrawingOrderEnabled"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->af:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1794452
    :cond_0
    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->af:Ljava/lang/reflect/Method;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, p0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 1794453
    :cond_1
    :goto_1
    return-void

    .line 1794454
    :catch_0
    move-exception v0

    .line 1794455
    const-string v1, "ViewPager"

    const-string v2, "Can\'t find setChildrenDrawingOrderEnabled"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1794456
    :catch_1
    move-exception v0

    .line 1794457
    const-string v1, "ViewPager"

    const-string v2, "Error changing children drawing order"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public setCurrentItem(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1794445
    iput-boolean v1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->y:Z

    .line 1794446
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->U:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IZZ)V

    .line 1794447
    return-void

    :cond_0
    move v0, v1

    .line 1794448
    goto :goto_0
.end method

.method public setFlingDistance(I)V
    .locals 0

    .prologue
    .line 1794443
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->O:I

    .line 1794444
    return-void
.end method

.method public setIgnoreTouchSlop(Z)V
    .locals 0

    .prologue
    .line 1794350
    iput-boolean p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->aa:Z

    .line 1794351
    return-void
.end method

.method public setMinimumVelocity(I)V
    .locals 0

    .prologue
    .line 1794387
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->M:I

    .line 1794388
    return-void
.end method

.method public setOffscreenPageLimit(I)V
    .locals 3

    .prologue
    .line 1794380
    if-gtz p1, :cond_0

    .line 1794381
    const-string v0, "ViewPager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested offscreen page limit "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too small; defaulting to 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1794382
    const/4 p1, 0x1

    .line 1794383
    :cond_0
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->z:I

    if-eq p1, v0, :cond_1

    .line 1794384
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->z:I

    .line 1794385
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b()V

    .line 1794386
    :cond_1
    return-void
.end method

.method public setOnAdapterChangeListener(LX/BY8;)V
    .locals 0

    .prologue
    .line 1794378
    iput-object p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ad:LX/BY8;

    .line 1794379
    return-void
.end method

.method public setOnPageChangeListener(LX/Avr;)V
    .locals 0

    .prologue
    .line 1794376
    iput-object p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->ab:LX/Avr;

    .line 1794377
    return-void
.end method

.method public setOnlyCreatePagesImmediatelyOffscreen(Z)V
    .locals 1

    .prologue
    .line 1794372
    iget-boolean v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->A:Z

    if-eq p1, v0, :cond_0

    .line 1794373
    iput-boolean p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->A:Z

    .line 1794374
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->b()V

    .line 1794375
    :cond_0
    return-void
.end method

.method public setPageMargin(I)V
    .locals 2

    .prologue
    .line 1794366
    iget v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    .line 1794367
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->q:I

    .line 1794368
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getHeight()I

    move-result v1

    .line 1794369
    invoke-direct {p0, v1, v1, p1, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->a(IIII)V

    .line 1794370
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->requestLayout()V

    .line 1794371
    return-void
.end method

.method public setPageMarginDrawable(I)V
    .locals 1

    .prologue
    .line 1794364
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1794365
    return-void
.end method

.method public setPageMarginDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1794358
    iput-object p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->r:Landroid/graphics/drawable/Drawable;

    .line 1794359
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->refreshDrawableState()V

    .line 1794360
    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->setWillNotDraw(Z)V

    .line 1794361
    invoke-virtual {p0}, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->invalidate()V

    .line 1794362
    return-void

    .line 1794363
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setScrollFactor(D)V
    .locals 2

    .prologue
    .line 1794355
    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->o:LX/BYC;

    .line 1794356
    iput-wide p1, v0, LX/BYC;->b:D

    .line 1794357
    return-void
.end method

.method public setTruncator(F)V
    .locals 0

    .prologue
    .line 1794353
    iput p1, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->Q:F

    .line 1794354
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 1794352
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/verticalviewpager/VerticalViewPager;->r:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
