.class public Lcom/facebook/widget/friendselector/FriendSelectorResultBar;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/4mV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

.field private d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

.field public e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/view/View;

.field public h:LX/BWy;

.field private i:LX/4mU;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1792547
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1792548
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a()V

    .line 1792549
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1792544
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1792545
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a()V

    .line 1792546
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1792541
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1792542
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a()V

    .line 1792543
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 1792520
    const-class v0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    invoke-static {v0, p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1792521
    const v0, 0x7f0306f2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1792522
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->setVisibility(I)V

    .line 1792523
    invoke-virtual {p0, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->setClickable(Z)V

    .line 1792524
    const v0, 0x7f0d129b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    .line 1792525
    const v0, 0x7f0d129c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    .line 1792526
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->setBadgeView(Lcom/facebook/widget/friendselector/SelectedFriendItemView;)V

    .line 1792527
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->setClickable(Z)V

    .line 1792528
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    new-instance v1, LX/BWq;

    invoke-direct {v1, p0}, LX/BWq;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;)V

    .line 1792529
    iput-object v1, v0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->c:LX/BWp;

    .line 1792530
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    new-instance v1, LX/BWs;

    invoke-direct {v1, p0}, LX/BWs;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;)V

    .line 1792531
    iput-object v1, v0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->d:LX/BWr;

    .line 1792532
    const v0, 0x7f0d1299

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    .line 1792533
    const v0, 0x7f0d129a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->f:Landroid/view/View;

    .line 1792534
    const v0, 0x7f0d118a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->g:Landroid/view/View;

    .line 1792535
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->g:Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080be6

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1792536
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->g:Landroid/view/View;

    new-instance v1, LX/BWt;

    invoke-direct {v1, p0}, LX/BWt;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1792537
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->b:LX/4mV;

    invoke-virtual {v0, p0}, LX/4mV;->a(Landroid/view/View;)LX/4mU;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->i:LX/4mU;

    .line 1792538
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->i:LX/4mU;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, LX/4mU;->a(J)V

    .line 1792539
    return-void

    .line 1792540
    :cond_0
    const v0, 0x7f080be5

    goto :goto_0
.end method

.method private static a(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;Ljava/lang/Boolean;LX/4mV;)V
    .locals 0

    .prologue
    .line 1792519
    iput-object p1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a:Ljava/lang/Boolean;

    iput-object p2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->b:LX/4mV;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v1}, LX/4mV;->a(LX/0QB;)LX/4mV;

    move-result-object v1

    check-cast v1, LX/4mV;

    invoke-static {p0, v0, v1}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;Ljava/lang/Boolean;LX/4mV;)V

    return-void
.end method

.method public static b$redex0(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;)V
    .locals 2

    .prologue
    .line 1792477
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 1792478
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->d()V

    .line 1792479
    :cond_0
    :goto_0
    return-void

    .line 1792480
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 1792481
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c()V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1792515
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->setVisibility(I)V

    .line 1792516
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->i:LX/4mU;

    new-instance v1, LX/BWu;

    invoke-direct {v1, p0}, LX/BWu;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;)V

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1792517
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->i:LX/4mU;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    .line 1792518
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1792512
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->i:LX/4mU;

    new-instance v1, LX/BWv;

    invoke-direct {v1, p0}, LX/BWv;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;)V

    invoke-virtual {v0, v1}, LX/4mU;->a(LX/4mR;)V

    .line 1792513
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->i:LX/4mU;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, LX/4mU;->j(F)V

    .line 1792514
    return-void
.end method


# virtual methods
.method public final a(LX/BWx;)V
    .locals 2

    .prologue
    .line 1792509
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1792510
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    new-instance v1, LX/BWw;

    invoke-direct {v1, p0, p1}, LX/BWw;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultBar;LX/BWx;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1792511
    return-void
.end method

.method public final a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V
    .locals 1

    .prologue
    .line 1792505
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1792506
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c()V

    .line 1792507
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 1792508
    return-void
.end method

.method public final a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V
    .locals 1

    .prologue
    .line 1792503
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->c:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V

    .line 1792504
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1792500
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->f:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1792501
    return-void

    .line 1792502
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1792485
    iget-object v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->g:Landroid/view/View;

    if-eqz p1, :cond_2

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1792486
    if-eqz p1, :cond_1

    .line 1792487
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->removeView(Landroid/view/View;)V

    .line 1792488
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->removeView(Landroid/view/View;)V

    .line 1792489
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1792490
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b04a0

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 1792491
    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    float-to-int v4, v2

    invoke-virtual {v0, v3, v1, v4, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1792492
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 1792493
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v1, v3, :cond_0

    .line 1792494
    float-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginEnd(I)V

    .line 1792495
    :cond_0
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1792496
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1792497
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->f:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1792498
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1792499
    goto :goto_0
.end method

.method public getAddNoteButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 1792484
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->e:Landroid/view/View;

    return-object v0
.end method

.method public setListener(LX/BWy;)V
    .locals 0

    .prologue
    .line 1792482
    iput-object p1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->h:LX/BWy;

    .line 1792483
    return-void
.end method
