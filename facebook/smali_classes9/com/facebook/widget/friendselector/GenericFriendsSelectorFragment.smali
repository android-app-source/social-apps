.class public abstract Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static final v:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:Ljava/lang/Boolean;

.field private B:LX/3Oq;

.field public C:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

.field public D:Lcom/facebook/widget/listview/BetterListView;

.field public E:Landroid/widget/ListView;

.field public F:LX/2EJ;

.field public G:LX/BXD;

.field private H:Landroid/view/View;

.field public I:Landroid/view/View;

.field public J:Landroid/widget/TextView;

.field public K:Landroid/view/ViewStub;

.field public L:Z

.field public M:Z

.field public final N:LX/3NY;

.field public final O:Landroid/widget/AbsListView$OnScrollListener;

.field public c:LX/0TD;

.field public d:LX/1Ck;

.field public e:LX/3LP;

.field public f:LX/0Sy;

.field public g:Landroid/view/inputmethod/InputMethodManager;

.field public h:LX/8uo;

.field public i:LX/8RL;

.field public j:LX/8tB;

.field public k:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

.field public l:Landroid/content/ContentResolver;

.field public m:LX/BWl;

.field public n:LX/BWn;

.field public o:LX/2RQ;

.field public p:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field

.field public s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public t:Landroid/view/View;

.field private final u:LX/BXU;

.field public w:LX/BWm;

.field public x:LX/0kL;

.field public y:Ljava/lang/String;

.field public z:Landroid/content/res/Resources;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1696483
    const-class v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    sput-object v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->v:Ljava/lang/Class;

    .line 1696484
    const-string v0, "all_friends_alphabetic_section"

    sput-object v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a:Ljava/lang/String;

    .line 1696485
    const-string v0, "all_coworkers_alphabetic_section"

    sput-object v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1696486
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1696487
    new-instance v0, LX/BXU;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, LX/BXU;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->u:LX/BXU;

    .line 1696488
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1696489
    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    .line 1696490
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1696491
    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->q:LX/0Rf;

    .line 1696492
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    .line 1696493
    new-instance v0, LX/BXL;

    invoke-direct {v0, p0}, LX/BXL;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->N:LX/3NY;

    .line 1696494
    new-instance v0, LX/BXM;

    invoke-direct {v0, p0}, LX/BXM;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->O:Landroid/widget/AbsListView$OnScrollListener;

    .line 1696495
    return-void
.end method

.method public static Q(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V
    .locals 2

    .prologue
    .line 1696496
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1696497
    :goto_0
    return-void

    .line 1696498
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->J:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1696499
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->E()V

    goto :goto_0
.end method

.method public static a(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1696500
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1696501
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v0, :cond_0

    .line 1696502
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    .line 1696503
    :goto_0
    return-object v0

    .line 1696504
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    check-cast v0, [LX/8ul;

    .line 1696505
    array-length v4, v0

    const/4 v1, 0x0

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 1696506
    iget-object p0, v1, LX/8uk;->f:LX/8QK;

    move-object v1, p0

    .line 1696507
    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1696508
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 1696509
    goto :goto_0
.end method

.method private static a(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;LX/0TD;LX/1Ck;LX/3LP;LX/0Sy;Landroid/view/inputmethod/InputMethodManager;LX/8uo;LX/8RL;LX/BXZ;Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/BWl;Landroid/content/ContentResolver;LX/BWn;LX/2RQ;LX/0kL;Landroid/content/res/Resources;Ljava/lang/Boolean;LX/3Oq;)V
    .locals 4
    .param p0    # Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p15    # Landroid/content/res/Resources;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p16    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/contacts/module/ContactLinkQueryType;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1696510
    iput-object p1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c:LX/0TD;

    .line 1696511
    iput-object p2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->d:LX/1Ck;

    .line 1696512
    iput-object p3, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->e:LX/3LP;

    .line 1696513
    iput-object p4, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->f:LX/0Sy;

    .line 1696514
    iput-object p5, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->g:Landroid/view/inputmethod/InputMethodManager;

    .line 1696515
    iput-object p6, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->h:LX/8uo;

    .line 1696516
    iput-object p7, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->i:LX/8RL;

    .line 1696517
    iput-object p8, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    .line 1696518
    iput-object p9, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->k:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    .line 1696519
    iput-object p11, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->l:Landroid/content/ContentResolver;

    .line 1696520
    iput-object p10, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    .line 1696521
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n:LX/BWn;

    .line 1696522
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o:LX/2RQ;

    .line 1696523
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->x:LX/0kL;

    .line 1696524
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->z:Landroid/content/res/Resources;

    .line 1696525
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->A:Ljava/lang/Boolean;

    .line 1696526
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->B:LX/3Oq;

    .line 1696527
    const/4 v1, 0x0

    .line 1696528
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1696529
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "is_show_caspian_style"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    .line 1696530
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "is_sticky_header_off"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1696531
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "hide_caspian_send_button"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->M:Z

    .line 1696532
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n:LX/BWn;

    invoke-virtual {v2, v1}, LX/BWn;->b(Z)V

    .line 1696533
    :cond_0
    iget-boolean v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v2, :cond_1

    .line 1696534
    new-instance v2, LX/BWm;

    const/4 v3, 0x1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    invoke-direct {v2, v3, v1}, LX/BWm;-><init>(ZZ)V

    iput-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->w:LX/BWm;

    .line 1696535
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->w:LX/BWm;

    iput-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    .line 1696536
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->n:LX/BWn;

    iput-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    .line 1696537
    :cond_1
    return-void

    .line 1696538
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 18

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v17

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-static/range {v17 .. v17}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-static/range {v17 .. v17}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static/range {v17 .. v17}, LX/3LP;->a(LX/0QB;)LX/3LP;

    move-result-object v3

    check-cast v3, LX/3LP;

    invoke-static/range {v17 .. v17}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v4

    check-cast v4, LX/0Sy;

    invoke-static/range {v17 .. v17}, LX/10d;->a(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v5

    check-cast v5, Landroid/view/inputmethod/InputMethodManager;

    invoke-static/range {v17 .. v17}, LX/8uo;->a(LX/0QB;)LX/8uo;

    move-result-object v6

    check-cast v6, LX/8uo;

    invoke-static/range {v17 .. v17}, LX/8RL;->a(LX/0QB;)LX/8RL;

    move-result-object v7

    check-cast v7, LX/8RL;

    invoke-static/range {v17 .. v17}, LX/BXZ;->b(LX/0QB;)LX/BXZ;

    move-result-object v8

    check-cast v8, LX/BXZ;

    invoke-static/range {v17 .. v17}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v9

    check-cast v9, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static/range {v17 .. v17}, LX/BWo;->a(LX/0QB;)LX/BWo;

    move-result-object v10

    check-cast v10, LX/BWl;

    invoke-static/range {v17 .. v17}, LX/0cd;->a(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object v11

    check-cast v11, Landroid/content/ContentResolver;

    invoke-static/range {v17 .. v17}, LX/BWn;->b(LX/0QB;)LX/BWn;

    move-result-object v12

    check-cast v12, LX/BWn;

    invoke-static/range {v17 .. v17}, LX/2RQ;->a(LX/0QB;)LX/2RQ;

    move-result-object v13

    check-cast v13, LX/2RQ;

    invoke-static/range {v17 .. v17}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v14

    check-cast v14, LX/0kL;

    invoke-static/range {v17 .. v17}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v15

    check-cast v15, Landroid/content/res/Resources;

    invoke-static/range {v17 .. v17}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v16

    check-cast v16, Ljava/lang/Boolean;

    invoke-static/range {v17 .. v17}, LX/6NS;->a(LX/0QB;)LX/3Oq;

    move-result-object v17

    check-cast v17, LX/3Oq;

    invoke-static/range {v0 .. v17}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;LX/0TD;LX/1Ck;LX/3LP;LX/0Sy;Landroid/view/inputmethod/InputMethodManager;LX/8uo;LX/8RL;LX/BXZ;Lcom/facebook/contacts/background/AddressBookPeriodicRunner;LX/BWl;Landroid/content/ContentResolver;LX/BWn;LX/2RQ;LX/0kL;Landroid/content/res/Resources;Ljava/lang/Boolean;LX/3Oq;)V

    return-void
.end method

.method public static a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Ljava/util/List;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1696539
    iget-object v0, p0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1696540
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v1

    .line 1696541
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1696542
    invoke-virtual {v0}, LX/8QL;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 1696543
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1696544
    const/4 v0, 0x1

    .line 1696545
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            "Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1696546
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {p1, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1696547
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v0, :cond_3

    .line 1696548
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->C:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    invoke-virtual {v0, p1, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V

    .line 1696549
    :cond_0
    :goto_0
    invoke-interface {p3, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1696550
    invoke-interface {p3, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1696551
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D()V

    .line 1696552
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-nez v0, :cond_2

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1696553
    invoke-static {p0, v2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Z)V

    .line 1696554
    :cond_2
    return-void

    .line 1696555
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    .line 1696556
    iget-object v1, p1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v1, v1

    .line 1696557
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1696558
    iget-object v4, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v4, v4

    .line 1696559
    invoke-virtual {v4}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1696560
    :goto_1
    move-object v0, v0

    .line 1696561
    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b$redex0(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1696562
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->g:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1696563
    return-void
.end method

.method public static c(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1696564
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->H:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1696565
    if-nez p1, :cond_0

    .line 1696566
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setEnabled(Z)V

    .line 1696567
    :cond_0
    if-eqz p1, :cond_2

    .line 1696568
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->H:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1696569
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    .line 1696570
    :cond_1
    :goto_0
    return-void

    .line 1696571
    :cond_2
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->H:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1696572
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public static d(Ljava/lang/String;)LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1696573
    if-nez p0, :cond_0

    .line 1696574
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1696575
    :goto_0
    return-object v0

    .line 1696576
    :cond_0
    const-string v0, "-1"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1696577
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1696578
    goto :goto_0

    .line 1696579
    :cond_1
    const/16 v0, 0x2c

    invoke-static {v0}, LX/2Cb;->on(C)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0}, LX/2Cb;->trimResults()LX/2Cb;

    move-result-object v0

    invoke-virtual {v0}, LX/2Cb;->omitEmptyStrings()LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    .line 1696580
    invoke-static {v0}, LX/0Rf;->copyOf(Ljava/lang/Iterable;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final A()V
    .locals 4

    .prologue
    .line 1696581
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1696582
    new-instance v0, LX/BXT;

    invoke-direct {v0, p0}, LX/BXT;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    .line 1696583
    new-instance v1, LX/BXE;

    invoke-direct {v1, p0}, LX/BXE;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    .line 1696584
    new-instance v2, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v3, 0x7f080bdd

    invoke-virtual {v2, v3}, LX/0ju;->a(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f080bde

    invoke-virtual {v2, v3}, LX/0ju;->b(I)LX/0ju;

    move-result-object v2

    const v3, 0x7f080bdf

    invoke-virtual {v2, v3, v1}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080be0

    invoke-virtual {v1, v2, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    .line 1696585
    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1696586
    :goto_0
    return-void

    .line 1696587
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->B()V

    .line 1696588
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 1696589
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1696590
    goto :goto_0
.end method

.method public B()V
    .locals 0

    .prologue
    .line 1696591
    return-void
.end method

.method public D()V
    .locals 2

    .prologue
    .line 1696592
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    if-eqz v0, :cond_0

    .line 1696593
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    const v1, 0x6d0f3ae1

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1696594
    :cond_0
    return-void
.end method

.method public E()V
    .locals 4

    .prologue
    .line 1696596
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->d:LX/1Ck;

    sget-object v1, LX/BXV;->FETCH_INIT_IDS:LX/BXV;

    new-instance v2, LX/BXI;

    invoke-direct {v2, p0}, LX/BXI;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    new-instance v3, LX/BXJ;

    invoke-direct {v3, p0}, LX/BXJ;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1696597
    return-void
.end method

.method public F()Z
    .locals 1

    .prologue
    .line 1696595
    const/4 v0, 0x0

    return v0
.end method

.method public G()V
    .locals 1

    .prologue
    .line 1696713
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1696714
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1696715
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->x()V

    .line 1696716
    :cond_0
    return-void
.end method

.method public H()Z
    .locals 1

    .prologue
    .line 1696712
    const/4 v0, 0x1

    return v0
.end method

.method public a(Ljava/lang/String;Ljava/util/Map;)LX/44w;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;)",
            "LX/44w",
            "<",
            "LX/0Rf",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;",
            "LX/621",
            "<+",
            "LX/8QL;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1696684
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1696685
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 1696686
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v4

    .line 1696687
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1696688
    :cond_0
    const/4 v0, 0x0

    .line 1696689
    :goto_0
    return-object v0

    .line 1696690
    :cond_1
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    .line 1696691
    invoke-static {v1}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v6

    .line 1696692
    invoke-virtual {p0, v1, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/user/model/User;Ljava/lang/String;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-result-object v1

    .line 1696693
    iget-object v7, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->q:LX/0Rf;

    .line 1696694
    iget-object v8, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v8, v8

    .line 1696695
    invoke-virtual {v7, v8}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 1696696
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1696697
    iget-object v7, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    .line 1696698
    iget-object v8, v6, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v6, v8

    .line 1696699
    invoke-virtual {v7, v6}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1696700
    invoke-virtual {v4, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1696701
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1696702
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1696703
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1696704
    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    .line 1696705
    :cond_4
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1696706
    new-instance v1, LX/623;

    invoke-virtual {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Ljava/lang/String;)I

    move-result v2

    .line 1696707
    if-lez v2, :cond_5

    .line 1696708
    iget-object v3, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->z:Landroid/content/res/Resources;

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1696709
    :goto_2
    move-object v2, v3

    .line 1696710
    invoke-virtual {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c(Ljava/lang/String;)Z

    move-result v3

    invoke-direct {v1, v2, v0, v3}, LX/623;-><init>(Ljava/lang/String;LX/0Px;Z)V

    .line 1696711
    new-instance v0, LX/44w;

    invoke-virtual {v4}, LX/0cA;->b()LX/0Rf;

    move-result-object v2

    invoke-direct {v0, v2, v1}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0

    :cond_5
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public a(Lcom/facebook/user/model/User;Ljava/lang/String;)Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;
    .locals 2

    .prologue
    .line 1696675
    invoke-static {p1}, LX/8uo;->a(Lcom/facebook/user/model/User;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 1696676
    new-instance v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v1, v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 1696677
    iget-object v0, v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1696678
    if-eqz v0, :cond_0

    .line 1696679
    iget-object v0, v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, v0

    .line 1696680
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 1696681
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->i:Z

    .line 1696682
    return-object v1

    .line 1696683
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 1696672
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v0, p1}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1696673
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    .line 1696674
    return-void
.end method

.method public final a(LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1696667
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->J:Landroid/widget/TextView;

    const v1, 0x7f080bd7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1696668
    invoke-virtual {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/util/Map;)V

    .line 1696669
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1696670
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->G()V

    .line 1696671
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1696652
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1696653
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1696654
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->k:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 1696655
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1696656
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1696657
    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Landroid/os/Bundle;)V

    .line 1696658
    if-nez p1, :cond_1

    .line 1696659
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->l:Landroid/content/ContentResolver;

    sget-object v1, LX/0P0;->g:Landroid/net/Uri;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->u:LX/BXU;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1696660
    return-void

    .line 1696661
    :cond_1
    const-string v0, "savedSelectedIds"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1696662
    if-eqz v0, :cond_0

    .line 1696663
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v1

    .line 1696664
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    invoke-virtual {v1, v2}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 1696665
    invoke-virtual {v1, v0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 1696666
    invoke-virtual {v1}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V
    .locals 3

    .prologue
    .line 1696633
    invoke-virtual {p1}, LX/8QK;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1696634
    :cond_0
    :goto_0
    return-void

    .line 1696635
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1696636
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    .line 1696637
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t()I

    move-result v2

    if-lt v1, v2, :cond_4

    .line 1696638
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->x:LX/0kL;

    new-instance v2, LX/27k;

    const p1, 0x7f080be4

    invoke-direct {v2, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v1, v2}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1696639
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    const v1, -0x265e2493

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1696640
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-nez v0, :cond_0

    .line 1696641
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    goto :goto_0

    .line 1696642
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {p0, p1, p2, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;Ljava/util/List;)V

    goto :goto_1

    .line 1696643
    :cond_4
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {p1, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1696644
    iget-boolean v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v1, :cond_6

    .line 1696645
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->C:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    invoke-virtual {v1, p1}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 1696646
    :cond_5
    :goto_2
    invoke-virtual {p2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->clearComposingText()V

    .line 1696647
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1696648
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D()V

    .line 1696649
    iget-boolean v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-nez v1, :cond_2

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1696650
    const/4 v1, 0x0

    invoke-static {p0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Z)V

    goto :goto_1

    .line 1696651
    :cond_6
    invoke-virtual {p2, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_2
.end method

.method public final a(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1696612
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1696613
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v3

    .line 1696614
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->u()LX/0Px;

    move-result-object v4

    .line 1696615
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1696616
    invoke-virtual {p0, v0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/lang/String;Ljava/util/Map;)LX/44w;

    move-result-object v6

    .line 1696617
    if-eqz v6, :cond_1

    .line 1696618
    iget-object v0, v6, LX/44w;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1696619
    iget-object v0, v6, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {v3, v0}, LX/0cA;->b(Ljava/lang/Iterable;)LX/0cA;

    .line 1696620
    :cond_0
    iget-object v0, v6, LX/44w;->b:Ljava/lang/Object;

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1696621
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1696622
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1696623
    invoke-virtual {v3}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    .line 1696624
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b(Z)V

    .line 1696625
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v2, v0}, LX/8tB;->a(Ljava/util/List;)V

    .line 1696626
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    const v2, 0x68df22e8

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1696627
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-nez v0, :cond_3

    .line 1696628
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1696629
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->H()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1696630
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b$redex0(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Landroid/view/View;)V

    .line 1696631
    :cond_4
    invoke-virtual {p0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/util/Set;)V

    .line 1696632
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1696605
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1696606
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-static {v0, v2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1696607
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0, v0, v2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V

    goto :goto_0

    .line 1696608
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    const v1, -0x4f732393

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1696609
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-nez v0, :cond_2

    .line 1696610
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->d()V

    .line 1696611
    :cond_2
    return-void
.end method

.method public final a(LX/8QL;)Z
    .locals 1

    .prologue
    .line 1696598
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1696482
    const/4 v0, 0x1

    return v0
.end method

.method public b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1696599
    sget-object v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1696600
    const v0, 0x7f080bd8

    .line 1696601
    :goto_0
    return v0

    .line 1696602
    :cond_0
    sget-object v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1696603
    const v0, 0x7f080bd9

    goto :goto_0

    .line 1696604
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()LX/8tB;
    .locals 1

    .prologue
    .line 1696359
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    return-object v0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1696368
    if-eqz p1, :cond_0

    .line 1696369
    const-string v0, "friendsSelectorSelected"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1696370
    const-string v1, "friendsSelectorExcluded"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1696371
    invoke-static {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->d(Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->p:LX/0Rf;

    .line 1696372
    invoke-static {v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->d(Ljava/lang/String;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->q:LX/0Rf;

    .line 1696373
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1696360
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_0

    .line 1696361
    :goto_0
    return-void

    .line 1696362
    :cond_0
    if-eqz p1, :cond_1

    .line 1696363
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->J:Landroid/widget/TextView;

    const v1, 0x7f080bd7

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1696364
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1696365
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->J:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1696366
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1696367
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->J:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Z
    .locals 1

    .prologue
    .line 1696374
    invoke-static {p0, p2}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    .line 1696375
    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()LX/8RE;
    .locals 2

    .prologue
    .line 1696376
    new-instance v0, LX/8RF;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/8RF;-><init>(Z)V

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1696377
    const/4 v0, 0x0

    return v0
.end method

.method public k()Z
    .locals 1

    .prologue
    .line 1696378
    const/4 v0, 0x0

    return v0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 1696379
    return-void
.end method

.method public m()V
    .locals 0

    .prologue
    .line 1696380
    return-void
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 1696381
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1696382
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 1696383
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1696384
    iget-object p0, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->g:Lcom/facebook/user/model/UserKey;

    move-object v0, p0

    .line 1696385
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 1696386
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x270506ca

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1696387
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1696388
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v0

    .line 1696389
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, v2}, LX/BWl;->d(Landroid/view/View;)Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1696390
    const v0, 0x7f0d0f24

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->C:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    .line 1696391
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v3, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getInputType()I

    move-result v3

    const/high16 v4, 0x10000

    or-int/2addr v3, v4

    or-int/lit8 v3, v3, 0x60

    invoke-virtual {v0, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setInputType(I)V

    .line 1696392
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, v2}, LX/BWl;->c(Landroid/view/View;)Lcom/facebook/widget/listview/BetterListView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    .line 1696393
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, v2}, LX/BWl;->e(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->I:Landroid/view/View;

    .line 1696394
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, v2}, LX/BWl;->f(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->H:Landroid/view/View;

    .line 1696395
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, v2}, LX/BWl;->b(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    .line 1696396
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, v2}, LX/BWl;->a(Landroid/view/View;)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->J:Landroid/widget/TextView;

    .line 1696397
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v0, v2}, LX/BWl;->g(Landroid/view/View;)Landroid/view/ViewStub;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->K:Landroid/view/ViewStub;

    .line 1696398
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v2, LX/BXN;

    invoke-direct {v2, p0}, LX/BXN;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1696399
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v2, LX/BXO;

    invoke-direct {v2, p0}, LX/BXO;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 1696400
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m()V

    .line 1696401
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1696402
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v0, :cond_0

    .line 1696403
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Z)V

    .line 1696404
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->I:Landroid/view/View;

    new-instance v2, LX/BXQ;

    invoke-direct {v2, p0}, LX/BXQ;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1696405
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->w:LX/BWm;

    .line 1696406
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b()LX/8tB;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    .line 1696407
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    .line 1696408
    iget-object v3, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->i:LX/8RL;

    move-object v3, v3

    .line 1696409
    invoke-virtual {v2, v3, v0}, LX/8tB;->a(LX/8RK;LX/8RE;)V

    .line 1696410
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r:Ljava/util/List;

    .line 1696411
    iput-object v2, v0, LX/8tB;->i:Ljava/util/List;

    .line 1696412
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    new-instance v2, LX/623;

    invoke-direct {v2}, LX/623;-><init>()V

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8tB;->a(Ljava/util/List;)V

    .line 1696413
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1696414
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->O:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1696415
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    new-instance v2, LX/BXR;

    invoke-direct {v2, p0}, LX/BXR;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    invoke-virtual {v0, v2}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1696416
    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v2, v0}, Lcom/facebook/widget/listview/BetterListView;->setStickyHeaderEnabled(Z)V

    .line 1696417
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-nez v0, :cond_3

    .line 1696418
    :goto_3
    invoke-static {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->Q(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    .line 1696419
    const/16 v0, 0x2b

    const v2, -0x4784d0a8

    invoke-static {v5, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1696420
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Z)V

    goto :goto_0

    .line 1696421
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c()LX/8RE;

    move-result-object v0

    goto :goto_1

    .line 1696422
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->k()Z

    move-result v0

    goto :goto_2

    .line 1696423
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->C:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    iget-boolean v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->M:Z

    invoke-virtual {v0, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->b(Z)V

    .line 1696424
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->C:Lcom/facebook/widget/friendselector/FriendSelectorResultBar;

    new-instance v2, LX/BXS;

    invoke-direct {v2, p0}, LX/BXS;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    .line 1696425
    iput-object v2, v0, Lcom/facebook/widget/friendselector/FriendSelectorResultBar;->h:LX/BWy;

    .line 1696426
    goto :goto_3
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x10230255

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696427
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    invoke-interface {v1, p2, p1}, LX/BWl;->a(Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x51a47b3c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1b851cf5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696428
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1696429
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->l:Landroid/content/ContentResolver;

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->u:LX/BXU;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1696430
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1696431
    const/16 v1, 0x2b

    const v2, -0x2679a0db

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v1, -0x7b06d376

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696432
    iput-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->D:Lcom/facebook/widget/listview/BetterListView;

    .line 1696433
    iput-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->E:Landroid/widget/ListView;

    .line 1696434
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->F:LX/2EJ;

    if-eqz v1, :cond_0

    .line 1696435
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->F:LX/2EJ;

    invoke-virtual {v1}, LX/2EJ;->dismiss()V

    .line 1696436
    iput-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->F:LX/2EJ;

    .line 1696437
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1696438
    const/16 v1, 0x2b

    const v2, -0x18728bfd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x747260ee

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696439
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1696440
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0, v1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b$redex0(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;Landroid/view/View;)V

    .line 1696441
    const/16 v1, 0x2b

    const v2, 0x4a69f28c    # 3832995.0f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1696442
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1696443
    if-lez v0, :cond_0

    .line 1696444
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1696445
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 1696446
    const-string v1, "savedSelectedIds"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1696447
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1696448
    return-void
.end method

.method public p()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1696449
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->c:LX/0TD;

    new-instance v1, LX/BXP;

    invoke-direct {v1, p0}, LX/BXP;-><init>(Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final q()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1696450
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o:LX/2RQ;

    invoke-virtual {v0}, LX/2RQ;->a()LX/2RR;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->B:LX/3Oq;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1696451
    iput-object v1, v0, LX/2RR;->c:Ljava/util/Collection;

    .line 1696452
    move-object v0, v0

    .line 1696453
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s()Z

    move-result v1

    .line 1696454
    iput-boolean v1, v0, LX/2RR;->o:Z

    .line 1696455
    move-object v0, v0

    .line 1696456
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->r()LX/2RS;

    move-result-object v1

    .line 1696457
    iput-object v1, v0, LX/2RR;->n:LX/2RS;

    .line 1696458
    move-object v0, v0

    .line 1696459
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->e:LX/3LP;

    invoke-virtual {v1, v0}, LX/3LP;->a(LX/2RR;)LX/3On;

    move-result-object v1

    .line 1696460
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 1696461
    :goto_0
    :try_start_0
    invoke-interface {v1}, LX/3On;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1696462
    invoke-interface {v1}, LX/3On;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1696463
    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/3On;->close()V

    throw v0

    .line 1696464
    :cond_0
    :try_start_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 1696465
    invoke-interface {v1}, LX/3On;->close()V

    return-object v0
.end method

.method public r()LX/2RS;
    .locals 1

    .prologue
    .line 1696466
    sget-object v0, LX/2RS;->NAME:LX/2RS;

    return-object v0
.end method

.method public s()Z
    .locals 1

    .prologue
    .line 1696467
    const/4 v0, 0x0

    return v0
.end method

.method public t()I
    .locals 1

    .prologue
    .line 1696468
    const/16 v0, 0x32

    return v0
.end method

.method public u()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1696469
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->v()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1696470
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->A:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->b:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public w()V
    .locals 0

    .prologue
    .line 1696471
    return-void
.end method

.method public x()V
    .locals 3

    .prologue
    .line 1696472
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->t:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1696473
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v0}, LX/3Tf;->a()LX/333;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->s:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->N:LX/3NY;

    invoke-interface {v0, v1, v2}, LX/333;->a(Ljava/lang/CharSequence;LX/3NY;)V

    .line 1696474
    return-void
.end method

.method public y()V
    .locals 0

    .prologue
    .line 1696475
    return-void
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 1696476
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1696477
    :goto_0
    return-void

    .line 1696478
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->L:Z

    if-eqz v0, :cond_1

    .line 1696479
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 1696480
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 1696481
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
