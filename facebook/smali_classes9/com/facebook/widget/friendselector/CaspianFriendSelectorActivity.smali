.class public Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/7g9;


# instance fields
.field public p:LX/0jo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0gc;

.field public s:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

.field private t:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1792246
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1792247
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->t:Ljava/lang/Boolean;

    return-void
.end method

.method private static a(Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;LX/0jo;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1792245
    iput-object p1, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->p:LX/0jo;

    iput-object p2, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->q:Ljava/lang/Boolean;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;

    invoke-static {v1}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v0

    check-cast v0, LX/0jo;

    invoke-static {v1}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-static {p0, v0, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->a(Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;LX/0jo;Ljava/lang/Boolean;)V

    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 1792192
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1792193
    const-string v1, "target_fragment"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1792194
    if-ne v1, v2, :cond_0

    .line 1792195
    :goto_0
    return-void

    .line 1792196
    :cond_0
    const-string v2, "non_modal_display"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->t:Ljava/lang/Boolean;

    .line 1792197
    iget-object v2, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->t:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1792198
    const v2, 0x7f0400d9

    const v3, 0x7f040030

    invoke-virtual {p0, v2, v3}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->overridePendingTransition(II)V

    .line 1792199
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->m()V

    .line 1792200
    :cond_1
    iget-object v2, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->p:LX/0jo;

    invoke-interface {v2, v1}, LX/0jo;->a(I)LX/0jq;

    move-result-object v1

    .line 1792201
    invoke-interface {v1, v0}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 1792202
    if-eqz v1, :cond_2

    instance-of v0, v1, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    if-nez v0, :cond_3

    .line 1792203
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->finish()V

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 1792204
    check-cast v0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->s:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 1792205
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->s:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    .line 1792206
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v2

    .line 1792207
    if-nez v0, :cond_4

    .line 1792208
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1792209
    :cond_4
    const-string v2, "is_show_caspian_style"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1792210
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1792211
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->r:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v2, 0x7f0d002f

    invoke-virtual {v0, v2, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1792212
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->r:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1792236
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1792237
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1792238
    if-nez v1, :cond_0

    .line 1792239
    iget-object v1, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->q:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f080bdc

    .line 1792240
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1792241
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1792242
    new-instance v1, LX/BWi;

    invoke-direct {v1, p0}, LX/BWi;-><init>(Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1792243
    return-void

    .line 1792244
    :cond_1
    const v1, 0x7f080bda

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1792233
    const v0, 0x7f0d09a9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1792234
    const v1, 0x7f020756

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1792235
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1792225
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1792226
    invoke-static {p0, p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1792227
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->r:LX/0gc;

    .line 1792228
    const v0, 0x7f0400c8

    const v1, 0x7f040030

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->overridePendingTransition(II)V

    .line 1792229
    const v0, 0x7f0306f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->setContentView(I)V

    .line 1792230
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->l()V

    .line 1792231
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->b()V

    .line 1792232
    return-void
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 1792220
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1792221
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->t:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1792222
    const v0, 0x7f0400b7

    const v1, 0x7f0400ed

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->overridePendingTransition(II)V

    .line 1792223
    :goto_0
    return-void

    .line 1792224
    :cond_0
    const v0, 0x7f0400b6

    const v1, 0x7f0400ca

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method public final mr_()V
    .locals 0

    .prologue
    .line 1792218
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->finish()V

    .line 1792219
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1792216
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->s:Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->A()V

    .line 1792217
    return-void
.end method

.method public final startActivity(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1792213
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->startActivity(Landroid/content/Intent;)V

    .line 1792214
    const v0, 0x7f0400d9

    const v1, 0x7f040030

    invoke-virtual {p0, v0, v1}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorActivity;->overridePendingTransition(II)V

    .line 1792215
    return-void
.end method
