.class public Lcom/facebook/widget/friendselector/SelectedFriendItemView;
.super LX/4oR;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static final e:LX/0wT;


# instance fields
.field public a:LX/0wc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/0wd;

.field public g:Z

.field private h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private i:Lcom/facebook/fbui/glyph/GlyphView;

.field private j:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1793288
    const-class v0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->d:Lcom/facebook/common/callercontext/CallerContext;

    .line 1793289
    const-wide/high16 v0, 0x4054000000000000L    # 80.0

    const-wide/high16 v2, 0x4022000000000000L    # 9.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->e:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1793285
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/4oR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1793286
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->e()V

    .line 1793287
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1793282
    invoke-direct {p0, p1, p2}, LX/4oR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1793283
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->e()V

    .line 1793284
    return-void
.end method

.method private static a(Lcom/facebook/widget/friendselector/SelectedFriendItemView;LX/0wc;LX/0wW;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 1793281
    iput-object p1, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->a:LX/0wc;

    iput-object p2, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->b:LX/0wW;

    iput-object p3, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->c:Ljava/lang/Boolean;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-static {v2}, LX/0wc;->a(LX/0QB;)LX/0wc;

    move-result-object v0

    check-cast v0, LX/0wc;

    invoke-static {v2}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v1

    check-cast v1, LX/0wW;

    invoke-static {v2}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->a(Lcom/facebook/widget/friendselector/SelectedFriendItemView;LX/0wc;LX/0wW;Ljava/lang/Boolean;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/widget/friendselector/SelectedFriendItemView;F)V
    .locals 0

    .prologue
    .line 1793277
    invoke-virtual {p0, p1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setScaleX(F)V

    .line 1793278
    invoke-virtual {p0, p1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setScaleY(F)V

    .line 1793279
    invoke-virtual {p0, p1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setAlpha(F)V

    .line 1793280
    return-void
.end method

.method private e()V
    .locals 15

    .prologue
    .line 1793231
    const-class v0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-static {v0, p0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1793232
    const v0, 0x7f0312f3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1793233
    const/4 v0, 0x1

    .line 1793234
    iget-object v2, p0, LX/4oR;->a:LX/4oS;

    .line 1793235
    iget-object v3, v2, LX/4oS;->b:LX/4oN;

    move-object v2, v3

    .line 1793236
    new-instance v3, LX/4oM;

    invoke-direct {v3}, LX/4oM;-><init>()V

    iget-boolean v4, v2, LX/4oN;->a:Z

    .line 1793237
    iput-boolean v4, v3, LX/4oM;->a:Z

    .line 1793238
    move-object v3, v3

    .line 1793239
    iget-boolean v4, v2, LX/4oN;->b:Z

    .line 1793240
    iput-boolean v4, v3, LX/4oM;->b:Z

    .line 1793241
    move-object v3, v3

    .line 1793242
    iget-boolean v4, v2, LX/4oN;->c:Z

    .line 1793243
    iput-boolean v4, v3, LX/4oM;->c:Z

    .line 1793244
    move-object v3, v3

    .line 1793245
    iget-boolean v4, v2, LX/4oN;->d:Z

    .line 1793246
    iput-boolean v4, v3, LX/4oM;->d:Z

    .line 1793247
    move-object v3, v3

    .line 1793248
    iget-boolean v4, v2, LX/4oN;->e:Z

    .line 1793249
    iput-boolean v4, v3, LX/4oM;->e:Z

    .line 1793250
    move-object v3, v3

    .line 1793251
    iget v4, v2, LX/4oN;->f:F

    .line 1793252
    iput v4, v3, LX/4oM;->f:F

    .line 1793253
    move-object v3, v3

    .line 1793254
    iget v4, v2, LX/4oN;->g:I

    .line 1793255
    iput v4, v3, LX/4oM;->g:I

    .line 1793256
    move-object v3, v3

    .line 1793257
    iget v4, v2, LX/4oN;->h:F

    .line 1793258
    iput v4, v3, LX/4oM;->h:F

    .line 1793259
    move-object v3, v3

    .line 1793260
    iget v4, v2, LX/4oN;->i:I

    .line 1793261
    iput v4, v3, LX/4oM;->i:I

    .line 1793262
    move-object v3, v3

    .line 1793263
    move-object v2, v3

    .line 1793264
    iput-boolean v0, v2, LX/4oM;->a:Z

    .line 1793265
    move-object v2, v2

    .line 1793266
    new-instance v4, LX/4oN;

    iget-boolean v5, v2, LX/4oM;->a:Z

    iget-boolean v6, v2, LX/4oM;->b:Z

    iget-boolean v7, v2, LX/4oM;->c:Z

    iget-boolean v8, v2, LX/4oM;->d:Z

    iget-boolean v9, v2, LX/4oM;->e:Z

    iget v10, v2, LX/4oM;->f:F

    iget v11, v2, LX/4oM;->g:I

    iget v12, v2, LX/4oM;->h:F

    iget v13, v2, LX/4oM;->i:I

    const/4 v14, 0x0

    invoke-direct/range {v4 .. v14}, LX/4oN;-><init>(ZZZZZFIFIB)V

    move-object v2, v4

    .line 1793267
    iget-object v3, p0, LX/4oR;->a:LX/4oS;

    invoke-virtual {v3}, LX/4oS;->b()V

    .line 1793268
    new-instance v3, LX/4oS;

    invoke-direct {v3, p0, v2}, LX/4oS;-><init>(Landroid/view/View;LX/4oN;)V

    iput-object v3, p0, LX/4oR;->a:LX/4oS;

    .line 1793269
    invoke-virtual {p0}, LX/4oR;->invalidate()V

    .line 1793270
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->f:LX/0wd;

    .line 1793271
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->f:LX/0wd;

    sget-object v1, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->e:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 1793272
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->f:LX/0wd;

    new-instance v1, LX/BXY;

    invoke-direct {v1, p0}, LX/BXY;-><init>(Lcom/facebook/widget/friendselector/SelectedFriendItemView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1793273
    const v0, 0x7f0d2c16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1793274
    const v0, 0x7f0d0281

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1793275
    const v0, 0x7f0d2c17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->j:Landroid/widget/TextView;

    .line 1793276
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 1793226
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->f:LX/0wd;

    const/4 v1, 0x0

    .line 1793227
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1793228
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->a$redex0(Lcom/facebook/widget/friendselector/SelectedFriendItemView;F)V

    .line 1793229
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->f:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1793230
    return-void
.end method

.method public final a(I)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1793195
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->j:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "+"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1793196
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080bea

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1793197
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1793198
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1793199
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1793200
    return-void

    .line 1793201
    :cond_0
    const v0, 0x7f080be9

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1793222
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->j:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1793223
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1793224
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1793225
    return-void
.end method

.method public setToken(LX/8QL;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 1793202
    invoke-virtual {p1}, LX/8QL;->e()I

    move-result v0

    if-lez v0, :cond_1

    .line 1793203
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, LX/8QL;->e()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1793204
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1793205
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1793206
    :goto_0
    invoke-virtual {p1}, LX/8QL;->f()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1793207
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p1}, LX/8QL;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1793208
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->a$redex0(Lcom/facebook/widget/friendselector/SelectedFriendItemView;F)V

    .line 1793209
    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1793210
    return-void

    .line 1793211
    :cond_1
    invoke-virtual {p1}, LX/8QL;->g()I

    move-result v0

    if-gtz v0, :cond_2

    invoke-virtual {p1}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1793212
    :cond_2
    invoke-virtual {p1}, LX/8QL;->g()I

    move-result v0

    if-lez v0, :cond_3

    .line 1793213
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, LX/8QL;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1793214
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1793215
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v4}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0

    .line 1793216
    :cond_3
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1793217
    const/4 v3, 0x0

    move-object v3, v3

    .line 1793218
    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1793219
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1793220
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->h:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1793221
    iget-object v0, p0, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->i:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method
