.class public Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/widget/TextView;

.field private final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/fbui/glyph/GlyphView;

.field private final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final g:Landroid/widget/CheckBox;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1792318
    const-class v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1792309
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1792310
    const v0, 0x7f0306f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1792311
    const v0, 0x7f0d12a0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getOptionalView(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->b:LX/0am;

    .line 1792312
    const v0, 0x7f0d0beb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->c:Landroid/widget/TextView;

    .line 1792313
    const v0, 0x7f0d0945

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getOptionalView(I)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    .line 1792314
    const v0, 0x7f0d0281

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1792315
    const v0, 0x7f0d0e17

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1792316
    const v0, 0x7f0d0e16

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->g:Landroid/widget/CheckBox;

    .line 1792317
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1792248
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 1792249
    const v0, 0x7f0d1296

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1792250
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1792251
    const v0, 0x7f0d1297

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1792252
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 1792253
    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v1, v3, v4, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 1792254
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_0

    .line 1792255
    const v3, 0x7f0b0060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMarginStart(I)V

    .line 1792256
    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1792257
    return-void
.end method

.method public final a(LX/8QL;Z)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 1792274
    invoke-virtual {p1}, LX/8QK;->a()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->setEnabled(Z)V

    .line 1792275
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1792276
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-instance v2, Ljava/lang/String;

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1792277
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->c:Landroid/widget/TextView;

    invoke-virtual {p1}, LX/8QK;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1792278
    invoke-virtual {p1}, LX/8QL;->j()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1792279
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 1792280
    iget-object v2, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->g:Landroid/widget/CheckBox;

    if-nez p2, :cond_1

    invoke-virtual {p1}, LX/8QK;->a()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1792281
    :goto_1
    invoke-virtual {p1}, LX/8QL;->e()I

    move-result v0

    if-lez v0, :cond_6

    .line 1792282
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, LX/8QL;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1792283
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1792284
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1792285
    :goto_2
    invoke-virtual {p1}, LX/8QL;->f()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1792286
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, LX/8QL;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1792287
    :cond_2
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v2, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v0, v2, :cond_a

    .line 1792288
    const-string v0, "TAG_EXPANSION_VIEW"

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->setTag(Ljava/lang/Object;)V

    .line 1792289
    :goto_3
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, LX/8QL;->l()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, LX/8QL;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1792290
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1792291
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p1}, LX/8QL;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1792292
    :cond_3
    :goto_4
    return-void

    :cond_4
    move v0, v1

    .line 1792293
    goto/16 :goto_0

    .line 1792294
    :cond_5
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto/16 :goto_1

    .line 1792295
    :cond_6
    invoke-virtual {p1}, LX/8QL;->g()I

    move-result v0

    if-gtz v0, :cond_7

    invoke-virtual {p1}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    .line 1792296
    :cond_7
    invoke-virtual {p1}, LX/8QL;->g()I

    move-result v0

    if-lez v0, :cond_8

    .line 1792297
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, LX/8QL;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1792298
    :goto_5
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1792299
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1792300
    :cond_8
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1792301
    const/4 v4, 0x0

    move-object v4, v4

    .line 1792302
    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_5

    .line 1792303
    :cond_9
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p1}, LX/8QL;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1792304
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1792305
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto/16 :goto_2

    .line 1792306
    :cond_a
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_3

    .line 1792307
    :cond_b
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1792308
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4
.end method

.method public setAsHeaderItem(Z)V
    .locals 2

    .prologue
    .line 1792270
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1792271
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1792272
    :cond_0
    return-void

    .line 1792273
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 1792258
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setEnabled(Z)V

    .line 1792259
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->g:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 1792260
    if-eqz p1, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    move v1, v0

    .line 1792261
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setAlpha(F)V

    .line 1792262
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setAlpha(F)V

    .line 1792263
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1792264
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1792265
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1792266
    iget-object v0, p0, Lcom/facebook/widget/friendselector/CaspianFriendSelectorItemRow;->d:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p1, :cond_2

    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1792267
    :cond_0
    return-void

    .line 1792268
    :cond_1
    const v0, 0x3e851eb8    # 0.26f

    move v1, v0

    goto :goto_0

    .line 1792269
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
