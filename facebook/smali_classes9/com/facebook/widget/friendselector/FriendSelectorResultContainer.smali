.class public Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/BXC;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/BXC;

.field public c:LX/BWp;

.field public d:LX/BWr;

.field private e:Landroid/os/Handler;

.field private f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/os/Message;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:I

.field private i:I

.field public j:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1792692
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1792693
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    .line 1792694
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    .line 1792695
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b()V

    .line 1792696
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1792697
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1792698
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    .line 1792699
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    .line 1792700
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b()V

    .line 1792701
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1792702
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1792703
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    .line 1792704
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    .line 1792705
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b()V

    .line 1792706
    return-void
.end method

.method private a(LX/BXC;IIZ)Landroid/animation/Animator;
    .locals 9

    .prologue
    .line 1792707
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1792708
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1792709
    add-int/lit8 v1, p2, -0x1

    .line 1792710
    if-ne v1, p3, :cond_1

    const/4 v0, 0x1

    move v2, v0

    .line 1792711
    :goto_0
    if-ltz v1, :cond_2

    .line 1792712
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792713
    iget-object v5, p1, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    iget-object v6, v0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v5, v6}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->indexOfChild(Landroid/view/View;)I

    move-result v5

    .line 1792714
    iget-object v6, p1, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    iget-object v7, p1, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v6, v7}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->removeView(Landroid/view/View;)V

    .line 1792715
    iget-object v6, p1, LX/BXC;->b:Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;

    iget-object v7, p1, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v6, v7, v5}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->addView(Landroid/view/View;I)V

    .line 1792716
    iget-object v5, p1, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    sget v6, LX/BXC;->a:I

    int-to-float v6, v6

    invoke-virtual {v5, v6}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setTranslationX(F)V

    .line 1792717
    new-instance v5, Landroid/animation/AnimatorSet;

    invoke-direct {v5}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1792718
    invoke-virtual {p1}, LX/BXC;->q()Landroid/animation/Animator;

    move-result-object v6

    .line 1792719
    invoke-static {v0}, LX/BXC;->t(LX/BXC;)Landroid/animation/Animator;

    move-result-object v7

    .line 1792720
    const/4 v8, 0x2

    new-array v8, v8, [Landroid/animation/Animator;

    const/4 p2, 0x0

    aput-object v6, v8, p2

    const/4 v6, 0x1

    aput-object v7, v8, v6

    invoke-virtual {v5, v8}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1792721
    new-instance v6, LX/BXB;

    invoke-direct {v6, p1, v0}, LX/BXB;-><init>(LX/BXC;LX/BXC;)V

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792722
    move-object v5, v5

    .line 1792723
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792724
    if-eqz p4, :cond_0

    if-eqz v2, :cond_0

    .line 1792725
    iget-object v5, v0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-static {v5}, LX/BXX;->a(Lcom/facebook/widget/friendselector/SelectedFriendItemView;)Landroid/animation/Animator;

    move-result-object v5

    move-object v0, v5

    .line 1792726
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792727
    :cond_0
    add-int/lit8 v0, v1, -0x1

    :goto_1
    move v1, v0

    .line 1792728
    :goto_2
    if-le v1, p3, :cond_3

    .line 1792729
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792730
    invoke-virtual {v0}, LX/BXC;->p()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792731
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 1792732
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    .line 1792733
    :cond_2
    invoke-virtual {p1}, LX/BXC;->q()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_1

    .line 1792734
    :cond_3
    if-eqz p4, :cond_4

    if-nez v2, :cond_4

    .line 1792735
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792736
    iget-object v1, v0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    sget v2, LX/BXC;->a:I

    invoke-static {v1, v2}, LX/BXX;->d(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v1

    .line 1792737
    new-instance v2, LX/BX6;

    invoke-direct {v2, v0}, LX/BX6;-><init>(LX/BXC;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792738
    move-object v0, v1

    .line 1792739
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792740
    :cond_4
    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1792741
    return-object v3
.end method

.method private a(LX/BXC;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1792656
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792657
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->getChildCount()I

    move-result v0

    .line 1792658
    iget v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    if-ge v0, v2, :cond_0

    .line 1792659
    invoke-virtual {p1, v1}, LX/BXC;->a(Z)V

    .line 1792660
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    .line 1792661
    :goto_0
    return-void

    .line 1792662
    :cond_0
    iget v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792663
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    if-eqz v0, :cond_1

    move v0, v1

    .line 1792664
    :goto_1
    iget-object v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget v4, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792665
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    .line 1792666
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1792667
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1792668
    if-eqz v0, :cond_2

    .line 1792669
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    invoke-virtual {v0}, LX/BXC;->a()V

    .line 1792670
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v0}, LX/BXC;->j()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792671
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    iget v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    invoke-virtual {v0}, LX/BXC;->l()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792672
    :goto_2
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    add-int/lit8 v0, v0, 0x1

    move v2, v0

    :goto_3
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v2, v0, :cond_3

    .line 1792673
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792674
    invoke-virtual {v0}, LX/BXC;->n()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792675
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 1792676
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1792677
    :cond_2
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->getBadgeAppearAnimation()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1792678
    :cond_3
    invoke-virtual {p1, v1}, LX/BXC;->b(Z)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792679
    invoke-direct {p0, v3, v4}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a(Landroid/animation/AnimatorSet;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private a(LX/BXC;I)V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1792742
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1792743
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1792744
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1792745
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    if-ge v0, v3, :cond_0

    .line 1792746
    invoke-virtual {p1}, LX/BXC;->q()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792747
    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_3

    .line 1792748
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792749
    iget-object v3, v0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    sget v4, LX/BXC;->a:I

    .line 1792750
    const-string v5, "translationX"

    const/4 v6, 0x2

    new-array v6, v6, [F

    const/4 v7, 0x0

    const/4 p1, 0x0

    aput p1, v6, v7

    const/4 v7, 0x1

    neg-int p1, v4

    int-to-float p1, p1

    aput p1, v6, v7

    invoke-static {v3, v5, v6}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v5

    .line 1792751
    sget-object v6, LX/BXX;->a:Landroid/view/animation/Interpolator;

    move-object v6, v6

    .line 1792752
    invoke-virtual {v5, v6}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1792753
    invoke-static {v3}, LX/BXX;->f(Landroid/view/View;)Landroid/animation/AnimatorListenerAdapter;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792754
    move-object v3, v5

    .line 1792755
    new-instance v4, LX/BX7;

    invoke-direct {v4, v0}, LX/BX7;-><init>(LX/BXC;)V

    invoke-virtual {v3, v4}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792756
    move-object v0, v3

    .line 1792757
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792758
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 1792759
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    if-ne v0, v3, :cond_4

    .line 1792760
    iput v4, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792761
    const/4 v0, 0x2

    if-ge p2, v0, :cond_2

    .line 1792762
    invoke-virtual {p1}, LX/BXC;->a()V

    .line 1792763
    if-eqz p2, :cond_1

    .line 1792764
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    invoke-virtual {v0}, LX/BXC;->b()V

    .line 1792765
    :cond_1
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    .line 1792766
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    .line 1792767
    :goto_1
    return-void

    .line 1792768
    :cond_2
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->getBadgeDisappearAnimation()Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792769
    invoke-direct {p0, p1, p2, v5, v4}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a(LX/BXC;IIZ)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792770
    :cond_3
    :goto_2
    invoke-direct {p0, v1, v2}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a(Landroid/animation/AnimatorSet;Ljava/util/ArrayList;)V

    goto :goto_1

    .line 1792771
    :cond_4
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792772
    iget-object v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iget v4, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792773
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    .line 1792774
    if-gt p2, v0, :cond_5

    .line 1792775
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v0}, LX/BXC;->j()Landroid/animation/Animator;

    move-result-object v0

    .line 1792776
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1792777
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1792778
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    goto :goto_1

    .line 1792779
    :cond_5
    iget-object v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v3}, LX/BXC;->j()Landroid/animation/Animator;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792780
    invoke-direct {p0, p1, p2, v0, v5}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a(LX/BXC;IIZ)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1792781
    new-instance v0, LX/BWz;

    invoke-direct {v0, p0}, LX/BWz;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_2
.end method

.method private a(Landroid/animation/AnimatorSet;Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/animation/AnimatorSet;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1792782
    invoke-virtual {p1, p2}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1792783
    const-wide/16 v0, 0xfa

    invoke-virtual {p1, v0, v1}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 1792784
    new-instance v0, LX/BX0;

    invoke-direct {v0, p0}, LX/BX0;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    invoke-virtual {p1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792785
    invoke-virtual {p1}, Landroid/animation/AnimatorSet;->start()V

    .line 1792786
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1792787
    new-instance v0, LX/BX3;

    invoke-direct {v0, p0}, LX/BX3;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->e:Landroid/os/Handler;

    .line 1792788
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f:Ljava/util/Queue;

    .line 1792789
    return-void
.end method

.method public static b(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V
    .locals 3

    .prologue
    .line 1792790
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h:I

    if-nez v0, :cond_1

    .line 1792791
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792792
    sget v1, LX/BXC;->a:I

    if-nez v1, :cond_0

    iget-object v1, v0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->getMeasuredWidth()I

    move-result v1

    if-lez v1, :cond_0

    .line 1792793
    iget-object v1, v0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    invoke-virtual {v1}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->getMeasuredWidth()I

    move-result v1

    iget v2, v0, LX/BXC;->e:I

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    sput v1, LX/BXC;->a:I

    .line 1792794
    :cond_0
    sget v1, LX/BXC;->a:I

    move v0, v1

    .line 1792795
    iput v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h:I

    .line 1792796
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->c()V

    .line 1792797
    :cond_1
    new-instance v0, LX/BXC;

    invoke-direct {v0, p0, p1}, LX/BXC;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 1792798
    invoke-direct {p0, v0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a(LX/BXC;)V

    .line 1792799
    return-void
.end method

.method public static b(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 1792800
    const/4 v0, 0x0

    move v1, v2

    .line 1792801
    :goto_0
    iget-object v4, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_5

    .line 1792802
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792803
    iget-object v4, v0, LX/BXC;->c:Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-object v4, v4

    .line 1792804
    invoke-virtual {v4}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1792805
    :goto_1
    if-ne v1, v3, :cond_1

    .line 1792806
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    .line 1792807
    :goto_2
    return-void

    .line 1792808
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1792809
    :cond_1
    if-eqz p2, :cond_2

    .line 1792810
    invoke-direct {p0, v0, v1}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a(LX/BXC;I)V

    goto :goto_2

    .line 1792811
    :cond_2
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1792812
    invoke-virtual {v0}, LX/BXC;->a()V

    .line 1792813
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    if-gt v0, v1, :cond_4

    :goto_3
    iput v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792814
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1792815
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    iget v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    invoke-virtual {v0}, LX/BXC;->b()V

    .line 1792816
    :cond_3
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    .line 1792817
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    goto :goto_2

    .line 1792818
    :cond_4
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    sub-int v2, v0, v1

    goto :goto_3

    :cond_5
    move v1, v3

    goto :goto_1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1792819
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->i:I

    if-eqz v0, :cond_0

    .line 1792820
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->i:I

    iget v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->h:I

    div-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    .line 1792821
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->d()V

    .line 1792822
    :cond_0
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1792823
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->getChildCount()I

    move-result v0

    .line 1792824
    iget v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    if-le v0, v1, :cond_0

    .line 1792825
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->e()V

    .line 1792826
    :goto_0
    invoke-static {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V

    .line 1792827
    return-void

    .line 1792828
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f()V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 1792829
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792830
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 1792831
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792832
    invoke-virtual {v0}, LX/BXC;->a()V

    .line 1792833
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 1792834
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1792680
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    if-nez v0, :cond_1

    .line 1792681
    :cond_0
    return-void

    .line 1792682
    :cond_1
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792683
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    if-gt v1, v2, :cond_3

    .line 1792684
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    .line 1792685
    :goto_0
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    if-lt v1, v0, :cond_0

    .line 1792686
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792687
    iget-boolean v2, v0, LX/BXC;->f:Z

    move v2, v2

    .line 1792688
    if-nez v2, :cond_2

    .line 1792689
    invoke-virtual {v0}, LX/BXC;->b()V

    .line 1792690
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 1792691
    :cond_3
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->g:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    goto :goto_0
.end method

.method public static g(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V
    .locals 2

    .prologue
    .line 1792583
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    if-eqz v0, :cond_0

    .line 1792584
    iget v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    if-gtz v0, :cond_1

    .line 1792585
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    .line 1792586
    iget-object v1, v0, LX/BXC;->d:Lcom/facebook/widget/friendselector/SelectedFriendItemView;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Lcom/facebook/widget/friendselector/SelectedFriendItemView;->setVisibility(I)V

    .line 1792587
    :cond_0
    :goto_0
    return-void

    .line 1792588
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    iget v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, LX/BXC;->a(I)V

    .line 1792589
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v0}, LX/BXC;->c()V

    goto :goto_0
.end method

.method private getBadgeAppearAnimation()Landroid/animation/Animator;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1792590
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1792591
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792592
    iget-boolean v2, v0, LX/BXC;->f:Z

    move v2, v2

    .line 1792593
    if-eqz v2, :cond_0

    .line 1792594
    invoke-virtual {v0}, LX/BXC;->a()V

    .line 1792595
    :cond_0
    iget-object v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    .line 1792596
    iget-object v3, v0, LX/BXC;->c:Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-object v0, v3

    .line 1792597
    invoke-virtual {v2, v0}, LX/BXC;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 1792598
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v0}, LX/BXC;->h()V

    .line 1792599
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v0}, LX/BXC;->q()Landroid/animation/Animator;

    move-result-object v2

    .line 1792600
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792601
    iget v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v3}, LX/BXC;->a(I)V

    .line 1792602
    invoke-virtual {v0}, LX/BXC;->n()Landroid/animation/Animator;

    move-result-object v3

    .line 1792603
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v2, v4, v5

    aput-object v3, v4, v6

    invoke-virtual {v1, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1792604
    new-instance v2, LX/BX1;

    invoke-direct {v2, p0, v0}, LX/BX1;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;LX/BXC;)V

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792605
    return-object v1
.end method

.method private getBadgeDisappearAnimation()Landroid/animation/Animator;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1792606
    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1792607
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792608
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    .line 1792609
    iget-object v3, v0, LX/BXC;->c:Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-object v3, v3

    .line 1792610
    invoke-virtual {v1, v3}, LX/BXC;->a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V

    .line 1792611
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v1}, LX/BXC;->h()V

    .line 1792612
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v1}, LX/BXC;->c()V

    .line 1792613
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    invoke-virtual {v1, v6}, LX/BXC;->b(Z)Landroid/animation/Animator;

    move-result-object v3

    .line 1792614
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BXC;

    .line 1792615
    iget v4, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->j:I

    add-int/lit8 v4, v4, 0x2

    invoke-virtual {v1, v4}, LX/BXC;->a(I)V

    .line 1792616
    invoke-virtual {v1}, LX/BXC;->p()Landroid/animation/Animator;

    move-result-object v4

    .line 1792617
    const/4 v5, 0x2

    new-array v5, v5, [Landroid/animation/Animator;

    aput-object v3, v5, v6

    aput-object v4, v5, v7

    invoke-virtual {v2, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1792618
    new-instance v3, LX/BX2;

    invoke-direct {v3, p0, v0, v1}, LX/BX2;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;LX/BXC;LX/BXC;)V

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1792619
    return-object v2
.end method

.method public static h(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;)V
    .locals 2

    .prologue
    .line 1792620
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 1792621
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->d:LX/BWr;

    if-eqz v0, :cond_0

    .line 1792622
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->d:LX/BWr;

    invoke-interface {v0}, LX/BWr;->a()V

    .line 1792623
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1792624
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->e:Landroid/os/Handler;

    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1792625
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;)V
    .locals 3

    .prologue
    .line 1792626
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->e:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1792627
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f:Ljava/util/Queue;

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1792628
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1792629
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1792630
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1792631
    iget-object v3, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->e:Landroid/os/Handler;

    if-eqz p2, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v1, v0, v2, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 1792632
    iget-object v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f:Ljava/util/Queue;

    invoke-interface {v2, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1792633
    iget-object v2, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->f:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    if-ne v2, v1, :cond_0

    .line 1792634
    iget-object v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->e:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1792635
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1792636
    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1792637
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public getItemClickListener()LX/BWp;
    .locals 1

    .prologue
    .line 1792638
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->c:LX/BWp;

    return-object v0
.end method

.method public getItems()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1792639
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1792640
    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BXC;

    .line 1792641
    iget-object v4, v0, LX/BXC;->c:Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    move-object v0, v4

    .line 1792642
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1792643
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1792644
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1792645
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 1792646
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->i:I

    if-eq v0, v1, :cond_0

    .line 1792647
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->i:I

    .line 1792648
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->c()V

    .line 1792649
    :cond_0
    return-void
.end method

.method public setBadgeView(Lcom/facebook/widget/friendselector/SelectedFriendItemView;)V
    .locals 1

    .prologue
    .line 1792650
    new-instance v0, LX/BXC;

    invoke-direct {v0, p0, p1}, LX/BXC;-><init>(Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;Lcom/facebook/widget/friendselector/SelectedFriendItemView;)V

    iput-object v0, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->b:LX/BXC;

    .line 1792651
    return-void
.end method

.method public setItemClickListener(LX/BWp;)V
    .locals 0

    .prologue
    .line 1792652
    iput-object p1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->c:LX/BWp;

    .line 1792653
    return-void
.end method

.method public setItemSetChangeListener(LX/BWr;)V
    .locals 0

    .prologue
    .line 1792654
    iput-object p1, p0, Lcom/facebook/widget/friendselector/FriendSelectorResultContainer;->d:LX/BWr;

    .line 1792655
    return-void
.end method
