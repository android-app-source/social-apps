.class public Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;
.super Lcom/facebook/widget/CustomViewPager;
.source ""


# instance fields
.field private a:LX/BYE;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1795345
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewPager;-><init>(Landroid/content/Context;)V

    .line 1795346
    invoke-direct {p0}, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;->g()V

    .line 1795347
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1795348
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1795349
    invoke-direct {p0}, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;->g()V

    .line 1795350
    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 1795351
    new-instance v0, LX/BYE;

    invoke-direct {v0}, LX/BYE;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;->a:LX/BYE;

    .line 1795352
    iget-object v0, p0, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;->a:LX/BYE;

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1795353
    return-void
.end method


# virtual methods
.method public setOnPageChangeListener(LX/0hc;)V
    .locals 1

    .prologue
    .line 1795354
    if-eqz p1, :cond_0

    .line 1795355
    iget-object v0, p0, Lcom/facebook/widget/viewpager/ViewPagerWithCompositeOnPageChangeListener;->a:LX/BYE;

    .line 1795356
    iget-object p0, v0, LX/BYE;->a:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1795357
    :cond_0
    return-void
.end method
