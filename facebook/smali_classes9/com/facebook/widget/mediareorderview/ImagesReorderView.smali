.class public Lcom/facebook/widget/mediareorderview/ImagesReorderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final e:LX/0wT;

.field private static final f:LX/0wT;

.field private static final g:LX/0wT;

.field private static final h:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/BXn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:Lcom/facebook/widget/mediareorderview/MovableImageView;

.field public j:Lcom/facebook/widget/ScrollingAwareScrollView;

.field public k:Lcom/facebook/widget/CustomViewGroup;

.field public l:Landroid/view/View;

.field public m:Landroid/view/View;

.field public n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/mediareorderview/MovableImageView;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/BXl;

.field public p:LX/BXg;

.field private q:LX/BXi;

.field public r:LX/0wd;

.field private s:LX/BXf;

.field private t:I

.field private u:I

.field private v:LX/BXc;

.field private w:F

.field private x:Z

.field private final y:LX/4oV;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const-wide v4, 0x4062c00000000000L    # 150.0

    const-wide/high16 v2, 0x4030000000000000L    # 16.0

    .line 1793502
    invoke-static {v4, v5, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->e:LX/0wT;

    .line 1793503
    invoke-static {v4, v5, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->f:LX/0wT;

    .line 1793504
    const-wide v0, 0x406f400000000000L    # 250.0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->g:LX/0wT;

    .line 1793505
    const-class v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->h:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1793506
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1793507
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->w:F

    .line 1793508
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->x:Z

    .line 1793509
    new-instance v0, LX/BXb;

    invoke-direct {v0, p0}, LX/BXb;-><init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->y:LX/4oV;

    .line 1793510
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a()V

    .line 1793511
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1793512
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1793513
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->w:F

    .line 1793514
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->x:Z

    .line 1793515
    new-instance v0, LX/BXb;

    invoke-direct {v0, p0}, LX/BXb;-><init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->y:LX/4oV;

    .line 1793516
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a()V

    .line 1793517
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1793518
    const-class v0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-static {v0, p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1793519
    const v0, 0x7f0308da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1793520
    const v0, 0x7f0d16f6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ScrollingAwareScrollView;

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1793521
    const v0, 0x7f0d16f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomViewGroup;

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->k:Lcom/facebook/widget/CustomViewGroup;

    .line 1793522
    const v0, 0x7f0d16f5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->l:Landroid/view/View;

    .line 1793523
    new-instance v0, LX/BXl;

    invoke-direct {v0}, LX/BXl;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    .line 1793524
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    .line 1793525
    new-instance v0, LX/BXf;

    invoke-direct {v0, p0}, LX/BXf;-><init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->s:LX/BXf;

    .line 1793526
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->e:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const/4 v1, 0x1

    .line 1793527
    iput-boolean v1, v0, LX/0wd;->c:Z

    .line 1793528
    move-object v0, v0

    .line 1793529
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->s:LX/BXf;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->r:LX/0wd;

    .line 1793530
    new-instance v0, LX/BXi;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a:LX/0wW;

    invoke-direct {v0, v1, v2}, LX/BXi;-><init>(Landroid/widget/ScrollView;LX/0wW;)V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->q:LX/BXi;

    .line 1793531
    new-instance v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793532
    return-void
.end method

.method private a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1793533
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793534
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793535
    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    add-int/lit8 v3, p1, 0x1

    invoke-interface {v2, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 1793536
    invoke-virtual {v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getCurrentHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v3, v3, LX/BXg;->e:I

    add-int/2addr v2, v3

    invoke-virtual {v0, v4, v2}, Lcom/facebook/widget/mediareorderview/MovableImageView;->b(II)V

    .line 1793537
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getCurrentHeight()I

    move-result v0

    neg-int v0, v0

    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v2, v2, LX/BXg;->e:I

    sub-int/2addr v0, v2

    invoke-virtual {v1, v4, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->b(II)V

    .line 1793538
    return-void
.end method

.method private static a(Lcom/facebook/widget/mediareorderview/ImagesReorderView;LX/0wW;Landroid/content/res/Resources;LX/1Ad;LX/BXn;)V
    .locals 0

    .prologue
    .line 1793539
    iput-object p1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a:LX/0wW;

    iput-object p2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->b:Landroid/content/res/Resources;

    iput-object p3, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->c:LX/1Ad;

    iput-object p4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->d:LX/BXn;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;

    invoke-static {v3}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {v3}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-static {v3}, LX/BXn;->a(LX/0QB;)LX/BXn;

    move-result-object v3

    check-cast v3, LX/BXn;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a(Lcom/facebook/widget/mediareorderview/ImagesReorderView;LX/0wW;Landroid/content/res/Resources;LX/1Ad;LX/BXn;)V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Rect;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1793540
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    sub-int v1, v0, v1

    .line 1793541
    if-lez v1, :cond_2

    move v0, v3

    .line 1793542
    :goto_0
    if-ge v0, v1, :cond_0

    .line 1793543
    new-instance v2, Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/facebook/widget/mediareorderview/MovableImageView;-><init>(Landroid/content/Context;)V

    .line 1793544
    iget-object v4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1793545
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v3

    .line 1793546
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 1793547
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793548
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v1, :cond_1

    .line 1793549
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v1, v1, LX/BXg;->f:I

    int-to-float v4, v1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    div-float v1, v4, v1

    float-to-int v1, v1

    .line 1793550
    iget-object v4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->k:Lcom/facebook/widget/CustomViewGroup;

    iget-object v5, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v5, v5, LX/BXg;->f:I

    invoke-virtual {v4, v0, v5, v1}, Lcom/facebook/widget/CustomViewGroup;->addView(Landroid/view/View;II)V

    .line 1793551
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1793552
    :cond_2
    if-gez v1, :cond_3

    .line 1793553
    mul-int/lit8 v2, v1, -0x1

    move v1, v3

    .line 1793554
    :goto_2
    if-ge v1, v2, :cond_3

    .line 1793555
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793556
    iget-object v4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->k:Lcom/facebook/widget/CustomViewGroup;

    invoke-virtual {v4, v0}, Lcom/facebook/widget/CustomViewGroup;->removeView(Landroid/view/View;)V

    .line 1793557
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1793558
    :cond_3
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 1793559
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793560
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->d()V

    .line 1793561
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setCurrentRect(Landroid/graphics/Rect;)V

    .line 1793562
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1793563
    :cond_4
    return-void
.end method

.method public static b(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V
    .locals 2

    .prologue
    .line 1793356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->x:Z

    .line 1793357
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->REORDER:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->a(LX/BXk;)V

    .line 1793358
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793359
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->c()V

    goto :goto_0

    .line 1793360
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->y:LX/4oV;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->a(LX/4oV;)V

    .line 1793361
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    sget-object v1, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->g:LX/0wT;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setSpringConfig(LX/0wT;)V

    .line 1793362
    return-void
.end method

.method private b(Ljava/util/List;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Rect;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1793491
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1793492
    invoke-virtual {p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->d:LX/BXn;

    invoke-virtual {v2}, LX/BXn;->a()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v0, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    .line 1793493
    int-to-float v3, v2

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v3, v0

    float-to-int v3, v0

    .line 1793494
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->c:LX/1Ad;

    sget-object v4, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/net/Uri;

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v5, LX/1o9;

    invoke-direct {v5, v2, v3}, LX/1o9;-><init>(II)V

    .line 1793495
    iput-object v5, v0, LX/1bX;->c:LX/1o9;

    .line 1793496
    move-object v0, v0

    .line 1793497
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 1793498
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793499
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1793500
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1793501
    :cond_0
    return-void
.end method

.method public static e$redex0(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V
    .locals 3

    .prologue
    .line 1793468
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->REORDER:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1793469
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->q:LX/BXi;

    invoke-virtual {v0}, LX/BXi;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->x:Z

    if-nez v0, :cond_1

    .line 1793470
    :cond_0
    return-void

    .line 1793471
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getCurrentRectCenterY()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->getScrollY()I

    move-result v1

    add-int v2, v0, v1

    .line 1793472
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    iget v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793473
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getEndRectCenterY()I

    move-result v1

    if-ge v2, v1, :cond_2

    .line 1793474
    iget v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    .line 1793475
    :goto_0
    if-ltz v1, :cond_0

    .line 1793476
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793477
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getEndRectCenterY()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 1793478
    invoke-direct {p0, v1}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a(I)V

    .line 1793479
    add-int/lit8 v0, v1, -0x1

    .line 1793480
    iget v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    move v1, v0

    .line 1793481
    goto :goto_0

    .line 1793482
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getEndRectCenterY()I

    move-result v0

    if-le v2, v0, :cond_0

    .line 1793483
    iget v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    add-int/lit8 v0, v0, 0x1

    move v1, v0

    .line 1793484
    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1793485
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793486
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getEndRectCenterY()I

    move-result v0

    if-le v2, v0, :cond_0

    .line 1793487
    add-int/lit8 v0, v1, -0x1

    invoke-direct {p0, v0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a(I)V

    .line 1793488
    add-int/lit8 v0, v1, 0x1

    .line 1793489
    iget v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    move v1, v0

    .line 1793490
    goto :goto_1
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1793456
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->y:LX/4oV;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->b(LX/4oV;)V

    .line 1793457
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->r:LX/0wd;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->s:LX/BXf;

    invoke-virtual {v0, v1}, LX/0wd;->b(LX/0xi;)LX/0wd;

    .line 1793458
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->SHRINK:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1793459
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->r:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1793460
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    iget v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getCurrentRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 1793461
    :goto_0
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v2}, Lcom/facebook/widget/ScrollingAwareScrollView;->getScrollY()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    .line 1793462
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    sget-object v2, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->f:LX/0wT;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setSpringConfig(LX/0wT;)V

    .line 1793463
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setEndRect(Landroid/graphics/Rect;)V

    .line 1793464
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->EXPAND:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->a(LX/BXk;)V

    .line 1793465
    return-void

    .line 1793466
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->q:LX/BXi;

    invoke-virtual {v0}, LX/BXi;->a()V

    .line 1793467
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    iget v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getEndRect()Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method public static g$redex0(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V
    .locals 3

    .prologue
    .line 1793451
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setVisibility(I)V

    .line 1793452
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    iget v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793453
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setAlpha(F)V

    .line 1793454
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->v:LX/BXc;

    iget v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    iget v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->t:I

    invoke-interface {v0, v1, v2}, LX/BXc;->a(II)V

    .line 1793455
    return-void
.end method

.method public static h(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V
    .locals 2

    .prologue
    .line 1793446
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->setVisibility(I)V

    .line 1793447
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->INVISIBLE:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->a(LX/BXk;)V

    .line 1793448
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->v:LX/BXc;

    invoke-interface {v0}, LX/BXc;->a()V

    .line 1793449
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->l:Landroid/view/View;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setScaleX(F)V

    .line 1793450
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Rect;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 1793436
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->EXPAND:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1793437
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793438
    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->d()V

    .line 1793439
    add-int/lit8 v3, v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    .line 1793440
    iget-object v5, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v5}, Lcom/facebook/widget/ScrollingAwareScrollView;->getScrollY()I

    move-result v5

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Rect;->offset(II)V

    .line 1793441
    invoke-virtual {v0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setEndRect(Landroid/graphics/Rect;)V

    move v1, v3

    .line 1793442
    goto :goto_0

    .line 1793443
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->r:LX/0wd;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->s:LX/BXf;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    .line 1793444
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->r:LX/0wd;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1793445
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;Landroid/net/Uri;ILandroid/view/View;LX/BXc;Landroid/graphics/Point;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Rect;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;",
            "Landroid/net/Uri;",
            "I",
            "Landroid/view/View;",
            "LX/BXc;",
            "Landroid/graphics/Point;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1793392
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1793393
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->SHRINK:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->a(LX/BXk;)V

    .line 1793394
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1793395
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->j:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ScrollingAwareScrollView;->setVisibility(I)V

    .line 1793396
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setVisibility(I)V

    .line 1793397
    iput p4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->t:I

    iput p4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->u:I

    .line 1793398
    iput-object p5, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->m:Landroid/view/View;

    .line 1793399
    iput-object p6, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->v:LX/BXc;

    .line 1793400
    new-instance v0, LX/BXg;

    invoke-virtual {p5}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {p5}, Landroid/view/View;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->b:Landroid/content/res/Resources;

    const v4, 0x7f0b0c4e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->b:Landroid/content/res/Resources;

    const v5, 0x7f0b0c4f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iget-object v5, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->b:Landroid/content/res/Resources;

    const v6, 0x7f0b0c50

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    move v6, p4

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, LX/BXg;-><init>(IIIIIILjava/util/List;)V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    .line 1793401
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->a(Ljava/util/List;Ljava/util/List;)V

    .line 1793402
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->q:LX/BXi;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    invoke-virtual {v0, v1}, LX/BXi;->a(LX/BXg;)V

    .line 1793403
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->k:Lcom/facebook/widget/CustomViewGroup;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v1, v1, LX/BXg;->g:I

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomViewGroup;->setMinimumHeight(I)V

    .line 1793404
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->k:Lcom/facebook/widget/CustomViewGroup;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomViewGroup;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/BXa;

    invoke-direct {v1, p0}, LX/BXa;-><init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1793405
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v1, v0, LX/BXg;->c:I

    .line 1793406
    const/4 v0, 0x0

    move v2, v0

    move v3, v1

    :goto_1
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1793407
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793408
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v1, v1, LX/BXg;->f:I

    int-to-float v4, v1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    div-float v1, v4, v1

    float-to-int v1, v1

    .line 1793409
    iget-object v4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v4, v4, LX/BXg;->a:I

    iget-object v5, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v5, v5, LX/BXg;->f:I

    sub-int/2addr v4, v5

    div-int/lit8 v4, v4, 0x2

    .line 1793410
    new-instance v5, Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v6, v6, LX/BXg;->f:I

    add-int/2addr v6, v4

    add-int v7, v3, v1

    invoke-direct {v5, v4, v3, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1793411
    const/4 v4, 0x0

    iget-object v6, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v6, v6, LX/BXg;->i:I

    invoke-virtual {v0, v4, v6}, Lcom/facebook/widget/mediareorderview/MovableImageView;->a(II)V

    .line 1793412
    invoke-virtual {v0, v5}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setEndRect(Landroid/graphics/Rect;)V

    .line 1793413
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v0, v0, LX/BXg;->e:I

    add-int/2addr v0, v1

    add-int v1, v3, v0

    .line 1793414
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_1

    .line 1793415
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1793416
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v0, v0, LX/BXg;->f:I

    int-to-float v1, v0

    invoke-interface {p2, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    div-float v0, v1, v0

    float-to-int v1, v0

    .line 1793417
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    .line 1793418
    const v2, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v2}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setAlpha(F)V

    .line 1793419
    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v2}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1793420
    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    iget-object v3, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v3, v3, LX/BXg;->f:I

    invoke-virtual {p0, v2, v3, v1}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->addView(Landroid/view/View;II)V

    .line 1793421
    :cond_2
    invoke-virtual {p3}, Landroid/net/Uri;->isAbsolute()Z

    move-result v2

    if-nez v2, :cond_3

    .line 1793422
    new-instance v2, Ljava/io/File;

    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object p3

    .line 1793423
    :cond_3
    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->c:LX/1Ad;

    sget-object v3, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->h:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-static {p3}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v3

    new-instance v4, LX/1o9;

    iget-object v5, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v5, v5, LX/BXg;->f:I

    invoke-direct {v4, v5, v1}, LX/1o9;-><init>(II)V

    .line 1793424
    iput-object v4, v3, LX/1bX;->c:LX/1o9;

    .line 1793425
    move-object v1, v3

    .line 1793426
    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    .line 1793427
    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v2, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1793428
    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-interface {p1, p4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Rect;

    invoke-virtual {v2, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setCurrentRect(Landroid/graphics/Rect;)V

    .line 1793429
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getEndRect()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setEndRect(Landroid/graphics/Rect;)V

    .line 1793430
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    iget v1, p7, Landroid/graphics/Point;->x:I

    iget v2, p7, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/mediareorderview/MovableImageView;->c(II)V

    .line 1793431
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    new-instance v1, LX/BXe;

    invoke-direct {v1, p0}, LX/BXe;-><init>(Lcom/facebook/widget/mediareorderview/ImagesReorderView;)V

    .line 1793432
    iput-object v1, v0, Lcom/facebook/widget/mediareorderview/MovableImageView;->k:LX/BXd;

    .line 1793433
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    sget-object v1, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->f:LX/0wT;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setSpringConfig(LX/0wT;)V

    .line 1793434
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->b(Ljava/util/List;Ljava/util/List;)V

    .line 1793435
    return-void
.end method

.method public getDraweeControllers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/1aZ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1793387
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1793388
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1793389
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->n:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1793390
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1793391
    :cond_0
    return-object v2
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1793383
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1793384
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->REORDER:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->SHRINK:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1793385
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->f()V

    .line 1793386
    :cond_1
    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1793382
    const/4 v0, 0x1

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/high16 v5, -0x40800000    # -1.0f

    const-wide/16 v2, 0x0

    const/4 v4, 0x2

    const/4 v8, 0x1

    const v0, 0x2a75e2f8

    invoke-static {v4, v8, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1793363
    iget v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->w:F

    cmpl-float v0, v0, v5

    if-nez v0, :cond_0

    .line 1793364
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->w:F

    .line 1793365
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->INVISIBLE:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v1, LX/BXk;->EXPAND:LX/BXk;

    invoke-virtual {v0, v1}, LX/BXl;->b(LX/BXk;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1793366
    :cond_1
    const v0, 0x52428de5

    invoke-static {v4, v4, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1793367
    :goto_0
    return v8

    .line 1793368
    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1793369
    :cond_3
    :goto_1
    const v0, 0x41526dc

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0

    .line 1793370
    :pswitch_0
    iput v5, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->w:F

    .line 1793371
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->x:Z

    .line 1793372
    :pswitch_1
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->f()V

    goto :goto_1

    .line 1793373
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-double v0, v0

    iget-object v4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v4, v4, LX/BXg;->a:I

    int-to-double v4, v4

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-int v7, v0

    .line 1793374
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-double v0, v0

    iget-object v4, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->p:LX/BXg;

    iget v4, v4, LX/BXg;->b:I

    int-to-double v4, v4

    invoke-static/range {v0 .. v5}, LX/0xw;->a(DDD)D

    move-result-wide v0

    double-to-int v0, v0

    .line 1793375
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->i:Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-virtual {v1, v7, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->c(II)V

    .line 1793376
    invoke-virtual {p0}, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 1793377
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-static {v8, v2, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 1793378
    iget v2, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->w:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v1, v2, v1

    if-lez v1, :cond_3

    .line 1793379
    iput-boolean v8, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->x:Z

    .line 1793380
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->o:LX/BXl;

    sget-object v2, LX/BXk;->REORDER:LX/BXk;

    invoke-virtual {v1, v2}, LX/BXl;->b(LX/BXk;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1793381
    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/ImagesReorderView;->q:LX/BXi;

    invoke-virtual {v1, v0}, LX/BXi;->a(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
