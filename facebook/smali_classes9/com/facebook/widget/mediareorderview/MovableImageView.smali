.class public Lcom/facebook/widget/mediareorderview/MovableImageView;
.super Lcom/facebook/drawee/fbpipeline/FbDraweeView;
.source ""


# static fields
.field private static final c:LX/0wT;


# instance fields
.field private d:Landroid/graphics/Rect;

.field private e:Landroid/graphics/Rect;

.field private f:Landroid/graphics/Rect;

.field public g:LX/0wW;

.field private h:LX/0wd;

.field public i:D

.field private j:Z

.field public k:LX/BXd;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1793734
    const-wide v0, 0x406f400000000000L    # 250.0

    const-wide/high16 v2, 0x4034000000000000L    # 20.0

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/widget/mediareorderview/MovableImageView;->c:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1793735
    invoke-direct {p0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 1793736
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->e()V

    .line 1793737
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1793738
    invoke-direct {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1793739
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->e()V

    .line 1793740
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1793741
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1793742
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->e()V

    .line 1793743
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-static {v0}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v0

    check-cast v0, LX/0wW;

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->g:LX/0wW;

    return-void
.end method

.method public static b(Lcom/facebook/widget/mediareorderview/MovableImageView;DDD)V
    .locals 11

    .prologue
    .line 1793744
    iget-object v10, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-double v6, v0

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-double v8, v0

    move-wide v0, p1

    move-wide v2, p3

    move-wide/from16 v4, p5

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, v10, Landroid/graphics/Rect;->left:I

    .line 1793745
    iget-object v10, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-double v6, v0

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-double v8, v0

    move-wide v0, p1

    move-wide v2, p3

    move-wide/from16 v4, p5

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, v10, Landroid/graphics/Rect;->top:I

    .line 1793746
    iget-object v10, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-double v6, v0

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-double v8, v0

    move-wide v0, p1

    move-wide v2, p3

    move-wide/from16 v4, p5

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, v10, Landroid/graphics/Rect;->right:I

    .line 1793747
    iget-object v10, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-double v6, v0

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-double v8, v0

    move-wide v0, p1

    move-wide v2, p3

    move-wide/from16 v4, p5

    invoke-static/range {v0 .. v9}, LX/0xw;->a(DDDDD)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, v10, Landroid/graphics/Rect;->bottom:I

    .line 1793748
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->g()V

    .line 1793749
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1793695
    const-class v0, Lcom/facebook/widget/mediareorderview/MovableImageView;

    invoke-static {v0, p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1793696
    invoke-virtual {p0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setPivotX(F)V

    .line 1793697
    invoke-virtual {p0, v1}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setPivotY(F)V

    .line 1793698
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->d:Landroid/graphics/Rect;

    .line 1793699
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    .line 1793700
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    .line 1793701
    iput-boolean v2, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->j:Z

    .line 1793702
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->g:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/widget/mediareorderview/MovableImageView;->c:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1793703
    iput-boolean v2, v0, LX/0wd;->c:Z

    .line 1793704
    move-object v0, v0

    .line 1793705
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    move-result-object v0

    new-instance v1, LX/BXm;

    invoke-direct {v1, p0}, LX/BXm;-><init>(Lcom/facebook/widget/mediareorderview/MovableImageView;)V

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->h:LX/0wd;

    .line 1793706
    return-void
.end method

.method private f()V
    .locals 8

    .prologue
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    const-wide/16 v4, 0x0

    .line 1793750
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v0

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->d:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    iget-object v3, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->i:D

    .line 1793751
    iget-wide v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->i:D

    cmpl-double v0, v0, v4

    if-lez v0, :cond_1

    .line 1793752
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->h:LX/0wd;

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->i:D

    invoke-virtual {v0, v2, v3}, LX/0wd;->b(D)LX/0wd;

    .line 1793753
    :cond_0
    :goto_0
    return-void

    .line 1793754
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1793755
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->g()V

    .line 1793756
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->k:LX/BXd;

    if-eqz v0, :cond_0

    .line 1793757
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->k:LX/BXd;

    invoke-interface {v0}, LX/BXd;->b()V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1793758
    invoke-virtual {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 1793759
    :cond_0
    :goto_0
    return-void

    .line 1793760
    :cond_1
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setTranslationX(F)V

    .line 1793761
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setTranslationY(F)V

    .line 1793762
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setScaleX(F)V

    .line 1793763
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setScaleY(F)V

    goto :goto_0
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1793764
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->offset(II)V

    .line 1793765
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setCurrentRect(Landroid/graphics/Rect;)V

    .line 1793766
    return-void
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 1793767
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Rect;->offset(II)V

    .line 1793768
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setEndRect(Landroid/graphics/Rect;)V

    .line 1793769
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1793732
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->j:Z

    .line 1793733
    return-void
.end method

.method public final c(II)V
    .locals 3

    .prologue
    .line 1793770
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p1, v1

    iget-object v2, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, p2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 1793771
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->setEndRect(Landroid/graphics/Rect;)V

    .line 1793772
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1793730
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->j:Z

    .line 1793731
    return-void
.end method

.method public getCurrentHeight()I
    .locals 1

    .prologue
    .line 1793729
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    return v0
.end method

.method public getCurrentRect()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1793728
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getCurrentRectCenterY()I
    .locals 1

    .prologue
    .line 1793727
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    return v0
.end method

.method public getCurrentWidth()I
    .locals 1

    .prologue
    .line 1793726
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    return v0
.end method

.method public getEndRect()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1793725
    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    return-object v0
.end method

.method public getEndRectCenterY()I
    .locals 1

    .prologue
    .line 1793724
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    return v0
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7d4aff3f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1793721
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->onSizeChanged(IIII)V

    .line 1793722
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->g()V

    .line 1793723
    const/16 v1, 0x2d

    const v2, -0x29945c5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setCurrentRect(Landroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 1793716
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1793717
    iget-boolean v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->h:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->i()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1793718
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->f()V

    .line 1793719
    :goto_0
    return-void

    .line 1793720
    :cond_0
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->g()V

    goto :goto_0
.end method

.method public setEndRect(Landroid/graphics/Rect;)V
    .locals 2

    .prologue
    .line 1793711
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1793712
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->d:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->f:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 1793713
    iget-boolean v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->j:Z

    if-eqz v0, :cond_0

    .line 1793714
    invoke-direct {p0}, Lcom/facebook/widget/mediareorderview/MovableImageView;->f()V

    .line 1793715
    :cond_0
    return-void
.end method

.method public setEventListener(LX/BXd;)V
    .locals 0

    .prologue
    .line 1793709
    iput-object p1, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->k:LX/BXd;

    .line 1793710
    return-void
.end method

.method public setSpringConfig(LX/0wT;)V
    .locals 1

    .prologue
    .line 1793707
    iget-object v0, p0, Lcom/facebook/widget/mediareorderview/MovableImageView;->h:LX/0wd;

    invoke-virtual {v0, p1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    .line 1793708
    return-void
.end method
