.class public Lcom/facebook/places/create/citypicker/CityPickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final A:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;>;"
        }
    .end annotation
.end field

.field public p:Lcom/facebook/places/create/citypicker/CityPickerAdapter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/places/create/citypicker/FetchCityRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Lcom/facebook/widget/listview/BetterListView;

.field private s:LX/94Z;

.field private t:Landroid/widget/EditText;

.field private u:Landroid/location/Location;

.field public v:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field

.field private w:Ljava/lang/String;

.field private x:Landroid/widget/FrameLayout;

.field private y:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/FrameLayout;",
            ">;"
        }
    .end annotation
.end field

.field private z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1921748
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1921749
    new-instance v0, LX/Ccq;

    invoke-direct {v0, p0}, LX/Ccq;-><init>(Lcom/facebook/places/create/citypicker/CityPickerActivity;)V

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->A:LX/0TF;

    return-void
.end method

.method private static a(Lcom/facebook/places/create/citypicker/CityPickerActivity;Lcom/facebook/places/create/citypicker/CityPickerAdapter;Lcom/facebook/places/create/citypicker/FetchCityRunner;)V
    .locals 0

    .prologue
    .line 1921747
    iput-object p1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->p:Lcom/facebook/places/create/citypicker/CityPickerAdapter;

    iput-object p2, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->q:Lcom/facebook/places/create/citypicker/FetchCityRunner;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;

    new-instance p1, Lcom/facebook/places/create/citypicker/CityPickerAdapter;

    invoke-static {v1}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    invoke-direct {p1, v0}, Lcom/facebook/places/create/citypicker/CityPickerAdapter;-><init>(Landroid/view/LayoutInflater;)V

    move-object v0, p1

    check-cast v0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;

    invoke-static {v1}, Lcom/facebook/places/create/citypicker/FetchCityRunner;->b(LX/0QB;)Lcom/facebook/places/create/citypicker/FetchCityRunner;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/create/citypicker/FetchCityRunner;

    invoke-static {p0, v0, v1}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->a(Lcom/facebook/places/create/citypicker/CityPickerActivity;Lcom/facebook/places/create/citypicker/CityPickerAdapter;Lcom/facebook/places/create/citypicker/FetchCityRunner;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1921743
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->q:Lcom/facebook/places/create/citypicker/FetchCityRunner;

    invoke-virtual {v0}, Lcom/facebook/places/create/citypicker/FetchCityRunner;->a()V

    .line 1921744
    new-instance v0, Lcom/facebook/places/create/citypicker/FetchCityParam;

    iget-object v1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->w:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->u:Landroid/location/Location;

    invoke-direct {v0, v1, v2}, Lcom/facebook/places/create/citypicker/FetchCityParam;-><init>(Ljava/lang/String;Landroid/location/Location;)V

    .line 1921745
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->q:Lcom/facebook/places/create/citypicker/FetchCityRunner;

    iget-object v2, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->A:LX/0TF;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/places/create/citypicker/FetchCityRunner;->a(Lcom/facebook/places/create/citypicker/FetchCityParam;LX/0TF;)V

    .line 1921746
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1921735
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->z:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1921736
    :goto_0
    return-void

    .line 1921737
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->y:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->x:Landroid/widget/FrameLayout;

    .line 1921738
    const v0, 0x7f081706

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->z:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1921739
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->x:Landroid/widget/FrameLayout;

    const v2, 0x7f0d252d

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1921740
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1921741
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->x:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1921742
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->x:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->bringToFront()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1921730
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1921731
    const-string v1, "extra_cleared_location"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1921732
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1921733
    invoke-virtual {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->finish()V

    .line 1921734
    return-void
.end method

.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 1

    .prologue
    .line 1921727
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->w:Ljava/lang/String;

    .line 1921728
    invoke-direct {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->b()V

    .line 1921729
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1921693
    invoke-static {p0, p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1921694
    const v0, 0x7f030294

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->setContentView(I)V

    .line 1921695
    invoke-virtual {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1921696
    if-nez p1, :cond_2

    .line 1921697
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->w:Ljava/lang/String;

    .line 1921698
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->v:Ljava/util/ArrayList;

    .line 1921699
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->u:Landroid/location/Location;

    .line 1921700
    invoke-virtual {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "previously_tagged_location"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->z:Ljava/lang/String;

    .line 1921701
    const v0, 0x7f0d0a7f

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/94V;

    .line 1921702
    new-instance v1, LX/Ccn;

    invoke-direct {v1, p0}, LX/Ccn;-><init>(Lcom/facebook/places/create/citypicker/CityPickerActivity;)V

    invoke-virtual {v0, v1}, LX/94V;->setOnBackPressedListener(LX/63J;)V

    .line 1921703
    new-instance v1, LX/94Z;

    new-instance v2, LX/94Y;

    invoke-direct {v2}, LX/94Y;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08172e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1921704
    iput-object v3, v2, LX/94Y;->a:Ljava/lang/String;

    .line 1921705
    move-object v2, v2

    .line 1921706
    invoke-static {}, LX/94c;->c()LX/94c;

    move-result-object v3

    .line 1921707
    iput-object v3, v2, LX/94Y;->d:LX/94c;

    .line 1921708
    move-object v2, v2

    .line 1921709
    invoke-virtual {v2}, LX/94Y;->a()LX/94X;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/94Z;-><init>(LX/94V;LX/94X;)V

    iput-object v1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->s:LX/94Z;

    .line 1921710
    const v0, 0x7f0d0948

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1921711
    const v1, 0x7f0d034e

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->t:Landroid/widget/EditText;

    .line 1921712
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->t:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1921713
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    .line 1921714
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->p:Lcom/facebook/places/create/citypicker/CityPickerAdapter;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1921715
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 1921716
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0, p0}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1921717
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->p:Lcom/facebook/places/create/citypicker/CityPickerAdapter;

    iget-object v1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->a(Ljava/util/List;)V

    .line 1921718
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->p:Lcom/facebook/places/create/citypicker/CityPickerAdapter;

    const v1, 0x6000e49d

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1921719
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921720
    invoke-direct {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->b()V

    .line 1921721
    :cond_0
    new-instance v1, LX/0zw;

    const v0, 0x7f0d0949

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    new-instance v2, LX/Ccp;

    invoke-direct {v2, p0}, LX/Ccp;-><init>(Lcom/facebook/places/create/citypicker/CityPickerActivity;)V

    invoke-direct {v1, v0, v2}, LX/0zw;-><init>(Landroid/view/ViewStub;LX/0zy;)V

    iput-object v1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->y:LX/0zw;

    .line 1921722
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->z:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1921723
    invoke-direct {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->l()V

    .line 1921724
    :cond_1
    return-void

    .line 1921725
    :cond_2
    const-string v0, "state_query"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->w:Ljava/lang/String;

    .line 1921726
    const-string v0, "state_current_list"

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->v:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1921692
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 1921686
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->r:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1921687
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1921688
    const-string v2, "selected_city"

    invoke-static {v1, v2, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1921689
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 1921690
    invoke-virtual {p0}, Lcom/facebook/places/create/citypicker/CityPickerActivity;->finish()V

    .line 1921691
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x40c5ef2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1921683
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1921684
    invoke-static {p0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1921685
    const/16 v1, 0x23

    const v2, 0x388dcfd0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1921679
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1921680
    const-string v0, "state_query"

    iget-object v1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->w:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1921681
    const-string v0, "state_current_list"

    iget-object v1, p0, Lcom/facebook/places/create/citypicker/CityPickerActivity;->v:Ljava/util/ArrayList;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 1921682
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 1921678
    return-void
.end method
