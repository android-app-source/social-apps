.class public Lcom/facebook/places/create/citypicker/NewCityPickerFragment;
.super Lcom/facebook/places/pickers/PlaceContentPickerFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/places/pickers/PlaceContentPickerFragment",
        "<",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Ccv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Cd4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/CdG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/CdD;

.field public g:Z

.field public h:Z

.field public i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<+",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<+",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field

.field public k:Landroid/location/Location;

.field private final l:LX/Ccy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1922037
    invoke-direct {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;-><init>()V

    .line 1922038
    new-instance v0, LX/Ccy;

    invoke-direct {v0, p0}, LX/Ccy;-><init>(Lcom/facebook/places/create/citypicker/NewCityPickerFragment;)V

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->l:LX/Ccy;

    return-void
.end method

.method public static a(Landroid/location/Location;ZZLX/Ccr;ZLX/CdF;Landroid/os/Parcelable;)Lcom/facebook/places/create/citypicker/NewCityPickerFragment;
    .locals 2
    .param p6    # Landroid/os/Parcelable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1922026
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1922027
    const-string v1, "extra_current_location"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1922028
    const-string v1, "extra_is_checking_into_city"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1922029
    const-string v1, "extra_show_current_location"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1922030
    const-string v1, "extra_city_selected_listener"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1922031
    const-string v1, "extra_show_null_state_header"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1922032
    const-string v1, "extra_logger_type"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1922033
    const-string v1, "extra_logger_params"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1922034
    new-instance v1, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    invoke-direct {v1}, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;-><init>()V

    .line 1922035
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1922036
    return-object v1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    move-object v1, p1

    check-cast v1, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    invoke-static {p0}, LX/Ccv;->a(LX/0QB;)LX/Ccv;

    move-result-object v2

    check-cast v2, LX/Ccv;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/Cd4;->b(LX/0QB;)LX/Cd4;

    move-result-object v5

    check-cast v5, LX/Cd4;

    new-instance v0, LX/CdG;

    const-class p1, LX/CdJ;

    invoke-interface {p0, p1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/CdJ;

    invoke-direct {v0, p1}, LX/CdG;-><init>(LX/CdJ;)V

    move-object p0, v0

    check-cast p0, LX/CdG;

    iput-object v2, v1, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a:LX/Ccv;

    iput-object v3, v1, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->b:LX/1Ck;

    iput-object v4, v1, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->c:LX/0tX;

    iput-object v5, v1, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->d:LX/Cd4;

    iput-object p0, v1, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->e:LX/CdG;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "LX/9ks",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1922008
    new-instance v0, Lcom/facebook/places/create/citypicker/FetchCityParam;

    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->k:Landroid/location/Location;

    invoke-direct {v0, p1, v1}, Lcom/facebook/places/create/citypicker/FetchCityParam;-><init>(Ljava/lang/String;Landroid/location/Location;)V

    .line 1922009
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1922010
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a:LX/Ccv;

    .line 1922011
    iget-object v3, v1, LX/Ccv;->a:LX/0am;

    invoke-virtual {v3}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v4, v0, Lcom/facebook/places/create/citypicker/FetchCityParam;->a:Ljava/lang/String;

    iget-object v3, v1, LX/Ccv;->a:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/places/create/citypicker/FetchCityParam;

    iget-object v3, v3, Lcom/facebook/places/create/citypicker/FetchCityParam;->a:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v4, v0, Lcom/facebook/places/create/citypicker/FetchCityParam;->b:Landroid/location/Location;

    iget-object v3, v1, LX/Ccv;->a:LX/0am;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/places/create/citypicker/FetchCityParam;

    iget-object v3, v3, Lcom/facebook/places/create/citypicker/FetchCityParam;->b:Landroid/location/Location;

    invoke-virtual {v4, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1922012
    iget-object v3, v1, LX/Ccv;->b:LX/0am;

    .line 1922013
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 1922014
    invoke-virtual {v3, v4}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Px;

    .line 1922015
    :goto_0
    move-object v3, v3

    .line 1922016
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_0

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1922017
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v6, v7, v5}, LX/9ks;->a(Ljava/lang/Object;JLjava/lang/String;)LX/9kr;

    move-result-object v0

    invoke-virtual {v0}, LX/9kr;->a()LX/9ks;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1922018
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1922019
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1922020
    :cond_1
    iget-object v3, v1, LX/Ccv;->c:Lcom/facebook/places/create/citypicker/FetchCityRunner;

    invoke-virtual {v3}, Lcom/facebook/places/create/citypicker/FetchCityRunner;->a()V

    .line 1922021
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    iput-object v3, v1, LX/Ccv;->a:LX/0am;

    .line 1922022
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    iput-object v3, v1, LX/Ccv;->b:LX/0am;

    .line 1922023
    iget-object v3, v1, LX/Ccv;->c:Lcom/facebook/places/create/citypicker/FetchCityRunner;

    new-instance v4, LX/Ccu;

    invoke-direct {v4, v1}, LX/Ccu;-><init>(LX/Ccv;)V

    invoke-virtual {v3, v0, v4}, Lcom/facebook/places/create/citypicker/FetchCityRunner;->a(Lcom/facebook/places/create/citypicker/FetchCityParam;LX/0TF;)V

    .line 1922024
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 1922025
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1921987
    invoke-super {p0, p1}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->a(Landroid/os/Bundle;)V

    .line 1921988
    const-class v0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;

    invoke-static {v0, p0}, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1921989
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    .line 1921990
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    .line 1921991
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1921992
    const-string v1, "extra_current_location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->k:Landroid/location/Location;

    .line 1921993
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->e:LX/CdG;

    .line 1921994
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1921995
    const-string v2, "extra_logger_type"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CdF;

    .line 1921996
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1921997
    const-string v3, "extra_logger_params"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .line 1921998
    sget-object v3, LX/CdE;->a:[I

    invoke-virtual {v0}, LX/CdF;->ordinal()I

    move-result p1

    aget v3, v3, p1

    packed-switch v3, :pswitch_data_0

    .line 1921999
    new-instance v3, LX/CdH;

    invoke-direct {v3}, LX/CdH;-><init>()V

    :goto_0
    move-object v0, v3

    .line 1922000
    iput-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->f:LX/CdD;

    .line 1922001
    return-void

    .line 1922002
    :pswitch_0
    instance-of v3, v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    if-eqz v3, :cond_0

    .line 1922003
    iget-object v3, v1, LX/CdG;->a:LX/CdJ;

    check-cast v2, Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;

    .line 1922004
    new-instance v0, LX/CdI;

    invoke-static {v3}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object p1

    check-cast p1, LX/0Zb;

    invoke-static {v3}, LX/31g;->a(LX/0QB;)LX/31g;

    move-result-object v1

    check-cast v1, LX/31g;

    invoke-direct {v0, p1, v1, v2}, LX/CdI;-><init>(LX/0Zb;LX/31g;Lcom/facebook/crowdsourcing/logging/CrowdsourcingContext;)V

    .line 1922005
    move-object v3, v0

    .line 1922006
    goto :goto_0

    .line 1922007
    :cond_0
    new-instance v3, LX/CdH;

    invoke-direct {v3}, LX/CdH;-><init>()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 1921975
    check-cast p1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1921976
    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1921977
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_1

    .line 1921978
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->f:LX/CdD;

    sget-object v1, LX/969;->PLACE_CITY:LX/969;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/CdD;->a(LX/969;J)V

    .line 1921979
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1921980
    const-string v1, "extra_city_selected_listener"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/Ccr;

    .line 1921981
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_3

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v0, p0, p1, v1}, LX/Ccr;->a(Lcom/facebook/places/create/citypicker/NewCityPickerFragment;Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;Z)V

    .line 1921982
    return-void

    .line 1921983
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 1921984
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->f:LX/CdD;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, LX/CdD;->a(J)V

    goto :goto_0

    .line 1921985
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->f:LX/CdD;

    sget-object v1, LX/969;->PLACE_CITY:LX/969;

    invoke-virtual {p1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->m()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, LX/CdD;->a(LX/969;JLjava/lang/String;)V

    goto :goto_0

    .line 1921986
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b()LX/0Px;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/9ks",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1921947
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1921948
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921949
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1921950
    const-string v1, "extra_show_current_location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1921951
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1921952
    const-string v1, "extra_is_checking_into_city"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 1921953
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 1921954
    :goto_0
    if-eqz v1, :cond_3

    const v0, 0x7f08173c

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1921955
    :goto_1
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->j:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v4, v6, v7, v2}, LX/9ks;->a(Ljava/lang/Object;JLjava/lang/String;)LX/9kr;

    move-result-object v0

    const v2, 0x7f0e06d2

    .line 1921956
    iput v2, v0, LX/9kr;->d:I

    .line 1921957
    move-object v0, v0

    .line 1921958
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1921959
    iput-object v1, v0, LX/9kr;->e:LX/0am;

    .line 1921960
    move-object v0, v0

    .line 1921961
    const v1, 0x7f021216

    .line 1921962
    iput v1, v0, LX/9kr;->f:I

    .line 1921963
    move-object v0, v0

    .line 1921964
    invoke-virtual {v0}, LX/9kr;->a()LX/9ks;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1921965
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1921966
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1921967
    const-string v1, "extra_show_current_location"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1921968
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v4, v5, v0}, LX/9ks;->a(Ljava/lang/Object;JLjava/lang/String;)LX/9kr;

    move-result-object v0

    const v1, 0x7f02148b

    .line 1921969
    iput v1, v0, LX/9kr;->f:I

    .line 1921970
    move-object v0, v0

    .line 1921971
    invoke-virtual {v0}, LX/9kr;->a()LX/9ks;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1921972
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 1921973
    :cond_2
    const v0, 0x7f08173a

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_0

    .line 1921974
    :cond_3
    const v0, 0x7f08173b

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1921918
    const v0, 0x7f081746

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1921946
    const v0, 0x7f081739

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/0am;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "LX/9kq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1921940
    new-instance v0, LX/9kq;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/9kq;-><init>(Landroid/content/Context;)V

    .line 1921941
    const v1, 0x7f020350

    invoke-virtual {v0, v1}, LX/9kq;->setImage(I)V

    .line 1921942
    const v1, 0x7f081744

    invoke-virtual {v0, v1}, LX/9kq;->setTitle(I)V

    .line 1921943
    const v1, 0x7f081745

    invoke-virtual {v0, v1}, LX/9kq;->setSubTitle(I)V

    .line 1921944
    const v1, 0x7f081743

    invoke-virtual {v0, v1}, LX/9kq;->setSectionTitle(I)V

    .line 1921945
    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1921939
    iget-boolean v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->g:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1921936
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a:LX/Ccv;

    .line 1921937
    iget-object p0, v0, LX/Ccv;->a:LX/0am;

    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_0

    iget-object p0, v0, LX/Ccv;->b:LX/0am;

    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result p0

    if-nez p0, :cond_0

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 1921938
    return v0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x472e5035

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1921931
    invoke-super {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->onPause()V

    .line 1921932
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a:LX/Ccv;

    iget-object v2, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->l:LX/Ccy;

    .line 1921933
    iget-object v4, v1, LX/Ccv;->e:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1921934
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1921935
    const/16 v1, 0x2b

    const v2, -0x3495b375    # -1.5355019E7f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x838c442

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1921919
    invoke-super {p0}, Lcom/facebook/places/pickers/PlaceContentPickerFragment;->onResume()V

    .line 1921920
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->a:LX/Ccv;

    iget-object v2, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->l:LX/Ccy;

    .line 1921921
    iget-object v4, v1, LX/Ccv;->e:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1921922
    const-class v1, LX/1ZF;

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1ZF;

    .line 1921923
    const v2, 0x7f081737

    invoke-interface {v1, v2}, LX/1ZF;->x_(I)V

    .line 1921924
    invoke-interface {v1}, LX/1ZF;->lH_()V

    .line 1921925
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->i:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1921926
    :goto_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->h:Z

    .line 1921927
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->b:LX/1Ck;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v4, LX/Cd1;

    invoke-direct {v4, p0}, LX/Cd1;-><init>(Lcom/facebook/places/create/citypicker/NewCityPickerFragment;)V

    new-instance v5, LX/Cd2;

    invoke-direct {v5, p0}, LX/Cd2;-><init>(Lcom/facebook/places/create/citypicker/NewCityPickerFragment;)V

    invoke-virtual {v1, v2, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1921928
    const/16 v1, 0x2b

    const v2, 0x6d818cf6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1921929
    :cond_0
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->g:Z

    .line 1921930
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/NewCityPickerFragment;->b:LX/1Ck;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    new-instance v4, LX/Ccz;

    invoke-direct {v4, p0}, LX/Ccz;-><init>(Lcom/facebook/places/create/citypicker/NewCityPickerFragment;)V

    new-instance v5, LX/Cd0;

    invoke-direct {v5, p0}, LX/Cd0;-><init>(Lcom/facebook/places/create/citypicker/NewCityPickerFragment;)V

    invoke-virtual {v1, v2, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
