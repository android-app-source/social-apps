.class public final Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/0am",
        "<+",
        "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/location/Location;

.field public final synthetic b:LX/Cd4;


# direct methods
.method public constructor <init>(LX/Cd4;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 1922065
    iput-object p1, p0, Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;->b:LX/Cd4;

    iput-object p2, p0, Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;->a:Landroid/location/Location;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1922066
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;->b:LX/Cd4;

    iget-object v0, v0, LX/Cd4;->f:LX/6Wr;

    iget-object v1, p0, Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;->a:Landroid/location/Location;

    .line 1922067
    invoke-virtual {v1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    .line 1922068
    invoke-virtual {v1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v6

    .line 1922069
    const-string v8, "current_location_city"

    invoke-static {v8}, LX/6Wo;->a(Ljava/lang/String;)LX/6We;

    move-result-object v8

    const/4 v9, 0x2

    new-array v9, v9, [LX/6Wc;

    const/4 v10, 0x0

    sget-object v11, LX/Cd4;->a:LX/6Wc;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    sget-object v11, LX/Cd4;->c:LX/6Wc;

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, LX/6Wd;->a([LX/6Wc;)LX/6Wk;

    move-result-object v8

    sget-object v9, LX/6Wn;->GEO_REGION:LX/6Wn;

    invoke-virtual {v8, v9}, LX/6Wd;->a(LX/6Wn;)LX/6Wi;

    move-result-object v8

    sget-object v9, LX/Cd4;->d:LX/6Wc;

    invoke-virtual {v9, v4, v5}, LX/6Wb;->a(D)LX/6Wa;

    move-result-object v4

    sget-object v5, LX/Cd4;->e:LX/6Wc;

    invoke-virtual {v5, v6, v7}, LX/6Wb;->a(D)LX/6Wa;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/6Wa;->a(LX/6WX;)LX/6WZ;

    move-result-object v4

    sget-object v5, LX/Cd4;->b:LX/6Wc;

    const-string v6, "city"

    invoke-virtual {v5, v6}, LX/6Wb;->a(Ljava/lang/String;)LX/6Wa;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/6WZ;->a(LX/6WX;)LX/6WZ;

    move-result-object v4

    invoke-virtual {v8, v4}, LX/6Wd;->a(LX/6WX;)LX/6Wl;

    move-result-object v4

    move-object v1, v4

    .line 1922070
    new-instance v2, LX/Cd3;

    invoke-direct {v2, p0}, LX/Cd3;-><init>(Lcom/facebook/places/create/citypicker/PlaceCreationCityAtLocationQuery$1;)V

    const-class v3, LX/Cd4;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/6Wr;->a(LX/6Wd;LX/0QK;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0am;

    return-object v0
.end method
