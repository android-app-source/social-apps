.class public final Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1922237
    const-class v0, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;

    new-instance v1, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1922238
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1922239
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1922240
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1922241
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1922242
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1922243
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1922244
    if-eqz v2, :cond_0

    .line 1922245
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1922246
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1922247
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1922248
    if-eqz v2, :cond_1

    .line 1922249
    const-string p0, "current_city"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1922250
    invoke-static {v1, v2, p1, p2}, LX/CdB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1922251
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1922252
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1922253
    check-cast p1, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel$Serializer;->a(Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$SuggestedCitiesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
