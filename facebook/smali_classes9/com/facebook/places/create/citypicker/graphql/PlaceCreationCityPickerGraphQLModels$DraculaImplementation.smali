.class public final Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1922139
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1922140
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1922137
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1922138
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 1922127
    if-nez p1, :cond_0

    .line 1922128
    :goto_0
    return v1

    .line 1922129
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1922130
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1922131
    :pswitch_0
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1922132
    invoke-virtual {p0, p1, v8, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1922133
    const/4 v0, 0x2

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    move-object v0, p3

    .line 1922134
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    move-object v0, p3

    move v1, v8

    move-wide v2, v6

    .line 1922135
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1922136
    invoke-virtual {p3}, LX/186;->d()I

    move-result v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x23e805c5
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1922126
    new-instance v0, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1922123
    packed-switch p0, :pswitch_data_0

    .line 1922124
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1922125
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x23e805c5
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1922122
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1922120
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;->b(I)V

    .line 1922121
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1922089
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1922090
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1922091
    :cond_0
    iput-object p1, p0, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1922092
    iput p2, p0, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;->b:I

    .line 1922093
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1922119
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1922118
    new-instance v0, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1922115
    iget v0, p0, LX/1vt;->c:I

    .line 1922116
    move v0, v0

    .line 1922117
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1922112
    iget v0, p0, LX/1vt;->c:I

    .line 1922113
    move v0, v0

    .line 1922114
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1922109
    iget v0, p0, LX/1vt;->b:I

    .line 1922110
    move v0, v0

    .line 1922111
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1922106
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1922107
    move-object v0, v0

    .line 1922108
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1922097
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1922098
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1922099
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1922100
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1922101
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1922102
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1922103
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1922104
    invoke-static {v3, v9, v2}, Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/places/create/citypicker/graphql/PlaceCreationCityPickerGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1922105
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1922094
    iget v0, p0, LX/1vt;->c:I

    .line 1922095
    move v0, v0

    .line 1922096
    return v0
.end method
