.class public Lcom/facebook/places/create/citypicker/CityPickerAdapter;
.super Landroid/widget/BaseAdapter;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private b:Landroid/view/LayoutInflater;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1921760
    const-class v0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;

    const-string v1, "place_home"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921765
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1921766
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->c:Ljava/util/List;

    .line 1921767
    iput-object p1, p0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->b:Landroid/view/LayoutInflater;

    .line 1921768
    return-void
.end method

.method private a(I)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 1

    .prologue
    .line 1921764
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1921761
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->c:Ljava/util/List;

    .line 1921762
    const v0, 0x4e1f2124    # 6.6743731E8f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1921763
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1921769
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1921751
    invoke-direct {p0, p1}, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->a(I)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1921750
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1921752
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030295

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1921753
    invoke-direct {p0, p1}, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->a(I)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v2

    .line 1921754
    const v0, 0x7f0d094a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1921755
    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1921756
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1921757
    const v0, 0x7f0d094c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1921758
    const/4 v2, 0x0

    sget-object v3, Lcom/facebook/places/create/citypicker/CityPickerAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1921759
    return-object v1
.end method
