.class public Lcom/facebook/places/create/citypicker/FetchCityRunner;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<",
            "Lcom/facebook/places/create/citypicker/FetchCityRunner;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final c:LX/9kE;

.field private d:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1921842
    const-class v0, Lcom/facebook/places/create/citypicker/FetchCityRunner;

    .line 1921843
    sput-object v0, Lcom/facebook/places/create/citypicker/FetchCityRunner;->a:Ljava/lang/Class;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/create/citypicker/FetchCityRunner;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/9kE;LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1921844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1921845
    iput-object p1, p0, Lcom/facebook/places/create/citypicker/FetchCityRunner;->c:LX/9kE;

    .line 1921846
    iput-object p2, p0, Lcom/facebook/places/create/citypicker/FetchCityRunner;->d:LX/0tX;

    .line 1921847
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/places/create/citypicker/FetchCityRunner;
    .locals 3

    .prologue
    .line 1921848
    new-instance v2, Lcom/facebook/places/create/citypicker/FetchCityRunner;

    invoke-static {p0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v0

    check-cast v0, LX/9kE;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-direct {v2, v0, v1}, Lcom/facebook/places/create/citypicker/FetchCityRunner;-><init>(LX/9kE;LX/0tX;)V

    .line 1921849
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1921850
    iget-object v0, p0, Lcom/facebook/places/create/citypicker/FetchCityRunner;->c:LX/9kE;

    invoke-virtual {v0}, LX/9kE;->c()V

    .line 1921851
    return-void
.end method

.method public final a(Lcom/facebook/places/create/citypicker/FetchCityParam;LX/0TF;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/create/citypicker/FetchCityParam;",
            "LX/0TF",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1921852
    if-nez p1, :cond_0

    .line 1921853
    :goto_0
    return-void

    .line 1921854
    :cond_0
    new-instance v0, LX/4DI;

    invoke-direct {v0}, LX/4DI;-><init>()V

    .line 1921855
    iget-object v1, p1, Lcom/facebook/places/create/citypicker/FetchCityParam;->b:Landroid/location/Location;

    if-eqz v1, :cond_2

    .line 1921856
    new-instance v1, LX/3Aj;

    invoke-direct {v1}, LX/3Aj;-><init>()V

    .line 1921857
    iget-object v2, p1, Lcom/facebook/places/create/citypicker/FetchCityParam;->b:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    .line 1921858
    iget-object v2, p1, Lcom/facebook/places/create/citypicker/FetchCityParam;->b:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    .line 1921859
    iget-object v2, p1, Lcom/facebook/places/create/citypicker/FetchCityParam;->b:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 1921860
    iget-object v2, p1, Lcom/facebook/places/create/citypicker/FetchCityParam;->b:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->hasSpeed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1921861
    iget-object v2, p1, Lcom/facebook/places/create/citypicker/FetchCityParam;->b:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getSpeed()F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/3Aj;->d(Ljava/lang/Double;)LX/3Aj;

    .line 1921862
    :cond_1
    invoke-virtual {v0, v1}, LX/4DI;->a(LX/3Aj;)LX/4DI;

    .line 1921863
    :cond_2
    iget-object v1, p1, Lcom/facebook/places/create/citypicker/FetchCityParam;->a:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1921864
    iget-object v1, p1, Lcom/facebook/places/create/citypicker/FetchCityParam;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4DI;->a(Ljava/lang/String;)LX/4DI;

    .line 1921865
    :cond_3
    new-instance v1, LX/5m1;

    invoke-direct {v1}, LX/5m1;-><init>()V

    move-object v1, v1

    .line 1921866
    const-string v2, "query_data"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    const-string v2, "num_nearby_places"

    const/16 v3, 0x14

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "search_context"

    const-string v3, "CITY_SEARCH"

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1921867
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1921868
    iget-object v1, p0, Lcom/facebook/places/create/citypicker/FetchCityRunner;->c:LX/9kE;

    iget-object v2, p0, Lcom/facebook/places/create/citypicker/FetchCityRunner;->d:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/Ccx;

    invoke-direct {v2, p0, p2}, LX/Ccx;-><init>(Lcom/facebook/places/create/citypicker/FetchCityRunner;LX/0TF;)V

    invoke-virtual {v1, v0, v2}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_0
.end method
