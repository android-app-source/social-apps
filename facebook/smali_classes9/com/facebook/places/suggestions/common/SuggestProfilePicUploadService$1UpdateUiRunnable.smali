.class public final Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4ck;
.implements Ljava/lang/Runnable;


# instance fields
.field public a:I

.field public final synthetic b:LX/Cdg;

.field public final synthetic c:Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;


# direct methods
.method public constructor <init>(Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;LX/Cdg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 1923080
    iput-object p1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;->c:Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;

    iput-object p2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;->b:LX/Cdg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(JJ)V
    .locals 3

    .prologue
    .line 1923081
    const-wide/16 v0, 0x64

    mul-long/2addr v0, p1

    div-long/2addr v0, p3

    long-to-int v0, v0

    .line 1923082
    iget v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;->a:I

    if-eq v0, v1, :cond_0

    .line 1923083
    iput v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;->a:I

    .line 1923084
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;->c:Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;

    iget-object v0, v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0, p0}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 1923085
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 6

    .prologue
    .line 1923086
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;->b:LX/Cdg;

    iget v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;->a:I

    const/16 p0, 0x64

    const/4 v5, 0x0

    .line 1923087
    iget-object v2, v0, LX/Cdg;->d:LX/2HB;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1923088
    div-int/lit8 v2, v1, 0xa

    iget v3, v0, LX/Cdg;->c:I

    div-int/lit8 v3, v3, 0xa

    if-ne v2, v3, :cond_0

    .line 1923089
    :goto_0
    return-void

    .line 1923090
    :cond_0
    iput v1, v0, LX/Cdg;->c:I

    .line 1923091
    iget-object v2, v0, LX/Cdg;->d:LX/2HB;

    invoke-virtual {v2, p0, v1, v5}, LX/2HB;->a(IIZ)LX/2HB;

    .line 1923092
    iget-object v2, v0, LX/Cdg;->d:LX/2HB;

    iget-object v3, v0, LX/Cdg;->f:Landroid/app/PendingIntent;

    .line 1923093
    iput-object v3, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1923094
    move-object v2, v2

    .line 1923095
    iget-object v3, v0, LX/Cdg;->a:Landroid/content/Context;

    const v4, 0x7f0816ac

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/2HB;->d(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    const v3, 0x1080088

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v2

    iget v3, v0, LX/Cdg;->c:I

    invoke-virtual {v2, p0, v3, v5}, LX/2HB;->a(IIZ)LX/2HB;

    move-result-object v2

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    .line 1923096
    iget-object v3, v0, LX/Cdg;->b:Landroid/app/NotificationManager;

    invoke-static {v0}, LX/Cdg;->d(LX/Cdg;)I

    move-result v4

    invoke-virtual {v3, v4, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0
.end method
