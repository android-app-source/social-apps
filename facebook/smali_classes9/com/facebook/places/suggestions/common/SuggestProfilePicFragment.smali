.class public Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/content/SecureContextHelper;

.field public c:LX/0kL;

.field public d:LX/9kE;

.field public e:LX/1Ad;

.field public f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public g:Lcom/facebook/photos/base/media/PhotoItem;

.field public h:Z

.field public i:Z

.field private j:Landroid/view/View;

.field private k:Landroid/widget/Button;

.field public l:J

.field private m:Landroid/net/Uri;

.field public n:LX/CdT;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field private final q:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1923006
    const-class v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    const-string v1, "place_home"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1922999
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1923000
    iput-boolean v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->h:Z

    .line 1923001
    iput-boolean v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->i:Z

    .line 1923002
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l:J

    .line 1923003
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->m:Landroid/net/Uri;

    .line 1923004
    new-instance v0, LX/CdZ;

    invoke-direct {v0, p0}, LX/CdZ;-><init>(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)V

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->q:Landroid/view/View$OnClickListener;

    .line 1923005
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    invoke-static {p0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v1

    check-cast v1, LX/9kE;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v3

    check-cast v3, LX/0kL;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object v1, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->d:LX/9kE;

    iput-object v2, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v3, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->c:LX/0kL;

    iput-object p0, p1, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->e:LX/1Ad;

    return-void
.end method

.method public static d(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)Z
    .locals 1

    .prologue
    .line 1922998
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->g:Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)Z
    .locals 4

    .prologue
    .line 1922997
    iget-wide v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)V
    .locals 5

    .prologue
    .line 1922989
    invoke-direct {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l()V

    .line 1922990
    invoke-static {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->e(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1922991
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0816b3

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1922992
    iget-wide v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l:J

    .line 1922993
    invoke-static {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->d(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1922994
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "page_id"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "photo_item"

    iget-object v4, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->g:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "source"

    iget-object v4, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->n:LX/CdT;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "entry_point"

    iget-object v4, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->o:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "endpoint"

    iget-object v4, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->p:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 1922995
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1922996
    :cond_0
    return-void
.end method

.method private l()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 1922976
    invoke-static {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->d(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1922977
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->g:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 1922978
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1922979
    move-object v0, v0

    .line 1922980
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    .line 1922981
    iget-object v2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->e:LX/1Ad;

    iget-object v3, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v3, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1922982
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1922983
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->k:Landroid/widget/Button;

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setVisibility(I)V

    .line 1922984
    :goto_0
    return-void

    .line 1922985
    :cond_0
    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->e:LX/1Ad;

    iget-object v2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/view/DraweeView;->getController()LX/1aZ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/1Ae;->b(LX/1aZ;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v2, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->m:Landroid/net/Uri;

    invoke-virtual {v0, v2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1922986
    iget-boolean v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->i:Z

    if-eqz v0, :cond_1

    .line 1922987
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1922988
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1922972
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1922973
    const-class v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;

    invoke-static {v0, p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1922974
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->d:LX/9kE;

    invoke-virtual {v0}, LX/9kE;->a()V

    .line 1922975
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1923007
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->PLACE_PROFILE_PIC_SUGGESTS:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->l()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->LAUNCH_GENERIC_CROPPER:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 1923008
    const/16 v1, 0x3af

    .line 1923009
    iget-object v2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, v1, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1923010
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1922969
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->g:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1922970
    invoke-direct {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l()V

    .line 1922971
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1922954
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1922955
    :goto_0
    return-void

    .line 1922956
    :cond_0
    const/16 v0, 0x3af

    if-ne p1, v0, :cond_1

    .line 1922957
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1922958
    if-eqz v0, :cond_2

    .line 1922959
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 1922960
    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1922961
    new-instance v1, LX/74k;

    invoke-direct {v1}, LX/74k;-><init>()V

    .line 1922962
    iget-object v2, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v2

    .line 1922963
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/74k;->a(Ljava/lang/String;)LX/74k;

    move-result-object v0

    const-string v1, "image/jpeg"

    invoke-virtual {v0, v1}, LX/74k;->c(Ljava/lang/String;)LX/74k;

    move-result-object v0

    invoke-virtual {v0}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->g:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1922964
    iget-boolean v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->h:Z

    if-eqz v0, :cond_3

    .line 1922965
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->d:LX/9kE;

    new-instance v1, LX/Cda;

    invoke-direct {v1, p0}, LX/Cda;-><init>(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)V

    new-instance v2, LX/Cdb;

    invoke-direct {v2, p0}, LX/Cdb;-><init>(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)V

    invoke-virtual {v0, v1, v2}, LX/9kE;->a(Ljava/util/concurrent/Callable;LX/0TF;)V

    .line 1922966
    :cond_1
    :goto_2
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0

    .line 1922967
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 1922968
    :cond_3
    invoke-static {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->k(Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;)V

    goto :goto_2
.end method

.method public final onContextItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1922948
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0d3207

    if-ne v1, v2, :cond_0

    .line 1922949
    invoke-virtual {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->b()V

    .line 1922950
    :goto_0
    return v0

    .line 1922951
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0d3208

    if-ne v1, v2, :cond_1

    .line 1922952
    invoke-virtual {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->c()V

    goto :goto_0

    .line 1922953
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 2

    .prologue
    .line 1922946
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f110008

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1922947
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x18afef55

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1922945
    const v1, 0x7f031429

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x50d8060c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1922935
    const-string v0, "SuggestProfilePicFragment.media_item"

    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->g:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1922936
    const-string v0, "place_id"

    iget-wide v2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1922937
    const-string v0, "source"

    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->n:LX/CdT;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 1922938
    const-string v0, "entry_point"

    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1922939
    const-string v0, "endpoint"

    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1922940
    const-string v0, "current_image_uri"

    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->m:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1922941
    const-string v0, "SuggestProfilePicFragment.confirm_dialog"

    iget-boolean v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1922942
    const-string v0, "display_as_cover_photo"

    iget-boolean v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1922943
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1922944
    return-void
.end method

.method public final onStart()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x564e9943

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1922927
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1922928
    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->d:LX/9kE;

    .line 1922929
    invoke-static {v1}, LX/9kE;->e(LX/9kE;)V

    .line 1922930
    iget-object v2, v1, LX/9kE;->e:Ljava/util/List;

    if-eqz v2, :cond_1

    .line 1922931
    iget-object v2, v1, LX/9kE;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Runnable;

    .line 1922932
    iget-object v5, v1, LX/9kE;->c:Landroid/os/Handler;

    const p0, 0x682e7e0b

    invoke-static {v5, v2, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 1922933
    :cond_0
    const/4 v2, 0x0

    iput-object v2, v1, LX/9kE;->e:Ljava/util/List;

    .line 1922934
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x5a9735c2

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4eb0c237

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1922924
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStop()V

    .line 1922925
    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->d:LX/9kE;

    invoke-virtual {v1}, LX/9kE;->a()V

    .line 1922926
    const/16 v1, 0x2b

    const v2, 0x7daf7be1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1922900
    if-eqz p2, :cond_0

    .line 1922901
    const-string v0, "SuggestProfilePicFragment.media_item"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->g:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1922902
    const-string v0, "SuggestProfilePicFragment.confirm_dialog"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->h:Z

    .line 1922903
    const-string v0, "display_as_cover_photo"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->i:Z

    .line 1922904
    const-string v0, "place_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l:J

    .line 1922905
    const-string v0, "source"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/CdT;

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->n:LX/CdT;

    .line 1922906
    const-string v0, "entry_point"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->o:Ljava/lang/String;

    .line 1922907
    const-string v0, "endpoint"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->p:Ljava/lang/String;

    .line 1922908
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->m:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 1922909
    const-string v0, "current_image_uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->m:Landroid/net/Uri;

    .line 1922910
    :cond_0
    const v0, 0x7f0d2dfc

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1922911
    iget-boolean v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->i:Z

    if-nez v0, :cond_1

    .line 1922912
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const v1, 0x7f02147e

    invoke-virtual {v0, v1}, LX/1af;->b(I)V

    .line 1922913
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1922914
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnCreateContextMenuListener(Landroid/view/View$OnCreateContextMenuListener;)V

    .line 1922915
    const v0, 0x7f0d2e38

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->j:Landroid/view/View;

    .line 1922916
    const v0, 0x7f0d2e39

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->k:Landroid/widget/Button;

    .line 1922917
    iget-boolean v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->i:Z

    if-eqz v0, :cond_2

    .line 1922918
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->k:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1922919
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->k:Landroid/widget/Button;

    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1922920
    :goto_0
    invoke-direct {p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->l()V

    .line 1922921
    return-void

    .line 1922922
    :cond_2
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->k:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 1922923
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicFragment;->q:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method
