.class public Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Lcom/facebook/photos/base/media/PhotoItem;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1923048
    new-instance v0, LX/Cdf;

    invoke-direct {v0}, LX/Cdf;-><init>()V

    sput-object v0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLcom/facebook/photos/base/media/PhotoItem;LX/CdT;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1923063
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923064
    iput-wide p1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->a:J

    .line 1923065
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->b:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1923066
    if-nez p4, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->c:Ljava/lang/String;

    .line 1923067
    iput-object p5, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->d:Ljava/lang/String;

    .line 1923068
    iput-object p6, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->e:Ljava/lang/String;

    .line 1923069
    return-void

    .line 1923070
    :cond_0
    invoke-virtual {p4}, LX/CdT;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1923056
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923057
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->a:J

    .line 1923058
    const-class v0, Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->b:Lcom/facebook/photos/base/media/PhotoItem;

    .line 1923059
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->c:Ljava/lang/String;

    .line 1923060
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->d:Ljava/lang/String;

    .line 1923061
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->e:Ljava/lang/String;

    .line 1923062
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1923055
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1923049
    iget-wide v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1923050
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->b:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1923051
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1923052
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1923053
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1923054
    return-void
.end method
