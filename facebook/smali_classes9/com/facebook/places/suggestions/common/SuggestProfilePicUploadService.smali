.class public Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;
.super LX/1ZN;
.source ""


# instance fields
.field public a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/Cdg;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Cde;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Cdg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1923146
    const-string v0, "SuggestedPicUploadService"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1923147
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->a:Ljava/util/HashMap;

    .line 1923148
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1923149
    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->c:LX/0Ot;

    .line 1923150
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1923151
    iput-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->d:LX/0Ot;

    .line 1923152
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->b:I

    .line 1923153
    return-void
.end method

.method private static a(Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Cde;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Cdg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1923125
    iput-object p1, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->c:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->d:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->e:LX/0Or;

    iput-object p4, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->f:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 5

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;

    const/16 v1, 0x271

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xb83

    invoke-static {v0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2f5f

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    const/16 v4, 0x2f60

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p0, v1, v2, v3, v0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->a(Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 14

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    const/4 v10, -0x1

    const/4 v2, 0x2

    const/16 v4, 0x24

    const v5, -0x59e52583

    invoke-static {v2, v4, v5}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v12

    .line 1923126
    const-string v2, "start_id"

    invoke-virtual {p1, v2, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1923127
    const-string v2, "source"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v7

    check-cast v7, LX/CdT;

    .line 1923128
    const-string v2, "entry_point"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1923129
    const-string v2, "endpoint"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1923130
    if-eq v4, v10, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 1923131
    iget-object v2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->a:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cdg;

    .line 1923132
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1923133
    const-string v4, "page_id"

    const-wide/16 v10, -0x1

    invoke-virtual {p1, v4, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 1923134
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-lez v10, :cond_1

    :goto_1
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1923135
    const-string v3, "photo_item"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v3

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1923136
    iget-object v3, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->e:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, LX/Cde;

    .line 1923137
    new-instance v13, LX/14U;

    invoke-direct {v13}, LX/14U;-><init>()V

    .line 1923138
    new-instance v3, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;

    invoke-direct {v3, p0, v2}, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1UpdateUiRunnable;-><init>(Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;LX/Cdg;)V

    .line 1923139
    iput-object v3, v13, LX/14U;->a:LX/4ck;

    .line 1923140
    :try_start_0
    iget-object v3, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, LX/11H;

    move-object v11, v0

    new-instance v3, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/places/suggestions/common/SuggestProfilePicParams;-><init>(JLcom/facebook/photos/base/media/PhotoItem;LX/CdT;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v10, v3, v13}, LX/11H;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    .line 1923141
    iget-object v3, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Sh;

    new-instance v4, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1;

    invoke-direct {v4, p0, v2}, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$1;-><init>(Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;LX/Cdg;)V

    invoke-virtual {v3, v4}, LX/0Sh;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1923142
    :goto_2
    const v2, 0xb6fb0c7

    invoke-static {v2, v12}, LX/02F;->d(II)V

    return-void

    :cond_0
    move v2, v6

    .line 1923143
    goto :goto_0

    :cond_1
    move v3, v6

    .line 1923144
    goto :goto_1

    .line 1923145
    :catch_0
    iget-object v3, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Sh;

    new-instance v4, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$2;

    invoke-direct {v4, p0, v2}, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService$2;-><init>(Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;LX/Cdg;)V

    invoke-virtual {v3, v4}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_2
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1923124
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, 0x7cbdbd7d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1923121
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1923122
    invoke-static {p0, p0}, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1923123
    const/16 v1, 0x25

    const v2, 0x45748b0a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x24

    const v1, -0x244a57d6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1923108
    const-string v0, "start_id"

    invoke-virtual {p1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1923109
    iget-object v0, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cdg;

    .line 1923110
    const/4 v2, 0x0

    invoke-static {p0, p3, p1, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1923111
    iput-object v2, v0, LX/Cdg;->e:Landroid/app/PendingIntent;

    .line 1923112
    const/4 v6, 0x0

    .line 1923113
    new-instance v2, LX/2HB;

    iget-object v3, v0, LX/Cdg;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/2HB;-><init>(Landroid/content/Context;)V

    const v3, 0x1080088

    invoke-virtual {v2, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v2

    iget-object v3, v0, LX/Cdg;->a:Landroid/content/Context;

    const v5, 0x7f0816ac

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v2

    iget-object v3, v0, LX/Cdg;->f:Landroid/app/PendingIntent;

    .line 1923114
    iput-object v3, v2, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1923115
    move-object v2, v2

    .line 1923116
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3, v6, v6}, LX/2HB;->a(IIZ)LX/2HB;

    move-result-object v2

    iput-object v2, v0, LX/Cdg;->d:LX/2HB;

    .line 1923117
    iget-object v2, v0, LX/Cdg;->b:Landroid/app/NotificationManager;

    invoke-static {v0}, LX/Cdg;->d(LX/Cdg;)I

    move-result v3

    iget-object v5, v0, LX/Cdg;->d:LX/2HB;

    invoke-virtual {v5}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1923118
    iget-object v2, p0, Lcom/facebook/places/suggestions/common/SuggestProfilePicUploadService;->a:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1923119
    invoke-super {p0, p1, p2, p3}, LX/1ZN;->onStartCommand(Landroid/content/Intent;II)I

    .line 1923120
    const/16 v0, 0x25

    const v2, 0x4c10c334    # 3.7948624E7f

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v4
.end method
