.class public Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:Landroid/support/v4/app/DialogFragment;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/widget/titlebar/TitleBarButtonSpec;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/CdM;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field private h:LX/CdO;

.field public i:LX/0kL;

.field private j:LX/CdY;

.field public k:LX/0h5;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1922599
    const-class v0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    sput-object v0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1922559
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1922560
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->f:Ljava/util/Set;

    return-void
.end method

.method private a(LX/0Px;LX/0Rf;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1922561
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1922562
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1922563
    invoke-static {v0}, LX/CdS;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/CdR;

    move-result-object v4

    .line 1922564
    iput-boolean v5, v4, LX/CdR;->b:Z

    .line 1922565
    move-object v4, v4

    .line 1922566
    invoke-virtual {v4}, LX/CdR;->a()LX/CdS;

    move-result-object v4

    .line 1922567
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0Rf;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1922568
    invoke-virtual {v4, v5}, LX/CdS;->a(Z)V

    .line 1922569
    :cond_0
    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1922570
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1922571
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->b(LX/0Px;)V

    .line 1922572
    return-void
.end method

.method public static a$redex0(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;Z)V
    .locals 2

    .prologue
    .line 1922573
    if-eqz p1, :cond_0

    .line 1922574
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->k:LX/0h5;

    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->c:Ljava/util/List;

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1922575
    :goto_0
    return-void

    .line 1922576
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->k:LX/0h5;

    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->d:Ljava/util/List;

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    goto :goto_0
.end method

.method private b(LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/CdS;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1922592
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->h:LX/CdO;

    .line 1922593
    iput-object p1, v0, LX/CdO;->b:LX/0Px;

    .line 1922594
    const v1, 0x6a34a8bc

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1922595
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 1922596
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->h:LX/CdO;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1922597
    new-instance v1, LX/CdQ;

    invoke-direct {v1, p0}, LX/CdQ;-><init>(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1922598
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1922577
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f080031

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1922578
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1922579
    move-object v0, v0

    .line 1922580
    const/4 v1, 0x1

    .line 1922581
    iput-boolean v1, v0, LX/108;->d:Z

    .line 1922582
    move-object v0, v0

    .line 1922583
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->c:Ljava/util/List;

    .line 1922584
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f080031

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1922585
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1922586
    move-object v0, v0

    .line 1922587
    const/4 v1, 0x0

    .line 1922588
    iput-boolean v1, v0, LX/108;->d:Z

    .line 1922589
    move-object v0, v0

    .line 1922590
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->d:Ljava/util/List;

    .line 1922591
    return-void
.end method

.method private d()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1922600
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->h:LX/CdO;

    .line 1922601
    iget-object v2, v0, LX/CdO;->b:LX/0Px;

    move-object v3, v2

    .line 1922602
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CdS;

    .line 1922603
    iget-boolean p0, v0, LX/CdS;->e:Z

    move v0, p0

    .line 1922604
    if-eqz v0, :cond_0

    .line 1922605
    const/4 v0, 0x1

    .line 1922606
    :goto_1
    return v0

    .line 1922607
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1922608
    goto :goto_1
.end method

.method public static synthetic d(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;)Z
    .locals 1

    .prologue
    .line 1922551
    invoke-direct {p0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->d()Z

    move-result v0

    return v0
.end method

.method public static e(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;)V
    .locals 3

    .prologue
    .line 1922552
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->f:Ljava/util/Set;

    monitor-enter v1

    .line 1922553
    :try_start_0
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CdM;

    .line 1922554
    invoke-interface {v0}, LX/CdM;->a()V

    goto :goto_0

    .line 1922555
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/places/graphql/PlacesGraphQLInterfaces$CheckinPlace;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1922556
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1922557
    invoke-direct {p0, p1, v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a(LX/0Px;LX/0Rf;)V

    .line 1922558
    return-void
.end method

.method public final a(LX/CdM;)V
    .locals 2

    .prologue
    .line 1922463
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->f:Ljava/util/Set;

    monitor-enter v1

    .line 1922464
    :try_start_0
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1922465
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1922466
    monitor-exit v1

    return-void

    .line 1922467
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1922468
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1922469
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1922470
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->kX_()LX/0QA;

    move-result-object v1

    .line 1922471
    invoke-static {v1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->i:LX/0kL;

    .line 1922472
    invoke-static {v1}, LX/CdY;->b(LX/0QB;)LX/CdY;

    move-result-object v0

    check-cast v0, LX/CdY;

    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->j:LX/CdY;

    .line 1922473
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->kX_()LX/0QA;

    move-result-object v0

    .line 1922474
    new-instance p1, LX/CdO;

    const-class v1, Landroid/content/Context;

    invoke-interface {v0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-direct {p1, v1}, LX/CdO;-><init>(Landroid/content/Context;)V

    .line 1922475
    move-object v0, p1

    .line 1922476
    check-cast v0, LX/CdO;

    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->h:LX/CdO;

    .line 1922477
    invoke-direct {p0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->c()V

    .line 1922478
    return-void
.end method

.method public final a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V
    .locals 2

    .prologue
    .line 1922479
    iput-object p1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1922480
    const v0, 0x7f0d1aa8

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/suggestions/PlaceRowView;

    .line 1922481
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v1}, LX/CdS;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/CdR;

    move-result-object v1

    invoke-virtual {v1}, LX/CdR;->a()LX/CdS;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/places/suggestions/PlaceRowView;->setInfo(LX/CdS;)V

    .line 1922482
    return-void
.end method

.method public final b()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1922483
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1922484
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->h:LX/CdO;

    .line 1922485
    iget-object v1, v0, LX/CdO;->b:LX/0Px;

    move-object v4, v1

    .line 1922486
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CdS;

    .line 1922487
    iget-boolean v6, v0, LX/CdS;->e:Z

    move v6, v6

    .line 1922488
    if-eqz v6, :cond_0

    .line 1922489
    iget-object v0, v0, LX/CdS;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1922490
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1922491
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 1922492
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1922493
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->i:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082944

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1922494
    :goto_1
    return-void

    .line 1922495
    :cond_2
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/CdW;->a(Ljava/lang/String;)LX/CdV;

    move-result-object v1

    .line 1922496
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1922497
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    if-lez v3, :cond_3

    const/4 v3, 0x1

    :goto_2
    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 1922498
    iput-object v0, v1, LX/CdV;->o:LX/0Px;

    .line 1922499
    sget-object v0, LX/CdT;->COMPOSER_EDIT:LX/CdT;

    .line 1922500
    iput-object v0, v1, LX/CdV;->A:LX/CdT;

    .line 1922501
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->e:Ljava/lang/String;

    .line 1922502
    iput-object v0, v1, LX/CdV;->D:Ljava/lang/String;

    .line 1922503
    const-string v0, "android_mark_duplicates"

    .line 1922504
    iput-object v0, v1, LX/CdV;->E:Ljava/lang/String;

    .line 1922505
    const v0, 0x7f080037

    const/4 v3, 0x1

    invoke-static {v0, v3, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->b:Landroid/support/v4/app/DialogFragment;

    .line 1922506
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->b:Landroid/support/v4/app/DialogFragment;

    .line 1922507
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v2

    .line 1922508
    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1922509
    new-instance v0, LX/CdP;

    invoke-direct {v0, p0}, LX/CdP;-><init>(Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;)V

    .line 1922510
    iget-object v2, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->j:LX/CdY;

    invoke-virtual {v1}, LX/CdV;->a()LX/CdW;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, LX/CdY;->a(LX/CdW;LX/0TF;)V

    goto :goto_1

    .line 1922511
    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x8ed81d6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1922512
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 1922513
    const v1, 0x7f030a7a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1ce560c0

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x31ade47e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1922514
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->j:LX/CdY;

    .line 1922515
    iget-object v2, v1, LX/CdY;->a:LX/9kE;

    invoke-virtual {v2}, LX/9kE;->c()V

    .line 1922516
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1922517
    const/16 v1, 0x2b

    const v2, -0x731117d8

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 1922518
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1922519
    const-string v0, "target_place"

    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->g:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1922520
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 1922521
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 1922522
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->h:LX/CdO;

    .line 1922523
    iget-object v1, v0, LX/CdO;->b:LX/0Px;

    move-object v4, v1

    .line 1922524
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CdS;

    .line 1922525
    iget-object v6, v0, LX/CdS;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1922526
    iget-boolean v6, v0, LX/CdS;->e:Z

    move v6, v6

    .line 1922527
    if-eqz v6, :cond_0

    .line 1922528
    iget-object v0, v0, LX/CdS;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1922529
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1922530
    :cond_1
    const-string v0, "place_list"

    invoke-static {p1, v0, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 1922531
    const-string v0, "checked_places"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1922532
    const-string v0, "entry_point"

    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1922533
    return-void
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x75aea96

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1922534
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 1922535
    if-eqz p1, :cond_3

    .line 1922536
    const-string v0, "target_place"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1922537
    if-eqz v0, :cond_0

    .line 1922538
    invoke-virtual {p0, v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 1922539
    :cond_0
    const-string v0, "place_list"

    invoke-static {p1, v0}, LX/4By;->b(Landroid/os/Bundle;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 1922540
    const-string v0, "checked_places"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 1922541
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v5

    .line 1922542
    if-eqz v4, :cond_1

    .line 1922543
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1922544
    invoke-virtual {v5, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1922545
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1922546
    :cond_1
    if-eqz v3, :cond_2

    .line 1922547
    invoke-virtual {v5}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a(LX/0Px;LX/0Rf;)V

    .line 1922548
    :cond_2
    const-string v0, "entry_point"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1922549
    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->e:Ljava/lang/String;

    .line 1922550
    :cond_3
    const v0, 0x7487e478

    invoke-static {v0, v2}, LX/02F;->f(II)V

    return-void
.end method
