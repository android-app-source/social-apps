.class public Lcom/facebook/places/suggestions/PlaceRowView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ToggleButton;

.field private final g:Landroid/widget/ImageView;

.field public h:LX/CdS;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1922678
    const-class v0, Lcom/facebook/places/suggestions/PlaceRowView;

    const-string v1, "places_recommendations"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/places/suggestions/PlaceRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1922676
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/places/suggestions/PlaceRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1922677
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1922674
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/places/suggestions/PlaceRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1922675
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1922665
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1922666
    const v0, 0x7f030f7c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1922667
    const v0, 0x7f0d094c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1922668
    const v0, 0x7f0d162b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->c:Landroid/widget/TextView;

    .line 1922669
    const v0, 0x7f0d2563

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->d:Landroid/widget/TextView;

    .line 1922670
    const v0, 0x7f0d1d0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->e:Landroid/widget/TextView;

    .line 1922671
    const v0, 0x7f0d0b20

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ToggleButton;

    iput-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->f:Landroid/widget/ToggleButton;

    .line 1922672
    const v0, 0x7f0d2561

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->g:Landroid/widget/ImageView;

    .line 1922673
    return-void
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 1922662
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1922663
    return-void

    .line 1922664
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 1922656
    if-nez p1, :cond_0

    .line 1922657
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->g:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1922658
    :goto_0
    return-void

    .line 1922659
    :cond_0
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    iget-object v0, v0, LX/CdS;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->n()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;->EVENT:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-eq v0, v1, :cond_1

    .line 1922660
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1922661
    :cond_1
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->g:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1922626
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    iget-object v1, v0, LX/CdS;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1922627
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->d()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1922628
    :goto_0
    iget-object v2, p0, Lcom/facebook/places/suggestions/PlaceRowView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/places/suggestions/PlaceRowView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1922629
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {p0}, Lcom/facebook/places/suggestions/PlaceRowView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f021480

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1922630
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->c:Landroid/widget/TextView;

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1922631
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\n"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1922632
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1922633
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1922634
    :goto_1
    return-void

    .line 1922635
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->d()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$CategoryIconModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 1922636
    :cond_1
    iget-object v1, p0, Lcom/facebook/places/suggestions/PlaceRowView;->d:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1922637
    iget-object v1, p0, Lcom/facebook/places/suggestions/PlaceRowView;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1922648
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    .line 1922649
    iget-boolean v1, v0, LX/CdS;->e:Z

    move v1, v1

    .line 1922650
    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, LX/CdS;->a(Z)V

    .line 1922651
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->f:Landroid/widget/ToggleButton;

    iget-object v1, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    .line 1922652
    iget-boolean p0, v1, LX/CdS;->e:Z

    move v1, p0

    .line 1922653
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 1922654
    return-void

    .line 1922655
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getInfo()LX/CdS;
    .locals 1

    .prologue
    .line 1922647
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    return-object v0
.end method

.method public setInfo(LX/CdS;)V
    .locals 2

    .prologue
    .line 1922638
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CdS;

    iput-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    .line 1922639
    invoke-direct {p0}, Lcom/facebook/places/suggestions/PlaceRowView;->b()V

    .line 1922640
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->f:Landroid/widget/ToggleButton;

    iget-object v1, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    iget-boolean v1, v1, LX/CdS;->b:Z

    invoke-static {v0, v1}, Lcom/facebook/places/suggestions/PlaceRowView;->a(Landroid/view/View;Z)V

    .line 1922641
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    iget-boolean v0, v0, LX/CdS;->c:Z

    invoke-direct {p0, v0}, Lcom/facebook/places/suggestions/PlaceRowView;->a(Z)V

    .line 1922642
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    iget-boolean v1, v1, LX/CdS;->d:Z

    invoke-static {v0, v1}, Lcom/facebook/places/suggestions/PlaceRowView;->a(Landroid/view/View;Z)V

    .line 1922643
    iget-object v0, p0, Lcom/facebook/places/suggestions/PlaceRowView;->f:Landroid/widget/ToggleButton;

    iget-object v1, p0, Lcom/facebook/places/suggestions/PlaceRowView;->h:LX/CdS;

    .line 1922644
    iget-boolean p0, v1, LX/CdS;->e:Z

    move v1, p0

    .line 1922645
    invoke-virtual {v0, v1}, Landroid/widget/ToggleButton;->setChecked(Z)V

    .line 1922646
    return-void
.end method
