.class public Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/10X;


# instance fields
.field private p:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

.field private q:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1922396
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1922397
    const v0, 0x7f030a79

    invoke-virtual {p0, v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->setContentView(I)V

    .line 1922398
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1922399
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->q:LX/0h5;

    .line 1922400
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->q:LX/0h5;

    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f080031

    invoke-virtual {p0, v2}, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1922401
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1922402
    move-object v1, v1

    .line 1922403
    const/4 v2, 0x0

    .line 1922404
    iput-boolean v2, v1, LX/108;->d:Z

    .line 1922405
    move-object v1, v1

    .line 1922406
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1922407
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->q:LX/0h5;

    new-instance v1, LX/CdL;

    invoke-direct {v1, p0}, LX/CdL;-><init>(Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1922408
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1922409
    invoke-virtual {p0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "duplicate_place"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1922410
    invoke-virtual {p0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_place_list"

    invoke-static {v1, v2}, LX/4By;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 1922411
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 1922412
    const v3, 0x7f0d1aa7

    invoke-virtual {v1, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    iput-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->p:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    .line 1922413
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->p:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    new-instance v3, LX/CdN;

    invoke-direct {v3, p0}, LX/CdN;-><init>(Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;)V

    invoke-virtual {v1, v3}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a(LX/CdM;)V

    .line 1922414
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->p:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    iget-object v3, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->q:LX/0h5;

    .line 1922415
    iput-object v3, v1, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->k:LX/0h5;

    .line 1922416
    iget-object v1, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->p:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)V

    .line 1922417
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->p:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    invoke-virtual {v0, v2}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->a(LX/0Px;)V

    .line 1922418
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->p:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    invoke-virtual {p0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "entry_point"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1922419
    iput-object v1, v0, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->e:Ljava/lang/String;

    .line 1922420
    return-void
.end method

.method public static m(Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;)V
    .locals 1

    .prologue
    .line 1922421
    iget-object v0, p0, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->p:Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;

    invoke-virtual {v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesFragment;->b()V

    .line 1922422
    return-void
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1922423
    const v0, 0x7f082943

    invoke-virtual {p0, v0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1922424
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1922425
    invoke-direct {p0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->a()V

    .line 1922426
    invoke-direct {p0}, Lcom/facebook/places/suggestions/MarkAsDuplicatesActivity;->l()V

    .line 1922427
    return-void
.end method
