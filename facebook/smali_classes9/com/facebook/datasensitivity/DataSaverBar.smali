.class public Lcom/facebook/datasensitivity/DataSaverBar;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final g:LX/0wT;


# instance fields
.field public a:Lcom/facebook/fig/button/FigButton;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field public c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:LX/0wd;

.field public e:LX/13Z;

.field public f:LX/3GW;

.field private h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bhc;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/BhR;

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1809245
    const-wide v0, 0x407561999999999aL    # 342.1

    const-wide v2, 0x4042770a3d70a3d7L    # 36.93

    invoke-static {v0, v1, v2, v3}, LX/0wT;->a(DD)LX/0wT;

    move-result-object v0

    sput-object v0, Lcom/facebook/datasensitivity/DataSaverBar;->g:LX/0wT;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1809234
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1809235
    sget-object v0, LX/13Z;->DSM_OFF:LX/13Z;

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->e:LX/13Z;

    .line 1809236
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809237
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->h:LX/0Ot;

    .line 1809238
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809239
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->i:LX/0Ot;

    .line 1809240
    new-instance v0, LX/BhR;

    invoke-direct {v0, p0}, LX/BhR;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    .line 1809241
    new-instance v0, LX/BhN;

    invoke-direct {v0, p0}, LX/BhN;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->k:Landroid/view/View$OnClickListener;

    .line 1809242
    new-instance v0, LX/BhO;

    invoke-direct {v0, p0}, LX/BhO;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->l:Landroid/view/View$OnClickListener;

    .line 1809243
    invoke-direct {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->b()V

    .line 1809244
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1809246
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1809247
    sget-object v0, LX/13Z;->DSM_OFF:LX/13Z;

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->e:LX/13Z;

    .line 1809248
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809249
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->h:LX/0Ot;

    .line 1809250
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809251
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->i:LX/0Ot;

    .line 1809252
    new-instance v0, LX/BhR;

    invoke-direct {v0, p0}, LX/BhR;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    .line 1809253
    new-instance v0, LX/BhN;

    invoke-direct {v0, p0}, LX/BhN;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->k:Landroid/view/View$OnClickListener;

    .line 1809254
    new-instance v0, LX/BhO;

    invoke-direct {v0, p0}, LX/BhO;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->l:Landroid/view/View$OnClickListener;

    .line 1809255
    invoke-direct {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->b()V

    .line 1809256
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1809257
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1809258
    sget-object v0, LX/13Z;->DSM_OFF:LX/13Z;

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->e:LX/13Z;

    .line 1809259
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809260
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->h:LX/0Ot;

    .line 1809261
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809262
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->i:LX/0Ot;

    .line 1809263
    new-instance v0, LX/BhR;

    invoke-direct {v0, p0}, LX/BhR;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    .line 1809264
    new-instance v0, LX/BhN;

    invoke-direct {v0, p0}, LX/BhN;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->k:Landroid/view/View$OnClickListener;

    .line 1809265
    new-instance v0, LX/BhO;

    invoke-direct {v0, p0}, LX/BhO;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;)V

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->l:Landroid/view/View$OnClickListener;

    .line 1809266
    invoke-direct {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->b()V

    .line 1809267
    return-void
.end method

.method private a(IIIILX/13Z;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xc8

    .line 1809268
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bhc;

    invoke-virtual {v0}, LX/Bhc;->b()J

    move-result-wide v2

    .line 1809269
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    invoke-virtual {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, LX/BhR;->a:I

    .line 1809270
    invoke-virtual {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1809271
    instance-of v1, v0, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v1, :cond_0

    .line 1809272
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    iput v0, v1, LX/BhR;->b:I

    .line 1809273
    :goto_0
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->a:Lcom/facebook/fig/button/FigButton;

    iput-object v1, v0, LX/BhR;->c:Landroid/widget/Button;

    .line 1809274
    invoke-static {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getWifiConnectedSpring(Lcom/facebook/datasensitivity/DataSaverBar;)LX/0wd;

    move-result-object v0

    const-wide v4, 0x406fe00000000000L    # 255.0

    invoke-virtual {v0, v4, v5}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, LX/0wd;->b(D)LX/0wd;

    .line 1809275
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, Lcom/facebook/datasensitivity/DataSaverBar$3;

    invoke-direct {v1, p0, p5, p1}, Lcom/facebook/datasensitivity/DataSaverBar$3;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;LX/13Z;I)V

    invoke-virtual {v0, v1, v6, v7}, Lcom/facebook/resources/ui/FbTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1809276
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, Lcom/facebook/datasensitivity/DataSaverBar$4;

    invoke-direct {v1, p0, p5}, Lcom/facebook/datasensitivity/DataSaverBar$4;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;LX/13Z;)V

    add-long v4, v2, v6

    invoke-virtual {v0, v1, v4, v5}, Lcom/facebook/resources/ui/FbTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1809277
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, Lcom/facebook/datasensitivity/DataSaverBar$5;

    invoke-direct {v1, p0, p5, p2, p3}, Lcom/facebook/datasensitivity/DataSaverBar$5;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;LX/13Z;II)V

    const-wide/16 v4, 0x190

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/resources/ui/FbTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1809278
    return-void

    .line 1809279
    :cond_0
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    iget v1, v1, LX/BhR;->a:I

    iput v1, v0, LX/BhR;->b:I

    goto :goto_0
.end method

.method private static a(Lcom/facebook/datasensitivity/DataSaverBar;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/datasensitivity/DataSaverBar;",
            "LX/0Ot",
            "<",
            "LX/Bhc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0wW;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1809280
    iput-object p1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->h:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/datasensitivity/DataSaverBar;->i:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/datasensitivity/DataSaverBar;

    const/16 v1, 0x1a6f

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x11fa

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->a(Lcom/facebook/datasensitivity/DataSaverBar;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/datasensitivity/DataSaverBar;IZ)V
    .locals 3

    .prologue
    .line 1809227
    invoke-virtual {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getHeight()I

    move-result v0

    .line 1809228
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1809229
    if-eqz p2, :cond_0

    .line 1809230
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->c:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1809231
    :goto_0
    invoke-direct {p0, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->c(I)V

    .line 1809232
    return-void

    .line 1809233
    :cond_0
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->c:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 1809214
    const-class v0, Lcom/facebook/datasensitivity/DataSaverBar;

    invoke-static {v0, p0}, Lcom/facebook/datasensitivity/DataSaverBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1809215
    const v0, 0x7f0303ce

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1809216
    const v0, 0x7f0a033c

    invoke-direct {p0, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->setColorResourceAndNotify(I)V

    .line 1809217
    const v0, 0x7f0d0764

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->a:Lcom/facebook/fig/button/FigButton;

    .line 1809218
    const v0, 0x7f0d0be5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1809219
    const v0, 0x7f0d0be6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1809220
    invoke-virtual {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0097

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 1809221
    const/16 v1, 0x99

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v2

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    invoke-static {v1, v2, v3, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 1809222
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1809223
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->a:Lcom/facebook/fig/button/FigButton;

    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809224
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809225
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->l:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809226
    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 1809211
    invoke-virtual {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1809212
    new-instance v1, LX/BhP;

    invoke-direct {v1, p0, p1}, LX/BhP;-><init>(Lcom/facebook/datasensitivity/DataSaverBar;I)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1809213
    return-void
.end method

.method public static getWifiConnectedSpring(Lcom/facebook/datasensitivity/DataSaverBar;)LX/0wd;
    .locals 4

    .prologue
    .line 1809208
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->d:LX/0wd;

    if-nez v0, :cond_0

    .line 1809209
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    sget-object v1, Lcom/facebook/datasensitivity/DataSaverBar;->g:LX/0wT;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, LX/0wd;->a(D)LX/0wd;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0xi;)LX/0wd;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->d:LX/0wd;

    .line 1809210
    :cond_0
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->d:LX/0wd;

    return-object v0
.end method

.method public static setColorAndNotify(Lcom/facebook/datasensitivity/DataSaverBar;I)V
    .locals 1

    .prologue
    .line 1809204
    invoke-virtual {p0, p1}, Lcom/facebook/datasensitivity/DataSaverBar;->setBackgroundColor(I)V

    .line 1809205
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->f:LX/3GW;

    if-eqz v0, :cond_0

    .line 1809206
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->f:LX/3GW;

    invoke-interface {v0}, LX/3GW;->c()V

    .line 1809207
    :cond_0
    return-void
.end method

.method private setColorResourceAndNotify(I)V
    .locals 1

    .prologue
    .line 1809202
    invoke-virtual {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->setColorAndNotify(Lcom/facebook/datasensitivity/DataSaverBar;I)V

    .line 1809203
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1809196
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->d:LX/0wd;

    if-eqz v0, :cond_0

    .line 1809197
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->d:LX/0wd;

    invoke-virtual {v0}, LX/0wd;->a()V

    .line 1809198
    iput-object v1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->d:LX/0wd;

    .line 1809199
    :cond_0
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->a:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809200
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809201
    return-void
.end method

.method public setIndicatorState(LX/13Z;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 1809165
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->e:LX/13Z;

    .line 1809166
    iput-object p1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->e:LX/13Z;

    .line 1809167
    sget-object v1, LX/BhQ;->a:[I

    invoke-virtual {p1}, LX/13Z;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1809168
    invoke-static {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getWifiConnectedSpring(Lcom/facebook/datasensitivity/DataSaverBar;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1809169
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    invoke-virtual {v0}, LX/BhR;->a()V

    .line 1809170
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->setVisibility(I)V

    .line 1809171
    :goto_0
    return-void

    .line 1809172
    :pswitch_0
    sget-object v1, LX/13Z;->DSM_INDICATOR_ENABLED_OFF_STATE:LX/13Z;

    if-ne v0, v1, :cond_1

    .line 1809173
    const v1, 0x7f080a11

    const v2, 0x7f080a0e

    const v3, 0x7f080a17

    const v4, 0x7f0a033c

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/datasensitivity/DataSaverBar;->a(IIIILX/13Z;)V

    .line 1809174
    :cond_0
    :goto_1
    invoke-virtual {p0, v6}, Lcom/facebook/datasensitivity/DataSaverBar;->setVisibility(I)V

    goto :goto_0

    .line 1809175
    :cond_1
    if-eq p1, v0, :cond_0

    .line 1809176
    invoke-static {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getWifiConnectedSpring(Lcom/facebook/datasensitivity/DataSaverBar;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1809177
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    invoke-virtual {v0}, LX/BhR;->a()V

    .line 1809178
    const v0, 0x7f080a0e

    invoke-static {p0, v0, v3}, Lcom/facebook/datasensitivity/DataSaverBar;->a$redex0(Lcom/facebook/datasensitivity/DataSaverBar;IZ)V

    .line 1809179
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->a:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f080a17

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1809180
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->a:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v6}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1809181
    const v0, 0x7f0a033c

    invoke-direct {p0, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->setColorResourceAndNotify(I)V

    goto :goto_1

    .line 1809182
    :pswitch_1
    if-eq v0, p1, :cond_2

    .line 1809183
    invoke-static {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getWifiConnectedSpring(Lcom/facebook/datasensitivity/DataSaverBar;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1809184
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    invoke-virtual {v0}, LX/BhR;->a()V

    .line 1809185
    const v1, 0x7f080a11

    const v2, 0x7f080a12

    const v3, 0x7f080a18

    const v4, 0x7f0a033e

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/facebook/datasensitivity/DataSaverBar;->a(IIIILX/13Z;)V

    .line 1809186
    :cond_2
    invoke-virtual {p0, v6}, Lcom/facebook/datasensitivity/DataSaverBar;->setVisibility(I)V

    goto :goto_0

    .line 1809187
    :pswitch_2
    invoke-static {p0}, Lcom/facebook/datasensitivity/DataSaverBar;->getWifiConnectedSpring(Lcom/facebook/datasensitivity/DataSaverBar;)LX/0wd;

    move-result-object v0

    invoke-virtual {v0}, LX/0wd;->j()LX/0wd;

    .line 1809188
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->j:LX/BhR;

    invoke-virtual {v0}, LX/BhR;->a()V

    .line 1809189
    const v0, 0x7f080a12

    invoke-static {p0, v0, v3}, Lcom/facebook/datasensitivity/DataSaverBar;->a$redex0(Lcom/facebook/datasensitivity/DataSaverBar;IZ)V

    .line 1809190
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->a:Lcom/facebook/fig/button/FigButton;

    const v1, 0x7f080a18

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1809191
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSaverBar;->a:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, v6}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1809192
    const v0, 0x7f0a033f

    invoke-direct {p0, v0}, Lcom/facebook/datasensitivity/DataSaverBar;->setColorResourceAndNotify(I)V

    .line 1809193
    invoke-virtual {p0, v6}, Lcom/facebook/datasensitivity/DataSaverBar;->setVisibility(I)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setListener(LX/3GW;)V
    .locals 0

    .prologue
    .line 1809194
    iput-object p1, p0, Lcom/facebook/datasensitivity/DataSaverBar;->f:LX/3GW;

    .line 1809195
    return-void
.end method
