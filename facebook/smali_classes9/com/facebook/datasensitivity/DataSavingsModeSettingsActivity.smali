.class public Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CIm;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13h;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0tK;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bhc;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/preference/PreferenceScreen;

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1809430
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    .line 1809431
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809432
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    .line 1809433
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809434
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->b:LX/0Ot;

    .line 1809435
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809436
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    .line 1809437
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809438
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->d:LX/0Ot;

    .line 1809439
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809440
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->e:LX/0Ot;

    .line 1809441
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1809442
    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    return-void
.end method

.method private a(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 1809416
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1809417
    invoke-direct {p0, p1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->b(Landroid/preference/PreferenceGroup;)V

    .line 1809418
    :cond_0
    :goto_0
    return-void

    .line 1809419
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c(Landroid/preference/PreferenceGroup;)V

    .line 1809420
    invoke-direct {p0, p1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->e(Landroid/preference/PreferenceGroup;)V

    .line 1809421
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1809422
    invoke-direct {p0, p1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f(Landroid/preference/PreferenceGroup;)V

    .line 1809423
    :cond_2
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809424
    invoke-direct {p0, p1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->d(Landroid/preference/PreferenceGroup;)V

    .line 1809425
    sget-object v0, LX/2nr;->b:LX/0Tn;

    invoke-direct {p0, p1, v0}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a(Landroid/preference/PreferenceGroup;LX/0Tn;)V

    goto :goto_0
.end method

.method private a(Landroid/preference/PreferenceGroup;LX/0Tn;)V
    .locals 4

    .prologue
    .line 1809400
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIm;

    sget-object v1, LX/2nr;->c:LX/0Tn;

    const v2, 0x7f080a05

    const/4 v3, 0x1

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v0

    .line 1809401
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 1809402
    if-eqz p2, :cond_0

    .line 1809403
    invoke-virtual {p2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4oi;->setDependency(Ljava/lang/String;)V

    .line 1809404
    :cond_0
    new-instance v1, LX/BhY;

    invoke-direct {v1, p0}, LX/BhY;-><init>(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;)V

    invoke-virtual {v0, v1}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1809405
    const v1, 0x7f080a06

    invoke-virtual {v0, v1}, LX/4oi;->setSummary(I)V

    .line 1809406
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1809443
    const v0, 0x7f0d0ce7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    .line 1809444
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bhc;

    invoke-virtual {v1}, LX/Bhc;->a()J

    move-result-wide v2

    .line 1809445
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tK;

    invoke-virtual {v1}, LX/0tK;->q()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v4, LX/0wm;->M:S

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 1809446
    :goto_0
    const-wide/16 v4, 0x1e

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    if-eqz v1, :cond_1

    .line 1809447
    :cond_0
    const v1, 0x7f020409

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setImageResource(I)V

    .line 1809448
    :cond_1
    return-void

    .line 1809449
    :cond_2
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v4, LX/0wm;->s:S

    invoke-interface {v1, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    goto :goto_0
.end method

.method private static a(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;",
            "LX/0Ot",
            "<",
            "LX/CIm;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/13h;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0tK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/03R;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Bhc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1809450
    iput-object p1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    iput-object p2, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->b:LX/0Ot;

    iput-object p3, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;

    const/16 v1, 0x25c3

    invoke-static {v6, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x494

    invoke-static {v6, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x497

    invoke-static {v6, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2fd

    invoke-static {v6, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1a6f

    invoke-static {v6, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v7, 0x1032

    invoke-static {v6, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 1809451
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809452
    invoke-direct {p0, p1, p2}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c(Landroid/view/View;Landroid/view/ViewGroup;)V

    .line 1809453
    :goto_0
    return-void

    .line 1809454
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->b(Landroid/view/View;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method private b(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 1809455
    new-instance v0, LX/BhS;

    invoke-direct {v0, p0, p0}, LX/BhS;-><init>(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;Landroid/content/Context;)V

    .line 1809456
    const v1, 0x7f030442

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 1809457
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 1809458
    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1809459
    const v0, 0x7f0d0ce9

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1809460
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bhc;

    invoke-virtual {v1}, LX/Bhc;->a()J

    move-result-wide v2

    .line 1809461
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1809462
    const v1, 0x7f080a1c

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {p0, v1, v4}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1809463
    :goto_0
    return-void

    .line 1809464
    :cond_0
    const v1, 0x7f080a02

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 1809465
    const v0, 0x7f0d0ce2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1809466
    const v1, 0x7f0d0ce6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1809467
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1809468
    invoke-direct {p0, p1, p2}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->d(Landroid/view/View;Landroid/view/ViewGroup;)V

    .line 1809469
    invoke-direct {p0, p1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->b(Landroid/view/View;)V

    .line 1809470
    invoke-direct {p0, p1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a(Landroid/view/View;)V

    .line 1809471
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1809472
    return-void
.end method

.method private c(Landroid/preference/PreferenceGroup;)V
    .locals 4

    .prologue
    .line 1809473
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIm;

    sget-object v1, LX/2nr;->b:LX/0Tn;

    const v2, 0x7f0809fe

    const/4 v3, 0x0

    invoke-virtual {v0, p0, v1, v2, v3}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;LX/0Tn;IZ)LX/4oi;

    move-result-object v1

    .line 1809474
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v2, LX/0wm;->G:C

    const-string v3, ""

    invoke-interface {v0, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1809475
    :goto_0
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1809476
    const v0, 0x7f080a02

    invoke-virtual {v1, v0}, LX/4oi;->setSummary(I)V

    .line 1809477
    :goto_1
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 1809478
    new-instance v0, LX/BhX;

    invoke-direct {v0, p0}, LX/BhX;-><init>(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;)V

    invoke-virtual {v1, v0}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1809479
    return-void

    .line 1809480
    :cond_0
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v2, LX/0wm;->m:C

    const-string v3, ""

    invoke-interface {v0, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1809481
    :cond_1
    invoke-virtual {v1, v0}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private c(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1809426
    const v0, 0x7f0d0ce5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 1809427
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tK;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0tK;->c(Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionState(Z)V

    .line 1809428
    new-instance v1, LX/BhW;

    invoke-direct {v1, p0, v0}, LX/BhW;-><init>(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;Lcom/facebook/fig/listitem/FigListItem;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809429
    return-void
.end method

.method private c(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 3

    .prologue
    .line 1809331
    const v0, 0x7f0d0ce2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1809332
    const v1, 0x7f0d0ce6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1809333
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1809334
    invoke-direct {p0, p1, p2}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->e(Landroid/view/View;Landroid/view/ViewGroup;)V

    .line 1809335
    invoke-direct {p0, p1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c(Landroid/view/View;)V

    .line 1809336
    invoke-direct {p0, p1, p2}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->d(Landroid/view/View;Landroid/view/ViewGroup;)V

    .line 1809337
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1809338
    return-void
.end method

.method private d(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 1809339
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1809340
    const v1, 0x7f0309f6

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setLayoutResource(I)V

    .line 1809341
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 1809342
    return-void
.end method

.method private d(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 1809343
    const v0, 0x7f0d0cea

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 1809344
    iget-object v1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0tK;

    invoke-virtual {v1}, LX/0tK;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1809345
    const v1, 0x7f080a1a

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1809346
    const/16 v1, 0x108

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1809347
    :goto_0
    new-instance v1, LX/BhV;

    invoke-direct {v1, p0, p1, p2}, LX/BhV;-><init>(Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;Landroid/view/View;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809348
    return-void

    .line 1809349
    :cond_0
    const v1, 0x7f080a19

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setText(I)V

    .line 1809350
    const/16 v1, 0x18

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    goto :goto_0
.end method

.method private e(Landroid/preference/PreferenceGroup;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1809351
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/0wm;->v:I

    invoke-interface {v0, v1, v6}, LX/0ad;->a(II)I

    move-result v0

    .line 1809352
    :goto_0
    const/4 v1, 0x0

    .line 1809353
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    .line 1809354
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bhc;

    invoke-virtual {v0}, LX/Bhc;->a()J

    move-result-wide v2

    .line 1809355
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_4

    .line 1809356
    const v0, 0x7f080a15

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1809357
    :goto_1
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1809358
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1809359
    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1809360
    invoke-virtual {p1, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 1809361
    :cond_0
    return-void

    .line 1809362
    :cond_1
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/0wm;->b:I

    invoke-interface {v0, v1, v6}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    .line 1809363
    :cond_2
    if-ne v0, v7, :cond_4

    .line 1809364
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v1, LX/0wm;->w:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v1, LX/0wm;->c:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method private e(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1809365
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/0wm;->v:I

    invoke-interface {v0, v1, v6}, LX/0ad;->a(II)I

    move-result v0

    .line 1809366
    :goto_0
    const/4 v1, 0x0

    .line 1809367
    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    .line 1809368
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Bhc;

    invoke-virtual {v0}, LX/Bhc;->a()J

    move-result-wide v2

    .line 1809369
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_5

    .line 1809370
    const v0, 0x7f080a15

    new-array v1, v7, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 1809371
    :cond_0
    :goto_2
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1809372
    const v0, 0x7f0d0ce4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1809373
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1809374
    invoke-virtual {v0, v6}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1809375
    const v0, 0x7f0d0ce3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1809376
    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1809377
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0402

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0401

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0403

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1809378
    :cond_1
    return-void

    .line 1809379
    :cond_2
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget v1, LX/0wm;->b:I

    invoke-interface {v0, v1, v6}, LX/0ad;->a(II)I

    move-result v0

    goto :goto_0

    .line 1809380
    :cond_3
    if-ne v0, v7, :cond_0

    .line 1809381
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v1, LX/0wm;->w:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    move-object v1, v0

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v1, LX/0wm;->c:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_5
    move-object v0, v1

    goto/16 :goto_1
.end method

.method private f(Landroid/preference/PreferenceGroup;)V
    .locals 2

    .prologue
    .line 1809382
    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1809383
    const-string v1, "FAQ @ https://fburl.com/DSM-FAQ"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 1809384
    invoke-virtual {p1, v0}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    .line 1809385
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1809386
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->a(Landroid/os/Bundle;)V

    .line 1809387
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->requestWindowFeature(I)Z

    .line 1809388
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1809389
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1809390
    invoke-virtual {p0}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->g:Landroid/preference/PreferenceScreen;

    .line 1809391
    invoke-static {p0, p0}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1809392
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->g:Landroid/preference/PreferenceScreen;

    invoke-virtual {p0, v0}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1809393
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIm;

    invoke-virtual {v0, p0}, LX/CIm;->a(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 1809394
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->g:Landroid/preference/PreferenceScreen;

    invoke-direct {p0, v0}, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a(Landroid/preference/PreferenceGroup;)V

    .line 1809395
    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2ac34f23

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1809396
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onPause()V

    .line 1809397
    iget-boolean v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->h:Z

    if-eqz v0, :cond_0

    .line 1809398
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->e()V

    .line 1809399
    :cond_0
    const/16 v0, 0x23

    const v2, 0x1534315a

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x22a7e077

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1809407
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onResume()V

    .line 1809408
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->h:Z

    .line 1809409
    const/16 v1, 0x23

    const v2, -0x371330c7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, -0x727076c8

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1809410
    invoke-super {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;->onStart()V

    .line 1809411
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIm;

    invoke-virtual {v0, p0}, LX/CIm;->b(Lcom/facebook/base/activity/FbPreferenceActivity;)V

    .line 1809412
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0tK;

    invoke-virtual {v0}, LX/0tK;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1809413
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIm;

    const v2, 0x7f0809ff

    invoke-virtual {v0, v2}, LX/CIm;->a(I)V

    .line 1809414
    :goto_0
    const v0, 0x1413e85d

    invoke-static {v0, v1}, LX/02F;->c(II)V

    return-void

    .line 1809415
    :cond_0
    iget-object v0, p0, Lcom/facebook/datasensitivity/DataSavingsModeSettingsActivity;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CIm;

    const v2, 0x7f0809fe

    invoke-virtual {v0, v2}, LX/CIm;->a(I)V

    goto :goto_0
.end method
