.class public final Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BVK;


# direct methods
.method public constructor <init>(LX/BVK;)V
    .locals 0

    .prologue
    .line 1790546
    iput-object p1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1790547
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v0, v0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790548
    :try_start_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v0, v0, LX/BVK;->j:LX/BET;

    const/4 v1, 0x0

    .line 1790549
    const/16 v2, 0x10

    invoke-virtual {v0, v2}, LX/0eW;->a(I)I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v3, v0, LX/0eW;->b:Ljava/nio/ByteBuffer;

    iget v4, v0, LX/0eW;->a:I

    add-int/2addr v2, v4

    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    move v0, v1

    .line 1790550
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v0, v0, LX/BVK;->j:LX/BET;

    invoke-virtual {v0}, LX/BET;->d()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v1, v1, LX/BVK;->j:LX/BET;

    invoke-virtual {v1}, LX/BET;->c()I

    move-result v1

    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    .line 1790551
    :goto_0
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v1, v1, LX/BVK;->d:Lcom/facebook/facedetection/detector/MacerFaceDetector;

    iget-object v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v2, v2, LX/BVK;->k:Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v3, v3, LX/BVK;->j:LX/BET;

    invoke-virtual {v3}, LX/BET;->a()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v4, v4, LX/BVK;->j:LX/BET;

    invoke-virtual {v4}, LX/BET;->b()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a(Ljava/nio/ByteBuffer;III)[B

    move-result-object v0

    .line 1790552
    if-eqz v0, :cond_1

    .line 1790553
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    invoke-static {v1}, LX/BVK;->a(LX/BVK;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;->setFaceDescriptors([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790554
    :cond_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v0, v0, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790555
    return-void

    .line 1790556
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v0, v0, LX/BVK;->j:LX/BET;

    invoke-virtual {v0}, LX/BET;->d()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v1, v1, LX/BVK;->j:LX/BET;

    invoke-virtual {v1}, LX/BET;->c()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1790557
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$2;->a:LX/BVK;

    iget-object v1, v1, LX/BVK;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
