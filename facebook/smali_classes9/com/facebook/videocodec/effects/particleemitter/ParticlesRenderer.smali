.class public Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/61B;
.implements LX/61G;
.implements LX/6Jv;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field public static final b:[LX/7Sc;


# instance fields
.field private final c:LX/BVI;

.field private final d:LX/7SX;

.field private final e:LX/7SW;

.field private final f:LX/BVK;

.field public final g:LX/BVE;

.field private final h:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "LX/BVF;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0UE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0UE",
            "<",
            "LX/7SU;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Ljava/util/concurrent/locks/Lock;

.field private final k:Ljava/util/concurrent/locks/Lock;

.field public l:LX/6Js;

.field private m:LX/5Pc;

.field private n:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "LX/5Pb;",
            ">;"
        }
    .end annotation
.end field

.field public o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/BV9;",
            ">;"
        }
    .end annotation
.end field

.field private p:I

.field public q:I

.field public r:I

.field public s:I

.field public t:I

.field public u:LX/03R;

.field private v:I

.field private w:I

.field public x:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1790854
    const-class v0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1790855
    const/4 v0, 0x4

    new-array v0, v0, [LX/7Sc;

    const/4 v1, 0x0

    sget-object v2, LX/7Sc;->INPUT_PREVIEW:LX/7Sc;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/7Sc;->INPUT_PREVIEW_SIZE:LX/7Sc;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/7Sc;->INPUT_FACING:LX/7Sc;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/7Sc;->INPUT_ROTATION:LX/7Sc;

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->b:[LX/7Sc;

    return-void
.end method

.method public constructor <init>(LX/BVI;LX/7SX;LX/7SW;LX/BVK;LX/BVE;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 1790838
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790839
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h:LX/0UE;

    .line 1790840
    new-instance v0, LX/0UE;

    invoke-direct {v0}, LX/0UE;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    .line 1790841
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    .line 1790842
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    .line 1790843
    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->q:I

    .line 1790844
    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->r:I

    .line 1790845
    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->s:I

    .line 1790846
    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->t:I

    .line 1790847
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->u:LX/03R;

    .line 1790848
    iput-object p1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->c:LX/BVI;

    .line 1790849
    iput-object p2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->d:LX/7SX;

    .line 1790850
    iput-object p3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->e:LX/7SW;

    .line 1790851
    iput-object p4, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->f:LX/BVK;

    .line 1790852
    iput-object p5, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->g:LX/BVE;

    .line 1790853
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;
    .locals 1

    .prologue
    .line 1790837
    invoke-static {p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->b(LX/0QB;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    move-result-object v0

    return-object v0
.end method

.method private a(LX/7Sk;)V
    .locals 3

    .prologue
    .line 1790824
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1790825
    :goto_0
    return-void

    .line 1790826
    :cond_0
    :try_start_0
    iget-object v0, p1, LX/7Sk;->a:[B

    move-object v0, v0

    .line 1790827
    if-eqz v0, :cond_2

    .line 1790828
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->f:LX/BVK;

    .line 1790829
    iget-object v1, p1, LX/7Sk;->a:[B

    move-object v1, v1

    .line 1790830
    iget v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    .line 1790831
    const/4 p1, 0x0

    invoke-static {v0, p1, v1, v2}, LX/BVK;->a(LX/BVK;Ljava/nio/ByteBuffer;[BI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790832
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 1790833
    :cond_2
    :try_start_1
    iget-object v0, p1, LX/7Sk;->b:[LX/6JH;

    move-object v0, v0

    .line 1790834
    if-eqz v0, :cond_1

    .line 1790835
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->f:LX/BVK;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    invoke-interface {v0}, LX/6JH;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    iget v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    invoke-virtual {v1, v0, v2}, LX/BVK;->a(Ljava/nio/ByteBuffer;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1790836
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;
    .locals 6

    .prologue
    .line 1790822
    new-instance v0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;

    const-class v1, LX/BVI;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/BVI;

    const-class v2, LX/7SX;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/7SX;

    invoke-static {p0}, LX/7SW;->a(LX/0QB;)LX/7SW;

    move-result-object v3

    check-cast v3, LX/7SW;

    invoke-static {p0}, LX/BVK;->a(LX/0QB;)LX/BVK;

    move-result-object v4

    check-cast v4, LX/BVK;

    invoke-static {p0}, LX/BVE;->a(LX/0QB;)LX/BVE;

    move-result-object v5

    check-cast v5, LX/BVE;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;-><init>(LX/BVI;LX/7SX;LX/7SW;LX/BVK;LX/BVE;)V

    .line 1790823
    return-object v0
.end method

.method public static g(Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V
    .locals 5

    .prologue
    .line 1790808
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790809
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790810
    :try_start_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->clear()V

    .line 1790811
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->clear()V

    .line 1790812
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->l:LX/6Js;

    if-eqz v0, :cond_0

    .line 1790813
    sget-object v1, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->b:[LX/7Sc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1790814
    iget-object v4, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->l:LX/6Js;

    invoke-virtual {v4, p0, v3}, LX/6Js;->b(LX/6Jv;LX/7Sc;)V

    .line 1790815
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1790816
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->f:LX/BVK;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/BVK;->a([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790817
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790818
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790819
    return-void

    .line 1790820
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790821
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public static h(Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V
    .locals 11

    .prologue
    const/4 v2, -0x1

    .line 1790759
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790760
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790761
    :try_start_0
    iget v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->q:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->r:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->s:I

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->t:I

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->u:LX/03R;

    sget-object v2, LX/03R;->UNSET:LX/03R;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_1

    .line 1790762
    :cond_0
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790763
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790764
    :goto_0
    return-void

    .line 1790765
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7SU;

    .line 1790766
    invoke-virtual {v1}, LX/7SU;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1790767
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790768
    iget-object v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1

    .line 1790769
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->clear()V

    .line 1790770
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->clear()V

    .line 1790771
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->o:Ljava/util/List;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-eqz v1, :cond_4

    .line 1790772
    :cond_3
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790773
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 1790774
    :cond_4
    :try_start_3
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->u:LX/03R;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v7

    .line 1790775
    iget-object v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->f:LX/BVK;

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->o:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BV9;

    iget-object v1, v1, LX/BV9;->c:LX/BVB;

    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->q:I

    iget v4, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->r:I

    invoke-virtual {v1, v3, v4}, LX/BVB;->a(II)LX/BVB;

    move-result-object v1

    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->s:I

    .line 1790776
    iput v3, v1, LX/BVB;->e:I

    .line 1790777
    move-object v1, v1

    .line 1790778
    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->t:I

    .line 1790779
    iput v3, v1, LX/BVB;->f:I

    .line 1790780
    move-object v1, v1

    .line 1790781
    iput-boolean v7, v1, LX/BVB;->g:Z

    .line 1790782
    move-object v1, v1

    .line 1790783
    invoke-virtual {v1}, LX/BVB;->a()[B

    move-result-object v1

    invoke-virtual {v2, v1}, LX/BVK;->a([B)V

    .line 1790784
    iget v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->s:I

    iget v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->t:I

    .line 1790785
    add-int/lit8 v0, v2, 0x2d

    div-int/lit8 v0, v0, 0x5a

    mul-int/lit8 v0, v0, 0x5a

    .line 1790786
    if-eqz v7, :cond_8

    .line 1790787
    add-int/2addr v0, v1

    rem-int/lit16 v0, v0, 0x168

    .line 1790788
    rsub-int v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    .line 1790789
    :goto_2
    move v8, v0

    .line 1790790
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    .line 1790791
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_3
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/BV9;

    move-object v4, v0

    .line 1790792
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->c:LX/BVI;

    iget-object v2, v4, LX/BV9;->b:LX/BVG;

    invoke-virtual {v2}, LX/BVG;->a()[B

    move-result-object v2

    iget-object v3, v4, LX/BV9;->c:LX/BVB;

    iget v5, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->q:I

    iget v6, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->r:I

    invoke-virtual {v3, v5, v6}, LX/BVB;->a(II)LX/BVB;

    move-result-object v3

    .line 1790793
    iput v8, v3, LX/BVB;->e:I

    .line 1790794
    move-object v3, v3

    .line 1790795
    iput-boolean v7, v3, LX/BVB;->g:Z

    .line 1790796
    move-object v3, v3

    .line 1790797
    invoke-virtual {v3}, LX/BVB;->a()[B

    move-result-object v3

    iget-object v5, v4, LX/BV9;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v5}, LX/BVI;->a([B[BLjava/lang/String;)LX/BVF;

    move-result-object v6

    .line 1790798
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h:LX/0UE;

    invoke-virtual {v1, v6}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 1790799
    iget v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    invoke-virtual {v6}, LX/BVF;->f()I

    move-result v2

    or-int/2addr v1, v2

    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    .line 1790800
    iget-object v10, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->d:LX/7SX;

    iget-object v2, v4, LX/BV9;->a:Landroid/net/Uri;

    iget-object v3, v4, LX/BV9;->e:LX/7SR;

    iget-object v4, v4, LX/BV9;->f:LX/7SR;

    sget-object v5, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {v1 .. v6}, LX/7SX;->a(Landroid/net/Uri;LX/7SR;LX/7SR;Lcom/facebook/common/callercontext/CallerContext;LX/BVF;)LX/7SU;

    move-result-object v1

    invoke-virtual {v10, v1}, LX/0UE;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1790801
    :cond_5
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->m:LX/5Pc;

    if-eqz v1, :cond_7

    .line 1790802
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_6
    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7SU;

    .line 1790803
    iget-object v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->m:LX/5Pc;

    invoke-virtual {v1, v3}, LX/7SU;->a(LX/5Pc;)V

    .line 1790804
    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->w:I

    if-eqz v3, :cond_6

    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->v:I

    if-eqz v3, :cond_6

    .line 1790805
    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->v:I

    iget v4, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->w:I

    invoke-virtual {v1, v3, v4}, LX/7SU;->a(II)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4

    .line 1790806
    :cond_7
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790807
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0

    :cond_8
    sub-int/2addr v0, v1

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    goto/16 :goto_2
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1790751
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790752
    :try_start_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7SU;

    .line 1790753
    invoke-virtual {v0, p1, p2}, LX/7SU;->a(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1790754
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 1790755
    :cond_0
    :try_start_1
    iput p1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->v:I

    .line 1790756
    iput p2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->w:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1790757
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790758
    return-void
.end method

.method public final a(LX/5Pc;)V
    .locals 2

    .prologue
    .line 1790856
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790857
    :try_start_0
    iput-object p1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->m:LX/5Pc;

    .line 1790858
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->n:LX/1FJ;

    if-nez v0, :cond_0

    .line 1790859
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->e:LX/7SW;

    invoke-virtual {v0}, LX/7SW;->a()LX/1FJ;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->n:LX/1FJ;

    .line 1790860
    :cond_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7SU;

    .line 1790861
    invoke-virtual {v0, p1}, LX/7SU;->a(LX/5Pc;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1790862
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790863
    return-void
.end method

.method public final a(LX/61H;)V
    .locals 0

    .prologue
    .line 1790643
    return-void
.end method

.method public final a(LX/6Js;)V
    .locals 2

    .prologue
    .line 1790644
    iput-object p1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->l:LX/6Js;

    .line 1790645
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->l:LX/6Js;

    if-eqz v0, :cond_0

    .line 1790646
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->l:LX/6Js;

    sget-object v1, LX/7Sc;->PARTICLES_CONFIG:LX/7Sc;

    invoke-virtual {v0, p0, v1}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1790647
    :cond_0
    return-void
.end method

.method public final a(LX/7Sb;)V
    .locals 4

    .prologue
    .line 1790648
    sget-object v0, LX/BVM;->a:[I

    invoke-interface {p1}, LX/7Sb;->a()LX/7Sc;

    move-result-object v1

    invoke-virtual {v1}, LX/7Sc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1790649
    :goto_0
    return-void

    .line 1790650
    :pswitch_0
    check-cast p1, LX/BVJ;

    .line 1790651
    iget-object v0, p1, LX/BVJ;->a:LX/BVA;

    move-object v0, v0

    .line 1790652
    if-eqz v0, :cond_7

    .line 1790653
    iget-object v0, p1, LX/BVJ;->a:LX/BVA;

    move-object v0, v0

    .line 1790654
    iget-object v1, v0, LX/BVA;->b:Ljava/util/List;

    move-object v0, v1

    .line 1790655
    :goto_1
    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->o:Ljava/util/List;

    .line 1790656
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->o:Ljava/util/List;

    if-eqz v0, :cond_8

    .line 1790657
    iget-object v0, p1, LX/BVJ;->a:LX/BVA;

    move-object v0, v0

    .line 1790658
    iget-object v1, v0, LX/BVA;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1790659
    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->x:Ljava/lang/String;

    .line 1790660
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->l:LX/6Js;

    if-eqz v0, :cond_0

    .line 1790661
    sget-object v1, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->b:[LX/7Sc;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1790662
    iget-object p1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->l:LX/6Js;

    invoke-virtual {p1, p0, v3}, LX/6Js;->a(LX/6Jv;LX/7Sc;)V

    .line 1790663
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1790664
    :cond_0
    invoke-static {p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h(Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V

    .line 1790665
    :goto_3
    goto :goto_0

    .line 1790666
    :pswitch_1
    check-cast p1, LX/7Sk;

    invoke-direct {p0, p1}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->a(LX/7Sk;)V

    goto :goto_0

    .line 1790667
    :pswitch_2
    check-cast p1, LX/7Sl;

    .line 1790668
    iget v0, p1, LX/7Sl;->a:I

    move v0, v0

    .line 1790669
    iget v1, p1, LX/7Sl;->b:I

    move v1, v1

    .line 1790670
    iget v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->q:I

    if-ne v0, v2, :cond_1

    iget v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->r:I

    if-eq v1, v2, :cond_2

    .line 1790671
    :cond_1
    iput v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->q:I

    .line 1790672
    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->r:I

    .line 1790673
    invoke-static {p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h(Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V

    .line 1790674
    :cond_2
    goto :goto_0

    .line 1790675
    :pswitch_3
    check-cast p1, LX/7Sj;

    .line 1790676
    iget-object v0, p1, LX/7Sj;->a:LX/7Si;

    move-object v0, v0

    .line 1790677
    sget-object v1, LX/7Si;->FRONT:LX/7Si;

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    .line 1790678
    :goto_4
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->u:LX/03R;

    invoke-virtual {v1}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->u:LX/03R;

    invoke-virtual {v1}, LX/03R;->asBoolean()Z

    move-result v1

    if-eq v1, v0, :cond_4

    .line 1790679
    :cond_3
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->u:LX/03R;

    .line 1790680
    invoke-static {p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h(Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V

    .line 1790681
    :cond_4
    goto :goto_0

    .line 1790682
    :pswitch_4
    check-cast p1, LX/7Sm;

    .line 1790683
    invoke-virtual {p1}, LX/7Sm;->d()I

    move-result v0

    .line 1790684
    iget v1, p1, LX/7Sm;->b:I

    move v1, v1

    .line 1790685
    iget v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->s:I

    if-ne v0, v2, :cond_5

    iget v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->t:I

    if-eq v1, v2, :cond_6

    .line 1790686
    :cond_5
    iput v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->s:I

    .line 1790687
    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->t:I

    .line 1790688
    invoke-static {p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h(Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V

    .line 1790689
    :cond_6
    goto/16 :goto_0

    .line 1790690
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1790691
    :cond_8
    invoke-static {p0}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->g(Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V

    goto :goto_3

    .line 1790692
    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(LX/BVA;)V
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1790693
    iget-object v0, p1, LX/BVA;->b:Ljava/util/List;

    move-object v8, v0

    .line 1790694
    if-nez v8, :cond_0

    .line 1790695
    :goto_0
    return-void

    .line 1790696
    :cond_0
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790697
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790698
    :try_start_0
    invoke-interface {v8}, Ljava/util/List;->size()I

    .line 1790699
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7SU;

    .line 1790700
    invoke-virtual {v1}, LX/7SU;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1790701
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790702
    iget-object v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v1

    .line 1790703
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->clear()V

    .line 1790704
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->clear()V

    .line 1790705
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    .line 1790706
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    move v7, v2

    .line 1790707
    :goto_2
    if-ge v7, v9, :cond_3

    .line 1790708
    invoke-interface {v8, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, LX/BV9;

    move-object v4, v0

    .line 1790709
    if-nez v7, :cond_2

    .line 1790710
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->f:LX/BVK;

    iget-object v2, v4, LX/BV9;->c:LX/BVB;

    invoke-virtual {v2}, LX/BVB;->a()[B

    move-result-object v2

    invoke-virtual {v1, v2}, LX/BVK;->a([B)V

    .line 1790711
    :cond_2
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->c:LX/BVI;

    iget-object v2, v4, LX/BV9;->b:LX/BVG;

    invoke-virtual {v2}, LX/BVG;->a()[B

    move-result-object v2

    iget-object v3, v4, LX/BV9;->c:LX/BVB;

    invoke-virtual {v3}, LX/BVB;->a()[B

    move-result-object v3

    iget-object v5, v4, LX/BV9;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v5}, LX/BVI;->a([B[BLjava/lang/String;)LX/BVF;

    move-result-object v6

    .line 1790712
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->h:LX/0UE;

    invoke-virtual {v1, v6}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 1790713
    iget v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    invoke-virtual {v6}, LX/BVF;->f()I

    move-result v2

    or-int/2addr v1, v2

    iput v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    .line 1790714
    iget-object v10, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->d:LX/7SX;

    iget-object v2, v4, LX/BV9;->a:Landroid/net/Uri;

    iget-object v3, v4, LX/BV9;->e:LX/7SR;

    iget-object v4, v4, LX/BV9;->f:LX/7SR;

    sget-object v5, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {v1 .. v6}, LX/7SX;->a(Landroid/net/Uri;LX/7SR;LX/7SR;Lcom/facebook/common/callercontext/CallerContext;LX/BVF;)LX/7SU;

    move-result-object v1

    invoke-virtual {v10, v1}, LX/0UE;->add(Ljava/lang/Object;)Z

    .line 1790715
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    goto :goto_2

    .line 1790716
    :cond_3
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v1}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7SU;

    .line 1790717
    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->w:I

    if-eqz v3, :cond_4

    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->v:I

    if-eqz v3, :cond_4

    .line 1790718
    iget v3, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->v:I

    iget v4, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->w:I

    invoke-virtual {v1, v3, v4}, LX/7SU;->a(II)V

    goto :goto_3

    .line 1790719
    :cond_5
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 1790720
    iget-object v0, p1, LX/BVA;->c:LX/BVN;

    move-object v1, v0

    .line 1790721
    if-eqz v1, :cond_6

    .line 1790722
    new-instance v2, LX/BVL;

    invoke-direct {v2, p0}, LX/BVL;-><init>(Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;)V

    .line 1790723
    iput-object v2, v1, LX/BVN;->a:LX/BVL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1790724
    :cond_6
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790725
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 1790726
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1790727
    :goto_0
    return-void

    .line 1790728
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->f:LX/BVK;

    iget v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->p:I

    invoke-virtual {v0, p1, v1}, LX/BVK;->a(Ljava/nio/ByteBuffer;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1790729
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->k:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a([F[F[FJ)V
    .locals 8

    .prologue
    .line 1790730
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->tryLock()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1790731
    :goto_0
    return-void

    .line 1790732
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7SU;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    .line 1790733
    invoke-virtual/range {v0 .. v5}, LX/7SU;->a([F[F[FJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1790734
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1790735
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790736
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->m:LX/5Pc;

    .line 1790737
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->i:LX/0UE;

    invoke-virtual {v0}, LX/0UE;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7SU;

    .line 1790738
    invoke-virtual {v0}, LX/7SU;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1790739
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 1790740
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->n:LX/1FJ;

    invoke-static {v0}, LX/1FJ;->c(LX/1FJ;)V

    .line 1790741
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->n:LX/1FJ;

    .line 1790742
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->e:LX/7SW;

    invoke-virtual {v0}, LX/7SW;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1790743
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->j:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790744
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1790745
    iget-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->o:Ljava/util/List;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1790746
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1790747
    const-string v1, "filter_type"

    const-string v2, "particle"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1790748
    iget-object v1, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->x:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1790749
    const-string v1, "filter_id"

    iget-object v2, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesRenderer;->x:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1790750
    :cond_0
    return-object v0
.end method
