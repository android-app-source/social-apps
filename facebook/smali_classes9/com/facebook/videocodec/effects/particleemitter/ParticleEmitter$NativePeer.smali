.class public final Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1790305
    const-string v0, "particleemitter"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1790306
    return-void
.end method

.method public constructor <init>([B[BLjava/lang/String;)V
    .locals 1

    .prologue
    .line 1790302
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790303
    invoke-static {p1, p2, p3}, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->initHybrid([B[BLjava/lang/String;)Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticleEmitter$NativePeer;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1790304
    return-void
.end method

.method private static native initHybrid([B[BLjava/lang/String;)Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native describeLayout()[B
.end method

.method public native getCapabilities()I
.end method

.method public native indexArray()Ljava/nio/ByteBuffer;
.end method

.method public native particlesBuffer()Ljava/nio/ByteBuffer;
.end method

.method public native setPreviewViewSize(II)V
.end method

.method public native setTextureSize(II)V
.end method

.method public native timestamp()I
.end method

.method public native update()I
.end method
