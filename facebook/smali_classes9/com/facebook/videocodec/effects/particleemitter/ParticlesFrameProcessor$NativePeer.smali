.class public final Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final mHybridData:Lcom/facebook/jni/HybridData;
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1790561
    const-string v0, "particleemitter"

    invoke-static {v0}, LX/01L;->a(Ljava/lang/String;)V

    .line 1790562
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1790558
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1790559
    invoke-static {}, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;->initHybrid()Lcom/facebook/jni/HybridData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/videocodec/effects/particleemitter/ParticlesFrameProcessor$NativePeer;->mHybridData:Lcom/facebook/jni/HybridData;

    .line 1790560
    return-void
.end method

.method private static native initHybrid()Lcom/facebook/jni/HybridData;
.end method


# virtual methods
.method public native calcOptFlow()V
.end method

.method public native configure([B)V
.end method

.method public native queueByteBuffer(Ljava/nio/ByteBuffer;F)V
.end method

.method public native queueFrame([BF)V
.end method

.method public native setFaceDescriptors([B)V
.end method
