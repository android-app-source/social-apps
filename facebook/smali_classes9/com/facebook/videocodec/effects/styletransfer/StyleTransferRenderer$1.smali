.class public final Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BVQ;


# direct methods
.method public constructor <init>(LX/BVQ;)V
    .locals 0

    .prologue
    .line 1790880
    iput-object p1, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1790881
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v0, v0, LX/BVQ;->o:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1790882
    const/4 v0, 0x0

    .line 1790883
    iget-object v1, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v1, v1, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790884
    :try_start_0
    iget-object v1, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v1, v1, LX/BVQ;->f:LX/AMG;

    if-eqz v1, :cond_1

    .line 1790885
    iget-object v0, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v0, v0, LX/BVQ;->f:LX/AMG;

    const/16 v1, 0x2d0

    const/16 v2, 0x168

    invoke-virtual {v0, v1, v2}, LX/AMG;->a(II)LX/AMF;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1790886
    :cond_1
    iget-object v1, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v1, v1, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 1790887
    if-eqz v0, :cond_0

    iget v1, v0, LX/AMF;->a:I

    if-eqz v1, :cond_0

    iget v1, v0, LX/AMF;->b:I

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/AMF;->c:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_0

    .line 1790888
    iget-object v1, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v1, v1, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 1790889
    :try_start_1
    iget-object v1, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    .line 1790890
    iput-object v0, v1, LX/BVQ;->g:LX/AMF;

    .line 1790891
    iget-object v0, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    const/4 v1, 0x1

    .line 1790892
    iput-boolean v1, v0, LX/BVQ;->h:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1790893
    iget-object v0, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v0, v0, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 1790894
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v1, v1, LX/BVQ;->e:Ljava/util/concurrent/locks/ReadWriteLock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/ReadWriteLock;->readLock()Ljava/util/concurrent/locks/Lock;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 1790895
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/facebook/videocodec/effects/styletransfer/StyleTransferRenderer$1;->a:LX/BVQ;

    iget-object v1, v1, LX/BVQ;->d:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 1790896
    :cond_2
    return-void
.end method
