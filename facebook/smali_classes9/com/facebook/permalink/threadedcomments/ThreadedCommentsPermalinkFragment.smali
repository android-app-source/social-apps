.class public Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements LX/0hF;
.implements LX/0fj;
.implements LX/0fk;
.implements LX/21l;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/base/fragment/FbFragment;",
        "LX/0fh;",
        "LX/0hF;",
        "LX/0fj;",
        "LX/0fk;",
        "LX/21l",
        "<",
        "Lcom/facebook/graphql/model/GraphQLComment;",
        ">;"
    }
.end annotation


# instance fields
.field private A:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/21l",
            "<",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;>;"
        }
    .end annotation
.end field

.field public final B:LX/1DI;

.field private C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

.field public D:LX/62O;

.field private E:Z

.field public F:Lcom/facebook/graphql/model/GraphQLComment;

.field private G:Lcom/facebook/graphql/model/GraphQLComment;

.field private H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public I:Ljava/lang/String;

.field private J:LX/1Cq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cq",
            "<",
            "LX/9Gu;",
            ">;"
        }
    .end annotation
.end field

.field private K:Lcom/facebook/graphql/model/GraphQLFeedback;

.field private L:Z

.field public a:LX/9DH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/9Ci;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1K9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/3iG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/20j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/9F3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0lC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/9FB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2yx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1ry;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0tF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/CZo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/9FH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0g8;

.field public s:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field private t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field private u:Landroid/view/ContextThemeWrapper;

.field public v:LX/9DG;

.field private w:LX/1Qq;

.field private x:LX/9Ch;

.field public y:LX/62C;

.field private z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1916859
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1916860
    new-instance v0, LX/CZv;

    invoke-direct {v0, p0}, LX/CZv;-><init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->B:LX/1DI;

    .line 1916861
    return-void
.end method

.method private a(LX/CZl;LX/9DG;)LX/1Cw;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 1916842
    new-instance v0, LX/9F2;

    invoke-direct {v0, p1, p2}, LX/9F2;-><init>(LX/9Bj;LX/9DG;)V

    .line 1916843
    move-object v2, v0

    .line 1916844
    new-instance v0, LX/1Cq;

    invoke-direct {v0}, LX/1Cq;-><init>()V

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->J:LX/1Cq;

    .line 1916845
    new-instance v0, LX/3dM;

    invoke-direct {v0}, LX/3dM;-><init>()V

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916846
    iget-object v4, v1, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    move-object v1, v4

    .line 1916847
    iput-object v1, v0, LX/3dM;->D:Ljava/lang/String;

    .line 1916848
    move-object v0, v0

    .line 1916849
    invoke-virtual {v0}, LX/3dM;->a()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->K:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1916850
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->h:LX/9FB;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 1916851
    const v5, 0x7f0e076e

    move v5, v5

    .line 1916852
    invoke-direct {v1, v4, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    new-instance v4, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment$6;

    invoke-direct {v4, p0}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment$6;-><init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    iget-object v6, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    const/4 v7, 0x1

    const/4 v8, 0x0

    move-object v5, v3

    invoke-virtual/range {v0 .. v8}, LX/9FB;->a(Landroid/content/Context;LX/9Bi;LX/9Ce;Ljava/lang/Runnable;LX/9Im;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZ)LX/9FA;

    move-result-object v0

    .line 1916853
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0, v1}, LX/9FA;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1916854
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->o:LX/1DS;

    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->n:LX/0Ot;

    iget-object v3, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->J:LX/1Cq;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 1916855
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 1916856
    move-object v0, v1

    .line 1916857
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->w:LX/1Qq;

    .line 1916858
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->w:LX/1Qq;

    return-object v0
.end method

.method private static a(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;LX/9DH;LX/9Ci;LX/1K9;LX/3iG;LX/20j;LX/9F3;LX/0lC;LX/9FB;LX/0Zb;LX/2yx;LX/1ry;LX/0tF;LX/CZo;LX/0Ot;LX/1DS;LX/0ad;LX/9FH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;",
            "LX/9DH;",
            "LX/9Ci;",
            "LX/1K9;",
            "LX/3iG;",
            "LX/20j;",
            "LX/9F3;",
            "LX/0lC;",
            "LX/9FB;",
            "LX/0Zb;",
            "LX/2yx;",
            "LX/1ry;",
            "LX/0tF;",
            "LX/CZo;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedback/ui/rows/ThreadedParentCommentGroupPartDefinition;",
            ">;",
            "LX/1DS;",
            "LX/0ad;",
            "LX/9FH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1916841
    iput-object p1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->a:LX/9DH;

    iput-object p2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->b:LX/9Ci;

    iput-object p3, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->c:LX/1K9;

    iput-object p4, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->d:LX/3iG;

    iput-object p5, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->e:LX/20j;

    iput-object p6, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->f:LX/9F3;

    iput-object p7, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->g:LX/0lC;

    iput-object p8, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->h:LX/9FB;

    iput-object p9, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->i:LX/0Zb;

    iput-object p10, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->j:LX/2yx;

    iput-object p11, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->k:LX/1ry;

    iput-object p12, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->l:LX/0tF;

    iput-object p13, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->m:LX/CZo;

    iput-object p14, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->o:LX/1DS;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->p:LX/0ad;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->q:LX/9FH;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 20

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v18

    move-object/from16 v1, p0

    check-cast v1, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;

    const-class v2, LX/9DH;

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/9DH;

    const-class v3, LX/9Ci;

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/9Ci;

    invoke-static/range {v18 .. v18}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v4

    check-cast v4, LX/1K9;

    const-class v5, LX/3iG;

    move-object/from16 v0, v18

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/3iG;

    invoke-static/range {v18 .. v18}, LX/20j;->a(LX/0QB;)LX/20j;

    move-result-object v6

    check-cast v6, LX/20j;

    const-class v7, LX/9F3;

    move-object/from16 v0, v18

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/9F3;

    invoke-static/range {v18 .. v18}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v8

    check-cast v8, LX/0lC;

    const-class v9, LX/9FB;

    move-object/from16 v0, v18

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/9FB;

    invoke-static/range {v18 .. v18}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v10

    check-cast v10, LX/0Zb;

    invoke-static/range {v18 .. v18}, LX/2yx;->a(LX/0QB;)LX/2yx;

    move-result-object v11

    check-cast v11, LX/2yx;

    invoke-static/range {v18 .. v18}, LX/1ry;->a(LX/0QB;)LX/1ry;

    move-result-object v12

    check-cast v12, LX/1ry;

    invoke-static/range {v18 .. v18}, LX/0tF;->a(LX/0QB;)LX/0tF;

    move-result-object v13

    check-cast v13, LX/0tF;

    const-class v14, LX/CZo;

    move-object/from16 v0, v18

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/CZo;

    const/16 v15, 0x1dcb

    move-object/from16 v0, v18

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v18 .. v18}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v16

    check-cast v16, LX/1DS;

    invoke-static/range {v18 .. v18}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    const-class v19, LX/9FH;

    invoke-interface/range {v18 .. v19}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v18

    check-cast v18, LX/9FH;

    invoke-static/range {v1 .. v18}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->a(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;LX/9DH;LX/9Ci;LX/1K9;LX/3iG;LX/20j;LX/9F3;LX/0lC;LX/9FB;LX/0Zb;LX/2yx;LX/1ry;LX/0tF;LX/CZo;LX/0Ot;LX/1DS;LX/0ad;LX/9FH;)V

    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    .line 1916831
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->q:LX/9FH;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1916832
    iget-wide v4, v1, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;->j:J

    move-wide v2, v4

    .line 1916833
    invoke-virtual {v0, v2, v3}, LX/9FH;->a(J)LX/9FG;

    move-result-object v0

    .line 1916834
    sget-object v1, LX/9FF;->REPLY:LX/9FF;

    invoke-virtual {v0, v1}, LX/9FG;->a(LX/9FF;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1916835
    :goto_0
    return-void

    .line 1916836
    :cond_0
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v1}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v1

    .line 1916837
    iget-wide v4, v0, LX/9FG;->b:J

    move-wide v2, v4

    .line 1916838
    iput-wide v2, v1, LX/21A;->j:J

    .line 1916839
    move-object v0, v1

    .line 1916840
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    goto :goto_0
.end method

.method public static c(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 4

    .prologue
    .line 1916814
    invoke-virtual {p0, p1}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1916815
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1916816
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v1

    const-string v2, "story_view"

    iget-object v3, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->I:Ljava/lang/String;

    .line 1916817
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "story_permalink_opened"

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "comment_id"

    invoke-virtual {v0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 1916818
    iput-object v2, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1916819
    move-object v0, v0

    .line 1916820
    invoke-static {v0, v3}, LX/2yx;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Ljava/lang/String;)V

    .line 1916821
    move-object v0, v0

    .line 1916822
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->i:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1916823
    :cond_0
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->D:LX/62O;

    invoke-virtual {v0}, LX/62O;->b()V

    .line 1916824
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->s:Lcom/facebook/widget/FbSwipeRefreshLayout;

    if-eqz v0, :cond_1

    .line 1916825
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->s:Lcom/facebook/widget/FbSwipeRefreshLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 1916826
    :cond_1
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916827
    iget-object v2, v1, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    move-object v1, v2

    .line 1916828
    invoke-virtual {v0, v1}, LX/9DG;->a(Ljava/lang/String;)V

    .line 1916829
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->E:Z

    .line 1916830
    return-void
.end method

.method public static d(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V
    .locals 14

    .prologue
    .line 1916802
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->D:LX/62O;

    invoke-virtual {v0}, LX/62O;->a()V

    .line 1916803
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->d:LX/3iG;

    sget-object v1, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v0, v1}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916804
    iget-object v2, v1, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1916805
    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916806
    iget-object v3, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1916807
    iget-object v3, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916808
    iget-object v4, v3, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    move-object v3, v4

    .line 1916809
    iget-object v4, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916810
    iget-boolean v5, v4, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->k:Z

    move v4, v5

    .line 1916811
    new-instance v5, LX/CZu;

    invoke-direct {v5, p0}, LX/CZu;-><init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    .line 1916812
    iget-object v12, v0, LX/3iK;->j:LX/1Ck;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fetch_comment_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    new-instance v6, LX/9DP;

    move-object v7, v0

    move-object v8, v1

    move-object v9, v2

    move-object v10, v3

    move v11, v4

    invoke-direct/range {v6 .. v11}, LX/9DP;-><init>(LX/3iK;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v12, v13, v6, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1916813
    return-void
.end method

.method private l()Landroid/content/Context;
    .locals 3

    .prologue
    .line 1916797
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->u:Landroid/view/ContextThemeWrapper;

    if-nez v0, :cond_0

    .line 1916798
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1916799
    const v2, 0x7f0e0770

    move v2, v2

    .line 1916800
    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->u:Landroid/view/ContextThemeWrapper;

    .line 1916801
    :cond_0
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->u:Landroid/view/ContextThemeWrapper;

    return-object v0
.end method


# virtual methods
.method public final S_()Z
    .locals 2

    .prologue
    .line 1916796
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9DG;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1916795
    const-string v0, "feedback_threaded_comments"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v6, 0x1

    const/4 v8, 0x0

    .line 1916742
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1916743
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1916744
    if-nez p1, :cond_0

    .line 1916745
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p1, v0

    .line 1916746
    :cond_0
    if-eqz p1, :cond_1

    .line 1916747
    const-string v0, "commentParams"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916748
    const-string v0, "loadingState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    .line 1916749
    const-string v0, "feedbackLoggingParams"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1916750
    :cond_1
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->p:LX/0ad;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->l:LX/0tF;

    invoke-static {v0, v1, p1}, LX/BFS;->a(LX/0ad;LX/0tF;Landroid/os/Bundle;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->L:Z

    .line 1916751
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916752
    iget-object v1, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->h:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v1

    .line 1916753
    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1916754
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916755
    iget-object v1, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->i:Lcom/facebook/graphql/model/GraphQLComment;

    move-object v0, v1

    .line 1916756
    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->G:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1916757
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-static {v0}, LX/21A;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)LX/21A;

    move-result-object v0

    const-string v1, "story_view"

    .line 1916758
    iput-object v1, v0, LX/21A;->c:Ljava/lang/String;

    .line 1916759
    move-object v0, v0

    .line 1916760
    const-string v1, "permalink_ufi"

    .line 1916761
    iput-object v1, v0, LX/21A;->b:Ljava/lang/String;

    .line 1916762
    move-object v0, v0

    .line 1916763
    sget-object v1, LX/21B;->THREADED_PERMALINK:LX/21B;

    .line 1916764
    iput-object v1, v0, LX/21A;->e:LX/21B;

    .line 1916765
    move-object v0, v0

    .line 1916766
    sget-object v1, LX/21D;->PERMALINK:LX/21D;

    .line 1916767
    iput-object v1, v0, LX/21A;->i:LX/21D;

    .line 1916768
    move-object v0, v0

    .line 1916769
    invoke-virtual {v0}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1916770
    invoke-direct {p0}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->c()V

    .line 1916771
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->m:LX/CZo;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CZo;->a(Landroid/content/Context;)LX/CZn;

    move-result-object v3

    .line 1916772
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->a:LX/9DH;

    invoke-direct {p0}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->l()Landroid/content/Context;

    move-result-object v2

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iget-boolean v7, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->L:Z

    new-instance v9, LX/CZp;

    invoke-direct {v9, p0}, LX/CZp;-><init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    new-instance v11, LX/CZq;

    invoke-direct {v11, p0}, LX/CZq;-><init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    move-object v1, p0

    move v10, v8

    invoke-virtual/range {v0 .. v11}, LX/9DH;->a(Landroid/support/v4/app/Fragment;Landroid/content/Context;LX/9Bj;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZZZLX/9Br;ZLX/0QK;)LX/9DG;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    .line 1916773
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0, v1}, LX/9DG;->a(Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    .line 1916774
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    invoke-direct {p0, v3, v0}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->a(LX/CZl;LX/9DG;)LX/1Cw;

    move-result-object v0

    .line 1916775
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    .line 1916776
    iget-object v2, v1, LX/9DG;->b:LX/9CC;

    move-object v1, v2

    .line 1916777
    iget-boolean v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->L:Z

    if-eqz v2, :cond_3

    new-array v2, v12, [LX/1Cw;

    aput-object v0, v2, v8

    aput-object v1, v2, v6

    invoke-static {v2}, LX/62C;->b([LX/1Cw;)LX/62C;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->y:LX/62C;

    .line 1916778
    check-cast v3, LX/CZn;

    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    .line 1916779
    iput-object v0, v3, LX/CZn;->a:LX/9DG;

    .line 1916780
    new-instance v0, LX/62O;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->B:LX/1DI;

    invoke-direct {v0, v1, v2}, LX/62O;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;LX/1DI;)V

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->D:LX/62O;

    .line 1916781
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->b:LX/9Ci;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916782
    iget-object v2, v1, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1916783
    new-instance v2, LX/CZr;

    invoke-direct {v2, p0}, LX/CZr;-><init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    .line 1916784
    new-instance v4, LX/9Ch;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v3

    check-cast v3, LX/1K9;

    invoke-direct {v4, v1, v2, v3}, LX/9Ch;-><init>(Ljava/lang/String;LX/0QK;LX/1K9;)V

    .line 1916785
    move-object v0, v4

    .line 1916786
    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->x:LX/9Ch;

    .line 1916787
    new-array v0, v6, [LX/21l;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    aput-object v1, v0, v8

    invoke-static {v0}, LX/2Ch;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->A:Ljava/util/Set;

    .line 1916788
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916789
    iget-object v1, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->j:Ljava/lang/String;

    move-object v0, v1

    .line 1916790
    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->I:Ljava/lang/String;

    .line 1916791
    iget-boolean v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->E:Z

    if-nez v0, :cond_2

    .line 1916792
    invoke-static {p0}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->d(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    .line 1916793
    :cond_2
    return-void

    .line 1916794
    :cond_3
    new-array v2, v12, [LX/1Cw;

    aput-object v0, v2, v8

    aput-object v1, v2, v6

    invoke-static {v2}, LX/62C;->a([LX/1Cw;)LX/62C;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1916862
    iput-object p1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1916863
    if-eqz p1, :cond_2

    .line 1916864
    new-instance v0, LX/9Gu;

    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->K:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;LX/0Px;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    invoke-direct {v0, v2, v3}, LX/9Gu;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/api/ufiservices/FetchSingleCommentParams;)V

    .line 1916865
    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->J:LX/1Cq;

    .line 1916866
    iput-object v0, v2, LX/1Cq;->a:Ljava/lang/Object;

    .line 1916867
    :goto_0
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->w:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 1916868
    if-nez p1, :cond_3

    move-object v0, v1

    .line 1916869
    :goto_1
    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->x:LX/9Ch;

    invoke-virtual {v2, p1}, LX/9Ch;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1916870
    iget-object v5, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    invoke-static {v5}, LX/14w;->r(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 1916871
    :cond_0
    :goto_2
    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916872
    iget-object v3, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    move-object v2, v3

    .line 1916873
    iget-object v3, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->G:Lcom/facebook/graphql/model/GraphQLComment;

    iget-object v4, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->k:LX/1ry;

    invoke-static {v0, v2, v3, v4}, LX/9DG;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;LX/1ry;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1916874
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1916875
    :cond_1
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->A:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/21l;

    .line 1916876
    invoke-interface {v0, v1}, LX/21l;->a(Ljava/lang/Object;)V

    goto :goto_3

    .line 1916877
    :cond_2
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->J:LX/1Cq;

    .line 1916878
    iput-object v1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 1916879
    goto :goto_0

    .line 1916880
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_1

    .line 1916881
    :cond_4
    return-void

    .line 1916882
    :cond_5
    iget-object v5, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    iget-object v6, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLComment;->o()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->aW()Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/9DG;->a(Ljava/lang/Long;)V

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1916643
    check-cast p1, Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {p0, p1}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1916647
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1916648
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->I:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1916649
    const-string v0, "source_group_id"

    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->I:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1916650
    :cond_0
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1916651
    const-string v0, "content_id"

    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1916652
    :cond_1
    :goto_0
    return-object v1

    .line 1916653
    :cond_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1916654
    if-eqz v0, :cond_1

    .line 1916655
    const-string v2, "commentParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916656
    if-eqz v0, :cond_1

    .line 1916657
    iget-object v2, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1916658
    if-eqz v2, :cond_1

    .line 1916659
    const-string v2, "feedback_id"

    .line 1916660
    iget-object p0, v0, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    move-object v0, p0

    .line 1916661
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1916662
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1916663
    const-string v0, "Comment Params"

    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916664
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1916665
    const-string v4, "Comment Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1916666
    iget-object v5, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->a:Ljava/lang/String;

    move-object v5, v5

    .line 1916667
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916668
    const-string v4, "Parent Legacy Api Post Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1916669
    iget-object v5, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 1916670
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916671
    const-string v4, "Max Comments Items: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1916672
    iget v5, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->c:I

    move v5, v5

    .line 1916673
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916674
    const-string v4, "Parent Story Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1916675
    iget-object v5, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->d:Ljava/lang/String;

    move-object v5, v5

    .line 1916676
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916677
    const-string v4, "Parent Story Cache Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1916678
    iget-object v5, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1916679
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916680
    const-string v4, "Include Permalink Title: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1916681
    iget-boolean v5, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->f:Z

    move v5, v5

    .line 1916682
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916683
    const-string v4, "Reply Comment Id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1916684
    iget-object v5, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->g:Ljava/lang/String;

    move-object v5, v5

    .line 1916685
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916686
    const-string v4, "Include Comments Disabled Fields:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 1916687
    iget-boolean v5, v2, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->k:Z

    move v5, v5

    .line 1916688
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\n"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1916689
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 1916690
    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1916691
    const-string v2, "Has Fetched Feedback: "

    iget-boolean v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->E:Z

    if-eqz v0, :cond_1

    const-string v0, "True"

    :goto_0
    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1916692
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v0, :cond_0

    .line 1916693
    :try_start_0
    const-string v0, "Threaded Comments Permalink"

    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->g:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->g()LX/4ps;

    move-result-object v2

    invoke-virtual {v2}, LX/4ps;->a()LX/4ps;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v2, v3}, LX/4ps;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1916694
    :cond_0
    :goto_1
    return-object v1

    .line 1916695
    :cond_1
    const-string v0, "False"

    goto :goto_0

    :catch_0
    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1916644
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1916645
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    invoke-virtual {v0, p1, p2, p3}, LX/9DG;->a(IILandroid/content/Intent;)V

    .line 1916646
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x68379c3a

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1916696
    invoke-direct {p0}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 1916697
    const v2, 0x7f030f28

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x6fc3b688

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x2693d2f6

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1916698
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1916699
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->x:LX/9Ch;

    .line 1916700
    iget-object v2, v1, LX/9Ch;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Vi;

    .line 1916701
    iget-object v4, v1, LX/9Ch;->a:LX/1K9;

    invoke-virtual {v4, v2}, LX/1K9;->a(LX/6Vi;)V

    goto :goto_0

    .line 1916702
    :cond_0
    iget-object v2, v1, LX/9Ch;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1916703
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->c()V

    .line 1916704
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->l:LX/0tF;

    invoke-virtual {v1}, LX/0tF;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1916705
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->c:LX/1K9;

    new-instance v2, LX/82i;

    iget-object v3, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLComment;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    .line 1916706
    iget-object p0, v4, Lcom/facebook/api/ufiservices/FetchSingleCommentParams;->b:Ljava/lang/String;

    move-object v4, p0

    .line 1916707
    invoke-direct {v2, v3, v4}, LX/82i;-><init>(Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    .line 1916708
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x2479dd8d

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2d9f5eec

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1916709
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1916710
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->b()V

    .line 1916711
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->r:LX/0g8;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/0g8;->a(LX/0fx;)V

    .line 1916712
    const/16 v1, 0x2b

    const v2, -0x783db385

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x29c6b4a2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1916713
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1916714
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->d()V

    .line 1916715
    const/16 v1, 0x2b

    const v2, 0xfb26049

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1323b544

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1916716
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1916717
    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    invoke-virtual {v1}, LX/9DG;->e()V

    .line 1916718
    const/16 v1, 0x2b

    const v2, -0x424f7b8e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1916719
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1916720
    const-string v0, "feedbackLoggingParams"

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->H:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1916721
    const-string v0, "commentParams"

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->z:Lcom/facebook/api/ufiservices/FetchSingleCommentParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1916722
    const-string v0, "loadingState"

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->C:Lcom/facebook/widget/loadingindicator/LoadingIndicatorState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1916723
    const-string v0, "use_recycler_view"

    iget-boolean v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->L:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1916724
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x44528fa8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1916725
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1916726
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1916727
    if-eqz v0, :cond_0

    .line 1916728
    const v2, 0x7f0810e8

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 1916729
    :cond_0
    const/16 v0, 0x2b

    const v2, 0x9e6e69e

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1916730
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1916731
    const v0, 0x7f0d24c3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbSwipeRefreshLayout;

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->s:Lcom/facebook/widget/FbSwipeRefreshLayout;

    .line 1916732
    const v0, 0x7f0d24c6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1916733
    iget-boolean v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->L:Z

    invoke-static {v0, p1}, LX/BFS;->a(ZLandroid/view/View;)LX/0g8;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->r:LX/0g8;

    .line 1916734
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->v:LX/9DG;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->r:LX/0g8;

    iget-object v2, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->y:LX/62C;

    invoke-virtual {v0, p1, v1, v2}, LX/9DG;->a(Landroid/view/View;LX/0g8;Landroid/widget/BaseAdapter;)V

    .line 1916735
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->r:LX/0g8;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->y:LX/62C;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1916736
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->r:LX/0g8;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-interface {v0, v1}, LX/0g8;->f(Landroid/view/View;)V

    .line 1916737
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->r:LX/0g8;

    new-instance v1, LX/CZs;

    invoke-direct {v1, p0}, LX/CZs;-><init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/0fx;)V

    .line 1916738
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->s:Lcom/facebook/widget/FbSwipeRefreshLayout;

    new-instance v1, LX/CZt;

    invoke-direct {v1, p0}, LX/CZt;-><init>(Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setOnRefreshListener(LX/1PH;)V

    .line 1916739
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->D:LX/62O;

    iget-object v1, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->t:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0, v1}, LX/62O;->a(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;)V

    .line 1916740
    iget-object v0, p0, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->F:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {p0, v0}, Lcom/facebook/permalink/threadedcomments/ThreadedCommentsPermalinkFragment;->a(Lcom/facebook/graphql/model/GraphQLComment;)V

    .line 1916741
    return-void
.end method
