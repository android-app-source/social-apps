.class public Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pg;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field private final b:Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

.field private final c:Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;


# direct methods
.method public constructor <init>(LX/14w;Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1916425
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1916426
    iput-object p1, p0, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->a:LX/14w;

    .line 1916427
    iput-object p3, p0, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->b:Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    .line 1916428
    iput-object p2, p0, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->c:Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    .line 1916429
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;
    .locals 6

    .prologue
    .line 1916430
    const-class v1, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    monitor-enter v1

    .line 1916431
    :try_start_0
    sget-object v0, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1916432
    sput-object v2, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1916433
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1916434
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1916435
    new-instance p0, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-static {v0}, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;-><init>(LX/14w;Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;)V

    .line 1916436
    move-object v0, p0

    .line 1916437
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1916438
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1916439
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1916440
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1916441
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1916442
    iget-object v0, p0, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->c:Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->b:Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1916443
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1916444
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1916445
    invoke-static {p1}, LX/14w;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
