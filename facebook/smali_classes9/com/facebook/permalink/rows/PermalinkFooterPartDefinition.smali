.class public Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pg;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

.field private final d:Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0pf;


# direct methods
.method public constructor <init>(LX/14w;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;LX/0pf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1916418
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1916419
    iput-object p1, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->a:LX/14w;

    .line 1916420
    iput-object p2, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 1916421
    iput-object p3, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    .line 1916422
    iput-object p4, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->d:Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    .line 1916423
    iput-object p5, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->e:LX/0pf;

    .line 1916424
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;
    .locals 9

    .prologue
    .line 1916406
    const-class v1, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    monitor-enter v1

    .line 1916407
    :try_start_0
    sget-object v0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1916408
    sput-object v2, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1916409
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1916410
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1916411
    new-instance v3, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v8

    check-cast v8, LX/0pf;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;-><init>(LX/14w;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;LX/0pf;)V

    .line 1916412
    move-object v0, v3

    .line 1916413
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1916414
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1916415
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1916416
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1916417
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1916395
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1916396
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1916397
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1916398
    iget-object v1, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->e:LX/0pf;

    invoke-virtual {v1, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v1

    .line 1916399
    iget-boolean v2, v1, LX/1g0;->o:Z

    move v1, v2

    .line 1916400
    if-eqz v1, :cond_0

    sget-object v1, LX/1Wi;->TOP:LX/1Wi;

    .line 1916401
    :goto_0
    iget-object v2, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v3, LX/20d;

    invoke-direct {v3, p2, v1}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1916402
    iget-object v1, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->d:Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1916403
    iget-object v0, p0, Lcom/facebook/permalink/rows/PermalinkFooterPartDefinition;->c:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    new-instance v1, LX/3Dt;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, LX/3Dt;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1916404
    const/4 v0, 0x0

    return-object v0

    .line 1916405
    :cond_0
    sget-object v1, LX/1Wi;->PERMALINK:LX/1Wi;

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1916393
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1916394
    invoke-static {p1}, LX/14w;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
