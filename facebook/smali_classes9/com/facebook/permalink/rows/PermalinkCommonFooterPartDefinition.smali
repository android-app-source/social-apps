.class public Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/1Pg;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1916377
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1916378
    const v0, 0x7f0b00a6

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;->a:I

    .line 1916379
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;
    .locals 4

    .prologue
    .line 1916380
    const-class v1, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    monitor-enter v1

    .line 1916381
    :try_start_0
    sget-object v0, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1916382
    sput-object v2, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1916383
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1916384
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1916385
    new-instance p0, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;-><init>(Landroid/content/res/Resources;)V

    .line 1916386
    move-object v0, p0

    .line 1916387
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1916388
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1916389
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1916390
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x11f80793

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1916391
    check-cast p4, LX/1wK;

    iget v1, p0, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;->a:I

    invoke-interface {p4, v1}, LX/1wK;->setButtonContainerHeight(I)V

    .line 1916392
    const/16 v1, 0x1f

    const v2, -0x5d617f34

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
