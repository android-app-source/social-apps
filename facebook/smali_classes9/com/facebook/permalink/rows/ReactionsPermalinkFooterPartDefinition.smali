.class public Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pg;",
        "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

.field private final d:Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/14w;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1916446
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1916447
    iput-object p1, p0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->a:LX/14w;

    .line 1916448
    iput-object p2, p0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 1916449
    iput-object p3, p0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->c:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    .line 1916450
    iput-object p4, p0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->d:Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    .line 1916451
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;
    .locals 7

    .prologue
    .line 1916452
    const-class v1, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    monitor-enter v1

    .line 1916453
    :try_start_0
    sget-object v0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1916454
    sput-object v2, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1916455
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1916456
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1916457
    new-instance p0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v3

    check-cast v3, LX/14w;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;-><init>(LX/14w;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;)V

    .line 1916458
    move-object v0, p0

    .line 1916459
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1916460
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1916461
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1916462
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1916463
    sget-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->h:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1916464
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1916465
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1916466
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1916467
    iget-object v1, p0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v2, LX/20d;

    sget-object v3, LX/1Wi;->PERMALINK:LX/1Wi;

    invoke-direct {v2, p2, v3}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1916468
    iget-object v1, p0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->d:Lcom/facebook/permalink/rows/PermalinkCommonFooterPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1916469
    iget-object v0, p0, Lcom/facebook/permalink/rows/ReactionsPermalinkFooterPartDefinition;->c:Lcom/facebook/feedplugins/feedbackreactions/ui/BaseReactionsFooterPartDefinition;

    new-instance v1, LX/20y;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, LX/20y;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1916470
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1916471
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1916472
    invoke-static {p1}, LX/14w;->p(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
