.class public Lcom/facebook/permalink/PermalinkParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/permalink/PermalinkParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/89m;

.field public b:LX/89g;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field public i:LX/21y;

.field public j:Z

.field public k:LX/21C;

.field public l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

.field public m:Lcom/facebook/graphql/model/GraphQLComment;

.field public n:Lcom/facebook/graphql/model/GraphQLComment;

.field public o:Z

.field public p:Z

.field public q:I

.field public r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

.field public s:Z

.field public t:Z

.field public u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

.field public v:Lcom/facebook/tagging/model/TaggingProfile;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1765715
    new-instance v0, LX/BFN;

    invoke-direct {v0}, LX/BFN;-><init>()V

    sput-object v0, Lcom/facebook/permalink/PermalinkParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/BFO;)V
    .locals 1

    .prologue
    .line 1765770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765771
    iget-object v0, p1, LX/BFO;->a:LX/89m;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->a:LX/89m;

    .line 1765772
    iget-object v0, p1, LX/BFO;->b:LX/89g;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->b:LX/89g;

    .line 1765773
    iget-object v0, p1, LX/BFO;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    .line 1765774
    iget-object v0, p1, LX/BFO;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->d:Ljava/lang/String;

    .line 1765775
    iget-object v0, p1, LX/BFO;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->e:Ljava/lang/String;

    .line 1765776
    iget-object v0, p1, LX/BFO;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    .line 1765777
    iget-object v0, p1, LX/BFO;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->g:Ljava/lang/String;

    .line 1765778
    iget-object v0, p1, LX/BFO;->i:LX/21y;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->i:LX/21y;

    .line 1765779
    iget-boolean v0, p1, LX/BFO;->j:Z

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->j:Z

    .line 1765780
    iget-object v0, p1, LX/BFO;->k:LX/21C;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->k:LX/21C;

    .line 1765781
    iget-object v0, p1, LX/BFO;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1765782
    iget-object v0, p1, LX/BFO;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->h:Ljava/lang/String;

    .line 1765783
    iget-object v0, p1, LX/BFO;->m:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1765784
    iget-object v0, p1, LX/BFO;->n:Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->n:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1765785
    iget-boolean v0, p1, LX/BFO;->o:Z

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->o:Z

    .line 1765786
    iget-boolean v0, p1, LX/BFO;->p:Z

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->p:Z

    .line 1765787
    iget v0, p1, LX/BFO;->q:I

    iput v0, p0, Lcom/facebook/permalink/PermalinkParams;->q:I

    .line 1765788
    iget-object v0, p1, LX/BFO;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1765789
    iget-boolean v0, p1, LX/BFO;->s:Z

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->s:Z

    .line 1765790
    iget-boolean v0, p1, LX/BFO;->t:Z

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->t:Z

    .line 1765791
    iget-object v0, p1, LX/BFO;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 1765792
    iget-object v0, p1, LX/BFO;->v:Lcom/facebook/tagging/model/TaggingProfile;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->v:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1765793
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1765794
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765795
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/89m;->valueOf(Ljava/lang/String;)LX/89m;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->a:LX/89m;

    .line 1765796
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1765797
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1765798
    invoke-static {v0}, LX/89g;->valueOf(Ljava/lang/String;)LX/89g;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->b:LX/89g;

    .line 1765799
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    .line 1765800
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->d:Ljava/lang/String;

    .line 1765801
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->e:Ljava/lang/String;

    .line 1765802
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    .line 1765803
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->g:Ljava/lang/String;

    .line 1765804
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1765805
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1765806
    invoke-static {v0}, LX/21y;->getOrder(Ljava/lang/String;)LX/21y;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->i:LX/21y;

    .line 1765807
    :cond_1
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->j:Z

    .line 1765808
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1765809
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1765810
    invoke-static {v0}, LX/21C;->valueOf(Ljava/lang/String;)LX/21C;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->k:LX/21C;

    .line 1765811
    :cond_2
    const-class v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    .line 1765812
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1765813
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLComment;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->n:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1765814
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->o:Z

    .line 1765815
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->p:Z

    .line 1765816
    invoke-static {p1}, LX/46R;->c(Landroid/os/Parcel;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/facebook/permalink/PermalinkParams;->q:I

    .line 1765817
    const-class v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1765818
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->s:Z

    .line 1765819
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->t:Z

    .line 1765820
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    .line 1765821
    const-class v0, Lcom/facebook/tagging/model/TaggingProfile;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/model/TaggingProfile;

    iput-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->v:Lcom/facebook/tagging/model/TaggingProfile;

    .line 1765822
    return-void
.end method


# virtual methods
.method public final a()LX/89m;
    .locals 1

    .prologue
    .line 1765823
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->a:LX/89m;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1765824
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1765768
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1765769
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1765767
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()LX/21y;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1765766
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->i:LX/21y;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1765765
    iget-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->j:Z

    return v0
.end method

.method public final u()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 1765762
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1765763
    const-string v1, "permalink_params"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1765764
    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1765716
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->a:LX/89m;

    move-object v0, v0

    .line 1765717
    invoke-virtual {v0}, LX/89m;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765718
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->b:LX/89g;

    move-object v0, v0

    .line 1765719
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    .line 1765720
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765721
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1765722
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765723
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->d:Ljava/lang/String;

    move-object v0, v0

    .line 1765724
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765725
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1765726
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765727
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->f:Ljava/lang/String;

    move-object v0, v0

    .line 1765728
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765729
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->g:Ljava/lang/String;

    move-object v0, v0

    .line 1765730
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765731
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->i:LX/21y;

    move-object v0, v0

    .line 1765732
    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    move-object v0, v0

    .line 1765733
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765734
    iget-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->j:Z

    move v0, v0

    .line 1765735
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1765736
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->k:LX/21C;

    move-object v0, v0

    .line 1765737
    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    move-object v0, v0

    .line 1765738
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765739
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->l:Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1765740
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->m:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1765741
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->n:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1765742
    iget-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->o:Z

    move v0, v0

    .line 1765743
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1765744
    iget-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->p:Z

    move v0, v0

    .line 1765745
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1765746
    iget v0, p0, Lcom/facebook/permalink/PermalinkParams;->q:I

    move v0, v0

    .line 1765747
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Integer;)V

    .line 1765748
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->r:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1765749
    iget-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->s:Z

    move v0, v0

    .line 1765750
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1765751
    iget-boolean v0, p0, Lcom/facebook/permalink/PermalinkParams;->t:Z

    move v0, v0

    .line 1765752
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1765753
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->u:Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1765754
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->v:Lcom/facebook/tagging/model/TaggingProfile;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1765755
    return-void

    .line 1765756
    :cond_0
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->b:LX/89g;

    move-object v0, v0

    .line 1765757
    invoke-virtual {v0}, LX/89g;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 1765758
    :cond_1
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->i:LX/21y;

    move-object v0, v0

    .line 1765759
    invoke-virtual {v0}, LX/21y;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 1765760
    :cond_2
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkParams;->k:LX/21C;

    move-object v0, v0

    .line 1765761
    invoke-virtual {v0}, LX/21C;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
