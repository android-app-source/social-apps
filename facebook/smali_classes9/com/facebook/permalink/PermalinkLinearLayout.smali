.class public Lcom/facebook/permalink/PermalinkLinearLayout;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements LX/7yo;


# instance fields
.field public a:LX/7yl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0w3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1765663
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1765664
    invoke-direct {p0}, Lcom/facebook/permalink/PermalinkLinearLayout;->a()V

    .line 1765665
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 1765660
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1765661
    invoke-direct {p0}, Lcom/facebook/permalink/PermalinkLinearLayout;->a()V

    .line 1765662
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1765657
    const-class v0, Lcom/facebook/permalink/PermalinkLinearLayout;

    invoke-static {v0, p0}, Lcom/facebook/permalink/PermalinkLinearLayout;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1765658
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkLinearLayout;->a:LX/7yl;

    invoke-virtual {v0, p0}, LX/7yl;->a(LX/7yo;)V

    .line 1765659
    return-void
.end method

.method private static a(Lcom/facebook/permalink/PermalinkLinearLayout;LX/7yl;LX/0w3;)V
    .locals 0

    .prologue
    .line 1765656
    iput-object p1, p0, Lcom/facebook/permalink/PermalinkLinearLayout;->a:LX/7yl;

    iput-object p2, p0, Lcom/facebook/permalink/PermalinkLinearLayout;->b:LX/0w3;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/permalink/PermalinkLinearLayout;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/permalink/PermalinkLinearLayout;

    invoke-static {v1}, LX/7yl;->b(LX/0QB;)LX/7yl;

    move-result-object v0

    check-cast v0, LX/7yl;

    invoke-static {v1}, LX/0w3;->a(LX/0QB;)LX/0w3;

    move-result-object v1

    check-cast v1, LX/0w3;

    invoke-static {p0, v0, v1}, Lcom/facebook/permalink/PermalinkLinearLayout;->a(Lcom/facebook/permalink/PermalinkLinearLayout;LX/7yl;LX/0w3;)V

    return-void
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1765653
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 1765654
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkLinearLayout;->a:LX/7yl;

    invoke-virtual {v0, p1}, LX/7yl;->a(Landroid/graphics/Canvas;)V

    .line 1765655
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x1cac0fe5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1765650
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onAttachedToWindow()V

    .line 1765651
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkLinearLayout;->a:LX/7yl;

    invoke-virtual {v1}, LX/7yl;->a()V

    .line 1765652
    const/16 v1, 0x2d

    const v2, -0x38efcee1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x113b80e5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1765647
    iget-object v1, p0, Lcom/facebook/permalink/PermalinkLinearLayout;->a:LX/7yl;

    invoke-virtual {v1}, LX/7yl;->b()V

    .line 1765648
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onDetachedFromWindow()V

    .line 1765649
    const/16 v1, 0x2d

    const v2, -0x21a1cc35

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1765643
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkLinearLayout;->a:LX/7yl;

    invoke-virtual {v0, p1}, LX/7yl;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onMeasure(II)V
    .locals 1

    .prologue
    .line 1765644
    iget-object v0, p0, Lcom/facebook/permalink/PermalinkLinearLayout;->b:LX/0w3;

    invoke-virtual {v0, p0, p2}, LX/0w3;->a(Landroid/view/View;I)V

    .line 1765645
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onMeasure(II)V

    .line 1765646
    return-void
.end method
