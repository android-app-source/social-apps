.class public Lcom/facebook/uicontrib/fab/FabView;
.super Landroid/view/View;
.source ""


# static fields
.field private static final b:LX/2iJ;

.field private static final c:I


# instance fields
.field public a:LX/BS7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:I

.field private e:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/Integer;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1784913
    sget-object v0, LX/2iJ;->BIG:LX/2iJ;

    sput-object v0, Lcom/facebook/uicontrib/fab/FabView;->b:LX/2iJ;

    .line 1784914
    const v0, 0x7f0a00d1

    sput v0, Lcom/facebook/uicontrib/fab/FabView;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1784935
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1784936
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/fab/FabView;->b(Landroid/util/AttributeSet;)V

    .line 1784937
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1784932
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1784933
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/fab/FabView;->b(Landroid/util/AttributeSet;)V

    .line 1784934
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1784929
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1784930
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/fab/FabView;->b(Landroid/util/AttributeSet;)V

    .line 1784931
    return-void
.end method

.method private static a(Landroid/content/res/TypedArray;)LX/2iJ;
    .locals 6

    .prologue
    .line 1784923
    const/16 v0, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    .line 1784924
    invoke-static {}, LX/2iJ;->values()[LX/2iJ;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 1784925
    invoke-virtual {v0}, LX/2iJ;->getAttrEnumValue()I

    move-result v5

    if-ne v2, v5, :cond_0

    .line 1784926
    :goto_1
    return-object v0

    .line 1784927
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1784928
    :cond_1
    sget-object v0, Lcom/facebook/uicontrib/fab/FabView;->b:LX/2iJ;

    goto :goto_1
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1784915
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->isPressed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1784916
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    iget v1, p0, Lcom/facebook/uicontrib/fab/FabView;->d:I

    invoke-virtual {v0, v1}, LX/BS7;->a(I)V

    .line 1784917
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, LX/BS7;->setAlpha(I)V

    .line 1784918
    :goto_0
    return-void

    .line 1784919
    :cond_0
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1784920
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    iget-object v1, p0, Lcom/facebook/uicontrib/fab/FabView;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, LX/BS7;->a(I)V

    goto :goto_0

    .line 1784921
    :cond_1
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    iget v1, p0, Lcom/facebook/uicontrib/fab/FabView;->d:I

    invoke-virtual {v0, v1}, LX/BS7;->a(I)V

    .line 1784922
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    iget-object v1, p0, Lcom/facebook/uicontrib/fab/FabView;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, LX/BS7;->setAlpha(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/uicontrib/fab/FabView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/uicontrib/fab/FabView;

    new-instance p1, LX/BS7;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v2

    check-cast v2, LX/0wM;

    invoke-direct {p1, v1, v2}, LX/BS7;-><init>(Landroid/content/res/Resources;LX/0wM;)V

    move-object v0, p1

    check-cast v0, LX/BS7;

    iput-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    return-void
.end method

.method private b(Landroid/content/res/TypedArray;)V
    .locals 3

    .prologue
    .line 1784899
    const/16 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 1784900
    if-eqz v0, :cond_1

    .line 1784901
    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    iput v1, p0, Lcom/facebook/uicontrib/fab/FabView;->d:I

    .line 1784902
    :goto_0
    invoke-direct {p0}, Lcom/facebook/uicontrib/fab/FabView;->a()V

    .line 1784903
    const/16 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    .line 1784904
    if-eqz v0, :cond_2

    .line 1784905
    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->e:Ljava/lang/Integer;

    .line 1784906
    :goto_1
    const/16 v0, 0x3

    const/16 v1, 0xc8

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->f:Ljava/lang/Integer;

    .line 1784907
    const/16 v0, 0x5

    invoke-virtual {p1, v0}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 1784908
    if-eqz v0, :cond_0

    .line 1784909
    iget-object v1, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    invoke-virtual {v1, v0}, LX/BS7;->b(I)V

    .line 1784910
    :cond_0
    return-void

    .line 1784911
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/facebook/uicontrib/fab/FabView;->c:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/uicontrib/fab/FabView;->d:I

    goto :goto_0

    .line 1784912
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->e:Ljava/lang/Integer;

    goto :goto_1
.end method

.method private b(Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1784894
    const-class v0, Lcom/facebook/uicontrib/fab/FabView;

    invoke-static {v0, p0}, Lcom/facebook/uicontrib/fab/FabView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1784895
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/uicontrib/fab/FabView;->setClickable(Z)V

    .line 1784896
    invoke-virtual {p0, p1}, Lcom/facebook/uicontrib/fab/FabView;->a(Landroid/util/AttributeSet;)V

    .line 1784897
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-virtual {v0, p0}, LX/BS7;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1784898
    return-void
.end method


# virtual methods
.method public final a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1784886
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FabView:[I

    invoke-virtual {v0, p1, v1, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1784887
    :try_start_0
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    const/16 v2, 0x6

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    invoke-virtual {v0, v2}, LX/BS7;->a(Z)V

    .line 1784888
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-static {v1}, Lcom/facebook/uicontrib/fab/FabView;->a(Landroid/content/res/TypedArray;)LX/2iJ;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/BS7;->a(LX/2iJ;)V

    .line 1784889
    invoke-direct {p0, v1}, Lcom/facebook/uicontrib/fab/FabView;->b(Landroid/content/res/TypedArray;)V

    .line 1784890
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    const/16 v2, 0x4

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v0, v2}, LX/BS7;->c(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1784891
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1784892
    return-void

    .line 1784893
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method

.method public final drawableStateChanged()V
    .locals 0

    .prologue
    .line 1784882
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    .line 1784883
    invoke-direct {p0}, Lcom/facebook/uicontrib/fab/FabView;->a()V

    .line 1784884
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->invalidate()V

    .line 1784885
    return-void
.end method

.method public getFillColor()I
    .locals 1

    .prologue
    .line 1784938
    iget v0, p0, Lcom/facebook/uicontrib/fab/FabView;->d:I

    return v0
.end method

.method public getPressedFillAlpha()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1784841
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->f:Ljava/lang/Integer;

    return-object v0
.end method

.method public getPressedFillColor()Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1784846
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->e:Ljava/lang/Integer;

    return-object v0
.end method

.method public getSize()LX/2iJ;
    .locals 1

    .prologue
    .line 1784847
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    .line 1784848
    iget-object p0, v0, LX/BS7;->f:LX/2iJ;

    move-object v0, p0

    .line 1784849
    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1784850
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1784851
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-virtual {v0, p1}, LX/BS7;->draw(Landroid/graphics/Canvas;)V

    .line 1784852
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1784853
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->getSize()LX/2iJ;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2iJ;->getFullSize(Landroid/content/res/Resources;)I

    move-result v0

    .line 1784854
    invoke-static {v0, p1}, Lcom/facebook/uicontrib/fab/FabView;->resolveSize(II)I

    move-result v1

    invoke-static {v0, p2}, Lcom/facebook/uicontrib/fab/FabView;->resolveSize(II)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/uicontrib/fab/FabView;->setMeasuredDimension(II)V

    .line 1784855
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, LX/BS7;->setBounds(IIII)V

    .line 1784856
    return-void
.end method

.method public setFillColor(I)V
    .locals 0

    .prologue
    .line 1784842
    iput p1, p0, Lcom/facebook/uicontrib/fab/FabView;->d:I

    .line 1784843
    invoke-direct {p0}, Lcom/facebook/uicontrib/fab/FabView;->a()V

    .line 1784844
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->invalidate()V

    .line 1784845
    return-void
.end method

.method public setGlyphDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1784857
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    .line 1784858
    iput-object p1, v0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    .line 1784859
    iget-object v1, v0, LX/BS7;->j:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 1784860
    invoke-static {v0}, LX/BS7;->c(LX/BS7;)V

    .line 1784861
    :cond_0
    const/4 v1, -0x1

    iput v1, v0, LX/BS7;->i:I

    .line 1784862
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->invalidate()V

    .line 1784863
    return-void
.end method

.method public setGlyphDrawableColor(I)V
    .locals 1

    .prologue
    .line 1784864
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-virtual {v0, p1}, LX/BS7;->b(I)V

    .line 1784865
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->invalidate()V

    .line 1784866
    return-void
.end method

.method public setGlyphDrawableID(I)V
    .locals 1

    .prologue
    .line 1784867
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-virtual {v0, p1}, LX/BS7;->c(I)V

    .line 1784868
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->invalidate()V

    .line 1784869
    return-void
.end method

.method public setPressedFillAlpha(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1784870
    iput-object p1, p0, Lcom/facebook/uicontrib/fab/FabView;->f:Ljava/lang/Integer;

    .line 1784871
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->invalidate()V

    .line 1784872
    return-void
.end method

.method public setPressedFillColor(Ljava/lang/Integer;)V
    .locals 0
    .param p1    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1784873
    iput-object p1, p0, Lcom/facebook/uicontrib/fab/FabView;->e:Ljava/lang/Integer;

    .line 1784874
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->invalidate()V

    .line 1784875
    return-void
.end method

.method public setShowShadow(Z)V
    .locals 1

    .prologue
    .line 1784876
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-virtual {v0, p1}, LX/BS7;->a(Z)V

    .line 1784877
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->invalidate()V

    .line 1784878
    return-void
.end method

.method public setSize(LX/2iJ;)V
    .locals 1

    .prologue
    .line 1784879
    iget-object v0, p0, Lcom/facebook/uicontrib/fab/FabView;->a:LX/BS7;

    invoke-virtual {v0, p1}, LX/BS7;->a(LX/2iJ;)V

    .line 1784880
    invoke-virtual {p0}, Lcom/facebook/uicontrib/fab/FabView;->requestLayout()V

    .line 1784881
    return-void
.end method
