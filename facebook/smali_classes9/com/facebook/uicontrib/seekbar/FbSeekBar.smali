.class public Lcom/facebook/uicontrib/seekbar/FbSeekBar;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/4Ba;
.implements LX/39D;
.implements LX/39E;


# instance fields
.field private a:LX/1wz;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field public g:I

.field private h:I

.field private i:I

.field public j:F

.field private k:F

.field private l:F

.field private m:LX/FcR;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1785010
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1785011
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1785012
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1785013
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1785014
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1785015
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->c()V

    .line 1785016
    return-void
.end method

.method private a(F)Z
    .locals 2

    .prologue
    .line 1785017
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getCenterY()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1785018
    new-instance v0, LX/1wz;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1wz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a:LX/1wz;

    .line 1785019
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a:LX/1wz;

    const/4 v1, 0x2

    new-array v1, v1, [LX/31M;

    const/4 v2, 0x0

    sget-object v3, LX/31M;->LEFT:LX/31M;

    aput-object v3, v1, v2

    sget-object v2, LX/31M;->RIGHT:LX/31M;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, LX/1wz;->a([LX/31M;)V

    .line 1785020
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a:LX/1wz;

    .line 1785021
    iput-object p0, v0, LX/1wz;->q:LX/4Ba;

    .line 1785022
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a:LX/1wz;

    .line 1785023
    iput-object p0, v0, LX/1wz;->r:LX/39D;

    .line 1785024
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a:LX/1wz;

    .line 1785025
    iput-object p0, v0, LX/1wz;->s:LX/39E;

    .line 1785026
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1785027
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->c:Landroid/graphics/Paint;

    .line 1785028
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->c:Landroid/graphics/Paint;

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785029
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1785030
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->c:Landroid/graphics/Paint;

    const v2, 0x7f0b1a05

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1785031
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->b:Landroid/graphics/Paint;

    .line 1785032
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->b:Landroid/graphics/Paint;

    const v2, 0x7f0a0114

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785033
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->b:Landroid/graphics/Paint;

    const v2, 0x7f0b1a05

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1785034
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->d:Landroid/graphics/Paint;

    .line 1785035
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->d:Landroid/graphics/Paint;

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785036
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->d:Landroid/graphics/Paint;

    const/16 v2, 0x7f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1785037
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1785038
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e:Landroid/graphics/Paint;

    .line 1785039
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e:Landroid/graphics/Paint;

    const v2, 0x7f0a00d5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785040
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1785041
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->f:Landroid/graphics/Paint;

    .line 1785042
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->f:Landroid/graphics/Paint;

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785043
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1785044
    const v1, 0x7f0b1a08

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    .line 1785045
    const v1, 0x7f0b1a07

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->h:I

    .line 1785046
    const v1, 0x7f0b1a06

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->i:I

    .line 1785047
    const/high16 v0, 0x7fc00000    # NaNf

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    .line 1785048
    return-void
.end method

.method private d()V
    .locals 9

    .prologue
    .line 1785049
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->m:LX/FcR;

    if-eqz v0, :cond_0

    .line 1785050
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->m:LX/FcR;

    .line 1785051
    iget v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    move v1, v1

    .line 1785052
    iget-object v2, v0, LX/FcR;->b:LX/FdQ;

    new-instance v3, LX/CyH;

    iget-object v4, v0, LX/FcR;->c:LX/CyH;

    .line 1785053
    iget-object v5, v4, LX/CyH;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1785054
    const/4 v5, 0x0

    invoke-static {v1}, LX/FcS;->c(F)I

    move-result v6

    int-to-float v6, v6

    .line 1785055
    const/high16 v7, 0x42c80000    # 100.0f

    cmpl-float v7, v6, v7

    if-nez v7, :cond_1

    .line 1785056
    const-string v7, "default"

    .line 1785057
    :goto_0
    move-object v6, v7

    .line 1785058
    invoke-direct {v3, v4, v5, v6}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/FdQ;->a(LX/CyH;)V

    .line 1785059
    :cond_0
    return-void

    .line 1785060
    :cond_1
    new-instance v7, LX/0lp;

    invoke-direct {v7}, LX/0lp;-><init>()V

    .line 1785061
    new-instance v8, Ljava/io/StringWriter;

    invoke-direct {v8}, Ljava/io/StringWriter;-><init>()V

    .line 1785062
    :try_start_0
    invoke-virtual {v7, v8}, LX/0lp;->a(Ljava/io/Writer;)LX/0nX;

    move-result-object v7

    .line 1785063
    invoke-virtual {v7}, LX/0nX;->f()V

    .line 1785064
    const-string p0, "filter_radius_km"

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3fcdfeda

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, p0, v0}, LX/0nX;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1785065
    invoke-virtual {v7}, LX/0nX;->g()V

    .line 1785066
    invoke-virtual {v7}, LX/0nX;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1785067
    invoke-virtual {v8}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 1785068
    :catch_0
    const-string v7, "default"

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1785069
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->m:LX/FcR;

    if-eqz v0, :cond_0

    .line 1785070
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->m:LX/FcR;

    .line 1785071
    iget v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    move v1, v1

    .line 1785072
    iget-object v2, v0, LX/FcR;->a:Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;

    invoke-static {v1}, LX/FcS;->c(F)I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;->c(I)V

    .line 1785073
    :cond_0
    return-void
.end method

.method private f(FF)Z
    .locals 1

    .prologue
    .line 1785074
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1785075
    const/4 v0, 0x0

    .line 1785076
    :goto_0
    return v0

    .line 1785077
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->setCurrentPosition(F)V

    .line 1785078
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->d()V

    .line 1785079
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getCenterY()I
    .locals 1

    .prologue
    .line 1785080
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private getCurrentPosition()F
    .locals 1

    .prologue
    .line 1785081
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getThumbX()F

    move-result v0

    return v0
.end method

.method private getLeftBound()I
    .locals 1

    .prologue
    .line 1785082
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    return v0
.end method

.method private getRightBound()I
    .locals 2

    .prologue
    .line 1784968
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private getThumbX()F
    .locals 5

    .prologue
    .line 1785083
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    iget v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->k:F

    iget v2, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->l:F

    .line 1785084
    iget v3, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    move v3, v3

    .line 1785085
    int-to-float v3, v3

    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getRightBound()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v0, v1, v2, v3, v4}, LX/0yq;->a(FFFFF)F

    move-result v0

    return v0
.end method

.method private setCurrentPosition(F)V
    .locals 4

    .prologue
    .line 1785086
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    move v0, v0

    .line 1785087
    int-to-float v0, v0

    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getRightBound()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->k:F

    iget v3, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->l:F

    invoke-static {p1, v0, v1, v2, v3}, LX/0yq;->a(FFFFF)F

    move-result v0

    .line 1785088
    iget v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->k:F

    iget v2, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->l:F

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    .line 1785089
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->invalidate()V

    .line 1785090
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e()V

    .line 1785091
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 1785006
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->d()V

    .line 1785007
    return-void
.end method

.method public final a(LX/31M;I)V
    .locals 0

    .prologue
    .line 1785008
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->d()V

    .line 1785009
    return-void
.end method

.method public final a(FF)Z
    .locals 1

    .prologue
    .line 1784965
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a(F)Z

    move-result v0

    return v0
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 1784966
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1784967
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 1

    .prologue
    .line 1784969
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getThumbX()F

    move-result v0

    add-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->setCurrentPosition(F)V

    .line 1784970
    return-void
.end method

.method public final b(FF)Z
    .locals 1

    .prologue
    .line 1784971
    const/4 v0, 0x1

    return v0
.end method

.method public final c(FF)Z
    .locals 1

    .prologue
    .line 1784972
    invoke-direct {p0, p1, p2}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->f(FF)Z

    move-result v0

    return v0
.end method

.method public final d(FF)V
    .locals 0

    .prologue
    .line 1784973
    invoke-direct {p0, p1, p2}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->f(FF)Z

    .line 1784974
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1784975
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getCenterY()I

    move-result v0

    int-to-float v2, v0

    .line 1784976
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getThumbX()F

    move-result v6

    .line 1784977
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1784978
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    move v0, v0

    .line 1784979
    int-to-float v1, v0

    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->getRightBound()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->b:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1784980
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1784981
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->g:I

    move v0, v0

    .line 1784982
    int-to-float v1, v0

    iget-object v5, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v3, v6

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1784983
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->h:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1784984
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->i:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1784985
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1784986
    return-void
.end method

.method public final e(FF)V
    .locals 1

    .prologue
    .line 1784987
    cmpl-float v0, p1, p2

    if-ltz v0, :cond_0

    .line 1784988
    :goto_0
    return-void

    .line 1784989
    :cond_0
    iput p1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->k:F

    .line 1784990
    iput p2, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->l:F

    .line 1784991
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1784992
    iput p1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    .line 1784993
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->invalidate()V

    .line 1784994
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e()V

    goto :goto_0
.end method

.method public getCurrentSelectedValue()F
    .locals 1

    .prologue
    .line 1784995
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1784996
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x5f1041a1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1784997
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->a:LX/1wz;

    invoke-virtual {v1, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x237043d3

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setCurrentSelectedValue(F)V
    .locals 1

    .prologue
    .line 1784998
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->k:F

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->l:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 1784999
    :cond_0
    :goto_0
    return-void

    .line 1785000
    :cond_1
    iput p1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->j:F

    .line 1785001
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->invalidate()V

    .line 1785002
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e()V

    goto :goto_0
.end method

.method public setOnSeekBarChangeListener(LX/FcR;)V
    .locals 0

    .prologue
    .line 1785003
    iput-object p1, p0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->m:LX/FcR;

    .line 1785004
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e()V

    .line 1785005
    return-void
.end method
