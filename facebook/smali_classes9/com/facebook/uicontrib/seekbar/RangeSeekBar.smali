.class public Lcom/facebook/uicontrib/seekbar/RangeSeekBar;
.super Landroid/widget/FrameLayout;
.source ""

# interfaces
.implements LX/4Ba;
.implements LX/39D;
.implements LX/39E;


# instance fields
.field private a:LX/1wz;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;

.field private f:Landroid/graphics/Paint;

.field public g:I

.field private h:I

.field private i:I

.field private j:LX/BSA;

.field public k:F

.field public l:F

.field private m:F

.field private n:F

.field private o:LX/ACu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1785192
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 1785193
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1785194
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1785195
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->c()V

    .line 1785196
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1785197
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1785198
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->c()V

    .line 1785199
    return-void
.end method

.method private a(F)Z
    .locals 2

    .prologue
    .line 1785200
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getCenterY()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    const/high16 v5, 0x7fc00000    # NaNf

    const/4 v4, 0x1

    .line 1785201
    new-instance v0, LX/1wz;

    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1wz;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a:LX/1wz;

    .line 1785202
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a:LX/1wz;

    const/4 v1, 0x2

    new-array v1, v1, [LX/31M;

    const/4 v2, 0x0

    sget-object v3, LX/31M;->LEFT:LX/31M;

    aput-object v3, v1, v2

    sget-object v2, LX/31M;->RIGHT:LX/31M;

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, LX/1wz;->a([LX/31M;)V

    .line 1785203
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a:LX/1wz;

    .line 1785204
    iput-object p0, v0, LX/1wz;->q:LX/4Ba;

    .line 1785205
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a:LX/1wz;

    .line 1785206
    iput-object p0, v0, LX/1wz;->r:LX/39D;

    .line 1785207
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a:LX/1wz;

    .line 1785208
    iput-object p0, v0, LX/1wz;->s:LX/39E;

    .line 1785209
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1785210
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->c:Landroid/graphics/Paint;

    .line 1785211
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->c:Landroid/graphics/Paint;

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785212
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1785213
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->c:Landroid/graphics/Paint;

    const v2, 0x7f0b1a05

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1785214
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->b:Landroid/graphics/Paint;

    .line 1785215
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->b:Landroid/graphics/Paint;

    const v2, 0x7f0a0114

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785216
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->b:Landroid/graphics/Paint;

    const v2, 0x7f0b1a05

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1785217
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d:Landroid/graphics/Paint;

    .line 1785218
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d:Landroid/graphics/Paint;

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785219
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d:Landroid/graphics/Paint;

    const/16 v2, 0x7f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1785220
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1785221
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e:Landroid/graphics/Paint;

    .line 1785222
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e:Landroid/graphics/Paint;

    const v2, 0x7f0a00d5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785223
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1785224
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->f:Landroid/graphics/Paint;

    .line 1785225
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->f:Landroid/graphics/Paint;

    const v2, 0x7f0a00d2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1785226
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1785227
    const v1, 0x7f0b1a08

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    .line 1785228
    const v1, 0x7f0b1a07

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->h:I

    .line 1785229
    const v1, 0x7f0b1a06

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->i:I

    .line 1785230
    iput v5, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    .line 1785231
    iput v5, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    .line 1785232
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1785233
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->o:LX/ACu;

    if-eqz v0, :cond_0

    .line 1785234
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->o:LX/ACu;

    .line 1785235
    iget v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    move v1, v1

    .line 1785236
    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    move v2, v2

    .line 1785237
    invoke-interface {v0, v1, v2}, LX/ACu;->b(FF)V

    .line 1785238
    :cond_0
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1785239
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->o:LX/ACu;

    if-eqz v0, :cond_0

    .line 1785240
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->o:LX/ACu;

    .line 1785241
    iget v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    move v1, v1

    .line 1785242
    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    move v2, v2

    .line 1785243
    invoke-interface {v0, v1, v2}, LX/ACu;->a(FF)V

    .line 1785244
    :cond_0
    return-void
.end method

.method private g(FF)Z
    .locals 1

    .prologue
    .line 1785246
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a(F)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1785247
    const/4 v0, 0x0

    .line 1785248
    :goto_0
    return v0

    .line 1785249
    :cond_0
    invoke-direct {p0, p1}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->setCurrentThumb(F)V

    .line 1785250
    invoke-direct {p0, p1}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->setCurrentPosition(F)V

    .line 1785251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->j:LX/BSA;

    .line 1785252
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d()V

    .line 1785253
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getCenterY()I
    .locals 1

    .prologue
    .line 1785245
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method private getCurrentPosition()F
    .locals 2

    .prologue
    .line 1785271
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->j:LX/BSA;

    sget-object v1, LX/BSA;->START:LX/BSA;

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getStartThumbX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getEndThumbX()F

    move-result v0

    goto :goto_0
.end method

.method private getEndThumbX()F
    .locals 5

    .prologue
    .line 1785267
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    iget v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    .line 1785268
    iget v3, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    move v3, v3

    .line 1785269
    int-to-float v3, v3

    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getRightBound()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v0, v1, v2, v3, v4}, LX/0yq;->a(FFFFF)F

    move-result v0

    return v0
.end method

.method private getLeftBound()I
    .locals 1

    .prologue
    .line 1785270
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    return v0
.end method

.method private getRightBound()I
    .locals 2

    .prologue
    .line 1785266
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    sub-int/2addr v0, v1

    return v0
.end method

.method private getStartThumbX()F
    .locals 5

    .prologue
    .line 1785263
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    iget v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    .line 1785264
    iget v3, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    move v3, v3

    .line 1785265
    int-to-float v3, v3

    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getRightBound()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v0, v1, v2, v3, v4}, LX/0yq;->a(FFFFF)F

    move-result v0

    return v0
.end method

.method private setCurrentPosition(F)V
    .locals 4

    .prologue
    .line 1785254
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->j:LX/BSA;

    if-nez v0, :cond_0

    .line 1785255
    :goto_0
    return-void

    .line 1785256
    :cond_0
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    move v0, v0

    .line 1785257
    int-to-float v0, v0

    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getRightBound()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    iget v3, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    invoke-static {p1, v0, v1, v2, v3}, LX/0yq;->a(FFFFF)F

    move-result v0

    .line 1785258
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->j:LX/BSA;

    sget-object v2, LX/BSA;->START:LX/BSA;

    if-ne v1, v2, :cond_1

    .line 1785259
    iget v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    .line 1785260
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->invalidate()V

    .line 1785261
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e()V

    goto :goto_0

    .line 1785262
    :cond_1
    iget v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    invoke-static {v0, v1, v2}, LX/0yq;->b(FFF)F

    move-result v0

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    goto :goto_1
.end method

.method private setCurrentThumb(F)V
    .locals 3

    .prologue
    .line 1785117
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getEndThumbX()F

    move-result v0

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 1785118
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getStartThumbX()F

    move-result v1

    sub-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 1785119
    cmpl-float v2, v0, v1

    if-nez v2, :cond_1

    .line 1785120
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getStartThumbX()F

    move-result v0

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    sget-object v0, LX/BSA;->END:LX/BSA;

    :goto_0
    iput-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->j:LX/BSA;

    .line 1785121
    :goto_1
    return-void

    .line 1785122
    :cond_0
    sget-object v0, LX/BSA;->START:LX/BSA;

    goto :goto_0

    .line 1785123
    :cond_1
    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    sget-object v0, LX/BSA;->END:LX/BSA;

    :goto_2
    iput-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->j:LX/BSA;

    goto :goto_1

    :cond_2
    sget-object v0, LX/BSA;->START:LX/BSA;

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1785186
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->j:LX/BSA;

    .line 1785187
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d()V

    .line 1785188
    return-void
.end method

.method public final a(LX/31M;I)V
    .locals 1

    .prologue
    .line 1785189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->j:LX/BSA;

    .line 1785190
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d()V

    .line 1785191
    return-void
.end method

.method public final a(FF)Z
    .locals 1

    .prologue
    .line 1785114
    invoke-direct {p0, p2}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a(F)Z

    move-result v0

    return v0
.end method

.method public final a(FFLX/31M;)Z
    .locals 1

    .prologue
    .line 1785115
    invoke-direct {p0, p1}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->setCurrentThumb(F)V

    .line 1785116
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1785124
    return-void
.end method

.method public final b(FFLX/31M;)V
    .locals 1

    .prologue
    .line 1785125
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getCurrentPosition()F

    move-result v0

    add-float/2addr v0, p1

    invoke-direct {p0, v0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->setCurrentPosition(F)V

    .line 1785126
    return-void
.end method

.method public final b(FF)Z
    .locals 1

    .prologue
    .line 1785127
    const/4 v0, 0x1

    return v0
.end method

.method public final c(FF)Z
    .locals 1

    .prologue
    .line 1785128
    invoke-direct {p0, p1, p2}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g(FF)Z

    move-result v0

    return v0
.end method

.method public final d(FF)V
    .locals 0

    .prologue
    .line 1785129
    invoke-direct {p0, p1, p2}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g(FF)Z

    .line 1785130
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 1785131
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getCenterY()I

    move-result v0

    int-to-float v2, v0

    .line 1785132
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getStartThumbX()F

    move-result v6

    .line 1785133
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getEndThumbX()F

    move-result v7

    .line 1785134
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1785135
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    move v0, v0

    .line 1785136
    int-to-float v1, v0

    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->getRightBound()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->b:Landroid/graphics/Paint;

    move-object v0, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1785137
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1785138
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->g:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1785139
    iget-object v5, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, v6

    move v3, v7

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1785140
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->h:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1785141
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->h:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1785142
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->i:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v6, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1785143
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->i:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v7, v2, v0, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1785144
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1785145
    return-void
.end method

.method public final e(FF)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1785146
    cmpl-float v0, p1, p2

    if-ltz v0, :cond_1

    .line 1785147
    :cond_0
    :goto_0
    return-void

    .line 1785148
    :cond_1
    iput p1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    .line 1785149
    iput p2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    .line 1785150
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1785151
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    .line 1785152
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    .line 1785153
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e()V

    .line 1785154
    :cond_2
    const/4 v0, 0x0

    .line 1785155
    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    iget v3, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 1785156
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    move v0, v1

    .line 1785157
    :cond_3
    iget v2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    iget v3, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_4

    .line 1785158
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    iput v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    .line 1785159
    :goto_1
    if-eqz v1, :cond_0

    .line 1785160
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->invalidate()V

    .line 1785161
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e()V

    goto :goto_0

    :cond_4
    move v1, v0

    goto :goto_1
.end method

.method public final f(FF)V
    .locals 1

    .prologue
    .line 1785162
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->m:F

    cmpg-float v0, p1, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->n:F

    cmpl-float v0, p2, v0

    if-gtz v0, :cond_0

    cmpl-float v0, p1, p2

    if-lez v0, :cond_1

    .line 1785163
    :cond_0
    :goto_0
    return-void

    .line 1785164
    :cond_1
    iput p1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    .line 1785165
    iput p2, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    .line 1785166
    invoke-virtual {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->invalidate()V

    .line 1785167
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e()V

    goto :goto_0
.end method

.method public getRangeEndValue()F
    .locals 1

    .prologue
    .line 1785168
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    return v0
.end method

.method public getRangeStartValue()F
    .locals 1

    .prologue
    .line 1785169
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1785170
    iget-object v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a:LX/1wz;

    invoke-virtual {v0, p1}, LX/1wz;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1785171
    check-cast p1, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;

    .line 1785172
    invoke-virtual {p1}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1785173
    iget v0, p1, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;->a:F

    iget v1, p1, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;->b:F

    invoke-virtual {p0, v0, v1}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->f(FF)V

    .line 1785174
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1785175
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1785176
    new-instance v1, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1785177
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->k:F

    move v0, v0

    .line 1785178
    iput v0, v1, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;->a:F

    .line 1785179
    iget v0, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->l:F

    move v0, v0

    .line 1785180
    iput v0, v1, Lcom/facebook/uicontrib/seekbar/RangeSeekBar$SavedState;->b:F

    .line 1785181
    return-object v1
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0xcfaca7c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1785182
    iget-object v1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->a:LX/1wz;

    invoke-virtual {v1, p1}, LX/1wz;->b(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x50934bdc

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method

.method public setRangeSeekBarChangeListener(LX/ACu;)V
    .locals 0

    .prologue
    .line 1785183
    iput-object p1, p0, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->o:LX/ACu;

    .line 1785184
    invoke-direct {p0}, Lcom/facebook/uicontrib/seekbar/RangeSeekBar;->e()V

    .line 1785185
    return-void
.end method
