.class public Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;
.super LX/CO8;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CO8",
        "<",
        "LX/1oI",
        "<",
        "LX/1oH;",
        ">;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final e:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final a:LX/CNb;

.field private final b:LX/1De;

.field private final c:Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;

.field private final d:LX/1Ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1882660
    const-class v0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/CNb;LX/1De;LX/CNq;Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;)V
    .locals 1

    .prologue
    .line 1882716
    invoke-direct {p0}, LX/CO8;-><init>()V

    .line 1882717
    iput-object p1, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->a:LX/CNb;

    .line 1882718
    iput-object p4, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->c:Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;

    .line 1882719
    invoke-virtual {p3}, LX/CNq;->f()LX/0Or;

    move-result-object v0

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    iput-object v0, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->d:LX/1Ad;

    .line 1882720
    iput-object p2, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->b:LX/1De;

    .line 1882721
    return-void
.end method

.method public static a(Ljava/lang/String;)LX/1Up;
    .locals 2

    .prologue
    .line 1882710
    const/4 v0, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 1882711
    sget-object v0, LX/1Up;->g:LX/1Up;

    :goto_1
    return-object v0

    .line 1882712
    :sswitch_0
    const-string v1, "COVER"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "CONTAIN"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "STRETCH"

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 1882713
    :pswitch_0
    sget-object v0, LX/1Up;->g:LX/1Up;

    goto :goto_1

    .line 1882714
    :pswitch_1
    sget-object v0, LX/1Up;->c:LX/1Up;

    goto :goto_1

    .line 1882715
    :pswitch_2
    sget-object v0, LX/1Up;->a:LX/1Up;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x455f8d1b -> :sswitch_2
        0x3d55b97 -> :sswitch_0
        0x6382b0b4 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/1De;LX/CNb;Ljava/lang/String;LX/CNc;)LX/1Di;
    .locals 3

    .prologue
    .line 1882722
    check-cast p4, LX/CNq;

    invoke-virtual {p4}, LX/CNq;->f()LX/0Or;

    move-result-object v0

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 1882723
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v2

    const-string v1, "image-source"

    invoke-virtual {p2, v1}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImage;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v0

    sget-object v1, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    invoke-static {p3}, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->a(Ljava/lang/String;)LX/1Up;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 1882678
    check-cast p1, LX/1oI;

    .line 1882679
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->c:Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;

    if-eqz v0, :cond_5

    .line 1882680
    invoke-virtual {p1}, LX/1oI;->e()V

    .line 1882681
    invoke-virtual {p1}, LX/1oI;->c()V

    .line 1882682
    invoke-virtual {p1}, LX/1oI;->d()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1oH;

    const/4 p1, 0x0

    .line 1882683
    iget-object v1, v0, LX/1oH;->c:LX/1dc;

    if-eqz v1, :cond_0

    .line 1882684
    iget-object v1, v0, LX/1oH;->b:LX/1De;

    iget-object v2, v0, LX/1oH;->h:Landroid/graphics/drawable/Drawable;

    iget-object p0, v0, LX/1oH;->c:LX/1dc;

    invoke-static {v1, v2, p0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 1882685
    iput-object p1, v0, LX/1oH;->c:LX/1dc;

    .line 1882686
    iput-object p1, v0, LX/1oH;->h:Landroid/graphics/drawable/Drawable;

    .line 1882687
    iget-object v1, v0, LX/1oH;->a:LX/1af;

    invoke-virtual {v1, p1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1882688
    :cond_0
    iget-object v1, v0, LX/1oH;->d:LX/1dc;

    if-eqz v1, :cond_1

    .line 1882689
    iget-object v1, v0, LX/1oH;->b:LX/1De;

    iget-object v2, v0, LX/1oH;->i:Landroid/graphics/drawable/Drawable;

    iget-object p0, v0, LX/1oH;->d:LX/1dc;

    invoke-static {v1, v2, p0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 1882690
    iput-object p1, v0, LX/1oH;->i:Landroid/graphics/drawable/Drawable;

    .line 1882691
    iput-object p1, v0, LX/1oH;->d:LX/1dc;

    .line 1882692
    iget-object v1, v0, LX/1oH;->a:LX/1af;

    invoke-virtual {v1, p1}, LX/1af;->d(Landroid/graphics/drawable/Drawable;)V

    .line 1882693
    :cond_1
    iget-object v1, v0, LX/1oH;->e:LX/1dc;

    if-eqz v1, :cond_2

    .line 1882694
    iget-object v1, v0, LX/1oH;->b:LX/1De;

    iget-object v2, v0, LX/1oH;->j:Landroid/graphics/drawable/Drawable;

    iget-object p0, v0, LX/1oH;->e:LX/1dc;

    invoke-static {v1, v2, p0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 1882695
    iput-object p1, v0, LX/1oH;->j:Landroid/graphics/drawable/Drawable;

    .line 1882696
    iput-object p1, v0, LX/1oH;->e:LX/1dc;

    .line 1882697
    iget-object v1, v0, LX/1oH;->a:LX/1af;

    invoke-virtual {v1, p1}, LX/1af;->c(Landroid/graphics/drawable/Drawable;)V

    .line 1882698
    :cond_2
    iget-object v1, v0, LX/1oH;->f:LX/1dc;

    if-eqz v1, :cond_3

    .line 1882699
    iget-object v1, v0, LX/1oH;->b:LX/1De;

    iget-object v2, v0, LX/1oH;->k:Landroid/graphics/drawable/Drawable;

    iget-object p0, v0, LX/1oH;->f:LX/1dc;

    invoke-static {v1, v2, p0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 1882700
    iput-object p1, v0, LX/1oH;->k:Landroid/graphics/drawable/Drawable;

    .line 1882701
    iput-object p1, v0, LX/1oH;->f:LX/1dc;

    .line 1882702
    iget-object v1, v0, LX/1oH;->a:LX/1af;

    invoke-virtual {v1, p1}, LX/1af;->e(Landroid/graphics/drawable/Drawable;)V

    .line 1882703
    :cond_3
    iget-object v1, v0, LX/1oH;->g:LX/1dc;

    if-eqz v1, :cond_4

    .line 1882704
    iget-object v1, v0, LX/1oH;->b:LX/1De;

    iget-object v2, v0, LX/1oH;->l:Landroid/graphics/drawable/Drawable;

    iget-object p0, v0, LX/1oH;->g:LX/1dc;

    invoke-static {v1, v2, p0}, LX/1dc;->a(LX/1De;Ljava/lang/Object;LX/1dc;)V

    .line 1882705
    iput-object p1, v0, LX/1oH;->l:Landroid/graphics/drawable/Drawable;

    .line 1882706
    iput-object p1, v0, LX/1oH;->g:LX/1dc;

    .line 1882707
    iget-object v1, v0, LX/1oH;->a:LX/1af;

    invoke-virtual {v1, p1}, LX/1af;->f(Landroid/graphics/drawable/Drawable;)V

    .line 1882708
    :cond_4
    iput-object p1, v0, LX/1oH;->b:LX/1De;

    .line 1882709
    :cond_5
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1882661
    check-cast p1, LX/1oI;

    .line 1882662
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->c:Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;

    if-eqz v0, :cond_2

    .line 1882663
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->c:Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;

    iget-object v1, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->b:LX/1De;

    iget-object v2, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->a:LX/CNb;

    iget-object v3, p0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->d:LX/1Ad;

    move-object v4, p1

    move-object v5, p2

    .line 1882664
    const-string p0, "image-source"

    invoke-virtual {v2, p0}, LX/CNb;->d(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p0

    .line 1882665
    sget-object p1, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    .line 1882666
    instance-of p2, p0, Lcom/facebook/graphql/model/GraphQLImage;

    if-eqz p2, :cond_3

    .line 1882667
    check-cast p0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 1882668
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p0

    .line 1882669
    :goto_0
    iget-object p1, v0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;->c:Landroid/net/Uri;

    if-eqz p1, :cond_0

    iget-object p1, v0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;->c:Landroid/net/Uri;

    invoke-virtual {p1, p0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_1

    .line 1882670
    :cond_0
    invoke-virtual {v3, p0}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object p1

    sget-object p2, Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p1, p2}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object p1

    invoke-virtual {p1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object p1

    iput-object p1, v0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;->b:LX/1aZ;

    .line 1882671
    iput-object p0, v0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;->c:Landroid/net/Uri;

    .line 1882672
    :cond_1
    invoke-virtual {v4}, LX/1oI;->d()LX/1aY;

    move-result-object p0

    check-cast p0, LX/1oH;

    .line 1882673
    iput-object v1, p0, LX/1oH;->b:LX/1De;

    .line 1882674
    invoke-static {v5}, Lcom/facebook/nativetemplates/fb/images/NTNetworkImage;->a(Ljava/lang/String;)LX/1Up;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/1oH;->a(LX/1Up;)V

    .line 1882675
    invoke-virtual {v4}, LX/1oI;->b()V

    .line 1882676
    iget-object p0, v0, Lcom/facebook/nativetemplates/fb/images/NTNetworkImageController;->b:LX/1aZ;

    invoke-virtual {v4, p0}, LX/1oI;->a(LX/1aZ;)V

    .line 1882677
    :cond_2
    return-void

    :cond_3
    move-object p0, p1

    goto :goto_0
.end method
