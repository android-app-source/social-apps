.class public Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/2d1;

.field private final e:LX/3j4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/2d1;LX/3j4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1882770
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1882771
    iput-object p2, p0, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;->d:LX/2d1;

    .line 1882772
    iput-object p3, p0, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;->e:LX/3j4;

    .line 1882773
    return-void
.end method

.method private a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/reaction/common/ReactionUnitComponentNode;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1882766
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 1882767
    invoke-interface {v0}, LX/9uc;->bG()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v0

    invoke-static {v0}, LX/5eL;->a(Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;)Lcom/facebook/graphql/model/GraphQLNativeTemplateView;

    move-result-object v0

    .line 1882768
    iget-object v1, p0, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;->e:LX/3j4;

    check-cast p2, LX/1Pq;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v1, v0, p2, p1, v2}, LX/3j4;->a(Lcom/facebook/graphql/model/GraphQLNativeTemplateView;LX/1Pq;LX/0jW;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X1;

    move-result-object v0

    .line 1882769
    new-instance v1, LX/COD;

    invoke-direct {v1, p0, v0}, LX/COD;-><init>(Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;LX/1X1;)V

    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;
    .locals 6

    .prologue
    .line 1882755
    const-class v1, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;

    monitor-enter v1

    .line 1882756
    :try_start_0
    sget-object v0, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1882757
    sput-object v2, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1882758
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1882759
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1882760
    new-instance p0, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2d1;->a(LX/0QB;)LX/2d1;

    move-result-object v4

    check-cast v4, LX/2d1;

    invoke-static {v0}, LX/3j4;->a(LX/0QB;)LX/3j4;

    move-result-object v5

    check-cast v5, LX/3j4;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;-><init>(Landroid/content/Context;LX/2d1;LX/3j4;)V

    .line 1882761
    move-object v0, p0

    .line 1882762
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1882763
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1882764
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1882765
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1882774
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p2, p3}, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1882754
    check-cast p2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-direct {p0, p2, p3}, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;->a(Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1PW;)Z
    .locals 1

    .prologue
    .line 1882753
    check-cast p1, LX/1Pn;

    invoke-virtual {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;->a(LX/1Pn;)Z

    move-result v0

    return v0
.end method

.method public final a(LX/1Pn;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .prologue
    .line 1882752
    iget-object v0, p0, Lcom/facebook/nativetemplates/fb/multirow/NativeTemplateUnitComponentPartDefinition;->d:LX/2d1;

    invoke-virtual {v0}, LX/2d1;->b()Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1882749
    check-cast p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 1882750
    iget-object v0, p1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->b:LX/9uc;

    move-object v0, v0

    .line 1882751
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/9uc;->bG()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, LX/9uc;->bG()Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nativetemplates/fb/graphql/NativeTemplateFragmentsModels$NativeTemplateViewFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1882748
    const/4 v0, 0x0

    return-object v0
.end method
