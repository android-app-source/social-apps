.class public final Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:[Ljava/lang/String;

.field public final synthetic c:[Ljava/lang/Object;

.field public final synthetic d:LX/1Sc;


# direct methods
.method public constructor <init>(LX/1Sc;J[Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1779654
    iput-object p1, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->d:LX/1Sc;

    iput-wide p2, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->a:J

    iput-object p4, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->b:[Ljava/lang/String;

    iput-object p5, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->c:[Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1779655
    iget-object v0, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->d:LX/1Sc;

    iget-object v0, v0, LX/1Sc;->a:LX/1Sd;

    invoke-virtual {v0}, LX/1Sd;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1779656
    const v2, -0x30575434

    invoke-static {v0, v2}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1779657
    :try_start_0
    const-string v3, "_id=?"

    .line 1779658
    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-wide v6, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    .line 1779659
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 1779660
    :goto_0
    iget-object v5, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_0

    .line 1779661
    iget-object v5, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->b:[Ljava/lang/String;

    aget-object v5, v5, v1

    iget-object v6, p0, Lcom/facebook/saved2/ui/mutator/Saved2DbMutator$2;->c:[Ljava/lang/Object;

    aget-object v6, v6, v1

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1779662
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1779663
    :cond_0
    const-string v1, "item"

    const/4 v5, 0x5

    invoke-virtual/range {v0 .. v5}, Landroid/database/sqlite/SQLiteDatabase;->updateWithOnConflict(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;I)I

    .line 1779664
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1779665
    const v1, -0x4346248

    invoke-static {v0, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 1779666
    sget-object v0, LX/AUM;->a:LX/AUM;

    move-object v0, v0

    .line 1779667
    const-class v1, LX/BNx;

    invoke-virtual {v0, v1}, LX/AUM;->a(Ljava/lang/Object;)V

    .line 1779668
    return-void

    .line 1779669
    :catchall_0
    move-exception v1

    const v2, 0x24888f24

    invoke-static {v0, v2}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v1
.end method
