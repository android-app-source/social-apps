.class public final Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4f9baad
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1745183
    const-class v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1745159
    const-class v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1745160
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1745161
    return-void
.end method

.method private a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1745162
    iget-object v0, p0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;->e:Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    iput-object v0, p0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;->e:Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    .line 1745163
    iget-object v0, p0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;->e:Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1745164
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1745165
    invoke-direct {p0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1745166
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1745167
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1745168
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1745169
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1745170
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1745171
    invoke-direct {p0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1745172
    invoke-direct {p0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    .line 1745173
    invoke-direct {p0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;->a()Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1745174
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;

    .line 1745175
    iput-object v0, v1, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;->e:Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel$ArticleModel;

    .line 1745176
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1745177
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1745178
    new-instance v0, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;

    invoke-direct {v0}, Lcom/facebook/instantarticles/model/graphql/InstantArticlesGraphQlModels$InstantArticleLastReadBlockMutationModel;-><init>()V

    .line 1745179
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1745180
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1745181
    const v0, 0x4f1d2017

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1745182
    const v0, 0x5d05cbc1

    return v0
.end method
