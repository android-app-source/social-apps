.class public Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;
.super Landroid/support/v7/widget/LinearLayoutCompat;
.source ""


# instance fields
.field public a:I

.field public b:LX/Bc8;

.field public c:Z

.field public d:LX/Bc9;

.field private e:LX/BcA;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1801578
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1801579
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1801573
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/LinearLayoutCompat;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1801574
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    .line 1801575
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->c:Z

    .line 1801576
    invoke-direct {p0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->b()V

    .line 1801577
    return-void
.end method

.method public static a$redex0(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;IZ)V
    .locals 2

    .prologue
    .line 1801569
    invoke-virtual {p0, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1801570
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/widget/Checkable;

    if-eqz v1, :cond_0

    .line 1801571
    check-cast v0, Landroid/widget/Checkable;

    invoke-interface {v0, p2}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 1801572
    :cond_0
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1801565
    new-instance v0, LX/Bc8;

    invoke-direct {v0, p0}, LX/Bc8;-><init>(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;)V

    iput-object v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->b:LX/Bc8;

    .line 1801566
    new-instance v0, LX/BcA;

    invoke-direct {v0, p0}, LX/BcA;-><init>(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;)V

    iput-object v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->e:LX/BcA;

    .line 1801567
    iget-object v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->e:LX/BcA;

    invoke-super {p0, v0}, Landroid/support/v7/widget/LinearLayoutCompat;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 1801568
    return-void
.end method

.method public static setCheckedId(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;I)V
    .locals 2

    .prologue
    .line 1801559
    iput p1, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    .line 1801560
    iget-object v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    if-eqz v0, :cond_0

    .line 1801561
    iget-object v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    iget v1, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    invoke-interface {v0, p0, v1}, LX/Bc9;->a(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;I)V

    .line 1801562
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1801563
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a(I)V

    .line 1801564
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1801580
    if-eq p1, v2, :cond_0

    iget v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    if-ne p1, v0, :cond_0

    .line 1801581
    :goto_0
    return-void

    .line 1801582
    :cond_0
    iget v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    if-eq v0, v2, :cond_1

    .line 1801583
    iget v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a$redex0(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;IZ)V

    .line 1801584
    :cond_1
    if-eq p1, v2, :cond_2

    .line 1801585
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a$redex0(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;IZ)V

    .line 1801586
    :cond_2
    invoke-static {p0, p1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->setCheckedId(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;I)V

    goto :goto_0
.end method

.method public final addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1801536
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/widget/LinearLayoutCompat;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1801537
    instance-of v0, p1, Landroid/widget/Checkable;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 1801538
    check-cast v0, Landroid/widget/Checkable;

    .line 1801539
    invoke-interface {v0}, Landroid/widget/Checkable;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1801540
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->c:Z

    .line 1801541
    iget v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1801542
    iget v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    invoke-static {p0, v0, v2}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a$redex0(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;IZ)V

    .line 1801543
    :cond_0
    iput-boolean v2, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->c:Z

    .line 1801544
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->setCheckedId(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;I)V

    .line 1801545
    :cond_1
    return-void
.end method

.method public getCheckedRadioButtonId()I
    .locals 1

    .prologue
    .line 1801546
    iget v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    return v0
.end method

.method public final onFinishInflate()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/16 v0, 0x2c

    const v1, 0xbccf5ae

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1801547
    invoke-super {p0}, Landroid/support/v7/widget/LinearLayoutCompat;->onFinishInflate()V

    .line 1801548
    iget v1, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 1801549
    iput-boolean v3, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->c:Z

    .line 1801550
    iget v1, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    invoke-static {p0, v1, v3}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a$redex0(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;IZ)V

    .line 1801551
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->c:Z

    .line 1801552
    iget v1, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->a:I

    invoke-static {p0, v1}, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->setCheckedId(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;I)V

    .line 1801553
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x308baf16

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnCheckedChangeRadioGroupListener(LX/Bc9;)V
    .locals 0

    .prologue
    .line 1801554
    iput-object p1, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->d:LX/Bc9;

    .line 1801555
    return-void
.end method

.method public setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V
    .locals 1

    .prologue
    .line 1801556
    iget-object v0, p0, Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;->e:LX/BcA;

    .line 1801557
    iput-object p1, v0, LX/BcA;->b:Landroid/view/ViewGroup$OnHierarchyChangeListener;

    .line 1801558
    return-void
.end method
