.class public Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/ExecutorService;

.field public final d:LX/1HI;

.field public final e:LX/1FZ;

.field public final f:LX/AVT;

.field public g:LX/15i;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:[F
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/1FJ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1FJ",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/1bA;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1680106
    const-class v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1HI;LX/1FZ;LX/AVT;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1680107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1680108
    iput-object p1, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->b:LX/0tX;

    .line 1680109
    iput-object p2, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->c:Ljava/util/concurrent/ExecutorService;

    .line 1680110
    iput-object p3, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->d:LX/1HI;

    .line 1680111
    iput-object p4, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->e:LX/1FZ;

    .line 1680112
    iput-object p5, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->f:LX/AVT;

    .line 1680113
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;
    .locals 7

    .prologue
    .line 1680114
    new-instance v1, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v4

    check-cast v4, LX/1HI;

    invoke-static {p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v5

    check-cast v5, LX/1FZ;

    invoke-static {p0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v6

    check-cast v6, LX/AVT;

    invoke-direct/range {v1 .. v6}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1HI;LX/1FZ;LX/AVT;)V

    .line 1680115
    move-object v0, v1

    .line 1680116
    return-object v0
.end method


# virtual methods
.method public final a(LX/AWK;)Z
    .locals 1

    .prologue
    .line 1680117
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->i:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->i:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->j:[F

    if-nez v0, :cond_1

    .line 1680118
    :cond_0
    const/4 v0, 0x0

    .line 1680119
    :goto_0
    return v0

    .line 1680120
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, LX/AWK;->a(Landroid/graphics/Bitmap;)V

    .line 1680121
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->i:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/AWK;->a(Landroid/net/Uri;)V

    .line 1680122
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioOnlyCoverPhotoHelper;->j:[F

    .line 1680123
    iget-object p0, p1, LX/AWK;->h:Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;

    invoke-virtual {p0, v0}, Lcom/facebook/videocodec/effects/renderers/OverlayRenderer;->a([F)V

    .line 1680124
    const/4 v0, 0x1

    goto :goto_0
.end method
