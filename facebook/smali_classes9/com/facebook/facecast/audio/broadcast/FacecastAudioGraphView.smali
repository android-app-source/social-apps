.class public Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private b:Ljava/util/ArrayList;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Path;

.field private e:Landroid/graphics/RectF;

.field private f:F

.field private g:F

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1680043
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1680044
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1680041
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1680042
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1680035
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1680036
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->h:Z

    .line 1680037
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    .line 1680038
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->c:Landroid/graphics/Paint;

    .line 1680039
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0384

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1680040
    return-void
.end method

.method private a(I)F
    .locals 1

    .prologue
    .line 1680034
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    return v0
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1680022
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 1680023
    :cond_0
    return-void

    .line 1680024
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1680025
    iput-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    .line 1680026
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1680027
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    invoke-direct {p0, v2}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a(I)F

    move-result v4

    iget v5, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->g:F

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1680028
    invoke-direct {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->c()V

    .line 1680029
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    iget v4, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->f:F

    iget v5, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->g:F

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1680030
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    invoke-direct {p0, v2}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a(I)F

    move-result v4

    iget v5, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->g:F

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1680031
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1680032
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1680033
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private b(I)F
    .locals 1

    .prologue
    .line 1680016
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    return v0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 1680045
    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    int-to-float v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->e:Landroid/graphics/RectF;

    .line 1680046
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->e:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->f:F

    .line 1680047
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->e:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->e:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->g:F

    .line 1680048
    return-void
.end method

.method private c(I)I
    .locals 1

    .prologue
    .line 1680017
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 1680018
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    .line 1680019
    :cond_0
    :goto_0
    return p1

    .line 1680020
    :cond_1
    if-gez p1, :cond_0

    .line 1680021
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private c()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    const v9, 0x3e19999a    # 0.15f

    .line 1679998
    iget-object v1, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1679999
    :cond_0
    return-void

    .line 1680000
    :cond_1
    iget-object v1, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    invoke-direct {p0, v0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a(I)F

    move-result v2

    invoke-direct {p0, v0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b(I)F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    move v7, v0

    .line 1680001
    :goto_0
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v7, v0, :cond_0

    .line 1680002
    invoke-direct {p0, v7}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a(I)F

    move-result v0

    .line 1680003
    invoke-direct {p0, v7}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b(I)F

    move-result v2

    .line 1680004
    add-int/lit8 v1, v7, 0x1

    invoke-direct {p0, v1}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a(I)F

    move-result v5

    .line 1680005
    add-int/lit8 v1, v7, 0x1

    invoke-direct {p0, v1}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b(I)F

    move-result v6

    .line 1680006
    add-int/lit8 v1, v7, -0x1

    invoke-direct {p0, v1}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->c(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a(I)F

    move-result v1

    sub-float v1, v5, v1

    .line 1680007
    add-int/lit8 v3, v7, -0x1

    invoke-direct {p0, v3}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->c(I)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b(I)F

    move-result v3

    sub-float v3, v6, v3

    .line 1680008
    add-int/lit8 v4, v7, 0x2

    invoke-direct {p0, v4}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->c(I)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a(I)F

    move-result v4

    sub-float/2addr v4, v0

    .line 1680009
    add-int/lit8 v8, v7, 0x2

    invoke-direct {p0, v8}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->c(I)I

    move-result v8

    invoke-direct {p0, v8}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b(I)F

    move-result v8

    sub-float/2addr v8, v2

    .line 1680010
    mul-float/2addr v1, v9

    add-float/2addr v1, v0

    .line 1680011
    mul-float v0, v9, v3

    add-float/2addr v2, v0

    .line 1680012
    mul-float v0, v9, v4

    sub-float v3, v5, v0

    .line 1680013
    mul-float v0, v9, v8

    sub-float v4, v6, v0

    .line 1680014
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->d:Landroid/graphics/Path;

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1680015
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1679995
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->h:Z

    .line 1679996
    invoke-virtual {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->invalidate()V

    .line 1679997
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1679991
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1679992
    iget-object v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->h:Z

    if-nez v0, :cond_0

    .line 1679993
    invoke-direct {p0, p1}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a(Landroid/graphics/Canvas;)V

    .line 1679994
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x2e06198d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1679987
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 1679988
    if-ne p3, p1, :cond_0

    if-eq p4, p2, :cond_1

    .line 1679989
    :cond_0
    invoke-direct {p0}, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->b()V

    .line 1679990
    :cond_1
    const/16 v1, 0x2d

    const v2, 0xd6a46ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Double;",
            "Ljava/lang/Double;",
            ">;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1679985
    iput-object p1, p0, Lcom/facebook/facecast/audio/broadcast/FacecastAudioGraphView;->a:Ljava/util/ArrayList;

    .line 1679986
    return-void
.end method
