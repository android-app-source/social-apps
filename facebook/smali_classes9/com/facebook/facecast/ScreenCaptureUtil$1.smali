.class public final Lcom/facebook/facecast/ScreenCaptureUtil$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/graphics/Bitmap;

.field public final synthetic c:[F

.field public final synthetic d:I

.field public final synthetic e:I

.field public final synthetic f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic g:LX/AVQ;

.field public final synthetic h:LX/AVR;


# direct methods
.method public constructor <init>(LX/AVR;Ljava/lang/String;Landroid/graphics/Bitmap;[FIILjava/util/concurrent/atomic/AtomicBoolean;LX/AVQ;)V
    .locals 0

    .prologue
    .line 1679713
    iput-object p1, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->h:LX/AVR;

    iput-object p2, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->b:Landroid/graphics/Bitmap;

    iput-object p4, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->c:[F

    iput p5, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->d:I

    iput p6, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->e:I

    iput-object p7, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p8, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->g:LX/AVQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 1679693
    const/4 v2, 0x0

    .line 1679694
    :try_start_0
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1679695
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1679696
    :try_start_1
    iget-object v0, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->b:Landroid/graphics/Bitmap;

    iget v3, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->d:I

    iget v4, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->e:I

    .line 1679697
    const/4 v7, 0x0

    .line 1679698
    new-instance v11, Landroid/graphics/Matrix;

    invoke-direct {v11}, Landroid/graphics/Matrix;-><init>()V

    .line 1679699
    const/high16 v6, -0x40800000    # -1.0f

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v11, v6, v8}, Landroid/graphics/Matrix;->preScale(FF)Z

    .line 1679700
    const/high16 v6, 0x43340000    # 180.0f

    div-int/lit8 v8, v3, 0x2

    int-to-float v8, v8

    div-int/lit8 v9, v4, 0x2

    int-to-float v9, v9

    invoke-virtual {v11, v6, v8, v9}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    move-object v6, v0

    move v8, v7

    move v9, v3

    move v10, v4

    move v12, v7

    .line 1679701
    invoke-static/range {v6 .. v12}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v6

    move-object v5, v6

    .line 1679702
    move-object v0, v5

    .line 1679703
    sget-object v2, LX/AVR;->b:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x5a

    invoke-virtual {v0, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 1679704
    iget-object v2, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    .line 1679705
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1679706
    iget-object v0, p0, Lcom/facebook/facecast/ScreenCaptureUtil$1;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1679707
    invoke-static {v1}, LX/AVR;->b(Ljava/io/BufferedOutputStream;)V

    .line 1679708
    :goto_0
    return-void

    .line 1679709
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 1679710
    :goto_1
    :try_start_2
    sget-object v2, LX/AVR;->a:Ljava/lang/String;

    const-string v3, "error while saving to bitmap"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1679711
    invoke-static {v1}, LX/AVR;->b(Ljava/io/BufferedOutputStream;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_2
    invoke-static {v1}, LX/AVR;->b(Ljava/io/BufferedOutputStream;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    .line 1679712
    :catch_1
    move-exception v0

    goto :goto_1
.end method
