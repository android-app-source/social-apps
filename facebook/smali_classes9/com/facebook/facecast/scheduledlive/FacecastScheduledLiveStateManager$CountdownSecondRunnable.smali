.class public final Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Abg;


# direct methods
.method public constructor <init>(LX/Abg;)V
    .locals 0

    .prologue
    .line 1691099
    iput-object p1, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->a:LX/Abg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1691100
    iget-object v0, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->a:LX/Abg;

    iget-object v0, v0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1691101
    iget-object v0, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->a:LX/Abg;

    iget-object v1, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->a:LX/Abg;

    iget-object v1, v1, LX/Abg;->a:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/Abg;->b$redex0(LX/Abg;J)J

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 1691102
    iget-object v2, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->a:LX/Abg;

    iget-object v2, v2, LX/Abg;->g:LX/Abf;

    if-eqz v2, :cond_0

    .line 1691103
    iget-object v2, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->a:LX/Abg;

    iget-object v2, v2, LX/Abg;->g:LX/Abf;

    invoke-interface {v2, v0, v1}, LX/Abf;->a(J)V

    .line 1691104
    :cond_0
    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    .line 1691105
    iget-object v2, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->a:LX/Abg;

    iget-object v2, v2, LX/Abg;->b:Landroid/os/Handler;

    invoke-static {v2, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1691106
    iget-object v2, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->a:LX/Abg;

    iget-object v2, v2, LX/Abg;->b:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    rem-long/2addr v0, v4

    const v3, 0x4e76ddf6

    invoke-static {v2, p0, v0, v1, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1691107
    :cond_1
    return-void
.end method
