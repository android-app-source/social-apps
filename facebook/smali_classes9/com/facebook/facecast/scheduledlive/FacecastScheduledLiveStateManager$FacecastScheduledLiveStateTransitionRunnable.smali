.class public final Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/Abg;


# direct methods
.method public constructor <init>(LX/Abg;)V
    .locals 0

    .prologue
    .line 1691117
    iput-object p1, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 1691118
    iget-object v0, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v0, v0, LX/Abg;->h:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1691119
    iget-object v0, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v0, v0, LX/Abg;->i:LX/Abe;

    .line 1691120
    iget-object v1, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    invoke-static {v1}, LX/Abg;->d(LX/Abg;)V

    .line 1691121
    iget-object v1, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v1, v1, LX/Abg;->i:LX/Abe;

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v0, v0, LX/Abg;->g:LX/Abf;

    if-eqz v0, :cond_0

    .line 1691122
    iget-object v0, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v0, v0, LX/Abg;->g:LX/Abf;

    iget-object v1, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v1, v1, LX/Abg;->i:LX/Abe;

    invoke-interface {v0, v1}, LX/Abf;->a(LX/Abe;)V

    .line 1691123
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v0, v0, LX/Abg;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 1691124
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v4, v4, LX/Abg;->i:LX/Abe;

    sget-object v5, LX/Abe;->PRELOBBY:LX/Abe;

    if-ne v4, v5, :cond_4

    .line 1691125
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    invoke-static {v4, v0, v1}, LX/Abg;->a$redex0(LX/Abg;J)J

    move-result-wide v0

    .line 1691126
    :goto_0
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-boolean v4, v4, LX/Abg;->f:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v4, v4, LX/Abg;->i:LX/Abe;

    sget-object v5, LX/Abe;->COUNTDOWN_STARTED:LX/Abe;

    if-eq v4, v5, :cond_1

    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v4, v4, LX/Abg;->i:LX/Abe;

    sget-object v5, LX/Abe;->COUNTDOWN_ENDED:LX/Abe;

    if-ne v4, v5, :cond_2

    .line 1691127
    :cond_1
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v4, v4, LX/Abg;->d:Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;

    invoke-virtual {v4}, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$CountdownSecondRunnable;->run()V

    .line 1691128
    :cond_2
    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    .line 1691129
    iget-object v2, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v2, v2, LX/Abg;->b:Landroid/os/Handler;

    invoke-static {v2, p0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1691130
    iget-object v2, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v2, v2, LX/Abg;->b:Landroid/os/Handler;

    const v3, 0x1aee857d

    invoke-static {v2, p0, v0, v1, v3}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1691131
    :cond_3
    return-void

    .line 1691132
    :cond_4
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v4, v4, LX/Abg;->i:LX/Abe;

    sget-object v5, LX/Abe;->COUNTDOWN_STARTED:LX/Abe;

    if-ne v4, v5, :cond_5

    .line 1691133
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    invoke-static {v4, v0, v1}, LX/Abg;->b$redex0(LX/Abg;J)J

    move-result-wide v0

    goto :goto_0

    .line 1691134
    :cond_5
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v4, v4, LX/Abg;->i:LX/Abe;

    sget-object v5, LX/Abe;->COUNTDOWN_ENDED:LX/Abe;

    if-ne v4, v5, :cond_6

    .line 1691135
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    invoke-static {v4, v0, v1}, LX/Abg;->c$redex0(LX/Abg;J)J

    move-result-wide v0

    goto :goto_0

    .line 1691136
    :cond_6
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    iget-object v4, v4, LX/Abg;->i:LX/Abe;

    sget-object v5, LX/Abe;->RUNNING_LATE:LX/Abe;

    if-ne v4, v5, :cond_7

    .line 1691137
    iget-object v4, p0, Lcom/facebook/facecast/scheduledlive/FacecastScheduledLiveStateManager$FacecastScheduledLiveStateTransitionRunnable;->a:LX/Abg;

    invoke-static {v4, v0, v1}, LX/Abg;->d$redex0(LX/Abg;J)J

    move-result-wide v0

    goto :goto_0

    :cond_7
    move-wide v0, v2

    goto :goto_0
.end method
