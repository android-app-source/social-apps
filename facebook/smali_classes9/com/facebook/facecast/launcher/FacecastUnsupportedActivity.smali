.class public Lcom/facebook/facecast/launcher/FacecastUnsupportedActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1682786
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecast/launcher/FacecastUnsupportedActivity;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    iput-object v0, p0, Lcom/facebook/facecast/launcher/FacecastUnsupportedActivity;->p:LX/03V;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    .line 1682771
    invoke-static {p0, p0}, Lcom/facebook/facecast/launcher/FacecastUnsupportedActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1682772
    invoke-virtual {p0}, Lcom/facebook/facecast/launcher/FacecastUnsupportedActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "error_exception"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    .line 1682773
    if-nez v0, :cond_0

    move-object v1, v2

    .line 1682774
    :goto_0
    const-string v3, "Launched FacecastUnsupportedActivity"

    invoke-static {v3, v1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 1682775
    iput v4, v1, LX/0VK;->e:I

    .line 1682776
    move-object v1, v1

    .line 1682777
    iput-object v0, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1682778
    move-object v0, v1

    .line 1682779
    iput-boolean v4, v0, LX/0VK;->d:Z

    .line 1682780
    move-object v0, v0

    .line 1682781
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1682782
    iget-object v1, p0, Lcom/facebook/facecast/launcher/FacecastUnsupportedActivity;->p:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    .line 1682783
    new-instance v0, LX/0ju;

    invoke-direct {v0, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    const v1, 0x7f080016

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080c9e

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f080ca4

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    new-instance v1, LX/AWN;

    invoke-direct {v1, p0}, LX/AWN;-><init>(Lcom/facebook/facecast/launcher/FacecastUnsupportedActivity;)V

    invoke-virtual {v0, v1}, LX/0ju;->a(Landroid/content/DialogInterface$OnDismissListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 1682784
    return-void

    .line 1682785
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
