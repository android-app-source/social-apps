.class public final Lcom/facebook/facecast/FacecastCopyrightMonitor$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;",
            ">;>;"
        }
    .end annotation
.end field

.field public b:J

.field public c:Z

.field public final synthetic d:LX/AV5;


# direct methods
.method public constructor <init>(LX/AV5;)V
    .locals 1

    .prologue
    .line 1678779
    iput-object p1, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->d:LX/AV5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1678780
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->c:Z

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1678781
    iget-object v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1678782
    iget-object v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->d:LX/AV5;

    iget-object v0, v0, LX/AV5;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    .line 1678783
    :cond_0
    :goto_0
    return-void

    .line 1678784
    :cond_1
    new-instance v0, LX/Aan;

    invoke-direct {v0}, LX/Aan;-><init>()V

    move-object v0, v0

    .line 1678785
    const-string v1, "targetID"

    iget-object v2, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->d:LX/AV5;

    iget-object v2, v2, LX/AV5;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1678786
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1678787
    iget-object v1, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->d:LX/AV5;

    iget-object v1, v1, LX/AV5;->e:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1678788
    iget-object v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->d:LX/AV5;

    iget-object v1, v1, LX/AV5;->k:LX/AV4;

    iget-object v2, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->d:LX/AV5;

    iget-object v2, v2, LX/AV5;->d:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1678789
    iget-object v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->d:LX/AV5;

    iget-object v0, v0, LX/AV5;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->b:J

    .line 1678790
    iget-boolean v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->c:Z

    if-eqz v0, :cond_0

    .line 1678791
    iget-object v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->d:LX/AV5;

    const-string v1, "copyright_monitor_fetching"

    invoke-static {v0, v1}, LX/AV5;->a$redex0(LX/AV5;Ljava/lang/String;)V

    .line 1678792
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facecast/FacecastCopyrightMonitor$1;->c:Z

    goto :goto_0
.end method
