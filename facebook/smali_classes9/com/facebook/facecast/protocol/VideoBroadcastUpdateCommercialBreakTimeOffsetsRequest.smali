.class public Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1690653
    new-instance v0, LX/AbP;

    invoke-direct {v0}, LX/AbP;-><init>()V

    sput-object v0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1690654
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690655
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->a:Ljava/lang/String;

    .line 1690656
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1690657
    new-array v0, v0, [F

    iput-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->b:[F

    .line 1690658
    iget-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->b:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readFloatArray([F)V

    .line 1690659
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[F)V
    .locals 0

    .prologue
    .line 1690660
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690661
    iput-object p1, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->a:Ljava/lang/String;

    .line 1690662
    iput-object p2, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->b:[F

    .line 1690663
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1690664
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1690665
    iget-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1690666
    iget-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateCommercialBreakTimeOffsetsRequest;->b:[F

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeFloatArray([F)V

    .line 1690667
    return-void
.end method
