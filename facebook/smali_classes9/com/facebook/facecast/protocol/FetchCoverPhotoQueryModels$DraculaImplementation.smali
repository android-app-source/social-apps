.class public final Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1689183
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1689184
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1689208
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1689209
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1689185
    if-nez p1, :cond_0

    .line 1689186
    :goto_0
    return v0

    .line 1689187
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1689188
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1689189
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1689190
    const v2, 0x51aa9a16

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1689191
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1689192
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1689193
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1689194
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1689195
    const v2, 0xd9414c7

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1689196
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1689197
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1689198
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1689199
    :sswitch_2
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1689200
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1689201
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1689202
    invoke-virtual {p0, p1, v6, v0}, LX/15i;->a(III)I

    move-result v3

    .line 1689203
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1689204
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1689205
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1689206
    invoke-virtual {p3, v6, v3, v0}, LX/186;->a(III)V

    .line 1689207
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xd9414c7 -> :sswitch_2
        0x2882f756 -> :sswitch_0
        0x51aa9a16 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1689140
    new-instance v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1689176
    sparse-switch p2, :sswitch_data_0

    .line 1689177
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1689178
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1689179
    const v1, 0x51aa9a16

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1689180
    :goto_0
    :sswitch_1
    return-void

    .line 1689181
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1689182
    const v1, 0xd9414c7

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0xd9414c7 -> :sswitch_1
        0x2882f756 -> :sswitch_0
        0x51aa9a16 -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1689170
    if-eqz p1, :cond_0

    .line 1689171
    invoke-static {p0, p1, p2}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1689172
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;

    .line 1689173
    if-eq v0, v1, :cond_0

    .line 1689174
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1689175
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1689169
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1689167
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1689168
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1689210
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1689211
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1689212
    :cond_0
    iput-object p1, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1689213
    iput p2, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->b:I

    .line 1689214
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1689166
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1689165
    new-instance v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1689162
    iget v0, p0, LX/1vt;->c:I

    .line 1689163
    move v0, v0

    .line 1689164
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1689159
    iget v0, p0, LX/1vt;->c:I

    .line 1689160
    move v0, v0

    .line 1689161
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1689156
    iget v0, p0, LX/1vt;->b:I

    .line 1689157
    move v0, v0

    .line 1689158
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689153
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1689154
    move-object v0, v0

    .line 1689155
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1689144
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1689145
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1689146
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1689147
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1689148
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1689149
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1689150
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1689151
    invoke-static {v3, v9, v2}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1689152
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1689141
    iget v0, p0, LX/1vt;->c:I

    .line 1689142
    move v0, v0

    .line 1689143
    return v0
.end method
