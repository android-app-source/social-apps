.class public Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1690530
    new-instance v0, LX/AbI;

    invoke-direct {v0}, LX/AbI;-><init>()V

    sput-object v0, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1690531
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690532
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->a:Ljava/lang/String;

    .line 1690533
    const-class v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    iput-object v0, p0, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->b:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    .line 1690534
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;)V
    .locals 0

    .prologue
    .line 1690535
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690536
    iput-object p1, p0, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->a:Ljava/lang/String;

    .line 1690537
    iput-object p2, p0, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->b:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    .line 1690538
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1690539
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1690540
    iget-object v0, p0, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1690541
    iget-object v0, p0, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;->b:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1690542
    return-void
.end method
