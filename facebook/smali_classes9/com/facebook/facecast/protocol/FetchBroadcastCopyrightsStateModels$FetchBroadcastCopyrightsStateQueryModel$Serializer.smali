.class public final Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1689089
    const-class v0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;

    new-instance v1, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1689090
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1689092
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1689093
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1689094
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x1

    .line 1689095
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1689096
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1689097
    if-eqz v2, :cond_0

    .line 1689098
    const-string p0, "copyrights_violation_dialog_state"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689099
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1689100
    :cond_0
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result v2

    .line 1689101
    if-eqz v2, :cond_1

    .line 1689102
    const-string v2, "copyrights_violation_ui_notification_texts"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689103
    invoke-virtual {v1, v0, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1689104
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1689105
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1689091
    check-cast p1, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel$Serializer;->a(Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
