.class public final Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4b3cd0b9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689949
    const-class v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689980
    const-class v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1689978
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1689979
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1689970
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689971
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1689972
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->j()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1689973
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1689974
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1689975
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1689976
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689977
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1689957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689958
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1689959
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    .line 1689960
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1689961
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    .line 1689962
    iput-object v0, v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->e:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    .line 1689963
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->j()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1689964
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->j()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    .line 1689965
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->j()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1689966
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    .line 1689967
    iput-object v0, v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->f:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    .line 1689968
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689969
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689981
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->e:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->e:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    .line 1689982
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->e:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$MaskEffectModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1689954
    new-instance v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    invoke-direct {v0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;-><init>()V

    .line 1689955
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1689956
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1689953
    const v0, 0x58c04374

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1689952
    const v0, 0x7bc82d4e

    return v0
.end method

.method public final j()Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689950
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->f:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->f:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    .line 1689951
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;->f:Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel$ThumbnailImageModel;

    return-object v0
.end method
