.class public final Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1be8b0c7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1690270
    const-class v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1690271
    const-class v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1690272
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1690273
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1690274
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1690275
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->a()Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1690276
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1690277
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1690278
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1690279
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1690280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1690281
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->a()Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1690282
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->a()Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    .line 1690283
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->a()Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1690284
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;

    .line 1690285
    iput-object v0, v1, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->e:Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    .line 1690286
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1690287
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1690288
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->e:Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->e:Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    .line 1690289
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;->e:Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1690290
    new-instance v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel;-><init>()V

    .line 1690291
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1690292
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1690293
    const v0, -0x60fcfe9d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1690294
    const v0, 0x4ed245b

    return v0
.end method
