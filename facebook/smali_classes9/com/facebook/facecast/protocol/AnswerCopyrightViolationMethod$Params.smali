.class public final Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1688725
    new-instance v0, LX/Aaa;

    invoke-direct {v0}, LX/Aaa;-><init>()V

    sput-object v0, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1688726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688727
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->a:Ljava/lang/String;

    .line 1688728
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->b:Z

    .line 1688729
    return-void

    .line 1688730
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1688731
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688732
    iput-object p1, p0, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->a:Ljava/lang/String;

    .line 1688733
    iput-boolean p2, p0, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->b:Z

    .line 1688734
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1688735
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1688736
    iget-object v0, p0, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1688737
    iget-boolean v0, p0, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1688738
    return-void

    .line 1688739
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
