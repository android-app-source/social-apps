.class public Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/facebook/facecast/model/FacecastCompositionData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1690767
    new-instance v0, LX/AbR;

    invoke-direct {v0}, LX/AbR;-><init>()V

    sput-object v0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1690768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690769
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->a:Ljava/lang/String;

    .line 1690770
    const-class v0, Lcom/facebook/facecast/model/FacecastCompositionData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/model/FacecastCompositionData;

    iput-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690771
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/facecast/model/FacecastCompositionData;)V
    .locals 0

    .prologue
    .line 1690772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690773
    iput-object p1, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->a:Ljava/lang/String;

    .line 1690774
    iput-object p2, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    .line 1690775
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1690776
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1690777
    iget-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1690778
    iget-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;->b:Lcom/facebook/facecast/model/FacecastCompositionData;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1690779
    return-void
.end method
