.class public final Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x76bc350a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689331
    const-class v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689315
    const-class v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1689329
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1689330
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1689323
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689324
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1689325
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1689326
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1689327
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689328
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1689332
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689333
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1689334
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    .line 1689335
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1689336
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;

    .line 1689337
    iput-object v0, v1, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->e:Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    .line 1689338
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689339
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getActor"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689321
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->e:Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->e:Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    .line 1689322
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;->e:Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1689318
    new-instance v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel;-><init>()V

    .line 1689319
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1689320
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1689317
    const v0, -0x9300dd9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1689316
    const v0, -0x6747e1ce

    return v0
.end method
