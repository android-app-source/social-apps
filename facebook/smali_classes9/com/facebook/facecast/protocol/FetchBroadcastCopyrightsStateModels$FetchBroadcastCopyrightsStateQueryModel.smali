.class public final Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x22c3f68
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689106
    const-class v0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689107
    const-class v0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1689108
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1689109
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1689110
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689111
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1689112
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/util/List;)I

    move-result v1

    .line 1689113
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1689114
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1689115
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1689116
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689117
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1689118
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689119
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689120
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689121
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->e:Ljava/lang/String;

    .line 1689122
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1689123
    new-instance v0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;-><init>()V

    .line 1689124
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1689125
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1689126
    const v0, -0x1cf52c5c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1689127
    const v0, 0x4ed245b

    return v0
.end method

.method public final j()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1689128
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->f:Ljava/util/List;

    .line 1689129
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchBroadcastCopyrightsStateModels$FetchBroadcastCopyrightsStateQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
