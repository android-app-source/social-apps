.class public final Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1689671
    const-class v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;

    new-instance v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1689672
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1689673
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;LX/0nX;LX/0my;)V
    .locals 7

    .prologue
    .line 1689674
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1689675
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1689676
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1689677
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1689678
    if-eqz v2, :cond_9

    .line 1689679
    const-string v3, "creative_tools"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689680
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1689681
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1689682
    if-eqz v3, :cond_8

    .line 1689683
    const-string v4, "edges"

    invoke-virtual {p1, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689684
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1689685
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v5

    if-ge v4, v5, :cond_7

    .line 1689686
    invoke-virtual {v1, v3, v4}, LX/15i;->q(II)I

    move-result v5

    .line 1689687
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1689688
    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, LX/15i;->g(II)I

    move-result v6

    .line 1689689
    if-eqz v6, :cond_6

    .line 1689690
    const-string p0, "node"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689691
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 1689692
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1689693
    invoke-virtual {v1, v6, v2}, LX/15i;->g(II)I

    move-result p0

    .line 1689694
    if-eqz p0, :cond_0

    .line 1689695
    const-string p0, "__type__"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689696
    invoke-static {v1, v6, v2, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1689697
    :cond_0
    invoke-virtual {v1, v6, v5}, LX/15i;->g(II)I

    move-result p0

    .line 1689698
    if-eqz p0, :cond_1

    .line 1689699
    const-string p0, "doodle_colors"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689700
    invoke-virtual {v1, v6, v5}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1689701
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v6, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1689702
    if-eqz p0, :cond_3

    .line 1689703
    const-string v2, "video_creative_tool_filters"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689704
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1689705
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v5

    if-ge v2, v5, :cond_2

    .line 1689706
    invoke-virtual {v1, p0, v2}, LX/15i;->q(II)I

    move-result v5

    invoke-static {v1, v5, p1, p2}, LX/Ab1;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1689707
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1689708
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1689709
    :cond_3
    const/4 p0, 0x3

    invoke-virtual {v1, v6, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1689710
    if-eqz p0, :cond_5

    .line 1689711
    const-string v2, "video_creative_tool_mask_effects"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689712
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1689713
    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v5

    if-ge v2, v5, :cond_4

    .line 1689714
    invoke-virtual {v1, p0, v2}, LX/15i;->q(II)I

    move-result v5

    invoke-static {v1, v5, p1, p2}, LX/Ab4;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1689715
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1689716
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1689717
    :cond_5
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1689718
    :cond_6
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1689719
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    .line 1689720
    :cond_7
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1689721
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1689722
    :cond_9
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1689723
    if-eqz v2, :cond_a

    .line 1689724
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1689725
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1689726
    :cond_a
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1689727
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1689728
    check-cast p1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel$Serializer;->a(Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
