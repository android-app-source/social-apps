.class public final Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1688845
    const-class v0, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;

    new-instance v1, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1688846
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1688847
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1688848
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1688849
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p2, 0x0

    .line 1688850
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1688851
    invoke-virtual {v1, v0, p2}, LX/15i;->g(II)I

    move-result p0

    .line 1688852
    if-eqz p0, :cond_0

    .line 1688853
    const-string p0, "funnel_logging_tags"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1688854
    invoke-virtual {v1, v0, p2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object p0

    invoke-static {p0, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1688855
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1688856
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1688857
    check-cast p1, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel$Serializer;->a(Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
