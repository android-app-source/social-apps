.class public final Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1689669
    const-class v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;

    new-instance v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1689670
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1689668
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1689574
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1689575
    const/4 v2, 0x0

    .line 1689576
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1689577
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1689578
    :goto_0
    move v1, v2

    .line 1689579
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1689580
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1689581
    new-instance v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;

    invoke-direct {v1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$FetchCreativeToolsQueryModel;-><init>()V

    .line 1689582
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1689583
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1689584
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1689585
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1689586
    :cond_0
    return-object v1

    .line 1689587
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1689588
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1689589
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1689590
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1689591
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_2

    if-eqz v4, :cond_2

    .line 1689592
    const-string v5, "creative_tools"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1689593
    const/4 v4, 0x0

    .line 1689594
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v3, v5, :cond_a

    .line 1689595
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1689596
    :goto_2
    move v3, v4

    .line 1689597
    goto :goto_1

    .line 1689598
    :cond_3
    const-string v5, "id"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1689599
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    .line 1689600
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1689601
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1689602
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1689603
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1689604
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1689605
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_9

    .line 1689606
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1689607
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1689608
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_7

    if-eqz v5, :cond_7

    .line 1689609
    const-string v6, "edges"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1689610
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1689611
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_8

    .line 1689612
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_8

    .line 1689613
    const/4 v6, 0x0

    .line 1689614
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_e

    .line 1689615
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1689616
    :goto_5
    move v5, v6

    .line 1689617
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1689618
    :cond_8
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1689619
    goto :goto_3

    .line 1689620
    :cond_9
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1689621
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 1689622
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto/16 :goto_2

    :cond_a
    move v3, v4

    goto :goto_3

    .line 1689623
    :cond_b
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1689624
    :cond_c
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_d

    .line 1689625
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1689626
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1689627
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_c

    if-eqz v7, :cond_c

    .line 1689628
    const-string v8, "node"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 1689629
    const/4 v7, 0x0

    .line 1689630
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_18

    .line 1689631
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1689632
    :goto_7
    move v5, v7

    .line 1689633
    goto :goto_6

    .line 1689634
    :cond_d
    const/4 v7, 0x1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1689635
    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1689636
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_5

    :cond_e
    move v5, v6

    goto :goto_6

    .line 1689637
    :cond_f
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1689638
    :cond_10
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_17

    .line 1689639
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1689640
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1689641
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_10

    if-eqz v11, :cond_10

    .line 1689642
    const-string p0, "__type__"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_11

    const-string p0, "__typename"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_12

    .line 1689643
    :cond_11
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    invoke-virtual {v0, v10}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v10

    goto :goto_8

    .line 1689644
    :cond_12
    const-string p0, "doodle_colors"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_13

    .line 1689645
    invoke-static {p1, v0}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_8

    .line 1689646
    :cond_13
    const-string p0, "video_creative_tool_filters"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_15

    .line 1689647
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1689648
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_14

    .line 1689649
    :goto_9
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_14

    .line 1689650
    invoke-static {p1, v0}, LX/Ab1;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1689651
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 1689652
    :cond_14
    invoke-static {v8, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 1689653
    goto :goto_8

    .line 1689654
    :cond_15
    const-string p0, "video_creative_tool_mask_effects"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 1689655
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1689656
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_16

    .line 1689657
    :goto_a
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_16

    .line 1689658
    invoke-static {p1, v0}, LX/Ab4;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1689659
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    .line 1689660
    :cond_16
    invoke-static {v5, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1689661
    goto/16 :goto_8

    .line 1689662
    :cond_17
    const/4 v11, 0x4

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1689663
    invoke-virtual {v0, v7, v10}, LX/186;->b(II)V

    .line 1689664
    const/4 v7, 0x1

    invoke-virtual {v0, v7, v9}, LX/186;->b(II)V

    .line 1689665
    const/4 v7, 0x2

    invoke-virtual {v0, v7, v8}, LX/186;->b(II)V

    .line 1689666
    const/4 v7, 0x3

    invoke-virtual {v0, v7, v5}, LX/186;->b(II)V

    .line 1689667
    invoke-virtual {v0}, LX/186;->d()I

    move-result v7

    goto/16 :goto_7

    :cond_18
    move v5, v7

    move v8, v7

    move v9, v7

    move v10, v7

    goto/16 :goto_8
.end method
