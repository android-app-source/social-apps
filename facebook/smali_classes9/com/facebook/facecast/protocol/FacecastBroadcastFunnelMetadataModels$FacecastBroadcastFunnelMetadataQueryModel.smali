.class public final Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xc5925f3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1688875
    const-class v0, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1688859
    const-class v0, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1688873
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1688874
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1688867
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1688868
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1688869
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1688870
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1688871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1688872
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1688876
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;->e:Ljava/util/List;

    .line 1688877
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1688864
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1688865
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1688866
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1688861
    new-instance v0, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecast/protocol/FacecastBroadcastFunnelMetadataModels$FacecastBroadcastFunnelMetadataQueryModel;-><init>()V

    .line 1688862
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1688863
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1688860
    const v0, 0x45ef1439

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1688858
    const v0, -0x7c307ab6

    return v0
.end method
