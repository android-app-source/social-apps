.class public Lcom/facebook/facecast/protocol/FacecastNetworker;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/0aG;


# direct methods
.method public constructor <init>(LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1688882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1688883
    iput-object p1, p0, Lcom/facebook/facecast/protocol/FacecastNetworker;->a:LX/0aG;

    .line 1688884
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;
    .locals 1

    .prologue
    .line 1688885
    invoke-static {p0}, Lcom/facebook/facecast/protocol/FacecastNetworker;->b(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;
    .locals 2

    .prologue
    .line 1688886
    new-instance v1, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v0

    check-cast v0, LX/0aG;

    invoke-direct {v1, v0}, Lcom/facebook/facecast/protocol/FacecastNetworker;-><init>(LX/0aG;)V

    .line 1688887
    return-object v1
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688888
    if-nez p1, :cond_0

    .line 1688889
    :goto_0
    return-void

    .line 1688890
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 1688891
    const-string v0, "delete_video_key"

    invoke-virtual {v2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1688892
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FacecastNetworker;->a:LX/0aG;

    const-string v1, "delete_video_type"

    sget-object v3, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v4, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v4}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v4

    const v5, -0x273da826

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0
.end method
