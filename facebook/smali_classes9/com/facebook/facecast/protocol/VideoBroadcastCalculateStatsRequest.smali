.class public Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1690575
    new-instance v0, LX/AbL;

    invoke-direct {v0}, LX/AbL;-><init>()V

    sput-object v0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1690576
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690577
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->a:Ljava/lang/String;

    .line 1690578
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->b:I

    .line 1690579
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->c:J

    .line 1690580
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IJ)V
    .locals 1

    .prologue
    .line 1690581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690582
    iput-object p1, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->a:Ljava/lang/String;

    .line 1690583
    iput p2, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->b:I

    .line 1690584
    iput-wide p3, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->c:J

    .line 1690585
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1690586
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1690587
    iget-object v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1690588
    iget v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1690589
    iget-wide v0, p0, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1690590
    return-void
.end method
