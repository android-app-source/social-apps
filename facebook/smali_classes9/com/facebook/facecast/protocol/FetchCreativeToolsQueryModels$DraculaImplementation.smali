.class public final Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1689572
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1689573
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1689570
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1689571
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1689530
    if-nez p1, :cond_0

    .line 1689531
    :goto_0
    return v0

    .line 1689532
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1689533
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1689534
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1689535
    const v2, -0x1fb21f93

    const/4 v5, 0x0

    .line 1689536
    if-nez v1, :cond_1

    move v3, v5

    .line 1689537
    :goto_1
    move v1, v3

    .line 1689538
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1689539
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1689540
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1689541
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1689542
    const v2, 0x4882c461

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1689543
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1689544
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1689545
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1689546
    :sswitch_2
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1689547
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1689548
    const-class v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;

    invoke-virtual {p0, p1, v5, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1689549
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1689550
    const-class v2, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    invoke-virtual {p0, p1, v6, v2}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    .line 1689551
    invoke-static {p3, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1689552
    const/4 v3, 0x4

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1689553
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1689554
    invoke-virtual {p3, v5, v1}, LX/186;->b(II)V

    .line 1689555
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1689556
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1689557
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1689558
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1689559
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1689560
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1689561
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1689562
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v6

    .line 1689563
    if-nez v6, :cond_2

    const/4 v3, 0x0

    .line 1689564
    :goto_2
    if-ge v5, v6, :cond_3

    .line 1689565
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p1

    .line 1689566
    invoke-static {p0, p1, v2, p3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p1

    aput p1, v3, v5

    .line 1689567
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1689568
    :cond_2
    new-array v3, v6, [I

    goto :goto_2

    .line 1689569
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1fb21f93 -> :sswitch_1
        -0x14ff00ab -> :sswitch_3
        0x18388808 -> :sswitch_0
        0x4882c461 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1689529
    new-instance v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1689525
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1689526
    if-eqz v0, :cond_0

    .line 1689527
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1689528
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1689508
    sparse-switch p2, :sswitch_data_0

    .line 1689509
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1689510
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1689511
    const v1, -0x1fb21f93

    .line 1689512
    if-eqz v0, :cond_0

    .line 1689513
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1689514
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1689515
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1689516
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1689517
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1689518
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1689519
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1689520
    const v1, 0x4882c461

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    .line 1689521
    :sswitch_3
    const/4 v0, 0x2

    const-class v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1689522
    invoke-static {v0, p3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1689523
    const/4 v0, 0x3

    const-class v1, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoCreativeToolsMaskEffectFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1689524
    invoke-static {v0, p3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x1fb21f93 -> :sswitch_2
        -0x14ff00ab -> :sswitch_1
        0x18388808 -> :sswitch_0
        0x4882c461 -> :sswitch_3
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1689502
    if-eqz p1, :cond_0

    .line 1689503
    invoke-static {p0, p1, p2}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;

    move-result-object v1

    .line 1689504
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;

    .line 1689505
    if-eq v0, v1, :cond_0

    .line 1689506
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1689507
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1689501
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1689499
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1689500
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1689468
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1689469
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1689470
    :cond_0
    iput-object p1, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1689471
    iput p2, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->b:I

    .line 1689472
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1689498
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1689497
    new-instance v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1689494
    iget v0, p0, LX/1vt;->c:I

    .line 1689495
    move v0, v0

    .line 1689496
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1689491
    iget v0, p0, LX/1vt;->c:I

    .line 1689492
    move v0, v0

    .line 1689493
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1689488
    iget v0, p0, LX/1vt;->b:I

    .line 1689489
    move v0, v0

    .line 1689490
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689485
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1689486
    move-object v0, v0

    .line 1689487
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1689476
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1689477
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1689478
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1689479
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1689480
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1689481
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1689482
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1689483
    invoke-static {v3, v9, v2}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1689484
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1689473
    iget v0, p0, LX/1vt;->c:I

    .line 1689474
    move v0, v0

    .line 1689475
    return v0
.end method
