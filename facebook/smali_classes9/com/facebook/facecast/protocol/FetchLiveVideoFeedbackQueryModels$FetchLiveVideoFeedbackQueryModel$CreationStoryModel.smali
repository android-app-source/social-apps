.class public final Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xd0942b1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1690230
    const-class v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1690229
    const-class v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1690227
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1690228
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1690221
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1690222
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1690223
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1690224
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1690225
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1690226
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1690213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1690214
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1690215
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    .line 1690216
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1690217
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    .line 1690218
    iput-object v0, v1, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->e:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    .line 1690219
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1690220
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1690202
    new-instance v0, LX/Ab8;

    invoke-direct {v0, p1}, LX/Ab8;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1690211
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->e:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->e:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    .line 1690212
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;->e:Lcom/facebook/api/graphql/feedback/FeedbackDefaultsGraphQLModels$BaseFeedbackFieldsModel;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1690209
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1690210
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1690208
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1690205
    new-instance v0, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;

    invoke-direct {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoFeedbackQueryModels$FetchLiveVideoFeedbackQueryModel$CreationStoryModel;-><init>()V

    .line 1690206
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1690207
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1690204
    const v0, -0x48c6ec1b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1690203
    const v0, 0x4c808d5

    return v0
.end method
