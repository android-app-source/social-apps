.class public final Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1690413
    const-class v0, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;

    new-instance v1, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1690414
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1690415
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;LX/0nX;LX/0my;)V
    .locals 8

    .prologue
    .line 1690416
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1690417
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1690418
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1690419
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1690420
    if-eqz v2, :cond_0

    .line 1690421
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690422
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1690423
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1690424
    if-eqz v2, :cond_4

    .line 1690425
    const-string v3, "video_tip_jar_data"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690426
    const/4 v4, 0x0

    const-wide/16 v6, 0x0

    .line 1690427
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1690428
    invoke-virtual {v1, v2, v4, v4}, LX/15i;->a(III)I

    move-result v4

    .line 1690429
    if-eqz v4, :cond_1

    .line 1690430
    const-string v5, "tip_giver_number"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690431
    invoke-virtual {p1, v4}, LX/0nX;->b(I)V

    .line 1690432
    :cond_1
    const/4 v4, 0x1

    invoke-virtual {v1, v2, v4, v6, v7}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 1690433
    cmpl-double v6, v4, v6

    if-eqz v6, :cond_2

    .line 1690434
    const-string v6, "total_tip_amount"

    invoke-virtual {p1, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690435
    invoke-virtual {p1, v4, v5}, LX/0nX;->a(D)V

    .line 1690436
    :cond_2
    const/4 v4, 0x2

    invoke-virtual {v1, v2, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1690437
    if-eqz v4, :cond_3

    .line 1690438
    const-string v5, "total_tipped_amount"

    invoke-virtual {p1, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1690439
    invoke-virtual {p1, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1690440
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1690441
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1690442
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1690443
    check-cast p1, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel$Serializer;->a(Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$FetchLiveVideoTipJarDataQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
