.class public final Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x71b48c7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689274
    const-class v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689273
    const-class v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1689271
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1689272
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689268
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1689269
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1689270
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1689260
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689261
    invoke-direct {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1689262
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x2882f756

    invoke-static {v2, v1, v3}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1689263
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1689264
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1689265
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1689266
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689267
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1689245
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689246
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1689247
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x2882f756

    invoke-static {v2, v0, v3}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1689248
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1689249
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    .line 1689250
    iput v3, v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->f:I

    .line 1689251
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689252
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1689253
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1689254
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689258
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1689259
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1689275
    new-instance v0, LX/Aat;

    invoke-direct {v0, p1}, LX/Aat;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1689255
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1689256
    const/4 v0, 0x1

    const v1, 0x2882f756

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;->f:I

    .line 1689257
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1689243
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1689244
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1689242
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1689239
    new-instance v0, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;

    invoke-direct {v0}, Lcom/facebook/facecast/protocol/FetchCoverPhotoQueryModels$FetchCoverPhotoQueryModel$ActorModel;-><init>()V

    .line 1689240
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1689241
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1689238
    const v0, -0xaf6e83a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1689237
    const v0, 0x3c2b9d5

    return v0
.end method
