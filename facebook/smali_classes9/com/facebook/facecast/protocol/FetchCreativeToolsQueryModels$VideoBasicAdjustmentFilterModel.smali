.class public final Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x29fac76b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel$Serializer;
.end annotation


# instance fields
.field private e:D

.field private f:D

.field private g:D

.field private h:Z

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:D

.field private k:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689835
    const-class v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1689834
    const-class v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1689832
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1689833
    return-void
.end method

.method private p()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689830
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->i:Ljava/lang/String;

    .line 1689831
    iget-object v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 1689817
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689818
    invoke-direct {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1689819
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->o()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x14ff00ab

    invoke-static {v1, v0, v2}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1689820
    const/4 v0, 0x7

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1689821
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->e:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1689822
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->f:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1689823
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->g:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1689824
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1689825
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1689826
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->j:D

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1689827
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1689828
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689829
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1689807
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1689808
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1689809
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->o()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x14ff00ab

    invoke-static {v2, v0, v3}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1689810
    invoke-virtual {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->o()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1689811
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;

    .line 1689812
    iput v3, v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->k:I

    .line 1689813
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1689814
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1689815
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1689816
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689806
    invoke-direct {p0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->p()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1689798
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1689799
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->e:D

    .line 1689800
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->f:D

    .line 1689801
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->g:D

    .line 1689802
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->h:Z

    .line 1689803
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->j:D

    .line 1689804
    const/4 v0, 0x6

    const v1, -0x14ff00ab

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->k:I

    .line 1689805
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1689836
    new-instance v0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;

    invoke-direct {v0}, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;-><init>()V

    .line 1689837
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1689838
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1689797
    const v0, -0xcc8a8ee

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1689796
    const v0, -0x4aee8ebe

    return v0
.end method

.method public final j()D
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1689794
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1689795
    iget-wide v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->e:D

    return-wide v0
.end method

.method public final k()D
    .locals 2

    .prologue
    .line 1689792
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1689793
    iget-wide v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->f:D

    return-wide v0
.end method

.method public final l()D
    .locals 2

    .prologue
    .line 1689790
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1689791
    iget-wide v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->g:D

    return-wide v0
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 1689788
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1689789
    iget-boolean v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->h:Z

    return v0
.end method

.method public final n()D
    .locals 2

    .prologue
    .line 1689786
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1689787
    iget-wide v0, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->j:D

    return-wide v0
.end method

.method public final o()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getTitle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1689784
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1689785
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/facecast/protocol/FetchCreativeToolsQueryModels$VideoBasicAdjustmentFilterModel;->k:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
