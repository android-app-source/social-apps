.class public final Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1690362
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1690363
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1690332
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1690333
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    const/4 v0, 0x0

    .line 1690364
    if-nez p1, :cond_0

    .line 1690365
    :goto_0
    return v0

    .line 1690366
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1690367
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1690368
    :pswitch_0
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v6

    .line 1690369
    invoke-virtual {p0, p1, v1, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1690370
    invoke-virtual {p0, p1, v9}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v7

    .line 1690371
    invoke-virtual {p3, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1690372
    const/4 v8, 0x3

    invoke-virtual {p3, v8}, LX/186;->c(I)V

    .line 1690373
    invoke-virtual {p3, v0, v6, v0}, LX/186;->a(III)V

    move-object v0, p3

    .line 1690374
    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1690375
    invoke-virtual {p3, v9, v7}, LX/186;->b(II)V

    .line 1690376
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x6e438b85
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1690377
    new-instance v0, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1690381
    packed-switch p0, :pswitch_data_0

    .line 1690382
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1690383
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch -0x6e438b85
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1690378
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1690379
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;->b(I)V

    .line 1690380
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1690356
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1690357
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1690358
    :cond_0
    iput-object p1, p0, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;->a:LX/15i;

    .line 1690359
    iput p2, p0, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;->b:I

    .line 1690360
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1690361
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1690355
    new-instance v0, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1690352
    iget v0, p0, LX/1vt;->c:I

    .line 1690353
    move v0, v0

    .line 1690354
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1690349
    iget v0, p0, LX/1vt;->c:I

    .line 1690350
    move v0, v0

    .line 1690351
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1690346
    iget v0, p0, LX/1vt;->b:I

    .line 1690347
    move v0, v0

    .line 1690348
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1690343
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1690344
    move-object v0, v0

    .line 1690345
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1690334
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1690335
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1690336
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1690337
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1690338
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1690339
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1690340
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1690341
    invoke-static {v3, v9, v2}, Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/facecast/protocol/FetchLiveVideoTipJarDataQueryModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1690342
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1690329
    iget v0, p0, LX/1vt;->c:I

    .line 1690330
    move v0, v0

    .line 1690331
    return v0
.end method
