.class public Lcom/facebook/facecast/FacecastPreviewView;
.super Landroid/view/SurfaceView;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View$OnTouchListener;",
            ">;"
        }
    .end annotation
.end field

.field private c:F

.field public d:LX/1b8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1679047
    const-class v0, Lcom/facebook/facecast/FacecastPreviewView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/FacecastPreviewView;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1679058
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/FacecastPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1679059
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1679056
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/FacecastPreviewView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1679057
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1679051
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1679052
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/facecast/FacecastPreviewView;->c:F

    .line 1679053
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastPreviewView;->b:Ljava/util/List;

    .line 1679054
    new-instance v0, LX/AVD;

    invoke-direct {v0, p0}, LX/AVD;-><init>(Lcom/facebook/facecast/FacecastPreviewView;)V

    invoke-super {p0, v0}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1679055
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1679049
    iget-object v0, p0, Lcom/facebook/facecast/FacecastPreviewView;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1679050
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1679048
    iget v0, p0, Lcom/facebook/facecast/FacecastPreviewView;->c:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/view/View$OnTouchListener;)V
    .locals 1

    .prologue
    .line 1679060
    iget-object v0, p0, Lcom/facebook/facecast/FacecastPreviewView;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1679061
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1679026
    invoke-static {v0, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v1

    .line 1679027
    invoke-static {v0, p2}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 1679028
    iget v2, p0, Lcom/facebook/facecast/FacecastPreviewView;->c:F

    const/high16 v3, -0x40800000    # -1.0f

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    .line 1679029
    int-to-float v0, v1

    iget v2, p0, Lcom/facebook/facecast/FacecastPreviewView;->c:F

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 1679030
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/facebook/facecast/FacecastPreviewView;->setMeasuredDimension(II)V

    .line 1679031
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x10e8e709

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1679032
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/SurfaceView;->onSizeChanged(IIII)V

    .line 1679033
    iget-object v1, p0, Lcom/facebook/facecast/FacecastPreviewView;->d:LX/1b8;

    if-eqz v1, :cond_0

    .line 1679034
    iget-object v1, p0, Lcom/facebook/facecast/FacecastPreviewView;->d:LX/1b8;

    invoke-interface {v1, p0}, LX/1b8;->a(Lcom/facebook/facecast/FacecastPreviewView;)V

    .line 1679035
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x5ee8ac2b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAspectRatio(F)V
    .locals 0

    .prologue
    .line 1679036
    iput p1, p0, Lcom/facebook/facecast/FacecastPreviewView;->c:F

    .line 1679037
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastPreviewView;->requestLayout()V

    .line 1679038
    return-void
.end method

.method public setListener(LX/1b8;)V
    .locals 0

    .prologue
    .line 1679039
    iput-object p1, p0, Lcom/facebook/facecast/FacecastPreviewView;->d:LX/1b8;

    .line 1679040
    return-void
.end method

.method public setOnTouchListener(Landroid/view/View$OnTouchListener;)V
    .locals 2

    .prologue
    .line 1679041
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1679042
    sget-object v0, Lcom/facebook/facecast/FacecastPreviewView;->a:Ljava/lang/String;

    const-string v1, "Do not set an onTouchListener to this class. Use addOnTouchListener instead"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1679043
    return-void
.end method

.method public setSquareScreen(Z)V
    .locals 1

    .prologue
    .line 1679044
    if-eqz p1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/facecast/FacecastPreviewView;->setAspectRatio(F)V

    .line 1679045
    return-void

    .line 1679046
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method
