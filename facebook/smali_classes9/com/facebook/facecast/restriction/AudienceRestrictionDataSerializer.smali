.class public Lcom/facebook/facecast/restriction/AudienceRestrictionDataSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecast/restriction/AudienceRestrictionData;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1690849
    const-class v0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;

    new-instance v1, Lcom/facebook/facecast/restriction/AudienceRestrictionDataSerializer;

    invoke-direct {v1}, Lcom/facebook/facecast/restriction/AudienceRestrictionDataSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1690850
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1690851
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecast/restriction/AudienceRestrictionData;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1690852
    if-nez p0, :cond_0

    .line 1690853
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1690854
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1690855
    invoke-static {p0, p1, p2}, Lcom/facebook/facecast/restriction/AudienceRestrictionDataSerializer;->b(Lcom/facebook/facecast/restriction/AudienceRestrictionData;LX/0nX;LX/0my;)V

    .line 1690856
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1690857
    return-void
.end method

.method private static b(Lcom/facebook/facecast/restriction/AudienceRestrictionData;LX/0nX;LX/0my;)V
    .locals 6

    .prologue
    .line 1690858
    const-string v0, "age_min"

    iget v1, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->ageMin:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1690859
    const-string v0, "age_max"

    iget v1, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->ageMax:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1690860
    const-string v0, "genders"

    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1690861
    sget-object v2, LX/AbT;->a:[I

    iget-object v3, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1690862
    :goto_0
    :pswitch_0
    move-object v1, v1

    .line 1690863
    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1690864
    const-string v0, "geo_locations"

    .line 1690865
    iget-object v1, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->b:LX/AbV;

    iget-object v1, v1, LX/AbV;->d:LX/0P1;

    move-object v1, v1

    .line 1690866
    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1690867
    const-string v0, "excluded_countries"

    .line 1690868
    iget-object v1, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->c:LX/AbV;

    iget-object v1, v1, LX/AbV;->a:LX/0Px;

    move-object v1, v1

    .line 1690869
    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1690870
    const-string v0, "excluded_regions"

    .line 1690871
    iget-object v1, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->c:LX/AbV;

    iget-object v1, v1, LX/AbV;->b:LX/0Px;

    move-object v1, v1

    .line 1690872
    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1690873
    const-string v0, "excluded_cities"

    .line 1690874
    iget-object v1, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->c:LX/AbV;

    iget-object v1, v1, LX/AbV;->c:LX/0Px;

    move-object v1, v1

    .line 1690875
    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1690876
    return-void

    .line 1690877
    :pswitch_1
    new-array v1, v4, [I

    aput v4, v1, v5

    goto :goto_0

    .line 1690878
    :pswitch_2
    new-array v1, v4, [I

    const/4 v2, 0x2

    aput v2, v1, v5

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1690879
    check-cast p1, Lcom/facebook/facecast/restriction/AudienceRestrictionData;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecast/restriction/AudienceRestrictionDataSerializer;->a(Lcom/facebook/facecast/restriction/AudienceRestrictionData;LX/0nX;LX/0my;)V

    return-void
.end method
