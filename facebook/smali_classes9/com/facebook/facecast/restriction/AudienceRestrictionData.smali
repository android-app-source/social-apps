.class public Lcom/facebook/facecast/restriction/AudienceRestrictionData;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

.field public final ageMax:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "age_max"
    .end annotation
.end field

.field public final ageMin:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "age_min"
    .end annotation
.end field

.field public final b:LX/AbV;

.field public final c:LX/AbV;


# direct methods
.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1690841
    const-class v0, Lcom/facebook/facecast/restriction/AudienceRestrictionDataSerializer;

    return-object v0
.end method

.method public constructor <init>(LX/AbU;)V
    .locals 3

    .prologue
    .line 1690842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1690843
    iget v0, p1, LX/AbU;->a:I

    iput v0, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->ageMin:I

    .line 1690844
    iget v0, p1, LX/AbU;->b:I

    iput v0, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->ageMax:I

    .line 1690845
    iget-object v0, p1, LX/AbU;->c:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    iput-object v0, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 1690846
    new-instance v0, LX/AbV;

    iget-object v1, p1, LX/AbU;->d:LX/0Px;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, LX/AbV;-><init>(LX/0Px;Z)V

    iput-object v0, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->b:LX/AbV;

    .line 1690847
    new-instance v0, LX/AbV;

    iget-object v1, p1, LX/AbU;->e:LX/0Px;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/AbV;-><init>(LX/0Px;Z)V

    iput-object v0, p0, Lcom/facebook/facecast/restriction/AudienceRestrictionData;->c:LX/AbV;

    .line 1690848
    return-void
.end method
