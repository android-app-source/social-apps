.class public Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# static fields
.field public static final m:LX/1jt;


# instance fields
.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation
.end field

.field public o:Lcom/facebook/facecast/restriction/RestrictionSwitchView;

.field public p:Landroid/view/View;

.field public q:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

.field public r:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

.field private s:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

.field public t:Lcom/facebook/facecast/restriction/LocationControlView;

.field public u:LX/AbS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1691003
    new-instance v0, LX/AbW;

    invoke-direct {v0}, LX/AbW;-><init>()V

    sput-object v0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->m:LX/1jt;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1691000
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1691001
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1691002
    iput-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->n:LX/0Px;

    return-void
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1690992
    const/16 v0, 0x1db1

    if-eq p1, v0, :cond_1

    .line 1690993
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1690994
    :cond_0
    :goto_0
    return-void

    .line 1690995
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1690996
    const-string v0, "selectedTokens"

    invoke-static {p3, v0}, LX/4By;->b(Landroid/content/Intent;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1690997
    if-eqz v0, :cond_2

    :goto_1
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->n:LX/0Px;

    .line 1690998
    iget-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->s:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    iget-object v1, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->n:LX/0Px;

    sget-object v2, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->m:LX/1jt;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a(Ljava/lang/Iterable;LX/1jt;)V

    goto :goto_0

    .line 1690999
    :cond_2
    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    goto :goto_1
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 1690987
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1690988
    iget-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->u:LX/AbS;

    if-eqz v0, :cond_0

    .line 1690989
    iget-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->u:LX/AbS;

    .line 1690990
    iget-object v1, v0, LX/AbS;->b:LX/AVT;

    const-string p0, "geotargeting_cancel_tapped"

    const/4 p1, 0x0

    invoke-virtual {v1, p0, p1}, LX/AVT;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1690991
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x6856229a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1690944
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1690945
    const v1, 0x7f0e05cc

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1690946
    const/16 v1, 0x2b

    const v2, -0x7925d153

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x347f43da

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1690986
    const v1, 0x7f0305a1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4632a11e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x2a4d4304

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1690983
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onResume()V

    .line 1690984
    iget-object v1, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->s:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    iget-object v2, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->n:LX/0Px;

    sget-object v3, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->m:LX/1jt;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->a(Ljava/lang/Iterable;LX/1jt;)V

    .line 1690985
    const/16 v1, 0x2b

    const v2, -0x56f57ef6

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, -0x1

    const/16 v0, 0x2a

    const v1, -0x68693b85

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1690971
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1690972
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1690973
    if-nez v0, :cond_0

    .line 1690974
    const/16 v0, 0x2b

    const v2, 0x36b1e80

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1690975
    :goto_0
    return-void

    .line 1690976
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v0

    .line 1690977
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 1690978
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1690979
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1690980
    iget-object v2, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v2, v2

    .line 1690981
    invoke-virtual {v2}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v2

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    invoke-virtual {v2, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 1690982
    const v0, -0x2dcb615a

    invoke-static {v0, v1}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x41

    .line 1690947
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1690948
    const v0, 0x7f0d0f6a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1690949
    const v1, 0x7f080bbe

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitle(I)V

    .line 1690950
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1690951
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1690952
    new-instance v1, LX/AbZ;

    invoke-direct {v1, p0}, LX/AbZ;-><init>(Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 1690953
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const p2, 0x7f080bc3

    invoke-virtual {p0, p2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p2

    .line 1690954
    iput-object p2, v1, LX/108;->g:Ljava/lang/String;

    .line 1690955
    move-object v1, v1

    .line 1690956
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1690957
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 1690958
    new-instance v1, LX/Aba;

    invoke-direct {v1, p0}, LX/Aba;-><init>(Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setOnToolbarButtonListener(LX/63W;)V

    .line 1690959
    const v0, 0x7f0d0f6c

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/restriction/RestrictionSwitchView;

    iput-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->o:Lcom/facebook/facecast/restriction/RestrictionSwitchView;

    .line 1690960
    const v0, 0x7f0d0f6d

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->p:Landroid/view/View;

    .line 1690961
    const v0, 0x7f0d048d

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    iput-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->q:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    .line 1690962
    const v0, 0x7f0d0490

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    iput-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->r:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    .line 1690963
    const v0, 0x7f0d0483

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    iput-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->s:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    .line 1690964
    const v0, 0x7f0d0f6f

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/restriction/LocationControlView;

    iput-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->t:Lcom/facebook/facecast/restriction/LocationControlView;

    .line 1690965
    iget-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->o:Lcom/facebook/facecast/restriction/RestrictionSwitchView;

    new-instance v1, LX/AbX;

    invoke-direct {v1, p0}, LX/AbX;-><init>(Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/restriction/RestrictionSwitchView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1690966
    iget-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->q:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->a(II)V

    .line 1690967
    iget-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->q:Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;

    const/16 v1, 0x12

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesAgeTargetingView;->b(II)V

    .line 1690968
    iget-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->r:Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;

    sget-object v1, LX/ACy;->ALL:LX/ACy;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesGenderView;->setGender(LX/ACy;)V

    .line 1690969
    iget-object v0, p0, Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;->s:Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;

    new-instance v1, LX/AbY;

    invoke-direct {v1, p0}, LX/AbY;-><init>(Lcom/facebook/facecast/restriction/FacecastAudienceDialogFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/view/AdInterfacesSelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1690970
    return-void
.end method
