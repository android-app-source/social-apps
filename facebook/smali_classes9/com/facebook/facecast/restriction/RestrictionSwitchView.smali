.class public Lcom/facebook/facecast/restriction/RestrictionSwitchView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/widget/SwitchCompat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1691034
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/restriction/RestrictionSwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691035
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691021
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/restriction/RestrictionSwitchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691022
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691028
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691029
    const v0, 0x7f0311d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1691030
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/restriction/RestrictionSwitchView;->setOrientation(I)V

    .line 1691031
    new-instance v0, LX/Abc;

    invoke-direct {v0, p0}, LX/Abc;-><init>(Lcom/facebook/facecast/restriction/RestrictionSwitchView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/restriction/RestrictionSwitchView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1691032
    const v0, 0x7f0d1a93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/facecast/restriction/RestrictionSwitchView;->a:Lcom/facebook/widget/SwitchCompat;

    .line 1691033
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1691027
    iget-object v0, p0, Lcom/facebook/facecast/restriction/RestrictionSwitchView;->a:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1691025
    iget-object v0, p0, Lcom/facebook/facecast/restriction/RestrictionSwitchView;->a:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 1691026
    return-void
.end method

.method public setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1691023
    iget-object v0, p0, Lcom/facebook/facecast/restriction/RestrictionSwitchView;->a:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1691024
    return-void
.end method
