.class public Lcom/facebook/facecast/restriction/LocationControlView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/widget/RadioGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1691004
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/restriction/LocationControlView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691005
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1691006
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/restriction/LocationControlView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691007
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1691008
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691009
    const v0, 0x7f0311d6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1691010
    const v0, 0x7f0d29e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/facebook/facecast/restriction/LocationControlView;->a:Landroid/widget/RadioGroup;

    .line 1691011
    return-void
.end method


# virtual methods
.method public getIsInclude()Z
    .locals 2

    .prologue
    .line 1691012
    iget-object v0, p0, Lcom/facebook/facecast/restriction/LocationControlView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f0d29e9

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setIsInclude(Z)V
    .locals 2

    .prologue
    .line 1691013
    iget-object v1, p0, Lcom/facebook/facecast/restriction/LocationControlView;->a:Landroid/widget/RadioGroup;

    if-eqz p1, :cond_0

    const v0, 0x7f0d29e9

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->check(I)V

    .line 1691014
    return-void

    .line 1691015
    :cond_0
    const v0, 0x7f0d29ea

    goto :goto_0
.end method

.method public setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V
    .locals 1

    .prologue
    .line 1691016
    iget-object v0, p0, Lcom/facebook/facecast/restriction/LocationControlView;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 1691017
    return-void
.end method
