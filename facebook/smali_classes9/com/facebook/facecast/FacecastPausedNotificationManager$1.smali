.class public final Lcom/facebook/facecast/FacecastPausedNotificationManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/AVC;


# direct methods
.method public constructor <init>(LX/AVC;)V
    .locals 0

    .prologue
    .line 1678978
    iput-object p1, p0, Lcom/facebook/facecast/FacecastPausedNotificationManager$1;->a:LX/AVC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1678979
    iget-object v0, p0, Lcom/facebook/facecast/FacecastPausedNotificationManager$1;->a:LX/AVC;

    const/4 v6, 0x1

    .line 1678980
    iget-object v1, v0, LX/AVC;->i:LX/1b2;

    iget-object v2, v0, LX/AVC;->c:Landroid/content/Context;

    .line 1678981
    new-instance v3, Landroid/content/Intent;

    iget-object v4, v1, LX/1b2;->c:Ljava/lang/Class;

    invoke-direct {v3, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object v1, v3

    .line 1678982
    iget-object v2, v0, LX/AVC;->c:Landroid/content/Context;

    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {v2, v3, v1, v4}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 1678983
    iget-object v1, v0, LX/AVC;->d:Landroid/content/res/Resources;

    const v3, 0x7f080cdc

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1678984
    iget-object v4, v0, LX/AVC;->d:Landroid/content/res/Resources;

    iget-boolean v1, v0, LX/AVC;->k:Z

    if-eqz v1, :cond_0

    const v1, 0x7f080cde

    :goto_0
    invoke-virtual {v4, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1678985
    new-instance v4, LX/2HB;

    iget-object v5, v0, LX/AVC;->c:Landroid/content/Context;

    invoke-direct {v4, v5}, LX/2HB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v3}, LX/2HB;->a(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/2HB;->e(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v1

    const v3, 0x7f0218e4

    invoke-virtual {v1, v3}, LX/2HB;->a(I)LX/2HB;

    move-result-object v1

    .line 1678986
    iput-object v2, v1, LX/2HB;->d:Landroid/app/PendingIntent;

    .line 1678987
    move-object v1, v1

    .line 1678988
    invoke-virtual {v1, v6}, LX/2HB;->c(Z)LX/2HB;

    move-result-object v1

    iget-object v2, v0, LX/AVC;->f:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v3

    invoke-virtual {v1, v3, v4}, LX/2HB;->a(J)LX/2HB;

    move-result-object v1

    .line 1678989
    iput v6, v1, LX/2HB;->j:I

    .line 1678990
    move-object v1, v1

    .line 1678991
    sget-object v2, LX/AVC;->a:[J

    invoke-virtual {v1, v2}, LX/2HB;->a([J)LX/2HB;

    move-result-object v1

    .line 1678992
    iput v6, v1, LX/2HB;->z:I

    .line 1678993
    move-object v1, v1

    .line 1678994
    invoke-virtual {v1}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v1

    .line 1678995
    iget-object v2, v0, LX/AVC;->b:Landroid/app/NotificationManager;

    const/4 v3, 0x0

    const/16 v4, 0x2766

    invoke-virtual {v2, v3, v4, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 1678996
    iget-object v1, v0, LX/AVC;->h:LX/AVU;

    .line 1678997
    const-string v2, "facecast_show_pause_notif"

    invoke-static {v1, v2}, LX/AVU;->c(LX/AVU;Ljava/lang/String;)V

    .line 1678998
    return-void

    .line 1678999
    :cond_0
    const v1, 0x7f080cdd

    goto :goto_0
.end method
