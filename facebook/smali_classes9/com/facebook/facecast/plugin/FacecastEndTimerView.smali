.class public Lcom/facebook/facecast/plugin/FacecastEndTimerView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:J

.field private final c:J

.field private final d:Lcom/facebook/resources/ui/FbTextView;

.field public final e:Landroid/animation/ObjectAnimator;

.field private f:LX/AXZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Z

.field private h:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1684362
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1684363
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684360
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684361
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684351
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1684352
    const-wide/16 v0, 0x1b58

    iput-wide v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->b:J

    .line 1684353
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->c:J

    .line 1684354
    const-class v0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;

    invoke-static {v0, p0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1684355
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setOrientation(I)V

    .line 1684356
    const v0, 0x7f0305ef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1684357
    const v0, 0x7f0d105a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1684358
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0}, LX/3Hk;->a(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->e:Landroid/animation/ObjectAnimator;

    .line 1684359
    return-void
.end method

.method private static a(II)J
    .locals 6

    .prologue
    .line 1684350
    int-to-long v0, p0

    const-wide/32 v2, 0xea60

    mul-long/2addr v0, v2

    int-to-long v2, p1

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v0

    check-cast v0, LX/0Uh;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a:LX/0Uh;

    return-void
.end method

.method private a(Ljava/lang/String;LX/47x;)V
    .locals 6

    .prologue
    .line 1684299
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1684300
    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->g:Z

    if-nez v1, :cond_0

    .line 1684301
    new-instance v1, Landroid/text/style/StyleSpan;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Landroid/text/style/StyleSpan;-><init>(I)V

    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a00d5

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1684302
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 1684303
    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 1684304
    :cond_0
    const-string v1, "%1$s"

    invoke-virtual {p2, v1, v0}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    .line 1684305
    return-void
.end method

.method private b(J)V
    .locals 9

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/16 v4, 0x37

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1684327
    const/4 v2, 0x5

    invoke-static {v2, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gez v2, :cond_1

    const/4 v2, 0x4

    invoke-static {v2, v4}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-lez v2, :cond_1

    .line 1684328
    invoke-direct {p0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setTimerVisibity(I)V

    .line 1684329
    :cond_0
    :goto_0
    return-void

    .line 1684330
    :cond_1
    const/4 v2, 0x4

    invoke-static {v2, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gtz v2, :cond_2

    const/4 v2, 0x3

    invoke-static {v2, v4}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_2

    .line 1684331
    invoke-direct {p0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setTimerVisibity(I)V

    goto :goto_0

    .line 1684332
    :cond_2
    const/4 v2, 0x3

    invoke-static {v2, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gtz v2, :cond_3

    const/4 v2, 0x2

    invoke-static {v2, v4}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_3

    .line 1684333
    invoke-direct {p0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setTimerVisibity(I)V

    goto :goto_0

    .line 1684334
    :cond_3
    const/4 v2, 0x2

    invoke-static {v2, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gtz v2, :cond_4

    invoke-static {v0, v4}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_4

    .line 1684335
    invoke-direct {p0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setTimerVisibity(I)V

    goto :goto_0

    .line 1684336
    :cond_4
    invoke-static {v0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gtz v2, :cond_5

    invoke-static {v1, v4}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_5

    .line 1684337
    invoke-direct {p0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setTimerVisibity(I)V

    goto :goto_0

    .line 1684338
    :cond_5
    const/16 v2, 0xa

    invoke-static {v1, v2}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gtz v2, :cond_6

    invoke-static {v1, v0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-ltz v2, :cond_6

    .line 1684339
    invoke-direct {p0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setTimerVisibity(I)V

    goto :goto_0

    .line 1684340
    :cond_6
    invoke-static {v1, v0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(II)J

    move-result-wide v2

    cmp-long v2, p1, v2

    if-gtz v2, :cond_9

    .line 1684341
    invoke-direct {p0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setTimerVisibity(I)V

    .line 1684342
    iget-boolean v2, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->g:Z

    if-nez v2, :cond_0

    .line 1684343
    const-wide/16 v2, 0x3e8

    sub-long v2, p1, v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-wide/16 v4, 0x2

    rem-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_7

    .line 1684344
    :goto_1
    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbTextView;->getAlpha()F

    move-result v1

    cmpl-float v1, v1, v7

    if-nez v1, :cond_8

    .line 1684345
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_0

    :cond_7
    move v0, v1

    .line 1684346
    goto :goto_1

    .line 1684347
    :cond_8
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v6

    if-nez v0, :cond_0

    .line 1684348
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_0

    .line 1684349
    :cond_9
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setTimerVisibity(I)V

    goto/16 :goto_0
.end method

.method private setTimerVisibity(I)V
    .locals 2

    .prologue
    .line 1684321
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->getVisibility()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1684322
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->f:LX/AXZ;

    if-eqz v0, :cond_0

    .line 1684323
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->f:LX/AXZ;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, LX/AXZ;->a(Z)V

    .line 1684324
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->setVisibility(I)V

    .line 1684325
    return-void

    .line 1684326
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 7

    .prologue
    .line 1684310
    iget-wide v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->h:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    sub-long/2addr v0, p1

    .line 1684311
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v2, v3}, LX/3Hk;->a(J)Ljava/lang/String;

    move-result-object v2

    .line 1684312
    new-instance v3, LX/47x;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1684313
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080c99

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 1684314
    invoke-direct {p0, v2, v3}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->a(Ljava/lang/String;LX/47x;)V

    .line 1684315
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684316
    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->b(J)V

    .line 1684317
    const-wide/16 v2, -0x1b58

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1684318
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->f:LX/AXZ;

    if-eqz v0, :cond_0

    .line 1684319
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->f:LX/AXZ;

    invoke-interface {v0}, LX/AXZ;->iE_()V

    .line 1684320
    :cond_0
    return-void
.end method

.method public final a(JLX/AXZ;)V
    .locals 1

    .prologue
    .line 1684307
    iput-wide p1, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->h:J

    .line 1684308
    iput-object p3, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->f:LX/AXZ;

    .line 1684309
    return-void
.end method

.method public getLiveAnimator()Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 1684306
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndTimerView;->e:Landroid/animation/ObjectAnimator;

    return-object v0
.end method
