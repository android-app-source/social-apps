.class public Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;
.super Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/AXX;


# instance fields
.field public g:LX/Acy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final h:Landroid/view/ViewGroup;

.field private final i:Lcom/facebook/widget/SwitchCompat;

.field private final j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private final k:Lcom/facebook/widget/text/BetterTextView;

.field private final l:Lcom/facebook/widget/text/BetterTextView;

.field private final m:Lcom/facebook/widget/text/BetterTextView;

.field private final n:Landroid/widget/LinearLayout;

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 1

    .prologue
    .line 1688411
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;-><init>(Landroid/content/Context;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Landroid/util/AttributeSet;)V

    .line 1688412
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Landroid/util/AttributeSet;)V
    .locals 1
    .param p3    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688409
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;-><init>(Landroid/content/Context;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Landroid/util/AttributeSet;I)V

    .line 1688410
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p3    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1688381
    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1688382
    const-class v0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;

    invoke-static {v0, p0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1688383
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 1688384
    const v0, 0x7f0d1002

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1688385
    const v4, 0x7f0305d4

    invoke-virtual {v3, v4, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1688386
    const v4, 0x7f0305d8

    invoke-virtual {v3, v4, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1688387
    const v0, 0x7f0d1005

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->n:Landroid/widget/LinearLayout;

    .line 1688388
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->n:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1688389
    const v0, 0x7f0d1012

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->h:Landroid/view/ViewGroup;

    .line 1688390
    const v0, 0x7f0d1014

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->i:Lcom/facebook/widget/SwitchCompat;

    .line 1688391
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->i:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 1688392
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->i:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080c8f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/SwitchCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1688393
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->h:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688394
    const v0, 0x7f0d1006

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1688395
    const v0, 0x7f0d1007

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->k:Lcom/facebook/widget/text/BetterTextView;

    .line 1688396
    const v0, 0x7f0d1008

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 1688397
    const v0, 0x7f0d1009

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->m:Lcom/facebook/widget/text/BetterTextView;

    .line 1688398
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1688399
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1688400
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 1688401
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1688402
    iget-object v4, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->j:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v3, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;

    invoke-static {v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v3

    invoke-virtual {v4, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1688403
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->k:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688404
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080cf3

    new-array v1, v1, [Ljava/lang/Object;

    .line 1688405
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->l()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object v5

    :goto_1
    move-object v5, v5

    .line 1688406
    aput-object v5, v1, v2

    invoke-virtual {v3, v4, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688407
    return-void

    :cond_1
    move v0, v2

    .line 1688408
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->l()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel$PageModel;->j()Ljava/lang/String;

    move-result-object v5

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;

    invoke-static {p0}, LX/Acy;->b(LX/0QB;)LX/Acy;

    move-result-object p0

    check-cast p0, LX/Acy;

    iput-object p0, p1, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->g:LX/Acy;

    return-void
.end method


# virtual methods
.method public final a(JJLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;I)V
    .locals 1

    .prologue
    .line 1688378
    invoke-super/range {p0 .. p8}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->a(JJLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;I)V

    .line 1688379
    iput-object p6, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->o:Ljava/lang/String;

    .line 1688380
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 1688374
    invoke-super {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->g()V

    .line 1688375
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->i:Lcom/facebook/widget/SwitchCompat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->i:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1688376
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->h()V

    .line 1688377
    :cond_0
    return-void
.end method

.method public final iF_()V
    .locals 2

    .prologue
    .line 1688371
    invoke-super {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->iF_()V

    .line 1688372
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->g:LX/Acy;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->o:Ljava/lang/String;

    invoke-virtual {v0, v1, p0}, LX/Acy;->a(Ljava/lang/String;LX/AXX;)V

    .line 1688373
    return-void
.end method

.method public final r_(I)V
    .locals 2

    .prologue
    .line 1688367
    if-gez p1, :cond_0

    .line 1688368
    const/4 p1, 0x0

    .line 1688369
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/livedonation/FacecastLiveDonationEndScreenPlugin;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688370
    return-void
.end method
