.class public Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/AZD;


# instance fields
.field private final a:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

.field private final b:Lcom/facebook/widget/text/BetterTextView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686685
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686686
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1686687
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686688
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1686689
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686690
    const v0, 0x7f0305b5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1686691
    const v0, 0x7f0d0fbb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->a:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    .line 1686692
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->a:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;

    invoke-virtual {v0, p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->a(LX/AZD;)V

    .line 1686693
    const v0, 0x7f0d0fbc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1686694
    const v0, 0x7f0d0fbd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1686695
    return-void
.end method


# virtual methods
.method public final iH_()V
    .locals 2

    .prologue
    .line 1686696
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setEnabled(Z)V

    .line 1686697
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->c:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1686698
    return-void
.end method
