.class public Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/48V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/webview/BasicWebView;

.field private r:Ljava/lang/String;

.field public s:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public t:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1686925
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1686926
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->q:Lcom/facebook/webview/BasicWebView;

    new-instance v1, LX/AZM;

    invoke-direct {v1, p0}, LX/AZM;-><init>(Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/webview/BasicWebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 1686927
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->p:LX/48V;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->q:Lcom/facebook/webview/BasicWebView;

    iget-object v2, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->r:Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 1686928
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;

    invoke-static {v0}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v0

    check-cast v0, LX/48V;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->p:LX/48V;

    return-void
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1686929
    const-string v0, "https://m.facebook.com/video/instream_video/onboarding/?creator_id=%s"

    invoke-static {v0, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686930
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1686931
    invoke-static {p0, p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1686932
    const v0, 0x7f0305b4

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->setContentView(I)V

    .line 1686933
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->s:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 1686934
    const v0, 0x7f0d0bec

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->t:Landroid/widget/ProgressBar;

    .line 1686935
    const v0, 0x7f0d0fb9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/webview/BasicWebView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->q:Lcom/facebook/webview/BasicWebView;

    .line 1686936
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->s:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/AZL;

    invoke-direct {v1, p0}, LX/AZL;-><init>(Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 1686937
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1686938
    const-string v1, "target_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->r:Ljava/lang/String;

    .line 1686939
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingPayoutActivity;->a()V

    .line 1686940
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x37f9b430

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1686941
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1686942
    const/16 v1, 0x23

    const v2, -0x29d1cb0b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
