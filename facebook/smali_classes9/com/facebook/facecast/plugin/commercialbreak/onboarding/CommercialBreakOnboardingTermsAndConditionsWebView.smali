.class public Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;
.super Lcom/facebook/webview/BasicWebView;
.source ""


# instance fields
.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AZD;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686762
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686763
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1686764
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686765
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1686766
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/webview/BasicWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686767
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->e:Ljava/util/List;

    .line 1686768
    return-void
.end method


# virtual methods
.method public final a(LX/AZD;)V
    .locals 1

    .prologue
    .line 1686769
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1686770
    return-void
.end method

.method public final onScrollChanged(IIII)V
    .locals 4

    .prologue
    .line 1686771
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/webview/BasicWebView;->onScrollChanged(IIII)V

    .line 1686772
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->getContentHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->getScale()F

    move-result v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4039000000000000L    # 25.0

    sub-double/2addr v0, v2

    double-to-int v0, v0

    .line 1686773
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->getHeight()I

    move-result v1

    .line 1686774
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    if-lt v1, v0, :cond_0

    .line 1686775
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnboardingTermsAndConditionsWebView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZD;

    .line 1686776
    invoke-interface {v0}, LX/AZD;->iH_()V

    goto :goto_0

    .line 1686777
    :cond_0
    return-void
.end method
