.class public final Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingFlowPlugin$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/AZK;


# direct methods
.method public constructor <init>(LX/AZK;)V
    .locals 0

    .prologue
    .line 1686803
    iput-object p1, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingFlowPlugin$6;->a:LX/AZK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x1de79049

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1686804
    iget-object v1, p0, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/FacecastCommercialBreakOnboardingFlowPlugin$6;->a:LX/AZK;

    .line 1686805
    iget-object v3, v1, LX/AZK;->I:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, v1, LX/AZK;->I:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1686806
    iget-object v3, v1, LX/AZK;->b:Lcom/facebook/facecast/protocol/FacecastNetworker;

    iget-object v4, v1, LX/AZK;->I:Ljava/lang/String;

    sget-object v5, Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;->AGREE_LEGAL_TERMS:Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;

    .line 1686807
    new-instance v6, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;

    invoke-direct {v6, v4, v5}, Lcom/facebook/facecast/protocol/InstreamAdsCreatorActionsRequest;-><init>(Ljava/lang/String;Lcom/facebook/video/videostreaming/protocol/InstreamAdsCreatorAPIActions;)V

    .line 1686808
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 1686809
    const-string v7, "instream_ads_creator_actions_key"

    invoke-virtual {v8, v7, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1686810
    iget-object v6, v3, Lcom/facebook/facecast/protocol/FacecastNetworker;->a:LX/0aG;

    const-string v7, "instream_ads_creator_actions_type"

    sget-object v9, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v10, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v10}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v10

    const v11, -0x15bb2f31

    invoke-static/range {v6 .. v11}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v6

    invoke-interface {v6}, LX/1MF;->start()LX/1ML;

    .line 1686811
    :cond_0
    iget-object v3, v1, LX/AZK;->l:Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/facebook/facecast/plugin/commercialbreak/onboarding/CommercialBreakOnBoardingTermsAndConditions;->setVisibility(I)V

    .line 1686812
    iget v3, v1, LX/AZK;->H:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, LX/AZK;->H:I

    .line 1686813
    invoke-static {v1}, LX/AZK;->l(LX/AZK;)V

    .line 1686814
    const v1, -0x6cb101c3

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
