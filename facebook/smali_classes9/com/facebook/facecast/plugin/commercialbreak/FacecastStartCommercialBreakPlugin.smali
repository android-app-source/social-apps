.class public Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;
.super LX/AWT;
.source ""

# interfaces
.implements LX/1bG;


# instance fields
.field private final a:Lcom/facebook/fbui/glyph/GlyphView;

.field private final b:I

.field private final c:I

.field private final f:I

.field public g:LX/AY8;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1686662
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1686663
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1686660
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686661
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1686650
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1686651
    const v0, 0x7f0305e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1686652
    new-instance v0, LX/AZA;

    invoke-direct {v0, p0}, LX/AZA;-><init>(Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;)V

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1686653
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->FacecastBottomBarToolbarBroadcaster:[I

    const v2, 0x7f01041f

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1686654
    const/16 v0, 0x1

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->b:I

    .line 1686655
    const/16 v0, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->c:I

    .line 1686656
    const/16 v0, 0x2

    invoke-virtual {v1, v0, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->f:I

    .line 1686657
    const v0, 0x7f0d1046

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1686658
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1686659
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 1686623
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->a:Lcom/facebook/fbui/glyph/GlyphView;

    const-string v1, "glyphColor"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->b:I

    aput v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->c:I

    aput v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1686624
    new-instance v1, Landroid/animation/ArgbEvaluator;

    invoke-direct {v1}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 1686625
    const-wide/16 v2, 0x258

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1686626
    sget-object v1, LX/BSf;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1686627
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1686628
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1686648
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->f:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1686649
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1686646
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->c:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1686647
    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 1686644
    iget-object v0, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget v1, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->b:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1686645
    return-void
.end method


# virtual methods
.method public final a(LX/AYp;LX/AYp;)V
    .locals 2

    .prologue
    .line 1686635
    sget-object v0, LX/AZB;->a:[I

    invoke-virtual {p2}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1686636
    :goto_0
    sget-object v0, LX/AZB;->a:[I

    invoke-virtual {p1}, LX/AYp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 1686637
    :goto_1
    return-void

    .line 1686638
    :pswitch_0
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->j()V

    goto :goto_0

    .line 1686639
    :pswitch_1
    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_UNINITIALIZED:LX/AYp;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/AYp;->COMMERCIAL_BREAK_FINISHED:LX/AYp;

    if-ne p2, v0, :cond_1

    .line 1686640
    :cond_0
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->c()V

    goto :goto_1

    .line 1686641
    :cond_1
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->i()V

    goto :goto_1

    .line 1686642
    :pswitch_2
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->g()V

    goto :goto_1

    .line 1686643
    :pswitch_3
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->j()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;LX/AY8;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1686629
    iget-boolean v1, p1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isEligible:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->isOnboarded:Z

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;->broadcasterViolations:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 1686630
    :goto_0
    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->setVisibility(I)V

    .line 1686631
    iput-object p2, p0, Lcom/facebook/facecast/plugin/commercialbreak/FacecastStartCommercialBreakPlugin;->g:LX/AY8;

    .line 1686632
    return-void

    :cond_0
    move v1, v0

    .line 1686633
    goto :goto_0

    .line 1686634
    :cond_1
    const/16 v0, 0x8

    goto :goto_1
.end method
