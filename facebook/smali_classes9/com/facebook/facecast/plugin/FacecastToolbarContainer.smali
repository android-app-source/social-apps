.class public Lcom/facebook/facecast/plugin/FacecastToolbarContainer;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/3iG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/AVU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Lcom/facebook/fbui/glyph/GlyphView;

.field private final f:Lcom/facebook/fbui/glyph/GlyphView;

.field private final g:Lcom/facebook/fbui/glyph/GlyphView;

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/graphql/model/GraphQLFeedback;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/AWk;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AYf;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:LX/AcC;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1685942
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1685943
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685924
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685925
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685926
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685927
    const-class v0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    invoke-static {v0, p0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1685928
    const v0, 0x7f0305ee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1685929
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    sget-object v1, LX/03r;->FacecastBottomBarToolbarBroadcaster:[I

    const v2, 0x7f01041f

    invoke-virtual {v0, p2, v1, v2, p3}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1685930
    const/16 v2, 0x0

    .line 1685931
    const v0, 0x7f0d1056

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1685932
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1685933
    const v0, 0x7f0d1057

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1685934
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1685935
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->c()V

    .line 1685936
    const v0, 0x7f0d1058

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->g:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1685937
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1685938
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->d()V

    .line 1685939
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1685940
    return-void
.end method

.method private static a(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;LX/3iG;LX/0ad;LX/0Ot;LX/AVU;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/facecast/plugin/FacecastToolbarContainer;",
            "LX/3iG;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "LX/AVU;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1685941
    iput-object p1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->a:LX/3iG;

    iput-object p2, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->b:LX/0ad;

    iput-object p3, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->c:LX/0Ot;

    iput-object p4, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->d:LX/AVU;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;

    const-class v0, LX/3iG;

    invoke-interface {v2, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/3iG;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    const/16 v3, 0x123

    invoke-static {v2, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static {v2}, LX/AVU;->a(LX/0QB;)LX/AVU;

    move-result-object v2

    check-cast v2, LX/AVU;

    invoke-static {p0, v0, v1, v3, v2}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->a(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;LX/3iG;LX/0ad;LX/0Ot;LX/AVU;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 1685944
    const/4 v0, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, LX/7Gl;

    invoke-interface {p1, v0, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/7Gl;

    .line 1685945
    if-eqz v0, :cond_0

    array-length v1, v0

    if-lez v1, :cond_0

    .line 1685946
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->d:LX/AVU;

    array-length v0, v0

    .line 1685947
    const-string v2, "facecast_broadcast_comment_mentions"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, v2, p0}, LX/AVU;->a(LX/AVU;Ljava/lang/String;Ljava/lang/String;)V

    .line 1685948
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 1685949
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_1

    .line 1685950
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->o:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1685951
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->o:Ljava/util/List;

    .line 1685952
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->o:Ljava/util/List;

    new-instance v1, LX/AYf;

    invoke-direct {v1, p1, p2}, LX/AYf;-><init>(Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1685953
    :goto_0
    return-void

    .line 1685954
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->b(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1685915
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v0, :cond_0

    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    if-ne v0, v1, :cond_0

    .line 1685916
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685917
    :goto_0
    return-void

    .line 1685918
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->e:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685919
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->e:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/AYa;

    invoke-direct {v1, p0}, LX/AYa;-><init>(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;I)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 1685920
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_0

    .line 1685921
    :goto_0
    return-void

    .line 1685922
    :cond_0
    new-instance v0, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    const/4 v8, 0x1

    move-object v3, p1

    move v5, v4

    move-object v7, v6

    move v9, p2

    invoke-direct/range {v0 .. v9}, Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/facebook/ipc/media/MediaItem;Lcom/facebook/ipc/media/StickerItem;ZI)V

    .line 1685923
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->a:LX/3iG;

    sget-object v2, Lcom/facebook/feedback/ui/FeedbackControllerParams;->a:Lcom/facebook/feedback/ui/FeedbackControllerParams;

    invoke-virtual {v1, v2}, LX/3iG;->a(Lcom/facebook/feedback/ui/FeedbackControllerParams;)LX/3iK;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v1, v0, v2, v6}, LX/3iK;->a(Lcom/facebook/ufiservices/cache/PendingCommentInputEntry;Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;)V

    goto :goto_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1685913
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->f:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/AYd;

    invoke-direct {v1, p0}, LX/AYd;-><init>(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685914
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1685909
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->b:LX/0ad;

    sget-short v1, LX/1v6;->n:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1685910
    :goto_0
    return-void

    .line 1685911
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->g:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1685912
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->g:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/AYe;

    invoke-direct {v1, p0}, LX/AYe;-><init>(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 1685904
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->o:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    if-nez v0, :cond_1

    .line 1685905
    :cond_0
    :goto_0
    return-void

    .line 1685906
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AYf;

    .line 1685907
    iget-object v2, v0, LX/AYf;->a:Ljava/lang/String;

    iget v0, v0, LX/AYf;->b:I

    invoke-direct {p0, v2, v0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->b(Ljava/lang/String;I)V

    goto :goto_1

    .line 1685908
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->o:Ljava/util/List;

    goto :goto_0
.end method

.method public static getCommentAuthorAuthor(Lcom/facebook/facecast/plugin/FacecastToolbarContainer;)LX/AcC;
    .locals 5

    .prologue
    .line 1685893
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->p:LX/AcC;

    if-eqz v0, :cond_0

    .line 1685894
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->p:LX/AcC;

    .line 1685895
    :goto_0
    return-object v0

    .line 1685896
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    if-eqz v0, :cond_2

    sget-object v0, LX/2rw;->PAGE:LX/2rw;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v1, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    if-ne v0, v1, :cond_2

    .line 1685897
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iget-wide v2, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 1685898
    new-instance v3, LX/AcC;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v1, v2, v4}, LX/AcC;-><init>(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    move-object v0, v3

    .line 1685899
    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->p:LX/AcC;

    .line 1685900
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->p:LX/AcC;

    goto :goto_0

    .line 1685901
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1685902
    if-eqz v0, :cond_1

    .line 1685903
    invoke-static {v0}, LX/AcC;->a(Lcom/facebook/graphql/model/GraphQLActor;)LX/AcC;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->p:LX/AcC;

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1685888
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    if-eqz v0, :cond_0

    .line 1685889
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->j:Lcom/facebook/facecastdisplay/friendInviter/LiveVideoFriendInviterDialog;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1685890
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->k:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    if-eqz v0, :cond_1

    .line 1685891
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->k:Lcom/facebook/facecastdisplay/LiveEventCommentDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1685892
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V
    .locals 0

    .prologue
    .line 1685877
    iput-object p1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->h:Ljava/lang/String;

    .line 1685878
    iput-object p2, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->i:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1685879
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->b()V

    .line 1685880
    return-void
.end method

.method public setFeedback(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 0

    .prologue
    .line 1685885
    iput-object p1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->l:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1685886
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->e()V

    .line 1685887
    return-void
.end method

.method public setPostCommentListener(LX/AWk;)V
    .locals 0
    .param p1    # LX/AWk;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685883
    iput-object p1, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->m:LX/AWk;

    .line 1685884
    return-void
.end method

.method public setTimeElapsed(J)V
    .locals 3

    .prologue
    .line 1685881
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Lcom/facebook/facecast/plugin/FacecastToolbarContainer;->n:I

    .line 1685882
    return-void
.end method
