.class public abstract Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;
.super LX/AWT;
.source ""


# static fields
.field private static final g:Ljava/lang/String;


# instance fields
.field public a:LX/AWf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/facecast/protocol/FacecastNetworker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/AYi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final h:Lcom/facebook/widget/text/BetterTextView;

.field private final i:Lcom/facebook/resources/ui/FbButton;

.field private final j:Landroid/widget/LinearLayout;

.field private final k:Lcom/facebook/user/tiles/UserTileView;

.field private final l:Lcom/facebook/widget/text/BetterTextView;

.field public final m:Landroid/view/View;

.field public final n:Landroid/view/ViewStub;

.field public o:Lcom/facebook/facecast/FacecastFacepileView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Lcom/facebook/widget/text/BetterTextView;

.field public q:LX/AYh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/1bC;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:J

.field private v:J

.field private w:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1683682
    const-class v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1683683
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683684
    const-class v0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    invoke-static {v0, p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1683685
    const v0, 0x7f0305d2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1683686
    const v0, 0x7f0d1000

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->i:Lcom/facebook/resources/ui/FbButton;

    .line 1683687
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->i:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AX4;

    invoke-direct {v1, p0}, LX/AX4;-><init>(Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1683688
    const v0, 0x7f0d1002

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->j:Landroid/widget/LinearLayout;

    .line 1683689
    const v0, 0x7f0d0ff9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/tiles/UserTileView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->k:Lcom/facebook/user/tiles/UserTileView;

    .line 1683690
    const v0, 0x7f0d0ff8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->h:Lcom/facebook/widget/text/BetterTextView;

    .line 1683691
    const v0, 0x7f0d0ffa

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 1683692
    const v0, 0x7f0d0ffb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->m:Landroid/view/View;

    .line 1683693
    const v0, 0x7f0d0ffc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->n:Landroid/view/ViewStub;

    .line 1683694
    const v0, 0x7f0d0ffd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1683695
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;

    invoke-static {v4}, LX/AWf;->b(LX/0QB;)LX/AWf;

    move-result-object v1

    check-cast v1, LX/AWf;

    invoke-static {v4}, Lcom/facebook/facecast/protocol/FacecastNetworker;->b(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;

    move-result-object v2

    check-cast v2, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v4}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    const-class p0, LX/AYi;

    invoke-interface {v4, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/AYi;

    iput-object v1, p1, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->a:LX/AWf;

    iput-object v2, p1, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->b:Lcom/facebook/facecast/protocol/FacecastNetworker;

    iput-object v3, p1, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->c:LX/03V;

    iput-object v4, p1, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->f:LX/AYi;

    return-void
.end method

.method public static i(Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1683696
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1683697
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1683698
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1683699
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setAlpha(F)V

    .line 1683700
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->j:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b05ba

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setTranslationY(F)V

    .line 1683701
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    .line 1683702
    return-void
.end method


# virtual methods
.method public a(JJLjava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerTargetData;I)V
    .locals 7

    .prologue
    .line 1683703
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1683704
    const/4 v1, 0x0

    invoke-static {v0, p3, p4, p7, v1}, LX/AXB;->a(Landroid/content/res/Resources;JLcom/facebook/ipc/composer/intent/ComposerTargetData;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    move-object v0, v1

    .line 1683705
    if-nez v0, :cond_0

    .line 1683706
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->c:LX/03V;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_setBroadcastData"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unsupported type "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p7, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is called."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1683707
    :cond_0
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683708
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->k:Lcom/facebook/user/tiles/UserTileView;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->a:LX/AWf;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b05ac

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, p7, v2, v3}, LX/AWf;->a(Lcom/facebook/ipc/composer/intent/ComposerTargetData;IZ)LX/8t9;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setParams(LX/8t9;)V

    .line 1683709
    iput-wide p1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->u:J

    .line 1683710
    iput-wide p3, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->v:J

    .line 1683711
    iput-object p5, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->s:Ljava/lang/String;

    .line 1683712
    iput-object p6, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->t:Ljava/lang/String;

    .line 1683713
    iput p8, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->w:I

    .line 1683714
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->f:LX/AYi;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->t:Ljava/lang/String;

    iget-wide v2, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->u:J

    new-instance v4, LX/AX6;

    invoke-direct {v4, p0}, LX/AX6;-><init>(Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;)V

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/AYi;->a(Ljava/lang/String;JLX/AX5;Landroid/content/Context;)LX/AYh;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->q:LX/AYh;

    .line 1683715
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 1

    .prologue
    .line 1683716
    invoke-super {p0, p1, p2}, LX/AWT;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1683717
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->q:LX/AYh;

    if-eqz v0, :cond_0

    .line 1683718
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->q:LX/AYh;

    invoke-virtual {v0}, LX/AYh;->b()V

    .line 1683719
    :cond_0
    return-void
.end method

.method public c()V
    .locals 6

    .prologue
    .line 1683720
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1683721
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, LX/03r;->FacecastCopyrightDialog:[I

    const v4, 0x7f01046a

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 1683722
    const/16 v2, 0x0

    invoke-static {v0, v1, v2}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v2

    .line 1683723
    const/16 v3, 0x1

    invoke-static {v0, v1, v3}, LX/1z0;->a(Landroid/content/Context;Landroid/content/res/TypedArray;I)Ljava/lang/String;

    move-result-object v0

    .line 1683724
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1683725
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->h:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683726
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1683727
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->k:Lcom/facebook/user/tiles/UserTileView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/user/tiles/UserTileView;->setVisibility(I)V

    .line 1683728
    invoke-static {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->i(Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;)V

    .line 1683729
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 1683730
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->r:LX/1bC;

    if-eqz v0, :cond_0

    .line 1683731
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->r:LX/1bC;

    invoke-interface {v0}, LX/1bC;->t()V

    .line 1683732
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1683733
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->b:Lcom/facebook/facecast/protocol/FacecastNetworker;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/protocol/FacecastNetworker;->b(Ljava/lang/String;)V

    .line 1683734
    return-void
.end method

.method public iF_()V
    .locals 15

    .prologue
    .line 1683735
    invoke-super {p0}, LX/AWT;->iF_()V

    .line 1683736
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    .line 1683737
    iget-object v1, v0, LX/AVF;->c:LX/AVE;

    move-object v0, v1

    .line 1683738
    sget-object v1, LX/AVE;->COPYRIGHT_VIOLATION:LX/AVE;

    if-ne v0, v1, :cond_1

    .line 1683739
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->c()V

    .line 1683740
    :cond_0
    :goto_0
    return-void

    .line 1683741
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->q:LX/AYh;

    if-eqz v0, :cond_2

    .line 1683742
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->q:LX/AYh;

    invoke-virtual {v0}, LX/AYh;->a()V

    .line 1683743
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->s:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1683744
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->b:Lcom/facebook/facecast/protocol/FacecastNetworker;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->s:Ljava/lang/String;

    iget v2, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->w:I

    iget-wide v4, p0, Lcom/facebook/facecast/plugin/FacecastEndScreenBasePlugin;->v:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    .line 1683745
    new-instance v8, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;

    invoke-direct {v8, v1, v2, v4, v5}, Lcom/facebook/facecast/protocol/VideoBroadcastCalculateStatsRequest;-><init>(Ljava/lang/String;IJ)V

    .line 1683746
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 1683747
    const-string v9, "video_broadcast_calculate_stats_key"

    invoke-virtual {v10, v9, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1683748
    iget-object v8, v0, Lcom/facebook/facecast/protocol/FacecastNetworker;->a:LX/0aG;

    const-string v9, "video_broadcast_calculate_stats_type"

    sget-object v11, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v12, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v12}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v12

    const v13, -0x20c9d44f

    invoke-static/range {v8 .. v13}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v8

    invoke-interface {v8}, LX/1MF;->start()LX/1ML;

    .line 1683749
    goto :goto_0
.end method
