.class public Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field private m:Landroid/view/View;

.field private n:Landroid/widget/ImageView;

.field private o:Lcom/facebook/resources/ui/FbTextView;

.field private p:Lcom/facebook/resources/ui/FbTextView;

.field private q:Lcom/facebook/resources/ui/FbButton;

.field private r:Lcom/facebook/resources/ui/FbTextView;

.field public s:LX/AXs;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1684629
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1684630
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1684624
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 1684625
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1684626
    const-string v2, "square_view_height"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1684627
    return-void

    .line 1684628
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b059c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 1684621
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1684622
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->a()V

    .line 1684623
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0xccd86d8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1684589
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1684590
    const v1, 0x7f0e0390

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1684591
    const/16 v1, 0x2b

    const v2, -0x1df8201d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x472c3ce1

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1684620
    const v1, 0x7f0305e3

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x371a1a5c

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1684592
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1684593
    new-instance v1, LX/AXn;

    invoke-direct {v1, p0}, LX/AXn;-><init>(Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;)V

    .line 1684594
    const v0, 0x7f0d1037

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->m:Landroid/view/View;

    .line 1684595
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->a()V

    .line 1684596
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->m:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1684597
    const v0, 0x7f0d103c

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->q:Lcom/facebook/resources/ui/FbButton;

    .line 1684598
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->q:Lcom/facebook/resources/ui/FbButton;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1684599
    const v0, 0x7f0d1039

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->n:Landroid/widget/ImageView;

    .line 1684600
    const v0, 0x7f0d103a

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 1684601
    const v0, 0x7f0d103b

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 1684602
    const v0, 0x7f0d103d

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1684603
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1684604
    const-string v1, "conflicting_status"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/AXr;

    .line 1684605
    if-nez v0, :cond_1

    .line 1684606
    :cond_0
    :goto_0
    return-void

    .line 1684607
    :cond_1
    sget-object v1, LX/AXq;->a:[I

    invoke-virtual {v0}, LX/AXr;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1684608
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->n:Landroid/widget/ImageView;

    const v1, 0x7f021123

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1684609
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->o:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080d12

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684610
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->p:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080d13

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684611
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080d14

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684612
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->s:LX/AXs;

    if-eqz v0, :cond_0

    .line 1684613
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/AXo;

    invoke-direct {v1, p0}, LX/AXo;-><init>(Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 1684614
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->n:Landroid/widget/ImageView;

    const v1, 0x7f021122

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1684615
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->o:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080d0f

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684616
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->p:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080d10

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684617
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f080d11

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1684618
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->s:LX/AXs;

    if-eqz v0, :cond_0

    .line 1684619
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/AXp;

    invoke-direct {v1, p0}, LX/AXp;-><init>(Lcom/facebook/facecast/plugin/FacecastMonetizationConflictingDialog;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
