.class public Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public A:Ljava/lang/String;

.field public B:Z

.field public C:LX/AaC;

.field public m:LX/Acy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Landroid/os/Handler;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/AaB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:Landroid/view/ViewStub;

.field private u:Lcom/facebook/resources/ui/FbButton;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/view/ViewGroup;

.field private x:Lcom/facebook/resources/ui/FbTextView;

.field private y:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field private z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1688344
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1688345
    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V
    .locals 4

    .prologue
    .line 1688338
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->s:LX/AaB;

    .line 1688339
    iget-object v1, v0, LX/AaB;->d:LX/0if;

    sget-object v2, LX/0ig;->E:LX/0ih;

    const-string v3, "tap_create_fundraiser"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1688340
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->p:LX/17Y;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->gW:Ljava/lang/String;

    const-string v3, "live_donation"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1688341
    const-string v1, "finish_after_creation_enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1688342
    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->o:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x1db4

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 1688343
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;)V
    .locals 5
    .param p1    # Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    .line 1688266
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->w:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688267
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;->a()I

    move-result v0

    if-nez v0, :cond_1

    .line 1688268
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->t:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->v:Landroid/view/ViewGroup;

    .line 1688269
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->v:Landroid/view/ViewGroup;

    const v1, 0x7f0d0feb

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->u:Lcom/facebook/resources/ui/FbButton;

    .line 1688270
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->u:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/AaJ;

    invoke-direct {v1, p0}, LX/AaJ;-><init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1688271
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->x:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1688272
    :goto_0
    return-void

    .line 1688273
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->x:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1688274
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    .line 1688275
    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->y:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1688276
    new-instance v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;-><init>(Landroid/content/Context;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    .line 1688277
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->A:Ljava/lang/String;

    const/4 p1, -0x1

    .line 1688278
    if-nez v1, :cond_2

    .line 1688279
    iput p1, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    .line 1688280
    :cond_2
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    iget-object v2, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    .line 1688281
    iget-object v2, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->b:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;

    .line 1688282
    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->u()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1688283
    iput v3, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    .line 1688284
    :goto_2
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->B:Z

    .line 1688285
    iput-boolean v1, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->g:Z

    .line 1688286
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    new-instance v1, LX/AaJ;

    invoke-direct {v1, p0}, LX/AaJ;-><init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V

    .line 1688287
    iput-object v1, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->e:LX/AaJ;

    .line 1688288
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    .line 1688289
    iput-object p0, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->f:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    .line 1688290
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->y:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 1688291
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->y:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    goto :goto_0

    .line 1688292
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 1688293
    :cond_4
    iput p1, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    goto :goto_2
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 1688324
    if-eqz p1, :cond_1

    .line 1688325
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->s:LX/AaB;

    .line 1688326
    iget-object v1, v0, LX/AaB;->d:LX/0if;

    sget-object v2, LX/0ig;->E:LX/0ih;

    const-string p1, "deselect_fundraiser"

    invoke-virtual {v1, v2, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1688327
    :goto_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->C:LX/AaC;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    if-eqz v0, :cond_0

    .line 1688328
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->C:LX/AaC;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->z:Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    .line 1688329
    iget v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    const/4 p1, -0x1

    if-ne v2, p1, :cond_2

    .line 1688330
    const/4 v2, 0x0

    .line 1688331
    :goto_1
    move-object v1, v2

    .line 1688332
    invoke-interface {v0, v1}, LX/AaC;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1688333
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->n:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog$3;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog$3;-><init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V

    const v2, 0x3da47133

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1688334
    return-void

    .line 1688335
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->s:LX/AaB;

    .line 1688336
    iget-object v1, v0, LX/AaB;->d:LX/0if;

    sget-object v2, LX/0ig;->E:LX/0ih;

    const-string p1, "select_fundraiser"

    invoke-virtual {v1, v2, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1688337
    goto :goto_0

    :cond_2
    iget-object v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->b:Ljava/util/List;

    iget p1, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v2

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1688346
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1688347
    const/16 v0, 0x1db4

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1688348
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->m:LX/Acy;

    invoke-virtual {v0, p0}, LX/Acy;->a(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V

    .line 1688349
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x16db7d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1688320
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1688321
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v4, p0

    check-cast v4, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    invoke-static {v1}, LX/Acy;->b(LX/0QB;)LX/Acy;

    move-result-object v5

    check-cast v5, LX/Acy;

    invoke-static {v1}, LX/43r;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-static {v1}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v9

    check-cast v9, LX/1b4;

    invoke-static {v1}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p1

    check-cast p1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v1}, LX/AaB;->a(LX/0QB;)LX/AaB;

    move-result-object v1

    check-cast v1, LX/AaB;

    iput-object v5, v4, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->m:LX/Acy;

    iput-object v6, v4, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->n:Landroid/os/Handler;

    iput-object v7, v4, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->o:Lcom/facebook/content/SecureContextHelper;

    iput-object v8, v4, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->p:LX/17Y;

    iput-object v9, v4, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->q:LX/1b4;

    iput-object p1, v4, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->r:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object v1, v4, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->s:LX/AaB;

    .line 1688322
    const v1, 0x7f0e05cc

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1688323
    const/16 v1, 0x2b

    const v2, -0xa1b86cf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x24bf810

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1688319
    const v1, 0x7f0305cb

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7d9cb16e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688294
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1688295
    const v0, 0x7f0d0fe8

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->t:Landroid/view/ViewStub;

    .line 1688296
    const v0, 0x7f0d0e9d

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->w:Landroid/view/ViewGroup;

    .line 1688297
    const v0, 0x7f0d0fe9

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->x:Lcom/facebook/resources/ui/FbTextView;

    .line 1688298
    const v0, 0x7f0d0fea

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->y:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1688299
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->q:LX/1b4;

    .line 1688300
    iget-object v1, v0, LX/1b4;->b:LX/0Uh;

    const/16 v2, 0x4bd

    const/4 p2, 0x0

    invoke-virtual {v1, v2, p2}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1688301
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->r:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1688302
    iget-boolean v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v1

    .line 1688303
    if-nez v0, :cond_0

    .line 1688304
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->B:Z

    .line 1688305
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->m:LX/Acy;

    invoke-virtual {v0, p0}, LX/Acy;->a(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V

    .line 1688306
    invoke-static {p1}, LX/63Z;->a(Landroid/view/View;)Z

    .line 1688307
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1688308
    const v1, 0x7f080ceb

    invoke-virtual {p0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1688309
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1688310
    new-instance v1, LX/AaH;

    invoke-direct {v1, p0}, LX/AaH;-><init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1688311
    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->B:Z

    if-nez v1, :cond_1

    .line 1688312
    :goto_0
    return-void

    .line 1688313
    :cond_1
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    const v2, 0x7f080cf1

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1688314
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1688315
    move-object v1, v1

    .line 1688316
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1688317
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1688318
    new-instance v1, LX/AaI;

    invoke-direct {v1, p0}, LX/AaI;-><init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    goto :goto_0
.end method
