.class public Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/content/Context;

.field public d:I

.field public e:LX/AaJ;

.field public f:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1688241
    const-class v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;)V
    .locals 1

    .prologue
    .line 1688236
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1688237
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    .line 1688238
    iput-object p1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->c:Landroid/content/Context;

    .line 1688239
    invoke-virtual {p2}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel;->j()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->b:Ljava/util/List;

    .line 1688240
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1688227
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1688228
    if-nez p2, :cond_0

    .line 1688229
    const v1, 0x7f0305c9

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1688230
    new-instance v0, LX/AaG;

    invoke-direct {v0, p0, v1}, LX/AaG;-><init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;Landroid/view/View;)V

    .line 1688231
    :goto_0
    return-object v0

    .line 1688232
    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    .line 1688233
    const v1, 0x7f0305ca

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1688234
    new-instance v0, LX/AaF;

    invoke-direct {v0, p0, v1}, LX/AaF;-><init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;Landroid/view/View;)V

    goto :goto_0

    .line 1688235
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 12

    .prologue
    .line 1688196
    instance-of v0, p1, LX/AaG;

    if-eqz v0, :cond_2

    .line 1688197
    check-cast p1, LX/AaG;

    .line 1688198
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;

    .line 1688199
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$FacecastConnectedFundraiserQueryModel$PersonForCharityFundraisersForLiveVideosModel$EdgesModel;->a()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-result-object v0

    .line 1688200
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1688201
    const/4 v11, 0x3

    const/4 v4, 0x0

    const/4 v10, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1688202
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_5

    .line 1688203
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1688204
    invoke-virtual {v5, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_0
    if-eqz v1, :cond_0

    .line 1688205
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1688206
    iget-object v6, p1, LX/AaG;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v5, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v5, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v6, v1, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1688207
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->n()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1688208
    iget-object v1, p1, LX/AaG;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688209
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->t()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_6

    move-object v1, v4

    .line 1688210
    :goto_1
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_9

    .line 1688211
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object v5

    .line 1688212
    :goto_2
    iget-object v6, p1, LX/AaG;->n:Lcom/facebook/resources/ui/FbTextView;

    iget-object v7, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->c:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f080cf0

    new-array v9, v10, [Ljava/lang/Object;

    aput-object v1, v9, v3

    aput-object v5, v9, v2

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688213
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    .line 1688214
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->l()Ljava/lang/String;

    move-result-object v5

    .line 1688215
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->q()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    if-nez v6, :cond_7

    .line 1688216
    :goto_3
    if-nez v4, :cond_8

    .line 1688217
    new-array v4, v11, [Ljava/lang/CharSequence;

    aput-object v5, v4, v3

    const-string v3, " / "

    aput-object v3, v4, v2

    aput-object v1, v4, v10

    invoke-static {v4}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1688218
    :goto_4
    iget-object v2, p1, LX/AaG;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688219
    iget-object v1, p1, LX/AaG;->p:Lcom/facebook/fbui/glyph/GlyphView;

    iget v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->d:I

    if-ne p2, v0, :cond_3

    const/4 v0, 0x0

    :goto_5
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1688220
    :cond_2
    return-void

    .line 1688221
    :cond_3
    const/4 v0, 0x4

    goto :goto_5

    :cond_4
    move v1, v3

    .line 1688222
    goto/16 :goto_0

    :cond_5
    move v1, v3

    goto/16 :goto_0

    .line 1688223
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->t()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1688224
    invoke-virtual {v5, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 1688225
    :cond_7
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->q()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v6, v4, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    goto :goto_3

    .line 1688226
    :cond_8
    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/CharSequence;

    aput-object v5, v6, v3

    const-string v3, " / "

    aput-object v3, v6, v2

    aput-object v1, v6, v10

    const-string v1, " \u2022 "

    aput-object v1, v6, v11

    const/4 v1, 0x4

    aput-object v4, v6, v1

    invoke-static {v6}, Landroid/text/TextUtils;->concat([Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    :cond_9
    move-object v5, v4

    goto/16 :goto_2
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1688193
    iget-boolean v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1688194
    const/4 v0, 0x1

    .line 1688195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1688190
    iget-boolean v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->g:Z

    if-eqz v0, :cond_0

    .line 1688191
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1688192
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationConnectedFundraiserAdapter;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
