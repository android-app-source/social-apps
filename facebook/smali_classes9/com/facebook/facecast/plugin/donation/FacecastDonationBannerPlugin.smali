.class public Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;
.super LX/AWT;
.source ""

# interfaces
.implements LX/AaC;
.implements LX/AaD;
.implements LX/AaE;


# instance fields
.field public a:LX/AaL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/AaB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

.field private i:Landroid/widget/FrameLayout;

.field public j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:LX/AYO;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1688122
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1688123
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688120
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1688121
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688111
    invoke-direct {p0, p1, p2, p3}, LX/AWT;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1688112
    const v0, 0x7f0305c8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1688113
    const-class v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    invoke-static {v0, p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1688114
    const v0, 0x7f0d0fe2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688115
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688116
    iput-object p0, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->i:LX/AaE;

    .line 1688117
    const v0, 0x7f0d0fe0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->i:Landroid/widget/FrameLayout;

    .line 1688118
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->k()V

    .line 1688119
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    new-instance v0, LX/AaL;

    invoke-static {v4}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v4}, LX/0UA;->b(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v4}, LX/AaB;->a(LX/0QB;)LX/AaB;

    move-result-object v3

    check-cast v3, LX/AaB;

    invoke-static {v4}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p0

    check-cast p0, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {v0, v1, v2, v3, p0}, LX/AaL;-><init>(LX/0tX;Ljava/util/concurrent/ScheduledExecutorService;LX/AaB;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    move-object v1, v0

    check-cast v1, LX/AaL;

    invoke-static {v4}, LX/AaB;->a(LX/0QB;)LX/AaB;

    move-result-object v2

    check-cast v2, LX/AaB;

    invoke-static {v4}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v3

    check-cast v3, Landroid/os/Handler;

    invoke-static {v4}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    iput-object v1, p1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->a:LX/AaL;

    iput-object v2, p1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->b:LX/AaB;

    iput-object v3, p1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->c:Landroid/os/Handler;

    iput-object v4, p1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->f:LX/0kL;

    return-void
.end method

.method private k()V
    .locals 6

    .prologue
    const/16 v1, 0x8

    const/4 v5, 0x0

    .line 1688124
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-nez v0, :cond_0

    .line 1688125
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1688126
    :goto_0
    return-void

    .line 1688127
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->i:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1688128
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688129
    iget-boolean v2, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->g:Z

    move v0, v2

    .line 1688130
    if-nez v0, :cond_1

    .line 1688131
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b()V

    .line 1688132
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688133
    iget-object v2, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->e:Lcom/facebook/fig/button/FigButton;

    move-object v0, v2

    .line 1688134
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1688135
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688136
    iget-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->f:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v1

    .line 1688137
    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1688138
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688139
    iget-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->d:Landroid/widget/ProgressBar;

    move-object v0, v1

    .line 1688140
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020e28

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1688141
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688142
    iget-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->c:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v1

    .line 1688143
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1688144
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688145
    iget-object v1, v0, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b:Lcom/facebook/resources/ui/FbTextView;

    move-object v0, v1

    .line 1688146
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1688147
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 1688148
    :goto_1
    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688149
    iget-object v2, v1, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->b:Lcom/facebook/resources/ui/FbTextView;

    move-object v1, v2

    .line 1688150
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080c43

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688151
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1688152
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1688153
    iget-object v2, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    .line 1688154
    iget-object v3, v2, Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v2, v3

    .line 1688155
    invoke-virtual {v1, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-class v1, Lcom/facebook/facecastdisplay/donation/LiveDonationController;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1688156
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->h:Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-static {v0, v1}, Lcom/facebook/facecastdisplay/donation/LiveDonationController;->a(Lcom/facebook/facecastdisplay/donation/LiveDonationEntryView;Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    goto/16 :goto_0

    .line 1688157
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->p()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 2
    .param p1    # Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1688107
    iput-object p1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1688108
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->k:LX/AYO;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-interface {v0, v1}, LX/AYO;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1688109
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->k()V

    .line 1688110
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 1688094
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->b:LX/AaB;

    .line 1688095
    iget-object v1, v0, LX/AaB;->d:LX/0if;

    sget-object v2, LX/0ig;->E:LX/0ih;

    const-string v3, "open_fundraiser_picker"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 1688096
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, LX/0ew;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ew;

    .line 1688097
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "FACECAST_DONATION_FUNDRAISER_DIALOG_TAG"

    invoke-virtual {v1, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1688098
    :cond_0
    :goto_0
    return-void

    .line 1688099
    :cond_1
    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->g:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    if-nez v1, :cond_2

    .line 1688100
    new-instance v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    invoke-direct {v1}, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;-><init>()V

    iput-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->g:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    .line 1688101
    :cond_2
    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->g:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    .line 1688102
    iput-object p0, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->C:LX/AaC;

    .line 1688103
    iget-object v2, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->g:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    .line 1688104
    :goto_1
    iput-object v1, v2, Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;->A:Ljava/lang/String;

    .line 1688105
    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->g:Lcom/facebook/facecast/plugin/donation/FacecastDonationFundraiserSelectionDialog;

    invoke-interface {v0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const-string v2, "FACECAST_DONATION_FUNDRAISER_DIALOG_TAG"

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0hH;Ljava/lang/String;Z)I

    goto :goto_0

    .line 1688106
    :cond_3
    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->u()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 1688089
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->setVisibility(I)V

    .line 1688090
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1688091
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->k:LX/AYO;

    if-eqz v0, :cond_0

    .line 1688092
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->k:LX/AYO;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-interface {v0, v1}, LX/AYO;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V

    .line 1688093
    :cond_0
    return-void
.end method

.method public final iJ_()V
    .locals 0

    .prologue
    .line 1688087
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->c()V

    .line 1688088
    return-void
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 1688084
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1688085
    iget-object v0, p0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin$1;

    invoke-direct {v1, p0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin$1;-><init>(Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;)V

    const v2, -0x669d6fc5

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1688086
    return-void
.end method
