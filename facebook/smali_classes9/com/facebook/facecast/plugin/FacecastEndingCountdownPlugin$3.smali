.class public final Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/AXg;


# direct methods
.method public constructor <init>(LX/AXg;)V
    .locals 0

    .prologue
    .line 1684403
    iput-object p1, p0, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;->a:LX/AXg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1684404
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;->a:LX/AXg;

    iget-object v0, v0, LX/AXg;->c:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;->a:LX/AXg;

    iget v1, v1, LX/AXg;->k:I

    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;->a:LX/AXg;

    invoke-virtual {v2}, LX/AXg;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 1684405
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;->a:LX/AXg;

    iget-object v0, v0, LX/AXg;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;->a:LX/AXg;

    iget v1, v1, LX/AXg;->k:I

    mul-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->yBy(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;->a:LX/AXg;

    iget v1, v1, LX/AXg;->l:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, LX/AXd;

    invoke-direct {v1, p0}, LX/AXd;-><init>(Lcom/facebook/facecast/plugin/FacecastEndingCountdownPlugin$3;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 1684406
    return-void
.end method
