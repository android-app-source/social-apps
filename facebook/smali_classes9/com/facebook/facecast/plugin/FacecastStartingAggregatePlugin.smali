.class public Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;
.super LX/AWU;
.source ""

# interfaces
.implements LX/AWW;
.implements LX/AWx;
.implements LX/AYM;
.implements LX/AYN;
.implements LX/AYO;
.implements LX/AYP;
.implements LX/AYQ;


# instance fields
.field private A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private B:Z

.field private C:LX/1bE;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Lcom/facebook/facecast/protocol/FacecastNetworker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AVT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Acy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/AaB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final j:LX/AWy;

.field public final k:LX/AYY;

.field public final l:LX/AZK;

.field public final m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

.field private final n:LX/AWX;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:LX/AWy;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/AWy;

.field private q:LX/AWb;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:LX/AXh;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private s:LX/AWc;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private t:LX/AWb;

.field private u:LX/AYo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Lcom/facebook/video/videostreaming/protocol/CommercialBreakSettings;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1685354
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1685355
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685312
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685313
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685314
    invoke-direct {p0, p1, p2, p3}, LX/AWU;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1685315
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;

    invoke-static {v0}, Lcom/facebook/facecast/protocol/FacecastNetworker;->b(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;

    move-result-object v3

    check-cast v3, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v4

    check-cast v4, LX/Ac6;

    invoke-static {v0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v5

    check-cast v5, LX/AVT;

    invoke-static {v0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object v6

    check-cast v6, LX/1b4;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object p2

    check-cast p2, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, LX/Acy;->b(LX/0QB;)LX/Acy;

    move-result-object p3

    check-cast p3, LX/Acy;

    invoke-static {v0}, LX/AaB;->a(LX/0QB;)LX/AaB;

    move-result-object v0

    check-cast v0, LX/AaB;

    iput-object v3, v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->a:Lcom/facebook/facecast/protocol/FacecastNetworker;

    iput-object v4, v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->b:LX/Ac6;

    iput-object v5, v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->c:LX/AVT;

    iput-object v6, v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->f:LX/1b4;

    iput-object p2, v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p3, v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->h:LX/Acy;

    iput-object v0, v2, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->i:LX/AaB;

    .line 1685316
    new-instance v0, LX/AWy;

    invoke-direct {v0, p1}, LX/AWy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    .line 1685317
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    .line 1685318
    iput-object p0, v0, LX/AWy;->A:LX/AWx;

    .line 1685319
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    .line 1685320
    new-instance v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    .line 1685321
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    .line 1685322
    iput-object p0, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->k:LX/AYO;

    .line 1685323
    new-instance v0, LX/AZK;

    invoke-direct {v0, p1}, LX/AZK;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    .line 1685324
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    .line 1685325
    iput-object p0, v0, LX/AZK;->E:LX/AYN;

    .line 1685326
    new-instance v0, LX/AYY;

    invoke-direct {v0, p1}, LX/AYY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    .line 1685327
    const/4 v0, 0x3

    new-array v0, v0, [LX/AWT;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, LX/AWU;->a([LX/AWT;)V

    .line 1685328
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->b:LX/Ac6;

    .line 1685329
    iget-object v1, v0, LX/Ac6;->a:LX/0Uh;

    const/16 v2, 0x353

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 1685330
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->b:LX/Ac6;

    invoke-virtual {v0}, LX/Ac6;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1685331
    new-instance v0, LX/AWX;

    invoke-direct {v0, p1}, LX/AWX;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n:LX/AWX;

    .line 1685332
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n:LX/AWX;

    .line 1685333
    iget-object v1, v0, LX/AWX;->a:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/AWV;

    invoke-direct {v2, v0, p0}, LX/AWV;-><init>(LX/AWX;LX/AWW;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685334
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n:LX/AWX;

    invoke-virtual {p0, v0}, LX/AWU;->b(LX/AWT;)V

    .line 1685335
    :goto_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->f:LX/1b4;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685336
    iget-boolean v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v2

    .line 1685337
    invoke-virtual {v0, v1}, LX/1b4;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1685338
    new-instance v0, LX/AYo;

    invoke-direct {v0, p1}, LX/AYo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    .line 1685339
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    .line 1685340
    iput-object p0, v0, LX/AYo;->k:LX/AYM;

    .line 1685341
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    invoke-virtual {p0, v0}, LX/AWU;->b(LX/AWT;)V

    .line 1685342
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    invoke-virtual {p0, v0}, LX/AWU;->b(LX/AWT;)V

    .line 1685343
    return-void

    .line 1685344
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n:LX/AWX;

    goto :goto_0
.end method

.method private a(LX/AWy;LX/AWy;)V
    .locals 1

    .prologue
    .line 1685345
    invoke-virtual {p1}, LX/AWy;->getDescription()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/AWy;->a(Ljava/lang/String;)V

    .line 1685346
    iget-object v0, p1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v0, v0

    .line 1685347
    invoke-virtual {p2, v0}, LX/AWy;->a(Lcom/facebook/facecast/model/FacecastPrivacyData;)V

    .line 1685348
    invoke-virtual {p0, p2, p1}, LX/AWU;->a(LX/AWT;LX/AWT;)V

    .line 1685349
    iget-object v0, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1685350
    :goto_0
    iput-object p2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    .line 1685351
    return-void

    .line 1685352
    :cond_0
    iget-object v0, p0, LX/AWU;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1685353
    invoke-virtual {p1}, LX/AWT;->d()V

    goto :goto_0
.end method

.method private static c(Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 6
    .param p0    # Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 1685277
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685278
    iget-boolean v2, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v0, v2

    .line 1685279
    if-nez v0, :cond_2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1685280
    if-eqz p1, :cond_0

    .line 1685281
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1685282
    if-eqz v0, :cond_0

    .line 1685283
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1685284
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    if-nez v0, :cond_4

    :cond_0
    move v0, v3

    .line 1685285
    :goto_0
    move v0, v0

    .line 1685286
    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->f:LX/1b4;

    .line 1685287
    iget-object v2, v0, LX/1b4;->b:LX/0Uh;

    const/16 v3, 0x43d

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v0, v2

    .line 1685288
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1685289
    :goto_1
    if-eqz v0, :cond_3

    .line 1685290
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    invoke-virtual {v0, v1}, LX/AYY;->a(Z)V

    .line 1685291
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    if-eqz v0, :cond_1

    .line 1685292
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    invoke-virtual {v0}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->i()V

    .line 1685293
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v1

    .line 1685294
    goto :goto_1

    .line 1685295
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->h:LX/Acy;

    .line 1685296
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1685297
    new-instance v1, LX/AdC;

    invoke-direct {v1}, LX/AdC;-><init>()V

    move-object v1, v1

    .line 1685298
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1685299
    iget-object v2, v0, LX/Acy;->b:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 1685300
    new-instance v2, LX/Acw;

    invoke-direct {v2, v0, p0}, LX/Acw;-><init>(LX/Acy;LX/AYQ;)V

    iget-object v3, v0, LX/Acy;->c:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1685301
    goto :goto_2

    .line 1685302
    :cond_4
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1685303
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->p()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v4

    .line 1685304
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->b()Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne v0, v5, :cond_7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v2, :cond_5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v5, "114000975315193"

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v2

    goto/16 :goto_0

    :cond_7
    move v0, v3

    goto/16 :goto_0
.end method

.method public static n(Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 1685356
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    invoke-virtual {v0}, LX/AWy;->h()V

    .line 1685357
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    invoke-virtual {v0, v1}, LX/AWy;->setVisibility(I)V

    .line 1685358
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    invoke-virtual {v0, v1}, LX/AYY;->setVisibility(I)V

    .line 1685359
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->setVisibility(I)V

    .line 1685360
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n:LX/AWX;

    if-eqz v0, :cond_0

    .line 1685361
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n:LX/AWX;

    invoke-virtual {v0, v1}, LX/AWX;->setVisibility(I)V

    .line 1685362
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    if-eqz v0, :cond_1

    .line 1685363
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    invoke-virtual {v0, v1}, LX/AYo;->setVisibility(I)V

    .line 1685364
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->t:LX/AWb;

    const/4 v2, 0x0

    .line 1685365
    iget-object v1, v0, LX/AWb;->f:LX/AWn;

    if-nez v1, :cond_2

    .line 1685366
    new-instance v1, LX/AWn;

    invoke-direct {v1, v0}, LX/AWn;-><init>(LX/AWb;)V

    iput-object v1, v0, LX/AWb;->f:LX/AWn;

    .line 1685367
    :cond_2
    iget-object v1, v0, LX/AWb;->c:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1685368
    iget-object v1, v0, LX/AWb;->f:LX/AWn;

    invoke-virtual {v1}, LX/AWn;->start()Landroid/os/CountDownTimer;

    .line 1685369
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->x:Z

    .line 1685370
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    if-eqz v0, :cond_3

    .line 1685371
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    invoke-interface {v0}, LX/1bE;->v()V

    .line 1685372
    :cond_3
    return-void
.end method

.method private o()V
    .locals 1

    .prologue
    .line 1685373
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->t:LX/AWb;

    invoke-virtual {v0}, LX/AWb;->g()V

    .line 1685374
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->x:Z

    .line 1685375
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    if-eqz v0, :cond_0

    .line 1685376
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    invoke-interface {v0}, LX/1bE;->w()V

    .line 1685377
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/facecast/model/FacecastCompositionData;Ljava/lang/String;ZZ)Lcom/facebook/facecast/model/FacecastCompositionData;
    .locals 10

    .prologue
    .line 1685378
    new-instance v1, LX/AWP;

    invoke-direct {v1, p1}, LX/AWP;-><init>(Lcom/facebook/facecast/model/FacecastCompositionData;)V

    if-eqz p3, :cond_1

    if-nez p4, :cond_1

    const/4 v0, 0x1

    .line 1685379
    :goto_0
    iput-boolean v0, v1, LX/AWP;->m:Z

    .line 1685380
    move-object v0, v1

    .line 1685381
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->A:Ljava/lang/String;

    .line 1685382
    iput-object v1, v0, LX/AWP;->r:Ljava/lang/String;

    .line 1685383
    move-object v0, v0

    .line 1685384
    iput-boolean p4, v0, LX/AWP;->n:Z

    .line 1685385
    move-object v0, v0

    .line 1685386
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->f:LX/1b4;

    .line 1685387
    iget-object v2, v1, LX/1b4;->b:LX/0Uh;

    const/16 v3, 0x51d

    const/4 p1, 0x0

    invoke-virtual {v2, v3, p1}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 1685388
    iput-boolean v1, v0, LX/AWP;->o:Z

    .line 1685389
    move-object v0, v0

    .line 1685390
    invoke-virtual {v0}, LX/AWP;->a()Lcom/facebook/facecast/model/FacecastCompositionData;

    move-result-object v0

    .line 1685391
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->A:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1685392
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->c:LX/AVT;

    const-string v2, "geotargeting_broadcast_started"

    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->A:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/AVT;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685393
    :cond_0
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->a:Lcom/facebook/facecast/protocol/FacecastNetworker;

    .line 1685394
    new-instance v4, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;

    invoke-direct {v4, p2, v0}, Lcom/facebook/facecast/protocol/VideoBroadcastUpdateRequest;-><init>(Ljava/lang/String;Lcom/facebook/facecast/model/FacecastCompositionData;)V

    .line 1685395
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 1685396
    const-string v5, "video_broadcast_update_key"

    invoke-virtual {v6, v5, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1685397
    iget-object v4, v1, Lcom/facebook/facecast/protocol/FacecastNetworker;->a:LX/0aG;

    const-string v5, "video_broadcast_update_type"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v8, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    const v9, 0x1686ddfb

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    .line 1685398
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    .line 1685399
    iget-object v2, v1, LX/AYY;->g:LX/Ah9;

    iget-boolean v3, v1, LX/AYY;->I:Z

    .line 1685400
    new-instance v5, LX/6Tf;

    invoke-direct {v5}, LX/6Tf;-><init>()V

    move-object v5, v5

    .line 1685401
    new-instance v6, LX/4JP;

    invoke-direct {v6}, LX/4JP;-><init>()V

    iget-object v7, v2, LX/Ah9;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685402
    iget-object v8, v7, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v7, v8

    .line 1685403
    const-string v8, "actor_id"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685404
    move-object v6, v6

    .line 1685405
    const-string v7, "video_broadcast_id"

    invoke-virtual {v6, v7, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685406
    move-object v6, v6

    .line 1685407
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 1685408
    const-string v8, "video_tip_jar_enabled"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1685409
    move-object v6, v6

    .line 1685410
    iget-object v7, v5, LX/0gW;->h:Ljava/lang/String;

    move-object v7, v7

    .line 1685411
    const-string v8, "client_mutation_id"

    invoke-virtual {v6, v8, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685412
    move-object v6, v6

    .line 1685413
    goto :goto_1

    .line 1685414
    :goto_1
    const-string v7, "input"

    invoke-virtual {v5, v7, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1685415
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 1685416
    iget-object v6, v2, LX/Ah9;->b:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1685417
    iget-object v2, v1, LX/AYY;->h:LX/AaQ;

    iget-boolean v3, v1, LX/AYY;->I:Z

    invoke-virtual {v2, v3}, LX/AaQ;->a(Z)V

    .line 1685418
    iget-object v2, v1, LX/AYY;->p:LX/AaY;

    invoke-virtual {v2}, LX/AaY;->e()V

    .line 1685419
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    .line 1685420
    iget-object v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->b:LX/AaB;

    .line 1685421
    iput-object p2, v2, LX/AaB;->c:Ljava/lang/String;

    .line 1685422
    iget-object v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-nez v2, :cond_2

    .line 1685423
    iget-object v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->b:LX/AaB;

    .line 1685424
    iget-object v3, v2, LX/AaB;->d:LX/0if;

    sget-object v4, LX/0ig;->E:LX/0ih;

    const-string v5, "go_live_with_no_fundraiser"

    const/4 v6, 0x0

    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v7

    const-string v1, "broadcast_id"

    iget-object p2, v2, LX/AaB;->c:Ljava/lang/String;

    invoke-virtual {v7, v1, p2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v7

    invoke-virtual {v3, v4, v5, v6, v7}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1685425
    invoke-static {v2}, LX/AaB;->i(LX/AaB;)V

    .line 1685426
    :goto_2
    return-object v0

    .line 1685427
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1685428
    :cond_2
    iget-object v2, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->a:LX/AaL;

    iget-object v3, v1, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v3}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->u()Ljava/lang/String;

    move-result-object v3

    .line 1685429
    new-instance v4, LX/AdB;

    invoke-direct {v4}, LX/AdB;-><init>()V

    move-object v4, v4

    .line 1685430
    new-instance v5, LX/4D0;

    invoke-direct {v5}, LX/4D0;-><init>()V

    iget-object v6, v2, LX/AaL;->d:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685431
    iget-object v7, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v6, v7

    .line 1685432
    const-string v7, "actor_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685433
    move-object v5, v5

    .line 1685434
    const-string v6, "video_broadcast_id"

    invoke-virtual {v5, v6, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685435
    move-object v5, v5

    .line 1685436
    const-string v6, "fundraiser_campaign_id"

    invoke-virtual {v5, v6, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685437
    move-object v5, v5

    .line 1685438
    iget-object v6, v4, LX/0gW;->h:Ljava/lang/String;

    move-object v6, v6

    .line 1685439
    const-string v7, "client_mutation_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1685440
    move-object v5, v5

    .line 1685441
    const-string v6, "input"

    invoke-virtual {v4, v6, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1685442
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 1685443
    iget-object v5, v2, LX/AaL;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1685444
    new-instance v5, LX/AaK;

    invoke-direct {v5, v2, v3, v1}, LX/AaK;-><init>(LX/AaL;Ljava/lang/String;LX/AaD;)V

    iget-object v6, v2, LX/AaL;->b:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1685445
    goto :goto_2
.end method

.method public final a(Landroid/view/ViewGroup;LX/AVF;)V
    .locals 1

    .prologue
    .line 1685446
    invoke-super {p0, p1, p2}, LX/AWU;->a(Landroid/view/ViewGroup;LX/AVF;)V

    .line 1685447
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->t:LX/AWb;

    invoke-virtual {v0}, LX/AWb;->g()V

    .line 1685448
    return-void
.end method

.method public final a(Lcom/facebook/facecast/model/FacecastCompositionData;ZZZZLX/1bE;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1685449
    iput-boolean p2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->w:Z

    .line 1685450
    iput-object p6, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    .line 1685451
    iget-object v0, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-object v1, v0

    .line 1685452
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->a()Z

    move-result v0

    if-nez v0, :cond_5

    iget-wide v4, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 1685453
    :goto_0
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->i:LX/AaB;

    iget-object v4, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685454
    iget-object v5, v4, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1685455
    iput-object v4, v3, LX/AaB;->a:Ljava/lang/String;

    .line 1685456
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->i:LX/AaB;

    .line 1685457
    iput-object v0, v3, LX/AaB;->b:Ljava/lang/String;

    .line 1685458
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    invoke-virtual {v3, p3}, LX/AWy;->setFullScreen(Z)V

    .line 1685459
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    .line 1685460
    iget-object v4, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1685461
    invoke-virtual {v3, v4}, LX/AWy;->a(Ljava/lang/String;)V

    .line 1685462
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    invoke-virtual {v3, v1}, LX/AWy;->setComposerTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V

    .line 1685463
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    .line 1685464
    iget-object v4, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v4, v4

    .line 1685465
    invoke-virtual {v3, v4}, LX/AWy;->a(Lcom/facebook/facecast/model/FacecastPrivacyData;)V

    .line 1685466
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    .line 1685467
    iget-object v4, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1685468
    iget-object v5, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->e:Ljava/lang/String;

    move-object v5, v5

    .line 1685469
    iput-object v4, v3, LX/AWy;->H:Ljava/lang/String;

    .line 1685470
    iput-object v5, v3, LX/AWy;->I:Ljava/lang/String;

    .line 1685471
    invoke-static {v3}, LX/AWy;->j(LX/AWy;)V

    .line 1685472
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    iget-wide v4, v1, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const/4 p2, 0x0

    .line 1685473
    iget-object v4, v3, LX/AWy;->n:LX/0ad;

    sget-short v5, LX/1v6;->l:S

    invoke-interface {v4, v5, p2}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 1685474
    if-eqz v4, :cond_0

    if-eqz p5, :cond_0

    .line 1685475
    iget-object v4, v3, LX/AWy;->w:Lcom/facebook/facecast/plugin/boostpost/BoostPostOptionView;

    invoke-virtual {v4, p2}, Lcom/facebook/facecast/plugin/boostpost/BoostPostOptionView;->setVisibility(I)V

    .line 1685476
    iget-object v4, v3, LX/AWy;->w:Lcom/facebook/facecast/plugin/boostpost/BoostPostOptionView;

    new-instance v5, LX/AWt;

    invoke-direct {v5, v3, v1}, LX/AWt;-><init>(LX/AWy;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Lcom/facebook/facecast/plugin/boostpost/BoostPostOptionView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685477
    :cond_0
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    new-instance v3, LX/AYL;

    invoke-direct {v3, p0}, LX/AYL;-><init>(Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;)V

    .line 1685478
    iput-object v3, v1, LX/AYY;->O:LX/AYL;

    .line 1685479
    iget-object v4, v1, LX/AYY;->x:Lcom/facebook/widget/text/BetterTextView;

    new-instance v5, LX/AYS;

    invoke-direct {v5, v1, v3}, LX/AYS;-><init>(LX/AYY;LX/AYL;)V

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685480
    iget-object v4, v1, LX/AYY;->y:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v5, LX/AYT;

    invoke-direct {v5, v1, v3}, LX/AYT;-><init>(LX/AYY;LX/AYL;)V

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685481
    iget-object v4, v1, LX/AYY;->z:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v5, LX/AYU;

    invoke-direct {v5, v1, v3}, LX/AYU;-><init>(LX/AYY;LX/AYL;)V

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685482
    iget-object v4, v1, LX/AYY;->A:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v5, LX/AYV;

    invoke-direct {v5, v1}, LX/AYV;-><init>(LX/AYY;)V

    invoke-virtual {v4, v5}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1685483
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685484
    iget-boolean v3, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v1, v3

    .line 1685485
    if-nez v1, :cond_1

    .line 1685486
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    .line 1685487
    iget-object v1, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v1, v1

    .line 1685488
    if-nez v1, :cond_6

    move-object v1, v2

    :goto_1
    invoke-virtual {v3, v1}, LX/AYY;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1685489
    :cond_1
    iget-object v1, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v1, v1

    .line 1685490
    if-nez v1, :cond_7

    :goto_2
    invoke-static {p0, v2}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->c(Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1685491
    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->w:Z

    if-eqz v1, :cond_2

    .line 1685492
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, LX/AYY;->setVisibility(I)V

    .line 1685493
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {p0, p3, v1}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->a(ZZ)V

    .line 1685494
    invoke-virtual {p0, p4}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->setLandscape(Z)V

    .line 1685495
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    .line 1685496
    iget-boolean v2, v1, LX/AWy;->G:Z

    if-eqz v2, :cond_3

    .line 1685497
    iget-object v2, v1, LX/AWy;->s:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/AbS;

    .line 1685498
    iput-object p0, v2, LX/AbS;->g:LX/AYP;

    .line 1685499
    :cond_3
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    if-eqz v1, :cond_4

    .line 1685500
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    .line 1685501
    iput-object v0, v1, LX/AZK;->I:Ljava/lang/String;

    .line 1685502
    :cond_4
    return-void

    .line 1685503
    :cond_5
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->g:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1685504
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1685505
    goto/16 :goto_0

    .line 1685506
    :cond_6
    iget-object v1, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v1, v1

    .line 1685507
    iget-object v4, v1, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v1, v4

    .line 1685508
    goto :goto_1

    .line 1685509
    :cond_7
    iget-object v1, p1, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v1, v1

    .line 1685510
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v2, v2

    .line 1685511
    goto :goto_2
.end method

.method public final a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;)V
    .locals 2
    .param p1    # Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1685512
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    if-eqz v0, :cond_0

    .line 1685513
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, LX/AYY;->b(Z)V

    .line 1685514
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    .line 1685515
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    invoke-interface {v0}, LX/1bE;->y()V

    .line 1685516
    :cond_1
    return-void

    .line 1685517
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 2

    .prologue
    .line 1685518
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    if-eqz v0, :cond_0

    .line 1685519
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, LX/1bE;->b(Z)V

    .line 1685520
    :cond_0
    invoke-static {p0, p1}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->c(Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1685521
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    invoke-virtual {v0, p1}, LX/AYY;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1685522
    return-void

    .line 1685523
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1685524
    iput-object p1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->A:Ljava/lang/String;

    .line 1685525
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 1685305
    iput-boolean p1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->B:Z

    .line 1685306
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->B:Z

    invoke-virtual {v0, v1}, LX/AYY;->a(Z)V

    .line 1685307
    iget-boolean v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->B:Z

    if-eqz v0, :cond_0

    .line 1685308
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->i:LX/AaB;

    .line 1685309
    iget-object v1, v0, LX/AaB;->d:LX/0if;

    sget-object v2, LX/0ig;->E:LX/0ih;

    invoke-virtual {v1, v2}, LX/0if;->a(LX/0ih;)V

    .line 1685310
    iget-object v1, v0, LX/AaB;->d:LX/0if;

    sget-object v2, LX/0ig;->E:LX/0ih;

    const-string v3, "start_session"

    const/4 v4, 0x0

    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v5

    const-string p0, "broadcaster_id"

    iget-object p1, v0, LX/AaB;->a:Ljava/lang/String;

    invoke-virtual {v5, p0, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    const-string p0, "publish_target_id"

    iget-object p1, v0, LX/AaB;->b:Ljava/lang/String;

    invoke-virtual {v5, p0, p1}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 1685311
    :cond_0
    return-void
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 1685261
    iput-boolean p1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->y:Z

    .line 1685262
    if-eqz p2, :cond_1

    .line 1685263
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->s:LX/AWc;

    if-nez v0, :cond_0

    .line 1685264
    new-instance v0, LX/AWc;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AWc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->s:LX/AWc;

    .line 1685265
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->s:LX/AWc;

    invoke-virtual {p0, v0}, LX/AWU;->b(LX/AWT;)V

    .line 1685266
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->s:LX/AWc;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->t:LX/AWb;

    .line 1685267
    :goto_0
    return-void

    .line 1685268
    :cond_1
    if-eqz p1, :cond_3

    .line 1685269
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->r:LX/AXh;

    if-nez v0, :cond_2

    .line 1685270
    new-instance v0, LX/AXh;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AXh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->r:LX/AXh;

    .line 1685271
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->r:LX/AXh;

    invoke-virtual {p0, v0}, LX/AWU;->b(LX/AWT;)V

    .line 1685272
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->r:LX/AXh;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->t:LX/AWb;

    goto :goto_0

    .line 1685273
    :cond_3
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->q:LX/AWb;

    if-nez v0, :cond_4

    .line 1685274
    new-instance v0, LX/AWb;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/AWb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->q:LX/AWb;

    .line 1685275
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->q:LX/AWb;

    invoke-virtual {p0, v0}, LX/AWU;->b(LX/AWT;)V

    .line 1685276
    :cond_4
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->q:LX/AWb;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->t:LX/AWb;

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1685244
    iget-boolean v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->x:Z

    if-eqz v2, :cond_4

    iget-boolean v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->w:Z

    if-eqz v2, :cond_4

    .line 1685245
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    invoke-virtual {v2, v1}, LX/AWy;->setVisibility(I)V

    .line 1685246
    iget-boolean v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->y:Z

    if-nez v2, :cond_0

    .line 1685247
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    invoke-virtual {v2}, LX/AWy;->g()V

    .line 1685248
    :cond_0
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    invoke-virtual {v2, v1}, LX/AYY;->setVisibility(I)V

    .line 1685249
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    .line 1685250
    iget-object v3, v2, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v3, :cond_1

    .line 1685251
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->setVisibility(I)V

    .line 1685252
    :cond_1
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n:LX/AWX;

    if-eqz v2, :cond_2

    .line 1685253
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->n:LX/AWX;

    invoke-virtual {v2, v1}, LX/AWX;->setVisibility(I)V

    .line 1685254
    :cond_2
    iget-boolean v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->z:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    if-eqz v2, :cond_3

    .line 1685255
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    invoke-virtual {v2, v1}, LX/AYo;->setVisibility(I)V

    .line 1685256
    :cond_3
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->o()V

    .line 1685257
    :goto_0
    return v0

    .line 1685258
    :cond_4
    iget-object v2, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    invoke-virtual {v2}, LX/AZK;->g()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1685259
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->l:LX/AZK;

    invoke-virtual {v1}, LX/AZK;->c()V

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1685260
    goto :goto_0
.end method

.method public final b(Lcom/facebook/facecast/model/FacecastCompositionData;)Lcom/facebook/facecast/model/FacecastCompositionData;
    .locals 2

    .prologue
    .line 1685233
    iget-boolean v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->w:Z

    if-eqz v0, :cond_0

    .line 1685234
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1685235
    new-instance v0, LX/AWP;

    invoke-direct {v0, p1}, LX/AWP;-><init>(Lcom/facebook/facecast/model/FacecastCompositionData;)V

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    invoke-virtual {v1}, LX/AWy;->getDescription()Ljava/lang/String;

    move-result-object v1

    .line 1685236
    iput-object v1, v0, LX/AWP;->a:Ljava/lang/String;

    .line 1685237
    move-object v0, v0

    .line 1685238
    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    .line 1685239
    iget-object p0, v1, LX/AWy;->D:Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-object v1, p0

    .line 1685240
    iput-object v1, v0, LX/AWP;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1685241
    move-object v0, v0

    .line 1685242
    invoke-virtual {v0}, LX/AWP;->a()Lcom/facebook/facecast/model/FacecastCompositionData;

    move-result-object p1

    .line 1685243
    :cond_0
    return-object p1
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1685230
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    if-eqz v0, :cond_0

    .line 1685231
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    invoke-interface {v0}, LX/1bE;->u()V

    .line 1685232
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 1

    .prologue
    .line 1685227
    invoke-static {p0, p1}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->c(Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1685228
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    invoke-virtual {v0, p1}, LX/AYY;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1685229
    return-void
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 1685212
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j()V

    .line 1685213
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    if-eqz v0, :cond_0

    .line 1685214
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    invoke-interface {v0, p1}, LX/1bE;->c(Z)V

    .line 1685215
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    .line 1685216
    iput-boolean p1, v0, LX/AYY;->M:Z

    .line 1685217
    if-eqz p1, :cond_2

    .line 1685218
    iget-object v1, v0, LX/AYY;->w:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a03b2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1685219
    iget-object v1, v0, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00aa

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1685220
    :goto_0
    invoke-static {v0}, LX/AYY;->j(LX/AYY;)V

    .line 1685221
    if-eqz p1, :cond_1

    .line 1685222
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->o:LX/AWy;

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->a(LX/AWy;LX/AWy;)V

    .line 1685223
    :goto_1
    return-void

    .line 1685224
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->o:LX/AWy;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->j:LX/AWy;

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->a(LX/AWy;LX/AWy;)V

    goto :goto_1

    .line 1685225
    :cond_2
    iget-object v1, v0, LX/AYY;->w:Landroid/widget/FrameLayout;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget v3, v0, LX/AYY;->K:I

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1685226
    iget-object v1, v0, LX/AYY;->B:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0}, LX/AYY;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 1685209
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    if-eqz v0, :cond_0

    .line 1685210
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->C:LX/1bE;

    invoke-interface {v0, p1}, LX/1bE;->d(Z)V

    .line 1685211
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1685207
    invoke-direct {p0}, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->o()V

    .line 1685208
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1685200
    iget-object v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    iget-boolean v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->w:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, LX/AWy;->setVisibility(I)V

    .line 1685201
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->k:LX/AYY;

    invoke-virtual {v0, v1}, LX/AYY;->setVisibility(I)V

    .line 1685202
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    if-eqz v0, :cond_0

    .line 1685203
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    iget-boolean v3, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->z:Z

    if-eqz v3, :cond_2

    :goto_1
    invoke-virtual {v0, v2}, LX/AYo;->setVisibility(I)V

    .line 1685204
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1685205
    goto :goto_0

    :cond_2
    move v2, v1

    .line 1685206
    goto :goto_1
.end method

.method public getFundraiserModel()Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1685176
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->m:Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;

    .line 1685177
    iget-object p0, v0, Lcom/facebook/facecast/plugin/donation/FacecastDonationBannerPlugin;->j:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    move-object v0, p0

    .line 1685178
    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 1685198
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    invoke-virtual {v0}, LX/AWy;->h()V

    .line 1685199
    return-void
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1685197
    iget-boolean v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->B:Z

    return v0
.end method

.method public setLandscape(Z)V
    .locals 3

    .prologue
    .line 1685179
    iput-boolean p1, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->z:Z

    .line 1685180
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->p:LX/AWy;

    .line 1685181
    invoke-virtual {v0}, LX/AWy;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_1

    const v1, 0x7f0b0615

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 1685182
    iget-object v2, v0, LX/AWy;->u:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1685183
    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1685184
    iget-object v1, v0, LX/AWy;->u:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1685185
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    if-eqz v0, :cond_0

    .line 1685186
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastStartingAggregatePlugin;->u:LX/AYo;

    .line 1685187
    if-eqz p1, :cond_2

    const/16 v1, 0x8

    :goto_1
    invoke-virtual {v0, v1}, LX/AYo;->setVisibility(I)V

    .line 1685188
    if-eqz p1, :cond_0

    .line 1685189
    iget-object v1, v0, LX/AYo;->b:LX/AVc;

    .line 1685190
    iget-object v0, v1, LX/AVc;->g:LX/0hs;

    if-eqz v0, :cond_0

    iget-object v0, v1, LX/AVc;->g:LX/0hs;

    .line 1685191
    iget-boolean p1, v0, LX/0ht;->r:Z

    move v0, p1

    .line 1685192
    if-eqz v0, :cond_0

    .line 1685193
    iget-object v0, v1, LX/AVc;->g:LX/0hs;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1685194
    :cond_0
    return-void

    .line 1685195
    :cond_1
    const v1, 0x7f0b0614

    goto :goto_0

    .line 1685196
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method
