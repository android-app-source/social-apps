.class public Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;
.super LX/AWm;
.source ""


# instance fields
.field public a:Lcom/facebook/facecast/protocol/FacecastNetworker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Ljava/lang/Runnable;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1683356
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1683357
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683358
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683359
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1683360
    invoke-direct {p0, p1, p2, p3}, LX/AWm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1683361
    const-class v0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    invoke-static {v0, p0}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1683362
    new-instance v0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin$1;-><init>(Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->b:Ljava/lang/Runnable;

    .line 1683363
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;

    invoke-static {p0}, Lcom/facebook/facecast/protocol/FacecastNetworker;->b(LX/0QB;)Lcom/facebook/facecast/protocol/FacecastNetworker;

    move-result-object p0

    check-cast p0, Lcom/facebook/facecast/protocol/FacecastNetworker;

    iput-object p0, p1, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->a:Lcom/facebook/facecast/protocol/FacecastNetworker;

    return-void
.end method

.method private a(Z)V
    .locals 8

    .prologue
    .line 1683364
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1683365
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->a:Lcom/facebook/facecast/protocol/FacecastNetworker;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->c:Ljava/lang/String;

    .line 1683366
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1683367
    const-string v2, "answer_copyright_violation_key"

    new-instance v3, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;

    invoke-direct {v3, v1, p1}, Lcom/facebook/facecast/protocol/AnswerCopyrightViolationMethod$Params;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v4, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1683368
    iget-object v2, v0, Lcom/facebook/facecast/protocol/FacecastNetworker;->a:LX/0aG;

    const-string v3, "answer_copyright_violation_type"

    sget-object v5, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    const-class v6, Lcom/facebook/facecast/protocol/FacecastNetworker;

    invoke-static {v6}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    const v7, 0x7083f08d

    invoke-static/range {v2 .. v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v2

    invoke-interface {v2}, LX/1MF;->start()LX/1ML;

    .line 1683369
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 1683370
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->a(Z)V

    .line 1683371
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    sget-object v1, LX/AVE;->SHOW_END_SCREEN:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 1683372
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1683373
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1683374
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 1683375
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1683376
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->a(Z)V

    .line 1683377
    iget-object v0, p0, LX/AWT;->e:LX/AVF;

    sget-object v1, LX/AVE;->RECORDING:LX/AVE;

    invoke-virtual {v0, v1}, LX/AVF;->b(LX/AVE;)V

    .line 1683378
    return-void
.end method

.method public final iF_()V
    .locals 4

    .prologue
    .line 1683379
    invoke-super {p0}, LX/AWm;->iF_()V

    .line 1683380
    iget-object v0, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1683381
    iget-object v0, p0, LX/AWT;->d:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->b:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/ViewGroup;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1683382
    return-void
.end method

.method public setBroadcastId(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1683383
    iput-object p1, p0, Lcom/facebook/facecast/plugin/FacecastCopyrightViolationPlugin;->c:Ljava/lang/String;

    .line 1683384
    return-void
.end method

.method public setViolationText(LX/AV3;)V
    .locals 1

    .prologue
    .line 1683385
    iget-object v0, p1, LX/AV3;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/AWm;->setTitle(Ljava/lang/CharSequence;)V

    .line 1683386
    iget-object v0, p1, LX/AV3;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/AWm;->setDescription(Ljava/lang/CharSequence;)V

    .line 1683387
    iget-object v0, p1, LX/AV3;->c:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/AWm;->setActionFinishText(Ljava/lang/CharSequence;)V

    .line 1683388
    iget-object v0, p1, LX/AV3;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/AWm;->setActionResumeText(Ljava/lang/CharSequence;)V

    .line 1683389
    return-void
.end method
