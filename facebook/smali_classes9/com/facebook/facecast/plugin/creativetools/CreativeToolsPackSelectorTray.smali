.class public Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:F

.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Path;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AZZ;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/Aa3;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1687863
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1687864
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1687880
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687881
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1687865
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687866
    invoke-virtual {p0, v2}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->setOrientation(I)V

    .line 1687867
    invoke-virtual {p0, v2}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->setWillNotDraw(Z)V

    .line 1687868
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->a:F

    .line 1687869
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->CreativeTools:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1687870
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1687871
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1687872
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->b:Landroid/graphics/Paint;

    .line 1687873
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->b:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1687874
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->b:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1687875
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1687876
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->c:Landroid/graphics/Path;

    .line 1687877
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->d:Ljava/util/List;

    .line 1687878
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->e:Ljava/util/List;

    .line 1687879
    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;Landroid/view/View;LX/AZZ;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1687849
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 1687850
    :goto_0
    return-void

    .line 1687851
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 1687852
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 1687853
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setSelected(Z)V

    .line 1687854
    :cond_1
    iput-object p1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    .line 1687855
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 1687856
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 1687857
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->g:LX/Aa3;

    if-eqz v0, :cond_2

    .line 1687858
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->g:LX/Aa3;

    invoke-interface {v0, p2}, LX/Aa3;->a(LX/AZZ;)V

    .line 1687859
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->invalidate()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1687860
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->e:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->d:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1687861
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AZZ;

    invoke-static {p0, v0, v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->a$redex0(Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;Landroid/view/View;LX/AZZ;)V

    .line 1687862
    :cond_0
    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 1687836
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1687837
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1687838
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->f:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 1687839
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 1687840
    iget-boolean v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->h:Z

    if-eqz v2, :cond_1

    .line 1687841
    iget v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->a:F

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1687842
    :cond_0
    :goto_0
    return-void

    .line 1687843
    :cond_1
    iget-object v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->c:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    .line 1687844
    iget-object v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->c:Landroid/graphics/Path;

    iget v3, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->a:F

    sub-float v3, v0, v3

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 1687845
    iget-object v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->c:Landroid/graphics/Path;

    iget v3, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->a:F

    add-float/2addr v3, v0

    invoke-virtual {v2, v3, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1687846
    iget-object v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->c:Landroid/graphics/Path;

    iget v3, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->a:F

    sub-float/2addr v1, v3

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1687847
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->c:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1687848
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->c:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setCreativeToolsPacks(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/AZZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1687824
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1687825
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1687826
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->removeAllViews()V

    .line 1687827
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1687828
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZZ;

    .line 1687829
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0305c0

    invoke-static {v1, v3, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1687830
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1687831
    invoke-virtual {v0}, LX/AZZ;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1687832
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0}, LX/AZZ;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1687833
    new-instance v3, LX/Aa2;

    invoke-direct {v3, p0, v0}, LX/Aa2;-><init>(Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;LX/AZZ;)V

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1687834
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1687835
    :cond_0
    return-void
.end method

.method public setFullScreen(Z)V
    .locals 0

    .prologue
    .line 1687822
    iput-boolean p1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->h:Z

    .line 1687823
    return-void
.end method

.method public setListener(LX/Aa3;)V
    .locals 0

    .prologue
    .line 1687820
    iput-object p1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsPackSelectorTray;->g:LX/Aa3;

    .line 1687821
    return-void
.end method
