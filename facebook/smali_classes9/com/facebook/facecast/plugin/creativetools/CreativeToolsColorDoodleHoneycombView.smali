.class public Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/AVT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Landroid/graphics/Rect;

.field public g:LX/AZe;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/AZd;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1687259
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1687260
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1687257
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687258
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1687249
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687250
    const-class v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    invoke-static {v0, p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1687251
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    .line 1687252
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05f0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->c:I

    .line 1687253
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05f1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->d:I

    .line 1687254
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->e:I

    .line 1687255
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    .line 1687256
    return-void
.end method

.method private a(Landroid/graphics/Rect;II)V
    .locals 8

    .prologue
    .line 1687233
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    .line 1687234
    mul-int v0, p3, v3

    .line 1687235
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v0, v1, v0

    .line 1687236
    div-int/2addr v0, p3

    .line 1687237
    div-int/lit8 v4, v0, 0x2

    .line 1687238
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    div-int/lit8 v1, v3, 0x2

    sub-int v5, v0, v1

    .line 1687239
    add-int v6, v5, v3

    .line 1687240
    iget v0, p1, Landroid/graphics/Rect;->left:I

    move v1, p2

    .line 1687241
    :goto_0
    add-int v2, p2, p3

    if-ge v1, v2, :cond_0

    .line 1687242
    add-int v2, v0, v4

    .line 1687243
    add-int v7, v2, v3

    .line 1687244
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1687245
    invoke-virtual {v0, v2, v5, v7, v6}, Landroid/view/View;->layout(IIII)V

    .line 1687246
    add-int v2, v7, v4

    .line 1687247
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 1687248
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;

    invoke-static {v0}, LX/AVT;->a(LX/0QB;)LX/AVT;

    move-result-object v0

    check-cast v0, LX/AVT;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->a:LX/AVT;

    return-void
.end method


# virtual methods
.method public getCurrentlySelectedColor()I
    .locals 1

    .prologue
    .line 1687229
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    if-eqz v0, :cond_0

    .line 1687230
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    .line 1687231
    iget p0, v0, LX/AZd;->e:I

    move v0, p0

    .line 1687232
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onLayout(ZIIII)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1687209
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1687210
    :goto_0
    return-void

    .line 1687211
    :cond_0
    sub-int v0, p4, p2

    .line 1687212
    sub-int v1, p5, p3

    .line 1687213
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingRight()I

    move-result v2

    sub-int v2, v0, v2

    .line 1687214
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingTop()I

    move-result v0

    sub-int v0, v1, v0

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingBottom()I

    move-result v1

    sub-int v1, v0, v1

    .line 1687215
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/lit8 v3, v0, 0x2

    .line 1687216
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sub-int v4, v0, v3

    .line 1687217
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 1687218
    if-lez v3, :cond_1

    .line 1687219
    iget-object v5, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingLeft()I

    move-result v6

    div-int/lit8 v7, v0, 0x2

    add-int/2addr v6, v7

    iput v6, v5, Landroid/graphics/Rect;->left:I

    .line 1687220
    iget-object v5, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    iget-object v6, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v2

    sub-int v0, v6, v0

    iput v0, v5, Landroid/graphics/Rect;->right:I

    .line 1687221
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingTop()I

    move-result v5

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 1687222
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    div-int/lit8 v6, v1, 0x2

    add-int/2addr v5, v6

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    .line 1687223
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v8, v3}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->a(Landroid/graphics/Rect;II)V

    .line 1687224
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingLeft()I

    move-result v5

    iput v5, v0, Landroid/graphics/Rect;->left:I

    .line 1687225
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v5

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 1687226
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingBottom()I

    move-result v2

    sub-int v2, p5, v2

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 1687227
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    div-int/lit8 v1, v1, 0x2

    sub-int v1, v2, v1

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1687228
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->f:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v3, v4}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->a(Landroid/graphics/Rect;II)V

    goto/16 :goto_0
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1687193
    invoke-static {v1, p1}, Landroid/view/View;->getDefaultSize(II)I

    move-result v0

    .line 1687194
    invoke-static {v1, p2}, Landroid/view/View;->resolveSize(II)I

    move-result v1

    .line 1687195
    invoke-virtual {p0, v0, v1}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->setMeasuredDimension(II)V

    .line 1687196
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1687197
    :cond_0
    return-void

    .line 1687198
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v0, v2

    .line 1687199
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    .line 1687200
    iget-object v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    .line 1687201
    iget-object v3, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    sub-int v2, v3, v2

    .line 1687202
    iget v3, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->e:I

    mul-int/lit8 v3, v3, 0x2

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 1687203
    div-int/2addr v0, v2

    iget v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->d:I

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v0, v2

    .line 1687204
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1687205
    iget v1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->c:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1687206
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1687207
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1687208
    invoke-virtual {v0, v1, v1}, Landroid/view/View;->measure(II)V

    goto :goto_0
.end method

.method public setColorDoodlePack(LX/AZe;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1687177
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->g:LX/AZe;

    if-ne v0, p1, :cond_1

    .line 1687178
    :cond_0
    return-void

    .line 1687179
    :cond_1
    iput-object p1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->g:LX/AZe;

    .line 1687180
    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->removeAllViews()V

    .line 1687181
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1687182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    .line 1687183
    invoke-virtual {p1}, LX/AZZ;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AZb;

    .line 1687184
    new-instance v2, LX/AZd;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/AZd;-><init>(Landroid/content/Context;)V

    .line 1687185
    iget v0, v0, LX/AZb;->a:I

    .line 1687186
    iput v0, v2, LX/AZd;->e:I

    .line 1687187
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    if-nez v0, :cond_2

    .line 1687188
    iput-object v2, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    .line 1687189
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->h:LX/AZd;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/AZd;->setSelected(Z)V

    .line 1687190
    :cond_2
    new-instance v0, LX/AZc;

    invoke-direct {v0, p0, v2}, LX/AZc;-><init>(Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;LX/AZd;)V

    invoke-virtual {v2, v0}, LX/AZd;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1687191
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1687192
    invoke-virtual {p0, v2, v4, v4}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsColorDoodleHoneycombView;->addView(Landroid/view/View;II)V

    goto :goto_0
.end method
