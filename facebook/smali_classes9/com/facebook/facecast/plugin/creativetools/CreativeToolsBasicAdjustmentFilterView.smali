.class public Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;
.super LX/AZX;
.source ""


# instance fields
.field public final a:LX/AZY;

.field public b:LX/AVP;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:LX/AVO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1687104
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1687105
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1687106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1687108
    invoke-direct {p0, p1, p2, p3}, LX/AZX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687109
    const v0, 0x7f0305ba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1687110
    new-instance v0, LX/AZY;

    invoke-direct {v0, p0}, LX/AZY;-><init>(Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->a:LX/AZY;

    .line 1687111
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->a:LX/AZY;

    iget-object v0, v0, LX/AZY;->m:Landroid/view/SurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/SurfaceView;->setZOrderMediaOverlay(Z)V

    .line 1687112
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->a:LX/AZY;

    iget-object v0, v0, LX/AZY;->n:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, LX/AZX;->setTileView(Landroid/view/View;)V

    .line 1687113
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 1687114
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->c:LX/AVO;

    if-nez v0, :cond_0

    .line 1687115
    :goto_0
    return-void

    .line 1687116
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->b:LX/AVP;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->c:LX/AVO;

    .line 1687117
    iget-object v2, v0, LX/AVP;->w:LX/AWK;

    if-eqz v2, :cond_1

    .line 1687118
    iget-object v2, v0, LX/AVP;->b:LX/AVN;

    iget-object v3, v0, LX/AVP;->b:LX/AVN;

    const/16 v4, 0xe

    invoke-virtual {v3, v4, v1}, LX/AVN;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/AVN;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    .line 1687119
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->c:LX/AVO;

    goto :goto_0
.end method

.method public final a(LX/AZO;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1687120
    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Binding null filter"

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1687121
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->c:LX/AVO;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "Already bound"

    invoke-static {v0, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1687122
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->b:LX/AVP;

    if-eqz v0, :cond_2

    :goto_2
    const-string v0, "Must have a preview renderer"

    invoke-static {v1, v0}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 1687123
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->a:LX/AZY;

    iget-object v0, v0, LX/AZY;->n:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p1, LX/AZO;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1687124
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->b:LX/AVP;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->a:LX/AZY;

    iget-object v1, v1, LX/AZY;->m:Landroid/view/SurfaceView;

    iget-object v2, p1, LX/AZO;->d:LX/7S6;

    invoke-virtual {v0, v1, v2}, LX/AVP;->a(Landroid/view/SurfaceView;LX/61B;)LX/AVO;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->c:LX/AVO;

    .line 1687125
    return-void

    :cond_0
    move v0, v2

    .line 1687126
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1687127
    goto :goto_1

    :cond_2
    move v1, v2

    .line 1687128
    goto :goto_2
.end method

.method public setPreviewRenderer(LX/AVP;)V
    .locals 0

    .prologue
    .line 1687129
    iput-object p1, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->b:LX/AVP;

    .line 1687130
    return-void
.end method

.method public setVisibility(Z)V
    .locals 1

    .prologue
    .line 1687131
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->c:LX/AVO;

    if-eqz v0, :cond_0

    .line 1687132
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsBasicAdjustmentFilterView;->c:LX/AVO;

    .line 1687133
    iput-boolean p1, v0, LX/AVO;->g:Z

    .line 1687134
    :cond_0
    return-void
.end method
