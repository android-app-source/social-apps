.class public Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;
.super LX/AZX;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final a:LX/AZw;

.field public c:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/AZl;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/AZx;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1687703
    const-class v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1687701
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1687702
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1687704
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687705
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1687687
    invoke-direct {p0, p1, p2, p3}, LX/AZX;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1687688
    const-class v0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;

    invoke-static {v0, p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1687689
    const v0, 0x7f0305bf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1687690
    new-instance v0, LX/AZw;

    invoke-direct {v0, p0}, LX/AZw;-><init>(Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    .line 1687691
    sget-object v0, LX/03r;->CreativeTools:[I

    invoke-virtual {p1, v0}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1687692
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 1687693
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1687694
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1687695
    const v1, 0x7f0206cf

    invoke-static {p1, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1687696
    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    .line 1687697
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v0, v0, LX/AZw;->m:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1687698
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v0, v0, LX/AZw;->m:Landroid/view/View;

    invoke-virtual {p0, v0}, LX/AZX;->setTileView(Landroid/view/View;)V

    .line 1687699
    iget-object v0, p0, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->a:LX/AZw;

    iget-object v0, v0, LX/AZw;->p:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/0wJ;

    invoke-virtual {p0}, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0wJ;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1687700
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object p0

    check-cast p0, LX/1Ad;

    iput-object p0, p1, Lcom/facebook/facecast/plugin/creativetools/CreativeToolsMaskView;->c:LX/1Ad;

    return-void
.end method
