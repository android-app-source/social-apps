.class public Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""

# interfaces
.implements LX/AaV;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/ui/dialogs/FbDialogFragment;",
        "LX/AaV",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;",
        ">;>;"
    }
.end annotation


# instance fields
.field public m:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:Landroid/app/Dialog;

.field private p:Landroid/view/ViewGroup;

.field public q:Lcom/facebook/widget/SwitchCompat;

.field public r:Lcom/facebook/resources/ui/FbTextView;

.field public s:Lcom/facebook/resources/ui/FbTextView;

.field public t:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

.field public u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

.field public v:LX/AYX;

.field public w:Z

.field private x:Z

.field public y:LX/AaY;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1688652
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1688653
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1688634
    invoke-static {p1}, LX/63Z;->a(Landroid/view/View;)Z

    .line 1688635
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1688636
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080d16

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1688637
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1688638
    new-instance v1, LX/AaT;

    invoke-direct {v1, p0}, LX/AaT;-><init>(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1688639
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080cef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1688640
    iput-object v2, v1, LX/108;->g:Ljava/lang/String;

    .line 1688641
    move-object v1, v1

    .line 1688642
    iget-boolean v2, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->x:Z

    .line 1688643
    iput-boolean v2, v1, LX/108;->d:Z

    .line 1688644
    move-object v1, v1

    .line 1688645
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00f9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1688646
    iput v2, v1, LX/108;->o:I

    .line 1688647
    move-object v1, v1

    .line 1688648
    invoke-virtual {v1}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v1

    .line 1688649
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1688650
    new-instance v1, LX/AaU;

    invoke-direct {v1, p0}, LX/AaU;-><init>(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1688651
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-static {p0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p0

    check-cast p0, Lcom/facebook/content/SecureContextHelper;

    iput-object v1, p1, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    iput-object p0, p1, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->n:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static d(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1688633
    if-eqz p1, :cond_0

    const v0, 0x7f080d18

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f080d17

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V
    .locals 3

    .prologue
    .line 1688607
    iget-boolean v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    if-nez v0, :cond_1

    .line 1688608
    const/4 v2, 0x0

    .line 1688609
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1688610
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->t:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->setVisibility(I)V

    .line 1688611
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->v:LX/AYX;

    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    invoke-interface {v0, v1, v2}, LX/AYX;->a(ZZ)V

    .line 1688612
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    if-eqz v0, :cond_0

    .line 1688613
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v0

    .line 1688614
    invoke-virtual {v0}, LX/0k3;->p()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1688615
    :cond_0
    :goto_0
    return-void

    .line 1688616
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1688617
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->t:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->setVisibility(I)V

    .line 1688618
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    const-string v1, "payment_providers_view_controller_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    .line 1688619
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    if-nez v0, :cond_2

    .line 1688620
    invoke-static {}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->newBuilder()LX/BF5;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->m:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1688621
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1688622
    iput-object v1, v0, LX/BF5;->c:Ljava/lang/String;

    .line 1688623
    move-object v0, v0

    .line 1688624
    sget-object v1, LX/6xg;->NMOR_TIP_JAR:LX/6xg;

    .line 1688625
    iput-object v1, v0, LX/BF5;->b:LX/6xg;

    .line 1688626
    move-object v0, v0

    .line 1688627
    invoke-virtual {v0}, LX/BF5;->a()Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    move-result-object v0

    .line 1688628
    invoke-static {v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;)Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    .line 1688629
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-virtual {v0, p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a(LX/AaV;)V

    .line 1688630
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mHost:LX/0k3;

    move-object v0, v0

    .line 1688631
    invoke-virtual {v0}, LX/0k3;->p()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    const-string v2, "payment_providers_view_controller_tag"

    invoke-virtual {v0, v1, v2}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1688632
    :cond_2
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->t:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    iget-object v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-virtual {v0, v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->setComponentController(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1688605
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->eK_()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->o:Landroid/app/Dialog;

    .line 1688606
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->o:Landroid/app/Dialog;

    return-object v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 1688604
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 1688552
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->v:LX/AYX;

    if-eqz v0, :cond_0

    .line 1688553
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    if-nez v0, :cond_1

    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 1688554
    :goto_0
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->x:Z

    .line 1688555
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->v:LX/AYX;

    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    iget-boolean v2, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->x:Z

    invoke-interface {v0, v1, v2}, LX/AYX;->a(ZZ)V

    .line 1688556
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1688557
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1688558
    invoke-direct {p0, v0}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->a(Landroid/view/View;)V

    .line 1688559
    :cond_0
    return-void

    .line 1688560
    :cond_1
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->u:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    .line 1688561
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    if-nez v1, :cond_3

    .line 1688562
    sget-object v1, LX/03R;->UNSET:LX/03R;

    .line 1688563
    :goto_2
    move-object v0, v1

    .line 1688564
    goto :goto_0

    .line 1688565
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    .line 1688566
    iget-object v0, v1, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->b:LX/0Px;

    move-object v1, v0

    .line 1688567
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_3
    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 1688603
    return-void
.end method

.method public final iK_()V
    .locals 3

    .prologue
    .line 1688600
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->v:LX/AYX;

    if-eqz v0, :cond_0

    .line 1688601
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->v:LX/AYX;

    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/AYX;->a(ZZ)V

    .line 1688602
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x666e1f72

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1688596
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1688597
    const-class v1, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;

    invoke-static {v1, p0}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->a(Ljava/lang/Class;LX/02k;)V

    .line 1688598
    const v1, 0x7f0e05cc

    invoke-virtual {p0, v3, v1}, Landroid/support/v4/app/DialogFragment;->a(II)V

    .line 1688599
    const/16 v1, 0x2b

    const v2, 0x5d7c976f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0xa22d1e4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1688595
    const v1, 0x7f0305ed

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x38961a81

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x56a02572

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1688590
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onStart()V

    .line 1688591
    iget-object v1, p0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v1, v1

    .line 1688592
    if-nez v1, :cond_0

    .line 1688593
    const/16 v1, 0x2b

    const v2, 0x27661ccf

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1688594
    :goto_0
    return-void

    :cond_0
    const v1, -0x68cd8d54

    invoke-static {v1, v0}, LX/02F;->f(II)V

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1688568
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1688569
    const v0, 0x7f0d1051

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->p:Landroid/view/ViewGroup;

    .line 1688570
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1688571
    const v0, 0x7f0d1053

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/SwitchCompat;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->q:Lcom/facebook/widget/SwitchCompat;

    .line 1688572
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->q:Lcom/facebook/widget/SwitchCompat;

    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 1688573
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->q:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/SwitchCompat;->setSaveEnabled(Z)V

    .line 1688574
    const v0, 0x7f0d1054

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    .line 1688575
    const v0, 0x7f0d1055

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->t:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    .line 1688576
    const v0, 0x7f0d1052

    invoke-virtual {p0, v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1688577
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->s:Lcom/facebook/resources/ui/FbTextView;

    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    invoke-static {p0, v1}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->d(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688578
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->q:Lcom/facebook/widget/SwitchCompat;

    iget-boolean v1, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->w:Z

    invoke-static {p0, v1}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->d(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1688579
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->q:Lcom/facebook/widget/SwitchCompat;

    new-instance v1, LX/AaR;

    invoke-direct {v1, p0}, LX/AaR;-><init>(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/SwitchCompat;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 1688580
    new-instance v0, LX/AaS;

    invoke-direct {v0, p0}, LX/AaS;-><init>(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V

    .line 1688581
    new-instance v1, LX/47x;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 1688582
    const v2, 0x7f080d1a

    invoke-virtual {v1, v2}, LX/47x;->a(I)LX/47x;

    .line 1688583
    const-string v2, "[[terms_link]]"

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const p2, 0x7f080d1b

    invoke-virtual {v3, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/16 p2, 0x21

    invoke-virtual {v1, v2, v3, v0, p2}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 1688584
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1688585
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setHighlightColor(I)V

    .line 1688586
    iget-object v0, p0, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->r:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1688587
    invoke-direct {p0, p1}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->a(Landroid/view/View;)V

    .line 1688588
    invoke-static {p0}, Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;->m(Lcom/facebook/facecast/plugin/tipjar/FacecastTipJarSettingDialog;)V

    .line 1688589
    return-void
.end method
