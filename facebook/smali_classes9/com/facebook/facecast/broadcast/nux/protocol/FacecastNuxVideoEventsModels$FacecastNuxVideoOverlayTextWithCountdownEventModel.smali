.class public final Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/AW3;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x63aaae27
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681997
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681996
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1681994
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1681995
    return-void
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1681991
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1681992
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1681993
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1681989
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1681990
    iget v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->k:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1681975
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1681976
    invoke-direct {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->l()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1681977
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1681978
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1681979
    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1681980
    const/4 v0, 0x1

    iget v2, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->f:I

    invoke-virtual {p1, v0, v2, v3}, LX/186;->a(III)V

    .line 1681981
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1681982
    const/4 v0, 0x3

    iget v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->h:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1681983
    const/4 v0, 0x4

    iget v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->i:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1681984
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->j:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1681985
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->k:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1681986
    const/4 v0, 0x7

    iget v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->l:I

    invoke-virtual {p1, v0, v1, v3}, LX/186;->a(III)V

    .line 1681987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681988
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1681955
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1681956
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681957
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1681967
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1681968
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->f:I

    .line 1681969
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->h:I

    .line 1681970
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->i:I

    .line 1681971
    const/4 v0, 0x5

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->j:I

    .line 1681972
    const/4 v0, 0x6

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->k:I

    .line 1681973
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->l:I

    .line 1681974
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1681964
    new-instance v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    invoke-direct {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;-><init>()V

    .line 1681965
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1681966
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1681963
    const v0, 0x441303d3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1681962
    const v0, -0x4715735a

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1681960
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1681961
    iget v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->f:I

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1681958
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->g:Ljava/lang/String;

    .line 1681959
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;->g:Ljava/lang/String;

    return-object v0
.end method
