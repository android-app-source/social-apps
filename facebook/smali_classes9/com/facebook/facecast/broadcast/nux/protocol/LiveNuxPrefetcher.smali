.class public Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/2Jw;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:LX/AWJ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AaZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/text/SimpleDateFormat;

.field public f:LX/1ly;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1682603
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1JD;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1682599
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682600
    sget-object v0, LX/1Li;->MISC:LX/1Li;

    const-class v1, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;

    invoke-static {v1}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/1JD;->a(LX/1Li;Lcom/facebook/common/callercontext/CallerContext;)LX/1ly;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->f:LX/1ly;

    .line 1682601
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "E, dd MMM yyyy hh:mm:ss"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->e:Ljava/text/SimpleDateFormat;

    .line 1682602
    return-void
.end method

.method public static f()Ljava/util/GregorianCalendar;
    .locals 2

    .prologue
    .line 1682596
    new-instance v0, Ljava/util/GregorianCalendar;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1682597
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1682598
    return-object v0
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 7

    .prologue
    .line 1682585
    new-instance v2, Ljava/util/GregorianCalendar;

    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    .line 1682586
    new-instance v3, Ljava/util/Date;

    const-wide/16 v4, 0x0

    invoke-direct {v3, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V

    .line 1682587
    iget-object v3, p0, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->c:LX/AaZ;

    .line 1682588
    iget-object v4, v3, LX/AaZ;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/AaZ;->b:LX/0Tn;

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 1682589
    if-eqz v3, :cond_0

    .line 1682590
    :try_start_0
    iget-object v4, p0, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->e:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/GregorianCalendar;->setTime(Ljava/util/Date;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1682591
    :cond_0
    :goto_0
    move-object v0, v2

    .line 1682592
    invoke-static {}, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->f()Ljava/util/GregorianCalendar;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v0

    .line 1682593
    if-eqz v0, :cond_1

    .line 1682594
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;->d:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher$1;

    invoke-direct {v2, p0}, Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher$1;-><init>(Lcom/facebook/facecast/broadcast/nux/protocol/LiveNuxPrefetcher;)V

    const v3, -0x443d828e

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1682595
    :cond_1
    return v0

    :catch_0
    goto :goto_0
.end method
