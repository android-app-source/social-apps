.class public final Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4f438602
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681615
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681616
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1681613
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1681614
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1681605
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1681606
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1681607
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->k()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1681608
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1681609
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1681610
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1681611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681612
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1681597
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1681598
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->k()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1681599
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->k()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    .line 1681600
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->k()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1681601
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;

    .line 1681602
    iput-object v0, v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->f:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    .line 1681603
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681604
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1681617
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1681594
    new-instance v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;

    invoke-direct {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;-><init>()V

    .line 1681595
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1681596
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1681593
    const v0, 0x6560bffa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1681592
    const v0, 0x4984e12

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1681588
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->e:Ljava/lang/String;

    .line 1681589
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1681590
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->f:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->f:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    .line 1681591
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel;->f:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel$CommenterPhotoModel$ImageModel;

    return-object v0
.end method
