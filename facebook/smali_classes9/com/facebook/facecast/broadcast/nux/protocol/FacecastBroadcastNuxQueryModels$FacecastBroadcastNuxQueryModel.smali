.class public final Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x75bb14c4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideCommentsEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideReactionsEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoReactionEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681012
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681011
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1681009
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1681010
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1680987
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1680988
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1680989
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1680990
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1680991
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1680992
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1680993
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->n()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1680994
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1680995
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->p()LX/0Px;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v7

    .line 1680996
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->q()LX/0Px;

    move-result-object v8

    invoke-static {p1, v8}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v8

    .line 1680997
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1680998
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1680999
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1681000
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1681001
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1681002
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1681003
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1681004
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1681005
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1681006
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1681007
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681008
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1680985
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoCommentEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->e:Ljava/util/List;

    .line 1680986
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1680937
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1680938
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1680939
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1680940
    if-eqz v1, :cond_0

    .line 1680941
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680942
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->e:Ljava/util/List;

    .line 1680943
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1680944
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1680945
    if-eqz v1, :cond_1

    .line 1680946
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680947
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->f:Ljava/util/List;

    .line 1680948
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1680949
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1680950
    if-eqz v1, :cond_2

    .line 1680951
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680952
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->g:Ljava/util/List;

    .line 1680953
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1680954
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1680955
    if-eqz v1, :cond_3

    .line 1680956
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680957
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->h:Ljava/util/List;

    .line 1680958
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1680959
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1680960
    if-eqz v1, :cond_4

    .line 1680961
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680962
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->i:Ljava/util/List;

    .line 1680963
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1680964
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1680965
    if-eqz v1, :cond_5

    .line 1680966
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680967
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j:Ljava/util/List;

    :cond_5
    move-object v1, v0

    .line 1680968
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1680969
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    .line 1680970
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    move-result-object v2

    if-eq v2, v0, :cond_6

    .line 1680971
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680972
    iput-object v0, v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    .line 1680973
    :cond_6
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->p()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1680974
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->p()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1680975
    if-eqz v2, :cond_7

    .line 1680976
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680977
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l:Ljava/util/List;

    move-object v1, v0

    .line 1680978
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->q()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_8

    .line 1680979
    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->q()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1680980
    if-eqz v2, :cond_8

    .line 1680981
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    .line 1680982
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m:Ljava/util/List;

    move-object v1, v0

    .line 1680983
    :cond_8
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1680984
    if-nez v1, :cond_9

    :goto_0
    return-object p0

    :cond_9
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1680934
    new-instance v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    invoke-direct {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;-><init>()V

    .line 1680935
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1680936
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1680933
    const v0, -0x52a7f538

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1681013
    const v0, 0x225eb2c6

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideCommentsEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1680917
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideCommentsEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->f:Ljava/util/List;

    .line 1680918
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1680931
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->g:Ljava/util/List;

    .line 1680932
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideReactionsEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1680929
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideReactionsEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->h:Ljava/util/List;

    .line 1680930
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final m()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1680927
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->i:Ljava/util/List;

    .line 1680928
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final n()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoReactionEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1680925
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoReactionEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j:Ljava/util/List;

    .line 1680926
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final o()Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1680923
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    .line 1680924
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->k:Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$VideoModel;

    return-object v0
.end method

.method public final p()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1680921
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l:Ljava/util/List;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l:Ljava/util/List;

    .line 1680922
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->l:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final q()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1680919
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoOverlayTextWithCountdownEventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m:Ljava/util/List;

    .line 1680920
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
