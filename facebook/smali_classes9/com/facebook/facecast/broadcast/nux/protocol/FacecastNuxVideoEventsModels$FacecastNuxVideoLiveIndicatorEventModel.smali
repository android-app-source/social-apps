.class public final Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/AW2;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4d477dc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I

.field private g:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681873
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681872
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1681870
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1681871
    return-void
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1681867
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1681868
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1681869
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1681843
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1681844
    iget v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->f:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1681859
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1681860
    invoke-direct {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->k()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1681861
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1681862
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1681863
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1681864
    const/4 v0, 0x2

    iget v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->g:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1681865
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681866
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1681856
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1681857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681858
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1681852
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1681853
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->f:I

    .line 1681854
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->g:I

    .line 1681855
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1681849
    new-instance v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;

    invoke-direct {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;-><init>()V

    .line 1681850
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1681851
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1681848
    const v0, -0x5beb3456

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1681847
    const v0, 0x54e02f3d

    return v0
.end method

.method public final j()I
    .locals 2

    .prologue
    .line 1681845
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1681846
    iget v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoLiveIndicatorEventModel;->g:I

    return v0
.end method
