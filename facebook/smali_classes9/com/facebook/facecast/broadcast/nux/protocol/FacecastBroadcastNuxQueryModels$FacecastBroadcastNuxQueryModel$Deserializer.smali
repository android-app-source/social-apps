.class public final Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1680632
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    new-instance v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1680633
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1680631
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1680536
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1680537
    const/4 v2, 0x0

    .line 1680538
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_14

    .line 1680539
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1680540
    :goto_0
    move v1, v2

    .line 1680541
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1680542
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1680543
    new-instance v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    invoke-direct {v1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;-><init>()V

    .line 1680544
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1680545
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1680546
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1680547
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1680548
    :cond_0
    return-object v1

    .line 1680549
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1680550
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_13

    .line 1680551
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1680552
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1680553
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v11, :cond_2

    .line 1680554
    const-string p0, "comment_events"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1680555
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 1680556
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_3

    .line 1680557
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_3

    .line 1680558
    invoke-static {p1, v0}, LX/AW8;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1680559
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1680560
    :cond_3
    invoke-static {v10, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v10

    move v10, v10

    .line 1680561
    goto :goto_1

    .line 1680562
    :cond_4
    const-string p0, "hide_comments_events"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1680563
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1680564
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_5

    .line 1680565
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_5

    .line 1680566
    invoke-static {p1, v0}, LX/AW9;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1680567
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1680568
    :cond_5
    invoke-static {v9, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v9

    move v9, v9

    .line 1680569
    goto :goto_1

    .line 1680570
    :cond_6
    const-string p0, "hide_live_indicator_events"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1680571
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1680572
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_7

    .line 1680573
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_7

    .line 1680574
    invoke-static {p1, v0}, LX/AWA;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1680575
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1680576
    :cond_7
    invoke-static {v8, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v8

    move v8, v8

    .line 1680577
    goto/16 :goto_1

    .line 1680578
    :cond_8
    const-string p0, "hide_reactions_events"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1680579
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1680580
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_9

    .line 1680581
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_9

    .line 1680582
    invoke-static {p1, v0}, LX/AWB;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1680583
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1680584
    :cond_9
    invoke-static {v7, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 1680585
    goto/16 :goto_1

    .line 1680586
    :cond_a
    const-string p0, "live_indicator_events"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 1680587
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1680588
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_b

    .line 1680589
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_b

    .line 1680590
    invoke-static {p1, v0}, LX/AWC;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1680591
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1680592
    :cond_b
    invoke-static {v6, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1680593
    goto/16 :goto_1

    .line 1680594
    :cond_c
    const-string p0, "reaction_events"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_e

    .line 1680595
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1680596
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_d

    .line 1680597
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_d

    .line 1680598
    invoke-static {p1, v0}, LX/AWF;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1680599
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1680600
    :cond_d
    invoke-static {v5, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1680601
    goto/16 :goto_1

    .line 1680602
    :cond_e
    const-string p0, "video"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_f

    .line 1680603
    invoke-static {p1, v0}, LX/AW0;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 1680604
    :cond_f
    const-string p0, "video_overlay_text_events"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_11

    .line 1680605
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1680606
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_10

    .line 1680607
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_10

    .line 1680608
    invoke-static {p1, v0}, LX/AWD;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1680609
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 1680610
    :cond_10
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1680611
    goto/16 :goto_1

    .line 1680612
    :cond_11
    const-string p0, "video_overlay_text_with_countdown_events"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 1680613
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1680614
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v11, p0, :cond_12

    .line 1680615
    :goto_9
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v11, p0, :cond_12

    .line 1680616
    invoke-static {p1, v0}, LX/AWE;->b(LX/15w;LX/186;)I

    move-result v11

    .line 1680617
    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 1680618
    :cond_12
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1680619
    goto/16 :goto_1

    .line 1680620
    :cond_13
    const/16 v11, 0x9

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1680621
    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 1680622
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 1680623
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 1680624
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1680625
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1680626
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1680627
    const/4 v2, 0x6

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1680628
    const/4 v2, 0x7

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1680629
    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1680630
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_14
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    goto/16 :goto_1
.end method
