.class public final Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1680634
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    new-instance v1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1680635
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1680636
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1680637
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1680638
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1680639
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1680640
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680641
    if-eqz v2, :cond_1

    .line 1680642
    const-string v3, "comment_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680643
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1680644
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 1680645
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/AW8;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1680646
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1680647
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1680648
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680649
    if-eqz v2, :cond_3

    .line 1680650
    const-string v3, "hide_comments_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680651
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1680652
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_2

    .line 1680653
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/AW9;->a(LX/15i;ILX/0nX;)V

    .line 1680654
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1680655
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1680656
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680657
    if-eqz v2, :cond_5

    .line 1680658
    const-string v3, "hide_live_indicator_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680659
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1680660
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_4

    .line 1680661
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/AWA;->a(LX/15i;ILX/0nX;)V

    .line 1680662
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1680663
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1680664
    :cond_5
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680665
    if-eqz v2, :cond_7

    .line 1680666
    const-string v3, "hide_reactions_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680667
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1680668
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_6

    .line 1680669
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/AWB;->a(LX/15i;ILX/0nX;)V

    .line 1680670
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1680671
    :cond_6
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1680672
    :cond_7
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680673
    if-eqz v2, :cond_9

    .line 1680674
    const-string v3, "live_indicator_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680675
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1680676
    const/4 v3, 0x0

    :goto_4
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_8

    .line 1680677
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/AWC;->a(LX/15i;ILX/0nX;)V

    .line 1680678
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1680679
    :cond_8
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1680680
    :cond_9
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680681
    if-eqz v2, :cond_b

    .line 1680682
    const-string v3, "reaction_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680683
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1680684
    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_a

    .line 1680685
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/AWF;->a(LX/15i;ILX/0nX;)V

    .line 1680686
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1680687
    :cond_a
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1680688
    :cond_b
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680689
    if-eqz v2, :cond_c

    .line 1680690
    const-string v3, "video"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680691
    invoke-static {v1, v2, p1, p2}, LX/AW0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1680692
    :cond_c
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680693
    if-eqz v2, :cond_e

    .line 1680694
    const-string v3, "video_overlay_text_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680695
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1680696
    const/4 v3, 0x0

    :goto_6
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_d

    .line 1680697
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/AWD;->a(LX/15i;ILX/0nX;)V

    .line 1680698
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 1680699
    :cond_d
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1680700
    :cond_e
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1680701
    if-eqz v2, :cond_10

    .line 1680702
    const-string v3, "video_overlay_text_with_countdown_events"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1680703
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1680704
    const/4 v3, 0x0

    :goto_7
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_f

    .line 1680705
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/AWE;->a(LX/15i;ILX/0nX;)V

    .line 1680706
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 1680707
    :cond_f
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1680708
    :cond_10
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1680709
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1680710
    check-cast p1, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel$Serializer;->a(Lcom/facebook/facecast/broadcast/nux/protocol/FacecastBroadcastNuxQueryModels$FacecastBroadcastNuxQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
