.class public final Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements LX/AW2;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xb4cce3e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681749
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1681768
    const-class v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1681766
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1681767
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1681763
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1681764
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1681765
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 1681761
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1681762
    iget v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;->f:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1681769
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1681770
    invoke-direct {p0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1681771
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1681772
    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1681773
    const/4 v0, 0x1

    iget v1, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;->f:I

    invoke-virtual {p1, v0, v1, v2}, LX/186;->a(III)V

    .line 1681774
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681775
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1681758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1681759
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1681760
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1681755
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1681756
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;->f:I

    .line 1681757
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1681752
    new-instance v0, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;

    invoke-direct {v0}, Lcom/facebook/facecast/broadcast/nux/protocol/FacecastNuxVideoEventsModels$FacecastNuxVideoHideLiveIndicatorEventModel;-><init>()V

    .line 1681753
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1681754
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1681751
    const v0, -0x54eb6dfb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1681750
    const v0, -0x552a66e1

    return v0
.end method
