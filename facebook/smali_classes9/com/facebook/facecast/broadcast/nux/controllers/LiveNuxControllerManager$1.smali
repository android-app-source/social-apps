.class public final Lcom/facebook/facecast/broadcast/nux/controllers/LiveNuxControllerManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/AVn;


# direct methods
.method public constructor <init>(LX/AVn;)V
    .locals 0

    .prologue
    .line 1680274
    iput-object p1, p0, Lcom/facebook/facecast/broadcast/nux/controllers/LiveNuxControllerManager$1;->a:LX/AVn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1680275
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/controllers/LiveNuxControllerManager$1;->a:LX/AVn;

    .line 1680276
    iget-object v1, v0, LX/AVn;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AVj;

    .line 1680277
    iget-object v3, v0, LX/AVn;->c:LX/AVt;

    .line 1680278
    iget-object v4, v3, LX/2oy;->j:LX/2pb;

    if-nez v4, :cond_2

    .line 1680279
    const v4, 0x7fffffff

    .line 1680280
    :goto_1
    move v3, v4

    .line 1680281
    iget-object v4, v1, LX/AVj;->a:Ljava/util/LinkedList;

    invoke-static {v4}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v1, LX/AVj;->b:LX/AW2;

    if-nez v4, :cond_3

    .line 1680282
    :goto_2
    goto :goto_0

    .line 1680283
    :cond_0
    iget-object v1, v0, LX/AVn;->c:LX/AVt;

    .line 1680284
    iget-object v2, v1, LX/2oy;->j:LX/2pb;

    if-nez v2, :cond_5

    .line 1680285
    const/4 v2, 0x1

    .line 1680286
    :goto_3
    move v1, v2

    .line 1680287
    if-eqz v1, :cond_1

    .line 1680288
    iget-object v1, v0, LX/AVn;->i:LX/AVd;

    invoke-virtual {v1}, LX/AVd;->b()V

    .line 1680289
    :goto_4
    return-void

    .line 1680290
    :cond_1
    invoke-static {v0}, LX/AVn;->d(LX/AVn;)V

    goto :goto_4

    :cond_2
    iget-object v4, v3, LX/2oy;->j:LX/2pb;

    invoke-virtual {v4}, LX/2pb;->h()I

    move-result v4

    goto :goto_1

    .line 1680291
    :cond_3
    iput v3, v1, LX/AVj;->c:I

    .line 1680292
    invoke-virtual {v1}, LX/AVj;->f()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1680293
    iget-object v4, v1, LX/AVi;->a:Landroid/view/View;

    move-object v4, v4

    .line 1680294
    const/16 p0, 0x8

    invoke-virtual {v4, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1680295
    const/4 v4, 0x0

    iput-object v4, v1, LX/AVj;->b:LX/AW2;

    goto :goto_2

    .line 1680296
    :cond_4
    invoke-virtual {v1}, LX/AVj;->g()V

    goto :goto_2

    :cond_5
    iget-object v2, v1, LX/2oy;->j:LX/2pb;

    invoke-virtual {v2}, LX/2pb;->p()Z

    move-result v2

    goto :goto_3
.end method
