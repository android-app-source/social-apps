.class public Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private final a:Lcom/facebook/widget/text/BetterTextView;

.field private final b:Lcom/facebook/widget/FbImageView;

.field private final c:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1682656
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1682657
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1682658
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1682659
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1682660
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1682661
    const v0, 0x7f0305ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1682662
    const v0, 0x7f0d0f90

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1682663
    const v0, 0x7f0d0f91

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->b:Lcom/facebook/widget/FbImageView;

    .line 1682664
    const v0, 0x7f0d0f92

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    .line 1682665
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 2

    .prologue
    .line 1682666
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->b:Lcom/facebook/widget/FbImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1682667
    return-void

    .line 1682668
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1682669
    iget-object v1, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->c:Lcom/facebook/widget/text/BetterTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1682670
    return-void

    .line 1682671
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1682672
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxOverlayTextView;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1682673
    return-void
.end method
