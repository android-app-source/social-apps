.class public Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;
.super Landroid/widget/ViewSwitcher;
.source ""


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1682631
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1682632
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1682633
    invoke-direct {p0, p1, p2}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1682634
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1682635
    const v2, 0x7f0e038e

    move v2, v2

    .line 1682636
    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 1682637
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0e0391

    invoke-virtual {v1, v2, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 1682638
    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const v2, 0x7f0e0392

    invoke-virtual {v1, v2, v4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    .line 1682639
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1682640
    const v1, 0x7f0309fc

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->a:Landroid/view/View;

    .line 1682641
    const v1, 0x7f0309fc

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->b:Landroid/view/View;

    .line 1682642
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->a:Landroid/view/View;

    const v1, 0x7f0d193f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1682643
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCommentViewSwitcher;->b:Landroid/view/View;

    const v1, 0x7f0d193f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1682644
    return-void
.end method
