.class public Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Lcom/facebook/widget/text/BetterTextView;

.field public final b:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1682654
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1682655
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1682652
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1682653
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1682645
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1682646
    const v0, 0x7f0305ac

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1682647
    const v0, 0x7f0d0f8f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1682648
    const v0, 0x7f0d0f8e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 1682649
    return-void
.end method


# virtual methods
.method public getCountdownOverlayTextView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1682651
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->a:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method

.method public getCountdownView()Lcom/facebook/widget/text/BetterTextView;
    .locals 1

    .prologue
    .line 1682650
    iget-object v0, p0, Lcom/facebook/facecast/broadcast/nux/views/LiveNuxCountdownView;->b:Lcom/facebook/widget/text/BetterTextView;

    return-object v0
.end method
