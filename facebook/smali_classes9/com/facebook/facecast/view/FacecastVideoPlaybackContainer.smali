.class public Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1691404
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691405
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1691394
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691395
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1691399
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;->a:F

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1691400
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1691401
    int-to-float v1, v1

    div-float v0, v1, v0

    float-to-int v0, v0

    .line 1691402
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1691403
    return-void
.end method

.method public setAspectRatio(F)V
    .locals 0

    .prologue
    .line 1691396
    iput p1, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;->a:F

    .line 1691397
    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackContainer;->requestLayout()V

    .line 1691398
    return-void
.end method
