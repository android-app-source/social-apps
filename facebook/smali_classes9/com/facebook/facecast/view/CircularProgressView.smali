.class public Lcom/facebook/facecast/view/CircularProgressView;
.super Landroid/view/View;
.source ""


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private c:F

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/Paint;

.field private j:Landroid/graphics/RectF;

.field private k:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1691252
    const v0, 0x7f0a00d5

    sput v0, Lcom/facebook/facecast/view/CircularProgressView;->a:I

    .line 1691253
    const v0, 0x7f0a00d2

    sput v0, Lcom/facebook/facecast/view/CircularProgressView;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1691204
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/view/CircularProgressView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691205
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/16 v3, 0xff

    .line 1691236
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691237
    sget-object v0, LX/03r;->CircularProgressView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1691238
    if-eqz v0, :cond_0

    .line 1691239
    const/16 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/facecast/view/CircularProgressView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->c:F

    .line 1691240
    const/16 v1, 0x1

    sget v2, Lcom/facebook/facecast/view/CircularProgressView;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->d:I

    .line 1691241
    const/16 v1, 0x2

    sget v2, Lcom/facebook/facecast/view/CircularProgressView;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->e:I

    .line 1691242
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->f:I

    .line 1691243
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->g:I

    .line 1691244
    :goto_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1691245
    invoke-direct {p0}, Lcom/facebook/facecast/view/CircularProgressView;->b()V

    .line 1691246
    return-void

    .line 1691247
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/facecast/view/CircularProgressView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v4}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->c:F

    .line 1691248
    sget v1, Lcom/facebook/facecast/view/CircularProgressView;->a:I

    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->d:I

    .line 1691249
    sget v1, Lcom/facebook/facecast/view/CircularProgressView;->b:I

    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->e:I

    .line 1691250
    iput v3, p0, Lcom/facebook/facecast/view/CircularProgressView;->f:I

    .line 1691251
    iput v3, p0, Lcom/facebook/facecast/view/CircularProgressView;->g:I

    goto :goto_0
.end method

.method private a(II)Landroid/graphics/Paint;
    .locals 2

    .prologue
    .line 1691229
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    .line 1691230
    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1691231
    iget v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->c:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1691232
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1691233
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1691234
    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1691235
    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 1691226
    iget v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->c:F

    .line 1691227
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/facecast/view/CircularProgressView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    invoke-virtual {p0}, Lcom/facebook/facecast/view/CircularProgressView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    invoke-direct {v1, v0, v0, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->j:Landroid/graphics/RectF;

    .line 1691228
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1691223
    iget v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->d:I

    iget v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->f:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/view/CircularProgressView;->a(II)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->h:Landroid/graphics/Paint;

    .line 1691224
    iget v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->e:I

    iget v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->g:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/facecast/view/CircularProgressView;->a(II)Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->i:Landroid/graphics/Paint;

    .line 1691225
    return-void
.end method


# virtual methods
.method public getProgress()F
    .locals 1

    .prologue
    .line 1691222
    iget v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->k:F

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    const/4 v2, 0x0

    .line 1691216
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1691217
    iget-object v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->j:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/facebook/facecast/view/CircularProgressView;->h:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1691218
    iget v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->k:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 1691219
    iget v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->k:F

    mul-float/2addr v3, v0

    .line 1691220
    iget-object v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->j:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget-object v5, p0, Lcom/facebook/facecast/view/CircularProgressView;->i:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 1691221
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x455dd7ef

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1691213
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 1691214
    invoke-direct {p0}, Lcom/facebook/facecast/view/CircularProgressView;->a()V

    .line 1691215
    const/16 v1, 0x2d

    const v2, -0x3284d800

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setProgress(F)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1691206
    iput p1, p0, Lcom/facebook/facecast/view/CircularProgressView;->k:F

    .line 1691207
    iget v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->k:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 1691208
    iput v2, p0, Lcom/facebook/facecast/view/CircularProgressView;->k:F

    .line 1691209
    :cond_0
    iget v0, p0, Lcom/facebook/facecast/view/CircularProgressView;->k:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 1691210
    iput v1, p0, Lcom/facebook/facecast/view/CircularProgressView;->k:F

    .line 1691211
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecast/view/CircularProgressView;->invalidate()V

    .line 1691212
    return-void
.end method
