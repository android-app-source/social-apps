.class public Lcom/facebook/facecast/view/FacecastHdUploadButton;
.super LX/4lL;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/AXS;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1691345
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691346
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691343
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691344
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691329
    invoke-direct {p0, p1, p2, p3}, LX/4lL;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691330
    invoke-direct {p0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->a()V

    .line 1691331
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1691337
    const-class v0, Lcom/facebook/facecast/view/FacecastHdUploadButton;

    invoke-static {v0, p0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1691338
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastHdUploadButton;->a:LX/0wM;

    const v1, 0x7f020a1f

    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x106000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1691339
    invoke-static {p0, v0, v4, v4, v4}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1691340
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->setClickable(Z)V

    .line 1691341
    sget-object v0, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v1, LX/0xr;->MEDIUM:LX/0xr;

    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 1691342
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecast/view/FacecastHdUploadButton;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecast/view/FacecastHdUploadButton;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastHdUploadButton;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public final performClick()Z
    .locals 1

    .prologue
    .line 1691334
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastHdUploadButton;->b:LX/AXS;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastHdUploadButton;->b:LX/AXS;

    invoke-interface {v0}, LX/AXS;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1691335
    const/4 v0, 0x1

    .line 1691336
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, LX/4lL;->performClick()Z

    move-result v0

    goto :goto_0
.end method

.method public setOnInterceptClickListener(LX/AXS;)V
    .locals 0

    .prologue
    .line 1691332
    iput-object p1, p0, Lcom/facebook/facecast/view/FacecastHdUploadButton;->b:LX/AXS;

    .line 1691333
    return-void
.end method
