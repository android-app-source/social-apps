.class public Lcom/facebook/facecast/view/FacecastPreviewSaveButton;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:Lcom/facebook/facecast/view/CircularProgressView;

.field public d:LX/Abm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1691359
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691360
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691361
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691362
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691363
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691364
    invoke-direct {p0}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a()V

    .line 1691365
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1691366
    const v0, 0x7f0305de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1691367
    const v0, 0x7f0d1028

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1691368
    const v0, 0x7f0d102a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1691369
    const v0, 0x7f0d1029

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/view/CircularProgressView;

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    .line 1691370
    sget-object v0, LX/Abm;->IDLE:LX/Abm;

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a(LX/Abm;)V

    .line 1691371
    return-void
.end method


# virtual methods
.method public final a(LX/Abm;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1691372
    sget-object v0, LX/Abl;->a:[I

    invoke-virtual {p1}, LX/Abm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1691373
    :goto_0
    iput-object p1, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->d:LX/Abm;

    .line 1691374
    return-void

    .line 1691375
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1691376
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1691377
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    invoke-virtual {v0, v2}, Lcom/facebook/facecast/view/CircularProgressView;->setVisibility(I)V

    goto :goto_0

    .line 1691378
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1691379
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1691380
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    invoke-virtual {v0, v3}, Lcom/facebook/facecast/view/CircularProgressView;->setVisibility(I)V

    .line 1691381
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/CircularProgressView;->setProgress(F)V

    goto :goto_0

    .line 1691382
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1691383
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1691384
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    invoke-virtual {v0, v3}, Lcom/facebook/facecast/view/CircularProgressView;->setVisibility(I)V

    .line 1691385
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/facecast/view/CircularProgressView;->setProgress(F)V

    .line 1691386
    new-instance v0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton$1;

    invoke-direct {v0, p0}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton$1;-><init>(Lcom/facebook/facecast/view/FacecastPreviewSaveButton;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v0, v2, v3}, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1691387
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->a:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1691388
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->b:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1691389
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    invoke-virtual {v0, v2}, Lcom/facebook/facecast/view/CircularProgressView;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getCurrentState()LX/Abm;
    .locals 1

    .prologue
    .line 1691390
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->d:LX/Abm;

    return-object v0
.end method

.method public setProgress(F)V
    .locals 1

    .prologue
    .line 1691391
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    invoke-virtual {v0}, Lcom/facebook/facecast/view/CircularProgressView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1691392
    :goto_0
    return-void

    .line 1691393
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastPreviewSaveButton;->c:Lcom/facebook/facecast/view/CircularProgressView;

    invoke-virtual {v0, p1}, Lcom/facebook/facecast/view/CircularProgressView;->setProgress(F)V

    goto :goto_0
.end method
