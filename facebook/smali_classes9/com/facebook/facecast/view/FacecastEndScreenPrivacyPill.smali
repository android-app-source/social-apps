.class public Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# static fields
.field private static final j:Ljava/lang/String;


# instance fields
.field public a:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Ajc;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/93j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/93n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/93d;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/privacy/PrivacyOperationsClient;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/8Sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/facecast/model/FacecastPrivacyData;

.field private l:I

.field private final m:LX/93q;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1691327
    const-class v0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1691325
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691326
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691323
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691324
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1691317
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1691318
    new-instance v0, LX/AWS;

    invoke-direct {v0}, LX/AWS;-><init>()V

    invoke-virtual {v0}, LX/AWS;->a()Lcom/facebook/facecast/model/FacecastPrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->k:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1691319
    new-instance v0, LX/Abj;

    invoke-direct {v0, p0}, LX/Abj;-><init>(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;)V

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->m:LX/93q;

    .line 1691320
    const-class v0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    invoke-static {v0, p0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1691321
    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->l:I

    .line 1691322
    return-void
.end method

.method public static synthetic a(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1691316
    invoke-direct {p0, p1}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 1691328
    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v2

    sget-object v3, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v2, v3}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;LX/0Sh;LX/03V;LX/Ajc;LX/93j;LX/93n;LX/93d;Lcom/facebook/privacy/PrivacyOperationsClient;LX/8Sa;LX/0wM;)V
    .locals 0

    .prologue
    .line 1691315
    iput-object p1, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a:LX/0Sh;

    iput-object p2, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->b:LX/03V;

    iput-object p3, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->c:LX/Ajc;

    iput-object p4, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->d:LX/93j;

    iput-object p5, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->e:LX/93n;

    iput-object p6, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->f:LX/93d;

    iput-object p7, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    iput-object p8, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->h:LX/8Sa;

    iput-object p9, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->i:LX/0wM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;

    invoke-static {v9}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v1

    check-cast v1, LX/0Sh;

    invoke-static {v9}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    const-class v3, LX/Ajc;

    invoke-interface {v9, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Ajc;

    const-class v4, LX/93j;

    invoke-interface {v9, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/93j;

    const-class v5, LX/93n;

    invoke-interface {v9, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/93n;

    const-class v6, LX/93d;

    invoke-interface {v9, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/93d;

    invoke-static {v9}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0QB;)Lcom/facebook/privacy/PrivacyOperationsClient;

    move-result-object v7

    check-cast v7, Lcom/facebook/privacy/PrivacyOperationsClient;

    invoke-static {v9}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v8

    check-cast v8, LX/8Sa;

    invoke-static {v9}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v9

    check-cast v9, LX/0wM;

    invoke-static/range {v0 .. v9}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;LX/0Sh;LX/03V;LX/Ajc;LX/93j;LX/93n;LX/93d;Lcom/facebook/privacy/PrivacyOperationsClient;LX/8Sa;LX/0wM;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Z)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1691281
    invoke-virtual {p0, p1}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->setText(Ljava/lang/CharSequence;)V

    .line 1691282
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->i:LX/0wM;

    iget v2, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->l:I

    invoke-virtual {v0, p2, v2}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1691283
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->i:LX/0wM;

    const v3, 0x7f020a13

    iget v4, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->l:I

    invoke-virtual {v0, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1691284
    :goto_0
    invoke-virtual {p0, v2, v1, v0, v1}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1691285
    return-void

    :cond_0
    move-object v0, v1

    .line 1691286
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/facecast/model/FacecastPrivacyData;Lcom/facebook/ipc/composer/intent/ComposerTargetData;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1691287
    iput-object p1, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->k:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1691288
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->k:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1691289
    iget-object v1, v0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v1

    .line 1691290
    if-eqz v0, :cond_1

    .line 1691291
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->k:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1691292
    iget-object v1, v0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-object v0, v1

    .line 1691293
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1691294
    if-eqz v0, :cond_1

    .line 1691295
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v1, v0, v2}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a$redex0(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;Z)V

    .line 1691296
    invoke-virtual {p0, v2}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->setClickable(Z)V

    .line 1691297
    :cond_0
    :goto_0
    return-void

    .line 1691298
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->setText(Ljava/lang/CharSequence;)V

    .line 1691299
    sget-object v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->k:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1691300
    iget-object v2, v1, Lcom/facebook/facecast/model/FacecastPrivacyData;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1691301
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1691302
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->c:LX/Ajc;

    iget-object v1, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->m:LX/93q;

    invoke-virtual {v0, v1}, LX/Ajc;->a(LX/93q;)LX/Ajb;

    move-result-object v0

    .line 1691303
    invoke-virtual {v0}, LX/93Q;->a()V

    goto :goto_0

    .line 1691304
    :cond_2
    if-eqz p2, :cond_0

    .line 1691305
    sget-object v0, LX/Abk;->a:[I

    iget-object v1, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v1}, LX/2rw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1691306
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->b:LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "updatePrivacyData"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1691307
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->d:LX/93j;

    iget-object v1, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->m:LX/93q;

    iget-wide v2, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/93j;->a(LX/93q;Ljava/lang/Long;)LX/93i;

    move-result-object v0

    .line 1691308
    invoke-virtual {v0}, LX/93Q;->a()V

    goto :goto_0

    .line 1691309
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->e:LX/93n;

    iget-object v1, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->m:LX/93q;

    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08131d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, LX/93n;->a(LX/93q;Ljava/lang/String;Ljava/lang/String;)LX/93m;

    move-result-object v0

    .line 1691310
    invoke-virtual {v0}, LX/93Q;->a()V

    goto/16 :goto_0

    .line 1691311
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->f:LX/93d;

    iget-object v1, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->m:LX/93q;

    iget-wide v2, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-object v3, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetName:Ljava/lang/String;

    iget-object v4, p2, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetPrivacy:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/93d;->a(LX/93q;Ljava/lang/Long;Ljava/lang/String;LX/2rX;)LX/93c;

    move-result-object v0

    .line 1691312
    invoke-virtual {v0}, LX/93Q;->a()V

    goto/16 :goto_0

    .line 1691313
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->g:Lcom/facebook/privacy/PrivacyOperationsClient;

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/PrivacyOperationsClient;->a(LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1691314
    iget-object v1, p0, Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;->a:LX/0Sh;

    new-instance v2, LX/Abi;

    invoke-direct {v2, p0}, LX/Abi;-><init>(Lcom/facebook/facecast/view/FacecastEndScreenPrivacyPill;)V

    invoke-virtual {v1, v0, v2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
