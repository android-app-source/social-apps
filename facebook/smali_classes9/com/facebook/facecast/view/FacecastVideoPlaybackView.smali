.class public Lcom/facebook/facecast/view/FacecastVideoPlaybackView;
.super Lcom/facebook/video/player/RichVideoPlayer;
.source ""


# instance fields
.field private m:Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;

.field private n:Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

.field public o:LX/Abn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1691414
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691415
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1691451
    invoke-direct {p0, p1, p2}, Lcom/facebook/video/player/RichVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1691452
    invoke-direct {p0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->A()V

    .line 1691453
    return-void
.end method

.method private A()V
    .locals 2

    .prologue
    .line 1691454
    new-instance v0, Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;

    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->m:Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;

    .line 1691455
    new-instance v0, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->n:Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    .line 1691456
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1691457
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1691458
    new-instance v0, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1691459
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1691460
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->n:Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    .line 1691461
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1691462
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->m:Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;

    .line 1691463
    invoke-static {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1691464
    invoke-virtual {p0}, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->d()V

    .line 1691465
    new-instance v0, LX/Abn;

    invoke-direct {v0, p0}, LX/Abn;-><init>(Lcom/facebook/facecast/view/FacecastVideoPlaybackView;)V

    iput-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->o:LX/Abn;

    .line 1691466
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->o:LX/Abn;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 1691467
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1691428
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1691429
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1691430
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1691431
    sget-object v0, LX/04D;->LIVE_VIDEO_END_SCREEN:LX/04D;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1691432
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v0

    .line 1691433
    iput-object p1, v0, LX/2oE;->a:Landroid/net/Uri;

    .line 1691434
    move-object v0, v0

    .line 1691435
    sget-object v1, LX/097;->FROM_LOCAL_STORAGE:LX/097;

    .line 1691436
    iput-object v1, v0, LX/2oE;->e:LX/097;

    .line 1691437
    move-object v0, v0

    .line 1691438
    invoke-virtual {v0}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v0

    .line 1691439
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v0

    .line 1691440
    iput-boolean v2, v0, LX/2oH;->t:Z

    .line 1691441
    move-object v0, v0

    .line 1691442
    iput-boolean v2, v0, LX/2oH;->g:Z

    .line 1691443
    move-object v0, v0

    .line 1691444
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1691445
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1691446
    move-object v0, v1

    .line 1691447
    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 1691448
    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1691449
    const/4 v0, 0x0

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1691450
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1691425
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1691426
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1691427
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1691422
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->n:Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-virtual {v0, p0}, LX/2oy;->a(Landroid/view/ViewGroup;)V

    .line 1691423
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->m:Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;

    invoke-virtual {v0, p0}, LX/2oy;->a(Landroid/view/ViewGroup;)V

    .line 1691424
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1691419
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->m:Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;

    invoke-virtual {v0}, LX/2oy;->b()Landroid/view/ViewGroup;

    .line 1691420
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->n:Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-virtual {v0}, LX/2oy;->b()Landroid/view/ViewGroup;

    .line 1691421
    return-void
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 1691416
    iget-object v0, p0, Lcom/facebook/facecast/view/FacecastVideoPlaybackView;->o:LX/Abn;

    invoke-virtual {p0, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 1691417
    invoke-virtual {p0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1691418
    return-void
.end method
