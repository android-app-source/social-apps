.class public Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public final a:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1679205
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1679206
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1679203
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1679204
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1679186
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1679187
    const v0, 0x7f03072e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1679188
    const v0, 0x7f0d1047

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;->a:Landroid/view/View;

    .line 1679189
    new-instance v0, LX/AVJ;

    invoke-direct {v0, p0, p0}, LX/AVJ;-><init>(Lcom/facebook/facecast/FullScreenRelativeLayoutWithDecorViews;Landroid/view/ViewGroup;)V

    .line 1679190
    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1679199
    const-string v0, "FullScreenRelativeLayoutWithDecorViews.dispatchDraw"

    const v1, -0x651114d

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1679200
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1679201
    const v0, 0x3392709d

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1679202
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 1679195
    const-string v0, "FullScreenRelativeLayoutWithDecorViews.onLayout"

    const v1, 0x417e56b1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1679196
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomRelativeLayout;->onLayout(ZIIII)V

    .line 1679197
    const v0, -0x10c2a25a

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1679198
    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1679191
    const-string v0, "FullScreenRelativeLayoutWithDecorViews.onMeasure"

    const v1, -0x4ab31d15

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 1679192
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;->onMeasure(II)V

    .line 1679193
    const v0, 0x74fdb27f

    invoke-static {v0}, LX/02m;->a(I)V

    .line 1679194
    return-void
.end method
