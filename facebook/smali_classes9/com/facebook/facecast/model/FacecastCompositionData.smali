.class public Lcom/facebook/facecast/model/FacecastCompositionData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecast/model/FacecastCompositionData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/facecast/model/FacecastPrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final k:LX/21D;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final m:Z

.field public final n:Z

.field public final o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final q:Z

.field public final r:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1682865
    new-instance v0, LX/AWO;

    invoke-direct {v0}, LX/AWO;-><init>()V

    sput-object v0, Lcom/facebook/facecast/model/FacecastCompositionData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1682863
    new-instance v0, LX/AWP;

    invoke-direct {v0}, LX/AWP;-><init>()V

    invoke-direct {p0, v0}, Lcom/facebook/facecast/model/FacecastCompositionData;-><init>(LX/AWP;)V

    .line 1682864
    return-void
.end method

.method public constructor <init>(LX/AWP;)V
    .locals 1

    .prologue
    .line 1682817
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682818
    iget-object v0, p1, LX/AWP;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->a:Ljava/lang/String;

    .line 1682819
    iget-object v0, p1, LX/AWP;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1682820
    iget-object v0, p1, LX/AWP;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1682821
    iget-object v0, p1, LX/AWP;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->d:Ljava/lang/String;

    .line 1682822
    iget-object v0, p1, LX/AWP;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->e:Ljava/lang/String;

    .line 1682823
    iget-object v0, p1, LX/AWP;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->f:Ljava/lang/String;

    .line 1682824
    iget-object v0, p1, LX/AWP;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->g:Ljava/lang/String;

    .line 1682825
    iget-object v0, p1, LX/AWP;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->h:Ljava/lang/String;

    .line 1682826
    iget-object v0, p1, LX/AWP;->i:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->i:Ljava/lang/String;

    .line 1682827
    iget-object v0, p1, LX/AWP;->j:LX/0Px;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->j:LX/0Px;

    .line 1682828
    iget-object v0, p1, LX/AWP;->k:LX/21D;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->k:LX/21D;

    .line 1682829
    iget-object v0, p1, LX/AWP;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->l:Ljava/lang/String;

    .line 1682830
    iget-boolean v0, p1, LX/AWP;->m:Z

    iput-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->m:Z

    .line 1682831
    iget-boolean v0, p1, LX/AWP;->p:Z

    iput-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->n:Z

    .line 1682832
    iget-object v0, p1, LX/AWP;->q:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->o:Ljava/lang/String;

    .line 1682833
    iget-object v0, p1, LX/AWP;->r:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->p:Ljava/lang/String;

    .line 1682834
    iget-boolean v0, p1, LX/AWP;->n:Z

    iput-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->q:Z

    .line 1682835
    iget-boolean v0, p1, LX/AWP;->o:Z

    iput-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->r:Z

    .line 1682836
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1682866
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682867
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->a:Ljava/lang/String;

    .line 1682868
    const-class v0, Lcom/facebook/facecast/model/FacecastPrivacyData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/model/FacecastPrivacyData;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    .line 1682869
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    .line 1682870
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->d:Ljava/lang/String;

    .line 1682871
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->e:Ljava/lang/String;

    .line 1682872
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->f:Ljava/lang/String;

    .line 1682873
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->g:Ljava/lang/String;

    .line 1682874
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->h:Ljava/lang/String;

    .line 1682875
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->i:Ljava/lang/String;

    .line 1682876
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1682877
    const-class v3, Ljava/util/List;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1682878
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->j:LX/0Px;

    .line 1682879
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 1682880
    const-string v3, ""

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->k:LX/21D;

    .line 1682881
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->l:Ljava/lang/String;

    .line 1682882
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->m:Z

    .line 1682883
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->n:Z

    .line 1682884
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->o:Ljava/lang/String;

    .line 1682885
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->p:Ljava/lang/String;

    .line 1682886
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->q:Z

    .line 1682887
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-ne v0, v1, :cond_4

    :goto_4
    iput-boolean v1, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->r:Z

    .line 1682888
    return-void

    .line 1682889
    :cond_0
    invoke-static {v0}, LX/21D;->valueOf(Ljava/lang/String;)LX/21D;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1682890
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1682891
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1682892
    goto :goto_3

    :cond_4
    move v1, v2

    .line 1682893
    goto :goto_4
.end method


# virtual methods
.method public final c()Lcom/facebook/ipc/composer/intent/ComposerTargetData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1682862
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1682861
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1682837
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682838
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->b:Lcom/facebook/facecast/model/FacecastPrivacyData;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1682839
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->c:Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1682840
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682841
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682842
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682843
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682844
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682845
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682846
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->j:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1682847
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->k:LX/21D;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682848
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682849
    iget-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->m:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1682850
    iget-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->n:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1682851
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682852
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682853
    iget-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->q:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1682854
    iget-boolean v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->r:Z

    if-eqz v0, :cond_4

    :goto_4
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1682855
    return-void

    .line 1682856
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastCompositionData;->k:LX/21D;

    invoke-virtual {v0}, LX/21D;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1682857
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1682858
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1682859
    goto :goto_3

    :cond_4
    move v1, v2

    .line 1682860
    goto :goto_4
.end method
