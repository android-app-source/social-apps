.class public Lcom/facebook/facecast/model/FacecastFixedPrivacyData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecast/model/FacecastFixedPrivacyData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1682933
    new-instance v0, LX/AWQ;

    invoke-direct {v0}, LX/AWQ;-><init>()V

    sput-object v0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    .line 1682916
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682917
    invoke-static {p1}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v0

    .line 1682918
    invoke-static {p1}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v1

    .line 1682919
    invoke-static {p1}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v2

    .line 1682920
    invoke-static {p1}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a(Landroid/os/Parcel;)Ljava/lang/String;

    move-result-object v3

    .line 1682921
    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->fromIconName(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v3

    .line 1682922
    new-instance v4, LX/7lN;

    invoke-direct {v4}, LX/7lN;-><init>()V

    .line 1682923
    iput-object v0, v4, LX/7lN;->b:Ljava/lang/String;

    .line 1682924
    move-object v0, v4

    .line 1682925
    iput-object v1, v0, LX/7lN;->c:Ljava/lang/String;

    .line 1682926
    move-object v0, v0

    .line 1682927
    iput-object v2, v0, LX/7lN;->d:Ljava/lang/String;

    .line 1682928
    move-object v0, v0

    .line 1682929
    iput-object v3, v0, LX/7lN;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    .line 1682930
    move-object v0, v0

    .line 1682931
    invoke-virtual {v0}, LX/7lN;->a()Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1682932
    return-void
.end method

.method public constructor <init>(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;)V
    .locals 0

    .prologue
    .line 1682913
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682914
    iput-object p1, p0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    .line 1682915
    return-void
.end method

.method private static a(Landroid/os/Parcel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1682910
    invoke-virtual {p0}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-nez v0, :cond_0

    .line 1682911
    const/4 v0, 0x0

    .line 1682912
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/os/Parcel;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1682897
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1682898
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1682899
    :goto_0
    return-void

    .line 1682900
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1682901
    invoke-virtual {p0, p1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1682909
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;
    .locals 1

    .prologue
    .line 1682908
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1682907
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1682902
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1682903
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1682904
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->d:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1682905
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a:Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;->a(Landroid/os/Parcel;Ljava/lang/String;)V

    .line 1682906
    return-void
.end method
