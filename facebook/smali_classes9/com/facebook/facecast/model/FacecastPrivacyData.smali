.class public Lcom/facebook/facecast/model/FacecastPrivacyData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/facecast/model/FacecastPrivacyData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/privacy/model/SelectablePrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1682964
    new-instance v0, LX/AWR;

    invoke-direct {v0}, LX/AWR;-><init>()V

    sput-object v0, Lcom/facebook/facecast/model/FacecastPrivacyData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1682948
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682949
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->a:Ljava/lang/String;

    .line 1682950
    const-class v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/model/SelectablePrivacyData;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1682951
    const-class v0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    iput-object v0, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    .line 1682952
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/privacy/model/SelectablePrivacyData;Lcom/facebook/facecast/model/FacecastFixedPrivacyData;)V
    .locals 0

    .prologue
    .line 1682953
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1682954
    iput-object p1, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->a:Ljava/lang/String;

    .line 1682955
    iput-object p2, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1682956
    iput-object p3, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    .line 1682957
    return-void
.end method


# virtual methods
.method public final d()LX/AWS;
    .locals 2

    .prologue
    .line 1682958
    new-instance v0, LX/AWS;

    invoke-direct {v0, p0}, LX/AWS;-><init>(Lcom/facebook/facecast/model/FacecastPrivacyData;)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1682959
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1682960
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1682961
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1682962
    iget-object v0, p0, Lcom/facebook/facecast/model/FacecastPrivacyData;->c:Lcom/facebook/facecast/model/FacecastFixedPrivacyData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1682963
    return-void
.end method
