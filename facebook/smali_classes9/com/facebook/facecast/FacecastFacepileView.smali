.class public Lcom/facebook/facecast/FacecastFacepileView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/3Rb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:I

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/fbui/facepile/FacepileView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6UY;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1678854
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/facecast/FacecastFacepileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1678855
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1678856
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/facecast/FacecastFacepileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1678857
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1678858
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1678859
    const-class v0, Lcom/facebook/facecast/FacecastFacepileView;

    invoke-static {v0, p0}, Lcom/facebook/facecast/FacecastFacepileView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1678860
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/FacecastFacepileView;->setOrientation(I)V

    .line 1678861
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastFacepileView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b05b3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/facecast/FacecastFacepileView;->b:I

    .line 1678862
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastFacepileView;->c:Ljava/util/List;

    .line 1678863
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-gtz v1, :cond_0

    .line 1678864
    const v0, 0x7f0305d1

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1678865
    invoke-virtual {p0}, Lcom/facebook/facecast/FacecastFacepileView;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/facebook/facecast/FacecastFacepileView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    .line 1678866
    iget-object v2, p0, Lcom/facebook/facecast/FacecastFacepileView;->c:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1678867
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1678868
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/facecast/FacecastFacepileView;->d:Ljava/util/List;

    .line 1678869
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/facecast/FacecastFacepileView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/facecast/FacecastFacepileView;

    invoke-static {v0}, LX/3Rb;->a(LX/0QB;)LX/3Rb;

    move-result-object v0

    check-cast v0, LX/3Rb;

    iput-object v0, p0, Lcom/facebook/facecast/FacecastFacepileView;->a:LX/3Rb;

    return-void
.end method


# virtual methods
.method public setFBIDs(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1678870
    iget-object v0, p0, Lcom/facebook/facecast/FacecastFacepileView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1678871
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1678872
    iget-object v2, p0, Lcom/facebook/facecast/FacecastFacepileView;->a:LX/3Rb;

    iget v3, p0, Lcom/facebook/facecast/FacecastFacepileView;->b:I

    iget v4, p0, Lcom/facebook/facecast/FacecastFacepileView;->b:I

    invoke-virtual {v2, v0, v3, v4}, LX/3Rb;->a(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    .line 1678873
    iget-object v2, p0, Lcom/facebook/facecast/FacecastFacepileView;->d:Ljava/util/List;

    new-instance v3, LX/6UY;

    invoke-direct {v3, v0}, LX/6UY;-><init>(Landroid/net/Uri;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1678874
    :cond_0
    iget-object v0, p0, Lcom/facebook/facecast/FacecastFacepileView;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iget-object v1, p0, Lcom/facebook/facecast/FacecastFacepileView;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 1678875
    return-void
.end method
