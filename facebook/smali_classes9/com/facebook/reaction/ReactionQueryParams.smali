.class public Lcom/facebook/reaction/ReactionQueryParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/reaction/ReactionQueryParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public A:Z

.field public B:Ljava/lang/String;

.field public a:Ljava/lang/String;

.field public b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private g:Lcom/facebook/location/ImmutableLocation;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/Long;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Lcom/facebook/http/interfaces/RequestPriority;

.field public p:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionRequestTypeValue;
    .end annotation
.end field

.field public q:Ljava/lang/String;

.field public r:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public t:Ljava/lang/Long;

.field public u:Ljava/lang/String;

.field private v:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public w:Ljava/lang/String;

.field public x:Ljava/lang/String;

.field public y:Ljava/lang/String;

.field public z:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1926278
    new-instance v0, LX/CfV;

    invoke-direct {v0}, LX/CfV;-><init>()V

    sput-object v0, Lcom/facebook/reaction/ReactionQueryParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1926244
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1926245
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    .line 1926246
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    .line 1926247
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1926248
    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    .line 1926249
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    .line 1926250
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1926251
    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    .line 1926252
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    .line 1926253
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    .line 1926254
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    .line 1926255
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 1926256
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    .line 1926257
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    .line 1926258
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    .line 1926259
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1926260
    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->v:LX/0Px;

    .line 1926261
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    .line 1926262
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    .line 1926263
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1926264
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1926265
    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    .line 1926266
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    .line 1926267
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    .line 1926268
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    .line 1926269
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 1926270
    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    .line 1926271
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1926272
    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->s:LX/0Px;

    .line 1926273
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 1926274
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    .line 1926275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->A:Z

    .line 1926276
    iput-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    .line 1926277
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1926214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1926215
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    .line 1926216
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    .line 1926217
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    .line 1926218
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    .line 1926219
    const-class v0, Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/location/ImmutableLocation;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    .line 1926220
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    .line 1926221
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    .line 1926222
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    .line 1926223
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    .line 1926224
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    .line 1926225
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    .line 1926226
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    .line 1926227
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    .line 1926228
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    .line 1926229
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->v:LX/0Px;

    .line 1926230
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    .line 1926231
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    .line 1926232
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/http/interfaces/RequestPriority;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1926233
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    .line 1926234
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    .line 1926235
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    .line 1926236
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    .line 1926237
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Rf;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    .line 1926238
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->s:LX/0Px;

    .line 1926239
    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    .line 1926240
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    .line 1926241
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->A:Z

    .line 1926242
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    .line 1926243
    return-void
.end method


# virtual methods
.method public final a(LX/0yD;)LX/4J6;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1926151
    new-instance v2, LX/4J6;

    invoke-direct {v2}, LX/4J6;-><init>()V

    .line 1926152
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1926153
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    .line 1926154
    const-string v3, "action"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926155
    :cond_0
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 1926156
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1926157
    const-string v3, "triggering_profile_ids"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1926158
    :cond_1
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1926159
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 1926160
    const-string v3, "reaction_unit_ids"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1926161
    :cond_2
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1926162
    const-string v1, " "

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    aput-object v4, v3, v0

    invoke-static {v1, v3}, LX/0YN;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1926163
    const-string v3, "post_text"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926164
    :cond_3
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1926165
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1926166
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_4

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1926167
    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1926168
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1926169
    :cond_4
    const-string v0, "mentioned_ids"

    invoke-virtual {v2, v0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1926170
    :cond_5
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1926171
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    .line 1926172
    const-string v1, "og_action_type_id"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926173
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    .line 1926174
    const-string v1, "og_object_id"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926175
    :cond_6
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 1926176
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 1926177
    const-string v1, "place_id"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926178
    :cond_7
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1926179
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    .line 1926180
    const-string v1, "share_id"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926181
    :cond_8
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    if-nez v0, :cond_9

    .line 1926182
    const-string v0, "normal"

    invoke-virtual {v2, v0}, LX/4J6;->d(Ljava/lang/String;)LX/4J6;

    .line 1926183
    :goto_1
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1926184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1926185
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1926186
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1926187
    :cond_9
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/4J6;->d(Ljava/lang/String;)LX/4J6;

    goto :goto_1

    .line 1926188
    :cond_a
    const-string v0, "tagged_user_ids"

    invoke-virtual {v2, v0, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1926189
    :cond_b
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->s:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1926190
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->s:LX/0Px;

    .line 1926191
    const-string v1, "tracking_codes"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1926192
    :cond_c
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    if-eqz v0, :cond_f

    .line 1926193
    new-instance v0, LX/3Aj;

    invoke-direct {v0}, LX/3Aj;-><init>()V

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Aj;->a(Ljava/lang/Double;)LX/3Aj;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/3Aj;->b(Ljava/lang/Double;)LX/3Aj;

    move-result-object v1

    .line 1926194
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->c()LX/0am;

    move-result-object v0

    .line 1926195
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1926196
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3Aj;->c(Ljava/lang/Double;)LX/3Aj;

    .line 1926197
    :cond_d
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {p1, v0}, LX/0yD;->a(Lcom/facebook/location/ImmutableLocation;)J

    move-result-wide v4

    .line 1926198
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_e

    .line 1926199
    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/3Aj;->e(Ljava/lang/Double;)LX/3Aj;

    .line 1926200
    :cond_e
    const-string v0, "viewer_coordinates"

    invoke-virtual {v2, v0, v1}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1926201
    :cond_f
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->v:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1926202
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->v:LX/0Px;

    .line 1926203
    const-string v1, "requested_units"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1926204
    :cond_10
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1926205
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    .line 1926206
    const-string v1, "start_date"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926207
    :cond_11
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_12

    .line 1926208
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    .line 1926209
    const-string v1, "end_date"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926210
    :cond_12
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1926211
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    .line 1926212
    const-string v1, "referrer"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926213
    :cond_13
    return-object v2
.end method

.method public final a(Ljava/lang/String;)Lcom/facebook/reaction/ReactionQueryParams;
    .locals 0

    .prologue
    .line 1926149
    iput-object p1, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    .line 1926150
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/facebook/reaction/ReactionQueryParams;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/facebook/reaction/ReactionQueryParams;"
        }
    .end annotation

    .prologue
    .line 1926279
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    .line 1926280
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1926148
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1926099
    if-ne p0, p1, :cond_1

    const/4 v0, 0x1

    .line 1926100
    :cond_0
    :goto_0
    return v0

    .line 1926101
    :cond_1
    instance-of v1, p1, Lcom/facebook/reaction/ReactionQueryParams;

    if-eqz v1, :cond_0

    .line 1926102
    check-cast p1, Lcom/facebook/reaction/ReactionQueryParams;

    .line 1926103
    iget-wide v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    iget-wide v4, p1, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1926104
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    if-eqz v1, :cond_14

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926105
    :cond_2
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    if-eqz v1, :cond_15

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926106
    :cond_3
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    if-eqz v1, :cond_16

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926107
    :cond_4
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    if-eqz v1, :cond_17

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926108
    :cond_5
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926109
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v1, v2}, Lcom/facebook/location/ImmutableLocation;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926110
    :cond_6
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926111
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    if-eqz v1, :cond_19

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926112
    :cond_7
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    if-eqz v1, :cond_1a

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926113
    :cond_8
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    if-eqz v1, :cond_1b

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926114
    :cond_9
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926115
    :cond_a
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    if-eqz v1, :cond_1d

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926116
    :cond_b
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    if-eqz v1, :cond_1e

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926117
    :cond_c
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    if-ne v1, v2, :cond_0

    .line 1926118
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    if-ne v1, v2, :cond_0

    .line 1926119
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    if-eqz v1, :cond_1f

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926120
    :cond_d
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    invoke-virtual {v1, v2}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926121
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->s:LX/0Px;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->s:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926122
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    if-eqz v1, :cond_20

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926123
    :cond_e
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    if-eqz v1, :cond_21

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926124
    :cond_f
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->v:LX/0Px;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->v:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926125
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    if-eqz v1, :cond_22

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926126
    :cond_10
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    if-eqz v1, :cond_23

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926127
    :cond_11
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    if-eqz v1, :cond_24

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926128
    :cond_12
    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    if-eqz v1, :cond_25

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    iget-object v2, p1, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1926129
    :cond_13
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    invoke-virtual {v0, v1}, LX/0Rf;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0

    .line 1926130
    :cond_14
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    goto/16 :goto_0

    .line 1926131
    :cond_15
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    goto/16 :goto_0

    .line 1926132
    :cond_16
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    goto/16 :goto_0

    .line 1926133
    :cond_17
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    if-eqz v1, :cond_5

    goto/16 :goto_0

    .line 1926134
    :cond_18
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    if-eqz v1, :cond_6

    goto/16 :goto_0

    .line 1926135
    :cond_19
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    if-eqz v1, :cond_7

    goto/16 :goto_0

    .line 1926136
    :cond_1a
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    if-eqz v1, :cond_8

    goto/16 :goto_0

    .line 1926137
    :cond_1b
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    if-eqz v1, :cond_9

    goto/16 :goto_0

    .line 1926138
    :cond_1c
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    if-eqz v1, :cond_a

    goto/16 :goto_0

    .line 1926139
    :cond_1d
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    if-eqz v1, :cond_b

    goto/16 :goto_0

    .line 1926140
    :cond_1e
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    if-eqz v1, :cond_c

    goto/16 :goto_0

    .line 1926141
    :cond_1f
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    if-eqz v1, :cond_d

    goto/16 :goto_0

    .line 1926142
    :cond_20
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    if-eqz v1, :cond_e

    goto/16 :goto_0

    .line 1926143
    :cond_21
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    if-eqz v1, :cond_f

    goto/16 :goto_0

    .line 1926144
    :cond_22
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    if-eqz v1, :cond_10

    goto/16 :goto_0

    .line 1926145
    :cond_23
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    if-eqz v1, :cond_11

    goto/16 :goto_0

    .line 1926146
    :cond_24
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    if-eqz v1, :cond_12

    goto/16 :goto_0

    .line 1926147
    :cond_25
    iget-object v1, p1, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    if-eqz v1, :cond_13

    goto/16 :goto_0
.end method

.method public final f()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1926098
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    return-object v0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1926051
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 1926052
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    iget-wide v4, p0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1926053
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 1926054
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 1926055
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    .line 1926056
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1926057
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {v0}, Lcom/facebook/location/ImmutableLocation;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    .line 1926058
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1926059
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    .line 1926060
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    .line 1926061
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    .line 1926062
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    .line 1926063
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    .line 1926064
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    .line 1926065
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0}, Lcom/facebook/http/interfaces/RequestPriority;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v0, v2

    .line 1926066
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_c
    add-int/2addr v0, v2

    .line 1926067
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_d
    add-int/2addr v0, v2

    .line 1926068
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    invoke-virtual {v2}, LX/0Rf;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1926069
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->s:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1926070
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    :goto_e
    add-int/2addr v0, v2

    .line 1926071
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_f
    add-int/2addr v0, v2

    .line 1926072
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->v:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1926073
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_10
    add-int/2addr v0, v2

    .line 1926074
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_11
    add-int/2addr v0, v2

    .line 1926075
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_12
    add-int/2addr v0, v2

    .line 1926076
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    invoke-virtual {v2}, LX/0Rf;->hashCode()I

    move-result v2

    add-int/2addr v0, v2

    .line 1926077
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 1926078
    return v0

    :cond_1
    move v0, v1

    .line 1926079
    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 1926080
    goto/16 :goto_1

    :cond_3
    move v0, v1

    .line 1926081
    goto/16 :goto_2

    :cond_4
    move v0, v1

    .line 1926082
    goto/16 :goto_3

    :cond_5
    move v0, v1

    .line 1926083
    goto/16 :goto_4

    :cond_6
    move v0, v1

    .line 1926084
    goto/16 :goto_5

    :cond_7
    move v0, v1

    .line 1926085
    goto/16 :goto_6

    :cond_8
    move v0, v1

    .line 1926086
    goto/16 :goto_7

    :cond_9
    move v0, v1

    .line 1926087
    goto/16 :goto_8

    :cond_a
    move v0, v1

    .line 1926088
    goto/16 :goto_9

    :cond_b
    move v0, v1

    .line 1926089
    goto/16 :goto_a

    :cond_c
    move v0, v1

    .line 1926090
    goto/16 :goto_b

    :cond_d
    move v0, v1

    .line 1926091
    goto/16 :goto_c

    :cond_e
    move v0, v1

    .line 1926092
    goto/16 :goto_d

    :cond_f
    move v0, v1

    .line 1926093
    goto :goto_e

    :cond_10
    move v0, v1

    .line 1926094
    goto :goto_f

    :cond_11
    move v0, v1

    .line 1926095
    goto :goto_10

    :cond_12
    move v0, v1

    .line 1926096
    goto :goto_11

    :cond_13
    move v0, v1

    .line 1926097
    goto :goto_12
.end method

.method public final p()Ljava/lang/Long;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1926050
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1926021
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926022
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926023
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926024
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->f:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926025
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->g:Lcom/facebook/location/ImmutableLocation;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1926026
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->h:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926027
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926028
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->j:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926029
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926030
    iget-wide v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1926031
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->l:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926032
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926033
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926034
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->w:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926035
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->v:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926036
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->x:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926037
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926038
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->o:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926039
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->z:LX/0Rf;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926040
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926041
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926042
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926043
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->r:LX/0Rf;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926044
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->s:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926045
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->t:Ljava/lang/Long;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    .line 1926046
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->u:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926047
    iget-boolean v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->A:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1926048
    iget-object v0, p0, Lcom/facebook/reaction/ReactionQueryParams;->B:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1926049
    return-void
.end method
