.class public Lcom/facebook/reaction/activity/ReactionDialogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f8;


# instance fields
.field public p:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0jo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/CfW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0iI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private t:LX/0hE;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1926497
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/facebook/reaction/ReactionQueryParams;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 1926496
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/reaction/activity/ReactionDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "close_activity_after_finish"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "handle_composer_publish"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "query_params"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "reaction_surface"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/reaction/activity/ReactionDialogActivity;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0jo;LX/CfW;LX/0iI;)V
    .locals 0

    .prologue
    .line 1926495
    iput-object p1, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->p:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    iput-object p2, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->q:LX/0jo;

    iput-object p3, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->r:LX/CfW;

    iput-object p4, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->s:LX/0iI;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;

    invoke-static {v3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {v3}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v1

    check-cast v1, LX/0jo;

    invoke-static {v3}, LX/CfW;->b(LX/0QB;)LX/CfW;

    move-result-object v2

    check-cast v2, LX/CfW;

    invoke-static {v3}, LX/0iI;->a(LX/0QB;)LX/0iI;

    move-result-object v3

    check-cast v3, LX/0iI;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/reaction/activity/ReactionDialogActivity;->a(Lcom/facebook/reaction/activity/ReactionDialogActivity;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0jo;LX/CfW;LX/0iI;)V

    return-void
.end method


# virtual methods
.method public final a(LX/0hE;)V
    .locals 0

    .prologue
    .line 1926494
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1926474
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1926475
    invoke-static {p0, p0}, Lcom/facebook/reaction/activity/ReactionDialogActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1926476
    const v0, 0x7f031148

    invoke-virtual {p0, v0}, Lcom/facebook/reaction/activity/ReactionDialogActivity;->setContentView(I)V

    .line 1926477
    invoke-virtual {p0}, Lcom/facebook/reaction/activity/ReactionDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1926478
    const-string v0, "query_params"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "reaction_surface"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "reaction_session_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1926479
    const-string v0, "query_params"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/ReactionQueryParams;

    .line 1926480
    const-string v2, "reaction_surface"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1926481
    iget-object v3, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->r:LX/CfW;

    invoke-virtual {v3, v2, v0}, LX/CfW;->a(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)LX/2jY;

    move-result-object v2

    .line 1926482
    const-string v0, "reaction_session_id"

    .line 1926483
    iget-object v3, v2, LX/2jY;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1926484
    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1926485
    iget-object v0, v2, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v0

    .line 1926486
    if-nez v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1926487
    :goto_0
    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1926488
    iput-object v0, v2, LX/2jY;->x:Landroid/os/Bundle;

    .line 1926489
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->q:LX/0jo;

    sget-object v2, LX/0cQ;->REACTION_DIALOG_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-interface {v0, v2}, LX/0jo;->a(I)LX/0jq;

    move-result-object v0

    invoke-interface {v0, v1}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1926490
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d03b1

    const-string v3, "chromeless:content:fragment:tag"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1926491
    return-void

    .line 1926492
    :cond_1
    iget-object v0, v2, LX/2jY;->x:Landroid/os/Bundle;

    move-object v0, v0

    .line 1926493
    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1926459
    invoke-virtual {p0}, Lcom/facebook/reaction/activity/ReactionDialogActivity;->i()LX/0hE;

    move-result-object v0

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    return v0
.end method

.method public final i()LX/0hE;
    .locals 1

    .prologue
    .line 1926471
    iget-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->t:LX/0hE;

    if-nez v0, :cond_0

    .line 1926472
    iget-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->s:LX/0iI;

    invoke-virtual {v0, p0}, LX/0iI;->a(Landroid/app/Activity;)LX/0hE;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->t:LX/0hE;

    .line 1926473
    :cond_0
    iget-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->t:LX/0hE;

    return-object v0
.end method

.method public final l()LX/0hE;
    .locals 1

    .prologue
    .line 1926470
    const/4 v0, 0x0

    return-object v0
.end method

.method public final m()LX/0hE;
    .locals 1

    .prologue
    .line 1926469
    const/4 v0, 0x0

    return-object v0
.end method

.method public final o()LX/0hE;
    .locals 1

    .prologue
    .line 1926468
    const/4 v0, 0x0

    return-object v0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1926465
    const/16 v0, 0x6dc

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1926466
    iget-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->p:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v0, p3}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1926467
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1926461
    iget-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->t:LX/0hE;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->t:LX/0hE;

    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1926462
    iget-object v0, p0, Lcom/facebook/reaction/activity/ReactionDialogActivity;->t:LX/0hE;

    invoke-interface {v0}, LX/0hE;->b()Z

    .line 1926463
    :goto_0
    return-void

    .line 1926464
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 1926460
    const/4 v0, 0x0

    return v0
.end method
