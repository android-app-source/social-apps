.class public Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;
.super Lcom/facebook/view/ViewController;
.source ""

# interfaces
.implements LX/7Um;


# instance fields
.field public b:Landroid/support/v4/view/ViewPager;

.field public c:LX/0hc;

.field private d:I

.field public e:I

.field public f:F

.field public g:Z

.field private final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/facebook/view/ControlledView;)V
    .locals 5

    .prologue
    .line 1798613
    invoke-direct {p0, p1}, Lcom/facebook/view/ViewController;-><init>(Lcom/facebook/view/ControlledView;)V

    .line 1798614
    new-instance v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;

    invoke-direct {v0, p0}, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;-><init>(Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;)V

    iput-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->h:Ljava/lang/Runnable;

    .line 1798615
    iget-object v0, p0, Lcom/facebook/view/ViewController;->a:Lcom/facebook/view/ControlledView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v2, p0, Lcom/facebook/view/ViewController;->a:Lcom/facebook/view/ControlledView;

    invoke-virtual {v2}, Lcom/facebook/view/ControlledView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f010271

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1798616
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1798609
    iget-boolean v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->g:Z

    if-nez v0, :cond_0

    .line 1798610
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->g:Z

    .line 1798611
    iget-object v0, p0, Lcom/facebook/view/ViewController;->a:Lcom/facebook/view/ControlledView;

    iget-object v1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/view/ControlledView;->post(Ljava/lang/Runnable;)Z

    .line 1798612
    :cond_0
    return-void
.end method


# virtual methods
.method public final B_(I)V
    .locals 1

    .prologue
    .line 1798603
    iget v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->d:I

    if-nez v0, :cond_0

    .line 1798604
    iput p1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->e:I

    .line 1798605
    invoke-direct {p0}, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b()V

    .line 1798606
    :cond_0
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    if-eqz v0, :cond_1

    .line 1798607
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->B_(I)V

    .line 1798608
    :cond_1
    return-void
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 1798617
    invoke-direct {p0}, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b()V

    .line 1798618
    return-void
.end method

.method public final a(IFI)V
    .locals 1

    .prologue
    .line 1798597
    iput p1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->e:I

    .line 1798598
    iput p2, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->f:F

    .line 1798599
    invoke-direct {p0}, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b()V

    .line 1798600
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    if-eqz v0, :cond_0

    .line 1798601
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    invoke-interface {v0, p1, p2, p3}, LX/0hc;->a(IFI)V

    .line 1798602
    :cond_0
    return-void
.end method

.method public final a(Landroid/support/v4/view/ViewPager;)V
    .locals 2

    .prologue
    .line 1798582
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b:Landroid/support/v4/view/ViewPager;

    if-ne v0, p1, :cond_0

    .line 1798583
    :goto_0
    return-void

    .line 1798584
    :cond_0
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_1

    .line 1798585
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1798586
    :cond_1
    invoke-virtual {p1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1798587
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ViewPager does not have adapter instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1798588
    :cond_2
    iput-object p1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b:Landroid/support/v4/view/ViewPager;

    .line 1798589
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 1798590
    invoke-direct {p0}, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b()V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1798591
    iput p1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->d:I

    .line 1798592
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    if-eqz v0, :cond_0

    .line 1798593
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    .line 1798594
    :cond_0
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    if-eqz v0, :cond_1

    .line 1798595
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->c:LX/0hc;

    invoke-interface {v0, p1}, LX/0hc;->b(I)V

    .line 1798596
    :cond_1
    return-void
.end method
