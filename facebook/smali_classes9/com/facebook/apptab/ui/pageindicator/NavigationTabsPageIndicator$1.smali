.class public final Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;


# direct methods
.method public constructor <init>(Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;)V
    .locals 0

    .prologue
    .line 1798566
    iput-object p1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1798567
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget-boolean v0, v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->g:Z

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1798568
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    const/4 v1, 0x0

    .line 1798569
    iput-boolean v1, v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->g:Z

    .line 1798570
    iget-object v0, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    .line 1798571
    iget-object v1, v0, Lcom/facebook/view/ViewController;->a:Lcom/facebook/view/ControlledView;

    invoke-virtual {v1}, Lcom/facebook/view/ControlledView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    .line 1798572
    if-eqz v1, :cond_0

    iget-object v2, v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v2

    if-nez v2, :cond_2

    .line 1798573
    :cond_0
    const/4 v1, 0x0

    .line 1798574
    :goto_0
    move v0, v1

    .line 1798575
    iget-object v1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget-object v1, v1, Lcom/facebook/view/ViewController;->a:Lcom/facebook/view/ControlledView;

    invoke-virtual {v1}, Lcom/facebook/view/ControlledView;->getWidth()I

    move-result v1

    if-eq v1, v0, :cond_1

    .line 1798576
    iget-object v1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget-object v1, v1, Lcom/facebook/view/ViewController;->a:Lcom/facebook/view/ControlledView;

    invoke-virtual {v1}, Lcom/facebook/view/ControlledView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1798577
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1798578
    iget-object v2, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget-object v2, v2, Lcom/facebook/view/ViewController;->a:Lcom/facebook/view/ControlledView;

    invoke-virtual {v2, v1}, Lcom/facebook/view/ControlledView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1798579
    :cond_1
    iget-object v1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget v1, v1, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->e:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget v2, v2, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->f:F

    add-float/2addr v1, v2

    int-to-float v0, v0

    mul-float/2addr v0, v1

    .line 1798580
    iget-object v1, p0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator$1;->a:Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;

    iget-object v1, v1, Lcom/facebook/view/ViewController;->a:Lcom/facebook/view/ControlledView;

    invoke-virtual {v1, v0}, Lcom/facebook/view/ControlledView;->setTranslationX(F)V

    .line 1798581
    return-void

    :cond_2
    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v1

    iget-object v2, v0, Lcom/facebook/apptab/ui/pageindicator/NavigationTabsPageIndicator;->b:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v2

    invoke-virtual {v2}, LX/0gG;->b()I

    move-result v2

    div-int/2addr v1, v2

    goto :goto_0
.end method
