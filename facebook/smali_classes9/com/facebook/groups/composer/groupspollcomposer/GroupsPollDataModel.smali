.class public Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModelDeserializer;
.end annotation


# instance fields
.field private canViewerAddPollOption:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_add_poll_option"
    .end annotation
.end field

.field private canViewerChooseMultipleOptions:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "can_choose_multiple_options"
    .end annotation
.end field

.field private pollOptions:Ljava/util/List;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "poll_optins"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1865710
    const-class v0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModelDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1865711
    const-class v0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModelSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1865713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1865714
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->pollOptions:Ljava/util/List;

    .line 1865715
    iput-boolean v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->canViewerAddPollOption:Z

    .line 1865716
    iput-boolean v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->canViewerChooseMultipleOptions:Z

    .line 1865717
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1865712
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->pollOptions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1865707
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->pollOptions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1865708
    iput-boolean p1, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->canViewerAddPollOption:Z

    .line 1865709
    return-void
.end method

.method public final a(Ljava/lang/String;LX/CGf;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1865701
    sget-object v1, LX/CGf;->ADD:LX/CGf;

    if-ne p2, v1, :cond_0

    .line 1865702
    iget-object v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->pollOptions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1865703
    :goto_0
    return v0

    .line 1865704
    :cond_0
    sget-object v1, LX/CGf;->REMOVE:LX/CGf;

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->pollOptions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->pollOptions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1865705
    iget-object v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->pollOptions:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1865706
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1865700
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->pollOptions:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 1865698
    iput-boolean p1, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->canViewerChooseMultipleOptions:Z

    .line 1865699
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1865697
    iget-boolean v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->canViewerAddPollOption:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1865696
    iget-boolean v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->canViewerChooseMultipleOptions:Z

    return v0
.end method
