.class public Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModelSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1865741
    const-class v0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    new-instance v1, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModelSerializer;

    invoke-direct {v1}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModelSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1865742
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1865743
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1865744
    if-nez p0, :cond_0

    .line 1865745
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1865746
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1865747
    invoke-static {p0, p1, p2}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModelSerializer;->b(Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;LX/0nX;LX/0my;)V

    .line 1865748
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1865749
    return-void
.end method

.method private static b(Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1865750
    const-string v0, "poll_optins"

    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1865751
    const-string v0, "can_add_poll_option"

    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->c()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1865752
    const-string v0, "can_choose_multiple_options"

    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1865753
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1865754
    check-cast p1, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModelSerializer;->a(Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;LX/0nX;LX/0my;)V

    return-void
.end method
