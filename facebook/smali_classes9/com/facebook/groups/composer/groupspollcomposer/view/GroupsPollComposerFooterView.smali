.class public Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbEditText;

.field private b:Lcom/facebook/widget/listview/BetterListView;

.field private c:LX/CGn;

.field public d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1865871
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1865872
    invoke-direct {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->a()V

    .line 1865873
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1865868
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1865869
    invoke-direct {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->a()V

    .line 1865870
    return-void
.end method

.method private final a()V
    .locals 3

    .prologue
    .line 1865854
    const v0, 0x7f030862

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1865855
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->setOrientation(I)V

    .line 1865856
    const v0, 0x7f0d15d8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->a:Lcom/facebook/resources/ui/FbEditText;

    .line 1865857
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->a:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/CGh;

    invoke-direct {v1, p0}, LX/CGh;-><init>(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1865858
    const v0, 0x7f0d15d7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1865859
    new-instance v1, LX/CGi;

    invoke-direct {v1, p0}, LX/CGi;-><init>(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1865860
    const v0, 0x7f0d15d9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1865861
    new-instance v1, LX/CGj;

    invoke-direct {v1, p0}, LX/CGj;-><init>(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1865862
    const v0, 0x7f0d15d5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->b:Lcom/facebook/widget/listview/BetterListView;

    .line 1865863
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->b:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->addFooterView(Landroid/view/View;)V

    .line 1865864
    new-instance v0, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-direct {v0}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    .line 1865865
    new-instance v0, LX/CGn;

    invoke-direct {v0, p0}, LX/CGn;-><init>(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;)V

    iput-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->c:LX/CGn;

    .line 1865866
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->b:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->c:LX/CGn;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1865867
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 1865808
    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f082964

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1865809
    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f082965

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1865810
    new-instance v2, LX/5OM;

    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/5OM;-><init>(Landroid/content/Context;)V

    .line 1865811
    new-instance v3, LX/CGp;

    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, LX/CGk;

    invoke-direct {v5, p0, v0, v1}, LX/CGk;-><init>(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v3, v4, v5}, LX/CGp;-><init>(Landroid/content/Context;LX/CGk;)V

    .line 1865812
    invoke-virtual {v2, v3}, LX/5OM;->a(LX/5OG;)V

    .line 1865813
    invoke-virtual {v3, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 1865814
    iget-object v4, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v4}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->c()Z

    move-result v4

    invoke-virtual {v0, v4}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 1865815
    invoke-virtual {v3, v1}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 1865816
    iget-object v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v1}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 1865817
    invoke-virtual {v2, p1}, LX/0ht;->c(Landroid/view/View;)V

    .line 1865818
    invoke-virtual {v2}, LX/0ht;->d()V

    .line 1865819
    return-void
.end method

.method public static a$redex0(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1865843
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/CGg;->a(Ljava/lang/String;Ljava/util/List;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1865844
    if-eqz v0, :cond_1

    .line 1865845
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1865846
    invoke-virtual {v1, v0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    .line 1865847
    const v0, 0x104000a

    new-instance v2, LX/CGl;

    invoke-direct {v2, p0}, LX/CGl;-><init>(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1865848
    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 1865849
    :cond_0
    :goto_0
    return-void

    .line 1865850
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/CGf;->ADD:LX/CGf;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->a(Ljava/lang/String;LX/CGf;)Z

    move-result v0

    .line 1865851
    if-eqz v0, :cond_0

    .line 1865852
    invoke-direct {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d()V

    .line 1865853
    invoke-direct {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->b()V

    goto :goto_0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1865840
    invoke-direct {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->c()V

    .line 1865841
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->c:LX/CGn;

    const v1, -0x58ebb9a5

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1865842
    return-void
.end method

.method public static b$redex0(Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1865836
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/CGf;->REMOVE:LX/CGf;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->a(Ljava/lang/String;LX/CGf;)Z

    move-result v0

    .line 1865837
    if-eqz v0, :cond_0

    .line 1865838
    invoke-direct {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->b()V

    .line 1865839
    :cond_0
    return-void
.end method

.method private c()V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v6, -0x1

    .line 1865828
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->a()I

    move-result v0

    .line 1865829
    if-eq v0, v2, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 1865830
    :goto_0
    return-void

    .line 1865831
    :cond_0
    if-ne v0, v2, :cond_1

    .line 1865832
    invoke-virtual {p0}, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c9a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1865833
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    int-to-double v2, v1

    const-wide/high16 v4, 0x4004000000000000L    # 2.5

    mul-double/2addr v2, v4

    double-to-int v1, v2

    invoke-direct {v0, v6, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 1865834
    :goto_1
    iget-object v1, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->b:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/listview/BetterListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1865835
    :cond_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v6, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    goto :goto_1
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1865823
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->a:Lcom/facebook/resources/ui/FbEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1865824
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    invoke-virtual {v0}, Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 1865825
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->a:Lcom/facebook/resources/ui/FbEditText;

    const v1, 0x7f082963

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setHint(I)V

    .line 1865826
    :goto_0
    return-void

    .line 1865827
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->a:Lcom/facebook/resources/ui/FbEditText;

    const v1, 0x7f082962

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setHint(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;)V
    .locals 2

    .prologue
    .line 1865820
    iput-object p1, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->d:Lcom/facebook/groups/composer/groupspollcomposer/GroupsPollDataModel;

    .line 1865821
    iget-object v0, p0, Lcom/facebook/groups/composer/groupspollcomposer/view/GroupsPollComposerFooterView;->c:LX/CGn;

    const v1, 0x62ef4cdd

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1865822
    return-void
.end method
