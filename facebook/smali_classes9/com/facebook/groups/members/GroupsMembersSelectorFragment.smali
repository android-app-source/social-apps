.class public Lcom/facebook/groups/members/GroupsMembersSelectorFragment;
.super Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;
.source ""


# instance fields
.field private A:LX/B1e;

.field private B:LX/B1e;

.field private final C:LX/B1b;

.field public u:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/B1f;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private w:Ljava/lang/String;

.field private x:Ljava/lang/String;

.field private y:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field private z:LX/621;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/621",
            "<",
            "Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1696892
    invoke-direct {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;-><init>()V

    .line 1696893
    const-string v0, ""

    iput-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->x:Ljava/lang/String;

    .line 1696894
    new-instance v0, LX/B1g;

    invoke-direct {v0, p0}, LX/B1g;-><init>(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;)V

    iput-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->C:LX/B1b;

    return-void
.end method

.method private J()LX/B1e;
    .locals 6

    .prologue
    .line 1696889
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->A:LX/B1e;

    if-nez v0, :cond_0

    .line 1696890
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->v:LX/B1f;

    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->w:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b19f9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->C:LX/B1b;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/B1f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/B1b;Ljava/lang/Boolean;)LX/B1e;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->A:LX/B1e;

    .line 1696891
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->A:LX/B1e;

    return-object v0
.end method

.method private K()LX/B1e;
    .locals 6

    .prologue
    .line 1696895
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    .line 1696896
    iget-object v1, v0, LX/B1e;->h:Ljava/lang/String;

    move-object v0, v1

    .line 1696897
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1696898
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    invoke-virtual {v0}, LX/B1d;->e()V

    .line 1696899
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    .line 1696900
    iget-object v1, v0, LX/B1e;->h:Ljava/lang/String;

    move-object v0, v1

    .line 1696901
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1696902
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->v:LX/B1f;

    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->w:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->x:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b19f9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->C:LX/B1b;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/B1f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;LX/B1b;Ljava/lang/Boolean;)LX/B1e;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    .line 1696903
    :cond_2
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    return-object v0
.end method

.method private L()V
    .locals 3

    .prologue
    .line 1696904
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->z:LX/621;

    if-nez v0, :cond_0

    .line 1696905
    new-instance v0, LX/623;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "group_members_section"

    invoke-virtual {p0, v2}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->b(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1696906
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 1696907
    invoke-direct {v0, v1, v2}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    iput-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->z:LX/621;

    .line 1696908
    :cond_0
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v0}, LX/8tB;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1696909
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->z:LX/621;

    invoke-virtual {v0, v1, v2}, LX/8tB;->a(ILX/621;)V

    .line 1696910
    :cond_1
    return-void
.end method

.method public static M(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;)V
    .locals 1

    .prologue
    .line 1696911
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->x:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1696912
    invoke-direct {p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->J()LX/B1e;

    move-result-object v0

    invoke-virtual {v0}, LX/B1d;->f()V

    .line 1696913
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    if-eqz v0, :cond_0

    .line 1696914
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    invoke-virtual {v0}, LX/B1d;->e()V

    .line 1696915
    :cond_0
    :goto_0
    return-void

    .line 1696916
    :cond_1
    invoke-direct {p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->K()LX/B1e;

    move-result-object v0

    invoke-virtual {v0}, LX/B1d;->f()V

    .line 1696917
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->A:LX/B1e;

    if-eqz v0, :cond_0

    .line 1696918
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->A:LX/B1e;

    invoke-virtual {v0}, LX/B1d;->e()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;LX/0kL;LX/B1f;)V
    .locals 0

    .prologue
    .line 1696919
    iput-object p1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->u:LX/0kL;

    iput-object p2, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->v:LX/B1f;

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    invoke-static {v1}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    const-class v2, LX/B1f;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/B1f;

    invoke-static {p0, v0, v1}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->a(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;LX/0kL;LX/B1f;)V

    return-void
.end method

.method private static a(III)Z
    .locals 1

    .prologue
    .line 1696920
    add-int v0, p0, p1

    add-int/lit8 v0, v0, 0x5

    if-lt v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;III)Z
    .locals 1

    .prologue
    .line 1696921
    invoke-static {p1, p2, p3}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->a(III)Z

    move-result v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1696883
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v0

    .line 1696884
    const-string v1, "group_members_section"

    invoke-virtual {v0, v1, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1696885
    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/util/Map;)V

    .line 1696886
    return-void
.end method


# virtual methods
.method public final E()V
    .locals 0

    .prologue
    .line 1696887
    invoke-static {p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->M(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;)V

    .line 1696888
    return-void
.end method

.method public final H()Z
    .locals 1

    .prologue
    .line 1696882
    const/4 v0, 0x0

    return v0
.end method

.method public I()Z
    .locals 1

    .prologue
    .line 1696881
    const/4 v0, 0x0

    return v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1696875
    invoke-virtual {p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->e()LX/0Px;

    move-result-object v1

    .line 1696876
    iget-object v0, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->j:LX/8tB;

    invoke-virtual {v0, p1}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    .line 1696877
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    const/16 v1, 0x31

    if-le v0, v1, :cond_0

    .line 1696878
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->u:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082706

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1696879
    :goto_0
    return-void

    .line 1696880
    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(I)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1696863
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Landroid/os/Bundle;)V

    .line 1696864
    const-class v0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;

    invoke-static {v0, p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1696865
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1696866
    if-eqz v0, :cond_0

    .line 1696867
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->w:Ljava/lang/String;

    .line 1696868
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->w:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1696869
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1696870
    if-eqz v0, :cond_1

    .line 1696871
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1696872
    const-string v1, "group_feed_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->w:Ljava/lang/String;

    .line 1696873
    :cond_1
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->w:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1696874
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1696856
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1696857
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1696858
    new-instance v4, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-direct {v4, v0}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;-><init>(Lcom/facebook/user/model/User;)V

    .line 1696859
    invoke-virtual {v2, v4}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1696860
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1696861
    :cond_0
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->a(Ljava/util/Set;)V

    .line 1696862
    return-void
.end method

.method public final b(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 1696855
    const v0, 0x7f082705

    return v0
.end method

.method public final e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1696854
    invoke-virtual {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->o()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1696853
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3be448bc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696840
    invoke-super {p0, p1}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1696841
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    .line 1696842
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1696843
    invoke-interface {v1, v2}, LX/BWl;->d(Landroid/view/View;)Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->y:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1696844
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->y:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v2, LX/8uy;->PLAIN_TEXT:LX/8uy;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTextMode(LX/8uy;)V

    .line 1696845
    invoke-virtual {p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->I()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1696846
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->y:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const v2, 0x7f082704

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setHint(I)V

    .line 1696847
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->y:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a05ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 1696848
    iput v2, v1, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    .line 1696849
    iget-object v1, p0, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->m:LX/BWl;

    .line 1696850
    iget-object v2, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v2, v2

    .line 1696851
    invoke-interface {v1, v2}, LX/BWl;->c(Landroid/view/View;)Lcom/facebook/widget/listview/BetterListView;

    move-result-object v1

    new-instance v2, LX/B1h;

    invoke-direct {v2, p0}, LX/B1h;-><init>(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/listview/BetterListView;->a(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1696852
    const/16 v1, 0x2b

    const v2, -0x1a4449b4

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x2225289c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1696834
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->A:LX/B1e;

    if-eqz v1, :cond_0

    .line 1696835
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->A:LX/B1e;

    invoke-virtual {v1}, LX/B1d;->e()V

    .line 1696836
    :cond_0
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    if-eqz v1, :cond_1

    .line 1696837
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->B:LX/B1e;

    invoke-virtual {v1}, LX/B1d;->e()V

    .line 1696838
    :cond_1
    invoke-super {p0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->onPause()V

    .line 1696839
    const/16 v1, 0x2b

    const v2, 0x21b8f90f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final u()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1696827
    const-string v0, "group_members_section"

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final x()V
    .locals 2

    .prologue
    .line 1696828
    iget-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->y:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1696829
    iget-object v1, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->x:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1696830
    :goto_0
    return-void

    .line 1696831
    :cond_0
    iput-object v0, p0, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->x:Ljava/lang/String;

    .line 1696832
    invoke-direct {p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->L()V

    .line 1696833
    invoke-static {p0}, Lcom/facebook/groups/members/GroupsMembersSelectorFragment;->M(Lcom/facebook/groups/members/GroupsMembersSelectorFragment;)V

    goto :goto_0
.end method
