.class public final Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3b607a90
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1736395
    const-class v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1736394
    const-class v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1736374
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1736375
    return-void
.end method

.method private m()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736396
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1736397
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1736398
    :cond_0
    iget-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1736409
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1736410
    invoke-direct {p0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->m()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1736411
    invoke-virtual {p0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1736412
    invoke-virtual {p0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1736413
    invoke-virtual {p0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0x4679d6dd

    invoke-static {v4, v3, v5}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1736414
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1736415
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1736416
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1736417
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1736418
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1736419
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1736420
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1736399
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1736400
    invoke-virtual {p0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1736401
    invoke-virtual {p0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x4679d6dd

    invoke-static {v2, v0, v3}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1736402
    invoke-virtual {p0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1736403
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;

    .line 1736404
    iput v3, v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->h:I

    .line 1736405
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1736406
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1736407
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1736408
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1736421
    new-instance v0, LX/B1k;

    invoke-direct {v0, p1}, LX/B1k;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736390
    invoke-virtual {p0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1736391
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1736392
    const/4 v0, 0x3

    const v1, 0x4679d6dd

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->h:I

    .line 1736393
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1736388
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1736389
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1736387
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1736384
    new-instance v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;-><init>()V

    .line 1736385
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1736386
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1736383
    const v0, -0x20a92c8a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1736382
    const v0, 0x50c72189

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736380
    iget-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->f:Ljava/lang/String;

    .line 1736381
    iget-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736378
    iget-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->g:Ljava/lang/String;

    .line 1736379
    iget-object v0, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736376
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1736377
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$GroupMemberProfilesModel$NodesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
