.class public final Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1736481
    const-class v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;

    new-instance v1, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1736482
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1736470
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1736472
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1736473
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1736474
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1736475
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1736476
    if-eqz v2, :cond_0

    .line 1736477
    const-string p0, "group_member_profiles"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1736478
    invoke-static {v1, v2, p1, p2}, LX/B1o;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1736479
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1736480
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1736471
    check-cast p1, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel$Serializer;->a(Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$FetchGroupFriendMemberModel;LX/0nX;LX/0my;)V

    return-void
.end method
