.class public final Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1736311
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1736312
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1736309
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1736310
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1736293
    if-nez p1, :cond_0

    .line 1736294
    :goto_0
    return v0

    .line 1736295
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1736296
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1736297
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1736298
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1736299
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1736300
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1736301
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1736302
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1736303
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1736304
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v2

    .line 1736305
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1736306
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1736307
    invoke-virtual {p3, v4, v2}, LX/186;->a(IZ)V

    .line 1736308
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x2a5442ca -> :sswitch_1
        0x4679d6dd -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1736292
    new-instance v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1736289
    sparse-switch p0, :sswitch_data_0

    .line 1736290
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1736291
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x2a5442ca -> :sswitch_0
        0x4679d6dd -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1736255
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1736287
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;->b(I)V

    .line 1736288
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1736282
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1736283
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1736284
    :cond_0
    iput-object p1, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;->a:LX/15i;

    .line 1736285
    iput p2, p0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;->b:I

    .line 1736286
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1736281
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1736280
    new-instance v0, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1736277
    iget v0, p0, LX/1vt;->c:I

    .line 1736278
    move v0, v0

    .line 1736279
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1736274
    iget v0, p0, LX/1vt;->c:I

    .line 1736275
    move v0, v0

    .line 1736276
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1736271
    iget v0, p0, LX/1vt;->b:I

    .line 1736272
    move v0, v0

    .line 1736273
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736268
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1736269
    move-object v0, v0

    .line 1736270
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1736259
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1736260
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1736261
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1736262
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1736263
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1736264
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1736265
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1736266
    invoke-static {v3, v9, v2}, Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/groups/members/protocol/FetchGroupMembersModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1736267
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1736256
    iget v0, p0, LX/1vt;->c:I

    .line 1736257
    move v0, v0

    .line 1736258
    return v0
.end method
