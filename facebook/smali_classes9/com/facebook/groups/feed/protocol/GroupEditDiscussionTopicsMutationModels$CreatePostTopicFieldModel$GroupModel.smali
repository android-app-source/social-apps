.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6748e118
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734384
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734385
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1734386
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1734387
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734396
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->e:Ljava/lang/String;

    .line 1734397
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1734388
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->f:Ljava/lang/String;

    .line 1734389
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1734390
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1734391
    if-eqz v0, :cond_0

    .line 1734392
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILjava/lang/String;)V

    .line 1734393
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734394
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->f:Ljava/lang/String;

    .line 1734395
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1734358
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734359
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1734360
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1734361
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1734362
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1734363
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1734364
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734365
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1734381
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734382
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734383
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1734380
    new-instance v0, LX/B0s;

    invoke-direct {v0, p1}, LX/B0s;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1734374
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1734375
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1734376
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1734377
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1734378
    :goto_0
    return-void

    .line 1734379
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1734371
    const-string v0, "name"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1734372
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;->a(Ljava/lang/String;)V

    .line 1734373
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1734368
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$GroupModel;-><init>()V

    .line 1734369
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1734370
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1734367
    const v0, 0xdb6caa1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1734366
    const v0, 0x41e065f

    return v0
.end method
