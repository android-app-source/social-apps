.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x225bd5e6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsRemovedModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735049
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735050
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1735051
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1735052
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735055
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    .line 1735056
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    return-object v0
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1735053
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->f:Ljava/util/List;

    .line 1735054
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsRemovedModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1735047
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsRemovedModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->g:Ljava/util/List;

    .line 1735048
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1735014
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735015
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1735016
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1735017
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1735018
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1735019
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1735020
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1735021
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1735022
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735023
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1735029
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735030
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1735031
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    .line 1735032
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1735033
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;

    .line 1735034
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    .line 1735035
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1735036
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1735037
    if-eqz v2, :cond_1

    .line 1735038
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;

    .line 1735039
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1735040
    :cond_1
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1735041
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->k()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1735042
    if-eqz v2, :cond_2

    .line 1735043
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;

    .line 1735044
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;->g:Ljava/util/List;

    move-object v1, v0

    .line 1735045
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735046
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1735026
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;-><init>()V

    .line 1735027
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1735028
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1735025
    const v0, -0x10caeafb

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1735024
    const v0, -0x3d9ca7bd

    return v0
.end method
