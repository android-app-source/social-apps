.class public final Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735864
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735863
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1735861
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1735862
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735859
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;->e:Ljava/lang/String;

    .line 1735860
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1735853
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735854
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1735855
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1735856
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1735857
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735858
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1735844
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735845
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735846
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735852
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1735849
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;-><init>()V

    .line 1735850
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1735851
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1735848
    const v0, 0x5e01b314

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1735847
    const v0, -0x6aa17dc

    return v0
.end method
