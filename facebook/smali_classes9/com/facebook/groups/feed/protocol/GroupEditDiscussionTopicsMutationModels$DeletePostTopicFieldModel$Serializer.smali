.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1734486
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1734487
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1734488
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1734489
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1734490
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1734491
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1734492
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1734493
    if-eqz p0, :cond_0

    .line 1734494
    const-string p2, "topic_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1734495
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1734496
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1734497
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1734498
    check-cast p1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$DeletePostTopicFieldModel;LX/0nX;LX/0my;)V

    return-void
.end method
