.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x283c15f3
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734968
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734944
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1734966
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1734967
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734964
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->e:Ljava/lang/String;

    .line 1734965
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734962
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->f:Ljava/lang/String;

    .line 1734963
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1734954
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734955
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1734956
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1734957
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1734958
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1734959
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1734960
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734961
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1734951
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734952
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734953
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734950
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1734947
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$TopicsAddedModel;-><init>()V

    .line 1734948
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1734949
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1734946
    const v0, 0x28ea6c63

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1734945
    const v0, 0xbc17cd0

    return v0
.end method
