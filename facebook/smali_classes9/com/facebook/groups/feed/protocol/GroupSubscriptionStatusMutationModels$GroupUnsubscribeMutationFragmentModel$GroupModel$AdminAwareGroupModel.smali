.class public final Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x6ca718f1
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735576
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735575
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1735573
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1735574
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 1735566
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1735567
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1735568
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1735569
    if-eqz v0, :cond_0

    .line 1735570
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1735571
    :cond_0
    return-void

    .line 1735572
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735564
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->e:Ljava/lang/String;

    .line 1735565
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735562
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1735563
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->f:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1735535
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735536
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1735537
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1735538
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1735539
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1735540
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1735541
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735542
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1735559
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735560
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735561
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1735558
    new-instance v0, LX/B19;

    invoke-direct {v0, p1}, LX/B19;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735557
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1735551
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1735552
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->k()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1735553
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1735554
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1735555
    :goto_0
    return-void

    .line 1735556
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1735548
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1735549
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 1735550
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1735545
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;-><init>()V

    .line 1735546
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1735547
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1735544
    const v0, 0xfaa0809

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1735543
    const v0, 0x41e065f

    return v0
.end method
