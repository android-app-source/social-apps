.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5bfcb573
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734801
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734800
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1734798
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1734799
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734796
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    .line 1734797
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    return-object v0
.end method

.method private j()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734802
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->f:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->f:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    .line 1734803
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->f:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1734788
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734789
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1734790
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->j()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1734791
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1734792
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1734793
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1734794
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734795
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1734775
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734776
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1734777
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    .line 1734778
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->a()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1734779
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;

    .line 1734780
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->e:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$GroupModel;

    .line 1734781
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->j()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1734782
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->j()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    .line 1734783
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->j()Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1734784
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;

    .line 1734785
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;->f:Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    .line 1734786
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734787
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1734772
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel;-><init>()V

    .line 1734773
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1734774
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1734770
    const v0, -0x531d7d17

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1734771
    const v0, -0x34d1142e    # -1.1463634E7f

    return v0
.end method
