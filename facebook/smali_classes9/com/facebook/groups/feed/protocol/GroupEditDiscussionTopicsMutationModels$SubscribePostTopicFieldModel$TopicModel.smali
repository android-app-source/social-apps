.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x33c8b9e7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734769
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734768
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1734766
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1734767
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734764
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->e:Ljava/lang/String;

    .line 1734765
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734762
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->f:Ljava/lang/String;

    .line 1734763
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734760
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    .line 1734761
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->g:Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1734750
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734751
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1734752
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1734753
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->l()Lcom/facebook/graphql/enums/GraphQLGroupPostTopicSubscriptionState;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1734754
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1734755
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1734756
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1734757
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1734758
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734759
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1734741
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734742
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734743
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734749
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1734746
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$SubscribePostTopicFieldModel$TopicModel;-><init>()V

    .line 1734747
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1734748
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1734745
    const v0, -0x706258b8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1734744
    const v0, 0xbc17cd0

    return v0
.end method
