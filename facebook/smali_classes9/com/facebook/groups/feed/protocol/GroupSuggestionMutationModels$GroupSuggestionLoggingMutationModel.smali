.class public final Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xb6a855e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735904
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735905
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1735906
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1735907
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735908
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;->e:Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;->e:Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    .line 1735909
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;->e:Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1735910
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735911
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;->a()Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1735912
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1735913
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1735914
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735915
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1735916
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735917
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;->a()Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1735918
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;->a()Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    .line 1735919
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;->a()Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1735920
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;

    .line 1735921
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;->e:Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel$AymtChannelModel;

    .line 1735922
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735923
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1735924
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupSuggestionMutationModels$GroupSuggestionLoggingMutationModel;-><init>()V

    .line 1735925
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1735926
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1735927
    const v0, 0x4215f0e4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1735928
    const v0, -0x4c62c188

    return v0
.end method
