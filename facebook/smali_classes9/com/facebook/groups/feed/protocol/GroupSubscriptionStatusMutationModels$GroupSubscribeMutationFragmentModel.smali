.class public final Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4dd1ef14
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735486
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735485
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1735483
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1735484
    return-void
.end method

.method private a()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735462
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;->e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;->e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    .line 1735463
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;->e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1735477
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735478
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;->a()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1735479
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1735480
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1735481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735482
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1735469
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735470
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;->a()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1735471
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;->a()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    .line 1735472
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;->a()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1735473
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;

    .line 1735474
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;->e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel$GroupModel;

    .line 1735475
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735476
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1735466
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupSubscribeMutationFragmentModel;-><init>()V

    .line 1735467
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1735468
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1735465
    const v0, 0x265caaa1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1735464
    const v0, 0x60c964e2

    return v0
.end method
