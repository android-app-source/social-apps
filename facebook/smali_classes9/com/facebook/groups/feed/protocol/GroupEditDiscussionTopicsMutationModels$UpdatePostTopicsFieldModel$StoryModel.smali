.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734912
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1734913
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1734922
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1734923
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734914
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;->e:Ljava/lang/String;

    .line 1734915
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1734916
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734917
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1734918
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1734919
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1734920
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734921
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1734908
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1734909
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1734910
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1734911
    new-instance v0, LX/B0u;

    invoke-direct {v0, p1}, LX/B0u;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1734907
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1734905
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1734906
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1734904
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1734901
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$StoryModel;-><init>()V

    .line 1734902
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1734903
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1734900
    const v0, 0x7df328ad

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1734899
    const v0, 0x4c808d5

    return v0
.end method
