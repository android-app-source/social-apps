.class public Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/B1T;

.field private final c:Ljava/lang/String;

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1736019
    new-instance v0, LX/B1Q;

    invoke-direct {v0}, LX/B1Q;-><init>()V

    sput-object v0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1736020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1736021
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->a:Ljava/lang/String;

    .line 1736022
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/B1T;->valueOf(Ljava/lang/String;)LX/B1T;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->b:LX/B1T;

    .line 1736023
    iput-object v2, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->d:Ljava/util/List;

    .line 1736024
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->c:Ljava/lang/String;

    .line 1736025
    iput-object v2, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->f:Ljava/lang/String;

    .line 1736026
    iput-object v2, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->g:Ljava/lang/String;

    .line 1736027
    iput-object v2, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->h:Ljava/lang/String;

    .line 1736028
    iput-object v2, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->i:Ljava/lang/String;

    .line 1736029
    iput-object v2, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->e:Ljava/util/List;

    .line 1736030
    const/4 v0, 0x2

    iput v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->j:I

    .line 1736031
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/B1T;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/B1T;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1736032
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1736033
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->a:Ljava/lang/String;

    .line 1736034
    iput-object p2, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->b:LX/B1T;

    .line 1736035
    iput-object p3, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->d:Ljava/util/List;

    .line 1736036
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->c:Ljava/lang/String;

    .line 1736037
    iput-object p4, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->f:Ljava/lang/String;

    .line 1736038
    iput-object p5, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->g:Ljava/lang/String;

    .line 1736039
    iput-object p6, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->h:Ljava/lang/String;

    .line 1736040
    iput-object p7, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->i:Ljava/lang/String;

    .line 1736041
    iput-object p8, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->e:Ljava/util/List;

    .line 1736042
    iput p9, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->j:I

    .line 1736043
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1736044
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->b:LX/B1T;

    sget-object v1, LX/B1T;->GroupsStories:LX/B1T;

    if-ne v0, v1, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->b:LX/B1T;

    invoke-virtual {v1}, LX/B1T;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1736045
    const/4 v0, 0x0

    return v0
.end method

.method public final g()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736046
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->d:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1736047
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->d:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 1736048
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1736049
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1736050
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1736051
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupsFeedTypeValueParams;->b:LX/B1T;

    invoke-virtual {v0}, LX/B1T;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1736052
    return-void
.end method
