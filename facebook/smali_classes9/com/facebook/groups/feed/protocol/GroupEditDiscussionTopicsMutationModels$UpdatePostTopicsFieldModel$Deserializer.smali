.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1734804
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1734805
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1734806
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1734807
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1734808
    const/4 v2, 0x0

    .line 1734809
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 1734810
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1734811
    :goto_0
    move v1, v2

    .line 1734812
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1734813
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1734814
    new-instance v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$UpdatePostTopicsFieldModel;-><init>()V

    .line 1734815
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1734816
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1734817
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1734818
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1734819
    :cond_0
    return-object v1

    .line 1734820
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1734821
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_7

    .line 1734822
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1734823
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1734824
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v5, :cond_2

    .line 1734825
    const-string p0, "story"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1734826
    invoke-static {p1, v0}, LX/B10;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 1734827
    :cond_3
    const-string p0, "topics_added"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1734828
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1734829
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, p0, :cond_4

    .line 1734830
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, p0, :cond_4

    .line 1734831
    invoke-static {p1, v0}, LX/B11;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1734832
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1734833
    :cond_4
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1734834
    goto :goto_1

    .line 1734835
    :cond_5
    const-string p0, "topics_removed"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1734836
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1734837
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, p0, :cond_6

    .line 1734838
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, p0, :cond_6

    .line 1734839
    invoke-static {p1, v0}, LX/B12;->b(LX/15w;LX/186;)I

    move-result v5

    .line 1734840
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1734841
    :cond_6
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1734842
    goto/16 :goto_1

    .line 1734843
    :cond_7
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1734844
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1734845
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1734846
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1734847
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    goto/16 :goto_1
.end method
