.class public final Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1734398
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel;

    new-instance v1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1734399
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1734400
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1734401
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1734402
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1734403
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1734404
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1734405
    if-eqz v2, :cond_0

    .line 1734406
    const-string p0, "group"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1734407
    invoke-static {v1, v2, p1}, LX/B0v;->a(LX/15i;ILX/0nX;)V

    .line 1734408
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1734409
    if-eqz v2, :cond_1

    .line 1734410
    const-string p0, "topic"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1734411
    invoke-static {v1, v2, p1}, LX/B0w;->a(LX/15i;ILX/0nX;)V

    .line 1734412
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1734413
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1734414
    check-cast p1, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel$Serializer;->a(Lcom/facebook/groups/feed/protocol/GroupEditDiscussionTopicsMutationModels$CreatePostTopicFieldModel;LX/0nX;LX/0my;)V

    return-void
.end method
