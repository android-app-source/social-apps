.class public final Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x17c77b3d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735649
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1735648
    const-class v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1735646
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1735647
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V
    .locals 4

    .prologue
    .line 1735639
    iput-object p1, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1735640
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1735641
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1735642
    if-eqz v0, :cond_0

    .line 1735643
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x2

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1735644
    :cond_0
    return-void

    .line 1735645
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735637
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    .line 1735638
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735599
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->f:Ljava/lang/String;

    .line 1735600
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735635
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iput-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 1735636
    iget-object v0, p0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->g:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1735625
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735626
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->j()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1735627
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1735628
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1735629
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1735630
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1735631
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1735632
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1735633
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735634
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1735617
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1735618
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->j()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1735619
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->j()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    .line 1735620
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->j()Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1735621
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;

    .line 1735622
    iput-object v0, v1, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->e:Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel$AdminAwareGroupModel;

    .line 1735623
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1735624
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1735616
    new-instance v0, LX/B1A;

    invoke-direct {v0, p1}, LX/B1A;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1735615
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1735609
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1735610
    invoke-direct {p0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->l()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1735611
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1735612
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    .line 1735613
    :goto_0
    return-void

    .line 1735614
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1735606
    const-string v0, "subscribe_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1735607
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-direct {p0, p2}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 1735608
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1735603
    new-instance v0, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/feed/protocol/GroupSubscriptionStatusMutationModels$GroupUnsubscribeMutationFragmentModel$GroupModel;-><init>()V

    .line 1735604
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1735605
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1735602
    const v0, -0x4d25891a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1735601
    const v0, 0x41e065f

    return v0
.end method
