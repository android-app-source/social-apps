.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x5903e444
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737306
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737283
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1737304
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1737305
    return-void
.end method

.method private a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1737302
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    .line 1737303
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1737296
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737297
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1737298
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1737299
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1737300
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737301
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1737288
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737289
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1737290
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    .line 1737291
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1737292
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;

    .line 1737293
    iput-object v0, v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    .line 1737294
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737295
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1737285
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;-><init>()V

    .line 1737286
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1737287
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1737284
    const v0, 0x4d086e28

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1737282
    const v0, -0x5e1878ce

    return v0
.end method
