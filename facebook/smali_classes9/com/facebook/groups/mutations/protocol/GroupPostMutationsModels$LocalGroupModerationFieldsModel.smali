.class public final Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x52bb3acb
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1739025
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1739026
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1739027
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1739028
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1739029
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1739030
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1739031
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1739032
    iput-boolean p1, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->f:Z

    .line 1739033
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1739034
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1739035
    if-eqz v0, :cond_0

    .line 1739036
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1739037
    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1739038
    iput-boolean p1, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->g:Z

    .line 1739039
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1739040
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1739041
    if-eqz v0, :cond_0

    .line 1739042
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1739043
    :cond_0
    return-void
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 1739044
    iput-boolean p1, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->h:Z

    .line 1739045
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1739046
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1739047
    if-eqz v0, :cond_0

    .line 1739048
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1739049
    :cond_0
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    .line 1739050
    iput-boolean p1, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->i:Z

    .line 1739051
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1739052
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1739053
    if-eqz v0, :cond_0

    .line 1739054
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1739055
    :cond_0
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1739056
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->e:Ljava/lang/String;

    .line 1739057
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 1739023
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1739024
    iget-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->f:Z

    return v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 1738968
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1738969
    iget-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->g:Z

    return v0
.end method

.method private m()Z
    .locals 2

    .prologue
    .line 1739058
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1739059
    iget-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->h:Z

    return v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 1738966
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1738967
    iget-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->i:Z

    return v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1738970
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1738971
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1738972
    const/4 v1, 0x5

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1738973
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1738974
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1738975
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->g:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1738976
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1738977
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1738978
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1738979
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1738980
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1738981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1738982
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1738983
    new-instance v0, LX/B2j;

    invoke-direct {v0, p1}, LX/B2j;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1738984
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1738985
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1738986
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->f:Z

    .line 1738987
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->g:Z

    .line 1738988
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->h:Z

    .line 1738989
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->i:Z

    .line 1738990
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1738991
    const-string v0, "local_group_did_approve"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1738992
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->k()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1738993
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1738994
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1738995
    :goto_0
    return-void

    .line 1738996
    :cond_0
    const-string v0, "local_group_did_ignore_report"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1738997
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->l()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1738998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1738999
    const/4 v0, 0x2

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1739000
    :cond_1
    const-string v0, "local_group_did_pin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1739001
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->m()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1739002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1739003
    const/4 v0, 0x3

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1739004
    :cond_2
    const-string v0, "local_group_did_unpin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1739005
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->n()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1739006
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1739007
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    goto :goto_0

    .line 1739008
    :cond_3
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1739009
    const-string v0, "local_group_did_approve"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1739010
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->a(Z)V

    .line 1739011
    :cond_0
    :goto_0
    return-void

    .line 1739012
    :cond_1
    const-string v0, "local_group_did_ignore_report"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1739013
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->b(Z)V

    goto :goto_0

    .line 1739014
    :cond_2
    const-string v0, "local_group_did_pin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1739015
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->c(Z)V

    goto :goto_0

    .line 1739016
    :cond_3
    const-string v0, "local_group_did_unpin"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739017
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;->d(Z)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1739018
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;-><init>()V

    .line 1739019
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1739020
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1739021
    const v0, -0x7715dad2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1739022
    const v0, 0x4c808d5

    return v0
.end method
