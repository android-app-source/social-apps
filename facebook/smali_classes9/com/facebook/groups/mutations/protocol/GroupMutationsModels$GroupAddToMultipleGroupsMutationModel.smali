.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6e611bf9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AddedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AlreadyAddedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AlreadyInvitedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$FailedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$InvitedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$RequestedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737792
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737791
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1737789
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1737790
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AddedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1737787
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AddedGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->e:Ljava/util/List;

    .line 1737788
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AlreadyAddedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1737785
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AlreadyAddedGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->f:Ljava/util/List;

    .line 1737786
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AlreadyInvitedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1737783
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$AlreadyInvitedGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->g:Ljava/util/List;

    .line 1737784
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private l()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$FailedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1737781
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->h:Ljava/util/List;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$FailedGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->h:Ljava/util/List;

    .line 1737782
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->h:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private m()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$InvitedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1737723
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$InvitedGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->i:Ljava/util/List;

    .line 1737724
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private n()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$RequestedGroupsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1737779
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->j:Ljava/util/List;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$RequestedGroupsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->j:Ljava/util/List;

    .line 1737780
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->j:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1737763
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737764
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1737765
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1737766
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->k()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1737767
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->l()LX/0Px;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v3

    .line 1737768
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->m()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1737769
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->n()LX/0Px;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1737770
    const/4 v6, 0x6

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 1737771
    const/4 v6, 0x0

    invoke-virtual {p1, v6, v0}, LX/186;->b(II)V

    .line 1737772
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1737773
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1737774
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1737775
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1737776
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1737777
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737778
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1737730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737731
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1737732
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1737733
    if-eqz v1, :cond_0

    .line 1737734
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    .line 1737735
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->e:Ljava/util/List;

    .line 1737736
    :cond_0
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1737737
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1737738
    if-eqz v1, :cond_1

    .line 1737739
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    .line 1737740
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->f:Ljava/util/List;

    .line 1737741
    :cond_1
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->k()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1737742
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->k()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1737743
    if-eqz v1, :cond_2

    .line 1737744
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    .line 1737745
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->g:Ljava/util/List;

    .line 1737746
    :cond_2
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->l()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1737747
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->l()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1737748
    if-eqz v1, :cond_3

    .line 1737749
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    .line 1737750
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->h:Ljava/util/List;

    .line 1737751
    :cond_3
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->m()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1737752
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->m()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1737753
    if-eqz v1, :cond_4

    .line 1737754
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    .line 1737755
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->i:Ljava/util/List;

    .line 1737756
    :cond_4
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->n()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 1737757
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->n()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1737758
    if-eqz v1, :cond_5

    .line 1737759
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    .line 1737760
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;->j:Ljava/util/List;

    .line 1737761
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737762
    if-nez v0, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1737727
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;-><init>()V

    .line 1737728
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1737729
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1737726
    const v0, -0x5a223aea

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1737725
    const v0, 0x1c45468c

    return v0
.end method
