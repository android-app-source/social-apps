.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1737269
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;

    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1737270
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1737281
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1737272
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1737273
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1737274
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1737275
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1737276
    if-eqz v2, :cond_0

    .line 1737277
    const-string p0, "group"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1737278
    invoke-static {v1, v2, p1}, LX/B2Q;->a(LX/15i;ILX/0nX;)V

    .line 1737279
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1737280
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1737271
    check-cast p1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$Serializer;->a(Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
