.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x75704ad4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737112
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737140
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1737138
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1737139
    return-void
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1737136
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;->e:Ljava/lang/String;

    .line 1737137
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1737129
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737130
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1737131
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1737132
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1737133
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;->f:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1737134
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737135
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1737126
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737127
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737128
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1737125
    new-instance v0, LX/B2B;

    invoke-direct {v0, p1}, LX/B2B;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1737124
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1737121
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1737122
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;->f:Z

    .line 1737123
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1737119
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1737120
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1737118
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1737115
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;-><init>()V

    .line 1737116
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1737117
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1737114
    const v0, -0x60488e35

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1737113
    const v0, 0x41e065f

    return v0
.end method
