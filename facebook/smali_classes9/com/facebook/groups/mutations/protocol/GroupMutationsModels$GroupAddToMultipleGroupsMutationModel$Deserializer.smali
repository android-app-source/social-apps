.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1737448
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1737449
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1737450
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1737451
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1737452
    const/4 v2, 0x0

    .line 1737453
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_f

    .line 1737454
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1737455
    :goto_0
    move v1, v2

    .line 1737456
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1737457
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1737458
    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    invoke-direct {v1}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;-><init>()V

    .line 1737459
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1737460
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1737461
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1737462
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1737463
    :cond_0
    return-object v1

    .line 1737464
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1737465
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_e

    .line 1737466
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1737467
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1737468
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v8, :cond_2

    .line 1737469
    const-string p0, "added_groups"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1737470
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 1737471
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, p0, :cond_3

    .line 1737472
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, p0, :cond_3

    .line 1737473
    invoke-static {p1, v0}, LX/B2R;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1737474
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1737475
    :cond_3
    invoke-static {v7, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v7

    move v7, v7

    .line 1737476
    goto :goto_1

    .line 1737477
    :cond_4
    const-string p0, "already_added_groups"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1737478
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1737479
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, p0, :cond_5

    .line 1737480
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, p0, :cond_5

    .line 1737481
    invoke-static {p1, v0}, LX/B2S;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1737482
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1737483
    :cond_5
    invoke-static {v6, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1737484
    goto :goto_1

    .line 1737485
    :cond_6
    const-string p0, "already_invited_groups"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1737486
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1737487
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, p0, :cond_7

    .line 1737488
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, p0, :cond_7

    .line 1737489
    invoke-static {p1, v0}, LX/B2T;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1737490
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1737491
    :cond_7
    invoke-static {v5, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v5

    move v5, v5

    .line 1737492
    goto/16 :goto_1

    .line 1737493
    :cond_8
    const-string p0, "failed_groups"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_a

    .line 1737494
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1737495
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, p0, :cond_9

    .line 1737496
    :goto_5
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, p0, :cond_9

    .line 1737497
    invoke-static {p1, v0}, LX/B2U;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1737498
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 1737499
    :cond_9
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1737500
    goto/16 :goto_1

    .line 1737501
    :cond_a
    const-string p0, "invited_groups"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_c

    .line 1737502
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1737503
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, p0, :cond_b

    .line 1737504
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, p0, :cond_b

    .line 1737505
    invoke-static {p1, v0}, LX/B2V;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1737506
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 1737507
    :cond_b
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1737508
    goto/16 :goto_1

    .line 1737509
    :cond_c
    const-string p0, "requested_groups"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1737510
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1737511
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, p0, :cond_d

    .line 1737512
    :goto_7
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, p0, :cond_d

    .line 1737513
    invoke-static {p1, v0}, LX/B2W;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1737514
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 1737515
    :cond_d
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1737516
    goto/16 :goto_1

    .line 1737517
    :cond_e
    const/4 v8, 0x6

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1737518
    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 1737519
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 1737520
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v5}, LX/186;->b(II)V

    .line 1737521
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1737522
    const/4 v2, 0x4

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1737523
    const/4 v2, 0x5

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1737524
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_f
    move v1, v2

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    goto/16 :goto_1
.end method
