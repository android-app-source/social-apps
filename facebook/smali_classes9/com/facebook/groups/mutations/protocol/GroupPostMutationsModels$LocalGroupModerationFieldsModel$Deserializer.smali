.class public final Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1738893
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1738894
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1738895
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 1738896
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1738897
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1738898
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_c

    .line 1738899
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1738900
    :goto_0
    move v1, v2

    .line 1738901
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1738902
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1738903
    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    invoke-direct {v1}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;-><init>()V

    .line 1738904
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1738905
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1738906
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1738907
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1738908
    :cond_0
    return-object v1

    .line 1738909
    :cond_1
    const-string p0, "local_group_did_approve"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 1738910
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v6

    move v10, v6

    move v6, v3

    .line 1738911
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v12

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v12, p0, :cond_7

    .line 1738912
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v12

    .line 1738913
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1738914
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v12, :cond_2

    .line 1738915
    const-string p0, "id"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 1738916
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 1738917
    :cond_3
    const-string p0, "local_group_did_ignore_report"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 1738918
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v5

    move v9, v5

    move v5, v3

    goto :goto_1

    .line 1738919
    :cond_4
    const-string p0, "local_group_did_pin"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1738920
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v4

    move v8, v4

    move v4, v3

    goto :goto_1

    .line 1738921
    :cond_5
    const-string p0, "local_group_did_unpin"

    invoke-virtual {v12, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_6

    .line 1738922
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v7, v1

    move v1, v3

    goto :goto_1

    .line 1738923
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 1738924
    :cond_7
    const/4 v12, 0x5

    invoke-virtual {v0, v12}, LX/186;->c(I)V

    .line 1738925
    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 1738926
    if-eqz v6, :cond_8

    .line 1738927
    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 1738928
    :cond_8
    if-eqz v5, :cond_9

    .line 1738929
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 1738930
    :cond_9
    if-eqz v4, :cond_a

    .line 1738931
    const/4 v2, 0x3

    invoke-virtual {v0, v2, v8}, LX/186;->a(IZ)V

    .line 1738932
    :cond_a
    if-eqz v1, :cond_b

    .line 1738933
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 1738934
    :cond_b
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_c
    move v1, v2

    move v4, v2

    move v5, v2

    move v6, v2

    move v7, v2

    move v8, v2

    move v9, v2

    move v10, v2

    move v11, v2

    goto/16 :goto_1
.end method
