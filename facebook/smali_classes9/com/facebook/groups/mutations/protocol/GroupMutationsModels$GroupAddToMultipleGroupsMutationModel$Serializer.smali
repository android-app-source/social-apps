.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1737721
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1737722
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1737720
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1737666
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1737667
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1737668
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1737669
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1737670
    if-eqz v2, :cond_1

    .line 1737671
    const-string v3, "added_groups"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1737672
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1737673
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 1737674
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/B2R;->a(LX/15i;ILX/0nX;)V

    .line 1737675
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1737676
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1737677
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1737678
    if-eqz v2, :cond_3

    .line 1737679
    const-string v3, "already_added_groups"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1737680
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1737681
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_2

    .line 1737682
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/B2S;->a(LX/15i;ILX/0nX;)V

    .line 1737683
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1737684
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1737685
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1737686
    if-eqz v2, :cond_5

    .line 1737687
    const-string v3, "already_invited_groups"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1737688
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1737689
    const/4 v3, 0x0

    :goto_2
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_4

    .line 1737690
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/B2T;->a(LX/15i;ILX/0nX;)V

    .line 1737691
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1737692
    :cond_4
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1737693
    :cond_5
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1737694
    if-eqz v2, :cond_7

    .line 1737695
    const-string v3, "failed_groups"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1737696
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1737697
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_6

    .line 1737698
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/B2U;->a(LX/15i;ILX/0nX;)V

    .line 1737699
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1737700
    :cond_6
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1737701
    :cond_7
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1737702
    if-eqz v2, :cond_9

    .line 1737703
    const-string v3, "invited_groups"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1737704
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1737705
    const/4 v3, 0x0

    :goto_4
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_8

    .line 1737706
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/B2V;->a(LX/15i;ILX/0nX;)V

    .line 1737707
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 1737708
    :cond_8
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1737709
    :cond_9
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1737710
    if-eqz v2, :cond_b

    .line 1737711
    const-string v3, "requested_groups"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1737712
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1737713
    const/4 v3, 0x0

    :goto_5
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_a

    .line 1737714
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1}, LX/B2W;->a(LX/15i;ILX/0nX;)V

    .line 1737715
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 1737716
    :cond_a
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1737717
    :cond_b
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1737718
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1737719
    check-cast p1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel$Serializer;->a(Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAddToMultipleGroupsMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
