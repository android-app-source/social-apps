.class public final Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1738739
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel;

    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1738740
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1738728
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1738730
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1738731
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1738732
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1738733
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1738734
    if-eqz v2, :cond_0

    .line 1738735
    const-string p0, "group"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1738736
    invoke-static {v1, v2, p1}, LX/B2m;->a(LX/15i;ILX/0nX;)V

    .line 1738737
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1738738
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1738729
    check-cast p1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel$Serializer;->a(Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupPinStoryMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
