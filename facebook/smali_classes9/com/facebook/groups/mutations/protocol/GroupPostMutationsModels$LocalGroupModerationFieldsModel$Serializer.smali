.class public final Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1738964
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    new-instance v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1738965
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1738937
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1738939
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1738940
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1738941
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1738942
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1738943
    if-eqz p0, :cond_0

    .line 1738944
    const-string p2, "id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1738945
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1738946
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1738947
    if-eqz p0, :cond_1

    .line 1738948
    const-string p2, "local_group_did_approve"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1738949
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1738950
    :cond_1
    const/4 p0, 0x2

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1738951
    if-eqz p0, :cond_2

    .line 1738952
    const-string p2, "local_group_did_ignore_report"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1738953
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1738954
    :cond_2
    const/4 p0, 0x3

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1738955
    if-eqz p0, :cond_3

    .line 1738956
    const-string p2, "local_group_did_pin"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1738957
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1738958
    :cond_3
    const/4 p0, 0x4

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1738959
    if-eqz p0, :cond_4

    .line 1738960
    const-string p2, "local_group_did_unpin"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1738961
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1738962
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1738963
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1738938
    check-cast p1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel$Serializer;->a(Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$LocalGroupModerationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
