.class public final Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x206d7aa8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1738654
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1738653
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1738630
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1738631
    return-void
.end method

.method private a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1738651
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    .line 1738652
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1738645
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1738646
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1738647
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1738648
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1738649
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1738650
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1738637
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1738638
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1738639
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    .line 1738640
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1738641
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;

    .line 1738642
    iput-object v0, v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel$GroupModel;

    .line 1738643
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1738644
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1738634
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupApprovePendingStoryMutationModel;-><init>()V

    .line 1738635
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1738636
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1738633
    const v0, -0x4e3c69c0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1738632
    const v0, -0x7cc72c3f

    return v0
.end method
