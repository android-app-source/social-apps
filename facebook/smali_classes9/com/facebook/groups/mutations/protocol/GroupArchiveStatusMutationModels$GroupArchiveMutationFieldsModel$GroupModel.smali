.class public final Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x8b75501
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1736762
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1736761
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1736759
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1736760
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1736756
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1736757
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1736758
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1736750
    iput-wide p1, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->e:J

    .line 1736751
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1736752
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1736753
    if-eqz v0, :cond_0

    .line 1736754
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 1736755
    :cond_0
    return-void
.end method

.method private j()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1736748
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1736749
    iget-wide v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->e:J

    return-wide v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736746
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->f:Ljava/lang/String;

    .line 1736747
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1736739
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1736740
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1736741
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1736742
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1736743
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1736744
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1736745
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1736763
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1736764
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1736765
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1736738
    new-instance v0, LX/B1v;

    invoke-direct {v0, p1}, LX/B1v;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736737
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1736734
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1736735
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->e:J

    .line 1736736
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1736728
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1736729
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1736730
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1736731
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1736732
    :goto_0
    return-void

    .line 1736733
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1736720
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1736721
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;->a(J)V

    .line 1736722
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1736725
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel$GroupModel;-><init>()V

    .line 1736726
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1736727
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1736724
    const v0, -0x48693d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1736723
    const v0, 0x41e065f

    return v0
.end method
