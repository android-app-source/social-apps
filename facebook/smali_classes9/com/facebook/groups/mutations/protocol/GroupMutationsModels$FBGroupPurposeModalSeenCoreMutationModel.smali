.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x448c4118
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737154
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737176
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1737177
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1737178
    return-void
.end method

.method private a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1737168
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    .line 1737169
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1737170
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737171
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1737172
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1737173
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1737174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737175
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1737160
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737161
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1737162
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    .line 1737163
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1737164
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;

    .line 1737165
    iput-object v0, v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel$GroupModel;

    .line 1737166
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737167
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1737157
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$FBGroupPurposeModalSeenCoreMutationModel;-><init>()V

    .line 1737158
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1737159
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1737156
    const v0, 0x64b56a2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1737155
    const v0, -0x18bc0bca

    return v0
.end method
