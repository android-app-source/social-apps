.class public final Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x8b75501
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:J

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1736857
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1736902
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1736900
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1736901
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1736897
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1736898
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1736899
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 1736891
    iput-wide p1, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->e:J

    .line 1736892
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1736893
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1736894
    if-eqz v0, :cond_0

    .line 1736895
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1, p2}, LX/15i;->b(IIJ)V

    .line 1736896
    :cond_0
    return-void
.end method

.method private j()J
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1736889
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1736890
    iget-wide v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->e:J

    return-wide v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736887
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->f:Ljava/lang/String;

    .line 1736888
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->f:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1736880
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1736881
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1736882
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1736883
    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->e:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1736884
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1736885
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1736886
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1736877
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1736878
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1736879
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1736876
    new-instance v0, LX/B1y;

    invoke-direct {v0, p1}, LX/B1y;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1736875
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1736872
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1736873
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->e:J

    .line 1736874
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1736866
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1736867
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1736868
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1736869
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1736870
    :goto_0
    return-void

    .line 1736871
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1736863
    const-string v0, "archived_time"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1736864
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;->a(J)V

    .line 1736865
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1736860
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel$GroupModel;-><init>()V

    .line 1736861
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1736862
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1736859
    const v0, -0x73852d66

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1736858
    const v0, 0x41e065f

    return v0
.end method
