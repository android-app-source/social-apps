.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x60154346
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737255
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1737265
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1737263
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1737264
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V
    .locals 4

    .prologue
    .line 1737256
    iput-object p1, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1737257
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1737258
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1737259
    if-eqz v0, :cond_0

    .line 1737260
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1737261
    :cond_0
    return-void

    .line 1737262
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1737253
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->e:Ljava/lang/String;

    .line 1737254
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1737251
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 1737252
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->f:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1737243
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737244
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1737245
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1737246
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1737247
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1737248
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1737249
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737250
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1737266
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1737267
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1737268
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1737242
    new-instance v0, LX/B2C;

    invoke-direct {v0, p1}, LX/B2C;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1737241
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1737235
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1737236
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1737237
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1737238
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1737239
    :goto_0
    return-void

    .line 1737240
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1737227
    const-string v0, "viewer_join_state"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1737228
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    invoke-direct {p0, p2}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupJoinState;)V

    .line 1737229
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1737232
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupAcceptInvitationToJoinMutationModel$GroupModel;-><init>()V

    .line 1737233
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1737234
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1737231
    const v0, -0x5d07e7d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1737230
    const v0, 0x41e065f

    return v0
.end method
