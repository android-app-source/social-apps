.class public final Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x500be17e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1738873
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1738876
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1738874
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1738875
    return-void
.end method

.method private a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1738865
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    .line 1738866
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1738867
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1738868
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1738869
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1738870
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1738871
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1738872
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1738855
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1738856
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1738857
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    .line 1738858
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1738859
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;

    .line 1738860
    iput-object v0, v1, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel$GroupModel;

    .line 1738861
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1738862
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1738852
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupPostMutationsModels$GroupUnpinStoryMutationModel;-><init>()V

    .line 1738853
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1738854
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1738864
    const v0, -0x531c95f4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1738863
    const v0, 0x2b83135

    return v0
.end method
