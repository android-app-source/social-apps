.class public final Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1598e81c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1738111
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1738110
    const-class v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1738108
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1738109
    return-void
.end method

.method private a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1738087
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    iput-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    .line 1738088
    iget-object v0, p0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1738102
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1738103
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1738104
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1738105
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1738106
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1738107
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1738094
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1738095
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1738096
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    .line 1738097
    invoke-direct {p0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;->a()Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1738098
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;

    .line 1738099
    iput-object v0, v1, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;->e:Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel$GroupModel;

    .line 1738100
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1738101
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1738091
    new-instance v0, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;

    invoke-direct {v0}, Lcom/facebook/groups/mutations/protocol/GroupMutationsModels$GroupRecordNotificationNuxDisplayCoreMutationModel;-><init>()V

    .line 1738092
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1738093
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1738090
    const v0, 0x245536ee

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1738089
    const v0, 0x4751c581

    return v0
.end method
