.class public final Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x28535e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924988
    const-class v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924987
    const-class v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1924985
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1924986
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924983
    iget-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel;->e:Ljava/lang/String;

    .line 1924984
    iget-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1924977
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924978
    invoke-direct {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1924979
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1924980
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1924981
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924982
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1924969
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924970
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924971
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1924974
    new-instance v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel;

    invoke-direct {v0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravityUnblacklistFeedbackMutationModel;-><init>()V

    .line 1924975
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1924976
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1924973
    const v0, 0x33b9fedc

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1924972
    const v0, 0x100f79ca

    return v0
.end method
