.class public final Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xce3ec26
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924929
    const-class v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924905
    const-class v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1924927
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1924928
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1924921
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924922
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1924923
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1924924
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1924925
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924926
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1924913
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924914
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1924915
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 1924916
    invoke-virtual {p0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1924917
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;

    .line 1924918
    iput-object v0, v1, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 1924919
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924920
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924911
    iget-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    iput-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    .line 1924912
    iget-object v0, p0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;->e:Lcom/facebook/placetips/settings/graphql/GravitySettingsGraphQlFragmentModels$GravitySettingsGraphQlFragmentModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1924908
    new-instance v0, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;

    invoke-direct {v0}, Lcom/facebook/placetips/settings/graphql/GravitySettingsMutationModels$GravitySettingsMutationModel;-><init>()V

    .line 1924909
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1924910
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1924907
    const v0, -0x5e69254b

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1924906
    const v0, 0x3611e341

    return v0
.end method
