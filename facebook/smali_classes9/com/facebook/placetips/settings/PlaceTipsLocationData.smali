.class public Lcom/facebook/placetips/settings/PlaceTipsLocationData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/placetips/settings/PlaceTipsLocationData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field public b:I

.field public c:D

.field public d:D

.field public e:D

.field public f:I

.field public g:D


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1924580
    new-instance v0, LX/CeO;

    invoke-direct {v0}, LX/CeO;-><init>()V

    sput-object v0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const-wide/16 v0, 0x0

    .line 1924613
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1924614
    iput v2, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->a:I

    .line 1924615
    iput v2, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->b:I

    .line 1924616
    iput-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->c:D

    .line 1924617
    iput-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->d:D

    .line 1924618
    iput-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->e:D

    .line 1924619
    iput v2, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->f:I

    .line 1924620
    iput-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->g:D

    .line 1924621
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1924604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1924605
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->a:I

    .line 1924606
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->b:I

    .line 1924607
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->c:D

    .line 1924608
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->d:D

    .line 1924609
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->e:D

    .line 1924610
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->f:I

    .line 1924611
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->g:D

    .line 1924612
    return-void
.end method


# virtual methods
.method public final a(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;
    .locals 1

    .prologue
    .line 1924602
    iput-wide p1, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->c:D

    .line 1924603
    return-object p0
.end method

.method public final a(I)Lcom/facebook/placetips/settings/PlaceTipsLocationData;
    .locals 0

    .prologue
    .line 1924600
    iput p1, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->a:I

    .line 1924601
    return-object p0
.end method

.method public final b(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;
    .locals 1

    .prologue
    .line 1924598
    iput-wide p1, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->d:D

    .line 1924599
    return-object p0
.end method

.method public final b(I)Lcom/facebook/placetips/settings/PlaceTipsLocationData;
    .locals 0

    .prologue
    .line 1924596
    iput p1, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->b:I

    .line 1924597
    return-object p0
.end method

.method public final c(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;
    .locals 1

    .prologue
    .line 1924594
    iput-wide p1, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->e:D

    .line 1924595
    return-object p0
.end method

.method public final c(I)Lcom/facebook/placetips/settings/PlaceTipsLocationData;
    .locals 0

    .prologue
    .line 1924592
    iput p1, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->f:I

    .line 1924593
    return-object p0
.end method

.method public final d(D)Lcom/facebook/placetips/settings/PlaceTipsLocationData;
    .locals 1

    .prologue
    .line 1924590
    iput-wide p1, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->g:D

    .line 1924591
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1924589
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1924581
    iget v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1924582
    iget v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1924583
    iget-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->c:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1924584
    iget-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->d:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1924585
    iget-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->e:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1924586
    iget v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1924587
    iget-wide v0, p0, Lcom/facebook/placetips/settings/PlaceTipsLocationData;->g:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 1924588
    return-void
.end method
