.class public Lcom/facebook/placetips/bootstrap/PresenceDescriptionSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/placetips/bootstrap/PresenceDescription;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1923309
    const-class v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    new-instance v1, Lcom/facebook/placetips/bootstrap/PresenceDescriptionSerializer;

    invoke-direct {v1}, Lcom/facebook/placetips/bootstrap/PresenceDescriptionSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1923310
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1923308
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1923311
    if-nez p0, :cond_0

    .line 1923312
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1923313
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1923314
    invoke-static {p0, p1, p2}, Lcom/facebook/placetips/bootstrap/PresenceDescriptionSerializer;->b(Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/0nX;LX/0my;)V

    .line 1923315
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1923316
    return-void
.end method

.method private static b(Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1923290
    const-string v0, "page_id"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1923291
    const-string v0, "page_name"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageName:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1923292
    const-string v0, "presence_acquired_at"

    iget-wide v2, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPresenceAcquiredAt:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1923293
    const-string v0, "presence_last_seen_at"

    iget-wide v2, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPulsarLastSeenAt:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1923294
    const-string v0, "feed_unit_header_styled"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitHeaderStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923295
    const-string v0, "feed_unit_subtitle_styled"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitSubtitleStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923296
    const-string v0, "feed_unit_show_suggestifier_footer"

    iget-boolean v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitShowSuggestifierFooter:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1923297
    const-string v0, "suggestifier_footer_question"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterQuestion:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923298
    const-string v0, "suggestifier_footer_description"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterDescription:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923299
    const-string v0, "suggestifier_footer_thank_you_text"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterThankYouText:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923300
    const-string v0, "presence_source"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSource:Lcom/facebook/placetips/bootstrap/PresenceSource;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923301
    const-string v0, "place_tip_welcome_header"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPlaceTipWelcomeHeader:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923302
    const-string v0, "reaction_stories"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mReactionStories:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923303
    const-string v0, "suggestifier_response_id"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierResponseId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1923304
    const-string v0, "existing_reaction_session_id"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mExistingReactionSessionId:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1923305
    const-string v0, "confidence_level"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mConfidenceLevel:LX/Cdj;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923306
    const-string v0, "page_categories"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageCategoryNames:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1923307
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1923289
    check-cast p1, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    invoke-static {p1, p2, p3}, Lcom/facebook/placetips/bootstrap/PresenceDescriptionSerializer;->a(Lcom/facebook/placetips/bootstrap/PresenceDescription;LX/0nX;LX/0my;)V

    return-void
.end method
