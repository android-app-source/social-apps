.class public Lcom/facebook/placetips/bootstrap/PresenceDescription;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/bootstrap/PresenceDescriptionDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/bootstrap/PresenceDescriptionSerializer;
.end annotation


# instance fields
.field public final mConfidenceLevel:LX/Cdj;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "confidence_level"
    .end annotation
.end field

.field public final mExistingReactionSessionId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "existing_reaction_session_id"
    .end annotation
.end field

.field public final mFeedUnitHeaderStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "feed_unit_header_styled"
    .end annotation
.end field

.field public mFeedUnitShowSuggestifierFooter:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "feed_unit_show_suggestifier_footer"
    .end annotation
.end field

.field public final mFeedUnitSubtitleStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "feed_unit_subtitle_styled"
    .end annotation
.end field

.field public final mPageCategoryNames:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_categories"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mPageId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_id"
    .end annotation
.end field

.field public final mPageName:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_name"
    .end annotation
.end field

.field public final mPlaceTipWelcomeHeader:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "place_tip_welcome_header"
    .end annotation
.end field

.field public final mPresenceAcquiredAt:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "presence_acquired_at"
    .end annotation
.end field

.field public final mPulsarLastSeenAt:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "presence_last_seen_at"
    .end annotation
.end field

.field public final mReactionStories:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "reaction_stories"
    .end annotation
.end field

.field public final mSource:Lcom/facebook/placetips/bootstrap/PresenceSource;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "presence_source"
    .end annotation
.end field

.field public final mSuggestifierFooterDescription:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggestifier_footer_description"
    .end annotation
.end field

.field public final mSuggestifierFooterQuestion:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggestifier_footer_question"
    .end annotation
.end field

.field public final mSuggestifierFooterThankYouText:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggestifier_footer_thank_you_text"
    .end annotation
.end field

.field public final mSuggestifierResponseId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "suggestifier_response_id"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1923251
    const-class v0, Lcom/facebook/placetips/bootstrap/PresenceDescriptionDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1923250
    const-class v0, Lcom/facebook/placetips/bootstrap/PresenceDescriptionSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1923231
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923232
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageId:Ljava/lang/String;

    .line 1923233
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageName:Ljava/lang/String;

    .line 1923234
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageCategoryNames:LX/0Px;

    .line 1923235
    iput-wide v2, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPresenceAcquiredAt:J

    .line 1923236
    iput-wide v2, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPulsarLastSeenAt:J

    .line 1923237
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitHeaderStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923238
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitSubtitleStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923239
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitShowSuggestifierFooter:Z

    .line 1923240
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterQuestion:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923241
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterDescription:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923242
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterThankYouText:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923243
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSource:Lcom/facebook/placetips/bootstrap/PresenceSource;

    .line 1923244
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPlaceTipWelcomeHeader:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 1923245
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mReactionStories:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    .line 1923246
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierResponseId:Ljava/lang/String;

    .line 1923247
    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mExistingReactionSessionId:Ljava/lang/String;

    .line 1923248
    sget-object v0, LX/Cdj;->UNKNOWN:LX/Cdj;

    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mConfidenceLevel:LX/Cdj;

    .line 1923249
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;JJLX/175;LX/175;ZLX/175;LX/175;LX/175;Lcom/facebook/placetips/bootstrap/PresenceSource;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;LX/9qT;Ljava/lang/String;Ljava/lang/String;LX/Cdj;)V
    .locals 2
    .param p9    # LX/175;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p11    # LX/175;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p12    # LX/175;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p13    # LX/175;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p15    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p16    # LX/9qT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p17    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p18    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p19    # LX/Cdj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;JJ",
            "LX/175;",
            "LX/175;",
            "Z",
            "LX/175;",
            "LX/175;",
            "LX/175;",
            "Lcom/facebook/placetips/bootstrap/PresenceSource;",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLInterfaces$PlaceTipWelcomeHeaderFragment;",
            "LX/9qT;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/Cdj;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1923212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923213
    iput-object p1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageId:Ljava/lang/String;

    .line 1923214
    iput-object p2, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageName:Ljava/lang/String;

    .line 1923215
    iput-object p3, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageCategoryNames:LX/0Px;

    .line 1923216
    iput-wide p4, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPresenceAcquiredAt:J

    .line 1923217
    iput-wide p6, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPulsarLastSeenAt:J

    .line 1923218
    invoke-static {p8}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a(LX/175;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitHeaderStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923219
    invoke-static {p9}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a(LX/175;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitSubtitleStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923220
    iput-boolean p10, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitShowSuggestifierFooter:Z

    .line 1923221
    invoke-static {p11}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a(LX/175;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterQuestion:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923222
    invoke-static {p12}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a(LX/175;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterDescription:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923223
    invoke-static {p13}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;->a(LX/175;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterThankYouText:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923224
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSource:Lcom/facebook/placetips/bootstrap/PresenceSource;

    .line 1923225
    invoke-static/range {p16 .. p16}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;->a(LX/9qT;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mReactionStories:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    .line 1923226
    invoke-static/range {p15 .. p15}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPlaceTipWelcomeHeader:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 1923227
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierResponseId:Ljava/lang/String;

    .line 1923228
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mExistingReactionSessionId:Ljava/lang/String;

    .line 1923229
    if-nez p19, :cond_0

    sget-object p19, LX/Cdj;->UNKNOWN:LX/Cdj;

    :cond_0
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mConfidenceLevel:LX/Cdj;

    .line 1923230
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1923211
    iget-wide v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPulsarLastSeenAt:J

    return-wide v0
.end method

.method public final a(JLcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;LX/9qT;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/placetips/bootstrap/PresenceSource;LX/Cdj;)Lcom/facebook/placetips/bootstrap/PresenceDescription;
    .locals 23
    .param p3    # Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # LX/9qT;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/Cdj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1923210
    new-instance v2, Lcom/facebook/placetips/bootstrap/PresenceDescription;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageName:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageCategoryNames:LX/0Px;

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPresenceAcquiredAt:J

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitHeaderStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitSubtitleStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitShowSuggestifierFooter:Z

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterQuestion:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterDescription:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterThankYouText:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-wide/from16 v8, p1

    move-object/from16 v16, p7

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    move-object/from16 v19, p5

    move-object/from16 v20, p6

    move-object/from16 v21, p8

    invoke-direct/range {v2 .. v21}, Lcom/facebook/placetips/bootstrap/PresenceDescription;-><init>(Ljava/lang/String;Ljava/lang/String;LX/0Px;JJLX/175;LX/175;ZLX/175;LX/175;LX/175;Lcom/facebook/placetips/bootstrap/PresenceSource;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;LX/9qT;Ljava/lang/String;Ljava/lang/String;LX/Cdj;)V

    return-object v2
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 1923208
    iput-boolean p1, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitShowSuggestifierFooter:Z

    .line 1923209
    return-void
.end method

.method public final b()LX/175;
    .locals 1

    .prologue
    .line 1923192
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitHeaderStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final c()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923207
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitSubtitleStyled:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1923206
    iget-boolean v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mFeedUnitShowSuggestifierFooter:Z

    return v0
.end method

.method public final e()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923205
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterQuestion:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final f()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923204
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterDescription:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final g()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923203
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierFooterThankYouText:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923202
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageName:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923201
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageId:Ljava/lang/String;

    return-object v0
.end method

.method public final j()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923200
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPageCategoryNames:LX/0Px;

    return-object v0
.end method

.method public final k()Lcom/facebook/placetips/bootstrap/PresenceSource;
    .locals 1

    .prologue
    .line 1923199
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSource:Lcom/facebook/placetips/bootstrap/PresenceSource;

    return-object v0
.end method

.method public final l()LX/2cx;
    .locals 1

    .prologue
    .line 1923198
    invoke-virtual {p0}, Lcom/facebook/placetips/bootstrap/PresenceDescription;->k()Lcom/facebook/placetips/bootstrap/PresenceSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/placetips/bootstrap/PresenceSource;->a()LX/2cx;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923197
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mPlaceTipWelcomeHeader:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    return-object v0
.end method

.method public final n()LX/9qT;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923196
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mReactionStories:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1923195
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mSuggestifierResponseId:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923194
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mExistingReactionSessionId:Ljava/lang/String;

    return-object v0
.end method

.method public final q()LX/Cdj;
    .locals 1

    .prologue
    .line 1923193
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/PresenceDescription;->mConfidenceLevel:LX/Cdj;

    return-object v0
.end method
