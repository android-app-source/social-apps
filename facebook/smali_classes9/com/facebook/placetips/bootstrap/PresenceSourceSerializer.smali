.class public Lcom/facebook/placetips/bootstrap/PresenceSourceSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/placetips/bootstrap/PresenceSource;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1923342
    const-class v0, Lcom/facebook/placetips/bootstrap/PresenceSource;

    new-instance v1, Lcom/facebook/placetips/bootstrap/PresenceSourceSerializer;

    invoke-direct {v1}, Lcom/facebook/placetips/bootstrap/PresenceSourceSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1923343
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1923344
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/placetips/bootstrap/PresenceSource;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1923345
    if-nez p0, :cond_0

    .line 1923346
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1923347
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1923348
    invoke-static {p0, p1, p2}, Lcom/facebook/placetips/bootstrap/PresenceSourceSerializer;->b(Lcom/facebook/placetips/bootstrap/PresenceSource;LX/0nX;LX/0my;)V

    .line 1923349
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1923350
    return-void
.end method

.method private static b(Lcom/facebook/placetips/bootstrap/PresenceSource;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1923351
    const-string v0, "type"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mPresenceSourceType:LX/2cx;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923352
    const-string v0, "pulsar_rssi"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mPulsarRssi:Ljava/lang/Integer;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1923353
    const-string v0, "latitude"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mLatitude:Ljava/lang/Double;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 1923354
    const-string v0, "longitude"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mLongitude:Ljava/lang/Double;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Double;)V

    .line 1923355
    const-string v0, "accuracy"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/PresenceSource;->mAccuracy:Ljava/lang/Float;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Float;)V

    .line 1923356
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1923357
    check-cast p1, Lcom/facebook/placetips/bootstrap/PresenceSource;

    invoke-static {p1, p2, p3}, Lcom/facebook/placetips/bootstrap/PresenceSourceSerializer;->a(Lcom/facebook/placetips/bootstrap/PresenceSource;LX/0nX;LX/0my;)V

    return-void
.end method
