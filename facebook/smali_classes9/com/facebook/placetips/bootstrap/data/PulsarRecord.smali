.class public Lcom/facebook/placetips/bootstrap/data/PulsarRecord;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/bootstrap/data/PulsarRecordDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/bootstrap/data/PulsarRecordSerializer;
.end annotation


# instance fields
.field public final mCompanyIdentifier:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "company_identifier"
    .end annotation
.end field

.field public final mMajor:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "major"
    .end annotation
.end field

.field public final mMinor:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minor"
    .end annotation
.end field

.field public final mPulsarAdvertismentIndicator:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "pulsar_ad_indicator"
    .end annotation
.end field

.field public final mUUID:Ljava/util/UUID;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "uuid"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1923399
    const-class v0, Lcom/facebook/placetips/bootstrap/data/PulsarRecordDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1923419
    const-class v0, Lcom/facebook/placetips/bootstrap/data/PulsarRecordSerializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1923412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923413
    iput v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mCompanyIdentifier:I

    .line 1923414
    iput v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mPulsarAdvertismentIndicator:I

    .line 1923415
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mUUID:Ljava/util/UUID;

    .line 1923416
    iput v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMajor:I

    .line 1923417
    iput v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMinor:I

    .line 1923418
    return-void
.end method

.method public constructor <init>(IILjava/util/UUID;II)V
    .locals 0

    .prologue
    .line 1923405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1923406
    iput p1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mCompanyIdentifier:I

    .line 1923407
    iput p2, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mPulsarAdvertismentIndicator:I

    .line 1923408
    iput-object p3, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mUUID:Ljava/util/UUID;

    .line 1923409
    iput p4, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMajor:I

    .line 1923410
    iput p5, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMinor:I

    .line 1923411
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/UUID;
    .locals 1

    .prologue
    .line 1923404
    iget-object v0, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mUUID:Ljava/util/UUID;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1923420
    iget v0, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMajor:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1923403
    iget v0, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMinor:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1923402
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->hashCode()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1923401
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mCompanyIdentifier:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mPulsarAdvertismentIndicator:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mUUID:Ljava/util/UUID;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMajor:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget v2, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMinor:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1923400
    const-string v0, "Pulsar Record: companyIdent: %d, adIndicator: %d, major: %d, minor: %d, uuid: %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mCompanyIdentifier:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mPulsarAdvertismentIndicator:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMajor:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMinor:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mUUID:Ljava/util/UUID;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
