.class public Lcom/facebook/placetips/bootstrap/data/PulsarRecordSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/placetips/bootstrap/data/PulsarRecord;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1923460
    const-class v0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;

    new-instance v1, Lcom/facebook/placetips/bootstrap/data/PulsarRecordSerializer;

    invoke-direct {v1}, Lcom/facebook/placetips/bootstrap/data/PulsarRecordSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1923461
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1923446
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/placetips/bootstrap/data/PulsarRecord;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1923454
    if-nez p0, :cond_0

    .line 1923455
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1923456
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1923457
    invoke-static {p0, p1, p2}, Lcom/facebook/placetips/bootstrap/data/PulsarRecordSerializer;->b(Lcom/facebook/placetips/bootstrap/data/PulsarRecord;LX/0nX;LX/0my;)V

    .line 1923458
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1923459
    return-void
.end method

.method private static b(Lcom/facebook/placetips/bootstrap/data/PulsarRecord;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1923448
    const-string v0, "company_identifier"

    iget v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mCompanyIdentifier:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1923449
    const-string v0, "pulsar_ad_indicator"

    iget v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mPulsarAdvertismentIndicator:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1923450
    const-string v0, "uuid"

    iget-object v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mUUID:Ljava/util/UUID;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1923451
    const-string v0, "major"

    iget v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMajor:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1923452
    const-string v0, "minor"

    iget v1, p0, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;->mMinor:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1923453
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1923447
    check-cast p1, Lcom/facebook/placetips/bootstrap/data/PulsarRecord;

    invoke-static {p1, p2, p3}, Lcom/facebook/placetips/bootstrap/data/PulsarRecordSerializer;->a(Lcom/facebook/placetips/bootstrap/data/PulsarRecord;LX/0nX;LX/0my;)V

    return-void
.end method
