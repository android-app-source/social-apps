.class public final Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1a361238
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924248
    const-class v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924251
    const-class v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1924249
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1924250
    return-void
.end method

.method private a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924240
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;->e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;->e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1924241
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;->e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1924242
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924243
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1924244
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1924245
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1924246
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924247
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1924232
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924233
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1924234
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1924235
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1924236
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;

    .line 1924237
    iput-object v0, v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;->e:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1924238
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924239
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1924229
    new-instance v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;-><init>()V

    .line 1924230
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1924231
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1924228
    const v0, -0x47f292f5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1924227
    const v0, -0xdd137d3

    return v0
.end method
