.class public final Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x2f21c514
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924352
    const-class v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924355
    const-class v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1924353
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1924354
    return-void
.end method

.method private j()LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1924322
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;I)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->e:Ljava/util/List;

    .line 1924323
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924350
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->f:Ljava/lang/String;

    .line 1924351
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924348
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->g:Ljava/lang/String;

    .line 1924349
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method private m()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924346
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->h:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->h:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    .line 1924347
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->h:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1924334
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924335
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/util/List;)I

    move-result v0

    .line 1924336
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1924337
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1924338
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->m()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1924339
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1924340
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1924341
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1924342
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1924343
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1924344
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924345
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1924356
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924357
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->m()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1924358
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->m()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    .line 1924359
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->m()Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1924360
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    .line 1924361
    iput-object v0, v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->h:Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    .line 1924362
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924363
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1924333
    new-instance v0, LX/Ce9;

    invoke-direct {v0, p1}, LX/Ce9;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924332
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1924330
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1924331
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1924329
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1924326
    new-instance v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;

    invoke-direct {v0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel;-><init>()V

    .line 1924327
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1924328
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1924325
    const v0, 0x1a82844a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1924324
    const v0, 0x25d6af

    return v0
.end method
