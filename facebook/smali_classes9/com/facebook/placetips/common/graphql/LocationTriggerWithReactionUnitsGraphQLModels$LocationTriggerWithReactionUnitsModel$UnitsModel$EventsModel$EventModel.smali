.class public final Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x79e1b38
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1923639
    const-class v0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1923638
    const-class v0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1923636
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1923637
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923634
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1923635
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1923626
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1923627
    invoke-virtual {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1923628
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1923629
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1923630
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1923631
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1923632
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1923633
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1923618
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1923619
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1923620
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1923621
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1923622
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    .line 1923623
    iput-object v0, v1, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1923624
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1923625
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1923617
    new-instance v0, LX/Cdz;

    invoke-direct {v0, p1}, LX/Cdz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923640
    invoke-virtual {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1923615
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1923616
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1923614
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1923611
    new-instance v0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    invoke-direct {v0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;-><init>()V

    .line 1923612
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1923613
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923609
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->e:Ljava/lang/String;

    .line 1923610
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923608
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1923607
    const v0, -0x7e723aa0

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1923606
    const v0, 0x403827a

    return v0
.end method
