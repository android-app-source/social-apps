.class public final Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x215cc192
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1923683
    const-class v0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1923648
    const-class v0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1923681
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1923682
    return-void
.end method

.method private j()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923679
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->e:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->e:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    .line 1923680
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->e:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923677
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923678
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1923669
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1923670
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->j()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1923671
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1923672
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1923673
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1923674
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1923675
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1923676
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1923656
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1923657
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->j()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1923658
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->j()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    .line 1923659
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->j()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1923660
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;

    .line 1923661
    iput-object v0, v1, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->e:Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    .line 1923662
    :cond_0
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1923663
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923664
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1923665
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;

    .line 1923666
    iput-object v0, v1, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->f:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 1923667
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1923668
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923655
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->j()Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel$EventModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/175;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1923654
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1923651
    new-instance v0, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;

    invoke-direct {v0}, Lcom/facebook/placetips/common/graphql/LocationTriggerWithReactionUnitsGraphQLModels$LocationTriggerWithReactionUnitsModel$UnitsModel$EventsModel;-><init>()V

    .line 1923652
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1923653
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1923650
    const v0, 0x783194a5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1923649
    const v0, 0x21064037

    return v0
.end method
