.class public final Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x7a257dc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924309
    const-class v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1924312
    const-class v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1924310
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1924311
    return-void
.end method

.method private a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1924307
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->e:Ljava/util/List;

    .line 1924308
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924305
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1924306
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924303
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    .line 1924304
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    return-object v0
.end method

.method private l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924313
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 1924314
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    return-object v0
.end method

.method private m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1924301
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->i:Ljava/lang/String;

    .line 1924302
    iget-object v0, p0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1924287
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924288
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1924289
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1924290
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1924291
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1924292
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1924293
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1924294
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1924295
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1924296
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1924297
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1924298
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1924299
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924300
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1924264
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1924265
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 1924266
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1924267
    if-eqz v1, :cond_4

    .line 1924268
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    .line 1924269
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1924270
    :goto_0
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1924271
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1924272
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1924273
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    .line 1924274
    iput-object v0, v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->f:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    .line 1924275
    :cond_0
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1924276
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    .line 1924277
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->k()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1924278
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    .line 1924279
    iput-object v0, v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->g:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipsFeedUnitFragmentModel;

    .line 1924280
    :cond_1
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1924281
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 1924282
    invoke-direct {p0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->l()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1924283
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    .line 1924284
    iput-object v0, v1, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;->h:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$PlaceTipWelcomeHeaderFragmentModel;

    .line 1924285
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1924286
    if-nez v1, :cond_3

    :goto_1
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1924259
    new-instance v0, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;

    invoke-direct {v0}, Lcom/facebook/placetips/common/graphql/PlaceTipWithReactionUnitsGraphQlFragmentModels$PlaceTipWithReactionUnitsGraphQlFragmentModel$PlaceReactionUnitsModel;-><init>()V

    .line 1924260
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1924261
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1924263
    const v0, -0x1717d586

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1924262
    const v0, 0x1e12454e

    return v0
.end method
