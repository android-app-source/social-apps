.class public final Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/0hs;

.field public final synthetic c:LX/0Ot;

.field public final synthetic d:LX/0Ot;

.field public final synthetic e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;


# direct methods
.method public constructor <init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;Landroid/view/View;LX/0hs;LX/0Ot;LX/0Ot;)V
    .locals 0

    .prologue
    .line 1809637
    iput-object p1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iput-object p2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->b:LX/0hs;

    iput-object p4, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->c:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->d:LX/0Ot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1809638
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1809639
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1809640
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->b:LX/0hs;

    const v2, 0x7f08062e

    invoke-virtual {v1, v2}, LX/0hs;->b(I)V

    .line 1809641
    aget v1, v0, v3

    if-nez v1, :cond_0

    aget v0, v0, v4

    if-eqz v0, :cond_2

    .line 1809642
    :cond_0
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->J:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1809643
    if-eqz v0, :cond_1

    .line 1809644
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1809645
    :goto_0
    return-void

    .line 1809646
    :cond_1
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->b:LX/0hs;

    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1809647
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0dQ;->J:LX/0Tn;

    invoke-interface {v0, v1, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 1809648
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "dialtone_optout_tooltip_impression"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1809649
    const-string v2, "carrier_id"

    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/0yh;->NORMAL:LX/0yh;

    invoke-virtual {v3}, LX/0yh;->getCarrierIdKey()LX/0Tn;

    move-result-object v3

    const-string v4, ""

    invoke-interface {v0, v3, v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1809650
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v2, "dialtone"

    .line 1809651
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1809652
    move-object v1, v1

    .line 1809653
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1809654
    :cond_2
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$8;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
