.class public final Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/0Ot;

.field public final synthetic d:LX/0Ot;

.field public final synthetic e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;


# direct methods
.method public constructor <init>(Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;Ljava/lang/String;Ljava/lang/String;LX/0Ot;LX/0Ot;)V
    .locals 0

    .prologue
    .line 1809616
    iput-object p1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iput-object p2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->c:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->d:LX/0Ot;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1809599
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-boolean v0, v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->h:Z

    if-eqz v0, :cond_0

    .line 1809600
    :goto_0
    return-void

    .line 1809601
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 1809602
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v1, v1, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v1, v0}, Landroid/widget/ToggleButton;->getLocationInWindow([I)V

    .line 1809603
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v1, v1, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 1809604
    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v1, v1, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    iget-object v2, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1809605
    aget v1, v0, v3

    if-nez v1, :cond_1

    const/4 v1, 0x1

    aget v0, v0, v1

    if-eqz v0, :cond_3

    .line 1809606
    :cond_1
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dQ;->J:LX/0Tn;

    invoke-interface {v0, v1, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 1809607
    if-eqz v0, :cond_2

    .line 1809608
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v0, v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 1809609
    :cond_2
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v0, v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->g:LX/0hs;

    iget-object v1, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v1, v1, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 1809610
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v0, "dialtone_switcher_info_tooltip_impression"

    invoke-direct {v1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1809611
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    const-string v2, "dialtone"

    .line 1809612
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1809613
    move-object v1, v1

    .line 1809614
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0

    .line 1809615
    :cond_3
    iget-object v0, p0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher$4;->e:Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;

    iget-object v0, v0, Lcom/facebook/dialtone/switcher/DialtoneManualSwitcher;->e:Landroid/widget/ToggleButton;

    invoke-virtual {v0, p0}, Landroid/widget/ToggleButton;->removeCallbacks(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
