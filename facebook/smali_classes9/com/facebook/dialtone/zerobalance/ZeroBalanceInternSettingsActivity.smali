.class public Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private A:Landroid/widget/TextView;

.field private B:Landroid/widget/TextView;

.field private C:Landroid/widget/TextView;

.field private D:Landroid/widget/TextView;

.field private E:Landroid/widget/TextView;

.field private F:Landroid/widget/TextView;

.field public p:LX/12J;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0yW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/zero/datacheck/annotations/IsInAndroidBalanceDetection;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1hq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0dO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private u:Landroid/widget/Button;

.field private v:Landroid/view/ViewGroup;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/widget/TextView;

.field private z:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1809771
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 1809772
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->w:Landroid/widget/TextView;

    .line 1809773
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->x:Landroid/widget/TextView;

    .line 1809774
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->y:Landroid/widget/TextView;

    .line 1809775
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 1809776
    const-string v1, "----------------------------------------------"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1809777
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->z:Landroid/widget/TextView;

    .line 1809778
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->A:Landroid/widget/TextView;

    .line 1809779
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->B:Landroid/widget/TextView;

    .line 1809780
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->C:Landroid/widget/TextView;

    .line 1809781
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->D:Landroid/widget/TextView;

    .line 1809782
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->E:Landroid/widget/TextView;

    .line 1809783
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->F:Landroid/widget/TextView;

    .line 1809784
    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->w:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809785
    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809786
    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->y:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809787
    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809788
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->z:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809789
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->A:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809790
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->B:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809791
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->C:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809792
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->D:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809793
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->E:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809794
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->F:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809795
    invoke-static {p0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->b$redex0(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;)V

    .line 1809796
    new-instance v0, Lcom/facebook/resources/ui/FbButton;

    invoke-direct {v0, p0}, Lcom/facebook/resources/ui/FbButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->u:Landroid/widget/Button;

    .line 1809797
    invoke-static {p0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->l(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;)V

    .line 1809798
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->u:Landroid/widget/Button;

    new-instance v1, LX/Bhv;

    invoke-direct {v1, p0}, LX/Bhv;-><init>(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809799
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->u:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809800
    new-instance v0, Lcom/facebook/resources/ui/FbButton;

    invoke-direct {v0, p0}, Lcom/facebook/resources/ui/FbButton;-><init>(Landroid/content/Context;)V

    .line 1809801
    const-string v1, "Reset ZB Counters"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1809802
    new-instance v1, LX/Bhw;

    invoke-direct {v1, p0}, LX/Bhw;-><init>(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1809803
    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1809804
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 1809805
    new-instance v1, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity$3;

    invoke-direct {v1, p0, v0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity$3;-><init>(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;Landroid/os/Handler;)V

    const-wide/16 v2, 0x3e8

    const v4, 0x2bb312fb

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1809806
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1809807
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<b>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": </b>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1809808
    return-void
.end method

.method private static a(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;LX/12J;LX/0yW;Ljava/lang/Boolean;LX/1hq;LX/0dO;)V
    .locals 0

    .prologue
    .line 1809809
    iput-object p1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->p:LX/12J;

    iput-object p2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->q:LX/0yW;

    iput-object p3, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->r:Ljava/lang/Boolean;

    iput-object p4, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->s:LX/1hq;

    iput-object p5, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->t:LX/0dO;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;

    invoke-static {v5}, LX/12J;->a(LX/0QB;)LX/12J;

    move-result-object v1

    check-cast v1, LX/12J;

    invoke-static {v5}, LX/0yW;->a(LX/0QB;)LX/0yW;

    move-result-object v2

    check-cast v2, LX/0yW;

    invoke-static {v5}, LX/1pi;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-static {v5}, LX/1hq;->a(LX/0QB;)LX/1hq;

    move-result-object v4

    check-cast v4, LX/1hq;

    invoke-static {v5}, LX/0dO;->a(LX/0QB;)LX/0dO;

    move-result-object v5

    check-cast v5, LX/0dO;

    invoke-static/range {v0 .. v5}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;LX/12J;LX/0yW;Ljava/lang/Boolean;LX/1hq;LX/0dO;)V

    return-void
.end method

.method public static b$redex0(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;)V
    .locals 6

    .prologue
    const-wide/32 v4, 0x5265c00

    .line 1809810
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->w:Landroid/widget/TextView;

    const-string v1, "Zero Balance Gatekeeper"

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->r:Ljava/lang/Boolean;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809811
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->x:Landroid/widget/TextView;

    const-string v1, "Zero Data State"

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->q:LX/0yW;

    invoke-virtual {v2}, LX/0yW;->a()LX/2XZ;

    move-result-object v2

    invoke-virtual {v2}, LX/2XZ;->name()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809812
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->y:Landroid/widget/TextView;

    const-string v1, "Zero Data State Last Changed"

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->q:LX/0yW;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0yW;->a(Z)J

    move-result-wide v2

    invoke-static {v2, v3}, LX/1m9;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809813
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->t:LX/0dO;

    sget-object v1, LX/0yY;->DIALTONE_SWITCHER_ZERO_BALANCE_REMINDER:LX/0yY;

    invoke-virtual {v0, v1}, LX/0dP;->a(LX/0yY;)Z

    move-result v0

    .line 1809814
    iget-object v1, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->z:Landroid/widget/TextView;

    const-string v2, "ZB Experiment Enabled"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809815
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->A:Landroid/widget/TextView;

    const-string v1, "ZB Experiment"

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->p:LX/12J;

    invoke-virtual {v2}, LX/12J;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809816
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->B:Landroid/widget/TextView;

    const-string v1, "ZB Impressions"

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->p:LX/12J;

    invoke-virtual {v2}, LX/12J;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809817
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->C:Landroid/widget/TextView;

    const-string v1, "ZB Max Impressions"

    const-string v2, "3"

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809818
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->D:Landroid/widget/TextView;

    const-string v1, "ZB Last Impression"

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->p:LX/12J;

    invoke-virtual {v2}, LX/12J;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/1m9;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809819
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->E:Landroid/widget/TextView;

    const-string v1, "ZB Min Time Between Impressions"

    invoke-static {v4, v5}, LX/1m9;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809820
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->F:Landroid/widget/TextView;

    const-string v1, "ZB Min Time Until Next Impression"

    iget-object v2, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->p:LX/12J;

    invoke-virtual {v2}, LX/12J;->b()J

    move-result-wide v2

    sub-long v2, v4, v2

    invoke-static {v2, v3}, LX/1m9;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 1809821
    return-void
.end method

.method public static l(Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;)V
    .locals 2

    .prologue
    .line 1809822
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->s:LX/1hq;

    .line 1809823
    iget-boolean v1, v0, LX/1hq;->a:Z

    move v0, v1

    .line 1809824
    if-eqz v0, :cond_0

    .line 1809825
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->u:Landroid/widget/Button;

    const-string v1, "Free Mode Simulation: <b>ON</b>"

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1809826
    :goto_0
    return-void

    .line 1809827
    :cond_0
    iget-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->u:Landroid/widget/Button;

    const-string v1, "Free Mode Simulation: <b>OFF</b>"

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1809828
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1809829
    invoke-static {p0, p0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1809830
    const v0, 0x7f03041b

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->setContentView(I)V

    .line 1809831
    const v0, 0x7f0d051b

    invoke-virtual {p0, v0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->v:Landroid/view/ViewGroup;

    .line 1809832
    invoke-direct {p0}, Lcom/facebook/dialtone/zerobalance/ZeroBalanceInternSettingsActivity;->a()V

    .line 1809833
    return-void
.end method
