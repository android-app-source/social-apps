.class public Lcom/facebook/audience/direct/upload/BackstageUploadService;
.super LX/1ZN;
.source ""


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field public a:LX/BaV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1El;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Er;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1798853
    const-class v0, Lcom/facebook/audience/direct/upload/BackstageUploadService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1798851
    const-string v0, "backstage_save_shot"

    invoke-direct {p0, v0}, LX/1ZN;-><init>(Ljava/lang/String;)V

    .line 1798852
    return-void
.end method

.method private static a(Lcom/facebook/audience/direct/upload/BackstageUploadService;LX/BaV;LX/1El;LX/1Er;)V
    .locals 0

    .prologue
    .line 1798854
    iput-object p1, p0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->a:LX/BaV;

    iput-object p2, p0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->b:LX/1El;

    iput-object p3, p0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->c:LX/1Er;

    return-void
.end method

.method private a(Lcom/facebook/audience/model/UploadShot;)V
    .locals 4

    .prologue
    .line 1798844
    sget-object v0, LX/BaI;->a:[I

    invoke-static {p1}, LX/7h8;->b(Lcom/facebook/audience/model/UploadShot;)LX/7h7;

    move-result-object v1

    invoke-virtual {v1}, LX/7h7;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1798845
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "attempted to use upload service for an unsupported type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1798846
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->b:LX/1El;

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getReplyThreadId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, LX/1El;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)V

    .line 1798847
    iget-object v1, p0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->a:LX/BaV;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/audience/model/UploadShot;

    new-instance v2, LX/BaH;

    invoke-direct {v2, p0, p1}, LX/BaH;-><init>(Lcom/facebook/audience/direct/upload/BackstageUploadService;Lcom/facebook/audience/model/UploadShot;)V

    .line 1798848
    iget-object v3, v1, LX/BaV;->h:LX/1Eo;

    sget-object p0, LX/7gQ;->UPLOAD_STARTED:LX/7gQ;

    invoke-virtual {v3, p0}, LX/1Eo;->a(LX/7gQ;)V

    .line 1798849
    iget-object v3, v1, LX/BaV;->b:Ljava/util/concurrent/ExecutorService;

    new-instance p0, Lcom/facebook/audience/upload/ShotApi$1;

    invoke-direct {p0, v1, v0, v2}, Lcom/facebook/audience/upload/ShotApi$1;-><init>(LX/BaV;Lcom/facebook/audience/model/UploadShot;LX/BaH;)V

    const p1, -0x1e661630    # -3.5489998E20f

    invoke-static {v3, p0, p1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 1798850
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/audience/direct/upload/BackstageUploadService;

    invoke-static {v2}, LX/BaV;->a(LX/0QB;)LX/BaV;

    move-result-object v0

    check-cast v0, LX/BaV;

    invoke-static {v2}, LX/1El;->a(LX/0QB;)LX/1El;

    move-result-object v1

    check-cast v1, LX/1El;

    invoke-static {v2}, LX/1Er;->a(LX/0QB;)LX/1Er;

    move-result-object v2

    check-cast v2, LX/1Er;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/audience/direct/upload/BackstageUploadService;->a(Lcom/facebook/audience/direct/upload/BackstageUploadService;LX/BaV;LX/1El;LX/1Er;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x24

    const v2, 0x26ac8b22

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1798839
    iget-object v0, p0, Lcom/facebook/audience/direct/upload/BackstageUploadService;->c:LX/1Er;

    invoke-virtual {v0}, LX/1Er;->a()V

    .line 1798840
    const-string v0, "shot"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v0, v3, v1

    .line 1798841
    check-cast v0, Lcom/facebook/audience/model/UploadShot;

    invoke-direct {p0, v0}, Lcom/facebook/audience/direct/upload/BackstageUploadService;->a(Lcom/facebook/audience/model/UploadShot;)V

    .line 1798842
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1798843
    :cond_0
    const v0, -0x1a68025f

    invoke-static {v0, v2}, LX/02F;->d(II)V

    return-void
.end method

.method public final onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x24

    const v1, -0x735f3efa

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1798836
    invoke-super {p0}, LX/1ZN;->onCreate()V

    .line 1798837
    invoke-static {p0, p0}, Lcom/facebook/audience/direct/upload/BackstageUploadService;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1798838
    const/16 v1, 0x25

    const v2, -0x7eceafd1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
