.class public final Lcom/facebook/audience/upload/ShotApi$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/audience/model/UploadShot;

.field public final synthetic b:LX/BaH;

.field public final synthetic c:LX/BaV;


# direct methods
.method public constructor <init>(LX/BaV;Lcom/facebook/audience/model/UploadShot;LX/BaH;)V
    .locals 0

    .prologue
    .line 1799113
    iput-object p1, p0, Lcom/facebook/audience/upload/ShotApi$1;->c:LX/BaV;

    iput-object p2, p0, Lcom/facebook/audience/upload/ShotApi$1;->a:Lcom/facebook/audience/model/UploadShot;

    iput-object p3, p0, Lcom/facebook/audience/upload/ShotApi$1;->b:LX/BaH;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1799114
    :try_start_0
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    .line 1799115
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1799116
    iput-object v1, v0, LX/14U;->e:Lcom/facebook/http/interfaces/RequestPriority;

    .line 1799117
    iget-object v1, p0, Lcom/facebook/audience/upload/ShotApi$1;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-virtual {v1}, Lcom/facebook/audience/model/UploadShot;->getMediaType()LX/7gi;

    move-result-object v1

    sget-object v2, LX/7gi;->PHOTO:LX/7gi;

    if-ne v1, v2, :cond_1

    .line 1799118
    iget-object v1, p0, Lcom/facebook/audience/upload/ShotApi$1;->c:LX/BaV;

    iget-object v2, p0, Lcom/facebook/audience/upload/ShotApi$1;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-virtual {v2}, Lcom/facebook/audience/model/UploadShot;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/audience/upload/ShotApi$1;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-virtual {v3}, Lcom/facebook/audience/model/UploadShot;->getCreativeEditingData()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    .line 1799119
    new-instance v4, LX/8NJ;

    new-instance v5, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;

    const-string v6, ""

    const-wide/16 v8, 0x0

    invoke-direct {v5, v6, v8, v9}, Lcom/facebook/photos/upload/protocol/UploadPhotoSource;-><init>(Ljava/lang/String;J)V

    invoke-direct {v4, v5}, LX/8NJ;-><init>(Lcom/facebook/photos/upload/protocol/UploadPhotoSource;)V

    .line 1799120
    if-eqz v3, :cond_0

    .line 1799121
    new-instance v5, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v5, v3, v6}, Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;-><init>(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Ljava/util/List;)V

    .line 1799122
    iput-object v5, v4, LX/8NJ;->E:Lcom/facebook/photos/creativeediting/model/CreativeEditingUploadParams;

    .line 1799123
    :cond_0
    invoke-virtual {v4}, LX/8NJ;->a()Lcom/facebook/photos/upload/protocol/UploadPhotoParams;

    move-result-object v4

    .line 1799124
    iput-object v2, v4, Lcom/facebook/photos/upload/protocol/UploadPhotoParams;->A:Ljava/lang/String;

    .line 1799125
    iget-object v5, v1, LX/BaV;->c:LX/18V;

    iget-object v6, v1, LX/BaV;->f:LX/8NH;

    invoke-virtual {v5, v6, v4, v0}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    move-object v1, v4

    .line 1799126
    iget-object v0, p0, Lcom/facebook/audience/upload/ShotApi$1;->c:LX/BaV;

    iget-object v2, p0, Lcom/facebook/audience/upload/ShotApi$1;->a:Lcom/facebook/audience/model/UploadShot;

    invoke-static {v0, v1, v2}, LX/BaV;->a$redex0(LX/BaV;Ljava/lang/String;Lcom/facebook/audience/model/UploadShot;)LX/0Px;

    move-result-object v0

    .line 1799127
    iget-object v2, p0, Lcom/facebook/audience/upload/ShotApi$1;->b:LX/BaH;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, LX/BaH;->a(Ljava/lang/String;)V

    .line 1799128
    iget-object v0, p0, Lcom/facebook/audience/upload/ShotApi$1;->c:LX/BaV;

    iget-object v0, v0, LX/BaV;->h:LX/1Eo;

    sget-object v2, LX/7gQ;->UPLOAD_COMPLETED:LX/7gQ;

    .line 1799129
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1799130
    const-string v4, "fb_id"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1799131
    invoke-static {v0, v2, v3}, LX/1Eo;->a(LX/1Eo;LX/7gQ;Landroid/os/Bundle;)V

    .line 1799132
    :goto_0
    return-void

    .line 1799133
    :cond_1
    iget-object v0, p0, Lcom/facebook/audience/upload/ShotApi$1;->c:LX/BaV;

    iget-object v1, p0, Lcom/facebook/audience/upload/ShotApi$1;->a:Lcom/facebook/audience/model/UploadShot;

    iget-object v2, p0, Lcom/facebook/audience/upload/ShotApi$1;->b:LX/BaH;

    .line 1799134
    iget-object v3, v0, LX/BaV;->g:LX/8LV;

    invoke-virtual {v3, v1}, LX/8LV;->a(Lcom/facebook/audience/model/UploadShot;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v3

    .line 1799135
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1799136
    const-string v5, "uploadOp"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1799137
    iget-object v5, v3, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v5, v5

    .line 1799138
    if-eqz v5, :cond_2

    .line 1799139
    const-string v5, "overridden_viewer_context"

    .line 1799140
    iget-object v6, v3, Lcom/facebook/photos/upload/operation/UploadOperation;->t:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v3, v6

    .line 1799141
    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1799142
    :cond_2
    iget-object v3, v0, LX/BaV;->i:LX/0aG;

    const-string v5, "video_upload_op"

    sget-object v6, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v7, -0x54b8077    # -4.6860008E35f

    invoke-static {v3, v5, v4, v6, v7}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    move-result-object v3

    .line 1799143
    new-instance v4, LX/BaU;

    invoke-direct {v4, v0, v1, v2}, LX/BaU;-><init>(LX/BaV;Lcom/facebook/audience/model/UploadShot;LX/BaH;)V

    iget-object v5, v0, LX/BaV;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1799144
    goto :goto_0

    .line 1799145
    :catch_0
    move-exception v0

    .line 1799146
    iget-object v1, p0, Lcom/facebook/audience/upload/ShotApi$1;->c:LX/BaV;

    iget-object v1, v1, LX/BaV;->h:LX/1Eo;

    sget-object v2, LX/7gQ;->UPLOAD_FAILED:LX/7gQ;

    invoke-virtual {v1, v2}, LX/1Eo;->a(LX/7gQ;)V

    .line 1799147
    sget-object v1, LX/BaV;->a:Ljava/lang/String;

    const-string v2, "Error in uploading a photo"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1799148
    iget-object v1, p0, Lcom/facebook/audience/upload/ShotApi$1;->b:LX/BaH;

    invoke-virtual {v1, v0}, LX/BaH;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method
