.class public Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final l:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final n:Lcom/facebook/resources/ui/FbTextView;

.field public final o:Lcom/facebook/widget/FbImageView;

.field public final p:Landroid/content/Context;

.field public final q:LX/BaM;

.field public final r:LX/7ga;

.field public final s:LX/0fD;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1799101
    const-class v0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->l:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/0fD;Landroid/content/Context;LX/BaM;LX/7ga;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0fD;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1799102
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1799103
    const v0, 0x7f0d0690

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1799104
    const v0, 0x7f0d0691

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1799105
    const v0, 0x7f0d068f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->o:Lcom/facebook/widget/FbImageView;

    .line 1799106
    iput-object p3, p0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->p:Landroid/content/Context;

    .line 1799107
    iput-object p4, p0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->q:LX/BaM;

    .line 1799108
    iput-object p2, p0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->s:LX/0fD;

    .line 1799109
    iput-object p5, p0, Lcom/facebook/audience/snacks/view/snacks_row/ProfileViewHolder;->r:LX/7ga;

    .line 1799110
    return-void
.end method
