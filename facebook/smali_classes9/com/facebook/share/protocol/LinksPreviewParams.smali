.class public Lcom/facebook/share/protocol/LinksPreviewParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/share/protocol/LinksPreviewParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/share/protocol/LinksPreviewParams$Size;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1779705
    new-instance v0, LX/BOM;

    invoke-direct {v0}, LX/BOM;-><init>()V

    sput-object v0, Lcom/facebook/share/protocol/LinksPreviewParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/BON;)V
    .locals 1

    .prologue
    .line 1779720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1779721
    iget-object v0, p1, LX/BON;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->a:Ljava/lang/String;

    .line 1779722
    iget-object v0, p1, LX/BON;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->b:Ljava/lang/String;

    .line 1779723
    iget-object v0, p1, LX/BON;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->c:Ljava/lang/String;

    .line 1779724
    iget-object v0, p1, LX/BON;->d:LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->d:LX/0Px;

    .line 1779725
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1779712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1779713
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->a:Ljava/lang/String;

    .line 1779714
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->b:Ljava/lang/String;

    .line 1779715
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->c:Ljava/lang/String;

    .line 1779716
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1779717
    const-class v1, Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1779718
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->d:LX/0Px;

    .line 1779719
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1779711
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1779706
    iget-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1779707
    iget-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1779708
    iget-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1779709
    iget-object v0, p0, Lcom/facebook/share/protocol/LinksPreviewParams;->d:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1779710
    return-void
.end method
