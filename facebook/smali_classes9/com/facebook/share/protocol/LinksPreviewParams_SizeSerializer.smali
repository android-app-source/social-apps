.class public Lcom/facebook/share/protocol/LinksPreviewParams_SizeSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/share/protocol/LinksPreviewParams$Size;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1779726
    const-class v0, Lcom/facebook/share/protocol/LinksPreviewParams$Size;

    new-instance v1, Lcom/facebook/share/protocol/LinksPreviewParams_SizeSerializer;

    invoke-direct {v1}, Lcom/facebook/share/protocol/LinksPreviewParams_SizeSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1779727
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1779728
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/share/protocol/LinksPreviewParams$Size;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1779729
    if-nez p0, :cond_0

    .line 1779730
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1779731
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1779732
    invoke-static {p0, p1, p2}, Lcom/facebook/share/protocol/LinksPreviewParams_SizeSerializer;->b(Lcom/facebook/share/protocol/LinksPreviewParams$Size;LX/0nX;LX/0my;)V

    .line 1779733
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1779734
    return-void
.end method

.method private static b(Lcom/facebook/share/protocol/LinksPreviewParams$Size;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1779735
    const-string v0, "width"

    iget v1, p0, Lcom/facebook/share/protocol/LinksPreviewParams$Size;->mWidth:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1779736
    const-string v0, "height"

    iget v1, p0, Lcom/facebook/share/protocol/LinksPreviewParams$Size;->mHeight:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1779737
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1779738
    check-cast p1, Lcom/facebook/share/protocol/LinksPreviewParams$Size;

    invoke-static {p1, p2, p3}, Lcom/facebook/share/protocol/LinksPreviewParams_SizeSerializer;->a(Lcom/facebook/share/protocol/LinksPreviewParams$Size;LX/0nX;LX/0my;)V

    return-void
.end method
