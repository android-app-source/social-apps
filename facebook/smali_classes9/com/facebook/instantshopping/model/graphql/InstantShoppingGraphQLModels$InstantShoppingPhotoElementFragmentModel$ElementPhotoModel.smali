.class public final Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3944053a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1870009
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1870008
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1869988
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1869989
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1870000
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1870001
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1870002
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1870003
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1870004
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1870005
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1870006
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1870007
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1869997
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1869998
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1869999
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1870010
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1869994
    new-instance v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;

    invoke-direct {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;-><init>()V

    .line 1869995
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1869996
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1869992
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->e:Ljava/lang/String;

    .line 1869993
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1869990
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->f:Ljava/lang/String;

    .line 1869991
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1869987
    const v0, 0xc9797d5

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1869986
    const v0, 0x4984e12

    return v0
.end method
