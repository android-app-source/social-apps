.class public final Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/CHX;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3e8ff72a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Z

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1867969
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1867928
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1867929
    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1867930
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867931
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1867932
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1867933
    :cond_0
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867934
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    .line 1867935
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->g:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867959
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->h:Ljava/lang/String;

    .line 1867960
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1867936
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1867937
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1867938
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1867939
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLInstantShoppingPostActionNoticeStyle;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1867940
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1867941
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->jr_()LX/2uF;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v4

    .line 1867942
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1867943
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 1867944
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1867945
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->js_()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 1867946
    const/16 v9, 0xa

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1867947
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1867948
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1867949
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1867950
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1867951
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1867952
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1867953
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1867954
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->l:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1867955
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1867956
    const/16 v0, 0x9

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1867957
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1867958
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1867961
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1867962
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->jr_()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1867963
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->jr_()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1867964
    if-eqz v1, :cond_0

    .line 1867965
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    .line 1867966
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->i:LX/3Sb;

    .line 1867967
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1867968
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867923
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    .line 1867924
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1867925
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1867926
    const/4 v0, 0x7

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->l:Z

    .line 1867927
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1867920
    new-instance v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-direct {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;-><init>()V

    .line 1867921
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1867922
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867918
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->j:Ljava/lang/String;

    .line 1867919
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867906
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->k:Ljava/lang/String;

    const/4 v1, 0x6

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->k:Ljava/lang/String;

    .line 1867907
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1867916
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1867917
    iget-boolean v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->l:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1867915
    const v0, -0xd7d064d

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867913
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->m:Ljava/lang/String;

    .line 1867914
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1867912
    const v0, 0x73a591df

    return v0
.end method

.method public final jr_()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOptions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1867910
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->i:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x4

    const v4, -0x3cc565a1

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->i:LX/3Sb;

    .line 1867911
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->i:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final js_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867908
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->n:Ljava/lang/String;

    const/16 v1, 0x9

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->n:Ljava/lang/String;

    .line 1867909
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;->n:Ljava/lang/String;

    return-object v0
.end method
