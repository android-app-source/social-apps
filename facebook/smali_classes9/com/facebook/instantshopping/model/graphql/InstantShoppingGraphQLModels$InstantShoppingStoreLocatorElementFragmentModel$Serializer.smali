.class public final Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1870285
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    new-instance v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1870286
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1870287
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;LX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 1870288
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1870289
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/16 p0, 0x8

    const/4 v4, 0x1

    .line 1870290
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1870291
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1870292
    if-eqz v2, :cond_0

    .line 1870293
    const-string v3, "bounding_box"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870294
    invoke-static {v1, v2, p1}, LX/CII;->a(LX/15i;ILX/0nX;)V

    .line 1870295
    :cond_0
    invoke-virtual {v1, v0, v4}, LX/15i;->g(II)I

    move-result v2

    .line 1870296
    if-eqz v2, :cond_1

    .line 1870297
    const-string v2, "document_element_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870298
    invoke-virtual {v1, v0, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1870299
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1870300
    if-eqz v2, :cond_2

    .line 1870301
    const-string v3, "element_descriptor"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870302
    invoke-static {v1, v2, p1}, LX/CIA;->a(LX/15i;ILX/0nX;)V

    .line 1870303
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1870304
    if-eqz v2, :cond_3

    .line 1870305
    const-string v3, "header"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870306
    invoke-static {v1, v2, p1, p2}, LX/CIT;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1870307
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1870308
    if-eqz v2, :cond_4

    .line 1870309
    const-string v3, "locations"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870310
    invoke-static {v1, v2, p1, p2}, LX/D1c;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1870311
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1870312
    if-eqz v2, :cond_5

    .line 1870313
    const-string v3, "logging_token"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870314
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1870315
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1870316
    if-eqz v2, :cond_6

    .line 1870317
    const-string v3, "page_set"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870318
    invoke-static {v1, v2, p1}, LX/CIJ;->a(LX/15i;ILX/0nX;)V

    .line 1870319
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1870320
    if-eqz v2, :cond_7

    .line 1870321
    const-string v3, "style"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870322
    invoke-static {v1, v2, p1}, LX/CIU;->a(LX/15i;ILX/0nX;)V

    .line 1870323
    :cond_7
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1870324
    if-eqz v2, :cond_8

    .line 1870325
    const-string v2, "style_list"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1870326
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1870327
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1870328
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1870329
    check-cast p1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$Serializer;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
