.class public final Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1869455
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel;

    new-instance v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1869456
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1869457
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1869458
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1869459
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x3

    const/4 v3, 0x0

    .line 1869460
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1869461
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1869462
    if-eqz v2, :cond_0

    .line 1869463
    const-string v2, "document_element_type"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1869464
    invoke-virtual {v1, v0, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1869465
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1869466
    if-eqz v2, :cond_1

    .line 1869467
    const-string v3, "element_descriptor"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1869468
    invoke-static {v1, v2, p1, p2}, LX/CI9;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1869469
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1869470
    if-eqz v2, :cond_2

    .line 1869471
    const-string v3, "footer_elements"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1869472
    invoke-static {v1, v2, p1, p2}, LX/CIC;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1869473
    :cond_2
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1869474
    if-eqz v2, :cond_3

    .line 1869475
    const-string v2, "style_list"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1869476
    invoke-virtual {v1, v0, p0}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2, p1}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 1869477
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1869478
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1869479
    check-cast p1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel$Serializer;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
