.class public final Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x1da36f66
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1868277
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1868276
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1868274
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1868275
    return-void
.end method

.method private j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAction"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1868272
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    .line 1868273
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    return-object v0
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1868270
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1868271
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1868256
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1868257
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1868258
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1868259
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1868260
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1868261
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1868262
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1868263
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1868264
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1868265
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1868266
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1868267
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1868268
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1868269
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1868278
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1868279
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1868280
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    .line 1868281
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1868282
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;

    .line 1868283
    iput-object v0, v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    .line 1868284
    :cond_0
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1868285
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1868286
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1868287
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;

    .line 1868288
    iput-object v0, v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->g:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1868289
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1868290
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1868254
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->f:Ljava/lang/String;

    .line 1868255
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic b()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1868253
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1868250
    new-instance v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;

    invoke-direct {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;-><init>()V

    .line 1868251
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1868252
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1868248
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->h:Ljava/lang/String;

    .line 1868249
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1868246
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->i:Ljava/lang/String;

    .line 1868247
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1868245
    const v0, -0x28be8e0

    return v0
.end method

.method public final synthetic e()LX/CHX;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAction"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1868244
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1868243
    const v0, 0x55b9164a

    return v0
.end method
