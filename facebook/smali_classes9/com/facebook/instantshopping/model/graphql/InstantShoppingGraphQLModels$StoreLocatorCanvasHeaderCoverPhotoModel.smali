.class public final Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x49274588
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1871345
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1871312
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1871343
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1871344
    return-void
.end method

.method private j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1871341
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1871342
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1871333
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1871334
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1871335
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1871336
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1871337
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1871338
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1871339
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1871340
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1871320
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1871321
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1871322
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1871323
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1871324
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    .line 1871325
    iput-object v0, v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->e:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1871326
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1871327
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    .line 1871328
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1871329
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    .line 1871330
    iput-object v0, v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->f:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    .line 1871331
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1871332
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1871318
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->f:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->f:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    .line 1871319
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;->f:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel$PhotoModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1871315
    new-instance v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;-><init>()V

    .line 1871316
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1871317
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1871314
    const v0, 0x896f34c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1871313
    const v0, 0x1da3a91b

    return v0
.end method
