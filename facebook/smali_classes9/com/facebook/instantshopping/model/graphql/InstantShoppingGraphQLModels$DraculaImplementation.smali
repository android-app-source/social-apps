.class public final Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1867883
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1867884
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1867881
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1867882
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 12

    .prologue
    .line 1867776
    if-nez p1, :cond_0

    .line 1867777
    const/4 v0, 0x0

    .line 1867778
    :goto_0
    return v0

    .line 1867779
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1867780
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1867781
    :sswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1867782
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, LX/15i;->a(III)I

    move-result v1

    .line 1867783
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1867784
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v0, v3}, LX/186;->a(III)V

    .line 1867785
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {p3, v0, v1, v2}, LX/186;->a(III)V

    .line 1867786
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1867787
    :sswitch_1
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    .line 1867788
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1867789
    const/4 v1, 0x2

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    move-result-object v1

    .line 1867790
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1867791
    const/4 v2, 0x3

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1867792
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1867793
    const/4 v3, 0x4

    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v3

    .line 1867794
    const/4 v4, 0x5

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1867795
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1867796
    const/4 v5, 0x6

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1867797
    const/4 v5, 0x1

    invoke-virtual {p3, v5, v0}, LX/186;->b(II)V

    .line 1867798
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1867799
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1867800
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v3}, LX/186;->a(IZ)V

    .line 1867801
    const/4 v0, 0x5

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1867802
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1867803
    :sswitch_2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 1867804
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1867805
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1867806
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1867807
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1867808
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1867809
    const/4 v4, 0x4

    invoke-virtual {p0, p1, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 1867810
    invoke-virtual {p3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1867811
    const/4 v5, 0x5

    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1867812
    const/4 v5, 0x0

    invoke-virtual {p3, v5, v0}, LX/186;->a(IZ)V

    .line 1867813
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1867814
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1867815
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1867816
    const/4 v0, 0x4

    invoke-virtual {p3, v0, v4}, LX/186;->b(II)V

    .line 1867817
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1867818
    :sswitch_3
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;

    .line 1867819
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1867820
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->b(II)Z

    move-result v1

    .line 1867821
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1867822
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1867823
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1867824
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 1867825
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1867826
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1867827
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1867828
    :sswitch_4
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1867829
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1867830
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 1867831
    const/4 v0, 0x3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 1867832
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1867833
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1867834
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1867835
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1867836
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1867837
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1867838
    :sswitch_5
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    .line 1867839
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1867840
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v1

    .line 1867841
    const v2, 0x6e7f593e

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1867842
    const/4 v2, 0x2

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1867843
    const/4 v2, 0x0

    invoke-virtual {p3, v2, v0}, LX/186;->b(II)V

    .line 1867844
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1867845
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1867846
    :sswitch_6
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1867847
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1867848
    const/4 v0, 0x1

    const-class v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    .line 1867849
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1867850
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1867851
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1867852
    const/4 v3, 0x3

    invoke-virtual {p0, p1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 1867853
    invoke-virtual {p3, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1867854
    const/4 v4, 0x4

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1867855
    const/4 v4, 0x0

    invoke-virtual {p3, v4, v1}, LX/186;->b(II)V

    .line 1867856
    const/4 v1, 0x1

    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1867857
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1867858
    const/4 v0, 0x3

    invoke-virtual {p3, v0, v3}, LX/186;->b(II)V

    .line 1867859
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1867860
    :sswitch_7
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1867861
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1867862
    const/4 v1, 0x1

    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLRichMediaStoreLocatorElementTheme;

    move-result-object v1

    .line 1867863
    invoke-virtual {p3, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1867864
    const/4 v2, 0x2

    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1867865
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1867866
    const/4 v3, 0x3

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1867867
    const/4 v3, 0x0

    invoke-virtual {p3, v3, v0}, LX/186;->b(II)V

    .line 1867868
    const/4 v0, 0x1

    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1867869
    const/4 v0, 0x2

    invoke-virtual {p3, v0, v2}, LX/186;->b(II)V

    .line 1867870
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1867871
    :sswitch_8
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v2

    .line 1867872
    const/4 v0, 0x1

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v6

    .line 1867873
    const/4 v0, 0x2

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v8

    .line 1867874
    const/4 v0, 0x3

    const-wide/16 v4, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v10

    .line 1867875
    const/4 v0, 0x4

    invoke-virtual {p3, v0}, LX/186;->c(I)V

    .line 1867876
    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    move-object v0, p3

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1867877
    const/4 v1, 0x1

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v6

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1867878
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v8

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1867879
    const/4 v1, 0x3

    const-wide/16 v4, 0x0

    move-object v0, p3

    move-wide v2, v10

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 1867880
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x78659a2f -> :sswitch_5
        -0x5af16748 -> :sswitch_7
        -0x3cc565a1 -> :sswitch_3
        -0x1ecba872 -> :sswitch_0
        0xb793ac9 -> :sswitch_6
        0x57f7d422 -> :sswitch_2
        0x5c9c77ce -> :sswitch_1
        0x6b0d1088 -> :sswitch_4
        0x6e7f593e -> :sswitch_8
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1867767
    if-nez p0, :cond_0

    move v0, v1

    .line 1867768
    :goto_0
    return v0

    .line 1867769
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1867770
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1867771
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1867772
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1867773
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1867774
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1867775
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1867760
    const/4 v7, 0x0

    .line 1867761
    const/4 v1, 0x0

    .line 1867762
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1867763
    invoke-static {v2, v3, v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1867764
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1867765
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1867766
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1867759
    new-instance v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1867754
    if-eqz p0, :cond_0

    .line 1867755
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1867756
    if-eq v0, p0, :cond_0

    .line 1867757
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1867758
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1867741
    sparse-switch p2, :sswitch_data_0

    .line 1867742
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1867743
    :sswitch_0
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    .line 1867744
    invoke-static {v0, p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1867745
    :goto_0
    :sswitch_1
    return-void

    .line 1867746
    :sswitch_2
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingOptionsActionFragmentModel;

    .line 1867747
    invoke-static {v0, p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    .line 1867748
    :sswitch_3
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-virtual {p0, p1, v2, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    .line 1867749
    invoke-static {v0, p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1867750
    invoke-virtual {p0, p1, v1}, LX/15i;->p(II)I

    move-result v0

    .line 1867751
    const v1, 0x6e7f593e

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1867752
    :sswitch_4
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$StoreLocatorCanvasHeaderCoverPhotoModel;

    .line 1867753
    invoke-static {v0, p3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x78659a2f -> :sswitch_3
        -0x5af16748 -> :sswitch_1
        -0x3cc565a1 -> :sswitch_2
        -0x1ecba872 -> :sswitch_1
        0xb793ac9 -> :sswitch_4
        0x57f7d422 -> :sswitch_1
        0x5c9c77ce -> :sswitch_0
        0x6b0d1088 -> :sswitch_1
        0x6e7f593e -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1867735
    if-eqz p1, :cond_0

    .line 1867736
    invoke-static {p0, p1, p2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1867737
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    .line 1867738
    if-eq v0, v1, :cond_0

    .line 1867739
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1867740
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1867885
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1867733
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1867734
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1867728
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1867729
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1867730
    :cond_0
    iput-object p1, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1867731
    iput p2, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->b:I

    .line 1867732
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1867727
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1867726
    new-instance v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1867723
    iget v0, p0, LX/1vt;->c:I

    .line 1867724
    move v0, v0

    .line 1867725
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1867720
    iget v0, p0, LX/1vt;->c:I

    .line 1867721
    move v0, v0

    .line 1867722
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1867717
    iget v0, p0, LX/1vt;->b:I

    .line 1867718
    move v0, v0

    .line 1867719
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1867714
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1867715
    move-object v0, v0

    .line 1867716
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1867705
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1867706
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1867707
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1867708
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1867709
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1867710
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1867711
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1867712
    invoke-static {v3, v9, v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1867713
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1867702
    iget v0, p0, LX/1vt;->c:I

    .line 1867703
    move v0, v0

    .line 1867704
    return v0
.end method
