.class public final Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/CHs;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x762e3fc6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1870379
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1870384
    const-class v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1870382
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1870383
    return-void
.end method

.method private a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1870380
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    .line 1870381
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->f:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    return-object v0
.end method

.method private j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1870349
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->g:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->g:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    .line 1870350
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->g:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1870377
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->j:Ljava/lang/String;

    .line 1870378
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->j:Ljava/lang/String;

    return-object v0
.end method

.method private l()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1870375
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->k:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->k:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    .line 1870376
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->k:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    return-object v0
.end method

.method private m()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1870373
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->m:Ljava/util/List;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->m:Ljava/util/List;

    .line 1870374
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->m:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    .line 1870351
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1870352
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->u()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, 0x6b0d1088

    invoke-static {v1, v0, v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1870353
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1870354
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1870355
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->v()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, 0xb793ac9    # 4.799988E-32f

    invoke-static {v4, v3, v5}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1870356
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->s()LX/0Px;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v4

    .line 1870357
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->k()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 1870358
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->l()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1870359
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->w()LX/1vs;

    move-result-object v7

    iget-object v8, v7, LX/1vs;->a:LX/15i;

    iget v7, v7, LX/1vs;->b:I

    const v9, -0x5af16748

    invoke-static {v8, v7, v9}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1870360
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->m()LX/0Px;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->c(Ljava/util/List;)I

    move-result v8

    .line 1870361
    const/16 v9, 0x9

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 1870362
    const/4 v9, 0x0

    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1870363
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1870364
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1870365
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1870366
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1870367
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1870368
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1870369
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1870370
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1870371
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1870372
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1870385
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1870386
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->u()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1870387
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->u()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x6b0d1088

    invoke-static {v2, v0, v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1870388
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->u()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1870389
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    .line 1870390
    iput v3, v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->e:I

    move-object v1, v0

    .line 1870391
    :cond_0
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1870392
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    .line 1870393
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1870394
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    .line 1870395
    iput-object v0, v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->g:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingElementDescriptorWithoutTextFragmentModel;

    .line 1870396
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1870397
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0xb793ac9    # 4.799988E-32f

    invoke-static {v2, v0, v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1870398
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->v()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1870399
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    .line 1870400
    iput v3, v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->h:I

    move-object v1, v0

    .line 1870401
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->s()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1870402
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->s()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1870403
    if-eqz v2, :cond_3

    .line 1870404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    .line 1870405
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->i:Ljava/util/List;

    move-object v1, v0

    .line 1870406
    :cond_3
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->l()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1870407
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->l()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    .line 1870408
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->l()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1870409
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    .line 1870410
    iput-object v0, v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->k:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    .line 1870411
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->w()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_5

    .line 1870412
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->w()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5af16748

    invoke-static {v2, v0, v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1870413
    invoke-virtual {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->w()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1870414
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    .line 1870415
    iput v3, v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->l:I

    move-object v1, v0

    .line 1870416
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1870417
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    .line 1870418
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1870419
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1870420
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_6
    move-object p0, v1

    .line 1870421
    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1870330
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1870331
    const/4 v0, 0x0

    const v1, 0x6b0d1088

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->e:I

    .line 1870332
    const/4 v0, 0x3

    const v1, 0xb793ac9    # 4.799988E-32f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->h:I

    .line 1870333
    const/4 v0, 0x7

    const v1, -0x5af16748

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->l:I

    .line 1870334
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1870335
    new-instance v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;

    invoke-direct {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;-><init>()V

    .line 1870336
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1870337
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1870338
    const v0, 0x3b70ad0a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1870339
    const v0, 0x3881bfa3

    return v0
.end method

.method public final s()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1870340
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->i:Ljava/util/List;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/storelocator/graphql/StoreLocatorQueryModels$StoreLocationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->i:Ljava/util/List;

    .line 1870341
    iget-object v0, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->i:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final synthetic t()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1870342
    invoke-direct {p0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->l()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel$PageSetModel;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBoundingBox"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1870343
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1870344
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final v()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getHeader"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1870345
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1870346
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final w()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getStyle"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1870347
    const/4 v0, 0x0

    const/4 v1, 0x7

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1870348
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingStoreLocatorElementFragmentModel;->l:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
