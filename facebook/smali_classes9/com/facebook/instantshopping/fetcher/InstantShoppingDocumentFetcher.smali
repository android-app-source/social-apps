.class public Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;
.super LX/CH4;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/CH4",
        "<TT;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile m:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;


# instance fields
.field private final c:LX/0tX;

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/8bW;

.field private final g:LX/0So;

.field public final h:LX/03V;

.field public final i:LX/8Yg;

.field public final j:LX/8Yh;

.field private final k:LX/0ad;

.field private l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1867608
    const-class v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a:Ljava/lang/String;

    .line 1867609
    const-class v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;LX/0So;LX/8bW;LX/03V;LX/8Yg;LX/8Yh;LX/0ad;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 1867595
    invoke-direct {p0, p1}, LX/CH4;-><init>(LX/0tX;)V

    .line 1867596
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->d:Ljava/util/Map;

    .line 1867597
    new-instance v0, Landroid/util/LruCache;

    const/16 v1, 0x10

    invoke-direct {v0, v1}, Landroid/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    .line 1867598
    iput-boolean v2, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->l:Z

    .line 1867599
    iput-object p2, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->g:LX/0So;

    .line 1867600
    iput-object p1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->c:LX/0tX;

    .line 1867601
    iput-object p3, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    .line 1867602
    iput-object p4, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->h:LX/03V;

    .line 1867603
    iput-object p5, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->i:LX/8Yg;

    .line 1867604
    iput-object p6, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->j:LX/8Yh;

    .line 1867605
    iput-object p7, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->k:LX/0ad;

    .line 1867606
    iget-object v0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->k:LX/0ad;

    sget-short v1, LX/CHN;->h:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->l:Z

    .line 1867607
    return-void
.end method

.method public static a(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/CId;)LX/CHQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/CId;",
            ")",
            "Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher",
            "<TT;>.PrefetchMonitor;"
        }
    .end annotation

    .prologue
    .line 1867581
    iget-object v0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->k:LX/0ad;

    sget-short v1, LX/CHN;->o:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1867582
    const/4 v0, 0x0

    .line 1867583
    :goto_0
    return-object v0

    .line 1867584
    :cond_0
    new-instance v0, LX/CId;

    invoke-direct {v0, p1, p2}, LX/CId;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1867585
    iput-object p3, v0, LX/CId;->f:Ljava/lang/String;

    .line 1867586
    iput-object p4, v0, LX/CId;->a:Ljava/lang/String;

    .line 1867587
    iput-object p5, v0, LX/CId;->b:Ljava/lang/String;

    .line 1867588
    iget-object v1, p6, LX/CId;->l:LX/0zS;

    move-object v1, v1

    .line 1867589
    iput-object v1, v0, LX/CId;->l:LX/0zS;

    .line 1867590
    iget v1, p6, LX/CId;->k:I

    move v1, v1

    .line 1867591
    iput v1, v0, LX/CId;->k:I

    .line 1867592
    const/4 v1, 0x1

    .line 1867593
    iput-boolean v1, v0, LX/CId;->g:Z

    .line 1867594
    invoke-static {p0, p2, v0}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;Ljava/lang/String;LX/CId;)LX/CHQ;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;Ljava/lang/String;LX/CId;)LX/CHQ;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/CId;",
            ")",
            "Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher",
            "<TT;>.PrefetchMonitor;"
        }
    .end annotation

    .prologue
    .line 1867610
    iget-object v0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->k:LX/0ad;

    sget v1, LX/CHN;->j:I

    const/4 v2, 0x5

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    .line 1867611
    new-instance v1, LX/CHQ;

    invoke-direct {v1, p0, p2}, LX/CHQ;-><init>(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;LX/CId;)V

    .line 1867612
    new-instance v2, LX/CHP;

    invoke-direct {v2, p0, v0, v1, p1}, LX/CHP;-><init>(Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;ILX/CHQ;Ljava/lang/String;)V

    invoke-virtual {p0, p2, v2}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    .line 1867613
    return-object v1
.end method

.method public static a(LX/0QB;)Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;
    .locals 11

    .prologue
    .line 1867568
    sget-object v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->m:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    if-nez v0, :cond_1

    .line 1867569
    const-class v1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    monitor-enter v1

    .line 1867570
    :try_start_0
    sget-object v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->m:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1867571
    if-eqz v2, :cond_0

    .line 1867572
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1867573
    new-instance v3, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {v0}, LX/8bW;->b(LX/0QB;)LX/8bW;

    move-result-object v6

    check-cast v6, LX/8bW;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/8Yg;->a(LX/0QB;)LX/8Yg;

    move-result-object v8

    check-cast v8, LX/8Yg;

    invoke-static {v0}, LX/8Yh;->a(LX/0QB;)LX/8Yh;

    move-result-object v9

    check-cast v9, LX/8Yh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;-><init>(LX/0tX;LX/0So;LX/8bW;LX/03V;LX/8Yg;LX/8Yh;LX/0ad;)V

    .line 1867574
    move-object v0, v3

    .line 1867575
    sput-object v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->m:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1867576
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1867577
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1867578
    :cond_1
    sget-object v0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->m:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    return-object v0

    .line 1867579
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1867580
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1867567
    iget-object v0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final a(LX/CGs;LX/0Ve;)V
    .locals 5
    .param p1    # LX/CGs;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/0Ve;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CGs",
            "<",
            "LX/0zO",
            "<TT;>;>;",
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 1867513
    instance-of v0, p1, LX/CId;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 1867514
    check-cast v0, LX/CId;

    .line 1867515
    iget-object v1, v0, LX/CId;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1867516
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1867517
    iget-object v1, v0, LX/CId;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1867518
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1867519
    iget-object v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    .line 1867520
    iget-object v2, v0, LX/CId;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1867521
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "instantshopping_document_fetch_query?product_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1867522
    iget-object v4, v0, LX/CId;->a:Ljava/lang/String;

    move-object v4, v4

    .line 1867523
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&product_view="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1867524
    iget-object v4, v0, LX/CId;->b:Ljava/lang/String;

    move-object v4, v4

    .line 1867525
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1867526
    :goto_0
    iget-object v1, v0, LX/CId;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1867527
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1867528
    iget-object v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    .line 1867529
    iget-object v2, v0, LX/CId;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1867530
    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1867531
    :goto_1
    move-object v1, v1

    .line 1867532
    iput-object v1, v0, LX/CId;->e:Ljava/lang/String;

    .line 1867533
    iget-boolean v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->l:Z

    .line 1867534
    iput-boolean v1, v0, LX/CId;->h:Z

    .line 1867535
    :cond_0
    invoke-super {p0, p1, p2}, LX/CH4;->a(LX/CGs;LX/0Ve;)V

    .line 1867536
    return-void

    .line 1867537
    :cond_1
    iget-object v1, v0, LX/CId;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1867538
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1867539
    iget-object v1, v0, LX/CId;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1867540
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1867541
    iget-object v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    .line 1867542
    iget-object v2, v0, LX/CId;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1867543
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "instantshopping_document_fetch_query?catalog_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1867544
    iget-object v4, v0, LX/CId;->c:Ljava/lang/String;

    move-object v4, v4

    .line 1867545
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "&catalog_view="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 1867546
    iget-object v4, v0, LX/CId;->f:Ljava/lang/String;

    move-object v4, v4

    .line 1867547
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1867548
    :cond_2
    iget-object v1, v0, LX/CId;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1867549
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1867550
    iget-object v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    .line 1867551
    iget-object v2, v0, LX/CId;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1867552
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "instantshopping_document_fetch_query?native_document_id="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1867553
    iget-object v4, v0, LX/CId;->d:Ljava/lang/String;

    move-object v4, v4

    .line 1867554
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1867555
    :cond_3
    iget-object v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    const-string v2, "instantshopping_document_fetch_query"

    const-string v3, "instantshopping_document_fetch_query"

    invoke-virtual {v1, v2, v3}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 1867556
    :cond_4
    iget-object v1, v0, LX/CId;->c:Ljava/lang/String;

    move-object v1, v1

    .line 1867557
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1867558
    iget-object v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    .line 1867559
    iget-object v2, v0, LX/CId;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1867560
    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto/16 :goto_1

    .line 1867561
    :cond_5
    iget-object v1, v0, LX/CId;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1867562
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1867563
    iget-object v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    .line 1867564
    iget-object v2, v0, LX/CId;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1867565
    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto/16 :goto_1

    .line 1867566
    :cond_6
    iget-object v1, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->e:Landroid/util/LruCache;

    const-string v2, "instantshopping_document_fetch_query"

    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto/16 :goto_1
.end method
