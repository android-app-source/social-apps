.class public Lcom/facebook/composer/ui/text/ComposerEditText;
.super Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;
.source ""

# interfaces
.implements LX/Be2;
.implements LX/8oi;


# instance fields
.field public b:Z

.field public final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Be0;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/text/TextWatcher;

.field private e:Landroid/view/View;

.field public f:Z

.field private g:Ljava/lang/String;

.field private final h:Landroid/text/style/ForegroundColorSpan;

.field private i:Landroid/text/Editable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1804453
    invoke-direct {p0, p1, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1804454
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->c:Ljava/util/Set;

    .line 1804455
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->f:Z

    .line 1804456
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getHintTextColors()Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->h:Landroid/text/style/ForegroundColorSpan;

    .line 1804457
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, LX/Bdy;

    invoke-direct {v1, p0}, LX/Bdy;-><init>(Lcom/facebook/composer/ui/text/ComposerEditText;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1804458
    new-instance v0, LX/Be3;

    invoke-direct {v0}, LX/Be3;-><init>()V

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 1804459
    new-instance v0, LX/Bdz;

    invoke-direct {v0, p0}, LX/Bdz;-><init>(Lcom/facebook/composer/ui/text/ComposerEditText;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->d:Landroid/text/TextWatcher;

    .line 1804460
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->d:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1804461
    iput-object p0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->b:LX/8oi;

    .line 1804462
    return-void
.end method

.method public static d(Lcom/facebook/composer/ui/text/ComposerEditText;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1804463
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 1804464
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1804465
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1804466
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->i:Landroid/text/Editable;

    .line 1804467
    iget-object v2, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->h:Landroid/text/style/ForegroundColorSpan;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1804468
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->i:Landroid/text/Editable;

    invoke-virtual {v1, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1804469
    invoke-virtual {p0, v1}, Lcom/facebook/composer/ui/text/ComposerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1804470
    :cond_0
    return-void
.end method

.method public static e(Lcom/facebook/composer/ui/text/ComposerEditText;)V
    .locals 2

    .prologue
    .line 1804471
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1804472
    iget-object v1, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->i:Landroid/text/Editable;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1804473
    iget-object v1, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->h:Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->removeSpan(Ljava/lang/Object;)V

    .line 1804474
    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1804475
    return-void
.end method

.method private f()V
    .locals 4

    .prologue
    .line 1804476
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->isPopupShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->e:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1804477
    :cond_0
    :goto_0
    return-void

    .line 1804478
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getSelectionStart()I

    move-result v0

    .line 1804479
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 1804480
    if-eqz v1, :cond_0

    .line 1804481
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineForOffset(I)I

    move-result v0

    .line 1804482
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineBaseline(I)I

    move-result v2

    .line 1804483
    invoke-virtual {v1, v0}, Landroid/text/Layout;->getLineAscent(I)I

    move-result v0

    .line 1804484
    add-int/2addr v0, v2

    .line 1804485
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getLineHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    .line 1804486
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getLineCount()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getHeight()I

    move-result v2

    if-eq v2, v1, :cond_2

    .line 1804487
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getHeight()I

    move-result v2

    sub-int/2addr v2, v1

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 1804488
    :cond_2
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v2, v3, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1804489
    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1804490
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private getEndIndex()I
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x0

    .line 1804510
    invoke-static {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getMetaHintIndex(Lcom/facebook/composer/ui/text/ComposerEditText;)I

    move-result v0

    .line 1804511
    iget-boolean v1, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->f:Z

    if-eqz v1, :cond_0

    if-eq v0, v7, :cond_0

    .line 1804512
    :goto_0
    return v0

    .line 1804513
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v5

    .line 1804514
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v2, LX/8zE;

    invoke-interface {v0, v4, v1, v2}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/8zE;

    .line 1804515
    invoke-interface {v5}, Landroid/text/Editable;->length()I

    move-result v2

    .line 1804516
    array-length v6, v0

    move v3, v4

    :goto_1
    if-ge v3, v6, :cond_1

    aget-object v1, v0, v3

    .line 1804517
    invoke-interface {v5, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v1

    .line 1804518
    if-eq v1, v7, :cond_2

    if-ge v1, v2, :cond_2

    .line 1804519
    :goto_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_1

    .line 1804520
    :cond_1
    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public static getMetaHintIndex(Lcom/facebook/composer/ui/text/ComposerEditText;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 1804491
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    .line 1804492
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v3

    .line 1804493
    if-eqz v3, :cond_1

    .line 1804494
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1804495
    if-eq v0, v1, :cond_1

    .line 1804496
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->g:Ljava/lang/String;

    .line 1804497
    :cond_0
    :goto_0
    return v0

    .line 1804498
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1804499
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 1804500
    if-ne v0, v1, :cond_0

    :cond_2
    move v0, v1

    .line 1804501
    goto :goto_0
.end method

.method public static getTextWithEntities(Lcom/facebook/composer/ui/text/ComposerEditText;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1804502
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getUserText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1804503
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v2}, LX/8oj;->a(Landroid/text/Editable;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0, v3, v3}, LX/16z;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1804504
    invoke-static {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getTextWithEntities(Lcom/facebook/composer/ui/text/ComposerEditText;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1804505
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Be0;

    .line 1804506
    invoke-interface {v0, v1}, LX/Be0;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)V

    goto :goto_0

    .line 1804507
    :cond_0
    return-void
.end method

.method public final a(LX/Be0;)V
    .locals 1

    .prologue
    .line 1804508
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1804509
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1804448
    iput-object p1, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->e:Landroid/view/View;

    .line 1804449
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/text/ComposerEditText;->setDropDownAnchor(I)V

    .line 1804450
    return-void
.end method

.method public final b(LX/Be0;)V
    .locals 1

    .prologue
    .line 1804451
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1804452
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1804444
    iget-boolean v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->f:Z

    if-nez v0, :cond_1

    .line 1804445
    :cond_0
    :goto_0
    return-void

    .line 1804446
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getMetaHintIndex(Lcom/facebook/composer/ui/text/ComposerEditText;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 1804447
    invoke-static {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->e(Lcom/facebook/composer/ui/text/ComposerEditText;)V

    goto :goto_0
.end method

.method public final extendSelection(I)V
    .locals 1

    .prologue
    .line 1804441
    invoke-direct {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getEndIndex()I

    move-result v0

    .line 1804442
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->extendSelection(I)V

    .line 1804443
    return-void
.end method

.method public getUserText()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 1804440
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getEndIndex()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final onSelectionChanged(II)V
    .locals 1

    .prologue
    .line 1804435
    invoke-super {p0, p1, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->onSelectionChanged(II)V

    .line 1804436
    invoke-direct {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getEndIndex()I

    move-result v0

    .line 1804437
    if-ltz p2, :cond_0

    if-ge p2, v0, :cond_0

    if-ltz p1, :cond_0

    if-lt p1, v0, :cond_1

    .line 1804438
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/facebook/composer/ui/text/ComposerEditText;->setSelection(II)V

    .line 1804439
    :cond_1
    return-void
.end method

.method public final onTextContextMenuItem(I)Z
    .locals 1

    .prologue
    .line 1804432
    const v0, 0x1020022

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->b:Z

    .line 1804433
    invoke-super {p0, p1}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->onTextContextMenuItem(I)Z

    move-result v0

    return v0

    .line 1804434
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final performFiltering(Ljava/lang/CharSequence;I)V
    .locals 0

    .prologue
    .line 1804416
    invoke-direct {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->f()V

    .line 1804417
    invoke-super {p0, p1, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->performFiltering(Ljava/lang/CharSequence;I)V

    .line 1804418
    return-void
.end method

.method public setBottomMargin(I)V
    .locals 4

    .prologue
    .line 1804427
    invoke-virtual {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1804428
    instance-of v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v1, :cond_0

    .line 1804429
    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1804430
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    invoke-virtual {v0, v1, v2, v3, p1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 1804431
    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 1804424
    invoke-direct {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getEndIndex()I

    move-result v0

    .line 1804425
    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setSelection(I)V

    .line 1804426
    return-void
.end method

.method public final setSelection(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1804421
    invoke-direct {p0}, Lcom/facebook/composer/ui/text/ComposerEditText;->getEndIndex()I

    move-result v0

    .line 1804422
    invoke-static {v2, p1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, p2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->setSelection(II)V

    .line 1804423
    return-void
.end method

.method public setShowPersistentHint(Z)V
    .locals 0

    .prologue
    .line 1804419
    iput-boolean p1, p0, Lcom/facebook/composer/ui/text/ComposerEditText;->f:Z

    .line 1804420
    return-void
.end method
