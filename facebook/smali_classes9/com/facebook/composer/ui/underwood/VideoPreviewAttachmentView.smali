.class public Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/BTf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/video/player/RichVideoPlayer;

.field public e:LX/7OL;

.field public f:Lcom/facebook/video/player/plugins/VideoPlugin;

.field public g:Lcom/facebook/video/player/plugins/CoverImagePlugin;

.field public h:Landroid/view/ViewStub;

.field public i:Landroid/view/View;

.field public j:Landroid/view/View;

.field public k:Lcom/facebook/widget/FbImageView;

.field public l:Lcom/facebook/resources/ui/FbFrameLayout;

.field public m:Lcom/facebook/resources/ui/FbTextView;

.field public n:LX/ATs;

.field public o:F

.field public p:F

.field public q:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1677024
    const-class v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1677022
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1677023
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1677020
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1677021
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1676997
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1676998
    const-class v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-static {v0, p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1676999
    const v0, 0x7f03156f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1677000
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1677001
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, LX/ATm;

    invoke-direct {p1, p0}, LX/ATm;-><init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V

    invoke-virtual {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1677002
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->f:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 1677003
    new-instance v0, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object p2, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v0, p1, p2}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->g:Lcom/facebook/video/player/plugins/CoverImagePlugin;

    .line 1677004
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->f:Lcom/facebook/video/player/plugins/VideoPlugin;

    .line 1677005
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1677006
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->g:Lcom/facebook/video/player/plugins/CoverImagePlugin;

    .line 1677007
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1677008
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance p1, LX/7Nb;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-direct {p1, p2}, LX/7Nb;-><init>(Landroid/content/Context;)V

    .line 1677009
    invoke-static {v0, p1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1677010
    const v0, 0x7f0d3025

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->h:Landroid/view/ViewStub;

    .line 1677011
    const v0, 0x7f0d0a7e

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->j:Landroid/view/View;

    .line 1677012
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->j:Landroid/view/View;

    new-instance p1, LX/ATn;

    invoke-direct {p1, p0}, LX/ATn;-><init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1677013
    const v0, 0x7f0d3038

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/FbImageView;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->k:Lcom/facebook/widget/FbImageView;

    .line 1677014
    const v0, 0x7f0d3039

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbFrameLayout;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l:Lcom/facebook/resources/ui/FbFrameLayout;

    .line 1677015
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l:Lcom/facebook/resources/ui/FbFrameLayout;

    new-instance p1, LX/ATo;

    invoke-direct {p1, p0}, LX/ATo;-><init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1677016
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l:Lcom/facebook/resources/ui/FbFrameLayout;

    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    .line 1677017
    const v0, 0x7f0d303a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1677018
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->p:F

    .line 1677019
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-static {p0}, LX/BTf;->b(LX/0QB;)LX/BTf;

    move-result-object p0

    check-cast p0, LX/BTf;

    iput-object v1, p1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->a:LX/0ad;

    iput-object p0, p1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->b:LX/BTf;

    return-void
.end method

.method public static l(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V
    .locals 3

    .prologue
    .line 1676946
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1676947
    :goto_0
    return-void

    .line 1676948
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1676949
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    if-eqz v1, :cond_2

    const v1, 0x800033

    :goto_1
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 1676950
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    if-eqz v1, :cond_1

    .line 1676951
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0ccb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1676952
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1676953
    :cond_2
    const v1, 0x800053

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1676992
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1676993
    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    .line 1676994
    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    .line 1676995
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->h()V

    .line 1676996
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 1676979
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1676980
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->n:LX/ATs;

    if-eqz v0, :cond_1

    .line 1676981
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->n:LX/ATs;

    .line 1676982
    iget-object v1, v0, LX/ATs;->h:LX/ATL;

    iget-object v2, v0, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1676983
    iget-object v3, v1, LX/ATL;->a:LX/ATO;

    iget-object v3, v3, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1676984
    const/4 v3, 0x0

    move v4, v3

    :goto_0
    iget-object v3, v1, LX/ATL;->a:LX/ATO;

    iget-object v3, v3, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_1

    .line 1676985
    if-eq v4, v0, :cond_0

    .line 1676986
    iget-object v3, v1, LX/ATL;->a:LX/ATO;

    iget-object v3, v3, LX/ATO;->b:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/ASn;

    invoke-interface {v3}, LX/ASn;->e()V

    .line 1676987
    :cond_0
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    .line 1676988
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz p1, :cond_3

    sget-object v0, LX/04g;->BY_USER:LX/04g;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1676989
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->k:Lcom/facebook/widget/FbImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1676990
    return-void

    .line 1676991
    :cond_3
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 1676974
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1676975
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz p1, :cond_1

    sget-object v0, LX/04g;->BY_USER:LX/04g;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1676976
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->k:Lcom/facebook/widget/FbImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1676977
    return-void

    .line 1676978
    :cond_1
    sget-object v0, LX/04g;->BY_PLAYER:LX/04g;

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 1676967
    new-instance v0, LX/7OL;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/7OL;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    .line 1676968
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    new-instance v1, LX/ATq;

    invoke-direct {v1, p0}, LX/ATq;-><init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V

    .line 1676969
    iput-object v1, v0, LX/7OL;->a:LX/ATq;

    .line 1676970
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    .line 1676971
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1676972
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V

    .line 1676973
    return-void
.end method

.method public getCurrentPositionMs()I
    .locals 1

    .prologue
    .line 1676966
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->d:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1676964
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l:Lcom/facebook/resources/ui/FbFrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    .line 1676965
    return-void
.end method

.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 1676956
    iget v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->q:I

    rem-int/lit16 v0, v0, 0xb4

    if-nez v0, :cond_0

    iget v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->o:F

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    new-instance v2, LX/ATB;

    invoke-direct {v2, p1, p2}, LX/ATB;-><init>(II)V

    invoke-static {v1, v0, v2}, LX/ATC;->a(FLandroid/widget/FrameLayout$LayoutParams;LX/ATB;)LX/ATB;

    move-result-object v0

    .line 1676957
    iget v1, v0, LX/ATB;->a:I

    iget v2, v0, LX/ATB;->b:I

    invoke-virtual {p0, v1, v2}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->setMeasuredDimension(II)V

    .line 1676958
    iget v1, v0, LX/ATB;->a:I

    iget v0, v0, LX/ATB;->b:I

    invoke-virtual {p0, v1, v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->measureChildren(II)V

    .line 1676959
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->g:Lcom/facebook/video/player/plugins/CoverImagePlugin;

    iget v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->q:I

    int-to-float v1, v1

    .line 1676960
    iget-object v2, v0, LX/2pH;->a:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object p1, v0, LX/2oy;->g:Landroid/view/ViewGroup;

    invoke-static {v2, p1, v1}, LX/2pC;->a(Landroid/view/View;Landroid/view/View;F)V

    .line 1676961
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->f:Lcom/facebook/video/player/plugins/VideoPlugin;

    iget v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->q:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/plugins/VideoPlugin;->setVideoRotation(F)V

    .line 1676962
    return-void

    .line 1676963
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->o:F

    div-float/2addr v0, v1

    move v1, v0

    goto :goto_0
.end method

.method public setListener(LX/ATs;)V
    .locals 0

    .prologue
    .line 1676954
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->n:LX/ATs;

    .line 1676955
    return-void
.end method
