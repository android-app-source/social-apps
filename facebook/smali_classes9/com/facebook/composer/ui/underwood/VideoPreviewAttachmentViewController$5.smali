.class public final Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/ATs;


# direct methods
.method public constructor <init>(LX/ATs;J)V
    .locals 0

    .prologue
    .line 1677096
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$5;->b:LX/ATs;

    iput-wide p2, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$5;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1677097
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$5;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->h:LX/ATL;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$5;->b:LX/ATs;

    iget-object v1, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-wide v2, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$5;->a:J

    .line 1677098
    iget-object v4, v0, LX/ATL;->a:LX/ATO;

    iget-object v4, v4, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 1677099
    const/4 v5, -0x1

    if-ne v4, v5, :cond_0

    .line 1677100
    :goto_0
    return-void

    .line 1677101
    :cond_0
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v5

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v6

    invoke-static {v6}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v6

    const/4 p0, 0x1

    invoke-virtual {v6, p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setHasFaceboxes(Z)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setTimeToFindFirstFaceMs(J)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v6

    .line 1677102
    iput-object v6, v5, LX/7kv;->g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1677103
    move-object v5, v5

    .line 1677104
    invoke-virtual {v5}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v5

    .line 1677105
    invoke-static {v0, v4, v5}, LX/ATL;->a(LX/ATL;ILcom/facebook/composer/attachments/ComposerAttachment;)V

    goto :goto_0
.end method
