.class public final Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

.field public final synthetic b:LX/ATs;


# direct methods
.method public constructor <init>(LX/ATs;Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;)V
    .locals 0

    .prologue
    .line 1677106
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->b:LX/ATs;

    iput-object p2, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->a:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1677107
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->h:LX/ATL;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->b:LX/ATs;

    iget-object v1, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->a:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    .line 1677108
    iget-object v4, v0, LX/ATL;->a:LX/ATO;

    iget-object v4, v4, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 1677109
    const/4 v5, -0x1

    if-ne v4, v5, :cond_3

    .line 1677110
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->f:LX/0ad;

    sget-short v1, LX/1EB;->W:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1677111
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->b:LX/ATs;

    iget v1, v1, LX/ATs;->B:I

    .line 1677112
    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l:Lcom/facebook/resources/ui/FbFrameLayout;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbFrameLayout;->isShown()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1677113
    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l:Lcom/facebook/resources/ui/FbFrameLayout;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbFrameLayout;->setVisibility(I)V

    .line 1677114
    :cond_0
    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->m:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1677115
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->f:LX/0ad;

    sget-short v1, LX/1EB;->V:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1677116
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$6;->a:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingFrame;->getTimestamp()J

    move-result-wide v2

    .line 1677117
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    if-eqz v1, :cond_2

    .line 1677118
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->e:LX/7OL;

    invoke-virtual {v1, v2, v3}, LX/7OL;->a(J)V

    .line 1677119
    :cond_2
    return-void

    .line 1677120
    :cond_3
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1677121
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 1677122
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->getFrames()LX/0Px;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1677123
    :cond_4
    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1677124
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v7

    invoke-static {v7}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v7

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    invoke-virtual {v7, v5}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setFrames(LX/0Px;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v5

    .line 1677125
    iput-object v5, v6, LX/7kv;->g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1677126
    move-object v5, v6

    .line 1677127
    invoke-virtual {v5}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v5

    .line 1677128
    invoke-static {v0, v4, v5}, LX/ATL;->a(LX/ATL;ILcom/facebook/composer/attachments/ComposerAttachment;)V

    goto/16 :goto_0
.end method
