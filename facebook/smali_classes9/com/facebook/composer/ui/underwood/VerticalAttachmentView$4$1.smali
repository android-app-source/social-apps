.class public final Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/ATa;


# direct methods
.method public constructor <init>(LX/ATa;)V
    .locals 0

    .prologue
    .line 1676196
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;->a:LX/ATa;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1676197
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;->a:LX/ATa;

    iget-object v0, v0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-nez v0, :cond_0

    .line 1676198
    :goto_0
    return-void

    .line 1676199
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;->a:LX/ATa;

    iget-object v0, v0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->J(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1676200
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;->a:LX/ATa;

    iget-object v0, v0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setDisplayUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    .line 1676201
    :goto_1
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;->a:LX/ATa;

    iget-object v1, v1, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;->a:LX/ATa;

    iget-object v2, v2, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;->a:LX/ATa;

    iget-object v3, v3, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v3, v3, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, LX/ATL;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Z)V

    goto :goto_0

    .line 1676202
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$4$1;->a:LX/ATa;

    iget-object v0, v0, LX/ATa;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    goto :goto_1
.end method
