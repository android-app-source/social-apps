.class public final Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/ATs;


# direct methods
.method public constructor <init>(LX/ATs;)V
    .locals 0

    .prologue
    .line 1677062
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$3;->a:LX/ATs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1677063
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$3;->a:LX/ATs;

    iget-object v0, v0, LX/ATs;->h:LX/ATL;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$3;->a:LX/ATs;

    iget-object v1, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    const/4 v5, 0x0

    .line 1677064
    iget-object v2, v0, LX/ATL;->a:LX/ATO;

    iget-object v2, v2, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1677065
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 1677066
    :goto_0
    return-void

    .line 1677067
    :cond_0
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v3

    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->newBuilder()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setHasFaceboxes(Z)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setHasFaceDetectionFinished(Z)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v4

    const-wide/16 v6, -0x1

    invoke-virtual {v4, v6, v7}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setTimeToFindFirstFaceMs(J)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v4

    .line 1677068
    iput-object v4, v3, LX/7kv;->g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1677069
    move-object v3, v3

    .line 1677070
    invoke-virtual {v3}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 1677071
    invoke-static {v0, v2, v3}, LX/ATL;->a(LX/ATL;ILcom/facebook/composer/attachments/ComposerAttachment;)V

    goto :goto_0
.end method
