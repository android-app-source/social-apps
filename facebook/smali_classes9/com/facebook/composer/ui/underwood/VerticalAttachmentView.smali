.class public Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "LX/0io;",
        ":",
        "LX/0iv;",
        ":",
        "LX/0j3;",
        ":",
        "LX/0j6;",
        "DerivedData::",
        "LX/5R0;",
        "Services::",
        "LX/0il",
        "<TModelData;>;:",
        "LX/0ik",
        "<TDerivedData;>;>",
        "Lcom/facebook/widget/CustomFrameLayout;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final J:Lcom/facebook/common/callercontext/CallerContext;

.field public static final K:Landroid/graphics/RectF;

.field public static final L:Ljava/lang/String;


# instance fields
.field public A:LX/36e;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public B:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public C:LX/8G7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public D:LX/8GP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public E:LX/ATR;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public F:LX/0Uh;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public G:LX/BTf;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public H:LX/BTg;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public I:Z

.field public final M:LX/9el;

.field public final N:LX/ATX;

.field private final O:LX/9cr;

.field private final P:LX/8GH;

.field private final Q:Landroid/view/View$OnTouchListener;

.field private final R:Landroid/view/View$OnClickListener;

.field public final S:Landroid/widget/FrameLayout;

.field private final T:Landroid/widget/ImageView;

.field private final U:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public final V:Landroid/view/ViewStub;

.field public final W:Landroid/view/ViewStub;

.field public a:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public aA:Landroid/widget/LinearLayout;

.field public aB:Landroid/widget/LinearLayout;

.field private aC:Z

.field private aD:Z

.field public aE:Z

.field public aF:Lcom/facebook/resources/ui/FbButton;

.field public aG:LX/0il;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TServices;"
        }
    .end annotation
.end field

.field private aH:Lcom/facebook/widget/ScrollingAwareScrollView;

.field public aI:LX/2rw;

.field public aJ:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public aK:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;",
            ">;"
        }
    .end annotation
.end field

.field public aL:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/SwipeableParams;",
            ">;"
        }
    .end annotation
.end field

.field public aM:Z

.field public aN:LX/9c7;

.field private aO:Ljava/lang/Runnable;

.field public aP:Z

.field public aQ:Z

.field private final aa:Landroid/view/ViewStub;

.field public final ab:LX/0gc;

.field public final ac:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

.field public final ad:LX/ATL;

.field public final ae:LX/9bz;

.field public final af:LX/ASg;

.field private final ag:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final ah:I

.field public final ai:I

.field public final aj:Landroid/graphics/Point;

.field public final ak:I

.field public final al:Ljava/lang/String;

.field private final am:Z

.field private final an:Z

.field private final ao:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableViewParentEventSubscriber;",
            ">;"
        }
    .end annotation
.end field

.field public ap:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9d5;",
            ">;"
        }
    .end annotation
.end field

.field public aq:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/AT6;",
            ">;"
        }
    .end annotation
.end field

.field public ar:Lcom/facebook/composer/attachments/ComposerAttachment;

.field public as:F

.field public at:F

.field public au:Z

.field public av:LX/1lW;

.field public aw:I

.field public ax:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/9fD;",
            ">;"
        }
    .end annotation
.end field

.field public ay:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/BSt;",
            ">;"
        }
    .end annotation
.end field

.field private az:I

.field public b:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/BXn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/AT7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/7kn;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/9d6;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9c3;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/1HI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/APK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/APO;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public k:LX/9cK;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/75F;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8GV;",
            ">;"
        }
    .end annotation
.end field

.field public o:LX/0Or;
    .annotation runtime Lcom/facebook/photos/creativeediting/abtest/IsSwipeableFiltersEnabled;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0Or;
    .annotation runtime Lcom/facebook/photos/creativeediting/abtest/IsEligibleForFramePrompts;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public q:LX/01T;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/9c7;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/8GN;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public t:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public u:LX/9dZ;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public v:LX/9dW;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public w:LX/74n;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public x:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public y:LX/BTG;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public z:LX/BSu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 1676757
    const-class v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    const-string v1, "composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->J:Lcom/facebook/common/callercontext/CallerContext;

    .line 1676758
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->K:Landroid/graphics/RectF;

    .line 1676759
    const-class v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0gc;LX/ATL;LX/9bz;LX/ASg;LX/1Ck;IILjava/lang/String;ZZ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0gc;",
            "Lcom/facebook/composer/ui/underwood/AttachmentsEventListener;",
            "LX/9bz;",
            "LX/ASg;",
            "LX/1Ck",
            "<",
            "Ljava/lang/Integer;",
            ">;II",
            "Ljava/lang/String;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1676633
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1676634
    new-instance v0, LX/ATW;

    invoke-direct {v0, p0}, LX/ATW;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->M:LX/9el;

    .line 1676635
    new-instance v0, LX/ATY;

    invoke-direct {v0, p0}, LX/ATY;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->N:LX/ATX;

    .line 1676636
    new-instance v0, LX/ATZ;

    invoke-direct {v0, p0}, LX/ATZ;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->O:LX/9cr;

    .line 1676637
    new-instance v0, LX/ATa;

    invoke-direct {v0, p0}, LX/ATa;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->P:LX/8GH;

    .line 1676638
    new-instance v0, LX/ATb;

    invoke-direct {v0, p0}, LX/ATb;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->Q:Landroid/view/View$OnTouchListener;

    .line 1676639
    new-instance v0, LX/ATc;

    invoke-direct {v0, p0}, LX/ATc;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->R:Landroid/view/View$OnClickListener;

    .line 1676640
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ao:Ljava/util/List;

    .line 1676641
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    .line 1676642
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    .line 1676643
    sget-object v0, LX/1lW;->a:LX/1lW;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    .line 1676644
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ax:LX/0am;

    .line 1676645
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ay:LX/0am;

    .line 1676646
    iput v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->az:I

    .line 1676647
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    .line 1676648
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aK:LX/0am;

    .line 1676649
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1676650
    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    .line 1676651
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p0, v2}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1676652
    invoke-virtual {p0, v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setWillNotDraw(Z)V

    .line 1676653
    iput-object p3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    .line 1676654
    iput-object p4, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ae:LX/9bz;

    .line 1676655
    iput-object p2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ab:LX/0gc;

    .line 1676656
    iput-object p5, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->af:LX/ASg;

    .line 1676657
    iput-object p6, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ag:LX/1Ck;

    .line 1676658
    iput p7, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ah:I

    .line 1676659
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aj:Landroid/graphics/Point;

    .line 1676660
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->c:LX/BXn;

    invoke-virtual {v0}, LX/BXn;->a()I

    move-result v0

    int-to-float v0, v0

    invoke-static {p1, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    .line 1676661
    iput-boolean p10, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->am:Z

    .line 1676662
    iput-boolean p11, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->an:Z

    .line 1676663
    const v0, 0x7f031566

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1676664
    const v0, 0x7f0d3022

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    .line 1676665
    const v0, 0x7f0d0a7e

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->T:Landroid/widget/ImageView;

    .line 1676666
    new-instance v2, LX/0zw;

    const v0, 0x7f0d0884

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->U:LX/0zw;

    .line 1676667
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    sget-short v2, LX/1EB;->F:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aQ:Z

    .line 1676668
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aQ:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0d302b

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->W:Landroid/view/ViewStub;

    .line 1676669
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aQ:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0d3029

    :goto_1
    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->V:Landroid/view/ViewStub;

    .line 1676670
    const v0, 0x7f0d302d

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aa:Landroid/view/ViewStub;

    .line 1676671
    const v0, 0x7f0d3024

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ac:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    .line 1676672
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ac:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setVisibility(I)V

    .line 1676673
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ao:Ljava/util/List;

    new-instance v2, LX/ATd;

    invoke-direct {v2, p0}, LX/ATd;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1676674
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->T:Landroid/widget/ImageView;

    new-instance v2, LX/ATe;

    invoke-direct {v2, p0}, LX/ATe;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1676675
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->am:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->an:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    sget-short v2, LX/1EB;->E:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    .line 1676676
    :goto_2
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->T:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1676677
    iput p8, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ak:I

    .line 1676678
    iput-object p9, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    .line 1676679
    iput-boolean v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->I:Z

    .line 1676680
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 1676681
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->az:I

    .line 1676682
    return-void

    .line 1676683
    :cond_1
    const v0, 0x7f0d3027

    goto/16 :goto_0

    .line 1676684
    :cond_2
    const v0, 0x7f0d3025

    goto :goto_1

    :cond_3
    move v0, v1

    .line 1676685
    goto :goto_2

    .line 1676686
    :cond_4
    const/16 v0, 0x8

    goto :goto_3
.end method

.method public static B(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .locals 5

    .prologue
    .line 1676687
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    .line 1676688
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1676689
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    invoke-virtual {v0}, LX/9d5;->i()Ljava/lang/String;

    move-result-object v0

    .line 1676690
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1676691
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    .line 1676692
    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFilterName(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    .line 1676693
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1676694
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    invoke-virtual {v0}, LX/9d5;->l()LX/0Px;

    move-result-object v0

    .line 1676695
    if-nez v0, :cond_4

    .line 1676696
    const/4 v2, 0x0

    .line 1676697
    :goto_2
    move-object v0, v2

    .line 1676698
    invoke-virtual {v1, v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setFrameOverlayItems(LX/0Px;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    .line 1676699
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    return-object v0

    .line 1676700
    :cond_2
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1676701
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1676702
    :cond_3
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v1

    goto :goto_1

    .line 1676703
    :cond_4
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1676704
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/362;

    .line 1676705
    instance-of p0, v2, Lcom/facebook/photos/creativeediting/model/StickerParams;

    if-eqz p0, :cond_5

    .line 1676706
    check-cast v2, Lcom/facebook/photos/creativeediting/model/StickerParams;

    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 1676707
    :cond_6
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_2
.end method

.method private D()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1676708
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static E(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z
    .locals 2

    .prologue
    .line 1676709
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    sget-object v1, LX/1ld;->a:LX/1lW;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    sget-object v1, LX/1ld;->b:LX/1lW;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static F(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z
    .locals 3

    .prologue
    .line 1676710
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->q:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    sget-object v1, LX/1ld;->c:LX/1lW;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j6;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->i:LX/APK;

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j6;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    .line 1676711
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676712
    sget-object v2, LX/2rw;->PAGE:LX/2rw;

    if-eq v0, v2, :cond_0

    iget-object v2, v1, LX/APK;->a:LX/01T;

    sget-object p0, LX/01T;->PAA:LX/01T;

    if-ne v2, p0, :cond_2

    .line 1676713
    :cond_0
    const/4 v2, 0x0

    .line 1676714
    :goto_0
    move v0, v2

    .line 1676715
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public static G(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z
    .locals 2

    .prologue
    .line 1676716
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    sget-object v1, LX/1ld;->c:LX/1lW;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j6;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->j:LX/APO;

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j6;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v1, v0}, LX/APO;->a(LX/2rw;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private H()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1676717
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    sget-short v2, LX/8Fn;->a:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    sget-object v2, LX/1ld;->a:LX/1lW;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j6;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->j:LX/APO;

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j6;

    invoke-interface {v0}, LX/0j6;->getTargetData()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v0

    iget-object v0, v0, Lcom/facebook/ipc/composer/intent/ComposerTargetData;->targetType:LX/2rw;

    invoke-virtual {v2, v0}, LX/APO;->a(LX/2rw;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static J(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z
    .locals 3

    .prologue
    .line 1676718
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->F:LX/0Uh;

    sget v1, LX/7l1;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method

.method private a(LX/Hqr;Z)V
    .locals 4

    .prologue
    .line 1676719
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1676720
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v0, v1

    .line 1676721
    instance-of v0, v0, LX/74x;

    if-nez v0, :cond_1

    .line 1676722
    :cond_0
    :goto_0
    return-void

    .line 1676723
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->d:LX/AT7;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    const v0, 0x7f0d3023

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    .line 1676724
    new-instance v3, LX/ATV;

    invoke-direct {v3, p0}, LX/ATV;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    move-object v3, v3

    .line 1676725
    invoke-virtual {v1, v2, v0, v3, p1}, LX/AT7;->a(Landroid/widget/FrameLayout;Lcom/facebook/photos/tagging/shared/FaceBoxesView;LX/ATV;LX/Hqr;)LX/AT6;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    .line 1676726
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1676727
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AT6;

    invoke-virtual {v1, v0}, LX/AT6;->a(Lcom/facebook/photos/base/media/PhotoItem;)V

    .line 1676728
    iput-boolean p2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aE:Z

    goto :goto_0
.end method

.method private static a(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/03V;Ljava/util/concurrent/ExecutorService;LX/BXn;LX/AT7;LX/7kn;LX/9d6;LX/0Or;LX/1HI;LX/APK;LX/APO;LX/9cK;LX/75F;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/01T;LX/0Or;LX/8GN;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/9dZ;LX/9dW;LX/74n;LX/0ad;LX/BTG;LX/BSu;LX/36e;Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/8G7;LX/8GP;LX/ATR;LX/0Uh;LX/BTf;LX/BTg;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/BXn;",
            "LX/AT7;",
            "LX/7kn;",
            "LX/9d6;",
            "LX/0Or",
            "<",
            "LX/9c3;",
            ">;",
            "LX/1HI;",
            "LX/APK;",
            "LX/APO;",
            "LX/9cK;",
            "LX/75F;",
            "LX/0Ot",
            "<",
            "LX/8GX;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8GV;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/01T;",
            "LX/0Or",
            "<",
            "LX/9c7;",
            ">;",
            "LX/8GN;",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;",
            "LX/9dZ;",
            "LX/9dW;",
            "LX/74n;",
            "LX/0ad;",
            "LX/BTG;",
            "LX/BSu;",
            "LX/36e;",
            "Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;",
            "LX/8G7;",
            "LX/8GP;",
            "LX/ATR;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/BTf;",
            "LX/BTg;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1676740
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a:LX/03V;

    iput-object p2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->b:Ljava/util/concurrent/ExecutorService;

    iput-object p3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->c:LX/BXn;

    iput-object p4, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->d:LX/AT7;

    iput-object p5, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e:LX/7kn;

    iput-object p6, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->f:LX/9d6;

    iput-object p7, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->g:LX/0Or;

    iput-object p8, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->h:LX/1HI;

    iput-object p9, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->i:LX/APK;

    iput-object p10, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->j:LX/APO;

    iput-object p11, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->k:LX/9cK;

    iput-object p12, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->l:LX/75F;

    iput-object p13, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->n:LX/0Ot;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->o:LX/0Or;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->p:LX/0Or;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->q:LX/01T;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->r:LX/0Or;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->s:LX/8GN;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->u:LX/9dZ;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->v:LX/9dW;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->w:LX/74n;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->y:LX/BTG;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->z:LX/BSu;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->A:LX/36e;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B:Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->C:LX/8G7;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->D:LX/8GP;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->E:LX/ATR;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->F:LX/0Uh;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->G:LX/BTf;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->H:LX/BTg;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 37

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v36

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-static/range {v36 .. v36}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static/range {v36 .. v36}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {v36 .. v36}, LX/BXn;->a(LX/0QB;)LX/BXn;

    move-result-object v5

    check-cast v5, LX/BXn;

    const-class v6, LX/AT7;

    move-object/from16 v0, v36

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/AT7;

    invoke-static/range {v36 .. v36}, LX/7kn;->a(LX/0QB;)LX/7kn;

    move-result-object v7

    check-cast v7, LX/7kn;

    const-class v8, LX/9d6;

    move-object/from16 v0, v36

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/9d6;

    const/16 v9, 0x2e22

    move-object/from16 v0, v36

    invoke-static {v0, v9}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {v36 .. v36}, LX/1Fo;->a(LX/0QB;)LX/1HI;

    move-result-object v10

    check-cast v10, LX/1HI;

    invoke-static/range {v36 .. v36}, LX/APK;->a(LX/0QB;)LX/APK;

    move-result-object v11

    check-cast v11, LX/APK;

    invoke-static/range {v36 .. v36}, LX/APO;->a(LX/0QB;)LX/APO;

    move-result-object v12

    check-cast v12, LX/APO;

    invoke-static/range {v36 .. v36}, LX/9cK;->a(LX/0QB;)LX/9cK;

    move-result-object v13

    check-cast v13, LX/9cK;

    invoke-static/range {v36 .. v36}, LX/75F;->a(LX/0QB;)LX/75F;

    move-result-object v14

    check-cast v14, LX/75F;

    const/16 v15, 0x2e3d

    move-object/from16 v0, v36

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x2e3c

    move-object/from16 v0, v36

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x154b

    move-object/from16 v0, v36

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    const/16 v18, 0x154a

    move-object/from16 v0, v36

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {v36 .. v36}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v19

    check-cast v19, LX/01T;

    const/16 v20, 0x2e23

    move-object/from16 v0, v36

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v20

    invoke-static/range {v36 .. v36}, LX/8GN;->a(LX/0QB;)LX/8GN;

    move-result-object v21

    check-cast v21, LX/8GN;

    invoke-static/range {v36 .. v36}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    move-result-object v22

    check-cast v22, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    const-class v23, LX/9dZ;

    move-object/from16 v0, v36

    move-object/from16 v1, v23

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v23

    check-cast v23, LX/9dZ;

    const-class v24, LX/9dW;

    move-object/from16 v0, v36

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/9dW;

    invoke-static/range {v36 .. v36}, LX/74n;->a(LX/0QB;)LX/74n;

    move-result-object v25

    check-cast v25, LX/74n;

    invoke-static/range {v36 .. v36}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v26

    check-cast v26, LX/0ad;

    invoke-static/range {v36 .. v36}, LX/BTG;->a(LX/0QB;)LX/BTG;

    move-result-object v27

    check-cast v27, LX/BTG;

    invoke-static/range {v36 .. v36}, LX/BSu;->a(LX/0QB;)LX/BSu;

    move-result-object v28

    check-cast v28, LX/BSu;

    invoke-static/range {v36 .. v36}, LX/36e;->a(LX/0QB;)LX/36e;

    move-result-object v29

    check-cast v29, LX/36e;

    invoke-static/range {v36 .. v36}, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;->a(LX/0QB;)Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    move-result-object v30

    check-cast v30, Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;

    invoke-static/range {v36 .. v36}, LX/8G7;->a(LX/0QB;)LX/8G7;

    move-result-object v31

    check-cast v31, LX/8G7;

    const-class v32, LX/8GP;

    move-object/from16 v0, v36

    move-object/from16 v1, v32

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v32

    check-cast v32, LX/8GP;

    invoke-static/range {v36 .. v36}, LX/ATR;->a(LX/0QB;)LX/ATR;

    move-result-object v33

    check-cast v33, LX/ATR;

    invoke-static/range {v36 .. v36}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v34

    check-cast v34, LX/0Uh;

    invoke-static/range {v36 .. v36}, LX/BTf;->a(LX/0QB;)LX/BTf;

    move-result-object v35

    check-cast v35, LX/BTf;

    invoke-static/range {v36 .. v36}, LX/BTg;->a(LX/0QB;)LX/BTg;

    move-result-object v36

    check-cast v36, LX/BTg;

    invoke-static/range {v2 .. v36}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/03V;Ljava/util/concurrent/ExecutorService;LX/BXn;LX/AT7;LX/7kn;LX/9d6;LX/0Or;LX/1HI;LX/APK;LX/APO;LX/9cK;LX/75F;LX/0Ot;LX/0Ot;LX/0Or;LX/0Or;LX/01T;LX/0Or;LX/8GN;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;LX/9dZ;LX/9dW;LX/74n;LX/0ad;LX/BTG;LX/BSu;LX/36e;Lcom/facebook/photos/creativeediting/swipeable/common/FrameAssetsLoader;LX/8G7;LX/8GP;LX/ATR;LX/0Uh;LX/BTf;LX/BTg;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/BTC;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1676781
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->y(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1676782
    new-instance v0, LX/ATS;

    invoke-direct {v0, p0}, LX/ATS;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    .line 1676783
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/ATT;

    invoke-direct {v0, p0, p1}, LX/ATT;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/BTC;)V

    goto :goto_0
.end method

.method private static getImageUri(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 1676768
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 1676769
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/VideoItem;

    if-eqz v0, :cond_0

    .line 1676770
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1676771
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1676772
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1676773
    new-instance v0, Ljava/io/File;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1676774
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1676775
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 1676776
    :cond_1
    return-object v1

    .line 1676777
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1676778
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/VideoItem;

    .line 1676779
    invoke-virtual {v0}, Lcom/facebook/photos/base/media/VideoItem;->s()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1676780
    invoke-virtual {v0}, Lcom/facebook/photos/base/media/VideoItem;->s()Landroid/net/Uri;

    move-result-object v1

    goto :goto_1
.end method

.method private m()V
    .locals 3

    .prologue
    .line 1676760
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aC:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1676761
    :cond_0
    :goto_0
    return-void

    .line 1676762
    :cond_1
    sget-object v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->K:Landroid/graphics/RectF;

    .line 1676763
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1676764
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    invoke-static {v0}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v0

    move-object v1, v0

    .line 1676765
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AT6;

    invoke-virtual {v0, v1}, LX/AT6;->b(Landroid/graphics/RectF;)Ljava/util/List;

    move-result-object v1

    .line 1676766
    if-eqz v1, :cond_0

    .line 1676767
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    const/4 v2, 0x0

    new-array v2, v2, [Landroid/graphics/RectF;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, LX/9d5;->a([Landroid/graphics/RectF;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    goto :goto_1
.end method

.method private n()V
    .locals 4

    .prologue
    .line 1676751
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getImageUri(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    new-instance v1, LX/1o9;

    iget v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    iget v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aw:I

    invoke-direct {v1, v2, v3}, LX/1o9;-><init>(II)V

    .line 1676752
    iput-object v1, v0, LX/1bX;->c:LX/1o9;

    .line 1676753
    move-object v0, v0

    .line 1676754
    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1676755
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->h:LX/1HI;

    sget-object v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->J:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, LX/1HI;->d(LX/1bf;Ljava/lang/Object;)LX/1ca;

    .line 1676756
    return-void
.end method

.method private o()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1676744
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 1676745
    :cond_1
    :goto_0
    return v0

    .line 1676746
    :cond_2
    const/4 v2, 0x2

    new-array v2, v2, [I

    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getLeft()I

    move-result v3

    aput v3, v2, v1

    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getTop()I

    move-result v3

    aput v3, v2, v0

    .line 1676747
    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v3, v2}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    .line 1676748
    aget v3, v2, v0

    iget-object v4, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 1676749
    if-ltz v3, :cond_3

    aget v2, v2, v0

    iget v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->az:I

    if-le v2, v3, :cond_1

    :cond_3
    move v0, v1

    .line 1676750
    goto :goto_0
.end method

.method private p()V
    .locals 8

    .prologue
    .line 1676741
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e()V

    .line 1676742
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e:LX/7kn;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->c()J

    move-result-wide v2

    iget v4, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ak:I

    iget v5, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    iget v6, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aw:I

    iget-boolean v7, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->au:Z

    invoke-virtual/range {v0 .. v7}, LX/7kn;->a(Ljava/lang/String;JIIIZ)V

    .line 1676743
    return-void
.end method

.method private r()V
    .locals 3

    .prologue
    .line 1676729
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->R:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, LX/9d5;->a(Landroid/view/View$OnClickListener;)V

    .line 1676730
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->Q:Landroid/view/View$OnTouchListener;

    .line 1676731
    iget-object v2, v0, LX/9d5;->L:LX/9dI;

    .line 1676732
    iput-object v1, v2, LX/9dI;->h:Landroid/view/View$OnTouchListener;

    .line 1676733
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aH:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1676734
    iput-object v1, v0, LX/9d5;->K:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1676735
    iget-object v2, v0, LX/9d5;->K:Lcom/facebook/widget/ScrollingAwareScrollView;

    const/4 p0, 0x1

    .line 1676736
    iput-boolean p0, v2, Lcom/facebook/widget/ScrollingAwareScrollView;->a:Z

    .line 1676737
    iget-object v2, v0, LX/9d5;->L:LX/9dI;

    iget-object p0, v0, LX/9d5;->K:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1676738
    iput-object p0, v2, LX/9dI;->v:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1676739
    return-void
.end method

.method public static s(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1676624
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->E(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->F(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->G(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1676625
    :cond_1
    :goto_0
    return v0

    .line 1676626
    :cond_2
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v1, :cond_1

    .line 1676627
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static t(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1676628
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->s(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1676629
    :goto_0
    return v0

    .line 1676630
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ax:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0j3;

    invoke-interface {v0}, LX/0j3;->getConfiguration()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->isEdit()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    .line 1676631
    goto :goto_0

    .line 1676632
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private u()V
    .locals 3

    .prologue
    .line 1676318
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aO:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    .line 1676319
    new-instance v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;

    invoke-direct {v0, p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aO:Ljava/lang/Runnable;

    .line 1676320
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->E:LX/ATR;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aO:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2}, LX/ATR;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Ljava/lang/Runnable;)V

    .line 1676321
    return-void
.end method

.method public static w(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 6

    .prologue
    .line 1676322
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aA:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    .line 1676323
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a:LX/03V;

    sget-object v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    const-string v2, "Editing Layout not initialized"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1676324
    :cond_0
    :goto_0
    return-void

    .line 1676325
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aA:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0d18

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1676326
    if-nez v1, :cond_2

    .line 1676327
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a:LX/03V;

    sget-object v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    const-string v2, "UG Launcher holder view not available"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1676328
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aQ:Z

    if-eqz v0, :cond_3

    .line 1676329
    const v0, 0x7f0d0d1a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    sget-char v3, LX/1EB;->G:C

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08134b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1676330
    :cond_3
    const v0, 0x7f0d0d19

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aF:Lcom/facebook/resources/ui/FbButton;

    .line 1676331
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->F(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1676332
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aF:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f020424

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    .line 1676333
    :goto_1
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1676334
    new-instance v0, LX/ATU;

    invoke-direct {v0, p0}, LX/ATU;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1676335
    iget v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ak:I

    if-nez v0, :cond_0

    .line 1676336
    new-instance v0, LX/ATu;

    .line 1676337
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 1676338
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->E(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1676339
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020423

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1676340
    :cond_4
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->G(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1676341
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020425

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1676342
    :cond_5
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->F(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 1676343
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020424

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1676344
    :goto_2
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 1676345
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aF:Lcom/facebook/resources/ui/FbButton;

    invoke-direct {v0, v1, v2}, LX/ATu;-><init>(LX/0Px;Landroid/widget/Button;)V

    invoke-virtual {v0}, LX/ATu;->a()V

    goto/16 :goto_0

    .line 1676346
    :cond_6
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aF:Lcom/facebook/resources/ui/FbButton;

    const v2, 0x7f020422

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbButton;->setBackgroundResource(I)V

    goto :goto_1

    .line 1676347
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020422

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2
.end method

.method public static y(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z
    .locals 1

    .prologue
    .line 1676348
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->shouldPostProcess()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static z(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)LX/0am;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0am",
            "<",
            "Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1676349
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->E(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v2

    .line 1676350
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->F(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v3

    .line 1676351
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->G(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v4

    .line 1676352
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->H()Z

    move-result v0

    .line 1676353
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->D()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1676354
    sget-object v0, LX/5Rr;->FILTER:LX/5Rr;

    .line 1676355
    :goto_0
    new-instance v5, LX/5Rw;

    invoke-direct {v5}, LX/5Rw;-><init>()V

    invoke-virtual {v5, v0}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v5

    if-nez v2, :cond_6

    sget-object v0, LX/5Rr;->CROP:LX/5Rr;

    :goto_1
    invoke-virtual {v5, v0}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v2

    if-nez v3, :cond_7

    sget-object v0, LX/5Rr;->STICKER:LX/5Rr;

    :goto_2
    invoke-virtual {v2, v0}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v2

    if-nez v4, :cond_8

    sget-object v0, LX/5Rr;->TEXT:LX/5Rr;

    :goto_3
    invoke-virtual {v2, v0}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->H()Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, LX/5Rr;->DOODLE:LX/5Rr;

    :goto_4
    invoke-virtual {v2, v0}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->D()Z

    move-result v2

    if-nez v2, :cond_0

    sget-object v1, LX/5Rr;->FILTER:LX/5Rr;

    :cond_0
    invoke-virtual {v0, v1}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/5Rw;->b(Ljava/lang/String;)LX/5Rw;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    .line 1676356
    iput-object v1, v0, LX/5Rw;->l:LX/0Px;

    .line 1676357
    move-object v0, v0

    .line 1676358
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->J(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v1

    .line 1676359
    iput-boolean v1, v0, LX/5Rw;->f:Z

    .line 1676360
    move-object v0, v0

    .line 1676361
    const/4 v1, 0x0

    .line 1676362
    iput-boolean v1, v0, LX/5Rw;->g:Z

    .line 1676363
    move-object v0, v0

    .line 1676364
    invoke-virtual {v0}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    :goto_5
    return-object v0

    .line 1676365
    :cond_1
    if-eqz v3, :cond_2

    .line 1676366
    sget-object v0, LX/5Rr;->STICKER:LX/5Rr;

    goto :goto_0

    .line 1676367
    :cond_2
    if-eqz v2, :cond_3

    .line 1676368
    sget-object v0, LX/5Rr;->CROP:LX/5Rr;

    goto :goto_0

    .line 1676369
    :cond_3
    if-eqz v4, :cond_4

    .line 1676370
    sget-object v0, LX/5Rr;->TEXT:LX/5Rr;

    goto :goto_0

    .line 1676371
    :cond_4
    if-eqz v0, :cond_5

    .line 1676372
    sget-object v0, LX/5Rr;->DOODLE:LX/5Rr;

    goto :goto_0

    .line 1676373
    :cond_5
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_5

    :cond_6
    move-object v0, v1

    .line 1676374
    goto :goto_1

    :cond_7
    move-object v0, v1

    goto :goto_2

    :cond_8
    move-object v0, v1

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_4
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1676375
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    invoke-interface {v0}, LX/0il;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0io;

    check-cast v0, LX/0iv;

    invoke-interface {v0}, LX/0iv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aD:Z

    if-eqz v0, :cond_1

    .line 1676376
    :cond_0
    :goto_0
    return-void

    .line 1676377
    :cond_1
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->s(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1676378
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e()V

    .line 1676379
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aD:Z

    goto :goto_0

    .line 1676380
    :cond_3
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->z(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    .line 1676381
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1676382
    new-instance v0, LX/9fD;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ab:LX/0gc;

    invoke-direct {v0, v1}, LX/9fD;-><init>(LX/0gc;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ax:LX/0am;

    .line 1676383
    :cond_4
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aA:Landroid/widget/LinearLayout;

    if-nez v0, :cond_2

    .line 1676384
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->W:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aA:Landroid/widget/LinearLayout;

    .line 1676385
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1676386
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aA:Landroid/widget/LinearLayout;

    const v1, 0x7f0d24f1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1676387
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1676388
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->w(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/composer/attachments/ComposerAttachment;LX/Hqr;LX/0il;ZZLcom/facebook/widget/ScrollingAwareScrollView;)V
    .locals 11
    .param p1    # Lcom/facebook/composer/attachments/ComposerAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/Hqr;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/0il;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            "Lcom/facebook/composer/ui/underwood/TaggingController$TagsChangedListener;",
            "TServices;ZZ",
            "Lcom/facebook/widget/ScrollingAwareScrollView;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1676389
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1676390
    move/from16 v0, p5

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->I:Z

    .line 1676391
    iput-object p3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    .line 1676392
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aH:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1676393
    if-nez p1, :cond_3

    .line 1676394
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ag:LX/1Ck;

    iget v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ah:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1676395
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1676396
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9d5;

    invoke-virtual {v1}, LX/9d5;->h()V

    .line 1676397
    :cond_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->as:F

    .line 1676398
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1676399
    sget-object v1, LX/1lW;->a:LX/1lW;

    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    .line 1676400
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aH:Lcom/facebook/widget/ScrollingAwareScrollView;

    .line 1676401
    iget-boolean v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aC:Z

    if-eqz v1, :cond_1

    .line 1676402
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9d5;

    invoke-virtual {v1}, LX/9d5;->m()V

    .line 1676403
    :cond_1
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->k:LX/9cK;

    invoke-virtual {v1}, LX/9cK;->c()V

    .line 1676404
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aP:Z

    .line 1676405
    :cond_2
    :goto_0
    return-void

    .line 1676406
    :cond_3
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    if-nez v1, :cond_4

    .line 1676407
    sget-object v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    const-string v2, "Media item is null"

    invoke-static {v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1676408
    :cond_4
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->r:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9c7;

    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aN:LX/9c7;

    .line 1676409
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aN:LX/9c7;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->b()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/data/MediaData;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/9c7;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1676410
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->m()LX/4gF;

    move-result-object v1

    sget-object v2, LX/4gF;->VIDEO:LX/4gF;

    if-ne v1, v2, :cond_7

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->au:Z

    .line 1676411
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    instance-of v2, v1, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1676412
    iget-boolean v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->au:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 1676413
    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, LX/4gF;->PHOTO:LX/4gF;

    invoke-static {v3, v4}, LX/ASg;->a(Landroid/net/Uri;LX/4gF;)F

    move-result v1

    iput v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->as:F

    .line 1676414
    :goto_2
    iget v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    int-to-float v1, v1

    iget v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->as:F

    div-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aw:I

    .line 1676415
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a()Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1676416
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p0, v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setScale(F)V

    .line 1676417
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    sget-object v3, LX/1lW;->a:LX/1lW;

    if-ne v1, v3, :cond_5

    .line 1676418
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1la;->a(Ljava/lang/String;)LX/1lW;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    .line 1676419
    :cond_5
    invoke-direct {p0, p2, p4}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a(LX/Hqr;Z)V

    .line 1676420
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->p()V

    .line 1676421
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->r()V

    .line 1676422
    iget-boolean v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->au:Z

    if-eqz v1, :cond_a

    sget-object v1, Lcom/facebook/ipc/media/MediaItem;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/ipc/media/MediaItem;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const/4 v1, 0x1

    .line 1676423
    :goto_3
    if-eqz v1, :cond_b

    .line 1676424
    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->U:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1676425
    :goto_4
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->n()V

    .line 1676426
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e()V

    .line 1676427
    if-eqz v1, :cond_c

    .line 1676428
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->u()V

    .line 1676429
    :goto_5
    if-eqz v2, :cond_6

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aG:LX/0il;

    check-cast v1, LX/0ik;

    invoke-interface {v1}, LX/0ik;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/5R0;

    invoke-interface {v1}, LX/5R0;->D()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1676430
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aa:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1676431
    :cond_6
    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1676432
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9d5;

    invoke-virtual {v1}, LX/9d5;->f()Landroid/graphics/RectF;

    move-result-object v4

    .line 1676433
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->k:LX/9cK;

    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v2

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ac:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    const/4 v7, 0x1

    const/4 v8, 0x3

    new-array v8, v8, [LX/9cJ;

    const/4 v9, 0x0

    sget-object v10, LX/9cJ;->STICKERS:LX/9cJ;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    sget-object v10, LX/9cJ;->TEXTS:LX/9cJ;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    sget-object v10, LX/9cJ;->DOODLE:LX/9cJ;

    aput-object v10, v8, v9

    invoke-virtual/range {v1 .. v8}, LX/9cK;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;IIILandroid/view/View;Z[LX/9cJ;)V

    .line 1676434
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ac:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->k:LX/9cK;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->setOverlayViewEventListener(LX/9cG;)V

    goto/16 :goto_0

    .line 1676435
    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_1

    .line 1676436
    :cond_8
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 1676437
    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v3}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, LX/4gF;->PHOTO:LX/4gF;

    invoke-static {v3, v4}, LX/ASg;->a(Landroid/net/Uri;LX/4gF;)F

    move-result v1

    iput v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->as:F

    goto/16 :goto_2

    .line 1676438
    :cond_9
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->af:LX/ASg;

    invoke-virtual {p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/ASg;->a(Lcom/facebook/ipc/media/MediaItem;)F

    move-result v1

    iput v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->as:F

    goto/16 :goto_2

    .line 1676439
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_3

    .line 1676440
    :cond_b
    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->U:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->c()V

    goto/16 :goto_4

    .line 1676441
    :cond_c
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a()V

    goto/16 :goto_5
.end method

.method public final b()V
    .locals 8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 1676601
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1676602
    :cond_0
    :goto_0
    return-void

    .line 1676603
    :cond_1
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v2

    .line 1676604
    invoke-static {v2}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/5iL;->AE08bit:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-static {v2}, LX/5iB;->f(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {v2}, LX/5iB;->b(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1676605
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8GV;

    const/high16 v1, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :goto_1
    invoke-virtual/range {v0 .. v5}, LX/8GV;->a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1676606
    new-instance v1, LX/4BY;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/4BY;-><init>(Landroid/content/Context;)V

    .line 1676607
    invoke-virtual {v1}, LX/4BY;->show()V

    .line 1676608
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, LX/4BY;->setContentView(Landroid/view/View;)V

    .line 1676609
    const v2, 0x63109b68

    :try_start_0
    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1676610
    const/4 v3, 0x0

    .line 1676611
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    .line 1676612
    invoke-static {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v4

    .line 1676613
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->setDisplayUri(Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    .line 1676614
    invoke-virtual {v4}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v4

    .line 1676615
    iget-object v5, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ad:LX/ATL;

    iget-object v6, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    iget-object v7, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v7}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v7

    invoke-virtual {v5, v6, v7, v4, v3}, LX/ATL;->a(Lcom/facebook/composer/attachments/ComposerAttachment;Lcom/facebook/ipc/media/MediaItem;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1676616
    invoke-virtual {v1}, LX/4BY;->dismiss()V

    goto/16 :goto_0

    .line 1676617
    :cond_4
    iget-object v4, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v4

    goto :goto_1

    .line 1676618
    :catch_0
    move-exception v0

    .line 1676619
    :try_start_1
    sget-object v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    const-string v3, "interrupted"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1676620
    invoke-virtual {v1}, LX/4BY;->dismiss()V

    goto/16 :goto_0

    .line 1676621
    :catch_1
    move-exception v0

    .line 1676622
    :try_start_2
    sget-object v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    const-string v3, "interrupted"

    invoke-static {v2, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1676623
    invoke-virtual {v1}, LX/4BY;->dismiss()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/4BY;->dismiss()V

    throw v0
.end method

.method public final dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1676442
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 1676443
    return-void
.end method

.method public final dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1676444
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 1676445
    return-void
.end method

.method public final e()V
    .locals 14

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    .line 1676446
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aC:Z

    if-eqz v0, :cond_0

    .line 1676447
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->m()V

    .line 1676448
    :goto_0
    return-void

    .line 1676449
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFilterName()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    .line 1676450
    :goto_1
    const v0, 0x7f0d1a94

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/ViewStub;

    .line 1676451
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1676452
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->f:LX/9d6;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ae:LX/9bz;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getImageUri(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v4}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {v0 .. v5}, LX/9d6;->a(LX/9bz;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;Landroid/net/Uri;Ljava/lang/String;Z)LX/9d5;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    .line 1676453
    :cond_1
    const v0, 0x7f0d2dfc

    invoke-virtual {p0, v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    .line 1676454
    if-nez v0, :cond_b

    .line 1676455
    invoke-virtual {v6}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    move-object v1, v0

    .line 1676456
    :goto_2
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    iget v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aw:I

    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->o()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/9d5;->a(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;IIZ)V

    .line 1676457
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->P:LX/8GH;

    invoke-virtual {v0, v1}, LX/9d5;->a(LX/8GH;)V

    .line 1676458
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->O:LX/9cr;

    invoke-virtual {v0, v1}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a(LX/9cr;)V

    .line 1676459
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->m()V

    .line 1676460
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    move-object v0, v0

    .line 1676461
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_8

    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 1676462
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    move-object v1, v1

    .line 1676463
    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getFramePacks()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 1676464
    :goto_3
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->P:LX/8GH;

    invoke-virtual {v0, v2}, LX/9d5;->a(LX/8GH;)V

    .line 1676465
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->O:LX/9cr;

    invoke-virtual {v0, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->a(LX/9cr;)V

    .line 1676466
    const/4 v2, 0x0

    .line 1676467
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/74n;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_c

    :cond_2
    move v0, v2

    .line 1676468
    :goto_4
    move v0, v0

    .line 1676469
    if-eqz v0, :cond_a

    .line 1676470
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->D:LX/8GP;

    iget v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    iget v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aw:I

    invoke-virtual {v0, v2, v3}, LX/8GP;->a(II)LX/8GO;

    move-result-object v0

    const/4 v6, 0x0

    .line 1676471
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676472
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v10

    move v9, v6

    :goto_5
    if-ge v9, v10, :cond_4

    invoke-virtual {v1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    .line 1676473
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->c()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v4, v6

    :goto_6
    if-ge v4, v12, :cond_3

    invoke-virtual {v11, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    .line 1676474
    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;->e()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v0, v3, v13}, LX/8GO;->a(Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;Ljava/lang/String;)LX/8GO;

    .line 1676475
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    .line 1676476
    :cond_3
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_5

    .line 1676477
    :cond_4
    move-object v0, v0

    .line 1676478
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->s:LX/8GN;

    invoke-virtual {v2}, LX/8GN;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8GO;->a(LX/0Px;)LX/8GO;

    move-result-object v0

    invoke-virtual {v0}, LX/8GO;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    .line 1676479
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    invoke-virtual {v0, v2, v7}, LX/9d5;->a(LX/0Px;Ljava/lang/String;)V

    .line 1676480
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aM:Z

    .line 1676481
    iget v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ak:I

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-le v0, v5, :cond_9

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->v:LX/9dW;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aL:LX/0Px;

    .line 1676482
    new-instance v4, LX/9dV;

    const/16 v3, 0x2e35

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v3

    check-cast v3, LX/3kp;

    invoke-direct {v4, v2, v6, v3}, LX/9dV;-><init>(LX/0Px;LX/0Ot;LX/3kp;)V

    .line 1676483
    move-object v0, v4

    .line 1676484
    move-object v2, v0

    .line 1676485
    :goto_7
    if-eqz v2, :cond_5

    .line 1676486
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/9d5;->a(LX/0Px;)V

    .line 1676487
    :cond_5
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->A:LX/36e;

    invoke-virtual {v0}, LX/36e;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/ATg;

    invoke-direct {v3, p0, v2, v1, v7}, LX/ATg;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/9dV;LX/0Px;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1676488
    :goto_8
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1676489
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->z(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    .line 1676490
    :cond_6
    iput-boolean v5, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aC:Z

    .line 1676491
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->m()V

    goto/16 :goto_0

    .line 1676492
    :cond_7
    sget-object v0, LX/5iL;->PassThrough:LX/5iL;

    invoke-virtual {v0}, LX/5iL;->name()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    goto/16 :goto_1

    .line 1676493
    :cond_8
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1676494
    move-object v1, v0

    goto/16 :goto_3

    :cond_9
    move-object v2, v8

    .line 1676495
    goto :goto_7

    .line 1676496
    :cond_a
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->s:LX/8GN;

    invoke-virtual {v0}, LX/8GN;->b()Lcom/facebook/photos/creativeediting/model/SwipeableParams;

    move-result-object v1

    .line 1676497
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1, v8}, LX/9d5;->a(LX/0Px;Ljava/lang/String;)V

    goto :goto_8

    :cond_b
    move-object v1, v0

    goto/16 :goto_2

    :cond_c
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_e

    :cond_d
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/photos/base/media/PhotoItem;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->av:LX/1lW;

    sget-object v3, LX/1ld;->c:LX/1lW;

    if-eq v0, v3, :cond_e

    const/4 v0, 0x1

    goto/16 :goto_4

    :cond_e
    move v0, v2

    goto/16 :goto_4
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1676498
    invoke-direct {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->o()Z

    move-result v1

    .line 1676499
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ao:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ATd;

    .line 1676500
    iget-object p0, v0, LX/ATd;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object p0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {p0}, LX/0am;->isPresent()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 1676501
    iget-object p0, v0, LX/ATd;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object p0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {p0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/9d5;

    invoke-virtual {p0, v1}, LX/9d5;->b(Z)V

    .line 1676502
    :cond_0
    goto :goto_0

    .line 1676503
    :cond_1
    return-void
.end method

.method public getCreativeEditingData()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;
    .locals 1

    .prologue
    .line 1676504
    invoke-static {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->B(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    return-object v0
.end method

.method public final h()V
    .locals 6

    .prologue
    .line 1676505
    iget-boolean v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aQ:Z

    if-eqz v0, :cond_2

    .line 1676506
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aA:Landroid/widget/LinearLayout;

    :goto_0
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    const/4 p0, 0x1

    .line 1676507
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    if-nez v2, :cond_4

    .line 1676508
    :cond_0
    :goto_1
    return-void

    .line 1676509
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    goto :goto_0

    .line 1676510
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aA:Landroid/widget/LinearLayout;

    :goto_2
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->T:Landroid/widget/ImageView;

    invoke-static {v0, v1, v2}, LX/ATC;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    goto :goto_2

    .line 1676511
    :cond_4
    const/4 v2, 0x2

    new-array v3, v2, [I

    .line 1676512
    invoke-virtual {v1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 1676513
    aget v2, v3, p0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v2, v4

    .line 1676514
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 1676515
    invoke-virtual {v1, v4}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1676516
    iget v5, v4, Landroid/graphics/Rect;->top:I

    if-lt v2, v5, :cond_0

    aget v2, v3, p0

    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    if-gt v2, v5, :cond_0

    .line 1676517
    aget v2, v3, p0

    iget v5, v4, Landroid/graphics/Rect;->top:I

    if-ge v2, v5, :cond_6

    .line 1676518
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    add-int/2addr v2, v5

    .line 1676519
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    if-le v2, v5, :cond_5

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v2, v5

    .line 1676520
    :goto_3
    iget v4, v4, Landroid/graphics/Rect;->top:I

    aget v3, v3, p0

    sub-int v3, v4, v3

    sub-int v2, v3, v2

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_1

    .line 1676521
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 1676522
    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_1
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x1c1d5acc

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1676523
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ag:LX/1Ck;

    iget v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ah:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1676524
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    if-eqz v1, :cond_0

    .line 1676525
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->t:Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->O:LX/9cr;

    invoke-virtual {v1, v2}, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;->b(LX/9cr;)V

    .line 1676526
    :cond_0
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onDetachedFromWindow()V

    .line 1676527
    const/16 v1, 0x2d

    const v2, -0x18212dce

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 1676528
    invoke-super/range {p0 .. p5}, Lcom/facebook/widget/CustomFrameLayout;->onLayout(ZIIII)V

    .line 1676529
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->f()V

    .line 1676530
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 1676531
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1676532
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v2

    .line 1676533
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    sub-int v1, v2, v1

    .line 1676534
    int-to-float v2, v1

    iget v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->as:F

    div-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 1676535
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 1676536
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 1676537
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;->onMeasure(II)V

    .line 1676538
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1676539
    instance-of v0, p1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;

    if-nez v0, :cond_1

    .line 1676540
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1676541
    :cond_0
    :goto_0
    return-void

    .line 1676542
    :cond_1
    check-cast p1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;

    .line 1676543
    invoke-virtual {p1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1676544
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e()V

    .line 1676545
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->a:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    .line 1676546
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1676547
    new-instance v0, LX/9fD;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ab:LX/0gc;

    invoke-direct {v0, v1}, LX/9fD;-><init>(LX/0gc;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ax:LX/0am;

    .line 1676548
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ax:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9fD;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->M:LX/9el;

    invoke-virtual {v0, v1}, LX/9fD;->a(LX/9el;)V

    .line 1676549
    :cond_2
    iget-object v0, p1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aK:LX/0am;

    .line 1676550
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aK:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1676551
    new-instance v0, LX/BSt;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ab:LX/0gc;

    invoke-direct {v0, v1}, LX/BSt;-><init>(LX/0gc;)V

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ay:LX/0am;

    .line 1676552
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ay:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BSt;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->N:LX/ATX;

    invoke-virtual {v0, v1}, LX/BSt;->a(LX/ATX;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 1676553
    new-instance v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;

    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aJ:LX/0am;

    invoke-virtual {v0}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aK:LX/0am;

    invoke-virtual {v1}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-direct {v2, v3, v0, v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;-><init>(Landroid/os/Parcelable;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)V

    return-object v2
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7913d89

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1676554
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomFrameLayout;->onSizeChanged(IIII)V

    .line 1676555
    iget-boolean v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aP:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1676556
    const/16 v1, 0x2d

    const v2, -0x45e443a1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 1676557
    :goto_0
    return-void

    .line 1676558
    :cond_0
    int-to-float v1, p1

    invoke-virtual {p0, v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setPivotX(F)V

    .line 1676559
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setPivotY(F)V

    .line 1676560
    if-lez p1, :cond_1

    if-lez p2, :cond_1

    .line 1676561
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->S:Landroid/widget/FrameLayout;

    new-instance v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;

    invoke-direct {v2, p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;-><init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V

    invoke-static {v1, v2}, LX/8He;->c(Landroid/view/View;Ljava/lang/Runnable;)Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1676562
    :cond_1
    const v1, -0x6db24294

    invoke-static {v1, v0}, LX/02F;->g(II)V

    goto :goto_0
.end method

.method public setFaceBoxesAndTags(Z)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1676563
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    if-nez v0, :cond_1

    .line 1676564
    :cond_0
    :goto_0
    return-void

    .line 1676565
    :cond_1
    iput-boolean p1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aE:Z

    .line 1676566
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/base/media/PhotoItem;

    .line 1676567
    iget-object v1, v0, Lcom/facebook/photos/base/media/PhotoItem;->i:Lcom/facebook/photos/base/photos/LocalPhoto;

    move-object v1, v1

    .line 1676568
    check-cast v1, LX/74x;

    .line 1676569
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->l:LX/75F;

    invoke-virtual {v2, v1}, LX/75F;->c(LX/74x;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1676570
    sget-object v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->K:Landroid/graphics/RectF;

    .line 1676571
    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 1676572
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getCropBox()Lcom/facebook/photos/creativeediting/model/PersistableRect;

    move-result-object v1

    invoke-static {v1}, LX/63w;->c(Lcom/facebook/photos/creativeediting/model/PersistableRect;)Landroid/graphics/RectF;

    move-result-object v1

    .line 1676573
    :cond_2
    sget-object v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->K:Landroid/graphics/RectF;

    if-ne v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v0

    if-eqz v0, :cond_4

    .line 1676574
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AT6;

    .line 1676575
    iget-object v2, v0, LX/AT6;->g:LX/9ic;

    iget-object v4, v0, LX/AT6;->f:LX/ASb;

    iget-object p1, v0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v4, p1}, LX/ASb;->a(Lcom/facebook/ipc/media/MediaItem;)Ljava/util/List;

    move-result-object v4

    iget-object p1, v0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {p1}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result p1

    invoke-virtual {v2, v1, v4, p1}, LX/9ic;->a(Landroid/graphics/RectF;Ljava/util/List;I)V

    .line 1676576
    iget-object v2, v0, LX/AT6;->h:LX/8GZ;

    iget-object v4, v0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v4

    invoke-virtual {v2, v1, v4}, LX/8GZ;->a(Landroid/graphics/RectF;I)V

    .line 1676577
    :cond_4
    new-instance v1, Landroid/graphics/RectF;

    iget v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    int-to-float v0, v0

    iget v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aw:I

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v0, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1676578
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1676579
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a:LX/03V;

    sget-object v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    const-string v3, "SwipableImageController not bound"

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1676580
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e()V

    .line 1676581
    :cond_5
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    invoke-virtual {v0}, LX/9d5;->f()Landroid/graphics/RectF;

    move-result-object v2

    .line 1676582
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AT6;

    iget-boolean v3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aE:Z

    .line 1676583
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676584
    invoke-static {v2, v1}, LX/AT6;->b(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v4

    .line 1676585
    if-eqz v4, :cond_6

    .line 1676586
    iget-object v5, v0, LX/AT6;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result p1

    float-to-int p1, p1

    invoke-virtual {v5, v6, p1, v4}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(IILandroid/graphics/Matrix;)V

    .line 1676587
    :cond_6
    iget-object v4, v0, LX/AT6;->f:LX/ASb;

    iget-object v5, v0, LX/AT6;->n:Lcom/facebook/photos/base/media/PhotoItem;

    invoke-virtual {v4, v5}, LX/ASb;->a(Lcom/facebook/ipc/media/MediaItem;)Ljava/util/List;

    move-result-object v4

    iput-object v4, v0, LX/AT6;->m:Ljava/util/List;

    .line 1676588
    iget-object v4, v0, LX/AT6;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    iget-object v5, v0, LX/AT6;->g:LX/9ic;

    iget-object v6, v0, LX/AT6;->m:Ljava/util/List;

    invoke-virtual {v5, v6}, LX/9ic;->a(Ljava/util/List;)Ljava/util/Collection;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->setFaceBoxes(Ljava/util/Collection;)V

    .line 1676589
    iget-object v4, v0, LX/AT6;->b:LX/8Jm;

    iget-object v5, v0, LX/AT6;->g:LX/9ic;

    iget-object v6, v0, LX/AT6;->m:Ljava/util/List;

    .line 1676590
    iget-object p1, v5, LX/9ic;->c:Landroid/graphics/RectF;

    iget v2, v5, LX/9ic;->d:I

    invoke-static {v6, p1, v2}, LX/9ic;->a(Ljava/util/List;Landroid/graphics/RectF;I)Ljava/util/List;

    move-result-object p1

    move-object v5, p1

    .line 1676591
    invoke-virtual {v4, v5}, LX/8Jm;->a(Ljava/util/Collection;)V

    .line 1676592
    iget-object v4, v0, LX/AT6;->m:Ljava/util/List;

    if-eqz v4, :cond_7

    if-eqz v3, :cond_7

    .line 1676593
    iget-object v4, v0, LX/AT6;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v4}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a()V

    .line 1676594
    :cond_7
    invoke-static {v0}, LX/AT6;->a(LX/AT6;)V

    .line 1676595
    invoke-virtual {p0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->e()V

    goto/16 :goto_0
.end method

.method public setScale(F)V
    .locals 0

    .prologue
    .line 1676596
    iput p1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->at:F

    .line 1676597
    invoke-virtual {p0, p1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setScaleX(F)V

    .line 1676598
    invoke-virtual {p0, p1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setScaleY(F)V

    .line 1676599
    invoke-virtual {p0, p1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->setAlpha(F)V

    .line 1676600
    return-void
.end method
