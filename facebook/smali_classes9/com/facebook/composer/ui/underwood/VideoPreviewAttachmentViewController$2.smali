.class public final Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/ATs;


# direct methods
.method public constructor <init>(LX/ATs;)V
    .locals 0

    .prologue
    .line 1677049
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$2;->a:LX/ATs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1677050
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$2;->a:LX/ATs;

    iget-object v0, v0, LX/ATs;->f:LX/0ad;

    sget-short v1, LX/8tG;->l:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1677051
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$2;->a:LX/ATs;

    iget-object v0, v0, LX/ATs;->w:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;

    const/4 p0, 0x0

    .line 1677052
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    if-nez v1, :cond_1

    .line 1677053
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->h:Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    .line 1677054
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->a:LX/0ad;

    sget-short v2, LX/8tG;->m:S

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1677055
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    const v2, 0x7f0d308d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1677056
    const v2, 0x7f0209bc

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1677057
    :cond_0
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->b:LX/BTf;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    invoke-virtual {v1, v2, v3}, LX/BTf;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1677058
    :cond_1
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setVisibility(I)V

    .line 1677059
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->i:Landroid/view/View;

    new-instance v2, LX/ATp;

    invoke-direct {v2, v0}, LX/ATp;-><init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1677060
    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;->l(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentView;)V

    .line 1677061
    :cond_2
    return-void
.end method
