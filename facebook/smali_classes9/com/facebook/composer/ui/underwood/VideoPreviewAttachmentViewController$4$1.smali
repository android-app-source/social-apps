.class public final Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;)V
    .locals 0

    .prologue
    .line 1677072
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4$1;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1677073
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4$1;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->h:LX/ATL;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4$1;->a:Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->b:LX/ATs;

    iget-object v1, v1, LX/ATs;->v:Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1677074
    iget-object v2, v0, LX/ATL;->a:LX/ATO;

    iget-object v2, v2, LX/ATO;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    .line 1677075
    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    .line 1677076
    :goto_0
    return-void

    .line 1677077
    :cond_0
    invoke-static {v1}, LX/7kv;->a(Lcom/facebook/composer/attachments/ComposerAttachment;)LX/7kv;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->g()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;->a(Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v4

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->setHasFaceDetectionFinished(Z)Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo$Builder;->a()Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    move-result-object v4

    .line 1677078
    iput-object v4, v3, LX/7kv;->g:Lcom/facebook/ipc/composer/model/ComposerVideoTaggingInfo;

    .line 1677079
    move-object v3, v3

    .line 1677080
    invoke-virtual {v3}, LX/7kv;->a()Lcom/facebook/composer/attachments/ComposerAttachment;

    move-result-object v3

    .line 1677081
    invoke-static {v0, v2, v3}, LX/ATL;->a(LX/ATL;ILcom/facebook/composer/attachments/ComposerAttachment;)V

    goto :goto_0
.end method
