.class public final Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/attachments/ComposerAttachment;

.field public final synthetic b:LX/ATs;


# direct methods
.method public constructor <init>(LX/ATs;Lcom/facebook/composer/attachments/ComposerAttachment;)V
    .locals 0

    .prologue
    .line 1677082
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->b:LX/ATs;

    iput-object p2, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 1677083
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1677084
    :goto_0
    return-void

    .line 1677085
    :cond_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->b:LX/ATs;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v0, v1}, LX/ATs;->a(LX/ATs;Lcom/facebook/composer/attachments/ComposerAttachment;)J

    move-result-wide v8

    .line 1677086
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1677087
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimStartTimeMs:I

    int-to-long v4, v0

    .line 1677088
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->f()Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/video/creativeediting/model/VideoCreativeEditingData;->getVideoTrimParams()Lcom/facebook/photos/creativeediting/model/VideoTrimParams;

    move-result-object v0

    iget v0, v0, Lcom/facebook/photos/creativeediting/model/VideoTrimParams;->videoTrimEndTimeMs:I

    int-to-long v6, v0

    .line 1677089
    :goto_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->z:LX/Azc;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v1}, Lcom/facebook/composer/attachments/ComposerAttachment;->c()Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    .line 1677090
    iget-object v3, v2, Lcom/facebook/ipc/media/MediaItem;->c:Lcom/facebook/ipc/media/data/LocalMediaData;

    move-object v2, v3

    .line 1677091
    iget-wide v11, v2, Lcom/facebook/ipc/media/data/LocalMediaData;->mMediaStoreId:J

    move-wide v2, v11

    .line 1677092
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->b:LX/ATs;

    iget-object v10, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->a:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-static {v3, v10}, LX/ATs;->e(LX/ATs;Lcom/facebook/composer/attachments/ComposerAttachment;)F

    move-result v3

    invoke-virtual/range {v0 .. v9}, LX/Azc;->a(Landroid/net/Uri;Ljava/lang/String;FJJJ)V

    .line 1677093
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;->b:LX/ATs;

    iget-object v0, v0, LX/ATs;->r:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4$1;

    invoke-direct {v1, p0}, Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4$1;-><init>(Lcom/facebook/composer/ui/underwood/VideoPreviewAttachmentViewController$4;)V

    const v2, 0x143a4af3

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0

    .line 1677094
    :cond_1
    const-wide/16 v4, 0x0

    move-wide v6, v8

    .line 1677095
    goto :goto_1
.end method
