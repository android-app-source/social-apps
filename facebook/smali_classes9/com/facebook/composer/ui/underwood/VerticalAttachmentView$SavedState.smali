.class public final Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;
.super Landroid/view/View$BaseSavedState;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1676313
    new-instance v0, LX/ATh;

    invoke-direct {v0}, LX/ATh;-><init>()V

    sput-object v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1676314
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1676315
    const-class v0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->a:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1676316
    const-class v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    iput-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1676317
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;)V
    .locals 0
    .param p2    # Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1676309
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1676310
    iput-object p2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->a:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1676311
    iput-object p3, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    .line 1676312
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1676305
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1676306
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->a:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1676307
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$SavedState;->b:Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1676308
    return-void
.end method
