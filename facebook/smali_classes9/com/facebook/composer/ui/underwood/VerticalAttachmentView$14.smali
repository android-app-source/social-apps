.class public final Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 0

    .prologue
    .line 1676114
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1676115
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-boolean v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aP:Z

    if-nez v0, :cond_2

    .line 1676116
    new-instance v1, Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ai:I

    int-to-float v0, v0

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aw:I

    int-to-float v2, v2

    invoke-direct {v1, v3, v3, v0, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1676117
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    invoke-virtual {v0}, LX/9d5;->f()Landroid/graphics/RectF;

    move-result-object v2

    .line 1676118
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aq:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AT6;

    .line 1676119
    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676120
    iget-object v3, v0, LX/AT6;->d:Lcom/facebook/photos/tagging/shared/FaceBoxesView;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v5, v5

    invoke-static {v2, v1}, LX/AT6;->b(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/Matrix;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, Lcom/facebook/photos/tagging/shared/FaceBoxesView;->a(IILandroid/graphics/Matrix;)V

    .line 1676121
    iget-object v3, v0, LX/AT6;->b:LX/8Jm;

    invoke-virtual {v3}, LX/8Jm;->a()V

    .line 1676122
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->e()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1676123
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1676124
    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    .line 1676125
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->k:LX/9cK;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ar:Lcom/facebook/composer/attachments/ComposerAttachment;

    invoke-virtual {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->b()Lcom/facebook/ipc/media/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->g()I

    move-result v2

    .line 1676126
    iget-object v3, v1, LX/9cK;->a:LX/9cL;

    .line 1676127
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1676128
    iput-object v0, v3, LX/9cL;->h:Landroid/graphics/Rect;

    .line 1676129
    iget-object v4, v3, LX/9cL;->d:LX/0Px;

    if-eqz v4, :cond_0

    iget-object v4, v3, LX/9cL;->d:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_0
    iget-object v4, v3, LX/9cL;->e:LX/0Px;

    if-eqz v4, :cond_1

    iget-object v4, v3, LX/9cL;->e:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1676130
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ac:Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/renderers/CreativeEditingPhotoOverlayView;->invalidate()V

    .line 1676131
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->h()V

    .line 1676132
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$14;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->f()V

    .line 1676133
    return-void

    .line 1676134
    :cond_3
    iget-boolean v4, v3, LX/9cL;->c:Z

    if-eqz v4, :cond_5

    .line 1676135
    iget-object v4, v3, LX/9cL;->b:Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;

    .line 1676136
    iput-object v0, v4, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->n:Landroid/graphics/Rect;

    .line 1676137
    iget-object v5, v4, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/5i8;

    .line 1676138
    invoke-interface {v5, v0}, LX/5i8;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v3

    .line 1676139
    iget-object v2, v4, Lcom/facebook/photos/creativeediting/renderers/MovableItemContainer;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v5}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1aX;

    invoke-virtual {v5}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    goto :goto_1

    .line 1676140
    :cond_4
    goto :goto_0

    .line 1676141
    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-static {v3, v4, v5, v2}, LX/9cL;->a(LX/9cL;III)V

    goto :goto_0
.end method
