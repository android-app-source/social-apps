.class public final Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)V
    .locals 0

    .prologue
    .line 1676056
    iput-object p1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1676057
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    sget-short v1, LX/8tG;->l:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1676058
    :cond_0
    :goto_0
    return-void

    .line 1676059
    :cond_1
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    const/4 v1, 0x1

    .line 1676060
    iput-boolean v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aP:Z

    .line 1676061
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ap:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9d5;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    sget-object v2, LX/BTC;->TAP_VIDEO:LX/BTC;

    invoke-static {v1, v2}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a$redex0(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/BTC;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/9d5;->a(Landroid/view/View$OnClickListener;)V

    .line 1676062
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    const/4 v5, 0x1

    .line 1676063
    new-instance v2, LX/5SM;

    invoke-direct {v2}, LX/5SM;-><init>()V

    sget-object v4, LX/5SK;->TRIM:LX/5SK;

    invoke-virtual {v2, v4}, LX/5SM;->a(LX/5SK;)LX/5SM;

    move-result-object v2

    iget-object v4, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->al:Ljava/lang/String;

    .line 1676064
    iput-object v4, v2, LX/5SM;->b:Ljava/lang/String;

    .line 1676065
    move-object v2, v2

    .line 1676066
    iput-boolean v5, v2, LX/5SM;->d:Z

    .line 1676067
    move-object v2, v2

    .line 1676068
    iput-boolean v5, v2, LX/5SM;->e:Z

    .line 1676069
    move-object v2, v2

    .line 1676070
    iget-object v4, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    sget-short v5, LX/8tG;->h:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 1676071
    iput-boolean v4, v2, LX/5SM;->j:Z

    .line 1676072
    move-object v2, v2

    .line 1676073
    invoke-virtual {v2}, LX/5SM;->a()Lcom/facebook/ipc/videoeditgallery/VideoEditGalleryLaunchConfiguration;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    move-object v1, v2

    .line 1676074
    iput-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aK:LX/0am;

    .line 1676075
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aK:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1676076
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    if-nez v0, :cond_3

    .line 1676077
    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->V:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1676078
    iput-object v0, v1, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    .line 1676079
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    sget-short v1, LX/8tG;->m:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1676080
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    const v1, 0x7f0d308d

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1676081
    const v1, 0x7f0209bc

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1676082
    :cond_2
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->G:LX/BTf;

    iget-object v1, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    invoke-virtual {v1}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1, v2}, LX/BTf;->a(Landroid/content/Context;Landroid/view/View;)V

    .line 1676083
    :cond_3
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    new-instance v1, LX/BSt;

    iget-object v2, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v2, v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ab:LX/0gc;

    invoke-direct {v1, v2}, LX/BSt;-><init>(LX/0gc;)V

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1676084
    iput-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ay:LX/0am;

    .line 1676085
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    iget-object v0, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->ay:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1676086
    iget-object v0, p0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView$12;->a:Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;

    .line 1676087
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    if-nez v1, :cond_4

    .line 1676088
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a:LX/03V;

    sget-object v2, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->L:Ljava/lang/String;

    const-string v3, "Video Editing Icon not initialized"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1676089
    :goto_1
    goto/16 :goto_0

    .line 1676090
    :cond_4
    iget-boolean v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aQ:Z

    if-eqz v1, :cond_5

    .line 1676091
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    const v2, 0x7f0d308e

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->x:LX/0ad;

    sget-char v3, LX/1EB;->H:C

    const v4, 0x7f0813dc

    invoke-virtual {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, LX/0ad;->a(CILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1676092
    :cond_5
    iget-object v2, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->y(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;)Z

    move-result v1

    if-eqz v1, :cond_6

    const/16 v1, 0x8

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1676093
    iget-object v1, v0, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->aB:Landroid/widget/LinearLayout;

    sget-object v2, LX/BTC;->CLICK_EDIT_BUTTON:LX/BTC;

    invoke-static {v0, v2}, Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;->a$redex0(Lcom/facebook/composer/ui/underwood/VerticalAttachmentView;LX/BTC;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 1676094
    :cond_6
    const/4 v1, 0x0

    goto :goto_2
.end method
