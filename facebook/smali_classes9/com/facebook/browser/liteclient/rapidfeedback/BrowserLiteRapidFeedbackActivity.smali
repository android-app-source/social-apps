.class public Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/1bH;


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field public static p:Ljava/lang/String;


# instance fields
.field public q:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1799442
    const-string v0, "browser_lite_feedback/?id=%s&session_id=%s&stage=%s&page_view_time=%s"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1799443
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;

    const/16 v1, 0x122d

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->q:LX/0Or;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1799444
    invoke-static {p0, p0}, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1799445
    invoke-virtual {p0}, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1799446
    const-string v1, "integration_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1799447
    iget-object v0, p0, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->q:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 1799448
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1799449
    move-object v0, v0

    .line 1799450
    const-string v1, "session_id"

    invoke-virtual {p0}, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "session_id"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v0

    const-string v1, "stage"

    invoke-virtual {p0}, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "stage"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v0

    const-string v1, "page_view_time"

    invoke-virtual {p0}, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "page_view_time"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/0gt;->b(Landroid/content/Context;)V

    .line 1799451
    return-void
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 1799452
    invoke-virtual {p0}, Lcom/facebook/browser/liteclient/rapidfeedback/BrowserLiteRapidFeedbackActivity;->finish()V

    .line 1799453
    return-void
.end method
