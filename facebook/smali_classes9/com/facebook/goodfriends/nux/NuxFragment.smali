.class public abstract Lcom/facebook/goodfriends/nux/NuxFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field public a:Landroid/support/v4/view/ViewPager;

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1733506
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1733507
    iput-boolean v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->b:Z

    .line 1733508
    iput-boolean v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->c:Z

    return-void
.end method


# virtual methods
.method public final c(Z)V
    .locals 2

    .prologue
    .line 1733509
    iget-boolean v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->c:Z

    if-nez v0, :cond_0

    .line 1733510
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t move to next page because not onboarding"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1733511
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v1

    invoke-virtual {v1}, LX/0gG;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    .line 1733512
    :goto_0
    if-eqz v0, :cond_3

    .line 1733513
    if-eqz p1, :cond_1

    .line 1733514
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1733515
    :cond_1
    :goto_1
    return-void

    .line 1733516
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1733517
    :cond_3
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1733518
    iget-boolean v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->c:Z

    if-nez v0, :cond_0

    .line 1733519
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t move to previous page because not onboarding"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1733520
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-nez v0, :cond_1

    .line 1733521
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1733522
    :goto_0
    return-void

    .line 1733523
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->a:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/facebook/goodfriends/nux/NuxFragment;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method public abstract d(Z)V
.end method

.method public setUserVisibleHint(Z)V
    .locals 1

    .prologue
    .line 1733524
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->setUserVisibleHint(Z)V

    .line 1733525
    if-eqz p1, :cond_0

    .line 1733526
    iget-boolean v0, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v0

    .line 1733527
    if-eqz v0, :cond_0

    .line 1733528
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/nux/NuxFragment;->d(Z)V

    .line 1733529
    :cond_0
    return-void
.end method
