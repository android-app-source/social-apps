.class public Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Ljava/lang/Void;",
        "LX/B0H;",
        "LX/1Pn;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/22r;

.field public b:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/22r;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1733553
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1733554
    iput-object p1, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->a:LX/22r;

    .line 1733555
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;
    .locals 4

    .prologue
    .line 1733556
    const-class v1, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;

    monitor-enter v1

    .line 1733557
    :try_start_0
    sget-object v0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1733558
    sput-object v2, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1733559
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733560
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1733561
    new-instance p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;

    invoke-static {v0}, LX/22r;->b(LX/0QB;)LX/22r;

    move-result-object v3

    check-cast v3, LX/22r;

    invoke-direct {p0, v3}, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;-><init>(LX/22r;)V

    .line 1733562
    move-object v0, p0

    .line 1733563
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1733564
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1733565
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1733566
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1733567
    check-cast p3, LX/1Pn;

    .line 1733568
    new-instance v0, LX/B0H;

    new-instance v1, LX/9J4;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/9J4;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/B0H;-><init>(LX/9J4;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x4920f820

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1733569
    check-cast p2, LX/B0H;

    .line 1733570
    iget-object v1, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->a:LX/22r;

    .line 1733571
    iget-object v2, v1, LX/22r;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object p1, LX/22r;->a:LX/0Tn;

    const/4 p3, 0x0

    invoke-interface {v2, p1, p3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    move v1, v2

    .line 1733572
    if-eqz v1, :cond_0

    iget-object v1, p2, LX/B0H;->a:LX/9J4;

    if-eqz v1, :cond_0

    invoke-virtual {p4}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1733573
    new-instance v1, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;

    invoke-direct {v1, p0, p2, p4}, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;-><init>(Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;LX/B0H;Landroid/view/View;)V

    iput-object v1, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->b:Ljava/lang/Runnable;

    .line 1733574
    iget-object v1, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->b:Ljava/lang/Runnable;

    invoke-virtual {p4, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1733575
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x427b8dca

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1733576
    check-cast p1, Ljava/lang/Void;

    check-cast p2, LX/B0H;

    check-cast p3, LX/1Pn;

    .line 1733577
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1733578
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->b:Ljava/lang/Runnable;

    invoke-virtual {p4, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1733579
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->b:Ljava/lang/Runnable;

    .line 1733580
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;->b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V

    .line 1733581
    return-void
.end method
