.class public final Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/B0H;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;LX/B0H;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1733530
    iput-object p1, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;->c:Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;

    iput-object p2, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;->a:LX/B0H;

    iput-object p3, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;->b:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1733531
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;->a:LX/B0H;

    iget-object v0, v0, LX/B0H;->a:LX/9J4;

    .line 1733532
    if-eqz v0, :cond_0

    .line 1733533
    iget-object v1, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;->b:Landroid/view/View;

    .line 1733534
    iget-object v2, v0, LX/9J4;->d:LX/9J2;

    const-string v3, "Composer Nux Shown"

    invoke-virtual {v2, v3}, LX/9J2;->a(Ljava/lang/String;)V

    .line 1733535
    iget-object v2, v0, LX/9J4;->g:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1733536
    const/4 v3, 0x2

    new-array v3, v3, [I

    .line 1733537
    invoke-virtual {v1, v3}, Landroid/view/View;->getLocationInWindow([I)V

    .line 1733538
    const/4 v4, 0x1

    aget v3, v3, v4

    move v3, v3

    .line 1733539
    iget-object v4, v0, LX/9J4;->c:LX/0hI;

    invoke-virtual {v0}, LX/9J4;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0hI;->a(Landroid/view/Window;)I

    move-result v4

    .line 1733540
    sub-int/2addr v3, v4

    move v3, v3

    .line 1733541
    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1733542
    iget-object v3, v0, LX/9J4;->g:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1733543
    iget-object v2, v0, LX/9J4;->f:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 1733544
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v3

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1733545
    iget-object v3, v0, LX/9J4;->f:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1733546
    invoke-virtual {v0}, LX/9J4;->show()V

    .line 1733547
    iget-object v0, p0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition$1;->c:Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;

    iget-object v0, v0, Lcom/facebook/goodfriends/nux/overlay/InlineComposerGoodFriendsNuxOverlayPartDefinition;->a:LX/22r;

    .line 1733548
    iget-object v1, v0, LX/22r;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/22r;->a:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 1733549
    :cond_0
    return-void
.end method
