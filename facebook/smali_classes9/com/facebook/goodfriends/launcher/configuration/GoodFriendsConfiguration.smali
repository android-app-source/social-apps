.class public Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Z

.field public final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1733504
    new-instance v0, LX/B0F;

    invoke-direct {v0}, LX/B0F;-><init>()V

    sput-object v0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/B0G;)V
    .locals 1

    .prologue
    .line 1733484
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733485
    iget-boolean v0, p1, LX/B0G;->a:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    .line 1733486
    iget-boolean v0, p1, LX/B0G;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    .line 1733487
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1733498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1733499
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    .line 1733500
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    .line 1733501
    return-void

    :cond_0
    move v0, v2

    .line 1733502
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1733503
    goto :goto_1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1733505
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1733489
    if-ne p0, p1, :cond_1

    .line 1733490
    :cond_0
    :goto_0
    return v0

    .line 1733491
    :cond_1
    instance-of v2, p1, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    if-nez v2, :cond_2

    move v0, v1

    .line 1733492
    goto :goto_0

    .line 1733493
    :cond_2
    check-cast p1, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;

    .line 1733494
    iget-boolean v2, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    iget-boolean v3, p1, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1733495
    goto :goto_0

    .line 1733496
    :cond_3
    iget-boolean v2, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    iget-boolean v3, p1, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1733497
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1733488
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1733479
    iget-boolean v0, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1733480
    iget-boolean v0, p0, Lcom/facebook/goodfriends/launcher/configuration/GoodFriendsConfiguration;->b:Z

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1733481
    return-void

    :cond_0
    move v0, v2

    .line 1733482
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1733483
    goto :goto_1
.end method
