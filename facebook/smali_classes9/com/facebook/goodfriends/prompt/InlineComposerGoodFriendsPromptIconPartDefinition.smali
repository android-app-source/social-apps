.class public Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aT;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Landroid/view/View$OnClickListener;",
        "LX/1af;",
        "LX/1Pn;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1733612
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1733613
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;
    .locals 3

    .prologue
    .line 1733594
    const-class v1, Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;

    monitor-enter v1

    .line 1733595
    :try_start_0
    sget-object v0, Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1733596
    sput-object v2, Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1733597
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733598
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1733599
    new-instance v0, Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;

    invoke-direct {v0}, Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;-><init>()V

    .line 1733600
    move-object v0, v0

    .line 1733601
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1733602
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/goodfriends/prompt/InlineComposerGoodFriendsPromptIconPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1733603
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1733604
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1733605
    check-cast p3, LX/1Pn;

    .line 1733606
    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1733607
    new-instance v1, LX/1Uo;

    invoke-direct {v1, v0}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    .line 1733608
    const v2, 0x7f0207b4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1733609
    const v3, 0x7f0a00e7

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v0, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1733610
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1733611
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x32b55f8f    # -2.1246952E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1733588
    check-cast p1, Landroid/view/View$OnClickListener;

    check-cast p2, LX/1af;

    .line 1733589
    move-object v1, p4

    check-cast v1, LX/1aT;

    invoke-interface {v1}, LX/1aT;->getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v1

    .line 1733590
    invoke-virtual {v1, p2}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 1733591
    invoke-virtual {v1, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1733592
    check-cast p4, LX/1aT;

    invoke-interface {p4}, LX/1aT;->getShimmerContainer()Lcom/facebook/widget/ShimmerFrameLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/ShimmerFrameLayout;->setVisibility(I)V

    .line 1733593
    const/16 v1, 0x1f

    const v2, -0x1bb2f8f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1733586
    check-cast p4, LX/1aT;

    invoke-interface {p4}, LX/1aT;->getIconView()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1733587
    return-void
.end method
