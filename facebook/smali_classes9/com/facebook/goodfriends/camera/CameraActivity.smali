.class public Lcom/facebook/goodfriends/camera/CameraActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# static fields
.field private static final u:[Ljava/lang/String;


# instance fields
.field public p:LX/87v;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/B0K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/9J2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private v:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

.field public w:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

.field private final x:LX/Aze;

.field private final y:LX/Azf;

.field private z:LX/0i5;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1732864
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.CAMERA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.RECORD_AUDIO"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.permission.WRITE_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/goodfriends/camera/CameraActivity;->u:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1732865
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1732866
    new-instance v0, LX/Aze;

    invoke-direct {v0, p0}, LX/Aze;-><init>(Lcom/facebook/goodfriends/camera/CameraActivity;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->x:LX/Aze;

    .line 1732867
    new-instance v0, LX/Azf;

    invoke-direct {v0, p0}, LX/Azf;-><init>(Lcom/facebook/goodfriends/camera/CameraActivity;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->y:LX/Azf;

    return-void
.end method

.method private static a(Lcom/facebook/goodfriends/camera/CameraActivity;LX/87v;LX/B0K;LX/0i4;LX/1Ml;LX/9J2;)V
    .locals 0

    .prologue
    .line 1732868
    iput-object p1, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->p:LX/87v;

    iput-object p2, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->q:LX/B0K;

    iput-object p3, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->r:LX/0i4;

    iput-object p4, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->s:LX/1Ml;

    iput-object p5, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->t:LX/9J2;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 12

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/goodfriends/camera/CameraActivity;

    invoke-static {v5}, LX/87v;->b(LX/0QB;)LX/87v;

    move-result-object v1

    check-cast v1, LX/87v;

    new-instance v6, LX/B0K;

    const/16 v7, 0x12cb

    invoke-static {v5, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v5}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v8

    check-cast v8, LX/74n;

    const-class v9, LX/ARD;

    invoke-interface {v5, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/ARD;

    const-class v10, LX/AR4;

    invoke-interface {v5, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/AR4;

    const-class v11, LX/AR2;

    invoke-interface {v5, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/AR2;

    invoke-direct/range {v6 .. v11}, LX/B0K;-><init>(LX/0Or;LX/74n;LX/ARD;LX/AR4;LX/AR2;)V

    move-object v2, v6

    check-cast v2, LX/B0K;

    const-class v3, LX/0i4;

    invoke-interface {v5, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/0i4;

    invoke-static {v5}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v4

    check-cast v4, LX/1Ml;

    invoke-static {v5}, LX/9J2;->a(LX/0QB;)LX/9J2;

    move-result-object v5

    check-cast v5, LX/9J2;

    invoke-static/range {v0 .. v5}, Lcom/facebook/goodfriends/camera/CameraActivity;->a(Lcom/facebook/goodfriends/camera/CameraActivity;LX/87v;LX/B0K;LX/0i4;LX/1Ml;LX/9J2;)V

    return-void
.end method

.method public static b$redex0(Lcom/facebook/goodfriends/camera/CameraActivity;)V
    .locals 2

    .prologue
    .line 1732869
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1732870
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 1732871
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->v:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1732872
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/16 v2, 0x400

    .line 1732873
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->s:LX/1Ml;

    sget-object v1, Lcom/facebook/goodfriends/camera/CameraActivity;->u:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1732874
    invoke-virtual {p0}, Lcom/facebook/goodfriends/camera/CameraActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 1732875
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "camera_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1732876
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->v:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

    const-string v3, "camera_fragment"

    invoke-virtual {v0, v1, v2, v3}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    .line 1732877
    :cond_0
    :goto_0
    return-void

    .line 1732878
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->z:LX/0i5;

    sget-object v1, Lcom/facebook/goodfriends/camera/CameraActivity;->u:[Ljava/lang/String;

    new-instance v2, LX/Azh;

    invoke-direct {v2, p0}, LX/Azh;-><init>(Lcom/facebook/goodfriends/camera/CameraActivity;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1732879
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1732880
    invoke-static {p0, p0}, Lcom/facebook/goodfriends/camera/CameraActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1732881
    const v0, 0x7f0307b2

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/camera/CameraActivity;->setContentView(I)V

    .line 1732882
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->r:LX/0i4;

    invoke-virtual {v0, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->z:LX/0i5;

    .line 1732883
    const v0, 0x7f0d002f

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/camera/CameraActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1732884
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "camera_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->v:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

    .line 1732885
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "preview_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->w:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    .line 1732886
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->v:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

    if-nez v0, :cond_0

    .line 1732887
    new-instance v0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

    invoke-direct {v0}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->v:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

    .line 1732888
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->w:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    if-nez v0, :cond_1

    .line 1732889
    new-instance v0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    invoke-direct {v0}, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->w:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    .line 1732890
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->v:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->x:LX/Aze;

    .line 1732891
    iput-object v1, v0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;->b:LX/Aze;

    .line 1732892
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->w:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->y:LX/Azf;

    .line 1732893
    iput-object v1, v0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->e:LX/Azf;

    .line 1732894
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->p:LX/87v;

    const/4 v1, 0x1

    new-instance v2, LX/Azg;

    invoke-direct {v2, p0}, LX/Azg;-><init>(Lcom/facebook/goodfriends/camera/CameraActivity;)V

    invoke-virtual {v0, v1, v2}, LX/87v;->a(ZLX/87u;)V

    .line 1732895
    invoke-virtual {p0}, Lcom/facebook/goodfriends/camera/CameraActivity;->a()V

    .line 1732896
    :cond_2
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1732897
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1732898
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/CameraActivity;->w:Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->i()Z

    .line 1732899
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->d()V

    .line 1732900
    :goto_0
    return-void

    .line 1732901
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method
