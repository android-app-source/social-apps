.class public Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

.field public b:LX/Aze;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1732953
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1732954
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1732955
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1732956
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1732957
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x41e5f2c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1732958
    const v1, 0x7f0307b4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x7a22d32e

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x31a9e5bf

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1732959
    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;->a:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

    invoke-virtual {v1}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->b()V

    .line 1732960
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1732961
    const/16 v1, 0x2b

    const v2, -0x24d87fc7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3016cd6d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1732962
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1732963
    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;->a:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

    invoke-virtual {v1}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->a()V

    .line 1732964
    const/16 v1, 0x2b

    const v2, 0x36ed4bc7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1732965
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1732966
    const v0, 0x7f0d147d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;->a:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

    .line 1732967
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;->a:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->setSelfie(Z)V

    .line 1732968
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;->a:Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

    new-instance v1, LX/Azn;

    invoke-direct {v1, p0}, LX/Azn;-><init>(Lcom/facebook/goodfriends/camera/GoodFriendsCameraFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->setInteractionListener(LX/Azm;)V

    .line 1732969
    return-void
.end method
