.class public Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/7gj;

.field public d:Lcom/facebook/backstage/camera/PreviewView;

.field public e:LX/Azf;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1733464
    const-class v0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1733462
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1733463
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    invoke-static {p0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object p0

    check-cast p0, LX/0hB;

    iput-object p0, p1, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->a:LX/0hB;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733459
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1733460
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/support/v4/app/Fragment;->setRetainInstance(Z)V

    .line 1733461
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1733465
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/PreviewView;->d()V

    .line 1733466
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->i()Z

    move-result v0

    return v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x43d061c3

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1733458
    const v1, 0x7f0307ca

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x4d27a7b3    # 1.75799088E8f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4eabe0a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1733455
    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    invoke-virtual {v1}, Lcom/facebook/backstage/camera/PreviewView;->b()V

    .line 1733456
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1733457
    const/16 v1, 0x2b

    const v2, 0x4f86d7d4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x15e8ce4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1733452
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1733453
    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    invoke-virtual {v1}, Lcom/facebook/backstage/camera/PreviewView;->a()V

    .line 1733454
    const/16 v1, 0x2b

    const v2, 0x2ce3fa24

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p2    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733431
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1733432
    const-class v0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;

    invoke-static {v0, p0}, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1733433
    const v0, 0x7f0d14a1

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/camera/PreviewView;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    .line 1733434
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/PreviewView;->c()V

    .line 1733435
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    .line 1733436
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    const v1, 0x3faaaaab

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/camera/PreviewView;->setPreviewAspectRatio(F)V

    .line 1733437
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    const v1, 0x7f0823fe

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/camera/PreviewView;->setShareButtonLabel(I)V

    .line 1733438
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    new-instance v1, LX/B0C;

    invoke-direct {v1, p0}, LX/B0C;-><init>(Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;)V

    .line 1733439
    iput-object v1, v0, Lcom/facebook/backstage/camera/PreviewView;->u:LX/ALj;

    .line 1733440
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->c:LX/7gj;

    if-nez v0, :cond_1

    .line 1733441
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->e:LX/Azf;

    if-eqz v0, :cond_0

    .line 1733442
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->e:LX/Azf;

    invoke-virtual {v0}, LX/Azf;->a()V

    .line 1733443
    :cond_0
    :goto_0
    return-void

    .line 1733444
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->c:LX/7gj;

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/camera/PreviewView;->setShot(LX/7gj;)V

    .line 1733445
    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->c:LX/7gj;

    .line 1733446
    iget v2, v0, LX/7gj;->j:I

    move v0, v2

    .line 1733447
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->c:LX/7gj;

    .line 1733448
    iget v2, v0, LX/7gj;->j:I

    move v0, v2

    .line 1733449
    const/16 v2, 0xb4

    if-ne v0, v2, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/backstage/camera/PreviewView;->a(Z)V

    .line 1733450
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->d:Lcom/facebook/backstage/camera/PreviewView;

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsPreviewFragment;->c:LX/7gj;

    invoke-virtual {v1}, LX/7gj;->h()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/backstage/camera/PreviewView;->a(Z)V

    goto :goto_0

    .line 1733451
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method
