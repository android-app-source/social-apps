.class public Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private i:LX/1OR;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1733167
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733168
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1733169
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733170
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1733171
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733172
    invoke-direct {p0, p1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;->a(Landroid/content/Context;)V

    .line 1733173
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1733174
    new-instance v0, LX/1P0;

    invoke-direct {v0, p1, v1, v1}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;->i:LX/1OR;

    .line 1733175
    new-instance v0, LX/62Z;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b190d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/62Z;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1733176
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;->i:LX/1OR;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1733177
    return-void
.end method
