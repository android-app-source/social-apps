.class public Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/74n;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ns;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1Nq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;

.field public f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

.field private g:LX/1aw;

.field private h:Landroid/content/Context;

.field private final i:LX/1OZ;

.field private final j:LX/B01;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1733247
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733248
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733255
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733256
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733249
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733250
    new-instance v0, LX/B00;

    invoke-direct {v0, p0}, LX/B00;-><init>(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->i:LX/1OZ;

    .line 1733251
    new-instance v0, LX/B01;

    invoke-direct {v0, p0}, LX/B01;-><init>(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;)V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->j:LX/B01;

    .line 1733252
    const-class v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    invoke-static {v0, p0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1733253
    invoke-direct {p0, p1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->a(Landroid/content/Context;)V

    .line 1733254
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1733233
    iput-object p1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->h:Landroid/content/Context;

    .line 1733234
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307bd

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1733235
    const v0, 0x7f0d1492

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1733236
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->d:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setEnabled(Z)V

    .line 1733237
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->d:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/B02;

    invoke-direct {v1, p0}, LX/B02;-><init>(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1733238
    new-instance v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    invoke-direct {v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    .line 1733239
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->j:LX/B01;

    .line 1733240
    iput-object v1, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->d:LX/B01;

    .line 1733241
    const v0, 0x7f0d1491

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->e:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;

    .line 1733242
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->e:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1733243
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->e:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryHScrollView;

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->i:LX/1OZ;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setOnItemClickListener(LX/1OZ;)V

    .line 1733244
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->b:LX/1Ns;

    new-instance v1, LX/1Qg;

    invoke-direct {v1}, LX/1Qg;-><init>()V

    new-instance v2, LX/AnN;

    invoke-direct {v2}, LX/AnN;-><init>()V

    invoke-virtual {v0, v1, v2}, LX/1Ns;->a(LX/1Qh;LX/1DQ;)LX/1aw;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->g:LX/1aw;

    .line 1733245
    return-void
.end method

.method private static a(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;LX/74n;LX/1Ns;LX/1Nq;)V
    .locals 0

    .prologue
    .line 1733246
    iput-object p1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->a:LX/74n;

    iput-object p2, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->b:LX/1Ns;

    iput-object p3, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->c:LX/1Nq;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    invoke-static {v2}, LX/74n;->b(LX/0QB;)LX/74n;

    move-result-object v0

    check-cast v0, LX/74n;

    const-class v1, LX/1Ns;

    invoke-interface {v2, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/1Ns;

    invoke-static {v2}, LX/1Nq;->b(LX/0QB;)LX/1Nq;

    move-result-object v2

    check-cast v2, LX/1Nq;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->a(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;LX/74n;LX/1Ns;LX/1Nq;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 1733231
    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->g:LX/1aw;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->h:Landroid/content/Context;

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    const-string v3, "goodFriendsSeeAllButton"

    iget-object v4, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->c:LX/1Nq;

    iget-object v5, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->h:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0823f8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, LX/1aw;->a(Ljava/lang/String;Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)V

    .line 1733232
    return-void
.end method

.method public final a(LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1733220
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1733221
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 1733222
    iget-object v4, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->a:LX/74n;

    .line 1733223
    iget-object v5, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v0, v5

    .line 1733224
    sget-object v5, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v4, v0, v5}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1733225
    if-eqz v0, :cond_0

    .line 1733226
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1733227
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1733228
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1733229
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->g:LX/1aw;

    const/4 v1, 0x0

    const-string v2, "goodfriendsAddToPostButton"

    iget-object v4, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->c:LX/1Nq;

    iget-object v5, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->h:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0823f8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a(Ljava/lang/String;)Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->h:Landroid/content/Context;

    const-class v6, Landroid/app/Activity;

    invoke-static {v5, v6}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    invoke-virtual/range {v0 .. v5}, LX/1aw;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;Landroid/app/Activity;)V

    .line 1733230
    return-void
.end method

.method public setMediaData(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1733216
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->f:Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    .line 1733217
    iput-object p1, v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->b:LX/0Px;

    .line 1733218
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1733219
    return-void
.end method
