.class public final Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1;


# direct methods
.method public constructor <init>(Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1733387
    iput-object p1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1$1;->b:Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1;

    iput-object p2, p0, Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1$1;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1733388
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1$1;->b:Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1;

    iget-object v0, v0, Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1;->b:LX/B09;

    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsMediaFetcher$1$1;->a:Ljava/util/List;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1733389
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1733390
    iget-object v2, v0, LX/B09;->a:LX/B0A;

    .line 1733391
    iget-object v3, v2, LX/B0A;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->b()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1733392
    iget-object v3, v2, LX/B0A;->f:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodfriends/camera/GoodFriendsNoGalleryMediaView;

    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsNoGalleryMediaView;->setVisibility(I)V

    .line 1733393
    :cond_0
    iget-object v2, v0, LX/B09;->a:LX/B0A;

    invoke-static {v2}, LX/B0A;->c$redex0(LX/B0A;)V

    .line 1733394
    iget-object v2, v0, LX/B09;->a:LX/B0A;

    invoke-static {v2}, LX/B0A;->g(LX/B0A;)V

    .line 1733395
    :goto_0
    return-void

    .line 1733396
    :cond_1
    iget-object v2, v0, LX/B09;->a:LX/B0A;

    .line 1733397
    iget-object v3, v2, LX/B0A;->e:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->b()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1733398
    iget-object v3, v2, LX/B0A;->e:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    .line 1733399
    const/4 p0, 0x0

    invoke-virtual {v3, p0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->setVisibility(I)V

    .line 1733400
    :cond_2
    iget-object v3, v2, LX/B0A;->e:LX/0zw;

    invoke-virtual {v3}, LX/0zw;->a()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;

    invoke-virtual {v3, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryImportView;->setMediaData(LX/0Px;)V

    .line 1733401
    iget-object v2, v0, LX/B09;->a:LX/B0A;

    invoke-static {v2}, LX/B0A;->g(LX/B0A;)V

    .line 1733402
    iget-object v2, v0, LX/B09;->a:LX/B0A;

    invoke-static {v2}, LX/B0A;->e(LX/B0A;)V

    goto :goto_0
.end method
