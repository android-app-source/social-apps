.class public Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/62U;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/B01;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1733318
    const-class v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1733313
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1733314
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1733315
    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->b:LX/0Px;

    .line 1733316
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    .line 1733317
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 1733293
    const/4 v2, 0x0

    .line 1733294
    sget-object v0, LX/1po;->PHOTO:LX/1po;

    invoke-virtual {v0}, LX/1po;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 1733295
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307be

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1733296
    new-instance v0, LX/B06;

    invoke-direct {v0, v1}, LX/B06;-><init>(Landroid/view/View;)V

    .line 1733297
    :goto_0
    return-object v0

    .line 1733298
    :cond_0
    sget-object v0, LX/1po;->VIDEO:LX/1po;

    invoke-virtual {v0}, LX/1po;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 1733299
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307c0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1733300
    new-instance v0, LX/B07;

    invoke-direct {v0, v1}, LX/B07;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1733301
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    .line 1733302
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307bf

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1733303
    new-instance v0, LX/62U;

    invoke-direct {v0, v1}, LX/62U;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 1733304
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1733319
    check-cast p1, LX/62U;

    .line 1733320
    invoke-virtual {p0, p2}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1733321
    :cond_0
    :goto_0
    return-void

    .line 1733322
    :cond_1
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->b:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 1733323
    iget-object v1, v0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    move-object v2, v1

    .line 1733324
    sget-object v1, LX/1po;->UNKNOWN:LX/1po;

    if-eq v2, v1, :cond_0

    move-object v1, p1

    .line 1733325
    check-cast v1, LX/B06;

    iget-object v1, v1, LX/B06;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1733326
    iget-object v3, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v3, v3

    .line 1733327
    sget-object v4, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    move-object v1, p1

    .line 1733328
    check-cast v1, LX/B06;

    iget-object v1, v1, LX/B06;->n:Landroid/view/View;

    .line 1733329
    iget-object v3, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1733330
    const v3, 0x7f02003f

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1733331
    :goto_1
    sget-object v1, LX/1po;->VIDEO:LX/1po;

    if-ne v2, v1, :cond_0

    .line 1733332
    check-cast p1, LX/B07;

    .line 1733333
    iget-object v1, p1, LX/B07;->o:Lcom/facebook/resources/ui/FbTextView;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1733334
    iget v3, v0, Lcom/facebook/media/util/model/MediaModel;->c:I

    move v0, v3

    .line 1733335
    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    long-to-int v0, v2

    invoke-static {v0}, LX/BV7;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1733336
    :cond_2
    const v3, 0x7f020157

    invoke-virtual {v1, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public final e(I)Z
    .locals 1

    .prologue
    .line 1733312
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1733306
    invoke-virtual {p0, p1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1733307
    const/4 v0, -0x1

    .line 1733308
    :goto_0
    return v0

    .line 1733309
    :cond_0
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 1733310
    iget-object p0, v0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    move-object v0, p0

    .line 1733311
    invoke-virtual {v0}, LX/1po;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1733305
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryScrollAdapter;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
