.class public Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Lcom/facebook/backstage/camera/CameraView;

.field public c:LX/Azm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1732947
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1732948
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1732945
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1732946
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const v1, 0x3faaaaab

    .line 1732936
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1732937
    const-class v0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

    invoke-static {v0, p0}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1732938
    const v0, 0x7f0307b3

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1732939
    const v0, 0x7f0d147c

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/backstage/camera/CameraView;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->b:Lcom/facebook/backstage/camera/CameraView;

    .line 1732940
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    .line 1732941
    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x358637bd    # 1.0E-6f

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 1732942
    const/high16 v0, 0x3f800000    # 1.0f

    .line 1732943
    :goto_0
    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->b:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v1, v0}, Lcom/facebook/backstage/camera/CameraView;->setCameraPreviewAspectRatio(F)V

    .line 1732944
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v0

    check-cast v0, LX/0hB;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->a:LX/0hB;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1732934
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->b:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->b()V

    .line 1732935
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1732932
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->b:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0}, Lcom/facebook/backstage/camera/CameraView;->a()V

    .line 1732933
    return-void
.end method

.method public setInteractionListener(LX/Azm;)V
    .locals 2

    .prologue
    .line 1732928
    iput-object p1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->c:LX/Azm;

    .line 1732929
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->b:Lcom/facebook/backstage/camera/CameraView;

    new-instance v1, LX/Azl;

    invoke-direct {v1, p0}, LX/Azl;-><init>(Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;)V

    .line 1732930
    iput-object v1, v0, Lcom/facebook/backstage/camera/CameraView;->x:LX/ALU;

    .line 1732931
    return-void
.end method

.method public setSelfie(Z)V
    .locals 1

    .prologue
    .line 1732926
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsCameraFlowView;->b:Lcom/facebook/backstage/camera/CameraView;

    invoke-virtual {v0, p1}, Lcom/facebook/backstage/camera/CameraView;->setSelfie(Z)V

    .line 1732927
    return-void
.end method
