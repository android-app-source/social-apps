.class public Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;
.super Landroid/widget/LinearLayout;
.source ""


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field public b:LX/0i4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Lcom/facebook/fig/button/FigButton;

.field public d:LX/B05;

.field private e:LX/0i5;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1733285
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1733283
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733284
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733281
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733282
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1733267
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1733268
    const-class v0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;

    invoke-static {v0, p0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1733269
    invoke-direct {p0, p1}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->a(Landroid/content/Context;)V

    .line 1733270
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1733275
    const-class v0, Landroid/app/Activity;

    invoke-static {p1, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1733276
    iget-object v1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->b:LX/0i4;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v1, v0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->e:LX/0i5;

    .line 1733277
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307c3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 1733278
    const v0, 0x7f0d1497

    invoke-virtual {p0, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->c:Lcom/facebook/fig/button/FigButton;

    .line 1733279
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->c:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/B03;

    invoke-direct {v1, p0}, LX/B03;-><init>(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1733280
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;

    const-class v1, LX/0i4;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/0i4;

    iput-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->b:LX/0i4;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;)V
    .locals 3

    .prologue
    .line 1733273
    iget-object v0, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->e:LX/0i5;

    sget-object v1, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->a:[Ljava/lang/String;

    new-instance v2, LX/B04;

    invoke-direct {v2, p0}, LX/B04;-><init>(Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;)V

    invoke-virtual {v0, v1, v2}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    .line 1733274
    return-void
.end method


# virtual methods
.method public setPermissionsListener(LX/B05;)V
    .locals 0

    .prologue
    .line 1733271
    iput-object p1, p0, Lcom/facebook/goodfriends/camera/GoodFriendsGalleryRequestPermissionsView;->d:LX/B05;

    .line 1733272
    return-void
.end method
