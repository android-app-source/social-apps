.class public abstract Lcom/facebook/components/list/fb/fragment/ListComponentFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field private a:Lcom/facebook/components/ComponentView;

.field public b:LX/1dV;

.field public c:LX/BcP;

.field public d:LX/BcO;

.field public e:LX/1De;

.field public f:LX/BdI;

.field public g:LX/195;

.field public h:LX/193;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:Lcom/facebook/quicklog/QuickPerformanceLogger;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:LX/5K7;

.field public k:LX/Bdb;

.field public final l:Ljava/lang/Runnable;

.field public final m:LX/BdJ;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1803547
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1803548
    new-instance v0, LX/5K7;

    invoke-direct {v0}, LX/5K7;-><init>()V

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->j:LX/5K7;

    .line 1803549
    new-instance v0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment$1;

    invoke-direct {v0, p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment$1;-><init>(Lcom/facebook/components/list/fb/fragment/ListComponentFragment;)V

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->l:Ljava/lang/Runnable;

    .line 1803550
    new-instance v0, LX/BdJ;

    invoke-direct {v0, p0}, LX/BdJ;-><init>(Lcom/facebook/components/list/fb/fragment/ListComponentFragment;)V

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->m:LX/BdJ;

    .line 1803551
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

    const-class v1, LX/193;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/193;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object p0

    check-cast p0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    iput-object v1, p1, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->h:LX/193;

    iput-object p0, p1, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    return-void
.end method

.method public static l(Lcom/facebook/components/list/fb/fragment/ListComponentFragment;)V
    .locals 5

    .prologue
    .line 1803602
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a:Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_0

    .line 1803603
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a:Lcom/facebook/components/ComponentView;

    invoke-virtual {v0}, Lcom/facebook/components/ComponentView;->h()V

    .line 1803604
    :cond_0
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->b:LX/1dV;

    if-eqz v0, :cond_1

    .line 1803605
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->b:LX/1dV;

    invoke-virtual {v0}, LX/1dV;->j()V

    .line 1803606
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->b:LX/1dV;

    .line 1803607
    :cond_1
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->b:LX/1dV;

    if-nez v0, :cond_3

    .line 1803608
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->c:LX/BcP;

    .line 1803609
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v1}, LX/BdI;->d()I

    move-result v1

    if-eqz v1, :cond_4

    .line 1803610
    new-instance v1, LX/BdU;

    invoke-direct {v1}, LX/BdU;-><init>()V

    .line 1803611
    sget-object v2, LX/BdW;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BdT;

    .line 1803612
    if-nez v2, :cond_2

    .line 1803613
    new-instance v2, LX/BdT;

    invoke-direct {v2}, LX/BdT;-><init>()V

    .line 1803614
    :cond_2
    iput-object v1, v2, LX/BcN;->a:LX/BcO;

    .line 1803615
    iput-object v1, v2, LX/BdT;->a:LX/BdU;

    .line 1803616
    iget-object v3, v2, LX/BdT;->d:Ljava/util/BitSet;

    invoke-virtual {v3}, Ljava/util/BitSet;->clear()V

    .line 1803617
    move-object v1, v2

    .line 1803618
    move-object v1, v1

    .line 1803619
    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1803620
    iget-object v3, v1, LX/BdT;->a:LX/BdU;

    iput-object v2, v3, LX/BdU;->d:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 1803621
    iget-object v3, v1, LX/BdT;->d:Ljava/util/BitSet;

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljava/util/BitSet;->set(I)V

    .line 1803622
    move-object v1, v1

    .line 1803623
    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v2}, LX/BdI;->d()I

    move-result v2

    .line 1803624
    iget-object v3, v1, LX/BdT;->a:LX/BdU;

    iput v2, v3, LX/BdU;->c:I

    .line 1803625
    iget-object v3, v1, LX/BdT;->d:Ljava/util/BitSet;

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/util/BitSet;->set(I)V

    .line 1803626
    move-object v1, v1

    .line 1803627
    iget-object v2, v1, LX/BdT;->a:LX/BdU;

    iput-object p0, v2, LX/BdU;->b:Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

    .line 1803628
    iget-object v2, v1, LX/BdT;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1803629
    move-object v1, v1

    .line 1803630
    invoke-virtual {v1}, LX/BdT;->b()LX/BcO;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 1803631
    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->d:LX/BcO;

    .line 1803632
    invoke-virtual {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->c()LX/1OX;

    move-result-object v0

    .line 1803633
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->m:LX/BdJ;

    .line 1803634
    iput-object v0, v1, LX/BdJ;->b:LX/1OX;

    .line 1803635
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->c:LX/BcP;

    invoke-static {v0}, LX/Bdq;->c(LX/1De;)LX/Bdl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->j:LX/5K7;

    invoke-virtual {v0, v1}, LX/Bdl;->a(LX/5K7;)LX/Bdl;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(LX/Bdl;)LX/Bdl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->m:LX/BdJ;

    .line 1803636
    iget-object v2, v0, LX/Bdl;->a:LX/Bdm;

    iput-object v1, v2, LX/Bdm;->f:LX/1OX;

    .line 1803637
    move-object v0, v0

    .line 1803638
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->k:LX/Bdb;

    invoke-virtual {v0, v1}, LX/Bdl;->a(LX/Bdb;)LX/Bdl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->e:LX/1De;

    invoke-virtual {p0, v1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(LX/1De;)LX/1X1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Bdl;->a(LX/1X1;)LX/Bdl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->e:LX/1De;

    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->l:Ljava/lang/Runnable;

    invoke-virtual {p0, v1, v2}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(LX/1De;Ljava/lang/Runnable;)LX/1X1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Bdl;->b(LX/1X1;)LX/Bdl;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->e:LX/1De;

    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->l:Ljava/lang/Runnable;

    .line 1803639
    invoke-static {v1}, LX/BdN;->c(LX/1De;)LX/BdL;

    move-result-object v3

    const v4, 0x7f080039

    invoke-virtual {v3, v4}, LX/BdL;->h(I)LX/BdL;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/BdL;->a(Ljava/lang/Runnable;)LX/BdL;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->d()LX/1X1;

    move-result-object v3

    move-object v1, v3

    .line 1803640
    iget-object v2, v0, LX/Bdl;->a:LX/Bdm;

    iput-object v1, v2, LX/Bdm;->e:LX/1X1;

    .line 1803641
    move-object v0, v0

    .line 1803642
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->d:LX/BcO;

    invoke-virtual {v0, v1}, LX/Bdl;->a(LX/BcO;)LX/Bdl;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->d()LX/BcG;

    move-result-object v1

    .line 1803643
    iget-object v2, v0, LX/Bdl;->a:LX/Bdm;

    iput-object v1, v2, LX/Bdm;->g:LX/BcG;

    .line 1803644
    move-object v0, v0

    .line 1803645
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1803646
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->c:LX/BcP;

    invoke-static {v1, v0}, LX/1cy;->a(LX/1De;LX/1X1;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->b:LX/1dV;

    .line 1803647
    :cond_3
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a:Lcom/facebook/components/ComponentView;

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->b:LX/1dV;

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1803648
    return-void

    :cond_4
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(LX/BcP;LX/BcQ;)LX/BcO;

    move-result-object v1

    goto/16 :goto_0
.end method


# virtual methods
.method public a(LX/1De;)LX/1X1;
    .locals 1

    .prologue
    .line 1803601
    invoke-static {p1}, LX/BdR;->c(LX/1De;)LX/BdP;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public a(LX/1De;Ljava/lang/Runnable;)LX/1X1;
    .locals 2

    .prologue
    .line 1803600
    invoke-static {p1}, LX/BdN;->c(LX/1De;)LX/BdL;

    move-result-object v0

    const v1, 0x7f082b11

    invoke-virtual {v0, v1}, LX/BdL;->h(I)LX/BdL;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/BdL;->a(Ljava/lang/Runnable;)LX/BdL;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(LX/BcP;LX/BcQ;)LX/BcO;
.end method

.method public a(LX/Bdl;)LX/Bdl;
    .locals 1

    .prologue
    .line 1803596
    const/4 v0, 0x0

    .line 1803597
    iget-object p0, p1, LX/Bdl;->a:LX/Bdm;

    iput-object v0, p0, LX/Bdm;->l:LX/1Of;

    .line 1803598
    move-object v0, p1

    .line 1803599
    return-object v0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1803583
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1803584
    const-class v0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;

    invoke-static {v0, p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1803585
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    if-nez v0, :cond_0

    .line 1803586
    invoke-virtual {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->e()LX/BdI;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    .line 1803587
    :cond_0
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v0}, LX/BdI;->a()LX/Bdb;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->k:LX/Bdb;

    .line 1803588
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->g:LX/195;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v0}, LX/BdI;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1803589
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->h:LX/193;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v2}, LX/BdI;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->g:LX/195;

    .line 1803590
    :cond_1
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v0}, LX/BdI;->d()I

    move-result v0

    if-eqz v0, :cond_2

    .line 1803591
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v1}, LX/BdI;->d()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 1803592
    iget-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v1}, LX/BdI;->d()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v2}, LX/BdI;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ILjava/lang/String;)V

    .line 1803593
    :cond_2
    new-instance v0, LX/BcP;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/BcP;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->c:LX/BcP;

    .line 1803594
    new-instance v0, LX/1De;

    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->c:LX/BcP;

    invoke-direct {v0, v1}, LX/1De;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->e:LX/1De;

    .line 1803595
    return-void
.end method

.method public c()LX/1OX;
    .locals 1

    .prologue
    .line 1803582
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract d()LX/BcG;
.end method

.method public e()LX/BdI;
    .locals 1

    .prologue
    .line 1803581
    new-instance v0, LX/BdK;

    invoke-direct {v0}, LX/BdK;-><init>()V

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x6c58803a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1803574
    if-nez p2, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    .line 1803575
    :goto_0
    new-instance v1, Lcom/facebook/components/ComponentView;

    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->e:LX/1De;

    invoke-direct {v1, v2}, Lcom/facebook/components/ComponentView;-><init>(LX/1De;)V

    iput-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a:Lcom/facebook/components/ComponentView;

    .line 1803576
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a:Lcom/facebook/components/ComponentView;

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Lcom/facebook/components/ComponentView;->setBackgroundResource(I)V

    .line 1803577
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a:Lcom/facebook/components/ComponentView;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Lcom/facebook/components/ComponentView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1803578
    invoke-static {p0}, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->l(Lcom/facebook/components/list/fb/fragment/ListComponentFragment;)V

    .line 1803579
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a:Lcom/facebook/components/ComponentView;

    const v2, 0x20f79de6

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-object v1

    .line 1803580
    :cond_0
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, -0x3bac39ac

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1803559
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1803560
    iput-object v3, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->j:LX/5K7;

    .line 1803561
    iput-object v3, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->k:LX/Bdb;

    .line 1803562
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->b:LX/1dV;

    invoke-virtual {v1}, LX/1dV;->j()V

    .line 1803563
    iput-object v3, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->b:LX/1dV;

    .line 1803564
    iput-object v3, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->c:LX/BcP;

    .line 1803565
    iput-object v3, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->e:LX/1De;

    .line 1803566
    iput-object v3, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->d:LX/BcO;

    .line 1803567
    iput-object v3, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->g:LX/195;

    .line 1803568
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v1}, LX/BdI;->d()I

    move-result v1

    if-eqz v1, :cond_0

    .line 1803569
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v2}, LX/BdI;->d()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 1803570
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->i:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v2, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    invoke-interface {v2}, LX/BdI;->d()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(I)V

    .line 1803571
    :cond_0
    iput-object v3, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->f:LX/BdI;

    .line 1803572
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1803573
    const/16 v1, 0x2b

    const v2, -0x48691389

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x499706b9

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1803556
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->a:Lcom/facebook/components/ComponentView;

    .line 1803557
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1803558
    const/16 v1, 0x2b

    const v2, 0x7e7c80be

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x5dbb63df

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1803552
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1803553
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->g:LX/195;

    if-eqz v1, :cond_0

    .line 1803554
    iget-object v1, p0, Lcom/facebook/components/list/fb/fragment/ListComponentFragment;->g:LX/195;

    invoke-virtual {v1}, LX/195;->b()V

    .line 1803555
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x3c6928dd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
