.class public final Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;
.super Ljava/lang/Thread;
.source ""


# instance fields
.field public final synthetic a:LX/Bcb;


# direct methods
.method public constructor <init>(LX/Bcb;)V
    .locals 0

    .prologue
    .line 1802347
    iput-object p1, p0, Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;->a:LX/Bcb;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1802348
    invoke-super {p0}, Ljava/lang/Thread;->run()V

    .line 1802349
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;->a:LX/Bcb;

    iget-object v0, p0, Lcom/facebook/components/list/ServiceRegistry$ReferenceQueueThread;->a:LX/Bcb;

    iget-object v0, v0, LX/Bcb;->b:Ljava/lang/ref/ReferenceQueue;

    invoke-virtual {v0}, Ljava/lang/ref/ReferenceQueue;->remove()Ljava/lang/ref/Reference;

    move-result-object v0

    check-cast v0, LX/Bca;

    .line 1802350
    iget-object v2, v1, LX/Bcb;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1802351
    iget-object v2, v1, LX/Bcb;->c:Ljava/util/HashMap;

    iget-object v3, v0, LX/Bca;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    .line 1802352
    if-nez v2, :cond_0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1802353
    :goto_1
    goto :goto_0

    .line 1802354
    :catch_0
    goto :goto_0

    .line 1802355
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_1

    .line 1802356
    iget-object v2, v0, LX/Bca;->a:LX/BcS;

    iget-object v3, v0, LX/Bca;->c:LX/BcP;

    iget-object v4, v0, LX/Bca;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, LX/BcS;->a(LX/BcP;Ljava/lang/Object;)V

    .line 1802357
    iget-object v2, v1, LX/Bcb;->c:Ljava/util/HashMap;

    iget-object v3, v0, LX/Bca;->b:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 1802358
    :cond_1
    iget-object v3, v1, LX/Bcb;->c:Ljava/util/HashMap;

    iget-object v4, v0, LX/Bca;->b:Ljava/lang/Object;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method
