.class public final Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Z

.field public final synthetic d:[B

.field public final synthetic e:LX/2Pc;


# direct methods
.method public constructor <init>(LX/2Pc;Ljava/lang/String;Ljava/lang/String;Z[B)V
    .locals 0

    .prologue
    .line 1894431
    iput-object p1, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->e:LX/2Pc;

    iput-object p2, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->b:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->c:Z

    iput-object p5, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->d:[B

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1894432
    iget-object v0, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->e:LX/2Pc;

    iget-object v0, v0, LX/2Pc;->b:LX/2Pd;

    iget-object v1, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->b:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->c:Z

    iget-object v4, p0, Lcom/facebook/offlinemode/boostedcomponent/OfflineLWIMutationRecord$1;->d:[B

    .line 1894433
    iget-object v5, v0, LX/2Pd;->b:LX/0Sh;

    invoke-virtual {v5}, LX/0Sh;->b()V

    .line 1894434
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 1894435
    sget-object v5, LX/2Ph;->a:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1894436
    sget-object v5, LX/2Ph;->b:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1894437
    sget-object v5, LX/2Ph;->c:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v7

    if-eqz v3, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1894438
    sget-object v5, LX/2Ph;->d:LX/0U1;

    invoke-virtual {v5}, LX/0U1;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1894439
    iget-object v5, v0, LX/2Pd;->a:LX/2Pe;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v7, "offline_lwi_table"

    const-string v8, ""

    const p0, 0x497f8643

    invoke-static {p0}, LX/03h;->a(I)V

    invoke-virtual {v5, v7, v8, v6}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v5, 0x51e7e946

    invoke-static {v5}, LX/03h;->a(I)V

    .line 1894440
    return-void

    .line 1894441
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method
