.class public Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;
.super Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;
.source ""


# annotations
.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public volatile r:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B4O;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B35;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1740380
    invoke-direct {p0}, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;",
            "LX/0Or",
            "<",
            "LX/B4O;",
            ">;",
            "LX/0Or",
            "<",
            "LX/B35;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1740393
    iput-object p1, p0, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;->r:LX/0Or;

    iput-object p2, p0, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;->s:LX/0Or;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;

    const/16 v1, 0x24ec

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x24de

    invoke-static {v0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;->a(Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;LX/0Or;LX/0Or;)V

    return-void
.end method


# virtual methods
.method public final a()LX/B3T;
    .locals 1

    .prologue
    .line 1740392
    new-instance v0, LX/B3U;

    invoke-direct {v0, p0}, LX/B3U;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;)V

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1740389
    invoke-static {p0, p0}, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1740390
    invoke-super {p0, p1}, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->b(Landroid/os/Bundle;)V

    .line 1740391
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1740386
    const/16 v0, 0x7b

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1740387
    invoke-virtual {p0}, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;->finish()V

    .line 1740388
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1740381
    invoke-super {p0}, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->onBackPressed()V

    .line 1740382
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayPivotActivity;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B4O;

    .line 1740383
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->p:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a()Ljava/lang/String;

    move-result-object v1

    .line 1740384
    const-string p0, "heisman_cancel_pivot"

    invoke-static {v0, p0, v1}, LX/B4O;->g(LX/B4O;Ljava/lang/String;Ljava/lang/String;)V

    .line 1740385
    return-void
.end method
