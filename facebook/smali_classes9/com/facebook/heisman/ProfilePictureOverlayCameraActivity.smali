.class public Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation

.annotation build Lcom/facebook/crudolib/urimap/UriMatchPatterns;
.end annotation


# instance fields
.field public A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

.field public B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private C:LX/03R;

.field public D:Z

.field private E:LX/B4P;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public F:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private G:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private H:LX/B33;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private I:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private J:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/B3O;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/B3I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/B3K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/1b0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/B4H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/2ua;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final y:LX/89S;

.field private final z:LX/B37;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1739711
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1739712
    new-instance v0, LX/B36;

    invoke-direct {v0, p0}, LX/B36;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->y:LX/89S;

    .line 1739713
    new-instance v0, LX/B38;

    invoke-direct {v0, p0}, LX/B38;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->z:LX/B37;

    .line 1739714
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->C:LX/03R;

    .line 1739715
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->D:Z

    return-void
.end method

.method private static a(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;LX/B3O;LX/B3I;LX/B3K;LX/1b0;LX/0ad;Ljava/util/concurrent/Executor;Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;LX/B4H;LX/2ua;)V
    .locals 0

    .prologue
    .line 1739716
    iput-object p1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p:LX/B3O;

    iput-object p2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->q:LX/B3I;

    iput-object p3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->r:LX/B3K;

    iput-object p4, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->s:LX/1b0;

    iput-object p5, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->t:LX/0ad;

    iput-object p6, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->u:Ljava/util/concurrent/Executor;

    iput-object p7, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->v:Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    iput-object p8, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->w:LX/B4H;

    iput-object p9, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->x:LX/2ua;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v9

    move-object v0, p0

    check-cast v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;

    invoke-static {v9}, LX/B3O;->a(LX/0QB;)LX/B3O;

    move-result-object v1

    check-cast v1, LX/B3O;

    const-class v2, LX/B3I;

    invoke-interface {v9, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/B3I;

    invoke-static {v9}, LX/B3K;->b(LX/0QB;)LX/B3K;

    move-result-object v3

    check-cast v3, LX/B3K;

    invoke-static {v9}, LX/1az;->a(LX/0QB;)LX/1az;

    move-result-object v4

    check-cast v4, LX/1b0;

    invoke-static {v9}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v9}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/Executor;

    invoke-static {v9}, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->a(LX/0QB;)Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    move-result-object v7

    check-cast v7, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    invoke-static {v9}, LX/B4H;->a(LX/0QB;)LX/B4H;

    move-result-object v8

    check-cast v8, LX/B4H;

    invoke-static {v9}, LX/2ua;->b(LX/0QB;)LX/2ua;

    move-result-object v9

    check-cast v9, LX/2ua;

    invoke-static/range {v0 .. v9}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;LX/B3O;LX/B3I;LX/B3K;LX/1b0;LX/0ad;Ljava/util/concurrent/Executor;Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;LX/B4H;LX/2ua;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;Lcom/facebook/graphql/executor/GraphQLResult;Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;",
            ">;",
            "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1739717
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->s()Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1739718
    :goto_0
    return-void

    .line 1739719
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739720
    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1739721
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739722
    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;->j()Ljava/lang/String;

    move-result-object v2

    .line 1739723
    :goto_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1739724
    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1739725
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->w:LX/B4H;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v0}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->d()J

    move-result-wide v6

    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p()Z

    move-result v8

    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p()Z

    move-result v9

    move-object v5, p2

    invoke-virtual/range {v1 .. v9}, LX/B4H;->a(Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;JZZ)Landroid/content/Intent;

    move-result-object v0

    .line 1739726
    const-string v1, "heisman_camera_intent_data"

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1739727
    invoke-static {v0}, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->a(Landroid/content/Intent;)Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    move-result-object v0

    .line 1739728
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    const-string v3, "heisman_default_picture_staging_ground_fragment"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    goto :goto_0

    :cond_1
    move-object v2, v4

    .line 1739729
    goto :goto_1
.end method

.method private static d(Landroid/os/Bundle;)Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;
    .locals 1
    .param p0    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1739730
    if-nez p0, :cond_0

    .line 1739731
    const/4 v0, 0x0

    .line 1739732
    :goto_0
    return-object v0

    .line 1739733
    :cond_0
    const-string v0, "heisman_saved_camera_model"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    goto :goto_0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1739734
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a00d5

    move v1, v0

    .line 1739735
    :goto_0
    const v0, 0x7f0d2769

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1739736
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1739737
    return-void

    .line 1739738
    :cond_0
    const v0, 0x7f0a00d6

    move v1, v0

    goto :goto_0
.end method

.method private m()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1739590
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1739591
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->g()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1739739
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1739740
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739741
    iput-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->G:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1739742
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->v:Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    .line 1739743
    iget-object v2, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v1, v2

    .line 1739744
    const-string v2, "ProfileOverlayDefaultPicture"

    invoke-static {v2}, LX/2U5;->a(Ljava/lang/String;)LX/0v6;

    move-result-object v3

    .line 1739745
    iget-object v2, v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1739746
    invoke-static {v2}, LX/B3W;->b(Ljava/lang/String;)LX/B4o;

    move-result-object v2

    .line 1739747
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "ProfileImageRequest"

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    .line 1739748
    iput-object v4, v2, LX/0zO;->d:Ljava/util/Set;

    .line 1739749
    move-object v2, v2

    .line 1739750
    sget-object v4, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v2, v4}, LX/B32;->a(LX/0zO;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1739751
    invoke-virtual {v3, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1739752
    iget-object v2, v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;

    .line 1739753
    invoke-virtual {v2, v1, v3}, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->a(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;LX/0v6;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1739754
    iget-object v5, v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->f:LX/0tX;

    invoke-virtual {v5, v3}, LX/0tX;->a(LX/0v6;)V

    .line 1739755
    new-instance v3, LX/B33;

    invoke-direct {v3, v4, v2}, LX/B33;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    move-object v0, v3

    .line 1739756
    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->H:LX/B33;

    .line 1739757
    :goto_0
    return-void

    .line 1739758
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    .line 1739759
    iget-object v1, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c:Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    move-object v0, v1

    .line 1739760
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-virtual {v1, v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a(Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->G:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1739761
    iput-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->H:LX/B33;

    goto :goto_0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 1739762
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739763
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->H:LX/B33;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1739764
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->H:LX/B33;

    iget-object v2, v2, LX/B33;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->H:LX/B33;

    iget-object v2, v2, LX/B33;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1739765
    new-instance v1, LX/B39;

    invoke-direct {v1, p0}, LX/B39;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V

    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->J:LX/0Vd;

    .line 1739766
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->J:LX/0Vd;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->u:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1739767
    :goto_0
    return-void

    .line 1739768
    :cond_0
    new-instance v0, LX/B3A;

    invoke-direct {v0, p0}, LX/B3A;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->I:LX/0Vd;

    .line 1739769
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->G:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->I:LX/0Vd;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->u:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method private p()Z
    .locals 3

    .prologue
    .line 1739770
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->C:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 1739771
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->t:LX/0ad;

    sget-short v1, LX/B3X;->b:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->C:LX/03R;

    .line 1739772
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->C:LX/03R;

    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    return v0
.end method

.method public static q(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1739668
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->t()Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1739669
    :goto_0
    return-void

    .line 1739670
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    .line 1739671
    iget-object v3, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v0, v3

    .line 1739672
    invoke-static {v0}, LX/5Qm;->b(LX/5QV;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1739673
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f()Ljava/lang/String;

    move-result-object v3

    .line 1739674
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->g()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1739675
    invoke-static {v3, v0}, LX/B5P;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v0

    .line 1739676
    new-instance v4, LX/5ii;

    invoke-direct {v4}, LX/5ii;-><init>()V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1739677
    iput-object v0, v4, LX/5ii;->b:LX/0Px;

    .line 1739678
    move-object v0, v4

    .line 1739679
    invoke-virtual {v0}, LX/5ii;->a()Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FramePackModel;

    move-result-object v4

    .line 1739680
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->x:LX/2ua;

    invoke-virtual {v0}, LX/2ua;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->t:LX/0ad;

    sget-short v5, LX/B3X;->e:S

    invoke-interface {v0, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1739681
    :goto_1
    if-eqz v0, :cond_2

    sget-object v0, LX/4gI;->ALL:LX/4gI;

    .line 1739682
    :goto_2
    iget-object v5, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->t:LX/0ad;

    sget v6, LX/0wf;->aN:I

    const/4 v7, 0x6

    invoke-interface {v5, v6, v7}, LX/0ad;->a(II)I

    move-result v5

    .line 1739683
    new-instance v6, LX/89V;

    invoke-direct {v6}, LX/89V;-><init>()V

    .line 1739684
    iput-boolean v1, v6, LX/89V;->b:Z

    .line 1739685
    move-object v6, v6

    .line 1739686
    iput-boolean v1, v6, LX/89V;->c:Z

    .line 1739687
    move-object v6, v6

    .line 1739688
    iput-boolean v1, v6, LX/89V;->d:Z

    .line 1739689
    move-object v6, v6

    .line 1739690
    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 1739691
    iput-object v4, v6, LX/89V;->h:LX/0Px;

    .line 1739692
    move-object v4, v6

    .line 1739693
    iput-object v3, v4, LX/89V;->i:Ljava/lang/String;

    .line 1739694
    move-object v3, v4

    .line 1739695
    mul-int/lit16 v4, v5, 0x3e8

    .line 1739696
    iput v4, v3, LX/89V;->j:I

    .line 1739697
    move-object v3, v3

    .line 1739698
    iput-object v0, v3, LX/89V;->f:LX/4gI;

    .line 1739699
    move-object v3, v3

    .line 1739700
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v0}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->h()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    .line 1739701
    :goto_3
    iput v0, v3, LX/89V;->g:I

    .line 1739702
    move-object v0, v3

    .line 1739703
    invoke-virtual {v0}, LX/89V;->a()Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    move-result-object v0

    .line 1739704
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->s:LX/1b0;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v2}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v3}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->g()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v3

    invoke-interface {v1, v0, v2, v3}, LX/1b0;->a(Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    move-result-object v0

    .line 1739705
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d002f

    .line 1739706
    move-object v0, v0

    .line 1739707
    const-string v3, "heisman_creative_cam_fragment"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->c()I

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 1739708
    goto :goto_1

    .line 1739709
    :cond_2
    sget-object v0, LX/4gI;->PHOTO_ONLY:LX/4gI;

    goto :goto_2

    :cond_3
    move v0, v2

    .line 1739710
    goto :goto_3
.end method

.method public static r(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)LX/B4P;
    .locals 1

    .prologue
    .line 1739773
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->E:LX/B4P;

    if-nez v0, :cond_0

    .line 1739774
    new-instance v0, LX/B4P;

    invoke-direct {v0}, LX/B4P;-><init>()V

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->E:LX/B4P;

    .line 1739775
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->E:LX/B4P;

    invoke-virtual {v0}, LX/B4P;->a()V

    .line 1739776
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->E:LX/B4P;

    return-object v0
.end method

.method private s()Lcom/facebook/timeline/stagingground/StagingGroundFragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1739663
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "heisman_default_picture_staging_ground_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1739664
    if-eqz v0, :cond_0

    .line 1739665
    instance-of v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1739666
    check-cast v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    .line 1739667
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1739658
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "heisman_creative_cam_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 1739659
    if-eqz v0, :cond_0

    .line 1739660
    instance-of v1, v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 1739661
    check-cast v0, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    .line 1739662
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1739657
    const-string v0, "timeline"

    return-object v0
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1739649
    instance-of v0, p1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    if-eqz v0, :cond_1

    .line 1739650
    check-cast p1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->y:LX/89S;

    .line 1739651
    iput-object v0, p1, Lcom/facebook/photos/creativecam/activity/CreativeEditingCameraFragment;->O:LX/89S;

    .line 1739652
    :cond_0
    :goto_0
    return-void

    .line 1739653
    :cond_1
    instance-of v0, p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    if-eqz v0, :cond_0

    .line 1739654
    check-cast p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->z:LX/B37;

    .line 1739655
    iput-object v0, p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->f:LX/B37;

    .line 1739656
    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1739619
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1739620
    invoke-static {p0, p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1739621
    invoke-virtual {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1739622
    const-string v1, "heisman_camera_intent_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1739623
    const-string v1, "heisman_camera_intent_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    .line 1739624
    :goto_0
    move-object v0, v1

    .line 1739625
    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    .line 1739626
    invoke-static {p1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->d(Landroid/os/Bundle;)Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    .line 1739627
    if-nez v0, :cond_0

    .line 1739628
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    .line 1739629
    iget-object v1, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->b:LX/5QV;

    move-object v0, v1

    .line 1739630
    invoke-static {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->a(LX/5QV;)Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v0

    .line 1739631
    new-instance v1, LX/B3N;

    invoke-direct {v1, v0}, LX/B3N;-><init>(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)V

    invoke-virtual {v1}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    move-object v0, v1

    .line 1739632
    :cond_0
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p:LX/B3O;

    invoke-virtual {v1, v0}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1739633
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->q:LX/B3I;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v0, p0, v1}, LX/B3I;->a(Landroid/app/Activity;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    .line 1739634
    const v0, 0x7f03107e

    invoke-virtual {p0, v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->setContentView(I)V

    .line 1739635
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->l()V

    .line 1739636
    const v0, 0x7f0d276a

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1739637
    new-instance v1, LX/0zw;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->F:LX/0zw;

    .line 1739638
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1739639
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->F:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1739640
    :cond_1
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->n()V

    .line 1739641
    return-void

    .line 1739642
    :cond_2
    const-string v1, "entry_point"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1739643
    const-string v2, "frame_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1739644
    new-instance v3, LX/5QX;

    invoke-direct {v3}, LX/5QX;-><init>()V

    .line 1739645
    iput-object v2, v3, LX/5QX;->b:Ljava/lang/String;

    .line 1739646
    move-object v2, v3

    .line 1739647
    invoke-virtual {v2}, LX/5QX;->a()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-result-object v2

    .line 1739648
    new-instance v3, LX/B4K;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v4, v1}, LX/B4K;-><init>(LX/5QV;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, LX/B4K;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    move-result-object v1

    goto/16 :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    .prologue
    .line 1739616
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1739617
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->r:LX/B3K;

    iget-object v4, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->A:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-static {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->r(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)LX/B4P;

    move-result-object v5

    move v1, p1

    move v2, p2

    move-object v3, p3

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, LX/B3K;->a(IILandroid/content/Intent;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;LX/B4P;Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;)V

    .line 1739618
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1739612
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    if-eqz v0, :cond_0

    .line 1739613
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->b()V

    .line 1739614
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1739615
    return-void
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x22

    const v2, 0x56379a02

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1739603
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1739604
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    if-eqz v1, :cond_0

    .line 1739605
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    invoke-virtual {v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->c()V

    .line 1739606
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->B:Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    .line 1739607
    :cond_0
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->r:LX/B3K;

    if-eqz v1, :cond_1

    .line 1739608
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->r:LX/B3K;

    invoke-virtual {v1}, LX/B3K;->a()V

    .line 1739609
    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->D:Z

    .line 1739610
    const v1, 0x218b51a6

    invoke-static {v1, v0}, LX/02F;->c(II)V

    return-void

    .line 1739611
    :cond_1
    const-string v1, "ProfilePictureOverlayCameraActivity"

    const-string v2, "onDestroy() called before onActivityCreate(), resulting in a null dependency"

    invoke-static {v1, v2}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, -0x2771c9ea

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1739597
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1739598
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->I:LX/0Vd;

    if-eqz v1, :cond_0

    .line 1739599
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->I:LX/0Vd;

    invoke-virtual {v1}, LX/0Vd;->dispose()V

    .line 1739600
    :cond_0
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->J:LX/0Vd;

    if-eqz v1, :cond_1

    .line 1739601
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->J:LX/0Vd;

    invoke-virtual {v1}, LX/0Vd;->dispose()V

    .line 1739602
    :cond_1
    const/16 v1, 0x23

    const v2, -0x51f9bc07

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x16be1926

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1739594
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1739595
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->o()V

    .line 1739596
    const/16 v1, 0x23

    const v2, -0x20f40f77

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1739592
    const-string v0, "heisman_saved_camera_model"

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivity;->p:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1739593
    return-void
.end method
