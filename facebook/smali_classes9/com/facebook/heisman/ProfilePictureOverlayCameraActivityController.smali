.class public Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/89c;
.implements LX/89f;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public volatile a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/app/Activity;

.field public e:Landroid/support/v7/widget/Toolbar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

.field public g:LX/B3O;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:LX/B35;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/B3R;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private j:Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private k:LX/B4H;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public l:LX/B4O;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private m:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private p:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private q:LX/74n;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private r:LX/B5y;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/B4D;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private t:LX/B3L;

.field public u:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:LX/1Mv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Mv",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1740098
    const-class v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;)V
    .locals 1
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740090
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740091
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1740092
    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->n:LX/0Ot;

    .line 1740093
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1740094
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->z:I

    .line 1740095
    iput-object p1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    .line 1740096
    iput-object p2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    .line 1740097
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1740087
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    .line 1740088
    iget-object p0, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v0, p0

    .line 1740089
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public static a(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;LX/0Or;LX/0Or;LX/B3O;LX/B35;LX/B3R;Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;LX/B4H;LX/B4O;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0TD;Ljava/util/concurrent/Executor;LX/74n;LX/B5y;LX/B4D;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;",
            "LX/B3O;",
            "LX/B35;",
            "LX/B3R;",
            "Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;",
            "LX/B4H;",
            "LX/B4O;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0TD;",
            "Ljava/util/concurrent/Executor;",
            "LX/74n;",
            "LX/B5y;",
            "LX/B4D;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1740086
    iput-object p1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    iput-object p4, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->h:LX/B35;

    iput-object p5, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->i:LX/B3R;

    iput-object p6, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->j:Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    iput-object p7, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->k:LX/B4H;

    iput-object p8, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->l:LX/B4O;

    iput-object p9, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->m:Lcom/facebook/content/SecureContextHelper;

    iput-object p10, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->n:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->o:LX/0TD;

    iput-object p12, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->p:Ljava/util/concurrent/Executor;

    iput-object p13, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->q:LX/74n;

    iput-object p14, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->r:LX/B5y;

    iput-object p15, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->s:LX/B4D;

    return-void
.end method

.method public static b(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1740074
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1740075
    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const-string v0, "_data"

    aput-object v0, v2, v3

    const/4 v0, 0x2

    const-string v1, "datetaken"

    aput-object v1, v2, v0

    .line 1740076
    sget-object v0, LX/4gI;->VIDEO_ONLY:LX/4gI;

    invoke-static {v0}, LX/4gB;->a(LX/4gI;)Ljava/lang/String;

    move-result-object v3

    .line 1740077
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v5, "date_modified DESC LIMIT 1"

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1740078
    if-nez v2, :cond_1

    .line 1740079
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1740080
    :cond_0
    :goto_0
    return-object v4

    .line 1740081
    :cond_1
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1740082
    const/4 v0, 0x1

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v4

    .line 1740083
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1740084
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1740085
    :catchall_0
    move-exception v1

    move-object v4, v0

    move-object v0, v1

    :goto_1
    if-eqz v2, :cond_3

    if-eqz v4, :cond_4

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_3
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    invoke-static {v4, v1}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private b(Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;)V
    .locals 14
    .param p1    # Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1739894
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->j:Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    .line 1739895
    iget-object v2, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v1, v2

    .line 1739896
    const-string v2, "ProfilePictureOverlayCamera"

    invoke-static {v2}, LX/2U5;->a(Ljava/lang/String;)LX/0v6;

    move-result-object v3

    .line 1739897
    iget-object v2, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1739898
    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v4}, LX/B3W;->a(Ljava/lang/String;Ljava/lang/Integer;)LX/B4o;

    move-result-object v4

    move-object v2, v4

    .line 1739899
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "ProfileImageRequest"

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    .line 1739900
    iput-object v4, v2, LX/0zO;->d:Ljava/util/Set;

    .line 1739901
    move-object v2, v2

    .line 1739902
    sget-object v4, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v2, v4}, LX/B32;->a(LX/0zO;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1739903
    invoke-virtual {v3, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v4, v2

    .line 1739904
    iget-object v2, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;

    .line 1739905
    invoke-virtual {v2, v1, v3}, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->a(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;LX/0v6;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1739906
    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 1739907
    iget-object v6, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->a:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;

    .line 1739908
    invoke-virtual {v6, v5}, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->a(Ljava/lang/String;)LX/0zO;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v5, v6

    .line 1739909
    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object v6

    .line 1739910
    if-eqz p1, :cond_0

    .line 1739911
    new-instance v8, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-static {p1}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;->a(Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;)Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    move-result-object v9

    sget-object v10, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v12, 0x0

    invoke-direct {v8, v9, v10, v12, v13}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    invoke-static {v8}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    .line 1739912
    :goto_0
    move-object v6, v8

    .line 1739913
    iget-object v7, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->g:LX/0tX;

    invoke-virtual {v7, v3}, LX/0tX;->a(LX/0v6;)V

    .line 1739914
    new-instance v3, LX/B3L;

    invoke-direct {v3, v4, v2, v5, v6}, LX/B3L;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    move-object v0, v3

    .line 1739915
    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->t:LX/B3L;

    .line 1739916
    return-void

    .line 1739917
    :cond_0
    new-instance v8, LX/B4e;

    invoke-direct {v8}, LX/B4e;-><init>()V

    move-object v8, v8

    .line 1739918
    const-string v9, "image_overlay_id"

    invoke-virtual {v8, v9, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v8

    check-cast v8, LX/B4e;

    move-object v8, v8

    .line 1739919
    invoke-static {v8}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v8

    .line 1739920
    sget-object v9, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v8, v9}, LX/B32;->a(LX/0zO;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1739921
    invoke-virtual {v3, v8}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    goto :goto_0
.end method

.method private e()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1740069
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 1740070
    new-instance v1, LX/B3B;

    invoke-direct {v1, p0, v0}, LX/B3B;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;Lcom/google/common/util/concurrent/SettableFuture;)V

    .line 1740071
    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->t:LX/B3L;

    iget-object v2, v2, LX/B3L;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->p:Ljava/util/concurrent/Executor;

    invoke-static {v2, v1, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1740072
    new-instance v2, LX/1Mv;

    iget-object v3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->t:LX/B3L;

    iget-object v3, v3, LX/B3L;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {v2, v3, v1}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->v:LX/1Mv;

    .line 1740073
    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 1740065
    new-instance v0, LX/B3C;

    invoke-direct {v0, p0}, LX/B3C;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1740066
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->t:LX/B3L;

    iget-object v1, v1, LX/B3L;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->p:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1740067
    new-instance v1, LX/1Mv;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->t:LX/B3L;

    iget-object v2, v2, LX/B3L;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {v1, v2, v0}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->u:LX/1Mv;

    .line 1740068
    return-void
.end method

.method public static g(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V
    .locals 3

    .prologue
    .line 1740059
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->z:I

    if-eqz v0, :cond_1

    .line 1740060
    :cond_0
    :goto_0
    return-void

    .line 1740061
    :cond_1
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 1740062
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    .line 1740063
    :cond_2
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1740064
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method public static h(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V
    .locals 3

    .prologue
    .line 1740049
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->z:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 1740050
    :cond_0
    :goto_0
    return-void

    .line 1740051
    :cond_1
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    .line 1740052
    iget-object v1, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1740053
    if-nez v0, :cond_2

    .line 1740054
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0

    .line 1740055
    :cond_2
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1740056
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    .line 1740057
    iget-object v2, v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->c:Ljava/lang/String;

    move-object v1, v2

    .line 1740058
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 1740044
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->o:LX/0TD;

    new-instance v1, LX/B3D;

    invoke-direct {v1, p0}, LX/B3D;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1740045
    new-instance v1, LX/B3E;

    invoke-direct {v1, p0}, LX/B3E;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1740046
    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->p:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1740047
    new-instance v2, LX/1Mv;

    invoke-direct {v2, v0, v1}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->x:LX/1Mv;

    .line 1740048
    return-void
.end method

.method private j()V
    .locals 3

    .prologue
    .line 1740040
    new-instance v0, LX/B3G;

    invoke-direct {v0, p0}, LX/B3G;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1740041
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->t:LX/B3L;

    iget-object v1, v1, LX/B3L;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->p:Ljava/util/concurrent/Executor;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1740042
    new-instance v1, LX/1Mv;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->t:LX/B3L;

    iget-object v2, v2, LX/B3L;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-direct {v1, v2, v0}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->w:LX/1Mv;

    .line 1740043
    return-void
.end method

.method private k()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 1740023
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    .line 1740024
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    sget-object v2, LX/0ax;->bY:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 1740025
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1740026
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->k:LX/B4H;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    .line 1740027
    iget-object v3, v2, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    move-object v2, v3

    .line 1740028
    iget-object v3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v3}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->i()LX/5QV;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->d()J

    move-result-wide v4

    move v7, v6

    invoke-virtual/range {v1 .. v7}, LX/B4H;->a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;JZZ)Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    move-result-object v1

    .line 1740029
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1740030
    new-instance v0, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    sget-object v4, LX/5SD;->EDIT_PROFILE_PIC:LX/5SD;

    invoke-direct {v0, v4, v2, v3, v1}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;-><init>(LX/5SD;JLcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V

    move-object v0, v0

    .line 1740031
    const-string v1, "extra_photo_tab_mode_params"

    invoke-virtual {v8, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1740032
    const-string v0, "extra_disable_camera_cell"

    invoke-virtual {v8, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1740033
    const-string v0, "extra_simple_picker_launcher_configuration"

    sget-object v1, LX/8A9;->LAUNCH_PROFILE_PIC_EDIT_GALLERY:LX/8A9;

    invoke-static {v6, v6, v1}, LX/BQ2;->a(ZZLX/8A9;)Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1740034
    const-string v0, "extra_should_merge_camera_roll"

    invoke-virtual {v8, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1740035
    const-string v0, "disable_camera_roll"

    invoke-virtual {v8, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1740036
    const-string v0, "extra_photo_title_text"

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    const v2, 0x7f082741

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1740037
    const-string v0, "disable_adding_photos_to_albums"

    invoke-virtual {v8, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1740038
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->m:Lcom/facebook/content/SecureContextHelper;

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    invoke-interface {v0, v8, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1740039
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1740016
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->k:LX/B4H;

    .line 1740017
    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, LX/B4H;->c:Landroid/content/Context;

    const-class v3, Lcom/facebook/photos/simplepicker/launcher/SimplePickerLauncherActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1740018
    new-instance v2, LX/8AA;

    sget-object v3, LX/8AB;->PROFILEPIC:LX/8AB;

    invoke-direct {v2, v3}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v2}, LX/8AA;->k()LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->i()LX/8AA;

    move-result-object v2

    sget-object v3, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v2, v3}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v2

    invoke-virtual {v2}, LX/8AA;->w()Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v2

    .line 1740019
    const-string v3, "extra_simple_picker_launcher_settings"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1740020
    move-object v0, v1

    .line 1740021
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->m:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1740022
    return-void
.end method

.method public static m(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V
    .locals 3

    .prologue
    .line 1740014
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080039

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1740015
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1740013
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Landroid/view/ViewStub;LX/Jvj;)Landroid/view/ViewGroup;
    .locals 6

    .prologue
    .line 1739994
    const v0, 0x7f031075

    invoke-virtual {p1, v0}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 1739995
    invoke-virtual {p1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->e:Landroid/support/v7/widget/Toolbar;

    .line 1739996
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->i:LX/B3R;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->e:Landroid/support/v7/widget/Toolbar;

    .line 1739997
    iget-object v2, v0, LX/B3R;->a:LX/0wM;

    const v3, 0x7f020756

    const/4 p1, -0x1

    invoke-virtual {v2, v3, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationIcon(Landroid/graphics/drawable/Drawable;)V

    .line 1739998
    new-instance v2, LX/B3P;

    invoke-direct {v2, v0, p2}, LX/B3P;-><init>(LX/B3R;LX/Jvj;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1739999
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->i:LX/B3R;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->e:Landroid/support/v7/widget/Toolbar;

    const/4 v4, 0x2

    const/4 p1, 0x0

    const/4 v5, 0x1

    .line 1740000
    invoke-virtual {v1}, Landroid/support/v7/widget/Toolbar;->getMenu()Landroid/view/Menu;

    move-result-object v2

    .line 1740001
    invoke-interface {v2, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-nez v3, :cond_0

    .line 1740002
    const v3, 0x7f082743

    invoke-interface {v2, p1, v4, p1, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v3

    .line 1740003
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1740004
    const v4, 0x7f030224

    invoke-interface {v3, v4}, Landroid/view/MenuItem;->setActionView(I)Landroid/view/MenuItem;

    .line 1740005
    :cond_0
    invoke-interface {v2, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    if-nez v3, :cond_1

    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v3

    if-le v3, v5, :cond_1

    .line 1740006
    const v3, 0x7f082745

    invoke-interface {v2, p1, v5, p1, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v2

    .line 1740007
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 1740008
    iget-object v3, v0, LX/B3R;->a:LX/0wM;

    const v4, 0x7f0209b7

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 1740009
    :cond_1
    new-instance v2, LX/B3Q;

    invoke-direct {v2, v0, p2}, LX/B3Q;-><init>(LX/B3R;LX/Jvj;)V

    .line 1740010
    iput-object v2, v1, Landroid/support/v7/widget/Toolbar;->D:LX/3xb;

    .line 1740011
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->j()V

    .line 1740012
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->e:Landroid/support/v7/widget/Toolbar;

    return-object v0
.end method

.method public final a(Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 14
    .param p1    # Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLInterfaces$ImageOverlayCameraTitleFields$;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1739979
    invoke-direct {p0, p1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->b(Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;)V

    .line 1739980
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->e()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1739981
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f()V

    .line 1739982
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->i()V

    .line 1739983
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->l:LX/B4O;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v2}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v3}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v5}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f()Ljava/lang/String;

    move-result-object v5

    .line 1739984
    const-string v7, "heisman_open_selfie_camera"

    const-string v8, "heisman_composer_session_id"

    const-string v10, "tracking"

    const-string v12, "heisman_entry_point"

    const-string p0, "profile_pic_frame_id"

    move-object v6, v1

    move-object v9, v2

    move-object v11, v3

    move-object v13, v4

    move-object p1, v5

    .line 1739985
    iget-object v1, v6, LX/B4O;->a:LX/0Zb;

    const/4 v2, 0x0

    invoke-interface {v1, v7, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 1739986
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1739987
    const-string v2, "profile_picture_overlay"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1739988
    invoke-virtual {v1, v8, v9}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1739989
    invoke-virtual {v1, v10, v11}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1739990
    invoke-virtual {v1, v12, v13}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1739991
    invoke-virtual {v1, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1739992
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 1739993
    :cond_0
    return-object v0
.end method

.method public final a(Landroid/net/Uri;LX/89R;)V
    .locals 12

    .prologue
    const/4 v8, 0x0

    .line 1739972
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->j()LX/B3N;

    move-result-object v1

    iget-object v2, p2, LX/89R;->c:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1739973
    iput-object v2, v1, LX/B3N;->f:Ljava/lang/String;

    .line 1739974
    move-object v1, v1

    .line 1739975
    invoke-virtual {v1}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1739976
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->i()LX/5QV;

    move-result-object v7

    .line 1739977
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->r:LX/B5y;

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v0}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p2, LX/89R;->a:LX/89W;

    invoke-static {v0}, LX/B61;->a(LX/89W;)I

    move-result v5

    const/4 v6, 0x3

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->s:LX/B4D;

    iget-object v4, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v4}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->d()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, LX/B4D;->a(J)J

    move-result-wide v10

    move-object v4, p1

    move-object v9, v8

    invoke-interface/range {v1 .. v11}, LX/B5y;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;IILX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    .line 1739978
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 1739959
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->j()LX/B3N;

    move-result-object v1

    invoke-direct {p0, p2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1739960
    iput-object v2, v1, LX/B3N;->f:Ljava/lang/String;

    .line 1739961
    move-object v1, v1

    .line 1739962
    invoke-virtual {v1}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1739963
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->i()LX/5QV;

    move-result-object v5

    .line 1739964
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->l:LX/B4O;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v2

    .line 1739965
    const-string v3, "heisman_photo_taken"

    invoke-static {v0, v3, v1, v2}, LX/B4O;->b(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1739966
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->q:LX/74n;

    sget-object v1, LX/74j;->SINGLE_SHOT_CAMERA:LX/74j;

    invoke-virtual {v0, p1, v1}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v0

    .line 1739967
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->k:LX/B4H;

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v0}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v0

    .line 1739968
    iget-object v3, v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    move-object v4, v3

    .line 1739969
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v0}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->d()J

    move-result-wide v6

    move-object v3, p1

    move v9, v8

    invoke-virtual/range {v1 .. v9}, LX/B4H;->a(Ljava/lang/String;Landroid/net/Uri;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;LX/5QV;JZZ)Landroid/content/Intent;

    move-result-object v0

    .line 1739970
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->m:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1739971
    return-void
.end method

.method public final a(Landroid/view/ViewGroup;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "LX/0Px",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1739958
    return-void
.end method

.method public final a(Lcom/facebook/drawee/fbpipeline/FbDraweeView;I)V
    .locals 0

    .prologue
    .line 1739952
    iput-object p1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1739953
    iput p2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->z:I

    .line 1739954
    if-nez p2, :cond_0

    .line 1739955
    invoke-static {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1739956
    :goto_0
    return-void

    .line 1739957
    :cond_0
    invoke-static {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->h(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 3

    .prologue
    .line 1739944
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v1}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->j()LX/B3N;

    move-result-object v1

    invoke-direct {p0, p1}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1739945
    iput-object v2, v1, LX/B3N;->f:Ljava/lang/String;

    .line 1739946
    move-object v1, v1

    .line 1739947
    invoke-virtual {v1}, LX/B3N;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B3O;->a(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    .line 1739948
    if-nez p2, :cond_0

    .line 1739949
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->k()V

    .line 1739950
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 1739951
    :cond_0
    invoke-direct {p0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->l()V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1739939
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->l:LX/B4O;

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->g:LX/B3O;

    invoke-virtual {v2}, LX/B3O;->a()Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f()Ljava/lang/String;

    move-result-object v2

    .line 1739940
    const-string v3, "heisman_cancel_camera"

    invoke-static {v0, v3, v1, v2}, LX/B4O;->b(LX/B4O;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1739941
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->f:Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;

    invoke-virtual {v1}, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->g()Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    const-class v2, LX/1kW;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/BMQ;->a(Lcom/facebook/productionprompts/logging/PromptAnalytics;Ljava/lang/Class;Ljava/lang/Boolean;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    .line 1739942
    iget-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->d:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1739943
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1739925
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->u:LX/1Mv;

    if-eqz v0, :cond_0

    .line 1739926
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->u:LX/1Mv;

    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V

    .line 1739927
    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->u:LX/1Mv;

    .line 1739928
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->v:LX/1Mv;

    if-eqz v0, :cond_1

    .line 1739929
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->v:LX/1Mv;

    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V

    .line 1739930
    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->v:LX/1Mv;

    .line 1739931
    :cond_1
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->w:LX/1Mv;

    if-eqz v0, :cond_2

    .line 1739932
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->w:LX/1Mv;

    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V

    .line 1739933
    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->w:LX/1Mv;

    .line 1739934
    :cond_2
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->x:LX/1Mv;

    if-eqz v0, :cond_3

    .line 1739935
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->x:LX/1Mv;

    invoke-virtual {v0, v2}, LX/1Mv;->a(Z)V

    .line 1739936
    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->x:LX/1Mv;

    .line 1739937
    :cond_3
    iput-object v1, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->y:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1739938
    return-void
.end method

.method public final d()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/model/FrameGraphQLInterfaces$Frame;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1739922
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;->t:LX/B3L;

    iget-object v0, v0, LX/B3L;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v1, LX/B3H;

    invoke-direct {v1, p0}, LX/B3H;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraActivityController;)V

    .line 1739923
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1739924
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
