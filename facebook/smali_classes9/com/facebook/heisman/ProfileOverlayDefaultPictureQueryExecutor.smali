.class public Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile g:Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/ImageOverlayUriRequestFactory;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/2U5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/B3W;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1739489
    const-class v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1739490
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1739491
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;
    .locals 8

    .prologue
    .line 1739492
    sget-object v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->g:Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    if-nez v0, :cond_1

    .line 1739493
    const-class v1, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    monitor-enter v1

    .line 1739494
    :try_start_0
    sget-object v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->g:Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1739495
    if-eqz v2, :cond_0

    .line 1739496
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1739497
    new-instance v3, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    invoke-direct {v3}, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;-><init>()V

    .line 1739498
    const/16 v4, 0x24dc

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const-class v6, LX/2U5;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2U5;

    invoke-static {v0}, LX/B3W;->a(LX/0QB;)LX/B3W;

    move-result-object v7

    check-cast v7, LX/B3W;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    .line 1739499
    iput-object v4, v3, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->a:LX/0Or;

    iput-object v5, v3, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->b:LX/0Or;

    iput-object v6, v3, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->d:LX/2U5;

    iput-object v7, v3, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->e:LX/B3W;

    iput-object p0, v3, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->f:LX/0tX;

    .line 1739500
    move-object v0, v3

    .line 1739501
    sput-object v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->g:Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1739502
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1739503
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1739504
    :cond_1
    sget-object v0, Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;->g:Lcom/facebook/heisman/ProfileOverlayDefaultPictureQueryExecutor;

    return-object v0

    .line 1739505
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1739506
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
