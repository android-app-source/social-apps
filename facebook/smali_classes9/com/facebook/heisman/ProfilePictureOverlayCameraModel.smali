.class public Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

.field public final b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1740255
    new-instance v0, LX/B3M;

    invoke-direct {v0}, LX/B3M;-><init>()V

    sput-object v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/B3N;)V
    .locals 1

    .prologue
    .line 1740247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740248
    iget-object v0, p1, LX/B3N;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1740249
    iget-object v0, p1, LX/B3N;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    .line 1740250
    iget-object v0, p1, LX/B3N;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->c:Ljava/lang/String;

    .line 1740251
    iget-object v0, p1, LX/B3N;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->d:Ljava/lang/String;

    .line 1740252
    iget-object v0, p1, LX/B3N;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1740253
    iget-object v0, p1, LX/B3N;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f:Ljava/lang/String;

    .line 1740254
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1740239
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740240
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1740241
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    .line 1740242
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->c:Ljava/lang/String;

    .line 1740243
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->d:Ljava/lang/String;

    .line 1740244
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1740245
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f:Ljava/lang/String;

    .line 1740246
    return-void
.end method

.method private a(Ljava/lang/String;)LX/5QV;
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1740226
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1740227
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    .line 1740228
    :goto_0
    return-object v0

    .line 1740229
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    if-eqz v0, :cond_1

    .line 1740230
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    invoke-static {v0, p1}, LX/B5R;->a(Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Ljava/lang/String;)LX/5QV;

    move-result-object v0

    goto :goto_0

    .line 1740231
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Did not find overlay with ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1740234
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 1740235
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1740236
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 1740237
    :goto_1
    return-object v0

    :cond_0
    move v0, v1

    .line 1740238
    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1740256
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1740233
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1740232
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->j()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->j()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->j()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()LX/5QV;
    .locals 1

    .prologue
    .line 1740224
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1740225
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a(Ljava/lang/String;)LX/5QV;

    move-result-object v0

    return-object v0
.end method

.method public final j()LX/B3N;
    .locals 1

    .prologue
    .line 1740223
    new-instance v0, LX/B3N;

    invoke-direct {v0, p0}, LX/B3N;-><init>(Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;)V

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1740216
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1740217
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->b:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1740218
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1740219
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1740220
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->e:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1740221
    iget-object v0, p0, Lcom/facebook/heisman/ProfilePictureOverlayCameraModel;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1740222
    return-void
.end method
