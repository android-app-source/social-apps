.class public Lcom/facebook/heisman/ImageOverlayUriRequestFactory;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile c:Lcom/facebook/heisman/ImageOverlayUriRequestFactory;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B30;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1739478
    const-class v0, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1739448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1739449
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/heisman/ImageOverlayUriRequestFactory;
    .locals 4

    .prologue
    .line 1739463
    sget-object v0, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->c:Lcom/facebook/heisman/ImageOverlayUriRequestFactory;

    if-nez v0, :cond_1

    .line 1739464
    const-class v1, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;

    monitor-enter v1

    .line 1739465
    :try_start_0
    sget-object v0, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->c:Lcom/facebook/heisman/ImageOverlayUriRequestFactory;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1739466
    if-eqz v2, :cond_0

    .line 1739467
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1739468
    new-instance v3, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;

    invoke-direct {v3}, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;-><init>()V

    .line 1739469
    const/16 p0, 0x24db

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1739470
    iput-object p0, v3, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->a:LX/0Or;

    .line 1739471
    move-object v0, v3

    .line 1739472
    sput-object v0, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->c:Lcom/facebook/heisman/ImageOverlayUriRequestFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1739473
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1739474
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1739475
    :cond_1
    sget-object v0, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->c:Lcom/facebook/heisman/ImageOverlayUriRequestFactory;

    return-object v0

    .line 1739476
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1739477
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;LX/0v6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
            "LX/0v6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1739450
    invoke-static {p1}, LX/5Qm;->b(LX/5QV;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1739451
    invoke-static {p1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1739452
    :goto_0
    return-object v0

    .line 1739453
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B30;

    .line 1739454
    invoke-virtual {p1}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 1739455
    new-instance v2, LX/B4f;

    invoke-direct {v2}, LX/B4f;-><init>()V

    move-object v2, v2

    .line 1739456
    const-string v3, "image_overlay_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "image_high_width"

    iget-object p1, v0, LX/B30;->a:LX/0sa;

    invoke-virtual {p1}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v2, v3, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/B4f;

    move-object v0, v2

    .line 1739457
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 1739458
    sget-object v1, Lcom/facebook/heisman/ImageOverlayUriRequestFactory;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v0, v1}, LX/B32;->a(LX/0zO;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1739459
    invoke-virtual {p2, v0}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1739460
    new-instance v1, LX/B31;

    invoke-direct {v1, p0}, LX/B31;-><init>(Lcom/facebook/heisman/ImageOverlayUriRequestFactory;)V

    .line 1739461
    sget-object v2, LX/131;->INSTANCE:LX/131;

    move-object v2, v2

    .line 1739462
    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
