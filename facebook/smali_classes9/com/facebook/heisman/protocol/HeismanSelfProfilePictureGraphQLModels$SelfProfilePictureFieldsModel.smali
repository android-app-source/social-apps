.class public final Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xe33add6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1743066
    const-class v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1743065
    const-class v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1743063
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1743064
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1743055
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1743056
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1743057
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x5d006bf9

    invoke-static {v2, v1, v3}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1743058
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1743059
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1743060
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1743061
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1743062
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1743040
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1743041
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1743042
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    .line 1743043
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1743044
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    .line 1743045
    iput-object v0, v1, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->e:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    .line 1743046
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1743047
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x5d006bf9

    invoke-static {v2, v0, v3}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1743048
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1743049
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    .line 1743050
    iput v3, v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->f:I

    move-object v1, v0

    .line 1743051
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1743052
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    .line 1743053
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object p0, v1

    .line 1743054
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1743039
    new-instance v0, LX/B4q;

    invoke-direct {v0, p1}, LX/B4q;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1743037
    iget-object v0, p0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->e:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    iput-object v0, p0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->e:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    .line 1743038
    iget-object v0, p0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->e:Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$ProfilePhotoModel;

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1743024
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1743025
    const/4 v0, 0x1

    const v1, -0x5d006bf9

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->f:I

    .line 1743026
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1743035
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1743036
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1743034
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1743031
    new-instance v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-direct {v0}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;-><init>()V

    .line 1743032
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1743033
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1743030
    const v0, 0x1e679bce

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1743029
    const v0, 0x285feb

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getProfilePicture"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1743027
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1743028
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
