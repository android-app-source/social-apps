.class public final Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1742551
    const-class v0, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;

    new-instance v1, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1742552
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1742546
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1742548
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1742549
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1, p2}, LX/B4m;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1742550
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1742547
    check-cast p1, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel$Serializer;->a(Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$CameraTitleFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
