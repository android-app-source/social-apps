.class public final Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1742916
    const-class v0, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    new-instance v1, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1742917
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1742918
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1742919
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1742920
    const/4 v2, 0x0

    .line 1742921
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_5

    .line 1742922
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1742923
    :goto_0
    move v1, v2

    .line 1742924
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1742925
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1742926
    new-instance v1, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;

    invoke-direct {v1}, Lcom/facebook/heisman/protocol/HeismanSelfProfilePictureGraphQLModels$SelfProfilePictureFieldsModel;-><init>()V

    .line 1742927
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1742928
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1742929
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1742930
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1742931
    :cond_0
    return-object v1

    .line 1742932
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1742933
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 1742934
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1742935
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1742936
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, p0, :cond_2

    if-eqz v4, :cond_2

    .line 1742937
    const-string v5, "profile_photo"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1742938
    invoke-static {p1, v0}, LX/B4s;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 1742939
    :cond_3
    const-string v5, "profile_picture"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1742940
    const/4 v4, 0x0

    .line 1742941
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_9

    .line 1742942
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1742943
    :goto_2
    move v1, v4

    .line 1742944
    goto :goto_1

    .line 1742945
    :cond_4
    const/4 v4, 0x2

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1742946
    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1742947
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1742948
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_5
    move v1, v2

    move v3, v2

    goto :goto_1

    .line 1742949
    :cond_6
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1742950
    :cond_7
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, p0, :cond_8

    .line 1742951
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1742952
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1742953
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_7

    if-eqz v5, :cond_7

    .line 1742954
    const-string p0, "uri"

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 1742955
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 1742956
    :cond_8
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1742957
    invoke-virtual {v0, v4, v1}, LX/186;->b(II)V

    .line 1742958
    invoke-virtual {v0}, LX/186;->d()I

    move-result v4

    goto :goto_2

    :cond_9
    move v1, v4

    goto :goto_3
.end method
