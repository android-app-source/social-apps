.class public final Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1741940
    const-class v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;

    new-instance v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1741941
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1741942
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1741943
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1741944
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1741945
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1741946
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1741947
    if-eqz v2, :cond_0

    .line 1741948
    const-string p0, "profile_pic_overlay_suggestions"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1741949
    invoke-static {v1, v2, p1, p2}, LX/B4X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1741950
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1741951
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1741952
    check-cast p1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$Serializer;->a(Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
