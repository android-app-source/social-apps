.class public final Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1743652
    const-class v0, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;

    new-instance v1, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1743653
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1743633
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1743634
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1743635
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1743636
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1743637
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1743638
    if-eqz v2, :cond_0

    .line 1743639
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1743640
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1743641
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1743642
    if-eqz v2, :cond_2

    .line 1743643
    const-string p0, "associated_pages"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1743644
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1743645
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 1743646
    invoke-virtual {v1, v2, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/B5C;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1743647
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1743648
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1743649
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1743650
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1743651
    check-cast p1, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel$Serializer;->a(Lcom/facebook/heisman/protocol/ProfileOverlayPagesByImageOverlayGraphQLModels$ProfileOverlayPagesByImageOverlayQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
