.class public final Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/5QV;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x51d7f1c7
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1743988
    const-class v0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1743985
    const-class v0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1743986
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1743987
    return-void
.end method

.method private m()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1744004
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->h:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    iput-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->h:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    .line 1744005
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->h:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1743989
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1743990
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1743991
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1743992
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->m()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1743993
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->k()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1743994
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1743995
    const/4 v5, 0x6

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1743996
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1743997
    const/4 v0, 0x1

    iget-boolean v5, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->f:Z

    invoke-virtual {p1, v0, v5}, LX/186;->a(IZ)V

    .line 1743998
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1743999
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1744000
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1744001
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1744002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1744003
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1744007
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1744008
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->m()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1744009
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->m()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    .line 1744010
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->m()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1744011
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;

    .line 1744012
    iput-object v0, v1, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->h:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    .line 1744013
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->k()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1744014
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->k()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    .line 1744015
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->k()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1744016
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;

    .line 1744017
    iput-object v0, v1, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->i:Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    .line 1744018
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1744019
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1744006
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1743979
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1743980
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->f:Z

    .line 1743981
    return-void
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1743982
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1743983
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1743984
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1743965
    new-instance v0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;

    invoke-direct {v0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;-><init>()V

    .line 1743966
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1743967
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1743968
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->g:Ljava/lang/String;

    .line 1743969
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1743970
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->m()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1743971
    const v0, -0x305825af

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1743972
    const v0, 0x51cf9e87

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 1743977
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1743978
    iget-boolean v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->f:Z

    return v0
.end method

.method public final k()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1743973
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->i:Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    iput-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->i:Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    .line 1743974
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->i:Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel$LayerOwnerModel;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1743975
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->j:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->j:Ljava/lang/String;

    .line 1743976
    iget-object v0, p0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;->j:Ljava/lang/String;

    return-object v0
.end method
