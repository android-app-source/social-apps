.class public final Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2ff071af
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1742263
    const-class v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1742233
    const-class v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1742266
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1742267
    return-void
.end method

.method private j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1742264
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->f:Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    iput-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->f:Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    .line 1742265
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->f:Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1742242
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1742243
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->b()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1742244
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1742245
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1742246
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1742247
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1742248
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1742249
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1742250
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1742251
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->b()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1742252
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->b()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1742253
    if-eqz v1, :cond_2

    .line 1742254
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    .line 1742255
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->e:LX/3Sb;

    move-object v1, v0

    .line 1742256
    :goto_0
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1742257
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    .line 1742258
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1742259
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    .line 1742260
    iput-object v0, v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->f:Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    .line 1742261
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1742262
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1742241
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->j()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getContextLines"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1742239
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, -0x5bbc224a

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->e:LX/3Sb;

    .line 1742240
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1742236
    new-instance v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    invoke-direct {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;-><init>()V

    .line 1742237
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1742238
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1742235
    const v0, -0x4730e0a9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1742234
    const v0, 0x43207eb

    return v0
.end method
