.class public final Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x76015203
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1741860
    const-class v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1741844
    const-class v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1741858
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1741859
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1741852
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1741853
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1741854
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1741855
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1741856
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1741857
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1741861
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1741862
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1741863
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    .line 1741864
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1741865
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;

    .line 1741866
    iput-object v0, v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->e:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    .line 1741867
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1741868
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1741850
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->e:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    iput-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->e:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    .line 1741851
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->e:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1741847
    new-instance v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;

    invoke-direct {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;-><init>()V

    .line 1741848
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1741849
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1741846
    const v0, -0x58dd276c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1741845
    const v0, -0x739a07f7

    return v0
.end method
