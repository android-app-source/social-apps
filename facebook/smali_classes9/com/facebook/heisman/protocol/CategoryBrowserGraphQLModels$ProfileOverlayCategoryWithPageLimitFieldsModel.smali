.class public final Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x67a61bc5
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1742211
    const-class v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1742210
    const-class v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1742208
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1742209
    return-void
.end method

.method private k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1742206
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    iput-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1742207
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1742196
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1742197
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1742198
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1742199
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1742200
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1742201
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1742202
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1742203
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1742204
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1742205
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1742183
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1742184
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1742185
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    .line 1742186
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1742187
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;

    .line 1742188
    iput-object v0, v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->f:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    .line 1742189
    :cond_0
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1742190
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1742191
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1742192
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;

    .line 1742193
    iput-object v0, v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->g:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    .line 1742194
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1742195
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1742212
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1742180
    new-instance v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;

    invoke-direct {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;-><init>()V

    .line 1742181
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1742182
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1742178
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->e:Ljava/lang/String;

    .line 1742179
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()LX/174;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1742177
    invoke-direct {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->k()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1742176
    const v0, 0xa95aecf

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1742175
    const v0, -0x79bab75b

    return v0
.end method

.method public final j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1742173
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->f:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    iput-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->f:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    .line 1742174
    iget-object v0, p0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->f:Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    return-object v0
.end method
