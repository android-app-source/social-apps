.class public final Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x8501c47
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1743149
    const-class v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1743148
    const-class v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1743125
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1743126
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1743142
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1743143
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1743144
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1743145
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1743146
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1743147
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1743140
    iget-object v0, p0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->e:Ljava/util/List;

    .line 1743141
    iget-object v0, p0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1743132
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1743133
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1743134
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1743135
    if-eqz v1, :cond_0

    .line 1743136
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;

    .line 1743137
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;->e:Ljava/util/List;

    .line 1743138
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1743139
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1743129
    new-instance v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;

    invoke-direct {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$PageProfilePictureOverlaysConnectionFieldsModel;-><init>()V

    .line 1743130
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1743131
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1743128
    const v0, 0x61489e28

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1743127
    const v0, -0x795323db

    return v0
.end method
