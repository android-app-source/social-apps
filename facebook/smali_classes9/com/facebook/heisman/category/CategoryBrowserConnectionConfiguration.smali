.class public Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/B0U;


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0sa;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/2U5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1740807
    const-class v0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740803
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1740804
    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->e:LX/0Ot;

    .line 1740805
    iput-boolean p1, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->f:Z

    .line 1740806
    return-void
.end method

.method public static a$redex0(Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;LX/B0d;Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserQueryModel;)LX/B0N;
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1740780
    new-instance v2, LX/9JH;

    invoke-direct {v2}, LX/9JH;-><init>()V

    .line 1740781
    invoke-virtual {p2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoriesConnectionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoriesConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoriesConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1740782
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "category_browser_malformed_data"

    const-string v3, "Missing list of categories"

    invoke-virtual {v0, v1, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1740783
    new-instance v0, LX/B0i;

    move-object v1, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    .line 1740784
    :goto_0
    return-object v0

    .line 1740785
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoriesConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoriesConnectionFieldsModel;->a()LX/0Px;

    move-result-object v7

    .line 1740786
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v6, v5

    :goto_1
    if-ge v6, v8, :cond_6

    invoke-virtual {v7, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;

    .line 1740787
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    move-result-object v1

    if-nez v1, :cond_3

    .line 1740788
    const-string v1, "null pages connection for category %s"

    new-array v3, v12, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1740789
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "category_browser_malformed_data"

    invoke-virtual {v0, v3, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1740790
    :cond_2
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 1740791
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v1

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v3

    invoke-virtual {v2, v1, v3, v4, v5}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 1740792
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;->a()LX/0Px;

    move-result-object v9

    .line 1740793
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v3, v5

    :goto_3
    if-ge v3, v10, :cond_4

    invoke-virtual {v9, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    .line 1740794
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v11

    invoke-static {v1}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v1

    invoke-virtual {v2, v11, v1, v4, v12}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 1740795
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 1740796
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_5

    .line 1740797
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel;->j()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlayCategoryWithPageLimitFieldsModel$PagesModel;->j()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1740798
    invoke-virtual {v3, v1, v5}, LX/15i;->h(II)Z

    move-result v1

    :goto_4
    if-eqz v1, :cond_2

    .line 1740799
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v1

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v0

    const/4 v3, 0x2

    invoke-virtual {v2, v1, v0, v4, v3}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    goto :goto_2

    :cond_5
    move v1, v5

    .line 1740800
    goto :goto_4

    .line 1740801
    :cond_6
    new-instance v0, LX/B0i;

    move-object v1, p2

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method public static synthetic b(Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    .locals 11

    .prologue
    .line 1740761
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1740762
    if-eqz p2, :cond_0

    .line 1740763
    iget-object v1, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1740764
    if-eqz v1, :cond_0

    .line 1740765
    iget-object v1, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1740766
    check-cast v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$ProfilePicOverlaySuggestionsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1740767
    iget-object v1, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1740768
    check-cast v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$ProfilePicOverlaySuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$ProfilePicOverlaySuggestionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    move-object v1, v5

    .line 1740769
    :goto_0
    move-object v0, v1

    .line 1740770
    return-object v0

    .line 1740771
    :cond_1
    iget-object v1, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 1740772
    check-cast v1, Lcom/facebook/flatbuffers/MutableFlattenable;

    invoke-static {v1}, LX/9JH;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;

    .line 1740773
    new-instance v3, LX/9JH;

    invoke-direct {v3}, LX/9JH;-><init>()V

    .line 1740774
    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$ProfilePicOverlaySuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v1

    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$ProfilePicOverlaySuggestionsModel;

    move-result-object v4

    invoke-static {v4}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v4

    const/4 v7, 0x4

    invoke-virtual {v3, v1, v4, v5, v7}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 1740775
    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$ProfilePicOverlaySuggestionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSuggestedFramesQueryModel$ProfilePicOverlaySuggestionsModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v4, v6

    .line 1740776
    :goto_1
    if-ge v4, v8, :cond_2

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;

    .line 1740777
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v9

    invoke-static {v1}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v1

    const/4 v10, 0x5

    invoke-virtual {v3, v9, v1, v5, v10}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 1740778
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 1740779
    :cond_2
    new-instance v1, LX/B0i;

    move-object v4, p1

    invoke-direct/range {v1 .. v6}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1740808
    const-wide/32 v0, 0x15180

    return-wide v0
.end method

.method public final a(Lcom/facebook/quicklog/QuickPerformanceLogger;LX/B0M;Ljava/util/concurrent/Executor;LX/B0d;)LX/0v6;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 1740728
    const-string v0, "ProfilePictureOverlayCategoryBrowser"

    invoke-static {v0}, LX/2U5;->a(Ljava/lang/String;)LX/0v6;

    move-result-object v1

    .line 1740729
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 1740730
    sget-short v2, LX/B3X;->a:S

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1740731
    iget-object v5, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0sa;

    .line 1740732
    invoke-static {}, LX/B5F;->a()LX/B5E;

    move-result-object v6

    const-string v7, "image_high_width"

    invoke-virtual {v5}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/B5E;

    .line 1740733
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v5, v6}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v5

    sget-object v6, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1740734
    iput-object v6, v5, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1740735
    move-object v5, v5

    .line 1740736
    sget-object v6, LX/0zS;->a:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    const-wide/16 v7, 0xe10

    invoke-virtual {v5, v7, v8}, LX/0zO;->a(J)LX/0zO;

    move-result-object v5

    .line 1740737
    invoke-virtual {v1, v5}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 1740738
    new-instance v2, LX/B3g;

    invoke-direct {v2, p0, p4, p2}, LX/B3g;-><init>(Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;LX/B0d;LX/B0M;)V

    invoke-static {v0, v2, p3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1740739
    :goto_0
    return-object v1

    .line 1740740
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    .line 1740741
    sget-short v2, LX/0wf;->d:S

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1740742
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1740743
    :goto_1
    move-object v0, v0

    .line 1740744
    iget-object v2, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0sa;

    .line 1740745
    new-instance v3, LX/B4Q;

    invoke-direct {v3}, LX/B4Q;-><init>()V

    move-object v3, v3

    .line 1740746
    const-string v5, "image_high_width"

    invoke-virtual {v2}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "page_logo_size"

    invoke-static {}, LX/0wB;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/B4Q;

    .line 1740747
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v3}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    sget-object v3, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1740748
    iput-object v3, v2, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1740749
    move-object v2, v2

    .line 1740750
    move-object v2, v2

    .line 1740751
    invoke-virtual {v1, v2}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1740752
    const/4 v3, 0x2

    new-array v3, v3, [Lcom/google/common/util/concurrent/ListenableFuture;

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v2, v3, v0

    invoke-static {v3}, LX/0Vg;->a([Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/B3h;

    invoke-direct {v2, p0, p4, p2}, LX/B3h;-><init>(Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;LX/B0d;LX/B0M;)V

    invoke-static {v0, v2, p3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0

    .line 1740753
    :cond_1
    sget v2, LX/0wf;->e:I

    const/4 v3, 0x4

    invoke-interface {v0, v2, v3}, LX/0ad;->a(II)I

    move-result v2

    .line 1740754
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sa;

    .line 1740755
    new-instance v3, LX/B4S;

    invoke-direct {v3}, LX/B4S;-><init>()V

    move-object v3, v3

    .line 1740756
    const-string v5, "suggested_page_count"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "image_high_width"

    invoke-virtual {v0}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v2, "page_logo_size"

    invoke-static {}, LX/0wB;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/B4S;

    .line 1740757
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v2, Lcom/facebook/heisman/category/CategoryBrowserConnectionConfiguration;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1740758
    iput-object v2, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1740759
    move-object v0, v0

    .line 1740760
    invoke-virtual {v1, v0}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public final b()I
    .locals 1
    .annotation build Lcom/facebook/graphql/connection/ConnectionTailLoaderManager$CachePolicy;
    .end annotation

    .prologue
    .line 1740727
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1740726
    const/4 v0, 0x0

    return v0
.end method
