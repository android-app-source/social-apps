.class public Lcom/facebook/heisman/category/RemoveFrameViewBinder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final c:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile e:Lcom/facebook/heisman/category/RemoveFrameViewBinder;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B3x;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/23P;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1741233
    const-class v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;

    const-string v1, "profile_picture_overlay"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1741234
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741235
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/heisman/category/RemoveFrameViewBinder;
    .locals 6

    .prologue
    .line 1741236
    sget-object v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->e:Lcom/facebook/heisman/category/RemoveFrameViewBinder;

    if-nez v0, :cond_1

    .line 1741237
    const-class v1, Lcom/facebook/heisman/category/RemoveFrameViewBinder;

    monitor-enter v1

    .line 1741238
    :try_start_0
    sget-object v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->e:Lcom/facebook/heisman/category/RemoveFrameViewBinder;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1741239
    if-eqz v2, :cond_0

    .line 1741240
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1741241
    new-instance v4, Lcom/facebook/heisman/category/RemoveFrameViewBinder;

    invoke-direct {v4}, Lcom/facebook/heisman/category/RemoveFrameViewBinder;-><init>()V

    .line 1741242
    const/16 v3, 0x24e6

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v3, 0x1b

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v3

    check-cast v3, LX/23P;

    .line 1741243
    iput-object v5, v4, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->a:LX/0Or;

    iput-object p0, v4, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->b:LX/0Or;

    iput-object v3, v4, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->d:LX/23P;

    .line 1741244
    move-object v0, v4

    .line 1741245
    sput-object v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->e:Lcom/facebook/heisman/category/RemoveFrameViewBinder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1741246
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1741247
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1741248
    :cond_1
    sget-object v0, Lcom/facebook/heisman/category/RemoveFrameViewBinder;->e:Lcom/facebook/heisman/category/RemoveFrameViewBinder;

    return-object v0

    .line 1741249
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1741250
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
