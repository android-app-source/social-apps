.class public Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B3x;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1741184
    const-class v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;

    const-string v1, "profile_picture_overlay"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1741185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741186
    return-void
.end method

.method public static a(Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;Lcom/facebook/fig/listitem/FigListItem;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1741172
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->a()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    move-result-object v0

    .line 1741173
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1741174
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->c()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel$PageLogoModel;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1741175
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;->c()Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel$PageLogoModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel$PageLogoModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1741176
    :cond_0
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->b()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->b()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1741177
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->b()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v3, v0, LX/1vs;->b:I

    .line 1741178
    if-eqz v3, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    .line 1741179
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->b()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 1741180
    invoke-virtual {v3, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    :goto_1
    if-eqz v1, :cond_1

    .line 1741181
    invoke-virtual {p0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$ProfileOverlaySuggestionPageWithContextModel;->b()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    invoke-virtual {v1, v3, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 1741182
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1741183
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;Lcom/facebook/fig/listitem/FigListItem;)V
    .locals 5

    .prologue
    .line 1741141
    iget-object v0, p0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B3x;

    .line 1741142
    iget-object v1, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->e:Ljava/lang/String;

    move-object v1, v1

    .line 1741143
    if-eqz v1, :cond_1

    .line 1741144
    iget-object v0, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1741145
    invoke-virtual {p2, v0}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1741146
    iget-object v0, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->e:Ljava/lang/String;

    move-object v0, v0

    .line 1741147
    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1741148
    :cond_0
    :goto_0
    return-void

    .line 1741149
    :cond_1
    iget-object v1, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v1, v1

    .line 1741150
    if-eqz v1, :cond_0

    .line 1741151
    iget-object v1, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1741152
    if-eqz v1, :cond_0

    .line 1741153
    const-string v1, ""

    .line 1741154
    iget-object v2, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1741155
    if-nez v2, :cond_2

    .line 1741156
    iget-object v2, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1741157
    invoke-virtual {p2, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1741158
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1741159
    iget-object v2, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1741160
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 1741161
    iget-object v1, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->f:Ljava/lang/String;

    move-object v1, v1

    .line 1741162
    iget-object v2, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v2, v2

    .line 1741163
    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;->j()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 1741164
    invoke-virtual {p2}, Lcom/facebook/fig/listitem/FigListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1a1b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1741165
    sget-object v4, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotListItemBinder;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v4, v1, v2, v3}, LX/B3x;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;Ljava/lang/String;I)Landroid/graphics/drawable/LayerDrawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 1741166
    :cond_2
    iget-object v2, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1741167
    invoke-virtual {p2, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1741168
    invoke-virtual {p2}, Lcom/facebook/fig/listitem/FigListItem;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08274c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1741169
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1741170
    iget-object v2, p1, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1741171
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method
