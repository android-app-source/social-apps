.class public final Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/B3l;

.field private final b:Ljava/lang/CharSequence;

.field public c:Z


# direct methods
.method public constructor <init>(LX/B3l;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1740858
    iput-object p1, p0, Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;->a:LX/B3l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740859
    iput-object p2, p0, Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;->b:Ljava/lang/CharSequence;

    .line 1740860
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1740861
    iget-boolean v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;->c:Z

    if-eqz v0, :cond_0

    .line 1740862
    :goto_0
    return-void

    .line 1740863
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;->a:LX/B3l;

    iget-object v1, p0, Lcom/facebook/heisman/category/CategoryBrowserFetchController$SearchTask;->b:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1740864
    const-string v2, "ProfilePictureOverlayCategoryBrowser_%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1740865
    iget-object v3, v0, LX/B3l;->h:LX/B3t;

    .line 1740866
    new-instance v5, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;

    invoke-static {v3}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {v5, v1, v4}, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;-><init>(Ljava/lang/String;LX/0ad;)V

    .line 1740867
    const/16 v4, 0x259

    invoke-static {v3, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v6, 0x1223

    invoke-static {v3, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    .line 1740868
    iput-object v4, v5, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->a:LX/0Or;

    iput-object v6, v5, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->b:LX/0Or;

    .line 1740869
    move-object v3, v5

    .line 1740870
    iget-object v4, v0, LX/B3l;->e:LX/B0S;

    new-instance v5, LX/B0c;

    const-wide/32 v6, 0x15180

    invoke-direct {v5, v3, v6, v7}, LX/B0c;-><init>(LX/B0V;J)V

    invoke-virtual {v4, v2, v5}, LX/B0S;->a(Ljava/lang/String;LX/B0U;)V

    .line 1740871
    goto :goto_0
.end method
