.class public Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile d:Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;


# instance fields
.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1741215
    const-class v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1741216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741217
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;
    .locals 5

    .prologue
    .line 1741218
    sget-object v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->d:Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    if-nez v0, :cond_1

    .line 1741219
    const-class v1, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    monitor-enter v1

    .line 1741220
    :try_start_0
    sget-object v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->d:Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1741221
    if-eqz v2, :cond_0

    .line 1741222
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1741223
    new-instance p0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    invoke-direct {p0}, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;-><init>()V

    .line 1741224
    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v4

    check-cast v4, LX/0sa;

    .line 1741225
    iput-object v3, p0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->b:LX/0tX;

    iput-object v4, p0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->c:LX/0sa;

    .line 1741226
    move-object v0, p0

    .line 1741227
    sput-object v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->d:Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1741228
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1741229
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1741230
    :cond_1
    sget-object v0, Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;->d:Lcom/facebook/heisman/category/ProfilePictureOverlayPivotQueryExecutor;

    return-object v0

    .line 1741231
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1741232
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
