.class public Lcom/facebook/heisman/category/CategoryBrowserActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/B4H;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public q:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/B3Z;

.field public t:LX/0h5;

.field private u:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public v:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1740473
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1740474
    new-instance v0, LX/B3Z;

    invoke-direct {v0, p0}, LX/B3Z;-><init>(Lcom/facebook/heisman/category/CategoryBrowserActivity;)V

    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->s:LX/B3Z;

    return-void
.end method

.method private a()LX/0h5;
    .locals 2

    .prologue
    .line 1740475
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1740476
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1740477
    new-instance v1, LX/B3Y;

    invoke-direct {v1, p0}, LX/B3Y;-><init>(Lcom/facebook/heisman/category/CategoryBrowserActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1740478
    return-object v0
.end method

.method private static a(Lcom/facebook/heisman/category/CategoryBrowserActivity;LX/B4H;LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/heisman/category/CategoryBrowserActivity;",
            "LX/B4H;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1740479
    iput-object p1, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->p:LX/B4H;

    iput-object p2, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->q:LX/0Or;

    iput-object p3, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->r:LX/0Ot;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;

    invoke-static {v1}, LX/B4H;->a(LX/0QB;)LX/B4H;

    move-result-object v0

    check-cast v0, LX/B4H;

    const/16 v2, 0x455

    invoke-static {v1, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0x259

    invoke-static {v1, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    invoke-static {p0, v0, v2, v1}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->a(Lcom/facebook/heisman/category/CategoryBrowserActivity;LX/B4H;LX/0Or;LX/0Ot;)V

    return-void
.end method

.method public static b(Lcom/facebook/heisman/category/CategoryBrowserActivity;)V
    .locals 3

    .prologue
    .line 1740480
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1740481
    invoke-virtual {p0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1740482
    invoke-virtual {p0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1740483
    :cond_0
    return-void
.end method

.method public static l(Lcom/facebook/heisman/category/CategoryBrowserActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1740484
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->u:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1740485
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->u:Ljava/lang/String;

    .line 1740486
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->u:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1740487
    invoke-static {p0, p0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1740488
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1740489
    instance-of v0, p1, Lcom/facebook/heisman/category/CategoryBrowserFragment;

    if-eqz v0, :cond_1

    .line 1740490
    check-cast p1, Lcom/facebook/heisman/category/CategoryBrowserFragment;

    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->s:LX/B3Z;

    .line 1740491
    iput-object v0, p1, Lcom/facebook/heisman/category/CategoryBrowserFragment;->h:LX/B3Z;

    .line 1740492
    :cond_0
    :goto_0
    return-void

    .line 1740493
    :cond_1
    instance-of v0, p1, Lcom/facebook/heisman/category/SingleCategoryFragment;

    if-eqz v0, :cond_0

    .line 1740494
    check-cast p1, Lcom/facebook/heisman/category/SingleCategoryFragment;

    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->s:LX/B3Z;

    .line 1740495
    iput-object v0, p1, Lcom/facebook/heisman/category/SingleCategoryFragment;->c:LX/B3T;

    .line 1740496
    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1740497
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1740498
    const v0, 0x7f031076

    invoke-virtual {p0, v0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->setContentView(I)V

    .line 1740499
    invoke-direct {p0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->a()LX/0h5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->t:LX/0h5;

    .line 1740500
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "category_browser_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1740501
    invoke-virtual {p0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1740502
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1740503
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1740504
    new-instance v2, Lcom/facebook/heisman/category/CategoryBrowserFragment;

    invoke-direct {v2}, Lcom/facebook/heisman/category/CategoryBrowserFragment;-><init>()V

    .line 1740505
    invoke-virtual {v2, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1740506
    move-object v0, v2

    .line 1740507
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d2766

    const-string v3, "category_browser_fragment"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1740508
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photo_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->v:Ljava/lang/String;

    .line 1740509
    invoke-virtual {p0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "photo_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserActivity;->w:Ljava/lang/String;

    .line 1740510
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1740511
    const/16 v0, 0xc35

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1740512
    invoke-virtual {p0, p2, p3}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->setResult(ILandroid/content/Intent;)V

    .line 1740513
    invoke-virtual {p0}, Lcom/facebook/heisman/category/CategoryBrowserActivity;->finish()V

    .line 1740514
    :cond_0
    return-void
.end method
