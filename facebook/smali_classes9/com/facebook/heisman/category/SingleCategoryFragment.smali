.class public Lcom/facebook/heisman/category/SingleCategoryFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/B4B;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:LX/B4A;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/B3T;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1741268
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1741269
    return-void
.end method

.method public static a(Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)Lcom/facebook/heisman/category/SingleCategoryFragment;
    .locals 3

    .prologue
    .line 1741251
    new-instance v0, Lcom/facebook/heisman/category/SingleCategoryFragment;

    invoke-direct {v0}, Lcom/facebook/heisman/category/SingleCategoryFragment;-><init>()V

    .line 1741252
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1741253
    const-string v2, "heisman_pivot_intent_data"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1741254
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1741255
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1741265
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1741266
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/heisman/category/SingleCategoryFragment;

    const-class v0, LX/B4B;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/B4B;

    iput-object p1, p0, Lcom/facebook/heisman/category/SingleCategoryFragment;->a:LX/B4B;

    .line 1741267
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x29082913

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1741264
    const v1, 0x7f031339

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x7e0370c9

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1741256
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1741257
    const-string v1, "heisman_pivot_intent_data"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    .line 1741258
    const v0, 0x7f0d2bb3

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/FrameLayout;

    .line 1741259
    const v0, 0x7f0d2c8a

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1741260
    const v0, 0x7f0d2768

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1741261
    iget-object v0, p0, Lcom/facebook/heisman/category/SingleCategoryFragment;->a:LX/B4B;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/heisman/category/SingleCategoryFragment;->c:LX/B3T;

    invoke-virtual/range {v0 .. v6}, LX/B4B;->a(Landroid/app/Activity;LX/B3T;Landroid/widget/FrameLayout;Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Lcom/facebook/widget/recyclerview/BetterRecyclerView;Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)LX/B4A;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/category/SingleCategoryFragment;->b:LX/B4A;

    .line 1741262
    iget-object v0, p0, Lcom/facebook/heisman/category/SingleCategoryFragment;->b:LX/B4A;

    invoke-virtual {v0}, LX/B4A;->a()V

    .line 1741263
    return-void
.end method
