.class public Lcom/facebook/heisman/category/CategoryBrowserFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final e:[Ljava/lang/String;


# instance fields
.field public a:LX/B3f;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/B3q;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/B3m;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0i4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/B3l;

.field public g:LX/B3e;

.field public h:LX/B3Z;

.field public i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public j:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1740965
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.ACCESS_FINE_LOCATION"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.ACCESS_COARSE_LOCATION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1740917
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1740918
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1740919
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1740920
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;

    const-class v2, LX/B3f;

    invoke-interface {p1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/B3f;

    const-class v3, LX/B3q;

    invoke-interface {p1, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/B3q;

    const-class v4, LX/B3m;

    invoke-interface {p1, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/B3m;

    const-class v0, LX/0i4;

    invoke-interface {p1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object p1

    check-cast p1, LX/0i4;

    iput-object v2, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->a:LX/B3f;

    iput-object v3, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->b:LX/B3q;

    iput-object v4, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->c:LX/B3m;

    iput-object p1, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->d:LX/0i4;

    .line 1740921
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x3135f571    # -1.6948448E9f

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1740922
    const v1, 0x7f030258

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x76a3b650

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x30081ab5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1740923
    iget-object v1, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->f:LX/B3l;

    .line 1740924
    invoke-static {v1}, LX/B3l;->d(LX/B3l;)V

    .line 1740925
    iget-object v2, v1, LX/B3l;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-static {v2, v4}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1740926
    iget-object v2, v1, LX/B3l;->e:LX/B0S;

    invoke-virtual {v2}, LX/B0S;->a()V

    .line 1740927
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1740928
    const/16 v1, 0x2b

    const v2, -0x5456027d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1740929
    check-cast p1, Landroid/view/ViewGroup;

    .line 1740930
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->h:LX/B3Z;

    .line 1740931
    new-instance v1, LX/B3p;

    invoke-direct {v1, v0}, LX/B3p;-><init>(LX/B3Z;)V

    .line 1740932
    move-object v1, v1

    .line 1740933
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1740934
    const-string v2, "overlay_owner"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1740935
    new-instance v2, LX/B3j;

    invoke-direct {v2, v0}, LX/B3j;-><init>(Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V

    .line 1740936
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->a:LX/B3f;

    .line 1740937
    iget-object v3, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 1740938
    const-string v4, "photo_uri"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1740939
    new-instance v4, LX/B3e;

    invoke-direct {v4, v1, v2, v3}, LX/B3e;-><init>(LX/B3p;LX/B3j;Ljava/lang/String;)V

    .line 1740940
    const/16 v5, 0x1b

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x24e9

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x24e7

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v8

    check-cast v8, Landroid/view/LayoutInflater;

    const/16 p2, 0x259

    invoke-static {v0, p2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p2

    .line 1740941
    iput-object v5, v4, LX/B3e;->a:LX/0Or;

    iput-object v6, v4, LX/B3e;->b:LX/0Or;

    iput-object v7, v4, LX/B3e;->c:LX/0Or;

    iput-object v8, v4, LX/B3e;->d:Landroid/view/LayoutInflater;

    iput-object p2, v4, LX/B3e;->e:LX/0Ot;

    .line 1740942
    move-object v0, v4

    .line 1740943
    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->g:LX/B3e;

    .line 1740944
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->c:LX/B3m;

    iget-object v1, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->d:LX/0i4;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    sget-object v2, Lcom/facebook/heisman/category/CategoryBrowserFragment;->e:[Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/0i5;->a([Ljava/lang/String;)Z

    move-result v1

    .line 1740945
    new-instance v4, LX/B3l;

    const-class v2, LX/B0T;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/B0T;

    invoke-direct {v4, p0, v1, v2}, LX/B3l;-><init>(Lcom/facebook/heisman/category/CategoryBrowserFragment;ZLX/B0T;)V

    .line 1740946
    const/16 v2, 0x2a

    invoke-static {v0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const-class v2, LX/B3i;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/B3i;

    const-class v3, LX/B3t;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/B3t;

    .line 1740947
    iput-object v5, v4, LX/B3l;->f:LX/0Ot;

    iput-object v2, v4, LX/B3l;->g:LX/B3i;

    iput-object v3, v4, LX/B3l;->h:LX/B3t;

    .line 1740948
    move-object v0, v4

    .line 1740949
    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->f:LX/B3l;

    .line 1740950
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->f:LX/B3l;

    invoke-virtual {v0}, LX/B3l;->a()V

    .line 1740951
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->h:LX/B3Z;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f082744

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/B3Z;->b(Ljava/lang/String;)V

    .line 1740952
    const v0, 0x7f0d2bb3

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1740953
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1740954
    const v0, 0x7f0d0a6b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    .line 1740955
    const v2, 0x7f0d094d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    .line 1740956
    const v2, 0x7f082748

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->setHint(I)V

    .line 1740957
    new-instance v2, LX/B3n;

    invoke-direct {v2, p0, v1}, LX/B3n;-><init>(Lcom/facebook/heisman/category/CategoryBrowserFragment;Landroid/widget/ImageButton;)V

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1740958
    new-instance v2, LX/B3o;

    invoke-direct {v2, p0, v0}, LX/B3o;-><init>(Lcom/facebook/heisman/category/CategoryBrowserFragment;Lcom/facebook/resources/ui/FbEditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1740959
    const v0, 0x7f0d08ed

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1740960
    const v0, 0x7f0d08ee

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1740961
    new-instance v1, LX/0zw;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->j:LX/0zw;

    .line 1740962
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->i:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f0d2768

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1740963
    iget-object v1, p0, Lcom/facebook/heisman/category/CategoryBrowserFragment;->g:LX/B3e;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/B3v;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/1OM;Landroid/content/Context;)V

    .line 1740964
    return-void
.end method
