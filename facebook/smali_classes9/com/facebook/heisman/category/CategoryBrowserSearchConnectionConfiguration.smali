.class public Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;
.super LX/B0V;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0sa;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1740995
    const-class v0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;

    const-string v1, "profile_picture_overlay"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0ad;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740996
    invoke-direct {p0}, LX/B0V;-><init>()V

    .line 1740997
    iput-object p1, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->d:Ljava/lang/String;

    .line 1740998
    iput-object p2, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->e:LX/0ad;

    .line 1740999
    return-void
.end method


# virtual methods
.method public final a(LX/B0d;)LX/0zO;
    .locals 4

    .prologue
    .line 1741000
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sa;

    .line 1741001
    iget-object v1, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->e:LX/0ad;

    sget-short v2, LX/B3X;->a:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1741002
    invoke-static {}, LX/B5F;->a()LX/B5E;

    move-result-object v1

    const-string v2, "image_high_width"

    invoke-virtual {v0}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    .line 1741003
    :goto_0
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1741004
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1741005
    move-object v0, v0

    .line 1741006
    return-object v0

    .line 1741007
    :cond_0
    new-instance v1, LX/B4R;

    invoke-direct {v1}, LX/B4R;-><init>()V

    move-object v1, v1

    .line 1741008
    const-string v2, "image_high_width"

    invoke-virtual {v0}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "page_logo_size"

    invoke-static {}, LX/0wB;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "query"

    iget-object v2, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/B0d;Lcom/facebook/graphql/executor/GraphQLResult;)LX/B0N;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 1741009
    new-instance v2, LX/9JH;

    invoke-direct {v2}, LX/9JH;-><init>()V

    .line 1741010
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->e:LX/0ad;

    sget-short v1, LX/B3X;->a:S

    invoke-interface {v0, v1, v5}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1741011
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1741012
    check-cast v0, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;

    invoke-static {v0}, LX/9JH;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;

    .line 1741013
    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1741014
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1741015
    const-string v3, "category_browser_search_invalid_model"

    const-string v6, "Null pages in search query model"

    invoke-virtual {v0, v3, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741016
    new-instance v0, LX/B0i;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    .line 1741017
    :goto_0
    return-object v0

    .line 1741018
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel;->a()Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/CategoryBrowserGraphQLModels$CategoryBrowserSearchQueryModel$PagesModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v5

    .line 1741019
    :goto_1
    if-ge v3, v7, :cond_2

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;

    .line 1741020
    invoke-static {v0}, LX/B3u;->a(Lcom/facebook/heisman/protocol/ProfileOverlayCategoryGraphQLModels$ProfileOverlayCategoryPageFieldsModel;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 1741021
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v8

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v0

    const/4 v9, 0x1

    invoke-virtual {v2, v8, v0, v4, v9}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 1741022
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1741023
    :cond_2
    new-instance v0, LX/B0i;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1741024
    :cond_3
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1741025
    check-cast v0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;

    invoke-static {v0}, LX/9JH;->a(Lcom/facebook/flatbuffers/MutableFlattenable;)Lcom/facebook/flatbuffers/MutableFlattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;

    .line 1741026
    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel$RankedProfilePictureOverlaysModel;

    move-result-object v0

    if-nez v0, :cond_4

    .line 1741027
    iget-object v0, p0, Lcom/facebook/heisman/category/CategoryBrowserSearchConnectionConfiguration;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 1741028
    const-string v3, "ranked_frame_browser_search_invalid_model"

    const-string v6, "Null pages in search query model"

    invoke-virtual {v0, v3, v6}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741029
    new-instance v0, LX/B0i;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto :goto_0

    .line 1741030
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel;->a()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel$RankedProfilePictureOverlaysModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$BrowserRankedFramesQueryModel$RankedProfilePictureOverlaysModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v5

    .line 1741031
    :goto_2
    if-ge v3, v7, :cond_6

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;

    .line 1741032
    invoke-virtual {v0}, Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel;->j()Lcom/facebook/heisman/protocol/RankedFramesBrowserGraphQLModels$ProfilePictureOverlayWithContextModel$OverlayModel;

    move-result-object v8

    invoke-static {v8}, LX/5Qm;->b(LX/5QV;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1741033
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v8

    invoke-static {v0}, LX/0w5;->a(Lcom/facebook/flatbuffers/Flattenable;)LX/0w5;

    move-result-object v0

    const/4 v9, 0x6

    invoke-virtual {v2, v8, v0, v4, v9}, LX/9JH;->a(ILX/0w5;Ljava/util/Collection;I)V

    .line 1741034
    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 1741035
    :cond_6
    new-instance v0, LX/B0i;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, LX/B0i;-><init>(Lcom/facebook/flatbuffers/MutableFlattenable;LX/9JH;LX/B0d;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1741036
    const-string v0, "ProfilePictureOverlayCategoryBrowserSearch"

    return-object v0
.end method
