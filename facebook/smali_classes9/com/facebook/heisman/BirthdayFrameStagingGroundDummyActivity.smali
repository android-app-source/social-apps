.class public Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/B2z;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/B2y;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1739318
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1739335
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1739336
    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1739337
    const-string v1, "imageoverlay"

    invoke-static {v0, v1, p2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1739338
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;

    const-class v1, LX/B2z;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/B2z;

    iput-object v0, p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->p:LX/B2z;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1739329
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1739330
    invoke-static {p0, p0}, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1739331
    invoke-virtual {p0}, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1739332
    iget-object v1, p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->p:LX/B2z;

    const-string v2, "profile_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "imageoverlay"

    invoke-static {v0, v3}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    invoke-virtual {v1, p0, v2, v0}, LX/B2z;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;)LX/B2y;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->q:LX/B2y;

    .line 1739333
    iget-object v0, p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->q:LX/B2y;

    invoke-virtual {v0}, LX/B2y;->a()V

    .line 1739334
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1739324
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1739325
    invoke-virtual {p0}, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->finish()V

    .line 1739326
    :goto_0
    return-void

    .line 1739327
    :cond_0
    iget-object v0, p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->q:LX/B2y;

    invoke-virtual {v0, p0, p1, p2, p3}, LX/B2y;->a(Landroid/app/Activity;IILandroid/content/Intent;)V

    .line 1739328
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x625399c0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1739319
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1739320
    iget-object v1, p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->q:LX/B2y;

    if-eqz v1, :cond_0

    .line 1739321
    iget-object v1, p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->q:LX/B2y;

    invoke-virtual {v1}, LX/B2y;->b()V

    .line 1739322
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/heisman/BirthdayFrameStagingGroundDummyActivity;->q:LX/B2y;

    .line 1739323
    :cond_0
    const/16 v1, 0x23

    const v2, 0x2cdec270

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
