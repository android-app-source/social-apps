.class public Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

.field public final b:LX/5QV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1741530
    new-instance v0, LX/B4I;

    invoke-direct {v0}, LX/B4I;-><init>()V

    sput-object v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1741525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741526
    const-class v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    .line 1741527
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/5QV;

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->b:LX/5QV;

    .line 1741528
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c:Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    .line 1741529
    return-void
.end method

.method public constructor <init>(Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;LX/5QV;Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;)V
    .locals 0
    .param p2    # LX/5QV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1741520
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741521
    iput-object p1, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    .line 1741522
    iput-object p2, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->b:LX/5QV;

    .line 1741523
    iput-object p3, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c:Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    .line 1741524
    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1741519
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d()J
    .locals 2

    .prologue
    .line 1741518
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-wide v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->c:J

    return-wide v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1741509
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1741517
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1741516
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1741515
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->e:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 1741514
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->f:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1741510
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1741511
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->b:LX/5QV;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1741512
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCameraIntentData;->c:Lcom/facebook/heisman/protocol/FetchImageOverlayGraphQLModels$ImageOverlayCameraTitleFieldsModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1741513
    return-void
.end method
