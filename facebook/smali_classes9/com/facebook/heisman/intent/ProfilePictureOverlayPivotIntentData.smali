.class public Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

.field public final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1741576
    new-instance v0, LX/B4M;

    invoke-direct {v0}, LX/B4M;-><init>()V

    sput-object v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1741589
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741590
    const-class v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    .line 1741591
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->b:Ljava/lang/String;

    .line 1741592
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->c:Ljava/lang/String;

    .line 1741593
    return-void
.end method

.method public constructor <init>(Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1741583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741584
    iput-object p1, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    .line 1741585
    iput-object p2, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->b:Ljava/lang/String;

    .line 1741586
    iput-object p3, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->c:Ljava/lang/String;

    .line 1741587
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1741588
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1741582
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1741581
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    iget-object v0, v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1741577
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->a:Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1741578
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1741579
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1741580
    return-void
.end method
