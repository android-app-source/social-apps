.class public Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1741558
    new-instance v0, LX/B4L;

    invoke-direct {v0}, LX/B4L;-><init>()V

    sput-object v0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1741550
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741551
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->a:Ljava/lang/String;

    .line 1741552
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->b:Ljava/lang/String;

    .line 1741553
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->c:J

    .line 1741554
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->d:Ljava/lang/String;

    .line 1741555
    const-class v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->e:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1741556
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->f:I

    .line 1741557
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;I)V
    .locals 1
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/productionprompts/logging/PromptAnalytics;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1741542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1741543
    iput-object p1, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->a:Ljava/lang/String;

    .line 1741544
    iput-object p2, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->b:Ljava/lang/String;

    .line 1741545
    iput-wide p3, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->c:J

    .line 1741546
    iput-object p5, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->d:Ljava/lang/String;

    .line 1741547
    iput-object p6, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->e:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1741548
    iput p7, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->f:I

    .line 1741549
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1741541
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1741534
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1741535
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1741536
    iget-wide v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1741537
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1741538
    iget-object v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->e:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1741539
    iget v0, p0, Lcom/facebook/heisman/intent/ProfilePictureOverlayCommonParams;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1741540
    return-void
.end method
