.class public Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile h:Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/heisman/ImageOverlayUriRequestFactory;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/2U5;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/B3W;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1740291
    const-class v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1740274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1740275
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;
    .locals 9

    .prologue
    .line 1740276
    sget-object v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->h:Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    if-nez v0, :cond_1

    .line 1740277
    const-class v1, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    monitor-enter v1

    .line 1740278
    :try_start_0
    sget-object v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->h:Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1740279
    if-eqz v2, :cond_0

    .line 1740280
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1740281
    new-instance v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    invoke-direct {v3}, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;-><init>()V

    .line 1740282
    const/16 v4, 0x24ee

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x15e7

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x24dc

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const-class v7, LX/2U5;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/2U5;

    invoke-static {v0}, LX/B3W;->a(LX/0QB;)LX/B3W;

    move-result-object v8

    check-cast v8, LX/B3W;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object p0

    check-cast p0, LX/0tX;

    .line 1740283
    iput-object v4, v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->a:LX/0Or;

    iput-object v5, v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->b:LX/0Or;

    iput-object v6, v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->c:LX/0Or;

    iput-object v7, v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->e:LX/2U5;

    iput-object v8, v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->f:LX/B3W;

    iput-object p0, v3, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->g:LX/0tX;

    .line 1740284
    move-object v0, v3

    .line 1740285
    sput-object v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->h:Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1740286
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1740287
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1740288
    :cond_1
    sget-object v0, Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;->h:Lcom/facebook/heisman/ProfilePictureOverlayCameraQueryExecutor;

    return-object v0

    .line 1740289
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1740290
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
