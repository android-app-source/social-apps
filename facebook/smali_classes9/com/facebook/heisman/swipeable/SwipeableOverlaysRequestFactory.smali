.class public Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static volatile c:Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B5Q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1744368
    const-class v0, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1744369
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1744370
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;
    .locals 4

    .prologue
    .line 1744371
    sget-object v0, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->c:Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;

    if-nez v0, :cond_1

    .line 1744372
    const-class v1, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;

    monitor-enter v1

    .line 1744373
    :try_start_0
    sget-object v0, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->c:Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1744374
    if-eqz v2, :cond_0

    .line 1744375
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 1744376
    new-instance v3, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;

    invoke-direct {v3}, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;-><init>()V

    .line 1744377
    const/16 p0, 0x24ed

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 1744378
    iput-object p0, v3, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->a:LX/0Or;

    .line 1744379
    move-object v0, v3

    .line 1744380
    sput-object v0, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->c:Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1744381
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 1744382
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1744383
    :cond_1
    sget-object v0, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->c:Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;

    return-object v0

    .line 1744384
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 1744385
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0zO;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1744386
    iget-object v0, p0, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/B5Q;

    .line 1744387
    new-instance v1, LX/B4u;

    invoke-direct {v1}, LX/B4u;-><init>()V

    move-object v1, v1

    .line 1744388
    const-string v2, "image_overlay_id"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "image_high_width"

    iget-object v3, v0, LX/B5Q;->a:LX/0sa;

    invoke-virtual {v3}, LX/0sa;->y()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/B4u;

    move-object v0, v1

    .line 1744389
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/heisman/swipeable/SwipeableOverlaysRequestFactory;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 1744390
    iput-object v1, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1744391
    move-object v0, v0

    .line 1744392
    sget-object v1, LX/0zS;->a:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/16 v2, 0xe10

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    return-object v0
.end method
