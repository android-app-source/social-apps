.class public Lcom/facebook/profilelist/ProfilesListActivityConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/profilelist/ProfilesListActivityConfig;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:J

.field public final e:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1778172
    new-instance v0, LX/BMw;

    invoke-direct {v0}, LX/BMw;-><init>()V

    sput-object v0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIZJ)V
    .locals 1

    .prologue
    .line 1778165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778166
    iput p1, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->a:I

    .line 1778167
    iput p2, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->b:I

    .line 1778168
    iput p3, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->c:I

    .line 1778169
    iput-wide p5, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->d:J

    .line 1778170
    iput-boolean p4, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->e:Z

    .line 1778171
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1778158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1778159
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->a:I

    .line 1778160
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->b:I

    .line 1778161
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->c:I

    .line 1778162
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->d:J

    .line 1778163
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->e:Z

    .line 1778164
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1778157
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1778151
    iget v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1778152
    iget v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1778153
    iget v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1778154
    iget-wide v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1778155
    iget-boolean v0, p0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1778156
    return-void
.end method
