.class public Lcom/facebook/profilelist/ProfileView;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Landroid/widget/TextView;

.field public c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public d:Landroid/widget/CheckBox;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1778098
    const-class v0, Lcom/facebook/profilelist/ProfileView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/profilelist/ProfileView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1778099
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1778100
    const p1, 0x7f031091

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1778101
    const p1, 0x7f0d2616

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lcom/facebook/profilelist/ProfileView;->b:Landroid/widget/TextView;

    .line 1778102
    const p1, 0x7f0d1602

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object p1, p0, Lcom/facebook/profilelist/ProfileView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1778103
    const p1, 0x7f0d092e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/CheckBox;

    iput-object p1, p0, Lcom/facebook/profilelist/ProfileView;->d:Landroid/widget/CheckBox;

    .line 1778104
    return-void
.end method


# virtual methods
.method public setIsSelected(Z)V
    .locals 1

    .prologue
    .line 1778105
    iget-object v0, p0, Lcom/facebook/profilelist/ProfileView;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1778106
    return-void
.end method
