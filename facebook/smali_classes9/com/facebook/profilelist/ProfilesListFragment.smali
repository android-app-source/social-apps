.class public Lcom/facebook/profilelist/ProfilesListFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public b:LX/0gd;

.field public c:LX/BN0;

.field public d:Lcom/facebook/widget/listview/BetterListView;

.field public e:Landroid/widget/ImageView;

.field public f:Landroid/widget/TextView;

.field private g:Landroid/widget/TextView;

.field public h:Ljava/lang/String;

.field private i:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public j:Ljava/lang/String;

.field public k:LX/BMq;

.field private l:I

.field private m:I

.field private n:LX/BN6;

.field private final o:Landroid/database/DataSetObserver;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1778335
    const-class v0, Lcom/facebook/profilelist/ProfilesListFragment;

    sput-object v0, Lcom/facebook/profilelist/ProfilesListFragment;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1778336
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1778337
    new-instance v0, LX/BN1;

    invoke-direct {v0, p0}, LX/BN1;-><init>(Lcom/facebook/profilelist/ProfilesListFragment;)V

    iput-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->o:Landroid/database/DataSetObserver;

    return-void
.end method

.method private a(LX/0gd;LX/1Ck;LX/BN6;LX/BN0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1778338
    iput-object p1, p0, Lcom/facebook/profilelist/ProfilesListFragment;->b:LX/0gd;

    .line 1778339
    iput-object p2, p0, Lcom/facebook/profilelist/ProfilesListFragment;->i:LX/1Ck;

    .line 1778340
    iput-object p3, p0, Lcom/facebook/profilelist/ProfilesListFragment;->n:LX/BN6;

    .line 1778341
    iput-object p4, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    .line 1778342
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/profilelist/ProfilesListFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-static {v3}, LX/0gd;->a(LX/0QB;)LX/0gd;

    move-result-object v0

    check-cast v0, LX/0gd;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {v3}, LX/BN6;->a(LX/0QB;)LX/BN6;

    move-result-object v2

    check-cast v2, LX/BN6;

    new-instance p1, LX/BN0;

    invoke-static {v3}, LX/1PK;->b(LX/0QB;)Landroid/view/LayoutInflater;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    invoke-static {v3}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object v5

    check-cast v5, LX/2Rd;

    const-class v6, Landroid/content/Context;

    invoke-interface {v3, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-direct {p1, v4, v5, v6}, LX/BN0;-><init>(Landroid/view/LayoutInflater;LX/2Rd;Landroid/content/Context;)V

    move-object v3, p1

    check-cast v3, LX/BN0;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/facebook/profilelist/ProfilesListFragment;->a(LX/0gd;LX/1Ck;LX/BN6;LX/BN0;)V

    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 1778343
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    iget-object v1, p0, Lcom/facebook/profilelist/ProfilesListFragment;->o:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, LX/BN0;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1778344
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1778345
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    new-instance v1, LX/BN4;

    invoke-direct {v1, p0}, LX/BN4;-><init>(Lcom/facebook/profilelist/ProfilesListFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1778346
    return-void
.end method

.method public static e$redex0(Lcom/facebook/profilelist/ProfilesListFragment;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1778347
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    invoke-virtual {v0}, LX/BN0;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/facebook/profilelist/ProfilesListFragment;->a(ZZ)V

    .line 1778348
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->i:LX/1Ck;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/profilelist/ProfilesListFragment;->k:LX/BMq;

    iget-object v3, p0, Lcom/facebook/profilelist/ProfilesListFragment;->h:Ljava/lang/String;

    invoke-static {v3}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    invoke-interface {v2, v3}, LX/BMq;->a(LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance v3, LX/BN5;

    invoke-direct {v3, p0}, LX/BN5;-><init>(Lcom/facebook/profilelist/ProfilesListFragment;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1778349
    return-void

    .line 1778350
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1778351
    const v0, 0x7f0d0d65

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1778352
    iget v1, p0, Lcom/facebook/profilelist/ProfilesListFragment;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 1778353
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1778263
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1778264
    const-class v0, Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-static {v0, p0}, Lcom/facebook/profilelist/ProfilesListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1778265
    return-void
.end method

.method public final a(Lcom/facebook/profilelist/ProfilesListActivityConfig;)V
    .locals 8

    .prologue
    .line 1778322
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->n:LX/BN6;

    iget-wide v2, p1, Lcom/facebook/profilelist/ProfilesListActivityConfig;->d:J

    .line 1778323
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-nez v4, :cond_0

    .line 1778324
    iget-object v4, v0, LX/BN6;->a:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BMq;

    .line 1778325
    :goto_0
    move-object v0, v4

    .line 1778326
    iput-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->k:LX/BMq;

    .line 1778327
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    iget-boolean v1, p1, Lcom/facebook/profilelist/ProfilesListActivityConfig;->e:Z

    .line 1778328
    iput-boolean v1, v0, LX/BN0;->f:Z

    .line 1778329
    iget v0, p1, Lcom/facebook/profilelist/ProfilesListActivityConfig;->b:I

    iput v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->l:I

    .line 1778330
    iget v0, p1, Lcom/facebook/profilelist/ProfilesListActivityConfig;->c:I

    iput v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->m:I

    .line 1778331
    return-void

    .line 1778332
    :cond_0
    iget-object v4, v0, LX/BN6;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BMu;

    .line 1778333
    iput-wide v2, v4, LX/BMu;->e:J

    .line 1778334
    goto :goto_0
.end method

.method public final a(Ljava/util/HashSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashSet",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1778319
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    .line 1778320
    iput-object p1, v0, LX/BN0;->e:Ljava/util/Set;

    .line 1778321
    return-void
.end method

.method public final a(ZZ)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/16 v4, 0x8

    const/4 v1, 0x0

    .line 1778304
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    move v5, v0

    .line 1778305
    :goto_0
    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    .line 1778306
    :goto_1
    iget-object v2, p0, Lcom/facebook/profilelist/ProfilesListFragment;->h:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    iget v2, p0, Lcom/facebook/profilelist/ProfilesListFragment;->l:I

    .line 1778307
    :goto_2
    iget-object v6, p0, Lcom/facebook/profilelist/ProfilesListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    if-eqz p2, :cond_5

    move v3, v1

    :goto_3
    invoke-virtual {v6, v3}, Lcom/facebook/widget/listview/BetterListView;->setVisibility(I)V

    .line 1778308
    iget-object v6, p0, Lcom/facebook/profilelist/ProfilesListFragment;->g:Landroid/widget/TextView;

    if-eqz v5, :cond_6

    move v3, v1

    :goto_4
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1778309
    iget-object v3, p0, Lcom/facebook/profilelist/ProfilesListFragment;->g:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(I)V

    .line 1778310
    const v2, 0x7f0d0d66

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v3

    if-eqz v0, :cond_7

    move v2, v1

    :goto_5
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1778311
    const v2, 0x1020004

    invoke-virtual {p0, v2}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    if-nez v5, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v4, v1

    :cond_1
    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1778312
    return-void

    :cond_2
    move v5, v1

    .line 1778313
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1778314
    goto :goto_1

    .line 1778315
    :cond_4
    iget v2, p0, Lcom/facebook/profilelist/ProfilesListFragment;->m:I

    goto :goto_2

    :cond_5
    move v3, v4

    .line 1778316
    goto :goto_3

    :cond_6
    move v3, v4

    .line 1778317
    goto :goto_4

    :cond_7
    move v2, v4

    .line 1778318
    goto :goto_5
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1778296
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 1778297
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1778298
    const-string v1, "full_profiles"

    iget-object v2, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    .line 1778299
    iget-object v3, v2, LX/BN0;->e:Ljava/util/Set;

    move-object v2, v3

    .line 1778300
    invoke-static {v2}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1778301
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1778302
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1778303
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3edb1b6

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1778282
    const v0, 0x7f031066

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1778283
    const v0, 0x102000a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->d:Lcom/facebook/widget/listview/BetterListView;

    .line 1778284
    if-eqz p3, :cond_0

    .line 1778285
    const-string v0, "full_profiles"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1778286
    iget-object v3, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    invoke-static {v0}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    .line 1778287
    iput-object v0, v3, LX/BN0;->e:Ljava/util/Set;

    .line 1778288
    :cond_0
    invoke-direct {p0}, Lcom/facebook/profilelist/ProfilesListFragment;->d()V

    .line 1778289
    const v0, 0x7f0d2749

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->f:Landroid/widget/TextView;

    .line 1778290
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->f:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/facebook/profilelist/ProfilesListFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1778291
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->f:Landroid/widget/TextView;

    new-instance v3, LX/BN2;

    invoke-direct {v3, p0}, LX/BN2;-><init>(Lcom/facebook/profilelist/ProfilesListFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1778292
    const v0, 0x7f0d274a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->e:Landroid/widget/ImageView;

    .line 1778293
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->e:Landroid/widget/ImageView;

    new-instance v3, LX/BN3;

    invoke-direct {v3, p0}, LX/BN3;-><init>(Lcom/facebook/profilelist/ProfilesListFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1778294
    const v0, 0x7f0d0d65

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/profilelist/ProfilesListFragment;->g:Landroid/widget/TextView;

    .line 1778295
    const/16 v0, 0x2b

    const v3, 0x238804f6

    invoke-static {v4, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x374dd842

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1778279
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1778280
    iget-object v1, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    iget-object v2, p0, Lcom/facebook/profilelist/ProfilesListFragment;->o:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, LX/BN0;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1778281
    const/16 v1, 0x2b

    const v2, -0x28a9b810

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x654350e1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1778276
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1778277
    iget-object v1, p0, Lcom/facebook/profilelist/ProfilesListFragment;->i:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1778278
    const/16 v1, 0x2b

    const v2, -0x19f79c9e

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x60054694

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1778271
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1778272
    invoke-direct {p0}, Lcom/facebook/profilelist/ProfilesListFragment;->k()V

    .line 1778273
    iget-object v1, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    invoke-virtual {v1}, LX/BN0;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1778274
    invoke-static {p0}, Lcom/facebook/profilelist/ProfilesListFragment;->e$redex0(Lcom/facebook/profilelist/ProfilesListFragment;)V

    .line 1778275
    :cond_0
    const/16 v1, 0x2b

    const v2, -0x7754bf4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1778266
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1778267
    const-string v0, "full_profiles"

    iget-object v1, p0, Lcom/facebook/profilelist/ProfilesListFragment;->c:LX/BN0;

    .line 1778268
    iget-object p0, v1, LX/BN0;->e:Ljava/util/Set;

    move-object v1, p0

    .line 1778269
    invoke-static {v1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1778270
    return-void
.end method
