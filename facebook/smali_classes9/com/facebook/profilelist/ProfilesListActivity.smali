.class public final Lcom/facebook/profilelist/ProfilesListActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/profilelist/ProfilesListFragment;

.field public q:LX/63Z;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1778110
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;JLX/0Px;Ljava/lang/String;)Landroid/content/Intent;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "J",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 1778141
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/profilelist/ProfilesListActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1778142
    const-string v1, "full_profiles"

    invoke-static {p3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 1778143
    const-string v8, "config"

    new-instance v1, Lcom/facebook/profilelist/ProfilesListActivityConfig;

    const v2, 0x7f081475

    const v3, 0x7f081478

    const v4, 0x7f081477

    const/4 v5, 0x0

    move-wide v6, p1

    invoke-direct/range {v1 .. v7}, Lcom/facebook/profilelist/ProfilesListActivityConfig;-><init>(IIIZJ)V

    invoke-virtual {v0, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1778144
    const-string v1, "extra_composer_session_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1778145
    return-object v0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/profilelist/ProfilesListActivity;

    invoke-static {v0}, LX/63Z;->a(LX/0QB;)LX/63Z;

    move-result-object v0

    check-cast v0, LX/63Z;

    iput-object v0, p0, Lcom/facebook/profilelist/ProfilesListActivity;->q:LX/63Z;

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1778111
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1778112
    invoke-static {p0, p0}, Lcom/facebook/profilelist/ProfilesListActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1778113
    const v0, 0x7f031064

    invoke-virtual {p0, v0}, Lcom/facebook/profilelist/ProfilesListActivity;->setContentView(I)V

    .line 1778114
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1778115
    invoke-virtual {p0}, Lcom/facebook/profilelist/ProfilesListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 1778116
    const-string v0, "config"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/profilelist/ProfilesListActivityConfig;

    .line 1778117
    const v1, 0x7f0d00bc

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->c(I)LX/0am;

    move-result-object v1

    .line 1778118
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1778119
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 1778120
    iget v3, v0, Lcom/facebook/profilelist/ProfilesListActivityConfig;->a:I

    invoke-interface {v1, v3}, LX/0h5;->setTitle(I)V

    .line 1778121
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v3

    const/4 v4, 0x1

    .line 1778122
    iput v4, v3, LX/108;->a:I

    .line 1778123
    move-object v3, v3

    .line 1778124
    const v4, 0x7f081397

    invoke-virtual {p0, v4}, Lcom/facebook/profilelist/ProfilesListActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1778125
    iput-object v4, v3, LX/108;->g:Ljava/lang/String;

    .line 1778126
    move-object v3, v3

    .line 1778127
    const/4 v4, -0x2

    .line 1778128
    iput v4, v3, LX/108;->h:I

    .line 1778129
    move-object v3, v3

    .line 1778130
    invoke-virtual {v3}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v3

    .line 1778131
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-interface {v1, v3}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1778132
    new-instance v3, LX/BMv;

    invoke-direct {v3, p0}, LX/BMv;-><init>(Lcom/facebook/profilelist/ProfilesListActivity;)V

    invoke-interface {v1, v3}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1778133
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    .line 1778134
    const v3, 0x7f0d2748

    invoke-virtual {v1, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/profilelist/ProfilesListFragment;

    iput-object v1, p0, Lcom/facebook/profilelist/ProfilesListActivity;->p:Lcom/facebook/profilelist/ProfilesListFragment;

    .line 1778135
    const-string v1, "full_profiles"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1778136
    iget-object v3, p0, Lcom/facebook/profilelist/ProfilesListActivity;->p:Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-virtual {v3, v0}, Lcom/facebook/profilelist/ProfilesListFragment;->a(Lcom/facebook/profilelist/ProfilesListActivityConfig;)V

    .line 1778137
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListActivity;->p:Lcom/facebook/profilelist/ProfilesListFragment;

    invoke-static {v1}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/profilelist/ProfilesListFragment;->a(Ljava/util/HashSet;)V

    .line 1778138
    iget-object v0, p0, Lcom/facebook/profilelist/ProfilesListActivity;->p:Lcom/facebook/profilelist/ProfilesListFragment;

    const-string v1, "extra_composer_internal_session_id"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1778139
    iput-object v1, v0, Lcom/facebook/profilelist/ProfilesListFragment;->j:Ljava/lang/String;

    .line 1778140
    return-void
.end method
