.class public Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/Bi1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final e:Landroid/view/View$OnClickListener;

.field private final f:Landroid/view/View$OnClickListener;

.field public g:LX/Bi2;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/Bi8;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1810049
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1810050
    new-instance v0, LX/BiA;

    invoke-direct {v0, p0}, LX/BiA;-><init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V

    iput-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->e:Landroid/view/View$OnClickListener;

    .line 1810051
    new-instance v0, LX/BiD;

    invoke-direct {v0, p0}, LX/BiD;-><init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V

    iput-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->f:Landroid/view/View$OnClickListener;

    .line 1810052
    invoke-direct {p0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->b()V

    .line 1810053
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1810044
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1810045
    new-instance v0, LX/BiA;

    invoke-direct {v0, p0}, LX/BiA;-><init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V

    iput-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->e:Landroid/view/View$OnClickListener;

    .line 1810046
    new-instance v0, LX/BiD;

    invoke-direct {v0, p0}, LX/BiD;-><init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V

    iput-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->f:Landroid/view/View$OnClickListener;

    .line 1810047
    invoke-direct {p0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->b()V

    .line 1810048
    return-void
.end method

.method private static a(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;LX/Bi1;LX/0tX;Landroid/content/res/Resources;LX/1Ck;)V
    .locals 0

    .prologue
    .line 1810043
    iput-object p1, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a:LX/Bi1;

    iput-object p2, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->b:LX/0tX;

    iput-object p3, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->c:Landroid/content/res/Resources;

    iput-object p4, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->d:LX/1Ck;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-static {v3}, LX/Bi1;->a(LX/0QB;)LX/Bi1;

    move-result-object v0

    check-cast v0, LX/Bi1;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v3}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;LX/Bi1;LX/0tX;Landroid/content/res/Resources;LX/1Ck;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1810038
    const-class v0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;

    invoke-static {v0, p0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1810039
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020337

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 1810040
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1810041
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1810042
    return-void
.end method

.method public static c(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V
    .locals 15

    .prologue
    .line 1810054
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->removeAllViews()V

    .line 1810055
    iget-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    invoke-virtual {v0}, LX/Bi8;->getCount()I

    move-result v6

    .line 1810056
    const/4 v4, 0x0

    :goto_0
    if-ge v4, v6, :cond_1

    .line 1810057
    iget-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v1, p0}, LX/1Cv;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 1810058
    instance-of v0, v7, LX/BiF;

    if-eqz v0, :cond_0

    .line 1810059
    iget-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a:LX/Bi1;

    iget-object v1, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    invoke-virtual {v1}, LX/Bi8;->c()Ljava/lang/String;

    move-result-object v1

    .line 1810060
    sget-object v9, LX/Bi0;->IMPRESSION:LX/Bi0;

    sget-object v10, LX/Bi3;->PAGE_HEADER:LX/Bi3;

    const-string v11, "expand"

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v14

    move-object v8, v0

    move-object v12, v1

    move v13, v4

    invoke-static/range {v8 .. v14}, LX/Bi1;->a(LX/Bi1;LX/Bi0;LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V

    .line 1810061
    const v0, 0x7f0d00df

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1810062
    iget-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->f:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810063
    :goto_1
    invoke-virtual {p0, v7}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->addView(Landroid/view/View;)V

    .line 1810064
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1810065
    :cond_0
    iget-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    invoke-virtual {v0, v4}, LX/Bi8;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;

    .line 1810066
    invoke-virtual {v0}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    move-result-object v5

    .line 1810067
    const v1, 0x7f0d00de

    invoke-virtual {v7, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1810068
    const v0, 0x7f0d00df

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1810069
    iget-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810070
    iget-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->a:LX/Bi1;

    iget-object v1, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    .line 1810071
    iget-object v2, v1, LX/Bi8;->f:LX/Bi3;

    move-object v1, v2

    .line 1810072
    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->d()Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLEntityCardContextItemType;->name()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    invoke-virtual {v3}, LX/Bi8;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, LX/Bi1;->b(LX/Bi3;Ljava/lang/String;Ljava/lang/String;ILX/0am;)V

    goto :goto_1

    .line 1810073
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1810034
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 1810035
    invoke-virtual {p0, v0}, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810036
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1810037
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1810031
    iput-object p1, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->i:Ljava/lang/String;

    .line 1810032
    iput-object p2, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->j:Ljava/lang/String;

    .line 1810033
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x555119ab

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1810027
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onDetachedFromWindow()V

    .line 1810028
    iget-object v1, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->d:LX/1Ck;

    if-eqz v1, :cond_0

    .line 1810029
    iget-object v1, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1810030
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x484f10f4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAdapter(LX/Bi8;)V
    .locals 2

    .prologue
    .line 1810024
    iput-object p1, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    .line 1810025
    iget-object v0, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->h:LX/Bi8;

    new-instance v1, LX/Bi9;

    invoke-direct {v1, p0}, LX/Bi9;-><init>(Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;)V

    invoke-virtual {v0, v1}, LX/Bi8;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1810026
    return-void
.end method

.method public setOnItemClickListener(LX/Bi2;)V
    .locals 0

    .prologue
    .line 1810022
    iput-object p1, p0, Lcom/facebook/entitycards/contextitems/ui/ContextItemsContainer;->g:LX/Bi2;

    .line 1810023
    return-void
.end method
