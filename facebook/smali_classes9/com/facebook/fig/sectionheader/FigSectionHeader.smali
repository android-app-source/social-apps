.class public Lcom/facebook/fig/sectionheader/FigSectionHeader;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/23P;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private k:LX/6WW;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1717959
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1717960
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->a(Landroid/util/AttributeSet;I)V

    .line 1717961
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1717962
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1717963
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->a(Landroid/util/AttributeSet;I)V

    .line 1717964
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1717965
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1717966
    invoke-direct {p0, p2, p3}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->a(Landroid/util/AttributeSet;I)V

    .line 1717967
    return-void
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v2, -0x2

    const/4 v4, 0x0

    .line 1717968
    const-class v0, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    invoke-static {v0, p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1717969
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    .line 1717970
    new-instance v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1717971
    new-instance v1, LX/1au;

    invoke-direct {v1, v2, v2}, LX/1au;-><init>(II)V

    .line 1717972
    iput-boolean v5, v1, LX/1au;->b:Z

    .line 1717973
    const/16 v2, 0x11

    iput v2, v1, LX/1au;->d:I

    .line 1717974
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e011e

    invoke-virtual {v0, v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1717975
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0123

    invoke-virtual {v0, v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1717976
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1717977
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b19ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1717978
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1a00

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1717979
    invoke-super {p0, v0, v0, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setPadding(IIII)V

    .line 1717980
    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1717981
    invoke-super {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1717982
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 1717983
    iget-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {v0, v5}, LX/6WW;->b(I)V

    .line 1717984
    iget-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0123

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1717985
    if-eqz p1, :cond_1

    .line 1717986
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FigSectionHeader:[I

    invoke-virtual {v0, p1, v1, p2, v4}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1717987
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1717988
    if-lez v1, :cond_2

    .line 1717989
    invoke-virtual {p0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(I)V

    .line 1717990
    :goto_0
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1717991
    if-lez v1, :cond_3

    .line 1717992
    invoke-virtual {p0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setActionText(I)V

    .line 1717993
    :goto_1
    iget-object v1, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1717994
    iget-object v1, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1717995
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1717996
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717997
    return-void

    .line 1717998
    :cond_2
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1717999
    invoke-virtual {p0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1718000
    :cond_3
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1718001
    invoke-virtual {p0, v1}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setActionText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->j:LX/23P;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1718002
    iget-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->c(I)V

    .line 1718003
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1718004
    iget-object v1, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 1718005
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(II)V

    .line 1718006
    return-void
.end method

.method public final a(IIII)V
    .locals 2

    .prologue
    .line 1718007
    iget-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v1

    invoke-virtual {v0, v1, p1, p2, p3}, LX/6WW;->a(ZIII)V

    .line 1718008
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1717954
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1717955
    iget-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1717956
    return-void
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1717957
    iget-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1717958
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1717953
    iget-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setActionContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1717951
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717952
    return-void
.end method

.method public setActionOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1717949
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1717950
    return-void
.end method

.method public setActionText(I)V
    .locals 1

    .prologue
    .line 1717947
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1717948
    return-void
.end method

.method public setActionText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1717945
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1717946
    return-void
.end method

.method public final setPadding(IIII)V
    .locals 0

    .prologue
    .line 1717944
    return-void
.end method

.method public setThumbnailPadding(I)V
    .locals 0

    .prologue
    .line 1717943
    return-void
.end method

.method public setThumbnailSize(I)V
    .locals 0

    .prologue
    .line 1717942
    return-void
.end method

.method public setTitleText(I)V
    .locals 1

    .prologue
    .line 1717935
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1717936
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 1717937
    iget-object v0, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->k:LX/6WW;

    iget-object v1, p0, Lcom/facebook/fig/sectionheader/FigSectionHeader;->j:LX/23P;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1717938
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717939
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->requestLayout()V

    .line 1717940
    invoke-virtual {p0}, Lcom/facebook/fig/sectionheader/FigSectionHeader;->invalidate()V

    .line 1717941
    return-void
.end method
