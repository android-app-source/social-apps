.class public Lcom/facebook/fig/footer/FigFooter;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/23P;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private final k:LX/6WW;

.field private l:I
    .annotation build Lcom/facebook/fig/footer/FigFooter$FooterType;
    .end annotation
.end field

.field private m:I
    .annotation build Lcom/facebook/fig/footer/FigFooter$ActionType;
    .end annotation
.end field

.field private n:Landroid/graphics/drawable/Drawable;

.field private o:Z

.field private p:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1717672
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1717673
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    .line 1717674
    iput v1, p0, Lcom/facebook/fig/footer/FigFooter;->l:I

    .line 1717675
    iput v1, p0, Lcom/facebook/fig/footer/FigFooter;->m:I

    .line 1717676
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/footer/FigFooter;->a(Landroid/util/AttributeSet;I)V

    .line 1717677
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1717678
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1717679
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    .line 1717680
    iput v1, p0, Lcom/facebook/fig/footer/FigFooter;->l:I

    .line 1717681
    iput v1, p0, Lcom/facebook/fig/footer/FigFooter;->m:I

    .line 1717682
    invoke-direct {p0, p2, v1}, Lcom/facebook/fig/footer/FigFooter;->a(Landroid/util/AttributeSet;I)V

    .line 1717683
    return-void
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1717684
    const-class v0, Lcom/facebook/fig/footer/FigFooter;

    invoke-static {v0, p0}, Lcom/facebook/fig/footer/FigFooter;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1717685
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0125

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1717686
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {v0, v4}, LX/6WW;->b(I)V

    .line 1717687
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a1c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1717688
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1a1d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1717689
    invoke-super {p0, v0, v0, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setPadding(IIII)V

    .line 1717690
    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1717691
    invoke-super {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1717692
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0097

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/fig/footer/FigFooter;->setBackgroundColor(I)V

    .line 1717693
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 1717694
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->p:Landroid/graphics/Paint;

    .line 1717695
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->p:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1717696
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->p:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1a1e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1717697
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->p:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a009f

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1717698
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1717699
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 1717700
    if-eqz p1, :cond_1

    .line 1717701
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FigFooter:[I

    invoke-virtual {v0, p1, v1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1717702
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/fig/footer/FigFooter;->o:Z

    .line 1717703
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1717704
    invoke-virtual {p0, v1}, Lcom/facebook/fig/footer/FigFooter;->setFooterType(I)V

    .line 1717705
    const/16 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1717706
    if-lez v1, :cond_2

    .line 1717707
    invoke-virtual {p0, v1}, Lcom/facebook/fig/footer/FigFooter;->setTitleText(I)V

    .line 1717708
    :goto_0
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1717709
    if-eqz v1, :cond_0

    .line 1717710
    invoke-virtual {p0, v1}, Lcom/facebook/fig/footer/FigFooter;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1717711
    :cond_0
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1717712
    invoke-virtual {p0, v1}, Lcom/facebook/fig/footer/FigFooter;->setActionType(I)V

    .line 1717713
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1717714
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/footer/FigFooter;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717715
    return-void

    .line 1717716
    :cond_2
    const/16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1717717
    invoke-virtual {p0, v1}, Lcom/facebook/fig/footer/FigFooter;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fig/footer/FigFooter;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fig/footer/FigFooter;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->j:LX/23P;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1717730
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->c(I)V

    .line 1717731
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1717732
    iget-object v1, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 1717733
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(II)V

    .line 1717734
    return-void
.end method

.method public final a(IIII)V
    .locals 2

    .prologue
    .line 1717718
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v1

    invoke-virtual {v0, v1, p1, p2, p3}, LX/6WW;->a(ZIII)V

    .line 1717719
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1717720
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1717721
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1717722
    return-void
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1717723
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1717724
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1717725
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1717726
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1717727
    iget-boolean v0, p0, Lcom/facebook/fig/footer/FigFooter;->o:Z

    if-eqz v0, :cond_0

    .line 1717728
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, Lcom/facebook/fig/footer/FigFooter;->p:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1717729
    :cond_0
    return-void
.end method

.method public setActionContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1717665
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1717666
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717667
    :cond_0
    return-void
.end method

.method public setActionDrawable(I)V
    .locals 1

    .prologue
    .line 1717668
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->n:Landroid/graphics/drawable/Drawable;

    .line 1717669
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717670
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717671
    return-void
.end method

.method public setActionDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1717642
    iput-object p1, p0, Lcom/facebook/fig/footer/FigFooter;->n:Landroid/graphics/drawable/Drawable;

    .line 1717643
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717644
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717645
    return-void
.end method

.method public setActionOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1717595
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1717596
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1717597
    :cond_0
    return-void
.end method

.method public setActionText(I)V
    .locals 1

    .prologue
    .line 1717598
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1717599
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717600
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717601
    return-void
.end method

.method public setActionText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1717602
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1717603
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717604
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717605
    return-void
.end method

.method public setActionType(I)V
    .locals 5
    .param p1    # I
        .annotation build Lcom/facebook/fig/footer/FigFooter$ActionType;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x2

    const/4 v4, 0x0

    .line 1717606
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a1d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1717607
    iget v1, p0, Lcom/facebook/fig/footer/FigFooter;->m:I

    if-eq p1, v1, :cond_1

    .line 1717608
    iput p1, p0, Lcom/facebook/fig/footer/FigFooter;->m:I

    .line 1717609
    new-instance v1, LX/1au;

    invoke-direct {v1, v2, v2}, LX/1au;-><init>(II)V

    .line 1717610
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/1au;->b:Z

    .line 1717611
    const/16 v2, 0x11

    iput v2, v1, LX/1au;->d:I

    .line 1717612
    iput v4, v1, LX/1au;->leftMargin:I

    .line 1717613
    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1717614
    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-super {p0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeView(Landroid/view/View;)V

    .line 1717615
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1717616
    :cond_1
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717617
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717618
    return-void

    .line 1717619
    :pswitch_1
    iput v0, v1, LX/1au;->height:I

    .line 1717620
    iput v0, v1, LX/1au;->width:I

    .line 1717621
    new-instance v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 1717622
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00a2

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1717623
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0207df

    invoke-static {v2, v3}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1717624
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1717625
    :pswitch_2
    iput v0, v1, LX/1au;->height:I

    .line 1717626
    iput v0, v1, LX/1au;->width:I

    .line 1717627
    new-instance v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 1717628
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a008f

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1717629
    iget-object v2, p0, Lcom/facebook/fig/footer/FigFooter;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1717630
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setFooterType(I)V
    .locals 5
    .param p1    # I
        .annotation build Lcom/facebook/fig/footer/FigFooter$FooterType;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 1717631
    iget v0, p0, Lcom/facebook/fig/footer/FigFooter;->l:I

    if-eq v0, p1, :cond_0

    .line 1717632
    iput p1, p0, Lcom/facebook/fig/footer/FigFooter;->l:I

    .line 1717633
    packed-switch p1, :pswitch_data_0

    .line 1717634
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717635
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717636
    :cond_0
    return-void

    .line 1717637
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0125

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 1717638
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0123

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1717639
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    iget-object v1, p0, Lcom/facebook/fig/footer/FigFooter;->j:LX/23P;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08274d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1717640
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0123

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1717641
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    iget-object v1, p0, Lcom/facebook/fig/footer/FigFooter;->j:LX/23P;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08274e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setPadding(IIII)V
    .locals 0

    .prologue
    .line 1717664
    return-void
.end method

.method public setThumbnailPadding(I)V
    .locals 0

    .prologue
    .line 1717646
    return-void
.end method

.method public setThumbnailSize(I)V
    .locals 0

    .prologue
    .line 1717647
    return-void
.end method

.method public setTitleText(I)V
    .locals 4

    .prologue
    .line 1717648
    iget v0, p0, Lcom/facebook/fig/footer/FigFooter;->l:I

    if-nez v0, :cond_0

    .line 1717649
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    iget-object v1, p0, Lcom/facebook/fig/footer/FigFooter;->j:LX/23P;

    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1717650
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/footer/FigFooter;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717651
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717652
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717653
    :cond_0
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 1717654
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/facebook/fig/footer/FigFooter;->l:I

    if-nez v0, :cond_0

    .line 1717655
    iget-object v0, p0, Lcom/facebook/fig/footer/FigFooter;->k:LX/6WW;

    iget-object v1, p0, Lcom/facebook/fig/footer/FigFooter;->j:LX/23P;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1717656
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/footer/FigFooter;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717657
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717658
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717659
    :cond_0
    return-void
.end method

.method public setTopDivider(Z)V
    .locals 0

    .prologue
    .line 1717660
    iput-boolean p1, p0, Lcom/facebook/fig/footer/FigFooter;->o:Z

    .line 1717661
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->requestLayout()V

    .line 1717662
    invoke-virtual {p0}, Lcom/facebook/fig/footer/FigFooter;->invalidate()V

    .line 1717663
    return-void
.end method
