.class public Lcom/facebook/fig/header/FigHeader;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/23P;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private k:LX/6WW;

.field private l:I
    .annotation build Lcom/facebook/fig/header/FigHeader$ActionType;
    .end annotation
.end field

.field private m:Landroid/graphics/drawable/Drawable;

.field private n:Ljava/lang/CharSequence;

.field private o:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1717797
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 1717798
    iput v1, p0, Lcom/facebook/fig/header/FigHeader;->l:I

    .line 1717799
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/header/FigHeader;->a(Landroid/util/AttributeSet;I)V

    .line 1717800
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1717847
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1717848
    iput v0, p0, Lcom/facebook/fig/header/FigHeader;->l:I

    .line 1717849
    invoke-direct {p0, p2, v0}, Lcom/facebook/fig/header/FigHeader;->a(Landroid/util/AttributeSet;I)V

    .line 1717850
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1717843
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1717844
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/fig/header/FigHeader;->l:I

    .line 1717845
    invoke-direct {p0, p2, p3}, Lcom/facebook/fig/header/FigHeader;->a(Landroid/util/AttributeSet;I)V

    .line 1717846
    return-void
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1717812
    const-class v0, Lcom/facebook/fig/header/FigHeader;

    invoke-static {v0, p0}, Lcom/facebook/fig/header/FigHeader;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1717813
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    .line 1717814
    iget-object v0, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0129

    invoke-virtual {v0, v1, v2}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1717815
    iget-object v0, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/6WW;->b(I)V

    .line 1717816
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a1f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1717817
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1a20

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1717818
    invoke-super {p0, v0, v0, v0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setPadding(IIII)V

    .line 1717819
    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1717820
    invoke-super {p0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailSize(I)V

    .line 1717821
    iput v1, p0, Lcom/facebook/fig/header/FigHeader;->o:I

    .line 1717822
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0097

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/facebook/fig/header/FigHeader;->setBackgroundColor(I)V

    .line 1717823
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setAuxViewPadding(I)V

    .line 1717824
    if-eqz p1, :cond_1

    .line 1717825
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FigHeader:[I

    invoke-virtual {v0, p1, v1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1717826
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1717827
    if-lez v1, :cond_2

    .line 1717828
    invoke-virtual {p0, v1}, Lcom/facebook/fig/header/FigHeader;->setTitleText(I)V

    .line 1717829
    :goto_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1717830
    if-eqz v1, :cond_0

    .line 1717831
    invoke-virtual {p0, v1}, Lcom/facebook/fig/header/FigHeader;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1717832
    :cond_0
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1717833
    if-lez v1, :cond_3

    .line 1717834
    invoke-virtual {p0, v1}, Lcom/facebook/fig/header/FigHeader;->setActionText(I)V

    .line 1717835
    :goto_1
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    .line 1717836
    invoke-virtual {p0, v1}, Lcom/facebook/fig/header/FigHeader;->setActionType(I)V

    .line 1717837
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1717838
    :cond_1
    return-void

    .line 1717839
    :cond_2
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1717840
    invoke-virtual {p0, v1}, Lcom/facebook/fig/header/FigHeader;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1717841
    :cond_3
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1717842
    invoke-virtual {p0, v1}, Lcom/facebook/fig/header/FigHeader;->setActionText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fig/header/FigHeader;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fig/header/FigHeader;

    invoke-static {v0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v0

    check-cast v0, LX/23P;

    iput-object v0, p0, Lcom/facebook/fig/header/FigHeader;->j:LX/23P;

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1717807
    iget-object v0, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->c(I)V

    .line 1717808
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 1717809
    iget-object v1, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 1717810
    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->b(II)V

    .line 1717811
    return-void
.end method

.method public final a(IIII)V
    .locals 3

    .prologue
    .line 1717804
    iget v0, p0, Lcom/facebook/fig/header/FigHeader;->o:I

    iget-object v1, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {v1}, LX/6WW;->c()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    .line 1717805
    iget-object v1, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->jx_()Z

    move-result v2

    add-int/2addr v0, p2

    invoke-virtual {v1, v2, p1, v0, p3}, LX/6WW;->a(ZIII)V

    .line 1717806
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1717801
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1717802
    iget-object v0, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1717803
    return-void
.end method

.method public final dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 1

    .prologue
    .line 1717795
    iget-object v0, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 1717796
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    move-result v0

    return v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1717794
    iget-object v0, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public setActionContentDescription(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1717851
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1717852
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717853
    :cond_0
    return-void
.end method

.method public setActionDrawable(I)V
    .locals 1

    .prologue
    .line 1717735
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fig/header/FigHeader;->m:Landroid/graphics/drawable/Drawable;

    .line 1717736
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->requestLayout()V

    .line 1717737
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->invalidate()V

    .line 1717738
    return-void
.end method

.method public setActionDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1717739
    iput-object p1, p0, Lcom/facebook/fig/header/FigHeader;->m:Landroid/graphics/drawable/Drawable;

    .line 1717740
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->requestLayout()V

    .line 1717741
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->invalidate()V

    .line 1717742
    return-void
.end method

.method public setActionOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1717791
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1717792
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1717793
    :cond_0
    return-void
.end method

.method public setActionText(I)V
    .locals 1

    .prologue
    .line 1717743
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/header/FigHeader;->setActionText(Ljava/lang/CharSequence;)V

    .line 1717744
    return-void
.end method

.method public setActionText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1717745
    iput-object p1, p0, Lcom/facebook/fig/header/FigHeader;->n:Ljava/lang/CharSequence;

    .line 1717746
    iget v0, p0, Lcom/facebook/fig/header/FigHeader;->l:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1717747
    iget-object v0, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1717748
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->requestLayout()V

    .line 1717749
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->invalidate()V

    .line 1717750
    return-void
.end method

.method public setActionType(I)V
    .locals 5
    .param p1    # I
        .annotation build Lcom/facebook/fig/header/FigHeader$ActionType;
        .end annotation
    .end param

    .prologue
    const/4 v2, -0x2

    const/4 v4, 0x0

    .line 1717751
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1a21

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1717752
    new-instance v1, LX/1au;

    invoke-direct {v1, v2, v2}, LX/1au;-><init>(II)V

    .line 1717753
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/1au;->b:Z

    .line 1717754
    const/16 v2, 0x11

    iput v2, v1, LX/1au;->d:I

    .line 1717755
    iput v4, v1, LX/1au;->leftMargin:I

    .line 1717756
    iget v2, p0, Lcom/facebook/fig/header/FigHeader;->l:I

    if-eq p1, v2, :cond_1

    .line 1717757
    iput p1, p0, Lcom/facebook/fig/header/FigHeader;->l:I

    .line 1717758
    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1717759
    iget-object v2, p0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    invoke-super {p0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->removeView(Landroid/view/View;)V

    .line 1717760
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1717761
    :cond_1
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->requestLayout()V

    .line 1717762
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->invalidate()V

    .line 1717763
    return-void

    .line 1717764
    :pswitch_1
    new-instance v0, Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1717765
    iget-object v2, p0, Lcom/facebook/fig/header/FigHeader;->n:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1717766
    iget-object v2, p0, Lcom/facebook/fig/header/FigHeader;->j:LX/23P;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 1717767
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0125

    invoke-virtual {v0, v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 1717768
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1717769
    :pswitch_2
    iput v0, v1, LX/1au;->height:I

    .line 1717770
    iput v0, v1, LX/1au;->width:I

    .line 1717771
    new-instance v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 1717772
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00a6

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1717773
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f020722

    invoke-static {v2, v3}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1717774
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 1717775
    :pswitch_3
    iput v0, v1, LX/1au;->height:I

    .line 1717776
    iput v0, v1, LX/1au;->width:I

    .line 1717777
    new-instance v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;)V

    .line 1717778
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0a00a6

    invoke-static {v2, v3}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1717779
    iget-object v2, p0, Lcom/facebook/fig/header/FigHeader;->m:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1717780
    invoke-super {p0, v0, v4, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setPadding(IIII)V
    .locals 0

    .prologue
    .line 1717781
    return-void
.end method

.method public setThumbnailPadding(I)V
    .locals 0

    .prologue
    .line 1717782
    return-void
.end method

.method public setThumbnailSize(I)V
    .locals 0

    .prologue
    .line 1717783
    return-void
.end method

.method public setTitleText(I)V
    .locals 1

    .prologue
    .line 1717784
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/header/FigHeader;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1717785
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1717786
    iget-object v0, p0, Lcom/facebook/fig/header/FigHeader;->k:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1717787
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/header/FigHeader;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1717788
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->requestLayout()V

    .line 1717789
    invoke-virtual {p0}, Lcom/facebook/fig/header/FigHeader;->invalidate()V

    .line 1717790
    return-void
.end method
