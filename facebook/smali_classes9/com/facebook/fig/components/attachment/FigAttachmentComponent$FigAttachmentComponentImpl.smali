.class public final Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ap5;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Landroid/net/Uri;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:I

.field public e:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public f:LX/33B;

.field public g:Ljava/lang/CharSequence;

.field public h:Ljava/lang/CharSequence;

.field public i:Ljava/lang/CharSequence;

.field public j:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<",
            "LX/ApL;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<",
            "LX/ApI;",
            ">;"
        }
    .end annotation
.end field

.field public final synthetic m:LX/Ap5;


# direct methods
.method public constructor <init>(LX/Ap5;)V
    .locals 1

    .prologue
    .line 1715255
    iput-object p1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->m:LX/Ap5;

    .line 1715256
    move-object v0, p1

    .line 1715257
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1715258
    sget-object v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1715259
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1715260
    const-string v0, "FigAttachmentComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1715261
    if-ne p0, p1, :cond_1

    .line 1715262
    :cond_0
    :goto_0
    return v0

    .line 1715263
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1715264
    goto :goto_0

    .line 1715265
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    .line 1715266
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1715267
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1715268
    if-eq v2, v3, :cond_0

    .line 1715269
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->a:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1715270
    goto :goto_0

    .line 1715271
    :cond_4
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->b:Landroid/net/Uri;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1715272
    goto :goto_0

    .line 1715273
    :cond_6
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->b:Landroid/net/Uri;

    if-nez v2, :cond_5

    .line 1715274
    :cond_7
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1715275
    goto :goto_0

    .line 1715276
    :cond_9
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_8

    .line 1715277
    :cond_a
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->d:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->d:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1715278
    goto :goto_0

    .line 1715279
    :cond_b
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->e:LX/1Po;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->e:LX/1Po;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->e:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    .line 1715280
    goto :goto_0

    .line 1715281
    :cond_d
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->e:LX/1Po;

    if-nez v2, :cond_c

    .line 1715282
    :cond_e
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->f:LX/33B;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->f:LX/33B;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->f:LX/33B;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    .line 1715283
    goto :goto_0

    .line 1715284
    :cond_10
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->f:LX/33B;

    if-nez v2, :cond_f

    .line 1715285
    :cond_11
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->g:Ljava/lang/CharSequence;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->g:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->g:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    .line 1715286
    goto/16 :goto_0

    .line 1715287
    :cond_13
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->g:Ljava/lang/CharSequence;

    if-nez v2, :cond_12

    .line 1715288
    :cond_14
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->h:Ljava/lang/CharSequence;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->h:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->h:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    .line 1715289
    goto/16 :goto_0

    .line 1715290
    :cond_16
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->h:Ljava/lang/CharSequence;

    if-nez v2, :cond_15

    .line 1715291
    :cond_17
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->i:Ljava/lang/CharSequence;

    if-eqz v2, :cond_19

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->i:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->i:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    .line 1715292
    goto/16 :goto_0

    .line 1715293
    :cond_19
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->i:Ljava/lang/CharSequence;

    if-nez v2, :cond_18

    .line 1715294
    :cond_1a
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->j:LX/1dQ;

    if-eqz v2, :cond_1c

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->j:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->j:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    :cond_1b
    move v0, v1

    .line 1715295
    goto/16 :goto_0

    .line 1715296
    :cond_1c
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->j:LX/1dQ;

    if-nez v2, :cond_1b

    .line 1715297
    :cond_1d
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1e
    move v0, v1

    .line 1715298
    goto/16 :goto_0

    .line 1715299
    :cond_1f
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    if-nez v2, :cond_1e

    .line 1715300
    :cond_20
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1715301
    goto/16 :goto_0

    .line 1715302
    :cond_21
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 3

    .prologue
    .line 1715303
    const/4 v2, 0x0

    .line 1715304
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;

    .line 1715305
    iget-object v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->k:LX/1X1;

    .line 1715306
    iget-object v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    :cond_0
    iput-object v2, v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponent$FigAttachmentComponentImpl;->l:LX/1X1;

    .line 1715307
    return-object v0

    :cond_1
    move-object v1, v2

    .line 1715308
    goto :goto_0
.end method
