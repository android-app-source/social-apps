.class public final Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ApL;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Landroid/net/Uri;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field

.field public final synthetic e:LX/ApL;


# direct methods
.method public constructor <init>(LX/ApL;)V
    .locals 1

    .prologue
    .line 1715859
    iput-object p1, p0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->e:LX/ApL;

    .line 1715860
    move-object v0, p1

    .line 1715861
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1715862
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1715858
    const-string v0, "FigAttachmentFooterMediaComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1715839
    if-ne p0, p1, :cond_1

    .line 1715840
    :cond_0
    :goto_0
    return v0

    .line 1715841
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1715842
    goto :goto_0

    .line 1715843
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    .line 1715844
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1715845
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1715846
    if-eq v2, v3, :cond_0

    .line 1715847
    iget v2, p0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->a:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1715848
    goto :goto_0

    .line 1715849
    :cond_4
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->b:Landroid/net/Uri;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1715850
    goto :goto_0

    .line 1715851
    :cond_6
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->b:Landroid/net/Uri;

    if-nez v2, :cond_5

    .line 1715852
    :cond_7
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1715853
    goto :goto_0

    .line 1715854
    :cond_9
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_8

    .line 1715855
    :cond_a
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->d:LX/1X1;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->d:LX/1X1;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->d:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1715856
    goto :goto_0

    .line 1715857
    :cond_b
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->d:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1715835
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;

    .line 1715836
    iget-object v1, v0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->d:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->d:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, Lcom/facebook/fig/components/attachment/footercomponents/FigAttachmentFooterMediaComponent$FigAttachmentFooterMediaComponentImpl;->d:LX/1X1;

    .line 1715837
    return-object v0

    .line 1715838
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
