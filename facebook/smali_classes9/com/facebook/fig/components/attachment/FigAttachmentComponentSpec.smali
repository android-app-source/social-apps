.class public Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pp;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final b:LX/1nu;

.field private final c:LX/ApF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1715337
    const-class v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "large_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nu;LX/ApF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715338
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1715339
    iput-object p1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->b:LX/1nu;

    .line 1715340
    iput-object p2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->c:LX/ApF;

    .line 1715341
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;
    .locals 5

    .prologue
    .line 1715342
    const-class v1, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;

    monitor-enter v1

    .line 1715343
    :try_start_0
    sget-object v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715344
    sput-object v2, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715345
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715346
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715347
    new-instance p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v3

    check-cast v3, LX/1nu;

    invoke-static {v0}, LX/ApF;->a(LX/0QB;)LX/ApF;

    move-result-object v4

    check-cast v4, LX/ApF;

    invoke-direct {p0, v3, v4}, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;-><init>(LX/1nu;LX/ApF;)V

    .line 1715348
    move-object v0, p0

    .line 1715349
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715350
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715351
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715352
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;ILX/1Po;LX/33B;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;LX/1dQ;LX/1X1;LX/1X1;)LX/1Dg;
    .locals 5
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigAttachmentType;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigCoverPhotoIconType;
        .end annotation
    .end param
    .param p6    # LX/1Po;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/33B;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p9    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p10    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p11    # LX/1dQ;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p13    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Landroid/net/Uri;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "ITE;",
            "LX/33B;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "Ljava/lang/CharSequence;",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;",
            "LX/1X1",
            "<",
            "LX/ApL;",
            ">;",
            "LX/1X1",
            "<",
            "LX/ApI;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1715353
    packed-switch p2, :pswitch_data_0

    .line 1715354
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported attachment type = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0zj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1715355
    :pswitch_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v2

    if-nez p3, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    if-nez p8, :cond_1

    if-nez p9, :cond_1

    if-nez p10, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    .line 1715356
    :goto_2
    return-object v1

    .line 1715357
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x4

    invoke-interface {v1, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->b:LX/1nu;

    invoke-virtual {v3, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v3

    check-cast p6, LX/1Pp;

    invoke-virtual {v3, p6}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, p4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, p3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, p7}, LX/1nw;->a(LX/33B;)LX/1nw;

    move-result-object v3

    const v4, 0x3ff47ae1    # 1.91f

    invoke-virtual {v3, v4}, LX/1nw;->c(F)LX/1nw;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1, p5}, LX/Ap3;->a(LX/1De;I)LX/1Dg;

    move-result-object v3

    invoke-interface {v1, v3}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {p1}, LX/Ap8;->c(LX/1De;)LX/Ap6;

    move-result-object v1

    invoke-virtual {v1, p8}, LX/Ap6;->a(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v1

    invoke-virtual {v1, p9}, LX/Ap6;->b(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v1

    invoke-virtual {v1, p10}, LX/Ap6;->c(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/Ap6;->a(LX/1dQ;)LX/Ap6;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/Ap6;->b(LX/1X1;)LX/Ap6;

    move-result-object v1

    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, LX/Ap6;->a(LX/1X1;)LX/Ap6;

    move-result-object v1

    goto :goto_1

    .line 1715358
    :pswitch_1
    iget-object v1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->c:LX/ApF;

    invoke-virtual {v1, p1}, LX/ApF;->c(LX/1De;)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/ApE;->a(Landroid/net/Uri;)LX/ApE;

    move-result-object v1

    const v2, 0x7f0b116b

    invoke-virtual {v1, v2}, LX/ApE;->i(I)LX/ApE;

    move-result-object v1

    const v2, 0x3f2aaaab

    invoke-virtual {v1, v2}, LX/ApE;->c(F)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/ApE;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p8}, LX/ApE;->a(Ljava/lang/CharSequence;)LX/ApE;

    move-result-object v1

    const/high16 v2, 0x41900000    # 18.0f

    invoke-virtual {v1, v2}, LX/ApE;->e(F)LX/ApE;

    move-result-object v1

    const v2, 0x7f0e0128

    invoke-virtual {v1, v2}, LX/ApE;->h(I)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p9}, LX/ApE;->b(Ljava/lang/CharSequence;)LX/ApE;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, LX/ApE;->l(I)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p10}, LX/ApE;->c(Ljava/lang/CharSequence;)LX/ApE;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/ApE;->m(I)LX/ApE;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, LX/ApE;->f(F)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/ApE;->n(I)LX/ApE;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-interface {v1, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_2

    .line 1715359
    :pswitch_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->c:LX/ApF;

    invoke-virtual {v2, p1}, LX/ApF;->c(LX/1De;)LX/ApE;

    move-result-object v2

    invoke-virtual {v2, p3}, LX/ApE;->a(Landroid/net/Uri;)LX/ApE;

    move-result-object v2

    const/high16 v3, 0x42f80000    # 124.0f

    invoke-virtual {v2, v3}, LX/ApE;->d(F)LX/ApE;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, LX/ApE;->c(F)LX/ApE;

    move-result-object v2

    invoke-virtual {v2, p4}, LX/ApE;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/ApE;

    move-result-object v2

    invoke-virtual {v2, p8}, LX/ApE;->a(Ljava/lang/CharSequence;)LX/ApE;

    move-result-object v2

    const v3, 0x7f0e0129

    invoke-virtual {v2, v3}, LX/ApE;->h(I)LX/ApE;

    move-result-object v2

    const v3, 0x7f0b116a

    invoke-virtual {v2, v3}, LX/ApE;->j(I)LX/ApE;

    move-result-object v2

    invoke-virtual {v2, p9}, LX/ApE;->b(Ljava/lang/CharSequence;)LX/ApE;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, LX/ApE;->l(I)LX/ApE;

    move-result-object v2

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v2, v3}, LX/ApE;->f(F)LX/ApE;

    move-result-object v2

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v2, v3}, LX/ApE;->g(F)LX/ApE;

    move-result-object v2

    invoke-virtual {v2, p10}, LX/ApE;->c(Ljava/lang/CharSequence;)LX/ApE;

    move-result-object v2

    invoke-virtual {v2, p5}, LX/ApE;->n(I)LX/ApE;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const v3, 0x7f020b07

    invoke-interface {v2, v3}, LX/1Di;->x(I)LX/1Di;

    move-result-object v2

    move-object/from16 v0, p11

    invoke-interface {v2, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_2

    .line 1715360
    :pswitch_3
    iget-object v1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;->c:LX/ApF;

    invoke-virtual {v1, p1}, LX/ApF;->c(LX/1De;)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/ApE;->a(Landroid/net/Uri;)LX/ApE;

    move-result-object v1

    const/high16 v2, 0x42880000    # 68.0f

    invoke-virtual {v1, v2}, LX/ApE;->d(F)LX/ApE;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, LX/ApE;->c(F)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/ApE;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/ApE;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, LX/ApE;->k(I)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p8}, LX/ApE;->a(Ljava/lang/CharSequence;)LX/ApE;

    move-result-object v1

    const v2, 0x7f0b116a

    invoke-virtual {v1, v2}, LX/ApE;->j(I)LX/ApE;

    move-result-object v1

    const v2, 0x7f0e0129

    invoke-virtual {v1, v2}, LX/ApE;->h(I)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p10}, LX/ApE;->c(Ljava/lang/CharSequence;)LX/ApE;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v1, v2}, LX/ApE;->h(F)LX/ApE;

    move-result-object v1

    invoke-virtual {v1, p5}, LX/ApE;->n(I)LX/ApE;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-interface {v1, v0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_2

    .line 1715361
    :pswitch_4
    if-nez p8, :cond_2

    if-nez p9, :cond_2

    if-nez p10, :cond_2

    .line 1715362
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto/16 :goto_2

    .line 1715363
    :cond_2
    invoke-static {p1}, LX/Ap8;->c(LX/1De;)LX/Ap6;

    move-result-object v1

    invoke-virtual {v1, p8}, LX/Ap6;->a(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v1

    invoke-virtual {v1, p9}, LX/Ap6;->b(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v1

    invoke-virtual {v1, p10}, LX/Ap6;->c(Ljava/lang/CharSequence;)LX/Ap6;

    move-result-object v1

    move-object/from16 v0, p12

    invoke-virtual {v1, v0}, LX/Ap6;->b(LX/1X1;)LX/Ap6;

    move-result-object v1

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, LX/Ap6;->a(LX/1dQ;)LX/Ap6;

    move-result-object v1

    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, LX/Ap6;->a(LX/1X1;)LX/Ap6;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->b()LX/1Dg;

    move-result-object v1

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
