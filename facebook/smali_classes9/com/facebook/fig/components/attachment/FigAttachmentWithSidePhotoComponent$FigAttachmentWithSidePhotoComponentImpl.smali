.class public final Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ApF;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Landroid/net/Uri;

.field public b:I

.field public c:F

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:I

.field public l:Lcom/facebook/common/callercontext/CallerContext;

.field public m:Ljava/lang/CharSequence;

.field public n:Ljava/lang/CharSequence;

.field public o:Ljava/lang/CharSequence;

.field public p:I

.field public final synthetic q:LX/ApF;


# direct methods
.method public constructor <init>(LX/ApF;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1715646
    iput-object p1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->q:LX/ApF;

    .line 1715647
    move-object v0, p1

    .line 1715648
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1715649
    const/4 v0, 0x4

    iput v0, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->f:I

    .line 1715650
    iput v1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->g:I

    .line 1715651
    iput v1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->h:I

    .line 1715652
    iput v1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->i:I

    .line 1715653
    iput v1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->j:I

    .line 1715654
    iput v1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->k:I

    .line 1715655
    sget-object v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    .line 1715656
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1715645
    const-string v0, "FigAttachmentWithSidePhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1715600
    if-ne p0, p1, :cond_1

    .line 1715601
    :cond_0
    :goto_0
    return v0

    .line 1715602
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1715603
    goto :goto_0

    .line 1715604
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;

    .line 1715605
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1715606
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1715607
    if-eq v2, v3, :cond_0

    .line 1715608
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->a:Landroid/net/Uri;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->a:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1715609
    goto :goto_0

    .line 1715610
    :cond_5
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->a:Landroid/net/Uri;

    if-nez v2, :cond_4

    .line 1715611
    :cond_6
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->b:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1715612
    goto :goto_0

    .line 1715613
    :cond_7
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->c:F

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->c:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_8

    move v0, v1

    .line 1715614
    goto :goto_0

    .line 1715615
    :cond_8
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->d:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->d:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1715616
    goto :goto_0

    .line 1715617
    :cond_9
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->e:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->e:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1715618
    goto :goto_0

    .line 1715619
    :cond_a
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->f:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->f:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1715620
    goto :goto_0

    .line 1715621
    :cond_b
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->g:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->g:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1715622
    goto :goto_0

    .line 1715623
    :cond_c
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->h:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 1715624
    goto :goto_0

    .line 1715625
    :cond_d
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->i:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->i:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 1715626
    goto :goto_0

    .line 1715627
    :cond_e
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->j:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->j:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1715628
    goto :goto_0

    .line 1715629
    :cond_f
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->k:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->k:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1715630
    goto :goto_0

    .line 1715631
    :cond_10
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 1715632
    goto/16 :goto_0

    .line 1715633
    :cond_12
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->l:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_11

    .line 1715634
    :cond_13
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->m:Ljava/lang/CharSequence;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->m:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->m:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 1715635
    goto/16 :goto_0

    .line 1715636
    :cond_15
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->m:Ljava/lang/CharSequence;

    if-nez v2, :cond_14

    .line 1715637
    :cond_16
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->n:Ljava/lang/CharSequence;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->n:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->n:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 1715638
    goto/16 :goto_0

    .line 1715639
    :cond_18
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->n:Ljava/lang/CharSequence;

    if-nez v2, :cond_17

    .line 1715640
    :cond_19
    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->o:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->o:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->o:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    .line 1715641
    goto/16 :goto_0

    .line 1715642
    :cond_1b
    iget-object v2, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->o:Ljava/lang/CharSequence;

    if-nez v2, :cond_1a

    .line 1715643
    :cond_1c
    iget v2, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->p:I

    iget v3, p1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponent$FigAttachmentWithSidePhotoComponentImpl;->p:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1715644
    goto/16 :goto_0
.end method
