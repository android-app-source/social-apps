.class public Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private static final c:I
    .annotation build Landroid/support/annotation/StyleRes;
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1715685
    const-class v0, Lcom/facebook/fig/components/attachment/FigAttachmentComponentSpec;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "attachment_side_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1715686
    const v0, 0x7f0e012b

    sput v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->b:I

    .line 1715687
    const v0, 0x7f0e012d

    sput v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->c:I

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1715688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1715689
    iput-object p1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->d:LX/0Or;

    .line 1715690
    return-void
.end method

.method private static a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1715691
    invoke-static {p0, v2, p2}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;
    .locals 4

    .prologue
    .line 1715692
    const-class v1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;

    monitor-enter v1

    .line 1715693
    :try_start_0
    sget-object v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1715694
    sput-object v2, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1715695
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1715696
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1715697
    new-instance v3, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;-><init>(LX/0Or;)V

    .line 1715698
    move-object v0, v3

    .line 1715699
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1715700
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1715701
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1715702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Landroid/net/Uri;IFIIIIIIIILcom/facebook/common/callercontext/CallerContext;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)LX/1Dg;
    .locals 6
    .param p2    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # F
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p7    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p10    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p11    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p12    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->DIMEN_SIZE:LX/32B;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p14    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p15    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p16    # Ljava/lang/CharSequence;
        .annotation build Lcom/facebook/components/annotations/Prop;
            resType = .enum LX/32B;->STRING:LX/32B;
        .end annotation
    .end param
    .param p17    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/attachment/annotations/FigCoverPhotoIconType;
        .end annotation
    .end param

    .prologue
    .line 1715703
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    int-to-float v2, p5

    div-float/2addr v2, p4

    float-to-int v2, v2

    invoke-interface {v1, v2}, LX/1Dh;->I(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v1, v2}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->D(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, p7}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const v2, 0x7f020b09

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v3

    iget-object v1, p0, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Ad;

    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/1up;->c(F)LX/1up;

    move-result-object v1

    sget-object v3, LX/1Up;->g:LX/1Up;

    invoke-virtual {v1, v3}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, p5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x4

    invoke-interface {v1, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-interface {v1, v3}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {v1, v3}, LX/1Dh;->D(I)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x6

    const v4, 0x7f0b1169

    invoke-interface {v1, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x1

    invoke-interface {v1, v3, p6}, LX/1Dh;->s(II)LX/1Dh;

    move-result-object v1

    const/4 v3, 0x3

    const v4, 0x7f0b116a

    invoke-interface {v1, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    invoke-static/range {p14 .. p14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-static/range {p15 .. p15}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static/range {p16 .. p16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :goto_2
    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    move/from16 v0, p17

    invoke-static {p1, v0}, LX/Ap3;->a(LX/1De;I)LX/1Dg;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    :cond_0
    const/4 v1, 0x2

    move-object/from16 v0, p14

    invoke-static {p1, v0, p3, v1}, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;

    move-result-object v1

    goto :goto_0

    :cond_1
    sget v1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->b:I

    move-object/from16 v0, p15

    invoke-static {p1, v0, v1, p8}, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {v1, v4, p9}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    const/4 v4, 0x3

    move/from16 v0, p10

    invoke-interface {v1, v4, v0}, LX/1Di;->a(II)LX/1Di;

    move-result-object v1

    goto :goto_1

    :cond_2
    if-nez p11, :cond_3

    sget v1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->c:I

    const/4 v4, 0x1

    move-object/from16 v0, p16

    invoke-static {p1, v0, v1, v4}, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v4, 0x1

    move/from16 v0, p12

    invoke-interface {v1, v4, v0}, LX/1Di;->e(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_2

    :cond_3
    sget v1, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->c:I

    const/4 v4, 0x1

    move-object/from16 v0, p16

    invoke-static {p1, v0, v1, v4}, Lcom/facebook/fig/components/attachment/FigAttachmentWithSidePhotoComponentSpec;->a(LX/1De;Ljava/lang/CharSequence;II)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v4, 0x1

    invoke-interface {v1, v4}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/4 v4, 0x3

    const v5, 0x7f0b116a

    invoke-interface {v1, v4, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v1

    const/4 v4, 0x4

    const v5, 0x7f0b1169

    invoke-interface {v1, v4, v5}, LX/1Di;->l(II)LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    goto :goto_2
.end method
