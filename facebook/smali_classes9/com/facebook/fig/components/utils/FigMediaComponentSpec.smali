.class public Lcom/facebook/fig/components/utils/FigMediaComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1716858
    const-class v0, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;

    const-string v1, "newsfeed_angora_attachment_view"

    const-string v2, "small_photo"

    const-string v3, "native_newsfeed"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 1716859
    sput-object v0, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    sput-object v0, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1716860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1716861
    iput-object p1, p0, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->c:LX/0Or;

    .line 1716862
    return-void
.end method

.method private a(LX/1De;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)LX/1Dg;
    .locals 2
    .param p2    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param

    .prologue
    .line 1716863
    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    if-eqz p3, :cond_0

    :goto_0
    invoke-virtual {v0, p3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1Ad;->b(Landroid/net/Uri;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, LX/1up;->c(F)LX/1up;

    move-result-object v0

    sget-object v1, LX/1Up;->g:LX/1Up;

    invoke-virtual {v0, v1}, LX/1up;->b(LX/1Up;)LX/1up;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0044

    invoke-interface {v0, v1}, LX/1Di;->i(I)LX/1Di;

    move-result-object v0

    const v1, 0x7f0b0044

    invoke-interface {v0, v1}, LX/1Di;->q(I)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object p3, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/fig/components/utils/FigMediaComponentSpec;
    .locals 4

    .prologue
    .line 1716864
    const-class v1, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;

    monitor-enter v1

    .line 1716865
    :try_start_0
    sget-object v0, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1716866
    sput-object v2, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1716867
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1716868
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1716869
    new-instance v3, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;-><init>(LX/0Or;)V

    .line 1716870
    move-object v0, v3

    .line 1716871
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1716872
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1716873
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1716874
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;ILandroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;LX/1X1;)LX/1Dg;
    .locals 4
    .param p2    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation build Lcom/facebook/fig/components/utils/annotations/FigMediaType;
        .end annotation
    .end param
    .param p3    # Landroid/net/Uri;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1X1;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation

        .annotation runtime Ljava/lang/Deprecated;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I",
            "Landroid/net/Uri;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/1X1",
            "<*>;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x2

    .line 1716875
    packed-switch p2, :pswitch_data_0

    .line 1716876
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported media type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1716877
    :pswitch_1
    if-nez p5, :cond_0

    const/4 v0, 0x0

    .line 1716878
    :goto_0
    return-object v0

    .line 1716879
    :cond_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, v2}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v0

    invoke-interface {v0, p5}, LX/1Dh;->a(LX/1X1;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0

    .line 1716880
    :pswitch_2
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->a(LX/1De;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)LX/1Dg;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v1

    const v2, 0x7f02093a

    invoke-virtual {v1, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v1

    sget-object v2, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v2}, LX/1o5;->a(Landroid/widget/ImageView$ScaleType;)LX/1o5;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, LX/1Di;->c(I)LX/1Di;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    goto :goto_0

    .line 1716881
    :pswitch_3
    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/fig/components/utils/FigMediaComponentSpec;->a(LX/1De;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)LX/1Dg;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
