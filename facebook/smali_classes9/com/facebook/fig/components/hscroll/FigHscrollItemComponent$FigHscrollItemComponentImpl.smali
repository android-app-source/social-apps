.class public final Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/ApZ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:Landroid/net/Uri;

.field public d:Lcom/facebook/common/callercontext/CallerContext;

.field public e:LX/1Po;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public f:LX/33B;

.field public g:Ljava/lang/CharSequence;

.field public h:Ljava/lang/CharSequence;

.field public i:LX/1dQ;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<",
            "LX/Apj;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<",
            "LX/Aph;",
            ">;"
        }
    .end annotation
.end field

.field public l:Z

.field public final synthetic m:LX/ApZ;


# direct methods
.method public constructor <init>(LX/ApZ;)V
    .locals 1

    .prologue
    .line 1716250
    iput-object p1, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->m:LX/ApZ;

    .line 1716251
    move-object v0, p1

    .line 1716252
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1716253
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1716254
    const-string v0, "FigHscrollItemComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716255
    if-ne p0, p1, :cond_1

    .line 1716256
    :cond_0
    :goto_0
    return v0

    .line 1716257
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1716258
    goto :goto_0

    .line 1716259
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    .line 1716260
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1716261
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1716262
    if-eq v2, v3, :cond_0

    .line 1716263
    iget v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->a:I

    iget v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1716264
    goto :goto_0

    .line 1716265
    :cond_4
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->b:Ljava/lang/CharSequence;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->b:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->b:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1716266
    goto :goto_0

    .line 1716267
    :cond_6
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->b:Ljava/lang/CharSequence;

    if-nez v2, :cond_5

    .line 1716268
    :cond_7
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->c:Landroid/net/Uri;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->c:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1716269
    goto :goto_0

    .line 1716270
    :cond_9
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->c:Landroid/net/Uri;

    if-nez v2, :cond_8

    .line 1716271
    :cond_a
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1716272
    goto :goto_0

    .line 1716273
    :cond_c
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->d:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_b

    .line 1716274
    :cond_d
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->e:LX/1Po;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->e:LX/1Po;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->e:LX/1Po;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    .line 1716275
    goto :goto_0

    .line 1716276
    :cond_f
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->e:LX/1Po;

    if-nez v2, :cond_e

    .line 1716277
    :cond_10
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->f:LX/33B;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->f:LX/33B;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->f:LX/33B;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 1716278
    goto :goto_0

    .line 1716279
    :cond_12
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->f:LX/33B;

    if-nez v2, :cond_11

    .line 1716280
    :cond_13
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->g:Ljava/lang/CharSequence;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->g:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->g:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    .line 1716281
    goto/16 :goto_0

    .line 1716282
    :cond_15
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->g:Ljava/lang/CharSequence;

    if-nez v2, :cond_14

    .line 1716283
    :cond_16
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->h:Ljava/lang/CharSequence;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->h:Ljava/lang/CharSequence;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->h:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    .line 1716284
    goto/16 :goto_0

    .line 1716285
    :cond_18
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->h:Ljava/lang/CharSequence;

    if-nez v2, :cond_17

    .line 1716286
    :cond_19
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->i:LX/1dQ;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->i:LX/1dQ;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->i:LX/1dQ;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    .line 1716287
    goto/16 :goto_0

    .line 1716288
    :cond_1b
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->i:LX/1dQ;

    if-nez v2, :cond_1a

    .line 1716289
    :cond_1c
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->j:LX/1X1;

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->j:LX/1X1;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->j:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    .line 1716290
    goto/16 :goto_0

    .line 1716291
    :cond_1e
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->j:LX/1X1;

    if-nez v2, :cond_1d

    .line 1716292
    :cond_1f
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    if-eqz v2, :cond_21

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    :cond_20
    move v0, v1

    .line 1716293
    goto/16 :goto_0

    .line 1716294
    :cond_21
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    if-nez v2, :cond_20

    .line 1716295
    :cond_22
    iget-boolean v2, p0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->l:Z

    iget-boolean v3, p1, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->l:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1716296
    goto/16 :goto_0
.end method

.method public final g()LX/1X1;
    .locals 3

    .prologue
    .line 1716297
    const/4 v2, 0x0

    .line 1716298
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;

    .line 1716299
    iget-object v1, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->j:LX/1X1;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->j:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->j:LX/1X1;

    .line 1716300
    iget-object v1, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v2

    :cond_0
    iput-object v2, v0, Lcom/facebook/fig/components/hscroll/FigHscrollItemComponent$FigHscrollItemComponentImpl;->k:LX/1X1;

    .line 1716301
    return-object v0

    :cond_1
    move-object v1, v2

    .line 1716302
    goto :goto_0
.end method
