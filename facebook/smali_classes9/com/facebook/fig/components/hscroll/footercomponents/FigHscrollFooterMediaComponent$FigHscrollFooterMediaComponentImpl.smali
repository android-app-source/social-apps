.class public final Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Apj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:I

.field public b:Landroid/net/Uri;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:LX/1X1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1X1",
            "<*>;"
        }
    .end annotation
.end field


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1716553
    const-string v0, "FigHscrollFooterMediaComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1716554
    if-ne p0, p1, :cond_1

    .line 1716555
    :cond_0
    :goto_0
    return v0

    .line 1716556
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1716557
    goto :goto_0

    .line 1716558
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;

    .line 1716559
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1716560
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1716561
    if-eq v2, v3, :cond_0

    .line 1716562
    iget v2, p0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->a:I

    iget v3, p1, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->a:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1716563
    goto :goto_0

    .line 1716564
    :cond_4
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->b:Landroid/net/Uri;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->b:Landroid/net/Uri;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->b:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 1716565
    goto :goto_0

    .line 1716566
    :cond_6
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->b:Landroid/net/Uri;

    if-nez v2, :cond_5

    .line 1716567
    :cond_7
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1716568
    goto :goto_0

    .line 1716569
    :cond_9
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_8

    .line 1716570
    :cond_a
    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->d:LX/1X1;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->d:LX/1X1;

    iget-object v3, p1, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->d:LX/1X1;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1716571
    goto :goto_0

    .line 1716572
    :cond_b
    iget-object v2, p1, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->d:LX/1X1;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1716573
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;

    .line 1716574
    iget-object v1, v0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->d:LX/1X1;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->d:LX/1X1;

    invoke-virtual {v1}, LX/1X1;->g()LX/1X1;

    move-result-object v1

    :goto_0
    iput-object v1, v0, Lcom/facebook/fig/components/hscroll/footercomponents/FigHscrollFooterMediaComponent$FigHscrollFooterMediaComponentImpl;->d:LX/1X1;

    .line 1716575
    return-object v0

    .line 1716576
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
