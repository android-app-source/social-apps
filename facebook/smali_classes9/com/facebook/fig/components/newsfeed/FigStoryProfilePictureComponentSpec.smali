.class public Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:I
    .annotation build Landroid/support/annotation/DimenRes;
    .end annotation
.end field

.field public static final b:Lcom/facebook/common/callercontext/CallerContext;

.field private static d:LX/0Xm;


# instance fields
.field public final c:LX/CDt;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1860711
    const v0, 0x7f0b1ae6

    sput v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;->a:I

    .line 1860712
    const-class v0, LX/CE3;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/CDt;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1860713
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1860714
    iput-object p1, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;->c:LX/CDt;

    .line 1860715
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;
    .locals 4

    .prologue
    .line 1860716
    const-class v1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;

    monitor-enter v1

    .line 1860717
    :try_start_0
    sget-object v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1860718
    sput-object v2, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1860719
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1860720
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1860721
    new-instance p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;

    invoke-static {v0}, LX/CDt;->a(LX/0QB;)LX/CDt;

    move-result-object v3

    check-cast v3, LX/CDt;

    invoke-direct {p0, v3}, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;-><init>(LX/CDt;)V

    .line 1860722
    move-object v0, p0

    .line 1860723
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1860724
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1860725
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1860726
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
