.class public final Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CE9;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/common/callercontext/CallerContext;

.field public final synthetic f:LX/CE9;


# direct methods
.method public constructor <init>(LX/CE9;)V
    .locals 1

    .prologue
    .line 1860636
    iput-object p1, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->f:LX/CE9;

    .line 1860637
    move-object v0, p1

    .line 1860638
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1860639
    sget v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;->a:I

    iput v0, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->b:I

    .line 1860640
    sget-object v0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponentSpec;->b:Lcom/facebook/common/callercontext/CallerContext;

    iput-object v0, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1860641
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1860642
    const-string v0, "FigStoryProfilePictureComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1860643
    if-ne p0, p1, :cond_1

    .line 1860644
    :cond_0
    :goto_0
    return v0

    .line 1860645
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1860646
    goto :goto_0

    .line 1860647
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;

    .line 1860648
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1860649
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1860650
    if-eq v2, v3, :cond_0

    .line 1860651
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1860652
    goto :goto_0

    .line 1860653
    :cond_5
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1860654
    :cond_6
    iget v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->b:I

    iget v3, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1860655
    goto :goto_0

    .line 1860656
    :cond_7
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->c:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    .line 1860657
    goto :goto_0

    .line 1860658
    :cond_9
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->c:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 1860659
    :cond_a
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->d:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    .line 1860660
    goto :goto_0

    .line 1860661
    :cond_c
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->d:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 1860662
    :cond_d
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1860663
    goto :goto_0

    .line 1860664
    :cond_e
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigStoryProfilePictureComponent$FigStoryProfilePictureComponentImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
