.class public final Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/CDt;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/facebook/common/callercontext/CallerContext;

.field public c:Landroid/graphics/PointF;

.field public d:LX/1Up;

.field public e:LX/1Up;

.field public f:LX/4Ab;

.field public g:F

.field public final synthetic h:LX/CDt;


# direct methods
.method public constructor <init>(LX/CDt;)V
    .locals 1

    .prologue
    .line 1860185
    iput-object p1, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->h:LX/CDt;

    .line 1860186
    move-object v0, p1

    .line 1860187
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1860188
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1860189
    const-string v0, "FigProfilePhotoComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1860190
    if-ne p0, p1, :cond_1

    .line 1860191
    :cond_0
    :goto_0
    return v0

    .line 1860192
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1860193
    goto :goto_0

    .line 1860194
    :cond_3
    check-cast p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;

    .line 1860195
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1860196
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1860197
    if-eq v2, v3, :cond_0

    .line 1860198
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1860199
    goto :goto_0

    .line 1860200
    :cond_5
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1860201
    :cond_6
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1860202
    goto :goto_0

    .line 1860203
    :cond_8
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->b:Lcom/facebook/common/callercontext/CallerContext;

    if-nez v2, :cond_7

    .line 1860204
    :cond_9
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->c:Landroid/graphics/PointF;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->c:Landroid/graphics/PointF;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->c:Landroid/graphics/PointF;

    invoke-virtual {v2, v3}, Landroid/graphics/PointF;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 1860205
    goto :goto_0

    .line 1860206
    :cond_b
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->c:Landroid/graphics/PointF;

    if-nez v2, :cond_a

    .line 1860207
    :cond_c
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->d:LX/1Up;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->d:LX/1Up;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->d:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 1860208
    goto :goto_0

    .line 1860209
    :cond_e
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->d:LX/1Up;

    if-nez v2, :cond_d

    .line 1860210
    :cond_f
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->e:LX/1Up;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->e:LX/1Up;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->e:LX/1Up;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    :cond_10
    move v0, v1

    .line 1860211
    goto :goto_0

    .line 1860212
    :cond_11
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->e:LX/1Up;

    if-nez v2, :cond_10

    .line 1860213
    :cond_12
    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->f:LX/4Ab;

    if-eqz v2, :cond_14

    iget-object v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->f:LX/4Ab;

    iget-object v3, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->f:LX/4Ab;

    invoke-virtual {v2, v3}, LX/4Ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    :cond_13
    move v0, v1

    .line 1860214
    goto/16 :goto_0

    .line 1860215
    :cond_14
    iget-object v2, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->f:LX/4Ab;

    if-nez v2, :cond_13

    .line 1860216
    :cond_15
    iget v2, p0, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->g:F

    iget v3, p1, Lcom/facebook/fig/components/newsfeed/FigProfilePhotoComponent$FigProfilePhotoComponentImpl;->g:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 1860217
    goto/16 :goto_0
.end method
