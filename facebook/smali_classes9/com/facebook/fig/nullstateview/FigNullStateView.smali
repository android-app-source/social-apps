.class public Lcom/facebook/fig/nullstateview/FigNullStateView;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation


# instance fields
.field private a:LX/6WW;

.field private b:LX/6WW;

.field private c:Lcom/facebook/fig/button/FigButton;

.field private d:Landroid/graphics/drawable/Drawable;

.field public e:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1860886
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 1860887
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/facebook/fig/nullstateview/FigNullStateView;->a(Landroid/util/AttributeSet;I)V

    .line 1860888
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1860889
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1860890
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->a(Landroid/util/AttributeSet;I)V

    .line 1860891
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1860892
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1860893
    invoke-direct {p0, p2, p3}, Lcom/facebook/fig/nullstateview/FigNullStateView;->a(Landroid/util/AttributeSet;I)V

    .line 1860894
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1860895
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getBodyText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1860896
    return-void
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, -0x2

    const/4 v1, 0x0

    .line 1860903
    const-class v0, Lcom/facebook/fig/nullstateview/FigNullStateView;

    invoke-static {v0, p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1860904
    new-instance v0, Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/facebook/fig/button/FigButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    .line 1860905
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    const/16 v2, 0x108

    invoke-virtual {v0, v2}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 1860906
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 1860907
    iget-object v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0, v2, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 1860908
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    .line 1860909
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e011e

    invoke-virtual {v0, v2, v3}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1860910
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {v0, v4}, LX/6WW;->b(I)V

    .line 1860911
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v2}, LX/6WW;->a(Landroid/text/Layout$Alignment;)V

    .line 1860912
    new-instance v0, LX/6WW;

    invoke-direct {v0}, LX/6WW;-><init>()V

    iput-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    .line 1860913
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0122

    invoke-virtual {v0, v2, v3}, LX/6WW;->a(Landroid/content/Context;I)V

    .line 1860914
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v0, v4}, LX/6WW;->b(I)V

    .line 1860915
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    sget-object v2, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v0, v2}, LX/6WW;->a(Landroid/text/Layout$Alignment;)V

    .line 1860916
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1860917
    const v2, 0x7f0b1dd2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->f:I

    .line 1860918
    const v2, 0x7f0b1dd3

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->g:I

    .line 1860919
    const v2, 0x7f0b1dd1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->h:I

    .line 1860920
    const v2, 0x7f0b1dd4

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->i:I

    .line 1860921
    const v2, 0x7f0b1dd5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->j:I

    .line 1860922
    const v2, 0x7f0b1dd6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->k:I

    .line 1860923
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->l:Z

    .line 1860924
    if-eqz p1, :cond_1

    .line 1860925
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v2, LX/03r;->FigNullStateView:[I

    invoke-virtual {v0, p1, v2, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1860926
    const/16 v2, 0x2

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1860927
    if-lez v2, :cond_3

    .line 1860928
    invoke-virtual {p0, v2}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setTitleText(I)V

    .line 1860929
    :goto_1
    const/16 v2, 0x3

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1860930
    if-lez v2, :cond_4

    .line 1860931
    invoke-virtual {p0, v2}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setBodyText(I)V

    .line 1860932
    :goto_2
    const/16 v2, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    .line 1860933
    if-lez v2, :cond_5

    .line 1860934
    invoke-virtual {p0, v2}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setButtonText(I)V

    .line 1860935
    :goto_3
    const/16 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1860936
    if-lez v1, :cond_0

    .line 1860937
    invoke-virtual {p0, v1}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setGlyph(I)V

    .line 1860938
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1860939
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 1860940
    goto :goto_0

    .line 1860941
    :cond_3
    const/16 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1860942
    invoke-virtual {p0, v2}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1860943
    :cond_4
    const/16 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1860944
    invoke-virtual {p0, v2}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setBodyText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1860945
    :cond_5
    const/16 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1860946
    invoke-virtual {p0, v2}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setButtonText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/fig/nullstateview/FigNullStateView;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->e:LX/0wM;

    return-void
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1860897
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1860898
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1860899
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1860900
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Landroid/graphics/Canvas;)V

    .line 1860901
    return-void
.end method

.method public getBodyText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1860902
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getButtonText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1860830
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0}, Lcom/facebook/fig/button/FigButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getGlyph()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1860884
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->d:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getTitleText()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1860885
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final onLayout(ZIIII)V
    .locals 6

    .prologue
    .line 1860814
    sub-int v1, p4, p2

    .line 1860815
    iget v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->h:I

    .line 1860816
    iget v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->g:I

    sub-int v2, v1, v2

    shr-int/lit8 v2, v2, 0x1

    .line 1860817
    iget-object v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->d:Landroid/graphics/drawable/Drawable;

    iget v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->g:I

    add-int/2addr v4, v2

    iget v5, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->f:I

    add-int/2addr v5, v0

    invoke-virtual {v3, v2, v0, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1860818
    iget v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->f:I

    iget v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->i:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1860819
    iget v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->h:I

    add-int/2addr v2, p2

    .line 1860820
    iget-object v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    iget-boolean v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->l:Z

    iget-object v5, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {v5}, LX/6WW;->b()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v0, v5}, LX/6WW;->a(ZIII)V

    .line 1860821
    iget-object v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {v2}, LX/6WW;->c()I

    move-result v2

    iget v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->j:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1860822
    iget-object v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    iget v2, v2, LX/6WW;->c:I

    if-nez v2, :cond_0

    .line 1860823
    iget v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->h:I

    add-int/2addr v2, p2

    .line 1860824
    iget-object v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    iget-boolean v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->l:Z

    iget-object v5, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v5}, LX/6WW;->b()I

    move-result v5

    add-int/2addr v5, v2

    invoke-virtual {v3, v4, v2, v0, v5}, LX/6WW;->a(ZIII)V

    .line 1860825
    iget-object v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v2}, LX/6WW;->c()I

    move-result v2

    iget v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->k:I

    add-int/2addr v2, v3

    add-int/2addr v0, v2

    .line 1860826
    :cond_0
    iget-object v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2}, Lcom/facebook/fig/button/FigButton;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 1860827
    iget-object v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v2}, Lcom/facebook/fig/button/FigButton;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v1, v2

    shr-int/lit8 v1, v1, 0x1

    .line 1860828
    iget-object v2, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    iget-object v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v3}, Lcom/facebook/fig/button/FigButton;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v4}, Lcom/facebook/fig/button/FigButton;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v2, v1, v0, v3, v4}, Lcom/facebook/fig/button/FigButton;->layout(IIII)V

    .line 1860829
    :cond_1
    return-void
.end method

.method public final onMeasure(II)V
    .locals 6

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1860831
    iget-object v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v0}, LX/6WW;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1860832
    :goto_0
    iput v0, v3, LX/6WW;->c:I

    .line 1860833
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0}, Lcom/facebook/fig/button/FigButton;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 1860834
    iget-object v3, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    if-eqz v0, :cond_3

    :goto_1
    invoke-virtual {v3, v2}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 1860835
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 1860836
    iget v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->h:I

    shl-int/lit8 v0, v0, 0x1

    sub-int v0, v2, v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1860837
    iget v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->f:I

    add-int/lit8 v0, v0, 0x0

    .line 1860838
    iget-object v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {v4, v3}, LX/6WW;->c(I)V

    .line 1860839
    iget-object v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {v4}, LX/6WW;->c()I

    move-result v4

    iget v5, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->i:I

    add-int/2addr v4, v5

    iget v5, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->j:I

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    .line 1860840
    iget-object v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    iget v4, v4, LX/6WW;->c:I

    if-nez v4, :cond_0

    .line 1860841
    iget-object v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v4, v3}, LX/6WW;->c(I)V

    .line 1860842
    iget-object v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v4}, LX/6WW;->c()I

    move-result v4

    iget v5, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->k:I

    add-int/2addr v4, v5

    add-int/2addr v0, v4

    .line 1860843
    :cond_0
    iget-object v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v4}, Lcom/facebook/fig/button/FigButton;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 1860844
    invoke-static {v3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    const/high16 v4, -0x80000000

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 1860845
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1860846
    iget-object v4, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v4, v3, v1}, Lcom/facebook/fig/button/FigButton;->measure(II)V

    .line 1860847
    iget-object v1, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v1}, Lcom/facebook/fig/button/FigButton;->getMeasuredHeight()I

    move-result v1

    add-int/2addr v0, v1

    .line 1860848
    :cond_1
    iget v1, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->h:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1860849
    invoke-virtual {p0, v2, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setMeasuredDimension(II)V

    .line 1860850
    return-void

    :cond_2
    move v0, v2

    .line 1860851
    goto :goto_0

    :cond_3
    move v2, v1

    .line 1860852
    goto :goto_1
.end method

.method public setBodyText(I)V
    .locals 1

    .prologue
    .line 1860853
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setBodyText(Ljava/lang/CharSequence;)V

    .line 1860854
    return-void
.end method

.method public setBodyText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1860855
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->b:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1860856
    invoke-direct {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->a()V

    .line 1860857
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->requestLayout()V

    .line 1860858
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->invalidate()V

    .line 1860859
    return-void
.end method

.method public setButtonText(I)V
    .locals 1

    .prologue
    .line 1860860
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setButtonText(Ljava/lang/CharSequence;)V

    .line 1860861
    return-void
.end method

.method public setButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1860862
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->c:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 1860863
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->requestLayout()V

    .line 1860864
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->invalidate()V

    .line 1860865
    return-void
.end method

.method public setGlyph(I)V
    .locals 1

    .prologue
    .line 1860866
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    .line 1860867
    return-void
.end method

.method public setGlyph(Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    .line 1860868
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->e:LX/0wM;

    if-eqz v0, :cond_0

    .line 1860869
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->e:LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00a1

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1860870
    iput-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->d:Landroid/graphics/drawable/Drawable;

    .line 1860871
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->requestLayout()V

    .line 1860872
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->invalidate()V

    .line 1860873
    return-void

    .line 1860874
    :cond_0
    iput-object p1, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method public setTitleText(I)V
    .locals 1

    .prologue
    .line 1860875
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1860876
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1860877
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1860878
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    const-string v1, "DEFAULT TITLE"

    invoke-virtual {v0, v1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    .line 1860879
    :goto_0
    invoke-direct {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->a()V

    .line 1860880
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->requestLayout()V

    .line 1860881
    invoke-virtual {p0}, Lcom/facebook/fig/nullstateview/FigNullStateView;->invalidate()V

    .line 1860882
    return-void

    .line 1860883
    :cond_0
    iget-object v0, p0, Lcom/facebook/fig/nullstateview/FigNullStateView;->a:LX/6WW;

    invoke-virtual {v0, p1}, LX/6WW;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
