.class public Lcom/facebook/notifications/server/GetNotificationsSettingsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/server/GetNotificationsSettingsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:LX/0rS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763931
    new-instance v0, LX/BDs;

    invoke-direct {v0}, LX/BDs;-><init>()V

    sput-object v0, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1763932
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763933
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;->c:LX/0rS;

    .line 1763934
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;->b:Ljava/lang/String;

    .line 1763935
    const-string v0, "me()"

    iput-object v0, p0, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;->a:Ljava/lang/String;

    .line 1763936
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1763937
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1763938
    iget-object v0, p0, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;->c:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763939
    iget-object v0, p0, Lcom/facebook/notifications/server/GetNotificationsSettingsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763940
    return-void
.end method
