.class public Lcom/facebook/notifications/server/FetchNotificationsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/server/FetchNotificationsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:LX/0rS;

.field private b:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763926
    new-instance v0, LX/BDr;

    invoke-direct {v0}, LX/BDr;-><init>()V

    sput-object v0, Lcom/facebook/notifications/server/FetchNotificationsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1763922
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763923
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0rS;->valueOf(Ljava/lang/String;)LX/0rS;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/FetchNotificationsParams;->a:LX/0rS;

    .line 1763924
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notifications/server/FetchNotificationsParams;->b:J

    .line 1763925
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1763927
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1763919
    iget-object v0, p0, Lcom/facebook/notifications/server/FetchNotificationsParams;->a:LX/0rS;

    invoke-virtual {v0}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763920
    iget-wide v0, p0, Lcom/facebook/notifications/server/FetchNotificationsParams;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1763921
    return-void
.end method
