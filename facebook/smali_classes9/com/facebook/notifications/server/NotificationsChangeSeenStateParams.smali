.class public Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763980
    new-instance v0, LX/BDu;

    invoke-direct {v0}, LX/BDu;-><init>()V

    sput-object v0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1763969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763970
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1763971
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1763972
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 1763973
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->a:LX/0Px;

    .line 1763974
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/graphql/enums/GraphQLStorySeenState;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1763975
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763976
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1763977
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->a:LX/0Px;

    .line 1763978
    iput-object p2, p0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1763979
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1763965
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1763966
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->b:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763967
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSeenStateParams;->a:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1763968
    return-void
.end method
