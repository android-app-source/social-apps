.class public Lcom/facebook/notifications/server/NotificationsChangeReadParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/server/NotificationsChangeReadParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763944
    new-instance v0, LX/BDt;

    invoke-direct {v0}, LX/BDt;-><init>()V

    sput-object v0, Lcom/facebook/notifications/server/NotificationsChangeReadParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1763945
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763946
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1763947
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/notifications/server/NotificationsChangeReadParams;->b:Z

    .line 1763948
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1763949
    :goto_1
    if-ge v1, v0, :cond_1

    .line 1763950
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1763951
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 1763952
    goto :goto_0

    .line 1763953
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeReadParams;->a:LX/0Rf;

    .line 1763954
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1763955
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1763956
    iget-boolean v0, p0, Lcom/facebook/notifications/server/NotificationsChangeReadParams;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1763957
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeReadParams;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1763958
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeReadParams;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1763959
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_1

    .line 1763960
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1763961
    :cond_1
    return-void
.end method
