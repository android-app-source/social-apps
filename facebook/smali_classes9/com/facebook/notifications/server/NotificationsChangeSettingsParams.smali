.class public Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763984
    new-instance v0, LX/BDv;

    invoke-direct {v0}, LX/BDv;-><init>()V

    sput-object v0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1763985
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763986
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->a:Ljava/lang/String;

    .line 1763987
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->b:Ljava/lang/String;

    .line 1763988
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->c:Ljava/lang/String;

    .line 1763989
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->d:Ljava/lang/String;

    .line 1763990
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->e:Ljava/lang/String;

    .line 1763991
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1763992
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1763993
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763994
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763995
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763996
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763997
    iget-object v0, p0, Lcom/facebook/notifications/server/NotificationsChangeSettingsParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1763998
    return-void
.end method
