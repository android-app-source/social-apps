.class public Lcom/facebook/notifications/model/NotificationSeenStates;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/model/NotificationSeenStatesDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/model/NotificationSeenStates;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final notificationSeenStatesList:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1753206
    const-class v0, Lcom/facebook/notifications/model/NotificationSeenStatesDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1753207
    new-instance v0, LX/BAW;

    invoke-direct {v0}, LX/BAW;-><init>()V

    sput-object v0, Lcom/facebook/notifications/model/NotificationSeenStates;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1753208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753209
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates;->notificationSeenStatesList:LX/0Px;

    .line 1753210
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1753211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753212
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1753213
    sget-object v1, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1753214
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates;->notificationSeenStatesList:LX/0Px;

    .line 1753215
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1753216
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1753217
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates;->notificationSeenStatesList:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1753218
    return-void
.end method
