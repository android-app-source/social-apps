.class public Lcom/facebook/notifications/model/FacebookNotificationSetting;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/model/FacebookNotificationSettingDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/model/FacebookNotificationSetting;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final settingId:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "setting_id"
    .end annotation
.end field

.field public final settingValue:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "setting_value"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1753106
    const-class v0, Lcom/facebook/notifications/model/FacebookNotificationSettingDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1753105
    new-instance v0, LX/BAU;

    invoke-direct {v0}, LX/BAU;-><init>()V

    sput-object v0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1753101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753102
    iput-object v0, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingId:Ljava/lang/String;

    .line 1753103
    iput-object v0, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingValue:Ljava/lang/String;

    .line 1753104
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1753097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753098
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingId:Ljava/lang/String;

    .line 1753099
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingValue:Ljava/lang/String;

    .line 1753100
    return-void
.end method

.method public static a(LX/15w;)Lcom/facebook/notifications/model/FacebookNotificationSetting;
    .locals 1

    .prologue
    .line 1753107
    const-class v0, Lcom/facebook/notifications/model/FacebookNotificationSetting;

    invoke-virtual {p0, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/FacebookNotificationSetting;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1753096
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1753092
    instance-of v1, p1, Lcom/facebook/notifications/model/FacebookNotificationSetting;

    if-eqz v1, :cond_0

    .line 1753093
    check-cast p1, Lcom/facebook/notifications/model/FacebookNotificationSetting;

    .line 1753094
    iget-object v1, p1, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingId:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingId:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingValue:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingValue:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1753095
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1753087
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingId:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingValue:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1753091
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "setting ID"

    iget-object v2, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "setting value"

    iget-object v2, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingValue:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1753088
    iget-object v0, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1753089
    iget-object v0, p0, Lcom/facebook/notifications/model/FacebookNotificationSetting;->settingValue:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1753090
    return-void
.end method
