.class public final Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/model/NotificationSeenStates_NotificationSeenStateDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final id:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "id"
    .end annotation
.end field

.field public final seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "seen_state"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1753205
    const-class v0, Lcom/facebook/notifications/model/NotificationSeenStates_NotificationSeenStateDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1753204
    new-instance v0, LX/BAX;

    invoke-direct {v0}, LX/BAX;-><init>()V

    sput-object v0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1753200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753201
    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    .line 1753202
    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1753203
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1753196
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753197
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    .line 1753198
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->valueOf(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1753199
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLStorySeenState;)V
    .locals 0
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 1753192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753193
    iput-object p1, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    .line 1753194
    iput-object p2, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1753195
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1753191
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1753187
    instance-of v1, p1, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;

    if-eqz v1, :cond_0

    .line 1753188
    check-cast p1, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;

    .line 1753189
    iget-object v1, p1, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iget-object v2, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 1753190
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1753182
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1753186
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "notification id"

    iget-object v2, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "seen state"

    iget-object v2, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1753183
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->id:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1753184
    iget-object v0, p0, Lcom/facebook/notifications/model/NotificationSeenStates$NotificationSeenState;->seenState:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1753185
    return-void
.end method
