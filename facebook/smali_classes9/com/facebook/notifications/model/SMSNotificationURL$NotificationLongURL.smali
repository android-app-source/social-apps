.class public final Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final longUrl:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "long_url"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1753278
    const-class v0, Lcom/facebook/notifications/model/SMSNotificationURL_NotificationLongURLDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1753268
    new-instance v0, LX/BAZ;

    invoke-direct {v0}, LX/BAZ;-><init>()V

    sput-object v0, Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1753275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753276
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;->longUrl:Ljava/lang/String;

    .line 1753277
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1753272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753273
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;->longUrl:Ljava/lang/String;

    .line 1753274
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1753271
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1753269
    iget-object v0, p0, Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;->longUrl:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1753270
    return-void
.end method
