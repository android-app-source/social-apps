.class public Lcom/facebook/notifications/model/NewNotificationStories;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/model/NewNotificationStoriesDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/model/NewNotificationStories;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "deltas"
    .end annotation
.end field

.field private final newStories:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "edges"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field private final pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "page_info"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1753152
    const-class v0, Lcom/facebook/notifications/model/NewNotificationStoriesDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1753133
    new-instance v0, LX/BAV;

    invoke-direct {v0}, LX/BAV;-><init>()V

    sput-object v0, Lcom/facebook/notifications/model/NewNotificationStories;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1753147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753148
    iput-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->newStories:LX/0Px;

    .line 1753149
    iput-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    .line 1753150
    iput-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1753151
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1753142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753143
    invoke-static {p1}, LX/4By;->b(Landroid/os/Parcel;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->newStories:LX/0Px;

    .line 1753144
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    iput-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    .line 1753145
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPageInfo;

    iput-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 1753146
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1753141
    iget-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->newStories:LX/0Px;

    return-object v0
.end method

.method public final b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;
    .locals 1

    .prologue
    .line 1753140
    iget-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLPageInfo;
    .locals 1

    .prologue
    .line 1753139
    iget-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1753138
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1753134
    iget-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->newStories:LX/0Px;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Ljava/util/List;)V

    .line 1753135
    iget-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->deltaStories:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1753136
    iget-object v0, p0, Lcom/facebook/notifications/model/NewNotificationStories;->pageInfo:Lcom/facebook/graphql/model/GraphQLPageInfo;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1753137
    return-void
.end method
