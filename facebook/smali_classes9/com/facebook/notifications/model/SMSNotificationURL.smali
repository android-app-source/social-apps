.class public Lcom/facebook/notifications/model/SMSNotificationURL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/model/SMSNotificationURL;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final notificationLongUrlList:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "data"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1753279
    const-class v0, Lcom/facebook/notifications/model/SMSNotificationURLDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1753280
    new-instance v0, LX/BAY;

    invoke-direct {v0}, LX/BAY;-><init>()V

    sput-object v0, Lcom/facebook/notifications/model/SMSNotificationURL;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1753281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/model/SMSNotificationURL;->notificationLongUrlList:LX/0Px;

    .line 1753283
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1753284
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1753285
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1753286
    sget-object v1, Lcom/facebook/notifications/model/SMSNotificationURL$NotificationLongURL;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedList(Ljava/util/List;Landroid/os/Parcelable$Creator;)V

    .line 1753287
    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/model/SMSNotificationURL;->notificationLongUrlList:LX/0Px;

    .line 1753288
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1753289
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1753290
    iget-object v0, p0, Lcom/facebook/notifications/model/SMSNotificationURL;->notificationLongUrlList:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1753291
    return-void
.end method
