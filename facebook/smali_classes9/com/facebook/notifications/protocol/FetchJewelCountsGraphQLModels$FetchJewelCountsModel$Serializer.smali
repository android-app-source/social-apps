.class public final Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1753441
    const-class v0, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;

    new-instance v1, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1753442
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1753443
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1753444
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1753445
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1753446
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1753447
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1753448
    if-eqz v2, :cond_0

    .line 1753449
    const-string p0, "friending_possibilities"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1753450
    invoke-static {v1, v2, p1}, LX/2vF;->a(LX/15i;ILX/0nX;)V

    .line 1753451
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1753452
    if-eqz v2, :cond_1

    .line 1753453
    const-string p0, "message_threads"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1753454
    invoke-static {v1, v2, p1}, LX/3CD;->a(LX/15i;ILX/0nX;)V

    .line 1753455
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1753456
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1753457
    check-cast p1, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel$Serializer;->a(Lcom/facebook/notifications/protocol/FetchJewelCountsGraphQLModels$FetchJewelCountsModel;LX/0nX;LX/0my;)V

    return-void
.end method
