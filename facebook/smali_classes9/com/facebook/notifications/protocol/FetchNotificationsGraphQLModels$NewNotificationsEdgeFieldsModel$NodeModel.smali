.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x76508b10
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Z

.field private j:I

.field private k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private l:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private n:J

.field private o:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1756094
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1756095
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756096
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1756097
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1756098
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1756099
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1756100
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 1756101
    iput-boolean p1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->i:Z

    .line 1756102
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1756103
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1756104
    if-eqz v0, :cond_0

    .line 1756105
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IIZ)V

    .line 1756106
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 1756107
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1756108
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1756109
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1756110
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->d()LX/0Px;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->c(Ljava/util/List;)I

    move-result v2

    .line 1756111
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1756112
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->s()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1756113
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->t()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1756114
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->u()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1756115
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->v()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v7

    invoke-static {p1, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1756116
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 1756117
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->w()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    move-result-object v9

    invoke-static {p1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1756118
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->q()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 1756119
    const/16 v11, 0xe

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 1756120
    invoke-virtual {p1, v12, v0}, LX/186;->b(II)V

    .line 1756121
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1756122
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1756123
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1756124
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->i:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1756125
    const/4 v0, 0x5

    iget v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->j:I

    invoke-virtual {p1, v0, v1, v12}, LX/186;->a(III)V

    .line 1756126
    const/4 v0, 0x6

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1756127
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1756128
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1756129
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->n:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1756130
    const/16 v0, 0xa

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1756131
    const/16 v0, 0xb

    invoke-virtual {p1, v0, v8}, LX/186;->b(II)V

    .line 1756132
    const/16 v0, 0xc

    invoke-virtual {p1, v0, v9}, LX/186;->b(II)V

    .line 1756133
    const/16 v0, 0xd

    invoke-virtual {p1, v0, v10}, LX/186;->b(II)V

    .line 1756134
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1756135
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1756136
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1756137
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1756138
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    .line 1756139
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1756140
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756141
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    .line 1756142
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->s()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1756143
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->s()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    .line 1756144
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->s()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1756145
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756146
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    .line 1756147
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->t()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1756148
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->t()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    .line 1756149
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->t()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1756150
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756151
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->l:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    .line 1756152
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->u()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1756153
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->u()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    .line 1756154
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->u()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_3

    .line 1756155
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756156
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->m:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    .line 1756157
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->v()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1756158
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->v()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 1756159
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->v()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v2

    if-eq v2, v0, :cond_4

    .line 1756160
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756161
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->o:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 1756162
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->w()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 1756163
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->w()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    .line 1756164
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->w()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    move-result-object v2

    if-eq v2, v0, :cond_5

    .line 1756165
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    .line 1756166
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->q:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    .line 1756167
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1756168
    if-nez v1, :cond_6

    :goto_0
    return-object p0

    :cond_6
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1756169
    new-instance v0, LX/BB2;

    invoke-direct {v0, p1}, LX/BB2;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756192
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1756170
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1756171
    const/4 v0, 0x4

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->i:Z

    .line 1756172
    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->j:I

    .line 1756173
    const/16 v0, 0x9

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->n:J

    .line 1756174
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1756175
    const-string v0, "local_is_rich_notif_collapsed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756176
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->hW_()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1756177
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1756178
    const/4 v0, 0x4

    iput v0, p2, LX/18L;->c:I

    .line 1756179
    :goto_0
    return-void

    .line 1756180
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1756181
    const-string v0, "local_is_rich_notif_collapsed"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756182
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->a(Z)V

    .line 1756183
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1756184
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;-><init>()V

    .line 1756185
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1756186
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756187
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756188
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->f:Ljava/lang/String;

    .line 1756189
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1756190
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->c(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->g:Ljava/util/List;

    .line 1756191
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1756091
    const v0, 0x396913d9

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756092
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->h:Ljava/lang/String;

    .line 1756093
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1756073
    const v0, 0x2d45dd0b

    return v0
.end method

.method public final hW_()Z
    .locals 2

    .prologue
    .line 1756071
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1756072
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->i:Z

    return v0
.end method

.method public final hX_()I
    .locals 2

    .prologue
    .line 1756069
    const/4 v0, 0x0

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1756070
    iget v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->j:I

    return v0
.end method

.method public final synthetic j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756068
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->s()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic k()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756067
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->t()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic l()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756066
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->u()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    move-result-object v0

    return-object v0
.end method

.method public final m()J
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1756064
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1756065
    iget-wide v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->n:J

    return-wide v0
.end method

.method public final synthetic n()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756063
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->v()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    move-result-object v0

    return-object v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLStorySeenState;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756074
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    const/16 v1, 0xb

    const-class v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    .line 1756075
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->p:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    return-object v0
.end method

.method public final synthetic p()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756076
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->w()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    move-result-object v0

    return-object v0
.end method

.method public final q()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756077
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r:Ljava/lang/String;

    const/16 v1, 0xd

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r:Ljava/lang/String;

    .line 1756078
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final r()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756079
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    .line 1756080
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$AppIconModel;

    return-object v0
.end method

.method public final s()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756081
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    const/4 v1, 0x6

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    .line 1756082
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->k:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NavigationEndpointModel;

    return-object v0
.end method

.method public final t()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756083
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->l:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->l:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    .line 1756084
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->l:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    return-object v0
.end method

.method public final u()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756085
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->m:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    const/16 v1, 0x8

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->m:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    .line 1756086
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->m:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel;

    return-object v0
.end method

.method public final v()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756089
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->o:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    const/16 v1, 0xa

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->o:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    .line 1756090
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->o:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationModel;

    return-object v0
.end method

.method public final w()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756087
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->q:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    const/16 v1, 0xc

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->q:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    .line 1756088
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel;->q:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$TitleModel;

    return-object v0
.end method
