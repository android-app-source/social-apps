.class public final Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1763662
    const-class v0, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;

    new-instance v1, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1763663
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1763664
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1763665
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1763666
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1763667
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1763668
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1763669
    if-eqz p0, :cond_0

    .line 1763670
    const-string p2, "status"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1763671
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1763672
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1763673
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1763674
    check-cast p1, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel$Serializer;->a(Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;LX/0nX;LX/0my;)V

    return-void
.end method
