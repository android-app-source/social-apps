.class public final Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x17c3c1b4
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762650
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762649
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1762619
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762620
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1762646
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762647
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762648
    return-void
.end method

.method private a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762644
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    .line 1762645
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1762638
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762639
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1762640
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1762641
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1762642
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762643
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1762630
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762631
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1762632
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    .line 1762633
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1762634
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;

    .line 1762635
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsModel;

    .line 1762636
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762637
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1762629
    new-instance v0, LX/BDB;

    invoke-direct {v0, p1}, LX/BDB;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1762627
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1762628
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1762626
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1762623
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationStorySeenStateMutationFieldsConnectionModel$EdgesModel;-><init>()V

    .line 1762624
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762625
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1762622
    const v0, -0x3ec141be

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1762621
    const v0, -0x2163595b

    return v0
.end method
