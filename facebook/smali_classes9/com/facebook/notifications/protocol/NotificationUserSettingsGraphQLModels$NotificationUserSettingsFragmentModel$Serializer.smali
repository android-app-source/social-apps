.class public final Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1760831
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;

    new-instance v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1760832
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1760834
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1760835
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1760836
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1760837
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1760838
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1760839
    if-eqz v2, :cond_0

    .line 1760840
    const-string p0, "notif_option_sets"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760841
    invoke-static {v1, v2, p1, p2}, LX/BCu;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1760842
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1760843
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1760833
    check-cast p1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$Serializer;->a(Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;LX/0nX;LX/0my;)V

    return-void
.end method
