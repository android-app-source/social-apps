.class public final Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x4fb8276b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:J

.field private g:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1759185
    const-class v0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1759186
    const-class v0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1759189
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1759190
    return-void
.end method

.method private a()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1759187
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->e:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->e:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    .line 1759188
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->e:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    return-object v0
.end method

.method private j()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1759183
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->g:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->g:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    .line 1759184
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->g:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 7

    .prologue
    .line 1759174
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1759175
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->a()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1759176
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->j()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1759177
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1759178
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1759179
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->f:J

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1759180
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1759181
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1759182
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1759153
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1759154
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->a()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1759155
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->a()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    .line 1759156
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->a()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1759157
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;

    .line 1759158
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->e:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$ApplicationModel;

    .line 1759159
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->j()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1759160
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->j()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    .line 1759161
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->j()Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1759162
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;

    .line 1759163
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->g:Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel$OwnerModel;

    .line 1759164
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1759165
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    .line 1759166
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1759167
    const/4 v0, 0x1

    const-wide/16 v2, 0x0

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;->f:J

    .line 1759168
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1759169
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationSettingMutationsModels$PushNotificationsMuteMutationModel$PushTokenModel;-><init>()V

    .line 1759170
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1759171
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1759172
    const v0, -0x1608f6a9

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1759173
    const v0, 0x49049adf

    return v0
.end method
