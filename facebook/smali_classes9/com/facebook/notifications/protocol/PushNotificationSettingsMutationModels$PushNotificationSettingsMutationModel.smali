.class public final Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3747aa08
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1763693
    const-class v0, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1763692
    const-class v0, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1763690
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1763691
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1763685
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1763686
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1763687
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1763688
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1763689
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1763682
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1763683
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1763684
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1763694
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1763695
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;->e:Z

    .line 1763696
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1763680
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1763681
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1763677
    new-instance v0, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/PushNotificationSettingsMutationModels$PushNotificationSettingsMutationModel;-><init>()V

    .line 1763678
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1763679
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1763676
    const v0, 0x3c307005

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1763675
    const v0, 0x1e83c8d3

    return v0
.end method
