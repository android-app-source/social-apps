.class public final Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/BCO;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4ea87670
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760377
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760378
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1760379
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1760380
    return-void
.end method

.method private j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760381
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    .line 1760382
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    return-object v0
.end method

.method private k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760408
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    .line 1760409
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1760383
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760384
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1760385
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1760386
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1760387
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1760388
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1760389
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1760390
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1760391
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1760392
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1760393
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760394
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1760395
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760396
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1760397
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    .line 1760398
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1760399
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;

    .line 1760400
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    .line 1760401
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1760402
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    .line 1760403
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1760404
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;

    .line 1760405
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    .line 1760406
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760407
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760376
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/BCN;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760365
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel$ClientInfoModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1760373
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;-><init>()V

    .line 1760374
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1760375
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760371
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->f:Ljava/lang/String;

    .line 1760372
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760370
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionFragmentModel$OptionDisplayModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1760369
    const v0, -0x3d3473f

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760367
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->h:Ljava/lang/String;

    .line 1760368
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel$NodesModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1760366
    const v0, -0x4b30b85b

    return v0
.end method
