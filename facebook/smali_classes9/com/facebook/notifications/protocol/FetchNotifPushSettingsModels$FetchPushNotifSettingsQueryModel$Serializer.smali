.class public final Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1753692
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;

    new-instance v1, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1753693
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1753694
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1753695
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1753696
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1753697
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1753698
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1753699
    if-eqz v2, :cond_2

    .line 1753700
    const-string p0, "notif_android_settings"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1753701
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1753702
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->g(II)I

    move-result p0

    .line 1753703
    if-eqz p0, :cond_1

    .line 1753704
    const-string v0, "nodes"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1753705
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1753706
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, p0}, LX/15i;->c(I)I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 1753707
    invoke-virtual {v1, p0, v0}, LX/15i;->q(II)I

    move-result v2

    invoke-static {v1, v2, p1}, LX/BAf;->a(LX/15i;ILX/0nX;)V

    .line 1753708
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1753709
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1753710
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1753711
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1753712
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1753713
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$Serializer;->a(Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
