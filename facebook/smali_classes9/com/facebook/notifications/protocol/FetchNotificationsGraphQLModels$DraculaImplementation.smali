.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1755090
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1755091
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1755088
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1755089
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1755076
    if-nez p1, :cond_0

    .line 1755077
    :goto_0
    return v0

    .line 1755078
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1755079
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1755080
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 1755081
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1755082
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1755083
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1755084
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v1

    .line 1755085
    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1755086
    invoke-virtual {p3, v0, v1}, LX/186;->a(IZ)V

    .line 1755087
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7783fa08 -> :sswitch_0
        -0x5bdc0eb -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1755075
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1755072
    sparse-switch p0, :sswitch_data_0

    .line 1755073
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1755074
    :sswitch_0
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x7783fa08 -> :sswitch_0
        -0x5bdc0eb -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1755071
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1755069
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;->b(I)V

    .line 1755070
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1755064
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1755065
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1755066
    :cond_0
    iput-object p1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1755067
    iput p2, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;->b:I

    .line 1755068
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1755038
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1755063
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1755060
    iget v0, p0, LX/1vt;->c:I

    .line 1755061
    move v0, v0

    .line 1755062
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1755057
    iget v0, p0, LX/1vt;->c:I

    .line 1755058
    move v0, v0

    .line 1755059
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1755054
    iget v0, p0, LX/1vt;->b:I

    .line 1755055
    move v0, v0

    .line 1755056
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755051
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1755052
    move-object v0, v0

    .line 1755053
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1755042
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1755043
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1755044
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1755045
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1755046
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1755047
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1755048
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1755049
    invoke-static {v3, v9, v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1755050
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1755039
    iget v0, p0, LX/1vt;->c:I

    .line 1755040
    move v0, v0

    .line 1755041
    return v0
.end method
