.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x741265ad
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Z


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755433
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755436
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1755434
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1755435
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1755428
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755429
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1755430
    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;->e:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1755431
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755432
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1755437
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755438
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755439
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1755425
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1755426
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;->e:Z

    .line 1755427
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1755423
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1755424
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;->e:Z

    return v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1755418
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;-><init>()V

    .line 1755419
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1755420
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1755422
    const v0, -0x97473b4

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1755421
    const v0, 0x370fbffd

    return v0
.end method
