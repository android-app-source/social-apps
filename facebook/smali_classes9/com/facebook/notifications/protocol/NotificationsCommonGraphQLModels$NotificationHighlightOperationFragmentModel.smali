.class public final Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5f91105e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Z

.field private i:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762518
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762519
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1762520
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762521
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1762560
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762561
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762562
    return-void
.end method

.method public static a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;
    .locals 12

    .prologue
    .line 1762522
    if-nez p0, :cond_0

    .line 1762523
    const/4 p0, 0x0

    .line 1762524
    :goto_0
    return-object p0

    .line 1762525
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    if-eqz v0, :cond_1

    .line 1762526
    check-cast p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    goto :goto_0

    .line 1762527
    :cond_1
    new-instance v2, LX/BD5;

    invoke-direct {v2}, LX/BD5;-><init>()V

    .line 1762528
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1762529
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1762530
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;

    invoke-static {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1762531
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1762532
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/BD5;->a:LX/0Px;

    .line 1762533
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->b()Z

    move-result v0

    iput-boolean v0, v2, LX/BD5;->b:Z

    .line 1762534
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->c()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v0

    iput-object v0, v2, LX/BD5;->c:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    .line 1762535
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->d()Z

    move-result v0

    iput-boolean v0, v2, LX/BD5;->d:Z

    .line 1762536
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v0

    iput-object v0, v2, LX/BD5;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    .line 1762537
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v6, 0x0

    .line 1762538
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1762539
    iget-object v5, v2, LX/BD5;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1762540
    iget-object v7, v2, LX/BD5;->c:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    invoke-static {v4, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 1762541
    iget-object v9, v2, LX/BD5;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 1762542
    const/4 v10, 0x5

    invoke-virtual {v4, v10}, LX/186;->c(I)V

    .line 1762543
    invoke-virtual {v4, v11, v5}, LX/186;->b(II)V

    .line 1762544
    iget-boolean v5, v2, LX/BD5;->b:Z

    invoke-virtual {v4, v8, v5}, LX/186;->a(IZ)V

    .line 1762545
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v7}, LX/186;->b(II)V

    .line 1762546
    const/4 v5, 0x3

    iget-boolean v7, v2, LX/BD5;->d:Z

    invoke-virtual {v4, v5, v7}, LX/186;->a(IZ)V

    .line 1762547
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 1762548
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1762549
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1762550
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1762551
    invoke-virtual {v5, v11}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1762552
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1762553
    new-instance v5, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    invoke-direct {v5, v4}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;-><init>(LX/15i;)V

    .line 1762554
    move-object p0, v5

    .line 1762555
    goto/16 :goto_0
.end method

.method private j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762556
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->g:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->g:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    .line 1762557
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->g:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    return-object v0
.end method

.method private k()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762558
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->i:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->i:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    .line 1762559
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->i:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1762504
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762505
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1762506
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1762507
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->k()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1762508
    const/4 v3, 0x5

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1762509
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1762510
    const/4 v0, 0x1

    iget-boolean v3, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->f:Z

    invoke-virtual {p1, v0, v3}, LX/186;->a(IZ)V

    .line 1762511
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1762512
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->h:Z

    invoke-virtual {p1, v0, v1}, LX/186;->a(IZ)V

    .line 1762513
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1762514
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762515
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1762516
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e:Ljava/util/List;

    .line 1762517
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1762486
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762487
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1762488
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1762489
    if-eqz v1, :cond_3

    .line 1762490
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    .line 1762491
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1762492
    :goto_0
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1762493
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    .line 1762494
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1762495
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    .line 1762496
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->g:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    .line 1762497
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->k()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1762498
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->k()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    .line 1762499
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->k()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1762500
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    .line 1762501
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->i:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    .line 1762502
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762503
    if-nez v1, :cond_2

    :goto_1
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 1762482
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1762483
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->f:Z

    .line 1762484
    const/4 v0, 0x3

    invoke-virtual {p1, p2, v0}, LX/15i;->b(II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->h:Z

    .line 1762485
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1762479
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;-><init>()V

    .line 1762480
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762481
    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 1762477
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1762478
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->f:Z

    return v0
.end method

.method public final synthetic c()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762476
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$HideTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 1762474
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1762475
    iget-boolean v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->h:Z

    return v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1762473
    const v0, -0x335f2905    # -8.432636E7f

    return v0
.end method

.method public final synthetic e()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762472
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel;->k()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    move-result-object v0

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1762471
    const v0, 0x12c6ff7e

    return v0
.end method
