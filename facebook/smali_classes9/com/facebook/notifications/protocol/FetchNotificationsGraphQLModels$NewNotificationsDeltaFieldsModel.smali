.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x54b1710b
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755617
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755616
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1755614
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1755615
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1755604
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755605
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1755606
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1755607
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1755608
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1755609
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1755610
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1755611
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1755612
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755613
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1755596
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755597
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1755598
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    .line 1755599
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1755600
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    .line 1755601
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    .line 1755602
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755603
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755595
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1755592
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;-><init>()V

    .line 1755593
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1755594
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755590
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->f:Ljava/lang/String;

    .line 1755591
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755588
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->g:Ljava/lang/String;

    .line 1755589
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1755584
    const v0, -0x5650331c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1755587
    const v0, 0x34bbc3d0

    return v0
.end method

.method public final j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755585
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    .line 1755586
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    return-object v0
.end method
