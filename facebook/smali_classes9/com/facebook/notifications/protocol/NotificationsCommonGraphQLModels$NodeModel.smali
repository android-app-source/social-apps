.class public final Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x314c7ccc
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762046
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762061
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1762059
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762060
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1762056
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762057
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762058
    return-void
.end method

.method public static a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;
    .locals 2

    .prologue
    .line 1762047
    if-nez p0, :cond_0

    .line 1762048
    const/4 p0, 0x0

    .line 1762049
    :goto_0
    return-object p0

    .line 1762050
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    if-eqz v0, :cond_1

    .line 1762051
    check-cast p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    goto :goto_0

    .line 1762052
    :cond_1
    new-instance v0, LX/BD0;

    invoke-direct {v0}, LX/BD0;-><init>()V

    .line 1762053
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v1

    iput-object v1, v0, LX/BD0;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    .line 1762054
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->b()LX/BCQ;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;->a(LX/BCQ;)Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/BD0;->b:Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    .line 1762055
    invoke-virtual {v0}, LX/BD0;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1762015
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762016
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1762017
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->k()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1762018
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1762019
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1762020
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1762021
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762022
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1762033
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762034
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1762035
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    .line 1762036
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1762037
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    .line 1762038
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    .line 1762039
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->k()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1762040
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->k()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    .line 1762041
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->k()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1762042
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    .line 1762043
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->f:Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    .line 1762044
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762045
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762062
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b()LX/BCQ;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762032
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->k()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1762029
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;-><init>()V

    .line 1762030
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762031
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1762028
    const v0, 0x6c3a6944

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1762027
    const v0, 0x5639e7d

    return v0
.end method

.method public final j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762025
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    .line 1762026
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel;

    return-object v0
.end method

.method public final k()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762023
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->f:Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->f:Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    .line 1762024
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->f:Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    return-object v0
.end method
