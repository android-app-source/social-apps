.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/Flattenable;
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements LX/BAw;


# instance fields
.field private final a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

.field private final b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

.field private c:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756584
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    .line 1756585
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    .line 1756586
    return-void
.end method

.method public constructor <init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;)V
    .locals 1

    .prologue
    .line 1756579
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756580
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    .line 1756581
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    .line 1756582
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 1756578
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 1

    .prologue
    .line 1756577
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->a(LX/186;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->a(LX/186;)I

    move-result v0

    goto :goto_0
.end method

.method public final a()LX/2nq;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756570
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->c:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    if-nez v0, :cond_0

    .line 1756571
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1756572
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1756573
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;)V

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->c:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    .line 1756574
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->c:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    return-object v0

    .line 1756575
    :cond_1
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1756576
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    iget-object v1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel;)V

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->c:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    goto :goto_0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1756566
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756567
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1756568
    :goto_0
    return-void

    .line 1756569
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    goto :goto_0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1756562
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756563
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/nio/ByteBuffer;I)V

    .line 1756564
    :goto_0
    return-void

    .line 1756565
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756557
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final hY_()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756561
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;->hY_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1756560
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    goto :goto_0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756559
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    goto :goto_0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1756558
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->u_()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsDeltaFieldsModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsDeltaFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->u_()I

    move-result v0

    goto :goto_0
.end method
