.class public final Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xddab484
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760868
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760867
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1760865
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1760866
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1760859
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760860
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1760861
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1760862
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1760863
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760864
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1760851
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760852
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1760853
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    .line 1760854
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1760855
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;

    .line 1760856
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    .line 1760857
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760858
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760849
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    .line 1760850
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;->e:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1760846
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel;-><init>()V

    .line 1760847
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1760848
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1760844
    const v0, -0x717029df

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1760845
    const v0, -0x42354a06

    return v0
.end method
