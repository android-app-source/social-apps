.class public final Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;
.implements LX/BCR;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x40cad499
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760797
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760796
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1760794
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1760795
    return-void
.end method

.method private j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760792
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->f:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->f:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    .line 1760793
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->f:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    return-object v0
.end method

.method private k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760762
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    .line 1760763
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1760782
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760783
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1760784
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1760785
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1760786
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1760787
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1760788
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1760789
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1760790
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760791
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1760769
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760770
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1760771
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    .line 1760772
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1760773
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;

    .line 1760774
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->f:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    .line 1760775
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1760776
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    .line 1760777
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1760778
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;

    .line 1760779
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    .line 1760780
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760781
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760798
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1760766
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;-><init>()V

    .line 1760767
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1760768
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760764
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->e:Ljava/lang/String;

    .line 1760765
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()LX/BCP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760761
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->j()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel$NotifOptionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760760
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationUserSettingsFragmentModel$NotifOptionSetsModel$NodesModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1760759
    const v0, -0x187f770

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1760758
    const v0, 0x5639e7d

    return v0
.end method
