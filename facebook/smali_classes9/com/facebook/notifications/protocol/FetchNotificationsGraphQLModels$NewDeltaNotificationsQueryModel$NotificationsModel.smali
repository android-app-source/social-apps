.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x24743245
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755320
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755319
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1755317
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1755318
    return-void
.end method

.method private k()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755315
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    .line 1755316
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1755305
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755306
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->k()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1755307
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1755308
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1755309
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1755310
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1755311
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1755312
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1755313
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755314
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1755303
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->f:Ljava/util/List;

    .line 1755304
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1755285
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755286
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->k()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1755287
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->k()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    .line 1755288
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->k()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1755289
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    .line 1755290
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsDeltaConnectionFieldsModel;

    .line 1755291
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1755292
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v2

    .line 1755293
    if-eqz v2, :cond_1

    .line 1755294
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    .line 1755295
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1755296
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1755297
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    .line 1755298
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1755299
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    .line 1755300
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    .line 1755301
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755302
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1755282
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;-><init>()V

    .line 1755283
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1755284
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1755278
    const v0, -0x63c75521

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1755281
    const v0, 0x2152c5c6

    return v0
.end method

.method public final j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755279
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    .line 1755280
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;->g:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel$PageInfoModel;

    return-object v0
.end method
