.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7cc8b384
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755029
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755028
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1755026
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1755027
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1755020
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755021
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1755022
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1755023
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1755024
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755025
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1755030
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755031
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1755032
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    .line 1755033
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1755034
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;

    .line 1755035
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    .line 1755036
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755037
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNotificationStories"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755018
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    .line 1755019
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel$NotificationStoriesModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1755015
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$DeltaNotificationsQueryModel;-><init>()V

    .line 1755016
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1755017
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1755014
    const v0, -0x2d15b8c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1755013
    const v0, -0x6747e1ce

    return v0
.end method
