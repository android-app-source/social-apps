.class public final Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/BCR;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x250804de
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760516
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760515
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1760513
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1760514
    return-void
.end method

.method private j()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760510
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1760511
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1760512
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method private k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760508
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    .line 1760509
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    return-object v0
.end method

.method private l()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760506
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->h:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->h:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    .line 1760507
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->h:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1760494
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760495
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1760496
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1760497
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1760498
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->l()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1760499
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1760500
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1760501
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1760502
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1760503
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1760504
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760505
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1760481
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760482
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1760483
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    .line 1760484
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1760485
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;

    .line 1760486
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->g:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    .line 1760487
    :cond_0
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->l()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1760488
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->l()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    .line 1760489
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->l()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1760490
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;

    .line 1760491
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->h:Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    .line 1760492
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760493
    if-nez v1, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1760480
    new-instance v0, LX/BCc;

    invoke-direct {v0, p1}, LX/BCc;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760479
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1760467
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1760468
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1760478
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1760475
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;-><init>()V

    .line 1760476
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1760477
    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760473
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->f:Ljava/lang/String;

    .line 1760474
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic c()LX/BCP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760472
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->k()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$NotifOptionsModel;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic d()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760471
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;->l()Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel;

    move-result-object v0

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1760470
    const v0, -0x7c2add8f

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1760469
    const v0, 0x252222

    return v0
.end method
