.class public final Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1762421
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    new-instance v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1762422
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1762423
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1762424
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1762425
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    invoke-static {v1, v0, p1}, LX/BDR;->a(LX/15i;ILX/0nX;)V

    .line 1762426
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1762427
    check-cast p1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel$Serializer;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$ShowTextModel;LX/0nX;LX/0my;)V

    return-void
.end method
