.class public final Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1bff80e8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1761924
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1761960
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1761958
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1761959
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1761955
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1761956
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1761957
    return-void
.end method

.method public static a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;
    .locals 2

    .prologue
    .line 1761947
    if-nez p0, :cond_0

    .line 1761948
    const/4 p0, 0x0

    .line 1761949
    :goto_0
    return-object p0

    .line 1761950
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 1761951
    check-cast p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    goto :goto_0

    .line 1761952
    :cond_1
    new-instance v0, LX/BD2;

    invoke-direct {v0}, LX/BD2;-><init>()V

    .line 1761953
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v1

    iput-object v1, v0, LX/BD2;->a:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    .line 1761954
    invoke-virtual {v0}, LX/BD2;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1761941
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1761942
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1761943
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1761944
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1761945
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1761946
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1761933
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1761934
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1761935
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    .line 1761936
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1761937
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    .line 1761938
    iput-object v0, v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    .line 1761939
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1761940
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final synthetic a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1761932
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1761929
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;-><init>()V

    .line 1761930
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1761931
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1761928
    const v0, -0x5017fee2

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1761927
    const v0, -0x7c640d42

    return v0
.end method

.method public final j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1761925
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    .line 1761926
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel$NotifOptionsModel$EdgesModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel;

    return-object v0
.end method
