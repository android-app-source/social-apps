.class public final Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7e29a69d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1758147
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1758146
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1758144
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1758145
    return-void
.end method

.method private a()Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1758142
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    .line 1758143
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    return-object v0
.end method

.method private a(Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;)V
    .locals 3
    .param p1    # Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1758136
    iput-object p1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    .line 1758137
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1758138
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1758139
    if-eqz v0, :cond_0

    .line 1758140
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->a(IILcom/facebook/flatbuffers/Flattenable;)V

    .line 1758141
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1758130
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1758131
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1758132
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1758133
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1758134
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1758135
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1758148
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1758149
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1758150
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    .line 1758151
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1758152
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;

    .line 1758153
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    .line 1758154
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1758155
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1758129
    new-instance v0, LX/BBk;

    invoke-direct {v0, p1}, LX/BBk;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 2

    .prologue
    .line 1758121
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1758122
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    move-result-object v0

    .line 1758123
    if-eqz v0, :cond_0

    .line 1758124
    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1758125
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1758126
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1758127
    :goto_0
    return-void

    .line 1758128
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1758118
    const-string v0, "likers"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1758119
    check-cast p2, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    invoke-direct {p0, p2}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->a(Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;)V

    .line 1758120
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 1758109
    const-string v0, "likers.count"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1758110
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    move-result-object v0

    .line 1758111
    if-eqz v0, :cond_0

    .line 1758112
    if-eqz p3, :cond_1

    .line 1758113
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->t_()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    .line 1758114
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;->a(I)V

    .line 1758115
    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    .line 1758116
    :cond_0
    :goto_0
    return-void

    .line 1758117
    :cond_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;->a(I)V

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1758106
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel;-><init>()V

    .line 1758107
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1758108
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1758105
    const v0, -0x738516e7

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1758104
    const v0, -0x78fb05b

    return v0
.end method
