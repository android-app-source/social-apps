.class public final Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/BCQ;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x7e2c6c18
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1763594
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1763593
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1763591
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1763592
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1763588
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1763589
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1763590
    return-void
.end method

.method public static a(LX/BCQ;)Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;
    .locals 2

    .prologue
    .line 1763580
    if-nez p0, :cond_0

    .line 1763581
    const/4 p0, 0x0

    .line 1763582
    :goto_0
    return-object p0

    .line 1763583
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    if-eqz v0, :cond_1

    .line 1763584
    check-cast p0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    goto :goto_0

    .line 1763585
    :cond_1
    new-instance v0, LX/BDZ;

    invoke-direct {v0}, LX/BDZ;-><init>()V

    .line 1763586
    invoke-interface {p0}, LX/BCQ;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v1

    iput-object v1, v0, LX/BDZ;->a:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 1763587
    invoke-virtual {v0}, LX/BDZ;->a()Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1763595
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1763596
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;->c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1763597
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1763598
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1763599
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1763600
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1763577
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1763578
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1763579
    return-object p0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1763574
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;-><init>()V

    .line 1763575
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1763576
    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1763570
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    .line 1763571
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsOptionRowCommonGraphQLModels$StyleOnlyNotifOptionSetDisplayFragmentModel;->e:Lcom/facebook/graphql/enums/GraphQLNotifOptionRowSetDisplayStyle;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1763573
    const v0, 0x14e2d1a3

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1763572
    const v0, -0x69808a4e

    return v0
.end method
