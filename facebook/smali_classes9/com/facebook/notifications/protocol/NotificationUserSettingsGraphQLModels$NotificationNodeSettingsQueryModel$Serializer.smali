.class public final Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1760464
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;

    new-instance v1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1760465
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1760466
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1760442
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1760443
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1760444
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1760445
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1760446
    if-eqz v2, :cond_0

    .line 1760447
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760448
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1760449
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1760450
    if-eqz v2, :cond_1

    .line 1760451
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760452
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1760453
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1760454
    if-eqz v2, :cond_2

    .line 1760455
    const-string p0, "notif_options"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760456
    invoke-static {v1, v2, p1, p2}, LX/BCo;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1760457
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1760458
    if-eqz v2, :cond_3

    .line 1760459
    const-string p0, "option_set_display"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1760460
    invoke-static {v1, v2, p1, p2}, LX/BCk;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1760461
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1760462
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1760463
    check-cast p1, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel$Serializer;->a(Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotificationNodeSettingsQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
