.class public final Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1753596
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;

    new-instance v1, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1753597
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1753598
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1753599
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1753600
    const/4 v2, 0x0

    .line 1753601
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1753602
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1753603
    :goto_0
    move v1, v2

    .line 1753604
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1753605
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1753606
    new-instance v1, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/FetchNotifPushSettingsModels$FetchPushNotifSettingsQueryModel;-><init>()V

    .line 1753607
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1753608
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1753609
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1753610
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1753611
    :cond_0
    return-object v1

    .line 1753612
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1753613
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1753614
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1753615
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1753616
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, p0, :cond_2

    if-eqz v3, :cond_2

    .line 1753617
    const-string v4, "notif_android_settings"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1753618
    const/4 v3, 0x0

    .line 1753619
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1753620
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1753621
    :goto_2
    move v1, v3

    .line 1753622
    goto :goto_1

    .line 1753623
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1753624
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1753625
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1753626
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1753627
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, p0, :cond_8

    .line 1753628
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1753629
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1753630
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v4, :cond_6

    .line 1753631
    const-string p0, "nodes"

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1753632
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1753633
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, p0, :cond_7

    .line 1753634
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, p0, :cond_7

    .line 1753635
    invoke-static {p1, v0}, LX/BAf;->b(LX/15w;LX/186;)I

    move-result v4

    .line 1753636
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1753637
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1753638
    goto :goto_3

    .line 1753639
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1753640
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1753641
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3
.end method
