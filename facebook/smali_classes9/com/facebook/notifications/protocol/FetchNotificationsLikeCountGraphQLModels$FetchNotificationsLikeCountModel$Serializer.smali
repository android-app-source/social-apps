.class public final Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1758181
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel;

    new-instance v1, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1758182
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1758180
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1758158
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1758159
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1758160
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1758161
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1758162
    if-eqz v2, :cond_0

    .line 1758163
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1758164
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1758165
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1758166
    if-eqz v2, :cond_1

    .line 1758167
    const-string p0, "attached_story"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1758168
    invoke-static {v1, v2, p1, p2}, LX/BBp;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1758169
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1758170
    if-eqz v2, :cond_2

    .line 1758171
    const-string p0, "feedback"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1758172
    invoke-static {v1, v2, p1, p2}, LX/BBr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1758173
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1758174
    if-eqz v2, :cond_3

    .line 1758175
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1758176
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1758177
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1758178
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1758179
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$Serializer;->a(Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel;LX/0nX;LX/0my;)V

    return-void
.end method
