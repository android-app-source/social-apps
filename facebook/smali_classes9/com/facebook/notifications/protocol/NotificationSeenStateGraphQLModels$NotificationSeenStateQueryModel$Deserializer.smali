.class public final Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1758910
    const-class v0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;

    new-instance v1, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1758911
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1758909
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1758836
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1758837
    const/4 v2, 0x0

    .line 1758838
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1758839
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758840
    :goto_0
    move v1, v2

    .line 1758841
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1758842
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1758843
    new-instance v1, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$NotificationSeenStateQueryModel;-><init>()V

    .line 1758844
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1758845
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1758846
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1758847
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1758848
    :cond_0
    return-object v1

    .line 1758849
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758850
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1758851
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1758852
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1758853
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1758854
    const-string v4, "notif_readness"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1758855
    const/4 v3, 0x0

    .line 1758856
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v4, :cond_9

    .line 1758857
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758858
    :goto_2
    move v1, v3

    .line 1758859
    goto :goto_1

    .line 1758860
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1758861
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1758862
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1758863
    :cond_5
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758864
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 1758865
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 1758866
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1758867
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 1758868
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1758869
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1758870
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 1758871
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 1758872
    const/4 v5, 0x0

    .line 1758873
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 1758874
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758875
    :goto_5
    move v4, v5

    .line 1758876
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1758877
    :cond_7
    invoke-static {v1, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v1

    move v1, v1

    .line 1758878
    goto :goto_3

    .line 1758879
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, LX/186;->c(I)V

    .line 1758880
    invoke-virtual {v0, v3, v1}, LX/186;->b(II)V

    .line 1758881
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_9
    move v1, v3

    goto :goto_3

    .line 1758882
    :cond_a
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758883
    :cond_b
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 1758884
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1758885
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1758886
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 1758887
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 1758888
    const/4 v6, 0x0

    .line 1758889
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v7, :cond_12

    .line 1758890
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758891
    :goto_7
    move v4, v6

    .line 1758892
    goto :goto_6

    .line 1758893
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1758894
    invoke-virtual {v0, v5, v4}, LX/186;->b(II)V

    .line 1758895
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6

    .line 1758896
    :cond_e
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758897
    :cond_f
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_11

    .line 1758898
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1758899
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1758900
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_f

    if-eqz v8, :cond_f

    .line 1758901
    const-string p0, "notif_id"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_10

    .line 1758902
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_8

    .line 1758903
    :cond_10
    const-string p0, "seen_state"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_e

    .line 1758904
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_8

    .line 1758905
    :cond_11
    const/4 v8, 0x2

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1758906
    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 1758907
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v4}, LX/186;->b(II)V

    .line 1758908
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto :goto_7

    :cond_12
    move v4, v6

    move v7, v6

    goto :goto_8
.end method
