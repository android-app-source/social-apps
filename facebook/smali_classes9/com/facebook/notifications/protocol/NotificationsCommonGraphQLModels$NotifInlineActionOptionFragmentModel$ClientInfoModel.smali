.class public final Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/BCS;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x68acc3d6
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762169
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762168
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1762166
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762167
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1762163
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762164
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762165
    return-void
.end method

.method public static a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;
    .locals 4

    .prologue
    .line 1762148
    if-nez p0, :cond_0

    .line 1762149
    const/4 p0, 0x0

    .line 1762150
    :goto_0
    return-object p0

    .line 1762151
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    if-eqz v0, :cond_1

    .line 1762152
    check-cast p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    goto :goto_0

    .line 1762153
    :cond_1
    new-instance v2, LX/BD4;

    invoke-direct {v2}, LX/BD4;-><init>()V

    .line 1762154
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    iput-object v0, v2, LX/BD4;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1762155
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v0

    iput-object v0, v2, LX/BD4;->b:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 1762156
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1762157
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1762158
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BCR;

    invoke-static {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;->a(LX/BCR;)Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1762159
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1762160
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/BD4;->c:LX/0Px;

    .line 1762161
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/BD4;->d:Ljava/lang/String;

    .line 1762162
    invoke-virtual {v2}, LX/BD4;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 5

    .prologue
    .line 1762117
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762118
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1762119
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1762120
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1762121
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1762122
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1762123
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1762124
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1762125
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1762126
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1762127
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762128
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1762140
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762141
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1762142
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1762143
    if-eqz v1, :cond_0

    .line 1762144
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    .line 1762145
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->g:Ljava/util/List;

    .line 1762146
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762147
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762170
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1762171
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1762172
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final b()Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762138
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->f:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->f:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    .line 1762139
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->f:Lcom/facebook/graphql/enums/GraphQLNotifOptionClientActionType;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1762135
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;-><init>()V

    .line 1762136
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762137
    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1762133
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->h:Ljava/lang/String;

    .line 1762134
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1762132
    const v0, -0x733cb713

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1762131
    const v0, -0x63ae99da

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1762129
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->g:Ljava/util/List;

    const/4 v1, 0x2

    const-class v2, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->g:Ljava/util/List;

    .line 1762130
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotifInlineActionOptionFragmentModel$ClientInfoModel;->g:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method
