.class public final Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1762810
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel;

    new-instance v1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1762811
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1762812
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1762813
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1762814
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1762815
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1762816
    const/4 p0, 0x0

    invoke-virtual {v1, v0, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1762817
    if-eqz p0, :cond_0

    .line 1762818
    const-string p2, "cache_id"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1762819
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1762820
    :cond_0
    const/4 p0, 0x1

    invoke-virtual {v1, v0, p0}, LX/15i;->b(II)Z

    move-result p0

    .line 1762821
    if-eqz p0, :cond_1

    .line 1762822
    const-string p2, "local_is_rich_notif_collapsed"

    invoke-virtual {p1, p2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1762823
    invoke-virtual {p1, p0}, LX/0nX;->a(Z)V

    .line 1762824
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1762825
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1762826
    check-cast p1, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel$Serializer;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$RichNotificationCollapsedMutationFieldsModel;LX/0nX;LX/0my;)V

    return-void
.end method
