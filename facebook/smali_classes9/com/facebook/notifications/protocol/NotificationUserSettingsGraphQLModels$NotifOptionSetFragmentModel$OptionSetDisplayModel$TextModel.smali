.class public final Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/174;
.implements LX/4aa;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4f259da
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithInlineStylesFieldsModel$InlineStyleRangesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760108
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1760107
    const-class v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1760105
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1760106
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1760102
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1760103
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1760104
    return-void
.end method

.method public static a(Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;)Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;
    .locals 11

    .prologue
    .line 1760075
    if-nez p0, :cond_0

    .line 1760076
    const/4 p0, 0x0

    .line 1760077
    :goto_0
    return-object p0

    .line 1760078
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    if-eqz v0, :cond_1

    .line 1760079
    check-cast p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    goto :goto_0

    .line 1760080
    :cond_1
    new-instance v2, LX/BCb;

    invoke-direct {v2}, LX/BCb;-><init>()V

    .line 1760081
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1760082
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1760083
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithInlineStylesFieldsModel$InlineStyleRangesModel;

    invoke-static {v0}, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithInlineStylesFieldsModel$InlineStyleRangesModel;->a(Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithInlineStylesFieldsModel$InlineStyleRangesModel;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithInlineStylesFieldsModel$InlineStyleRangesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1760084
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1760085
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/BCb;->a:LX/0Px;

    .line 1760086
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, LX/BCb;->b:Ljava/lang/String;

    .line 1760087
    const/4 v8, 0x1

    const/4 v10, 0x0

    const/4 v6, 0x0

    .line 1760088
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 1760089
    iget-object v5, v2, LX/BCb;->a:LX/0Px;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v5

    .line 1760090
    iget-object v7, v2, LX/BCb;->b:Ljava/lang/String;

    invoke-virtual {v4, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1760091
    const/4 v9, 0x2

    invoke-virtual {v4, v9}, LX/186;->c(I)V

    .line 1760092
    invoke-virtual {v4, v10, v5}, LX/186;->b(II)V

    .line 1760093
    invoke-virtual {v4, v8, v7}, LX/186;->b(II)V

    .line 1760094
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 1760095
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 1760096
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 1760097
    invoke-virtual {v5, v10}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1760098
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1760099
    new-instance v5, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    invoke-direct {v5, v4}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;-><init>(LX/15i;)V

    .line 1760100
    move-object p0, v5

    .line 1760101
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1760067
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760068
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->b()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1760069
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1760070
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1760071
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1760072
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1760073
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760074
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1760059
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1760060
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1760061
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1760062
    if-eqz v1, :cond_0

    .line 1760063
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    .line 1760064
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->e:Ljava/util/List;

    .line 1760065
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1760066
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1760050
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->f:Ljava/lang/String;

    .line 1760051
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithInlineStylesFieldsModel$InlineStyleRangesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1760057
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesWithInlineStylesFieldsModel$InlineStyleRangesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->e:Ljava/util/List;

    .line 1760058
    iget-object v0, p0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1760054
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationUserSettingsGraphQLModels$NotifOptionSetFragmentModel$OptionSetDisplayModel$TextModel;-><init>()V

    .line 1760055
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1760056
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1760053
    const v0, 0x19f9059a

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1760052
    const v0, -0x726d476c

    return v0
.end method
