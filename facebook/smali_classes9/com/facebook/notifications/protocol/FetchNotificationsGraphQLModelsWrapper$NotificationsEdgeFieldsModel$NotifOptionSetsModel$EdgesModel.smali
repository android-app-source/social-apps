.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/Flattenable;
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements LX/BAx;


# instance fields
.field public final a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

.field public final b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756886
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756887
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    .line 1756888
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    .line 1756889
    return-void
.end method

.method public constructor <init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;)V
    .locals 1

    .prologue
    .line 1756882
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756883
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    .line 1756884
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    .line 1756885
    return-void
.end method

.method public constructor <init>(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)V
    .locals 1

    .prologue
    .line 1756878
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1756879
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    .line 1756880
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    .line 1756881
    return-void
.end method

.method public static a(LX/BAx;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;
    .locals 2

    .prologue
    .line 1756870
    if-nez p0, :cond_0

    .line 1756871
    const/4 p0, 0x0

    .line 1756872
    :goto_0
    return-object p0

    .line 1756873
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    if-eqz v0, :cond_1

    .line 1756874
    check-cast p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    goto :goto_0

    .line 1756875
    :cond_1
    new-instance v0, LX/BBH;

    invoke-direct {v0}, LX/BBH;-><init>()V

    .line 1756876
    invoke-interface {p0}, LX/BAx;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/BBH;->a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;)LX/BBH;

    .line 1756877
    invoke-virtual {v0}, LX/BBH;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    move-result-object p0

    goto :goto_0
.end method

.method public static e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z
    .locals 1

    .prologue
    .line 1756869
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 1

    .prologue
    .line 1756890
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;->a(LX/186;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0, p1}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a(LX/186;)I

    move-result v0

    goto :goto_0
.end method

.method public final a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756868
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->j()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NodeModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1756864
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756865
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1756866
    :goto_0
    return-void

    .line 1756867
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    goto :goto_0
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1756860
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1756861
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/nio/ByteBuffer;I)V

    .line 1756862
    :goto_0
    return-void

    .line 1756863
    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/nio/ByteBuffer;I)V

    goto :goto_0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1756857
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    goto :goto_0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1756859
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v0

    goto :goto_0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1756858
    invoke-static {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->e(Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->b:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->u_()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->u_()I

    move-result v0

    goto :goto_0
.end method
