.class public final Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1758535
    const-class v0, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;

    new-instance v1, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1758536
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1758537
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1758538
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1758539
    const/4 v2, 0x0

    .line 1758540
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1758541
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758542
    :goto_0
    move v1, v2

    .line 1758543
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1758544
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1758545
    new-instance v1, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;-><init>()V

    .line 1758546
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1758547
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1758548
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1758549
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1758550
    :cond_0
    return-object v1

    .line 1758551
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1758552
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, p0, :cond_3

    .line 1758553
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1758554
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1758555
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_2

    if-eqz v3, :cond_2

    .line 1758556
    const-string p0, "notification"

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1758557
    invoke-static {p1, v0}, LX/BDJ;->a(LX/15w;LX/186;)I

    move-result v1

    goto :goto_1

    .line 1758558
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1758559
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1758560
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1
.end method
