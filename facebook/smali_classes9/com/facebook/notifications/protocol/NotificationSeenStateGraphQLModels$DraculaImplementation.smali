.class public final Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1758827
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1758828
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1758829
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1758830
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1758746
    if-nez p1, :cond_0

    .line 1758747
    :goto_0
    return v0

    .line 1758748
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1758749
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1758750
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1758751
    const v2, 0x739897c2

    const/4 v5, 0x0

    .line 1758752
    if-nez v1, :cond_1

    move v3, v5

    .line 1758753
    :goto_1
    move v1, v3

    .line 1758754
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1758755
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1758756
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1758757
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1758758
    const v2, 0xb1ec991

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1758759
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1758760
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1758761
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1758762
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1758763
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1758764
    invoke-virtual {p0, p1, v4}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    .line 1758765
    invoke-virtual {p3, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    .line 1758766
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1758767
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1758768
    invoke-virtual {p3, v4, v2}, LX/186;->b(II)V

    .line 1758769
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1758770
    :cond_1
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result p1

    .line 1758771
    if-nez p1, :cond_2

    const/4 v3, 0x0

    .line 1758772
    :goto_2
    if-ge v5, p1, :cond_3

    .line 1758773
    invoke-virtual {p0, v1, v5}, LX/15i;->q(II)I

    move-result p2

    .line 1758774
    invoke-static {p0, p2, v2, p3}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result p2

    aput p2, v3, v5

    .line 1758775
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1758776
    :cond_2
    new-array v3, p1, [I

    goto :goto_2

    .line 1758777
    :cond_3
    const/4 v5, 0x1

    invoke-virtual {p3, v3, v5}, LX/186;->a([IZ)I

    move-result v3

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x369cbd9c -> :sswitch_0
        0xb1ec991 -> :sswitch_2
        0x739897c2 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1758826
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1758813
    sparse-switch p2, :sswitch_data_0

    .line 1758814
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1758815
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1758816
    const v1, 0x739897c2

    .line 1758817
    if-eqz v0, :cond_0

    .line 1758818
    invoke-virtual {p0, v0}, LX/15i;->d(I)I

    move-result p1

    .line 1758819
    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    .line 1758820
    invoke-virtual {p0, v0, v2}, LX/15i;->q(II)I

    move-result p2

    .line 1758821
    invoke-static {p0, p2, v1, p3}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    .line 1758822
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1758823
    :cond_0
    :goto_1
    :sswitch_1
    return-void

    .line 1758824
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1758825
    const v1, 0xb1ec991

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->c(LX/15i;IILX/1jy;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x369cbd9c -> :sswitch_0
        0xb1ec991 -> :sswitch_1
        0x739897c2 -> :sswitch_2
    .end sparse-switch
.end method

.method public static c(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1758807
    if-eqz p1, :cond_0

    .line 1758808
    invoke-static {p0, p1, p2}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1758809
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;

    .line 1758810
    if-eq v0, v1, :cond_0

    .line 1758811
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1758812
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1758806
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1758804
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1758805
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1758831
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1758832
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1758833
    :cond_0
    iput-object p1, p0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1758834
    iput p2, p0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->b:I

    .line 1758835
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1758803
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1758802
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1758799
    iget v0, p0, LX/1vt;->c:I

    .line 1758800
    move v0, v0

    .line 1758801
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1758796
    iget v0, p0, LX/1vt;->c:I

    .line 1758797
    move v0, v0

    .line 1758798
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1758793
    iget v0, p0, LX/1vt;->b:I

    .line 1758794
    move v0, v0

    .line 1758795
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1758790
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1758791
    move-object v0, v0

    .line 1758792
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1758781
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1758782
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1758783
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1758784
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1758785
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1758786
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1758787
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1758788
    invoke-static {v3, v9, v2}, Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/notifications/protocol/NotificationSeenStateGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1758789
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1758778
    iget v0, p0, LX/1vt;->c:I

    .line 1758779
    move v0, v0

    .line 1758780
    return v0
.end method
