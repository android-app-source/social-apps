.class public final Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1754083
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel;

    new-instance v1, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1754084
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1754085
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1754086
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1754087
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1754088
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1754089
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1754090
    if-eqz v2, :cond_0

    .line 1754091
    const-string p0, "notifications"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1754092
    invoke-static {v1, v2, p1, p2}, LX/BAr;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 1754093
    :cond_0
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1754094
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1754095
    check-cast p1, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel$Serializer;->a(Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
