.class public final Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3d6a0038
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1758574
    const-class v0, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1758575
    const-class v0, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1758576
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1758577
    return-void
.end method

.method private a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1758578
    iget-object v0, p0, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    .line 1758579
    iget-object v0, p0, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1758580
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1758581
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1758582
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1758583
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1758584
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1758585
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1758586
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1758587
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1758588
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    .line 1758589
    invoke-direct {p0}, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1758590
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;

    .line 1758591
    iput-object v0, v1, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;->e:Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NewNotificationStoryReadStateMutationFieldsModel;

    .line 1758592
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1758593
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1758594
    new-instance v0, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/MarkNotificationsSeenReadMutationGraphQLModels$NewMarkNotificationReadMutationModel;-><init>()V

    .line 1758595
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1758596
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1758597
    const v0, -0x49229834

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1758598
    const v0, 0x6c52b3ff

    return v0
.end method
