.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4e7c405d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755480
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755479
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1755477
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1755478
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1755469
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755470
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1755471
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1755472
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1755473
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1755474
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1755475
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755476
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1755467
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->e:Ljava/util/List;

    .line 1755468
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1755447
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755448
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1755449
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1755450
    if-eqz v1, :cond_2

    .line 1755451
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;

    .line 1755452
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->e:Ljava/util/List;

    move-object v1, v0

    .line 1755453
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1755454
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    .line 1755455
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1755456
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;

    .line 1755457
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->f:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    .line 1755458
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755459
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1755464
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;-><init>()V

    .line 1755465
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1755466
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1755463
    const v0, 0x2e769faa

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1755462
    const v0, 0x2152c5c6

    return v0
.end method

.method public final j()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755460
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->f:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->f:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    .line 1755461
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel;->f:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewFirstNotificationsQueryModel$NotificationsModel$PageInfoModel;

    return-object v0
.end method
