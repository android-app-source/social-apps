.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x32384fc8
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755861
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755860
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1755858
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1755859
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1755852
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755853
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1755854
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1755855
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1755856
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755857
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1755849
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755851
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755842
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;->e:Ljava/lang/String;

    .line 1755843
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1755846
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewNotificationsEdgeFieldsModel$NodeModel$NotifImageModel;-><init>()V

    .line 1755847
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1755848
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1755845
    const v0, 0x46b65c7c

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1755844
    const v0, 0x437b93b

    return v0
.end method
