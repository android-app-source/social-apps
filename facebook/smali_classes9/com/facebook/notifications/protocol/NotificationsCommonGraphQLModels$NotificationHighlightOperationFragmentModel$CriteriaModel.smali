.class public final Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x4ff4100
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:J

.field private g:J


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762322
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1762321
    const-class v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1762319
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762320
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1762316
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1762317
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762318
    return-void
.end method

.method public static a(Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;)Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;
    .locals 4

    .prologue
    .line 1762306
    if-nez p0, :cond_0

    .line 1762307
    const/4 p0, 0x0

    .line 1762308
    :goto_0
    return-object p0

    .line 1762309
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;

    if-eqz v0, :cond_1

    .line 1762310
    check-cast p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;

    goto :goto_0

    .line 1762311
    :cond_1
    new-instance v0, LX/BD6;

    invoke-direct {v0}, LX/BD6;-><init>()V

    .line 1762312
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->a()I

    move-result v1

    iput v1, v0, LX/BD6;->a:I

    .line 1762313
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->b()J

    move-result-wide v2

    iput-wide v2, v0, LX/BD6;->b:J

    .line 1762314
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->c()J

    move-result-wide v2

    iput-wide v2, v0, LX/BD6;->c:J

    .line 1762315
    invoke-virtual {v0}, LX/BD6;->a()Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1762304
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1762305
    iget v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 1762297
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762298
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1762299
    iget v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1762300
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->f:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1762301
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->g:J

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 1762302
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762303
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1762280
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1762281
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1762282
    return-object p0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1762292
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1762293
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->e:I

    .line 1762294
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->f:J

    .line 1762295
    const/4 v0, 0x2

    invoke-virtual {p1, p2, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->g:J

    .line 1762296
    return-void
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 1762290
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1762291
    iget-wide v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->f:J

    return-wide v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1762287
    new-instance v0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;-><init>()V

    .line 1762288
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1762289
    return-object v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 1762285
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1762286
    iget-wide v0, p0, Lcom/facebook/notifications/protocol/NotificationsCommonGraphQLModels$NotificationHighlightOperationFragmentModel$CriteriaModel;->g:J

    return-wide v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1762284
    const v0, -0x52cb8fe

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1762283
    const v0, -0x3971c748

    return v0
.end method
