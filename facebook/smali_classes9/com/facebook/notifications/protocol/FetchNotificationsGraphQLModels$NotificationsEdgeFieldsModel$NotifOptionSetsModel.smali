.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;
.implements LX/BAy;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x3f9e83b9
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1756517
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1756516
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756514
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1756515
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1756511
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1756512
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1756513
    return-void
.end method

.method public static a(LX/BAy;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;
    .locals 4

    .prologue
    .line 1756499
    if-nez p0, :cond_0

    .line 1756500
    const/4 p0, 0x0

    .line 1756501
    :goto_0
    return-object p0

    .line 1756502
    :cond_0
    instance-of v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    if-eqz v0, :cond_1

    .line 1756503
    check-cast p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    goto :goto_0

    .line 1756504
    :cond_1
    new-instance v2, LX/BBA;

    invoke-direct {v2}, LX/BBA;-><init>()V

    .line 1756505
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1756506
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, LX/BAy;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1756507
    invoke-interface {p0}, LX/BAy;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BAx;

    invoke-static {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;->a(LX/BAx;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1756508
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1756509
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, v2, LX/BBA;->a:LX/0Px;

    .line 1756510
    invoke-virtual {v2}, LX/BBA;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1756478
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1756479
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1756480
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1756481
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1756482
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1756483
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1756497
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->e:Ljava/util/List;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel$EdgesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->e:Ljava/util/List;

    .line 1756498
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->e:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1756489
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1756490
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1756491
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1756492
    if-eqz v1, :cond_0

    .line 1756493
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    .line 1756494
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->e:Ljava/util/List;

    .line 1756495
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1756496
    if-nez v0, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v0

    goto :goto_0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1756486
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NotificationsEdgeFieldsModel$NotifOptionSetsModel;-><init>()V

    .line 1756487
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1756488
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1756485
    const v0, 0x62fb9d64

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1756484
    const v0, -0x5c9f8c52

    return v0
.end method
