.class public final Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x135db188
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755350
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1755349
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1755347
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1755348
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1755341
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755342
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1755343
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1755344
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1755345
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755346
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1755351
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1755352
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1755353
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    .line 1755354
    invoke-virtual {p0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1755355
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;

    .line 1755356
    iput-object v0, v1, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    .line 1755357
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1755358
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1755339
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    .line 1755340
    iget-object v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;->e:Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel$NotificationsModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1755336
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModels$NewDeltaNotificationsQueryModel;-><init>()V

    .line 1755337
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1755338
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1755335
    const v0, -0xd3349c1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1755334
    const v0, -0x6747e1ce

    return v0
.end method
