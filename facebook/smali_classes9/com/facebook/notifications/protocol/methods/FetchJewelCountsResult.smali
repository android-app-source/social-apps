.class public Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnoreProperties;
    b = true
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResultDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final mMessagesCounts:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "messages"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mNotificationsCounts:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "notification_counts"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mRequestsCounts:Ljava/util/Map;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "friend_requests_counts"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1763723
    const-class v0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResultDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763722
    new-instance v0, LX/BDh;

    invoke-direct {v0}, LX/BDh;-><init>()V

    sput-object v0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1763704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763705
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1763706
    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mMessagesCounts:Ljava/util/Map;

    .line 1763707
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1763708
    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mNotificationsCounts:Ljava/util/Map;

    .line 1763709
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 1763710
    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mRequestsCounts:Ljava/util/Map;

    .line 1763711
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1763717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1763718
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mMessagesCounts:Ljava/util/Map;

    .line 1763719
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mNotificationsCounts:Ljava/util/Map;

    .line 1763720
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mRequestsCounts:Ljava/util/Map;

    .line 1763721
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1763716
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1763712
    iget-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mMessagesCounts:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1763713
    iget-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mNotificationsCounts:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1763714
    iget-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchJewelCountsResult;->mRequestsCounts:Ljava/util/Map;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1763715
    return-void
.end method
