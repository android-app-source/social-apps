.class public Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/notifications/model/SMSNotificationURL;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763800
    new-instance v0, LX/BDk;

    invoke-direct {v0}, LX/BDk;-><init>()V

    sput-object v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1763797
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 1763798
    const-class v0, Lcom/facebook/notifications/model/SMSNotificationURL;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/SMSNotificationURL;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;->a:Lcom/facebook/notifications/model/SMSNotificationURL;

    .line 1763799
    return-void
.end method

.method public constructor <init>(Lcom/facebook/notifications/model/SMSNotificationURL;LX/0ta;J)V
    .locals 1

    .prologue
    .line 1763790
    invoke-direct {p0, p2, p3, p4}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 1763791
    iput-object p1, p0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;->a:Lcom/facebook/notifications/model/SMSNotificationURL;

    .line 1763792
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1763796
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1763793
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1763794
    iget-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchNotificationURIResult;->a:Lcom/facebook/notifications/model/SMSNotificationURL;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1763795
    return-void
.end method
