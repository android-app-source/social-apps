.class public Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;
.super Lcom/facebook/fbservice/results/BaseResult;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/notifications/model/NotificationSeenStates;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1763760
    new-instance v0, LX/BDi;

    invoke-direct {v0}, LX/BDi;-><init>()V

    sput-object v0, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/0ta;Lcom/facebook/notifications/model/NotificationSeenStates;J)V
    .locals 1

    .prologue
    .line 1763757
    invoke-direct {p0, p1, p3, p4}, Lcom/facebook/fbservice/results/BaseResult;-><init>(LX/0ta;J)V

    .line 1763758
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/NotificationSeenStates;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;->a:Lcom/facebook/notifications/model/NotificationSeenStates;

    .line 1763759
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1763750
    invoke-direct {p0, p1}, Lcom/facebook/fbservice/results/BaseResult;-><init>(Landroid/os/Parcel;)V

    .line 1763751
    const-class v0, Lcom/facebook/notifications/model/NotificationSeenStates;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/model/NotificationSeenStates;

    iput-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;->a:Lcom/facebook/notifications/model/NotificationSeenStates;

    .line 1763752
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1763756
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1763753
    invoke-super {p0, p1, p2}, Lcom/facebook/fbservice/results/BaseResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1763754
    iget-object v0, p0, Lcom/facebook/notifications/protocol/methods/FetchNotificationSeenStatesResult;->a:Lcom/facebook/notifications/model/NotificationSeenStates;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1763755
    return-void
.end method
