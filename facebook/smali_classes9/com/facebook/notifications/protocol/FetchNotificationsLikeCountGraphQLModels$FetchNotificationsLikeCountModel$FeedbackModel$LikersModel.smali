.class public final Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x66c20030
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel$Serializer;
.end annotation


# instance fields
.field private e:I


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1758090
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1758091
    const-class v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1758067
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1758068
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1758088
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1758089
    iget v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1758083
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1758084
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 1758085
    iget v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;->e:I

    invoke-virtual {p1, v1, v0, v1}, LX/186;->a(III)V

    .line 1758086
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1758087
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1758092
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1758093
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1758094
    return-object p0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 1758077
    iput p1, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;->e:I

    .line 1758078
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1758079
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1758080
    if-eqz v0, :cond_0

    .line 1758081
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p1}, LX/15i;->b(III)V

    .line 1758082
    :cond_0
    return-void
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1758074
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1758075
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;->e:I

    .line 1758076
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1758071
    new-instance v0, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;

    invoke-direct {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsLikeCountGraphQLModels$FetchNotificationsLikeCountModel$FeedbackModel$LikersModel;-><init>()V

    .line 1758072
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1758073
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1758070
    const v0, -0x37b9d2c8

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1758069
    const v0, 0x2bb653c8

    return v0
.end method
