.class public final Lcom/facebook/notifications/util/NotificationsUtils$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/BAy;

.field public final synthetic c:LX/1rn;


# direct methods
.method public constructor <init>(LX/1rn;Ljava/lang/String;LX/BAy;)V
    .locals 0

    .prologue
    .line 1764141
    iput-object p1, p0, Lcom/facebook/notifications/util/NotificationsUtils$5;->c:LX/1rn;

    iput-object p2, p0, Lcom/facebook/notifications/util/NotificationsUtils$5;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/notifications/util/NotificationsUtils$5;->b:LX/BAy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1764142
    iget-object v0, p0, Lcom/facebook/notifications/util/NotificationsUtils$5;->c:LX/1rn;

    iget-object v0, v0, LX/1rn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iget-object v1, p0, Lcom/facebook/notifications/util/NotificationsUtils$5;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/notifications/util/NotificationsUtils$5;->b:LX/BAy;

    const/4 v3, 0x0

    .line 1764143
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 1764144
    sget-object v5, LX/2A7;->t:LX/0U1;

    .line 1764145
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 1764146
    invoke-static {v2}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;->a(LX/BAy;)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel$NotifOptionSetsModel;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v0, v6, v7}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1764147
    sget-object v5, LX/2A7;->b:LX/0U1;

    .line 1764148
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 1764149
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    aput-object v1, v6, v3

    invoke-static {v5, v6}, LX/0uu;->a(Ljava/lang/String;[Ljava/lang/String;)LX/0ux;

    move-result-object v5

    .line 1764150
    iget-object v6, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v6, v6, LX/2A8;->b:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "NO_NOTIFY"

    const-string p0, "1"

    invoke-virtual {v6, v7, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 1764151
    :try_start_0
    iget-object v7, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v6, v4, p0, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1764152
    :goto_0
    return-void

    .line 1764153
    :catch_0
    move-exception v4

    .line 1764154
    iget-object v5, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v6, "GraphQLNotificationsContentProviderHelper_updateSeenStateSessionNumber"

    const-string v7, "Exception thrown when attempting to update selected option id"

    invoke-virtual {v5, v6, v7, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
