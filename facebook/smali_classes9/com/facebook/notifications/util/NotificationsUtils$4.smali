.class public final Lcom/facebook/notifications/util/NotificationsUtils$4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:LX/1rn;


# direct methods
.method public constructor <init>(LX/1rn;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1764126
    iput-object p1, p0, Lcom/facebook/notifications/util/NotificationsUtils$4;->c:LX/1rn;

    iput-object p2, p0, Lcom/facebook/notifications/util/NotificationsUtils$4;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/facebook/notifications/util/NotificationsUtils$4;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1764127
    iget-object v0, p0, Lcom/facebook/notifications/util/NotificationsUtils$4;->c:LX/1rn;

    iget-object v0, v0, LX/1rn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iget-object v1, p0, Lcom/facebook/notifications/util/NotificationsUtils$4;->a:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/facebook/notifications/util/NotificationsUtils$4;->b:Z

    const/4 v4, 0x0

    .line 1764128
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1764129
    sget-object v3, LX/2A7;->s:LX/0U1;

    .line 1764130
    iget-object v6, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v6

    .line 1764131
    if-eqz v2, :cond_0

    const/4 v3, 0x1

    :goto_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1764132
    sget-object v3, LX/2A7;->b:LX/0U1;

    .line 1764133
    iget-object v6, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v6

    .line 1764134
    invoke-static {v3, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v3

    .line 1764135
    iget-object v6, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v6, v6, LX/2A8;->b:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "NO_NOTIFY"

    const-string p0, "1"

    invoke-virtual {v6, v7, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 1764136
    :try_start_0
    iget-object v7, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    invoke-virtual {v3}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v6, v5, p0, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1764137
    :goto_1
    return-void

    :cond_0
    move v3, v4

    .line 1764138
    goto :goto_0

    .line 1764139
    :catch_0
    move-exception v3

    .line 1764140
    iget-object v5, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v6, "GraphQLNotificationsContentProviderHelper_updateIsRichNotifCollapsed"

    const-string v7, "Exception thrown when attempting to update is rich notif collapsed"

    invoke-virtual {v5, v6, v7, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
