.class public final Lcom/facebook/notifications/util/NotificationsUtils$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

.field public final synthetic c:LX/1rn;


# direct methods
.method public constructor <init>(LX/1rn;Ljava/lang/String;Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)V
    .locals 0

    .prologue
    .line 1764111
    iput-object p1, p0, Lcom/facebook/notifications/util/NotificationsUtils$3;->c:LX/1rn;

    iput-object p2, p0, Lcom/facebook/notifications/util/NotificationsUtils$3;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/notifications/util/NotificationsUtils$3;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1764112
    iget-object v0, p0, Lcom/facebook/notifications/util/NotificationsUtils$3;->c:LX/1rn;

    iget-object v0, v0, LX/1rn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iget-object v1, p0, Lcom/facebook/notifications/util/NotificationsUtils$3;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/notifications/util/NotificationsUtils$3;->b:Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    .line 1764113
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 1764114
    sget-object v4, LX/2A7;->r:LX/0U1;

    .line 1764115
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1764116
    invoke-static {v2}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->a(Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;)Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v0, v5, v6}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;Lcom/facebook/flatbuffers/Flattenable;LX/16a;)[B

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1764117
    sget-object v4, LX/2A7;->b:LX/0U1;

    .line 1764118
    iget-object v5, v4, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v5

    .line 1764119
    invoke-static {v4, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v4

    .line 1764120
    iget-object v5, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e:LX/2A8;

    iget-object v5, v5, LX/2A8;->b:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    const-string v6, "NO_NOTIFY"

    const-string p0, "1"

    invoke-virtual {v5, v6, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 1764121
    :try_start_0
    iget-object v6, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->f:Landroid/content/ContentResolver;

    invoke-virtual {v4}, LX/0ux;->a()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v4}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v5, v3, p0, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1764122
    :goto_0
    return-void

    .line 1764123
    :catch_0
    move-exception v3

    .line 1764124
    iget-object v4, v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->j:LX/03V;

    const-string v5, "GraphQLNotificationsContentProviderHelper_updateReactionUnit"

    const-string v6, "Exception thrown when attempting to update reactionUnit"

    invoke-virtual {v4, v5, v6, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1764125
    goto :goto_0
.end method
