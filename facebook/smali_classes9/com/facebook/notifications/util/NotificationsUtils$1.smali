.class public final Lcom/facebook/notifications/util/NotificationsUtils$1;
.super Lcom/facebook/common/executors/NamedRunnable;
.source ""


# instance fields
.field public final synthetic c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public final synthetic d:LX/1rn;


# direct methods
.method public constructor <init>(LX/1rn;Ljava/lang/Class;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 0

    .prologue
    .line 1764093
    iput-object p1, p0, Lcom/facebook/notifications/util/NotificationsUtils$1;->d:LX/1rn;

    iput-object p4, p0, Lcom/facebook/notifications/util/NotificationsUtils$1;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-direct {p0, p2, p3}, Lcom/facebook/common/executors/NamedRunnable;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1764094
    iget-object v0, p0, Lcom/facebook/notifications/util/NotificationsUtils$1;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1764095
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1764096
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 1764097
    iget-object v0, p0, Lcom/facebook/notifications/util/NotificationsUtils$1;->d:LX/1rn;

    iget-object v0, v0, LX/1rn;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->e(J)LX/0Px;

    move-result-object v0

    .line 1764098
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1764099
    iget-object v1, p0, Lcom/facebook/notifications/util/NotificationsUtils$1;->d:LX/1rn;

    iget-object v1, v1, LX/1rn;->g:LX/1ro;

    invoke-virtual {v1, v0}, LX/1ro;->a(Ljava/util/List;)V

    .line 1764100
    iget-object v1, p0, Lcom/facebook/notifications/util/NotificationsUtils$1;->d:LX/1rn;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    iget-object v3, p0, Lcom/facebook/notifications/util/NotificationsUtils$1;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-virtual {v1, v0, v2, v3}, LX/1rn;->a(Ljava/util/List;Lcom/facebook/graphql/enums/GraphQLStorySeenState;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 1764101
    :cond_0
    return-void
.end method
