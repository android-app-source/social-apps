.class public final Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Rf;

.field public final synthetic b:Ljava/util/List;

.field public final synthetic c:LX/CSD;


# direct methods
.method public constructor <init>(LX/CSD;LX/0Rf;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 1894327
    iput-object p1, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$2;->c:LX/CSD;

    iput-object p2, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$2;->a:LX/0Rf;

    iput-object p3, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$2;->b:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 1894328
    iget-object v0, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$2;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HYO;

    .line 1894329
    iget-object v1, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$2;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1894330
    iget-object v4, v0, LX/HYO;->c:LX/3Cm;

    invoke-virtual {v4, v1}, LX/3Cm;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 1894331
    iget-object v4, v0, LX/HYO;->f:LX/19j;

    iget-boolean v4, v4, LX/19j;->q:Z

    if-eqz v4, :cond_2

    iget-object v4, v0, LX/HYO;->f:LX/19j;

    iget-boolean v4, v4, LX/19j;->ag:Z

    if-nez v4, :cond_1

    iget-object v4, v0, LX/HYO;->g:LX/1b4;

    .line 1894332
    iget-object v5, v4, LX/1b4;->a:LX/0ad;

    sget-short v6, LX/1v6;->K:S

    const/4 v7, 0x1

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    move v4, v5

    .line 1894333
    if-eqz v4, :cond_2

    .line 1894334
    :cond_1
    iget-object v4, v0, LX/HYO;->d:LX/3DB;

    invoke-static {v1}, LX/3Cm;->e(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v5

    .line 1894335
    iget-object v6, v4, LX/3DB;->d:LX/3DC;

    sget-object v7, LX/379;->NOTIFICATION:LX/379;

    invoke-virtual {v6, v5, v7}, LX/3DC;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;LX/379;)Z

    .line 1894336
    :cond_2
    :goto_1
    goto :goto_0

    .line 1894337
    :cond_3
    return-void

    .line 1894338
    :cond_4
    invoke-static {v1}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLComment;

    move-result-object v4

    if-nez v4, :cond_2

    .line 1894339
    iget-object v4, v0, LX/HYO;->f:LX/19j;

    iget-boolean v4, v4, LX/19j;->q:Z

    if-eqz v4, :cond_5

    .line 1894340
    iget-object v4, v0, LX/HYO;->d:LX/3DB;

    invoke-virtual {v4, v1}, LX/3DB;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1894341
    :cond_5
    iget-object v4, v0, LX/HYO;->a:Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;

    invoke-virtual {v4, v1}, Lcom/facebook/prefetch/notifications/NotificationStoryImagesPrefetcher;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1894342
    iget-object v4, v0, LX/HYO;->b:LX/HYL;

    invoke-virtual {v4, v1}, LX/HYL;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1
.end method
