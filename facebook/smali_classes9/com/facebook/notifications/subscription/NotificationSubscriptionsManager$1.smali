.class public final Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Rf;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/CSD;


# direct methods
.method public constructor <init>(LX/CSD;LX/0Rf;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 1894312
    iput-object p1, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$1;->c:LX/CSD;

    iput-object p2, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$1;->a:LX/0Rf;

    iput-object p3, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1894313
    iget-object v0, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$1;->a:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D2V;

    .line 1894314
    iget-object v2, p0, Lcom/facebook/notifications/subscription/NotificationSubscriptionsManager$1;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1894315
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 1894316
    const-string v4, "context_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1894317
    :cond_0
    :goto_1
    goto :goto_0

    .line 1894318
    :cond_1
    return-void

    .line 1894319
    :cond_2
    :try_start_1
    const-string v4, "context_id"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, LX/D2V;->a:LX/D2W;

    iget-object v4, v4, LX/D2W;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1894320
    iget-object v3, v0, LX/D2V;->a:LX/D2W;

    iget-object v3, v3, LX/D2W;->d:LX/D2T;

    iget-object v4, v0, LX/D2V;->a:LX/D2W;

    iget-object v4, v4, LX/D2W;->b:Ljava/lang/String;

    .line 1894321
    invoke-static {v3, v4}, LX/D2T;->f(LX/D2T;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1894322
    invoke-static {v3}, LX/D2T;->c(LX/D2T;)V

    .line 1894323
    :cond_3
    iget-object v3, v0, LX/D2V;->a:LX/D2W;

    iget-object v3, v3, LX/D2W;->e:LX/BQP;

    invoke-virtual {v3}, LX/BQP;->e()V

    .line 1894324
    iget-object v3, v0, LX/D2V;->a:LX/D2W;

    iget-object v3, v3, LX/D2W;->e:LX/BQP;

    invoke-virtual {v3}, LX/BQP;->b()V

    .line 1894325
    iget-object v3, v0, LX/D2V;->a:LX/D2W;

    invoke-static {v3}, LX/D2W;->c(LX/D2W;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 1894326
    :catch_0
    goto :goto_1
.end method
