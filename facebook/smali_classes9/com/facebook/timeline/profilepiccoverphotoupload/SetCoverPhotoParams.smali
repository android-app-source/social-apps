.class public Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:J

.field public final b:Landroid/graphics/RectF;

.field public final c:LX/434;

.field public final d:Ljava/lang/String;

.field public final e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1782607
    new-instance v0, LX/BQh;

    invoke-direct {v0}, LX/BQh;-><init>()V

    sput-object v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Landroid/graphics/RectF;LX/434;J)V
    .locals 0

    .prologue
    .line 1782600
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782601
    iput-object p3, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d:Ljava/lang/String;

    .line 1782602
    iput-wide p1, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->a:J

    .line 1782603
    iput-object p4, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->b:Landroid/graphics/RectF;

    .line 1782604
    iput-object p5, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->c:LX/434;

    .line 1782605
    iput-wide p6, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e:J

    .line 1782606
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    .line 1782593
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782594
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->a:J

    .line 1782595
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d:Ljava/lang/String;

    .line 1782596
    const-class v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->b:Landroid/graphics/RectF;

    .line 1782597
    new-instance v0, LX/434;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/434;-><init>(II)V

    iput-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->c:LX/434;

    .line 1782598
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e:J

    .line 1782599
    return-void
.end method


# virtual methods
.method public final d()F
    .locals 1

    .prologue
    .line 1782581
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1782592
    const/4 v0, 0x0

    return v0
.end method

.method public final e()F
    .locals 1

    .prologue
    .line 1782591
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 4

    .prologue
    .line 1782590
    iget-wide v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1782589
    invoke-static {p0}, LX/0kk;->toStringHelper(Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "method"

    const-string v2, "SetCoverPhotoMethod"

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "profileId"

    iget-wide v2, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->a:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    const-string v1, "photoFilePath"

    iget-object v2, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "normalizedBounds"

    iget-object v2, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->b:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "uncroppedDimensions"

    iget-object v2, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->c:LX/434;

    invoke-virtual {v0, v1, v2}, LX/237;->add(Ljava/lang/String;Ljava/lang/Object;)LX/237;

    move-result-object v0

    const-string v1, "photoId"

    iget-wide v2, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e:J

    invoke-virtual {v0, v1, v2, v3}, LX/237;->add(Ljava/lang/String;J)LX/237;

    move-result-object v0

    invoke-virtual {v0}, LX/237;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1782582
    iget-wide v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1782583
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1782584
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->b:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1782585
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->c:LX/434;

    iget v0, v0, LX/434;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1782586
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->c:LX/434;

    iget v0, v0, LX/434;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1782587
    iget-wide v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1782588
    return-void
.end method
