.class public Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/8LV;

.field public final c:LX/1EZ;

.field public final d:Landroid/content/Context;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/BQg;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQQ;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQP;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQX;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/9fo;

.field public final k:LX/0TD;

.field public final l:LX/0TD;

.field public final m:LX/0SG;

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7ma;",
            ">;"
        }
    .end annotation
.end field

.field private final o:LX/0ad;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1782513
    const-class v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/8LV;LX/1EZ;LX/0Or;LX/BQg;LX/0SG;LX/0Ot;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/9fo;LX/0TD;LX/0TD;)V
    .locals 0
    .param p13    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p14    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForegroundExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/8LV;",
            "LX/1EZ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/BQg;",
            "LX/0SG;",
            "LX/0Ot",
            "<",
            "LX/7ma;",
            ">;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/BQQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BQP;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BQX;",
            ">;",
            "LX/9fo;",
            "LX/0TD;",
            "LX/0TD;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782496
    iput-object p1, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->d:Landroid/content/Context;

    .line 1782497
    iput-object p2, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->b:LX/8LV;

    .line 1782498
    iput-object p3, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->c:LX/1EZ;

    .line 1782499
    iput-object p4, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->e:LX/0Or;

    .line 1782500
    iput-object p5, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->f:LX/BQg;

    .line 1782501
    iput-object p6, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->m:LX/0SG;

    .line 1782502
    iput-object p7, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->n:LX/0Ot;

    .line 1782503
    iput-object p8, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->o:LX/0ad;

    .line 1782504
    iput-object p9, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->g:LX/0Ot;

    .line 1782505
    iput-object p10, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->h:LX/0Ot;

    .line 1782506
    iput-object p11, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->i:LX/0Ot;

    .line 1782507
    iput-object p12, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->j:LX/9fo;

    .line 1782508
    iput-object p13, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->l:LX/0TD;

    .line 1782509
    iput-object p14, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->k:LX/0TD;

    .line 1782510
    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Z)V
    .locals 10
    .param p1    # Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1782514
    invoke-virtual {p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1782515
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQQ;

    invoke-virtual {v0, p2, p3}, LX/BQQ;->b(Landroid/net/Uri;Ljava/lang/String;)V

    .line 1782516
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->k:LX/0TD;

    new-instance p4, LX/BQe;

    invoke-direct {p4, p0, p1}, LX/BQe;-><init>(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;)V

    invoke-interface {v0, p4}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1782517
    new-instance p4, LX/BQf;

    invoke-direct {p4, p0, p3}, LX/BQf;-><init>(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;Ljava/lang/String;)V

    iget-object p5, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->l:LX/0TD;

    invoke-static {v0, p4, p5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1782518
    :goto_0
    return-void

    .line 1782519
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->d:Landroid/content/Context;

    const v2, 0x7f08273d

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1782520
    new-instance v1, LX/74k;

    invoke-direct {v1}, LX/74k;-><init>()V

    .line 1782521
    iget-object v2, p1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d:Ljava/lang/String;

    move-object v2, v2

    .line 1782522
    invoke-virtual {v1, v2}, LX/74k;->a(Ljava/lang/String;)LX/74k;

    move-result-object v1

    const-string v2, "image/jpeg"

    invoke-virtual {v1, v2}, LX/74k;->c(Ljava/lang/String;)LX/74k;

    move-result-object v1

    invoke-virtual {v1}, LX/74k;->a()Lcom/facebook/photos/base/media/PhotoItem;

    move-result-object v3

    .line 1782523
    new-instance v1, LX/23u;

    invoke-direct {v1}, LX/23u;-><init>()V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-static {v2}, LX/ATz;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 1782524
    iput-object v2, v1, LX/23u;->k:LX/0Px;

    .line 1782525
    move-object v1, v1

    .line 1782526
    iput-object p3, v1, LX/23u;->t:Ljava/lang/String;

    .line 1782527
    move-object v1, v1

    .line 1782528
    iget-object v2, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->m:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v5

    const-wide/16 v7, 0x3e8

    div-long/2addr v5, v7

    .line 1782529
    iput-wide v5, v1, LX/23u;->v:J

    .line 1782530
    move-object v1, v1

    .line 1782531
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1782532
    new-instance v4, Lcom/facebook/composer/publish/common/PostParamsWrapper;

    new-instance v1, LX/5M9;

    invoke-direct {v1}, LX/5M9;-><init>()V

    .line 1782533
    iput-object p3, v1, LX/5M9;->G:Ljava/lang/String;

    .line 1782534
    move-object v1, v1

    .line 1782535
    invoke-virtual {v1}, LX/5M9;->a()Lcom/facebook/composer/publish/common/PublishPostParams;

    move-result-object v1

    invoke-direct {v4, v1}, Lcom/facebook/composer/publish/common/PostParamsWrapper;-><init>(Lcom/facebook/composer/publish/common/PublishPostParams;)V

    .line 1782536
    invoke-static {}, Lcom/facebook/composer/publish/common/PublishAttemptInfo;->newBuilder()Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/PublishAttemptInfo$Builder;->a()Lcom/facebook/composer/publish/common/PublishAttemptInfo;

    move-result-object v5

    .line 1782537
    iget-object v1, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7ma;

    new-instance v6, LX/7ml;

    new-instance v7, LX/5M1;

    new-instance v8, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;

    invoke-direct {v8, v2, v4, v5}, Lcom/facebook/composer/publish/common/PendingStoryPersistentData;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/composer/publish/common/PostParamsWrapper;Lcom/facebook/composer/publish/common/PublishAttemptInfo;)V

    invoke-direct {v7, v8}, LX/5M1;-><init>(Lcom/facebook/composer/publish/common/PendingStoryPersistentData;)V

    invoke-virtual {v7}, LX/5M1;->a()Lcom/facebook/composer/publish/common/PendingStory;

    move-result-object v2

    sget-object v4, LX/7mm;->COVER_PHOTO:LX/7mm;

    invoke-direct {v6, v2, v4}, LX/7ml;-><init>(Lcom/facebook/composer/publish/common/PendingStory;LX/7mm;)V

    invoke-virtual {v1, v6}, LX/7mV;->a(LX/7mi;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1782538
    iget-object v1, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->b:LX/8LV;

    invoke-virtual {p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d()F

    move-result v4

    invoke-virtual {p1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->e()F

    move-result v5

    move-object v2, p4

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, LX/8LV;->a(Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ipc/media/MediaItem;FFLjava/lang/String;)Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v2

    .line 1782539
    iget-object v1, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/BQX;

    invoke-virtual {v1, p2, p3, p5}, LX/BQX;->a(Landroid/net/Uri;Ljava/lang/String;Z)V

    .line 1782540
    iget-object v1, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->c:LX/1EZ;

    invoke-virtual {v1, v2}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 1782541
    goto/16 :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;
    .locals 15

    .prologue
    .line 1782511
    new-instance v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v2

    check-cast v2, LX/8LV;

    invoke-static {p0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v3

    check-cast v3, LX/1EZ;

    const/16 v4, 0xb83

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/BQg;->a(LX/0QB;)LX/BQg;

    move-result-object v5

    check-cast v5, LX/BQg;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const/16 v7, 0x19ee

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    const/16 v9, 0x36a4

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x36a3

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x36a7

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const-class v12, LX/9fo;

    invoke-interface {p0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/9fo;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v13

    check-cast v13, LX/0TD;

    invoke-static {p0}, LX/0tb;->a(LX/0QB;)LX/0TD;

    move-result-object v14

    check-cast v14, LX/0TD;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;-><init>(Landroid/content/Context;LX/8LV;LX/1EZ;LX/0Or;LX/BQg;LX/0SG;LX/0Ot;LX/0ad;LX/0Ot;LX/0Ot;LX/0Ot;LX/9fo;LX/0TD;LX/0TD;)V

    .line 1782512
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;Lcom/facebook/auth/viewercontext/ViewerContext;Z)V
    .locals 11

    .prologue
    .line 1782487
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1782488
    iget-object v0, p0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->j:LX/9fo;

    invoke-virtual {v0, v3}, LX/9fo;->a(Ljava/lang/String;)LX/9fn;

    move-result-object v6

    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v7

    new-instance v0, Ljava/io/File;

    .line 1782489
    iget-object v1, p1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 1782490
    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v8

    .line 1782491
    iget-object v0, p1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->c:LX/434;

    move-object v9, v0

    .line 1782492
    iget-object v0, p1, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;->b:Landroid/graphics/RectF;

    move-object v10, v0

    .line 1782493
    new-instance v0, LX/BQd;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, LX/BQd;-><init>(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Z)V

    move-object v1, v6

    move-object v2, v7

    move-object v3, v8

    move-object v4, v9

    move-object v5, v10

    move-object v6, v0

    invoke-virtual/range {v1 .. v6}, LX/9fn;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/434;Landroid/graphics/RectF;LX/9fc;)V

    .line 1782494
    return-void
.end method
