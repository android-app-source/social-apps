.class public Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/5QV;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1783943
    new-instance v0, LX/BRN;

    invoke-direct {v0}, LX/BRN;-><init>()V

    sput-object v0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V
    .locals 0
    .param p1    # LX/5QV;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1783944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783945
    iput-object p1, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a:LX/5QV;

    .line 1783946
    iput-object p2, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1783947
    iput-object p3, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1783948
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1783927
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783928
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, LX/5QV;

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a:LX/5QV;

    .line 1783929
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    .line 1783930
    const-class v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1783931
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 1783942
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a:LX/5QV;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1783941
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/5QV;
    .locals 1

    .prologue
    .line 1783949
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1783950
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a:LX/5QV;

    return-object v0
.end method

.method public final d()Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;
    .locals 1

    .prologue
    .line 1783939
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1783940
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1783938
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1783937
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a:LX/5QV;

    invoke-interface {v0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()LX/BRO;
    .locals 1

    .prologue
    .line 1783936
    new-instance v0, LX/BRO;

    invoke-direct {v0, p0}, LX/BRO;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;)V

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1783932
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a:LX/5QV;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1783933
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b:Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1783934
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c:Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1783935
    return-void
.end method
