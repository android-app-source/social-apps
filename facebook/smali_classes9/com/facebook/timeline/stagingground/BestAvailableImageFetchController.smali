.class public Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/0tX;

.field public final c:Ljava/util/concurrent/Executor;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1782646
    const-class v0, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0tX;Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1782647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1782648
    iput-object p1, p0, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;->b:LX/0tX;

    .line 1782649
    iput-object p2, p0, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;->c:Ljava/util/concurrent/Executor;

    .line 1782650
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;
    .locals 3

    .prologue
    .line 1782651
    new-instance v2, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    invoke-direct {v2, v0, v1}, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;-><init>(LX/0tX;Ljava/util/concurrent/Executor;)V

    .line 1782652
    move-object v0, v2

    .line 1782653
    return-object v0
.end method
