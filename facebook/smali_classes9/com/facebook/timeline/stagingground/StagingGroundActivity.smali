.class public Lcom/facebook/timeline/stagingground/StagingGroundActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private final p:LX/B37;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1782754
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1782755
    new-instance v0, LX/BQn;

    invoke-direct {v0, p0}, LX/BQn;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundActivity;)V

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundActivity;->p:LX/B37;

    return-void
.end method

.method private a()Lcom/facebook/timeline/stagingground/StagingGroundFragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1782756
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "staging_ground_fragment_tag"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    return-object v0
.end method

.method public static c(Lcom/facebook/timeline/stagingground/StagingGroundActivity;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1782757
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/facebook/timeline/stagingground/StagingGroundActivity;->setResult(ILandroid/content/Intent;)V

    .line 1782758
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundActivity;->finish()V

    .line 1782759
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1782760
    instance-of v0, p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    if-eqz v0, :cond_0

    .line 1782761
    check-cast p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundActivity;->p:LX/B37;

    .line 1782762
    iput-object v0, p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->f:LX/B37;

    .line 1782763
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1782764
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1782765
    const v0, 0x7f0313ab

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/stagingground/StagingGroundActivity;->setContentView(I)V

    .line 1782766
    invoke-direct {p0}, Lcom/facebook/timeline/stagingground/StagingGroundActivity;->a()Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1782767
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->a(Landroid/content/Intent;)Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    move-result-object v0

    .line 1782768
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d2d6c

    const-string v3, "staging_ground_fragment_tag"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1782769
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 5

    .prologue
    .line 1782770
    invoke-direct {p0}, Lcom/facebook/timeline/stagingground/StagingGroundActivity;->a()Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    move-result-object v0

    .line 1782771
    if-eqz v0, :cond_1

    .line 1782772
    iget-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    .line 1782773
    iget-object v2, v1, LX/BQx;->q:LX/BR0;

    invoke-interface {v2}, LX/BR0;->f()V

    .line 1782774
    iget-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    .line 1782775
    iget-object v2, v1, LX/BQx;->q:LX/BR0;

    invoke-interface {v2}, LX/BR0;->c()Z

    move-result v2

    move v1, v2

    .line 1782776
    if-eqz v1, :cond_2

    .line 1782777
    iget-object v2, v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/facebook/base/activity/FbFragmentActivity;

    .line 1782778
    new-instance v3, LX/0ju;

    iget-object v4, v2, LX/BQx;->h:Landroid/content/Context;

    invoke-direct {v3, v4}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 1782779
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/0ju;->a(Z)LX/0ju;

    .line 1782780
    const v4, 0x7f08143b

    invoke-virtual {v3, v4}, LX/0ju;->a(I)LX/0ju;

    .line 1782781
    const v4, 0x7f081435

    invoke-virtual {v3, v4}, LX/0ju;->b(I)LX/0ju;

    .line 1782782
    const v4, 0x7f08143f

    iget-object v0, v2, LX/BQx;->q:LX/BR0;

    invoke-interface {v0, v1}, LX/BR0;->a(Lcom/facebook/base/activity/FbFragmentActivity;)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1782783
    const v4, 0x7f08143e

    new-instance v0, LX/BQw;

    invoke-direct {v0, v2}, LX/BQw;-><init>(LX/BQx;)V

    invoke-virtual {v3, v4, v0}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    .line 1782784
    invoke-virtual {v3}, LX/0ju;->a()LX/2EJ;

    move-result-object v3

    invoke-virtual {v3}, LX/2EJ;->show()V

    .line 1782785
    const/4 v1, 0x1

    .line 1782786
    :goto_0
    move v0, v1

    .line 1782787
    :goto_1
    if-nez v0, :cond_0

    .line 1782788
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1782789
    :cond_0
    return-void

    .line 1782790
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 1782791
    :cond_2
    iget-object v1, v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->b:LX/BQo;

    iget-object v2, v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    invoke-virtual {v2}, LX/BQx;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    invoke-virtual {v3}, LX/BQx;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->e:LX/BRP;

    .line 1782792
    iget-object v0, v4, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v4, v0

    .line 1782793
    invoke-virtual {v4}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v4

    .line 1782794
    const-string v0, "staging_ground_tap_cancel"

    invoke-static {v1, v0, v2, v3, v4}, LX/BQo;->c(LX/BQo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1782795
    const/4 v1, 0x0

    goto :goto_0
.end method
