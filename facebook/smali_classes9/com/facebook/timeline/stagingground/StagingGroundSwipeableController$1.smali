.class public final Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/BRL;


# direct methods
.method public constructor <init>(LX/BRL;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1783787
    iput-object p1, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->b:LX/BRL;

    iput-object p2, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 12

    .prologue
    .line 1783788
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->b:LX/BRL;

    iget-object v0, v0, LX/BRL;->c:LX/BRP;

    .line 1783789
    iget-object v1, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v0, v1

    .line 1783790
    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v0

    invoke-interface {v0}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v0

    .line 1783791
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->b:LX/BRL;

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->a:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->a:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->b:LX/BRL;

    .line 1783792
    iget-object v5, v4, LX/BRL;->c:LX/BRP;

    .line 1783793
    iget-object v6, v5, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v5, v6

    .line 1783794
    invoke-virtual {v5}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1783795
    iget-object v5, v4, LX/BRL;->c:LX/BRP;

    .line 1783796
    iget-object v6, v5, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v5, v6

    .line 1783797
    invoke-virtual {v5}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v5

    iget-object v6, v4, LX/BRL;->c:LX/BRP;

    .line 1783798
    iget-object v7, v6, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v6, v7

    .line 1783799
    invoke-virtual {v6}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->d()Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, LX/BRL;->b(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Ljava/lang/String;)LX/0Px;

    move-result-object v5

    .line 1783800
    :goto_0
    move-object v4, v5

    .line 1783801
    iget-object v5, v1, LX/BRL;->i:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 1783802
    iput v2, v5, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1783803
    iput v3, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1783804
    iget-object v6, v1, LX/BRL;->i:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v6, v5}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1783805
    iget-object v5, v1, LX/BRL;->i:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v5}, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;->requestLayout()V

    .line 1783806
    const/4 v10, 0x1

    .line 1783807
    iput-object v4, v1, LX/BRL;->m:LX/0Px;

    .line 1783808
    iget-object v5, v1, LX/BRL;->f:LX/8GP;

    invoke-virtual {v5, v2, v3}, LX/8GP;->a(II)LX/8GO;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/8GO;->c(LX/0Px;)LX/8GO;

    move-result-object v5

    invoke-virtual {v5}, LX/8GO;->b()LX/0Px;

    move-result-object v11

    .line 1783809
    iget-object v5, v1, LX/BRL;->e:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;

    .line 1783810
    iget-object v5, v1, LX/BRL;->d:LX/9d6;

    iget-object v6, v1, LX/BRL;->k:LX/BRK;

    const/4 v8, 0x0

    const-string v9, "-1"

    invoke-virtual/range {v5 .. v10}, LX/9d6;->a(LX/9bz;Lcom/facebook/photos/creativeediting/swipeable/composer/SwipeableDraweeControllerGeneratorImpl;Landroid/net/Uri;Ljava/lang/String;Z)LX/9d5;

    move-result-object v5

    iput-object v5, v1, LX/BRL;->l:LX/9d5;

    .line 1783811
    iget-object v5, v1, LX/BRL;->l:LX/9d5;

    iget-object v6, v1, LX/BRL;->i:Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    invoke-virtual {v5, v6, v2, v3, v10}, LX/9d5;->a(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;IIZ)V

    .line 1783812
    iget-object v5, v1, LX/BRL;->l:LX/9d5;

    invoke-virtual {v5, v11, v0}, LX/9d5;->a(LX/0Px;Ljava/lang/String;)V

    .line 1783813
    iget-object v5, v1, LX/BRL;->j:LX/8GL;

    invoke-virtual {v5, v10, v11, v0, v10}, LX/8GL;->a(ZLX/0Px;Ljava/lang/String;Z)V

    .line 1783814
    iget-object v5, v1, LX/BRL;->l:LX/9d5;

    iget-object v6, v1, LX/BRL;->j:LX/8GL;

    .line 1783815
    iget-object v7, v6, LX/8GL;->b:LX/8GH;

    move-object v6, v7

    .line 1783816
    invoke-virtual {v5, v6}, LX/9d5;->a(LX/8GH;)V

    .line 1783817
    iget-object v5, v1, LX/BRL;->l:LX/9d5;

    invoke-virtual {v5, v10}, LX/9d5;->b(Z)V

    .line 1783818
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->b:LX/BRL;

    iget-object v0, v0, LX/BRL;->c:LX/BRP;

    .line 1783819
    iget-object v1, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v0, v1

    .line 1783820
    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1783821
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->b:LX/BRL;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;->a:Landroid/view/View;

    .line 1783822
    invoke-static {v0}, LX/BRL;->c(LX/BRL;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 1783823
    new-instance v3, LX/BRI;

    invoke-direct {v3, v0, v1}, LX/BRI;-><init>(LX/BRL;Landroid/view/View;)V

    move-object v3, v3

    .line 1783824
    iget-object v4, v0, LX/BRL;->g:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1783825
    new-instance v4, LX/1Mv;

    invoke-direct {v4, v2, v3}, LX/1Mv;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    iput-object v4, v0, LX/BRL;->n:LX/1Mv;

    .line 1783826
    :cond_0
    return-void

    .line 1783827
    :cond_1
    iget-object v5, v4, LX/BRL;->c:LX/BRP;

    .line 1783828
    iget-object v6, v5, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v5, v6

    .line 1783829
    invoke-virtual {v5}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v5

    .line 1783830
    invoke-interface {v5}, LX/5QV;->c()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5}, LX/5QV;->d()Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v5

    invoke-static {v6, v5}, LX/B5P;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/photos/creativeediting/model/FrameGraphQLModels$FrameModel;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    goto/16 :goto_0
.end method
