.class public Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;
.super Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;
.source ""


# instance fields
.field private d:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1783078
    invoke-direct {p0, p1}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;-><init>(Landroid/content/Context;)V

    .line 1783079
    invoke-direct {p0}, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->b()V

    .line 1783080
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1783081
    invoke-direct {p0, p1, p2}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1783082
    invoke-direct {p0}, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->b()V

    .line 1783083
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1783084
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    .line 1783085
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a00e6

    invoke-static {v1, v2}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1783086
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1783087
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1783088
    invoke-super {p0, p1}, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;->onDraw(Landroid/graphics/Canvas;)V

    .line 1783089
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->getHeight()I

    move-result v0

    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1783090
    return-void
.end method
