.class public Lcom/facebook/timeline/stagingground/StagingGroundFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/BQp;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/BQo;

.field public c:LX/BQy;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/BQx;

.field public e:LX/BRP;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public f:LX/B37;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1783075
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1783076
    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/facebook/timeline/stagingground/StagingGroundFragment;
    .locals 2

    .prologue
    .line 1783070
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1783071
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 1783072
    new-instance v1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    invoke-direct {v1}, Lcom/facebook/timeline/stagingground/StagingGroundFragment;-><init>()V

    .line 1783073
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1783074
    return-object v1
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    const-class v1, LX/BQp;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/BQp;

    const-class v2, LX/BQy;

    invoke-interface {p0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/BQy;

    invoke-static {p0}, LX/BRP;->a(LX/0QB;)LX/BRP;

    move-result-object p0

    check-cast p0, LX/BRP;

    iput-object v1, p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->a:LX/BQp;

    iput-object v2, p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->c:LX/BQy;

    iput-object p0, p1, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->e:LX/BRP;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1783058
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1783059
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 1783060
    const-class v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    invoke-static {v0, p0}, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1783061
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->e:LX/BRP;

    .line 1783062
    if-nez p1, :cond_1

    .line 1783063
    const/4 p0, 0x0

    .line 1783064
    :goto_0
    move-object p0, p0

    .line 1783065
    if-nez p0, :cond_0

    .line 1783066
    invoke-static {}, LX/BRP;->c()Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-result-object p0

    .line 1783067
    :cond_0
    iput-object p0, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    .line 1783068
    return-void

    .line 1783069
    :cond_1
    const-string p0, "swipeable_model"

    invoke-virtual {p1, p0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object p0

    check-cast p0, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 1783049
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/fragment/FbFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 1783050
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 1783051
    iget-object p0, v0, LX/BQx;->q:LX/BR0;

    invoke-interface {p0, v1, p1, p2, p3}, LX/BR0;->a(Landroid/app/Activity;IILandroid/content/Intent;)V

    .line 1783052
    iget-object p0, v0, LX/BQx;->r:Lcom/facebook/fig/button/FigButton;

    if-eqz p0, :cond_0

    .line 1783053
    iget-object p0, v0, LX/BQx;->b:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/BQq;

    .line 1783054
    iget-object v1, v0, LX/BQx;->d:LX/BRP;

    .line 1783055
    iget-object p1, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, p1

    .line 1783056
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v1

    iget-object p1, v0, LX/BQx;->r:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {p0, v1, p1}, LX/BQq;->a(ZLcom/facebook/fig/button/FigButton;)V

    .line 1783057
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x1368bb05

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1783077
    const v1, 0x7f0313aa

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x1e9c4568

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6bc52040

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1783045
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    .line 1783046
    iget-object v2, v1, LX/BQx;->q:LX/BR0;

    invoke-interface {v2}, LX/BR0;->a()V

    .line 1783047
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1783048
    const/16 v1, 0x2b

    const v2, 0x1ba6c350

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x7d62ab7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1783041
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1783042
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    .line 1783043
    iget-object v2, v1, LX/BQx;->q:LX/BR0;

    invoke-interface {v2}, LX/BR0;->e()V

    .line 1783044
    const/16 v1, 0x2b

    const v2, -0x546d2948

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x6091e8a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1783037
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1783038
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    .line 1783039
    iget-object v2, v1, LX/BQx;->q:LX/BR0;

    invoke-interface {v2}, LX/BR0;->d()V

    .line 1783040
    const/16 v1, 0x2b

    const v2, 0x2288aaa0

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1783023
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->e:LX/BRP;

    .line 1783024
    const-string v1, "swipeable_model"

    iget-object v2, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1783025
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    .line 1783026
    iget-object v1, v0, LX/BQx;->s:LX/BR1;

    .line 1783027
    const-string v2, "mediaUriKey"

    iget-object v3, v1, LX/BR1;->a:Landroid/net/Uri;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1783028
    const-string v2, "unmodifiedFileSchemeUriKey"

    iget-object v3, v1, LX/BR1;->b:Landroid/net/Uri;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1783029
    const-string v2, "mediaCropBoxKey"

    iget-object v3, v1, LX/BR1;->c:Landroid/graphics/RectF;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1783030
    const-string v2, "captionKey"

    iget-object v3, v1, LX/BR1;->h:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783031
    iget-object v2, v1, LX/BR1;->d:LX/434;

    if-eqz v2, :cond_0

    .line 1783032
    const-string v2, "uncroppedMediaWidth"

    iget-object v3, v1, LX/BR1;->d:LX/434;

    iget v3, v3, LX/434;->b:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1783033
    const-string v2, "uncroppedMediaHeight"

    iget-object v3, v1, LX/BR1;->d:LX/434;

    iget v3, v3, LX/434;->a:I

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1783034
    :cond_0
    iget-object v1, v0, LX/BQx;->q:LX/BR0;

    invoke-interface {v1, p1}, LX/BR0;->a(Landroid/os/Bundle;)V

    .line 1783035
    const-string v1, "expirationKey"

    iget-object v2, v0, LX/BQx;->l:LX/BRg;

    invoke-virtual {v2}, LX/BRg;->a()J

    move-result-wide v3

    invoke-virtual {p1, v1, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1783036
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1783000
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1783001
    const-string v1, "key_staging_ground_launch_config"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;

    .line 1783002
    if-nez v0, :cond_1

    .line 1783003
    const-class v0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;

    const-string v1, "StagingGroundLaunchConfiguration must be set"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1783004
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1783005
    :cond_0
    :goto_0
    return-void

    .line 1783006
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->a:LX/BQp;

    .line 1783007
    iget-object v2, v0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->j:Ljava/lang/String;

    move-object v2, v2

    .line 1783008
    new-instance v4, LX/BQo;

    invoke-static {v1}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {v4, v3, v2}, LX/BQo;-><init>(LX/0Zb;Ljava/lang/String;)V

    .line 1783009
    move-object v1, v4

    .line 1783010
    iput-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->b:LX/BQo;

    .line 1783011
    if-eqz p2, :cond_2

    const-string v1, "expirationKey"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1783012
    const-string v1, "expirationKey"

    const-wide/16 v2, 0x0

    invoke-virtual {p2, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1783013
    :goto_1
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->c:LX/BQy;

    iget-object v4, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->b:LX/BQo;

    iget-object v5, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->f:LX/B37;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/BQy;->a(JLX/BQo;LX/B37;LX/0gc;)LX/BQx;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    .line 1783014
    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    const v1, 0x7f0d2d60

    invoke-virtual {p0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;

    invoke-virtual {v2, p0, p2, v0, v1}, LX/BQx;->a(Lcom/facebook/base/fragment/FbFragment;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;)V

    .line 1783015
    if-nez p2, :cond_0

    .line 1783016
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->b:LX/BQo;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    invoke-virtual {v1}, LX/BQx;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->d:LX/BQx;

    invoke-virtual {v2}, LX/BQx;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundFragment;->e:LX/BRP;

    .line 1783017
    iget-object v4, v3, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v3, v4

    .line 1783018
    invoke-virtual {v3}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v3

    .line 1783019
    const-string v4, "staging_ground_enter"

    invoke-static {v0, v4, v1, v2, v3}, LX/BQo;->c(LX/BQo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1783020
    goto :goto_0

    .line 1783021
    :cond_2
    iget-wide v7, v0, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->e:J

    move-wide v2, v7

    .line 1783022
    goto :goto_1
.end method
