.class public final Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1784128
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1784129
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1784130
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1784131
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1784132
    if-nez p1, :cond_0

    .line 1784133
    :goto_0
    return v0

    .line 1784134
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 1784135
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1784136
    :pswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1784137
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1784138
    const/4 v2, 0x1

    invoke-virtual {p3, v2}, LX/186;->c(I)V

    .line 1784139
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1784140
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x9f29a30
        :pswitch_0
    .end packed-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1784145
    new-instance v0, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static b(I)V
    .locals 1

    .prologue
    .line 1784141
    packed-switch p0, :pswitch_data_0

    .line 1784142
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1784143
    :pswitch_0
    return-void

    :pswitch_data_0
    .packed-switch 0x9f29a30
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1784144
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 1

    .prologue
    .line 1784121
    iget v0, p0, LX/1vt;->c:I

    invoke-static {v0}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;->b(I)V

    .line 1784122
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1784123
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1784124
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1784125
    :cond_0
    iput-object p1, p0, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;->a:LX/15i;

    .line 1784126
    iput p2, p0, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;->b:I

    .line 1784127
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1784120
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1784119
    new-instance v0, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1784116
    iget v0, p0, LX/1vt;->c:I

    .line 1784117
    move v0, v0

    .line 1784118
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1784113
    iget v0, p0, LX/1vt;->c:I

    .line 1784114
    move v0, v0

    .line 1784115
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1784110
    iget v0, p0, LX/1vt;->b:I

    .line 1784111
    move v0, v0

    .line 1784112
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1784107
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1784108
    move-object v0, v0

    .line 1784109
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1784098
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1784099
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1784100
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1784101
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1784102
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1784103
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1784104
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1784105
    invoke-static {v3, v9, v2}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1784106
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1784095
    iget v0, p0, LX/1vt;->c:I

    .line 1784096
    move v0, v0

    .line 1784097
    return v0
.end method
