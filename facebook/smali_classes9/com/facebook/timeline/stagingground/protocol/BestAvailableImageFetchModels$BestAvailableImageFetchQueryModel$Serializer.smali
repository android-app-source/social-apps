.class public final Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1784034
    const-class v0, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;

    new-instance v1, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1784035
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1784036
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1784037
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1784038
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1784039
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1784040
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1784041
    if-eqz v2, :cond_0

    .line 1784042
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1784043
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1784044
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1784045
    if-eqz v2, :cond_2

    .line 1784046
    const-string p0, "image"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1784047
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1784048
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1784049
    if-eqz p0, :cond_1

    .line 1784050
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1784051
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1784052
    :cond_1
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1784053
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1784054
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1784055
    check-cast p1, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel$Serializer;->a(Lcom/facebook/timeline/stagingground/protocol/BestAvailableImageFetchModels$BestAvailableImageFetchQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
