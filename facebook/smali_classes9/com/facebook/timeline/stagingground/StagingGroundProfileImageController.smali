.class public Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/BR0;


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;

.field public static final e:Ljava/lang/String;

.field public static final f:Landroid/graphics/RectF;


# instance fields
.field public A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public B:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;",
            ">;"
        }
    .end annotation
.end field

.field public C:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/timeline/stagingground/StagingGroundHScrollCirclePageIndicator;",
            ">;"
        }
    .end annotation
.end field

.field public D:Ljava/lang/String;

.field public E:Ljava/lang/String;

.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/editgallery/utils/FetchImageUtils;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

.field public h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

.field public i:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

.field public j:Z

.field public k:LX/BR1;

.field public l:Ljava/lang/String;

.field private m:LX/0Vd;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Vd",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/BRP;

.field public o:LX/9fn;

.field private final p:LX/BRM;

.field public q:LX/BRL;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final r:LX/8GV;

.field private final s:LX/B37;

.field public final t:Landroid/content/Context;

.field public final u:Lcom/facebook/base/fragment/FbFragment;

.field public final v:LX/03V;

.field public final w:LX/BQo;

.field private final x:Ljava/util/concurrent/Executor;

.field public final y:Lcom/facebook/content/SecureContextHelper;

.field public final z:Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 1783423
    const-class v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->e:Ljava/lang/String;

    .line 1783424
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->f:Landroid/graphics/RectF;

    .line 1783425
    const-class v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;

    const-string v1, "timeline"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/BRP;LX/9fo;Ljava/lang/String;Landroid/content/Context;LX/03V;LX/BRM;LX/8GV;Lcom/facebook/content/SecureContextHelper;Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;Ljava/util/concurrent/Executor;LX/BQo;LX/B37;Lcom/facebook/base/fragment/FbFragment;LX/BR1;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p10    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p11    # LX/BQo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p12    # LX/B37;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p13    # Lcom/facebook/base/fragment/FbFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p14    # LX/BR1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p15    # Landroid/os/Bundle;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p16    # Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 1783492
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1783493
    iput-object p4, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->t:Landroid/content/Context;

    .line 1783494
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->u:Lcom/facebook/base/fragment/FbFragment;

    .line 1783495
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    .line 1783496
    iput-object p5, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->v:LX/03V;

    .line 1783497
    iput-object p7, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->r:LX/8GV;

    .line 1783498
    iput-object p8, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->y:Lcom/facebook/content/SecureContextHelper;

    .line 1783499
    iput-object p10, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->x:Ljava/util/concurrent/Executor;

    .line 1783500
    iput-object p11, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    .line 1783501
    iput-object p1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783502
    iput-object p6, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->p:LX/BRM;

    .line 1783503
    invoke-virtual {p2, p3}, LX/9fo;->a(Ljava/lang/String;)LX/9fn;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->o:LX/9fn;

    .line 1783504
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->s:LX/B37;

    .line 1783505
    iput-object p9, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->z:Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;

    .line 1783506
    move-object/from16 v0, p13

    move-object/from16 v1, p15

    move-object/from16 v2, p16

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->a(Lcom/facebook/base/fragment/FbFragment;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V

    .line 1783507
    return-void
.end method

.method private a(Lcom/facebook/base/fragment/FbFragment;Landroid/os/Bundle;Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;)V
    .locals 2

    .prologue
    .line 1783469
    iget-object v0, p1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1783470
    const-string v1, "extra_edit_gallery_launch_settings"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->i:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    .line 1783471
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->i:Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    if-nez v0, :cond_0

    .line 1783472
    const-class v0, Lcom/facebook/timeline/stagingground/StagingGroundActivity;

    const-string v1, "EditGalleryLaunchConfiguration must be set"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 1783473
    invoke-virtual {p1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 1783474
    :goto_0
    return-void

    .line 1783475
    :cond_0
    if-eqz p2, :cond_1

    const-string v0, "creativeEditingDataKey"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1783476
    const-string v0, "creativeEditingDataKey"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, p0

    .line 1783477
    :goto_1
    iput-object v0, v1, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783478
    iget-object v0, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->c:Ljava/lang/String;

    move-object v0, v0

    .line 1783479
    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->l:Ljava/lang/String;

    .line 1783480
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v0, v0, LX/BR1;->c:Landroid/graphics/RectF;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v0, v0, LX/BR1;->d:LX/434;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->j:Z

    .line 1783481
    if-eqz p2, :cond_4

    const-string v0, "didEnterCropKey"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1783482
    const-string v0, "didEnterCropKey"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->D:Ljava/lang/String;

    .line 1783483
    :goto_3
    if-eqz p2, :cond_5

    const-string v0, "didCropKey"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1783484
    const-string v0, "didCropKey"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    goto :goto_0

    .line 1783485
    :cond_1
    iget-object v0, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1783486
    if-eqz v0, :cond_2

    .line 1783487
    iget-object v0, p3, Lcom/facebook/ipc/profile/stagingground/StagingGroundLaunchConfig;->m:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 1783488
    move-object v1, p0

    goto :goto_1

    :cond_2
    invoke-static {}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->newBuilder()Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData$Builder;->a()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    move-object v1, p0

    goto :goto_1

    .line 1783489
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1783490
    :cond_4
    const-string v0, "no_enter_crop_view"

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->D:Ljava/lang/String;

    goto :goto_3

    .line 1783491
    :cond_5
    const-string v0, "no_crop"

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;",
            "LX/0Or",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Or",
            "<",
            "LX/23P;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/photos/editgallery/utils/FetchImageUtils;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1783468
    iput-object p1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->b:LX/0Or;

    iput-object p3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->c:LX/0Or;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/BRg;)V
    .locals 4

    .prologue
    .line 1783447
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1783448
    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1783449
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->h:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1783450
    const-string v1, "staging_ground_photo_caption"

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1783451
    :cond_0
    const-string v1, "extra_profile_pic_expiration"

    invoke-virtual {p1}, LX/BRg;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1783452
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783453
    iget-object v2, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, v2

    .line 1783454
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1783455
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783456
    iget-object v2, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, v2

    .line 1783457
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->c()LX/5QV;

    move-result-object v1

    .line 1783458
    const-string v2, "staging_ground_selected_frame"

    invoke-static {v0, v2, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1783459
    :cond_1
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783460
    iget-object v2, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, v2

    .line 1783461
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1783462
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783463
    iget-object v2, v1, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v1, v2

    .line 1783464
    invoke-virtual {v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->d()Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;

    move-result-object v1

    .line 1783465
    const-string v2, "staging_ground_all_frames"

    invoke-static {v0, v2, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1783466
    :cond_2
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->s:LX/B37;

    invoke-interface {v1, v0}, LX/B37;->a(Landroid/content/Intent;)V

    .line 1783467
    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/0Vd;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/photos/creativeediting/model/CreativeEditingData;",
            "Landroid/net/Uri;",
            "LX/0Vd",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1783445
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->r:LX/8GV;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v2, p1

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, LX/8GV;->a(FLcom/facebook/photos/creativeediting/model/CreativeEditingData;LX/0Px;Landroid/net/Uri;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->x:Ljava/util/concurrent/Executor;

    invoke-static {v0, p3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1783446
    return-void
.end method

.method public static l(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V
    .locals 4

    .prologue
    .line 1783440
    new-instance v0, LX/BR5;

    invoke-direct {v0, p0}, LX/BR5;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    move-object v0, v0

    .line 1783441
    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->m:LX/0Vd;

    .line 1783442
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;

    .line 1783443
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->t:Landroid/content/Context;

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->m:LX/0Vd;

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/photos/editgallery/utils/FetchImageUtils;->a(Landroid/content/Context;Landroid/net/Uri;LX/0Vd;)V

    .line 1783444
    return-void
.end method

.method public static q(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V
    .locals 12

    .prologue
    .line 1783428
    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->p:LX/BRM;

    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->B:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->C:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;

    .line 1783429
    new-instance v3, LX/BRL;

    invoke-direct {v3, v0, v1}, LX/BRL;-><init>(Lcom/facebook/photos/creativeediting/swipeable/composer/CreativeEditingSwipeableLayout;Lcom/facebook/photos/creativeediting/swipeable/common/SwipeableFramesHScrollCirclePageIndicator;)V

    .line 1783430
    const/16 v4, 0xafd

    invoke-static {v2, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x24ee

    invoke-static {v2, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v2}, LX/BRP;->a(LX/0QB;)LX/BRP;

    move-result-object v6

    check-cast v6, LX/BRP;

    const-class v7, LX/9d6;

    invoke-interface {v2, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/9d6;

    const/16 v8, 0x2e31

    invoke-static {v2, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    const-class v9, LX/8GP;

    invoke-interface {v2, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/8GP;

    invoke-static {v2}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    const/16 v11, 0x259

    invoke-static {v2, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    .line 1783431
    iput-object v4, v3, LX/BRL;->a:LX/0Or;

    iput-object v5, v3, LX/BRL;->b:LX/0Or;

    iput-object v6, v3, LX/BRL;->c:LX/BRP;

    iput-object v7, v3, LX/BRL;->d:LX/9d6;

    iput-object v8, v3, LX/BRL;->e:LX/0Or;

    iput-object v9, v3, LX/BRL;->f:LX/8GP;

    iput-object v10, v3, LX/BRL;->g:Ljava/util/concurrent/Executor;

    iput-object v11, v3, LX/BRL;->h:LX/0Ot;

    .line 1783432
    move-object v0, v3

    .line 1783433
    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q:LX/BRL;

    .line 1783434
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q:LX/BRL;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1783435
    iget-object v2, v0, LX/BRL;->c:LX/BRP;

    .line 1783436
    iget-object v3, v2, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v2, v3

    .line 1783437
    invoke-virtual {v2}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v2

    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 1783438
    new-instance v2, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;

    invoke-direct {v2, v0, v1}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableController$1;-><init>(LX/BRL;Landroid/view/View;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 1783439
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/base/fragment/FbFragment;LX/BRg;)LX/63W;
    .locals 1

    .prologue
    .line 1783427
    new-instance v0, LX/BR8;

    invoke-direct {v0, p0, p2}, LX/BR8;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/BRg;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/base/activity/FbFragmentActivity;)Landroid/content/DialogInterface$OnClickListener;
    .locals 1

    .prologue
    .line 1783426
    new-instance v0, LX/BRB;

    invoke-direct {v0, p0, p1}, LX/BRB;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;Lcom/facebook/base/activity/FbFragmentActivity;)V

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 1783417
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->m:LX/0Vd;

    if-eqz v0, :cond_0

    .line 1783418
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->m:LX/0Vd;

    invoke-virtual {v0}, LX/0Vd;->dispose()V

    .line 1783419
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q:LX/BRL;

    if-eqz v0, :cond_1

    .line 1783420
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q:LX/BRL;

    invoke-virtual {v0}, LX/BRL;->a()V

    .line 1783421
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q:LX/BRL;

    .line 1783422
    :cond_1
    return-void
.end method

.method public final a(Landroid/app/Activity;IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 1783508
    const/4 v0, 0x1

    if-ne p2, v0, :cond_4

    .line 1783509
    if-eq p3, v1, :cond_1

    .line 1783510
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783511
    iget-object p0, v3, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v3, p0

    .line 1783512
    invoke-virtual {v3}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v3

    .line 1783513
    const-string p0, "staging_ground_cancel_edit"

    invoke-static {v0, p0, v1, v2, v3}, LX/BQo;->c(LX/BQo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1783514
    :cond_0
    :goto_0
    return-void

    .line 1783515
    :cond_1
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1783516
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783517
    iget-object v2, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v2, v2

    .line 1783518
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1783519
    const-string v1, "user_crop"

    iput-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    .line 1783520
    :cond_2
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v3, v3, LX/BR1;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783521
    iget-object p1, v4, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v4, p1

    .line 1783522
    invoke-virtual {v4}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v4

    iget-object p1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    .line 1783523
    iget-object p2, v1, LX/BQo;->a:LX/0Zb;

    const-string p3, "staging_ground_use_edit"

    const/4 p4, 0x0

    invoke-interface {p2, p3, p4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object p2

    .line 1783524
    invoke-virtual {p2}, LX/0oG;->a()Z

    move-result p3

    if-eqz p3, :cond_3

    .line 1783525
    iget-object p3, v1, LX/BQo;->b:Ljava/lang/String;

    invoke-virtual {p2, p3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 1783526
    const-string p3, "heisman_composer_session_id"

    invoke-virtual {p2, p3, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783527
    const-string p3, "picture_id"

    invoke-virtual {p2, p3, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783528
    const-string p3, "profile_pic_frame_id"

    invoke-virtual {p2, p3, v4}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783529
    const-string p3, "use_cropping"

    invoke-virtual {p2, p3, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 1783530
    invoke-virtual {p2}, LX/0oG;->d()V

    .line 1783531
    :cond_3
    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->g:Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 1783532
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v1

    .line 1783533
    iput-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    .line 1783534
    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    .line 1783535
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v1

    .line 1783536
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_b

    .line 1783537
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v1, v1

    .line 1783538
    invoke-virtual {v1}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_1
    iput-object v1, v2, LX/BR1;->a:Landroid/net/Uri;

    .line 1783539
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    .line 1783540
    iget-object v2, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->g:Landroid/graphics/RectF;

    move-object v0, v2

    .line 1783541
    iput-object v0, v1, LX/BR1;->c:Landroid/graphics/RectF;

    .line 1783542
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h()V

    .line 1783543
    goto/16 :goto_0

    .line 1783544
    :cond_4
    const/4 v0, 0x3

    if-ne p2, v0, :cond_6

    .line 1783545
    if-ne p3, v1, :cond_0

    .line 1783546
    const/4 v3, 0x0

    .line 1783547
    const-string v0, "clear_frame"

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1783548
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    invoke-virtual {v0}, LX/BRP;->a()V

    .line 1783549
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->B:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1783550
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->C:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->c()V

    .line 1783551
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q:LX/BRL;

    if-eqz v0, :cond_5

    .line 1783552
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q:LX/BRL;

    invoke-virtual {v0}, LX/BRL;->a()V

    .line 1783553
    iput-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q:LX/BRL;

    .line 1783554
    :cond_5
    :goto_2
    goto/16 :goto_0

    .line 1783555
    :cond_6
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 1783556
    if-eq p3, v1, :cond_7

    .line 1783557
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v2, v2, LX/BR1;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783558
    iget-object v4, v3, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v3, v4

    .line 1783559
    invoke-virtual {v3}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v3

    .line 1783560
    const-string v4, "staging_ground_cancel_change_photo"

    invoke-static {v0, v4, v1, v2, v3}, LX/BQo;->c(LX/BQo;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1783561
    goto/16 :goto_0

    .line 1783562
    :cond_7
    const-string v0, "extra_is_from_simple_picker"

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1783563
    invoke-virtual {p4}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "photo"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1783564
    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_d

    .line 1783565
    :cond_8
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->v:LX/03V;

    const-string v3, "heisman_invalid_photo_picked"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p1, "null fields in the GraphQLPhoto from the photo tab picker: "

    invoke-direct {v4, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783566
    :goto_3
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iput-object v2, v0, LX/BR1;->b:Landroid/net/Uri;

    .line 1783567
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iput-object v2, v0, LX/BR1;->d:LX/434;

    .line 1783568
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->b()V

    goto/16 :goto_0

    .line 1783569
    :cond_9
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    const-string v0, "extra_staging_ground_photo_uri"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, v1, LX/BR1;->a:Landroid/net/Uri;

    .line 1783570
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    const/4 v1, 0x0

    iput-object v1, v0, LX/BR1;->f:Ljava/lang/String;

    .line 1783571
    const-string v0, "extra_staging_ground_selected_frame_id"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1783572
    if-eqz v0, :cond_a

    .line 1783573
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    invoke-virtual {v1, v0}, LX/BRP;->a(Ljava/lang/String;)V

    .line 1783574
    invoke-static {p0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    .line 1783575
    :cond_a
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->g:Ljava/lang/String;

    sget-object v3, LX/74j;->CREATIVECAM_MEDIA:LX/74j;

    invoke-virtual {v3}, LX/74j;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783576
    iget-object p1, v4, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v4, p1

    .line 1783577
    invoke-virtual {v4}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v4

    const-string p1, "from_camera"

    invoke-virtual {v0, v1, v3, v4, p1}, LX/BQo;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1783578
    goto :goto_3

    .line 1783579
    :cond_b
    iget-object v1, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 1783580
    goto/16 :goto_1

    .line 1783581
    :cond_c
    const-string v0, "heisman_profile_overlay_item"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;

    .line 1783582
    iget-object v1, v0, Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;->a:Lcom/facebook/heisman/protocol/imageoverlay/ImageOverlayGraphQLModels$ImageOverlayFieldsModel;

    move-object v1, v1

    .line 1783583
    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    invoke-virtual {v2}, LX/BRP;->a()V

    .line 1783584
    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    invoke-virtual {v2, v1, v3, v0}, LX/BRP;->a(LX/5QV;Lcom/facebook/heisman/protocol/swipeable/SwipeableOverlaysGraphQLModels$ImageOverlayWithSwipeableOverlaysModel;Lcom/facebook/ipc/profile/heisman/ProfilePictureOverlayItemModel;)V

    .line 1783585
    invoke-static {p0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    goto/16 :goto_2

    .line 1783586
    :cond_d
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iput-object v3, v1, LX/BR1;->a:Landroid/net/Uri;

    .line 1783587
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, LX/BR1;->f:Ljava/lang/String;

    .line 1783588
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->w:LX/BQo;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->g:Ljava/lang/String;

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v3, v3, LX/BR1;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783589
    iget-object p1, v4, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v4, p1

    .line 1783590
    invoke-virtual {v4}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->f()Ljava/lang/String;

    move-result-object v4

    const-string p1, "from_fb"

    invoke-virtual {v0, v1, v3, v4, p1}, LX/BQo;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_3
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1783413
    const-string v0, "creativeEditingDataKey"

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1783414
    const-string v0, "didCropKey"

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783415
    const-string v0, "didEnterCropKey"

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->D:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1783416
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1783362
    const v0, 0x7f0d2d63

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1783363
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1783364
    const v0, 0x7f0d2d64

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1783365
    const v1, 0x7f0d17ad

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 1783366
    new-instance v2, LX/0zw;

    invoke-direct {v2, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->B:LX/0zw;

    .line 1783367
    new-instance v0, LX/0zw;

    invoke-direct {v0, v1}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->C:LX/0zw;

    .line 1783368
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->n:LX/BRP;

    .line 1783369
    iget-object v1, v0, LX/BRP;->a:Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;

    move-object v0, v1

    .line 1783370
    invoke-virtual {v0}, Lcom/facebook/timeline/stagingground/StagingGroundSwipeableModel;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1783371
    :goto_0
    return-void

    .line 1783372
    :cond_0
    invoke-static {p0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->q(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;)V
    .locals 5

    .prologue
    .line 1783404
    const v0, 0x7f0d2d67

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1783405
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 1783406
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/23P;

    .line 1783407
    new-instance v2, LX/BR9;

    invoke-direct {v2, p0, v1}, LX/BR9;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/23P;)V

    move-object v1, v2

    .line 1783408
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1783409
    const v1, 0x7f0d0d19

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    .line 1783410
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->t:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020423

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-static {v0, v1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1783411
    new-instance v1, LX/ATu;

    iget-object v2, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->t:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020422

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->t:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020423

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/ATu;-><init>(LX/0Px;Landroid/widget/Button;)V

    invoke-virtual {v1}, LX/ATu;->a()V

    .line 1783412
    return-void
.end method

.method public final b()V
    .locals 9

    .prologue
    .line 1783388
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v0, v0, LX/BR1;->f:Ljava/lang/String;

    const/4 v3, 0x0

    .line 1783389
    :try_start_0
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v4, v5, v7

    if-lez v4, :cond_0

    const/4 v3, 0x1

    .line 1783390
    :cond_0
    :goto_0
    move v0, v3

    .line 1783391
    if-nez v0, :cond_1

    .line 1783392
    invoke-static {p0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->l(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    .line 1783393
    :goto_1
    return-void

    .line 1783394
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->z:Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->f:Ljava/lang/String;

    new-instance v2, LX/BR3;

    invoke-direct {v2, p0}, LX/BR3;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    .line 1783395
    new-instance v5, LX/BRQ;

    invoke-direct {v5}, LX/BRQ;-><init>()V

    move-object v5, v5

    .line 1783396
    const-string v6, "node"

    invoke-virtual {v5, v6, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1783397
    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    sget-object v6, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v5, v6}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v5

    sget-object v6, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1783398
    iput-object v6, v5, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1783399
    move-object v5, v5

    .line 1783400
    sget-object v6, LX/0zS;->a:LX/0zS;

    invoke-virtual {v5, v6}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v5

    const-wide/16 v7, 0xe10

    invoke-virtual {v5, v7, v8}, LX/0zO;->a(J)LX/0zO;

    move-result-object v5

    .line 1783401
    iget-object v6, v0, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;->b:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v5

    move-object v3, v5

    .line 1783402
    iget-object v4, v0, Lcom/facebook/timeline/stagingground/BestAvailableImageFetchController;->c:Ljava/util/concurrent/Executor;

    invoke-static {v3, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1783403
    goto :goto_1

    :catch_0
    goto :goto_0
.end method

.method public final b(Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;)V
    .locals 2

    .prologue
    .line 1783382
    const v0, 0x7f0d2d68

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/ui/common/ProfileMediaFragmentLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1783383
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    .line 1783384
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17Y;

    .line 1783385
    new-instance p1, LX/BRA;

    invoke-direct {p1, p0, v1}, LX/BRA;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;LX/17Y;)V

    move-object v1, p1

    .line 1783386
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1783387
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1783381
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    invoke-static {v0}, LX/5iB;->a(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1783380
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1783379
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 1783378
    return-void
.end method

.method public final g()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 1783377
    const v0, 0x7f081895

    return v0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 1783373
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1783374
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->A:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->a:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1783375
    :goto_0
    return-void

    .line 1783376
    :cond_1
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->h:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->k:LX/BR1;

    iget-object v1, v1, LX/BR1;->a:Landroid/net/Uri;

    new-instance v2, LX/BR6;

    invoke-direct {v2, p0}, LX/BR6;-><init>(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;)V

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;->a$redex0(Lcom/facebook/timeline/stagingground/StagingGroundProfileImageController;Lcom/facebook/photos/creativeediting/model/CreativeEditingData;Landroid/net/Uri;LX/0Vd;)V

    goto :goto_0
.end method
