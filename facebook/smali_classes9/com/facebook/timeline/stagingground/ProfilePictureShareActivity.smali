.class public Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# static fields
.field private static final q:[Ljava/lang/String;


# instance fields
.field public A:Z

.field public volatile p:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/5SF;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/content/SecureContextHelper;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private s:LX/5vm;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private t:LX/0ad;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private u:LX/3N2;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private v:LX/0i4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private w:Lcom/facebook/share/model/ComposerAppAttribution;

.field private x:Ljava/lang/String;

.field public y:J

.field public z:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1782750
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    sput-object v0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->q:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1782748
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1782749
    return-void
.end method

.method private static a(Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/5vm;LX/0ad;LX/3N2;LX/0i4;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;",
            "LX/0Or",
            "<",
            "LX/5SF;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/5vm;",
            "LX/0ad;",
            "LX/3N2;",
            "LX/0i4;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1782747
    iput-object p1, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->p:LX/0Or;

    iput-object p2, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->r:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->s:LX/5vm;

    iput-object p4, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->t:LX/0ad;

    iput-object p5, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->u:LX/3N2;

    iput-object p6, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->v:LX/0i4;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    const/16 v1, 0x36f4

    invoke-static {v6, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v6}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v6}, LX/5vm;->b(LX/0QB;)LX/5vm;

    move-result-object v3

    check-cast v3, LX/5vm;

    invoke-static {v6}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v6}, LX/3N2;->b(LX/0QB;)LX/3N2;

    move-result-object v5

    check-cast v5, LX/3N2;

    const-class v7, LX/0i4;

    invoke-interface {v6, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/0i4;

    invoke-static/range {v0 .. v6}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->a(Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/5vm;LX/0ad;LX/3N2;LX/0i4;)V

    return-void
.end method

.method private varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 1782744
    const-class v0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1, p2}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1782745
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->finish()V

    .line 1782746
    return-void
.end method

.method private c(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1782736
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v2, "facebook.intent.action.PROFILE_MEDIA_CREATE"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "proxied_app_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "proxied_app_package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1782737
    :cond_0
    iput-object v1, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1782738
    :goto_0
    return-void

    .line 1782739
    :cond_1
    const-string v0, "proxied_app_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1782740
    const-string v0, "proxied_app_package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1782741
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->u:LX/3N2;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, LX/3N2;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1782742
    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    new-instance v1, Lcom/facebook/share/model/ComposerAppAttribution;

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/facebook/share/model/ComposerAppAttribution;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iput-object v1, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 1782743
    goto :goto_1
.end method

.method private d(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1782734
    const-string v0, "\u0438\u0434\u0435\u043d\u0442\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440_\u043c\u0430\u0441\u043a\u0438"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->x:Ljava/lang/String;

    .line 1782735
    return-void
.end method

.method private e(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1782729
    const-string v0, "profile_media_extras_bundle"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 1782730
    if-nez v0, :cond_0

    .line 1782731
    iput-wide v2, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->y:J

    .line 1782732
    :goto_0
    return-void

    .line 1782733
    :cond_0
    const-string v1, "profile_media_extras_bundle_key_default_expiration_time_in_secs_since_epoch"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->y:J

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1782727
    invoke-static {p0, p0}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1782728
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 1782707
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1782708
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1782709
    if-eqz p1, :cond_0

    const-string v0, "key_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "key_has_launched_preview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1782710
    const-string v0, "key_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->z:Landroid/net/Uri;

    .line 1782711
    const-string v0, "key_has_launched_preview"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->A:Z

    .line 1782712
    :goto_0
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->z:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 1782713
    const-string v0, "Image Uri is NULL"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1782714
    :goto_1
    return-void

    .line 1782715
    :cond_0
    const-string v0, "android.intent.extra.STREAM"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->z:Landroid/net/Uri;

    .line 1782716
    iput-boolean v3, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->A:Z

    goto :goto_0

    .line 1782717
    :cond_1
    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1782718
    const-string v2, "facebook.intent.action.PROFILE_MEDIA_CREATE"

    invoke-static {v0, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "proxied_app_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "proxied_app_package_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "profile_media_extras_bundle"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1782719
    :cond_2
    const-string v0, "Required extras from 3rd party not present"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 1782720
    :cond_3
    invoke-direct {p0, v1}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->c(Landroid/content/Intent;)V

    .line 1782721
    const-string v2, "facebook.intent.action.PROFILE_MEDIA_CREATE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    if-nez v0, :cond_4

    .line 1782722
    const-string v0, "Application attribution not set"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-direct {p0, v0, v2}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1782723
    :cond_4
    invoke-direct {p0, v1}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->d(Landroid/content/Intent;)V

    .line 1782724
    invoke-direct {p0, v1}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->e(Landroid/content/Intent;)V

    .line 1782725
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->p:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5SF;

    .line 1782726
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->v:LX/0i4;

    invoke-virtual {v1, p0}, LX/0i4;->a(Landroid/app/Activity;)LX/0i5;

    move-result-object v1

    sget-object v2, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->q:[Ljava/lang/String;

    new-instance v3, LX/BQm;

    invoke-direct {v3, p0, v0}, LX/BQm;-><init>(Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;LX/5SF;)V

    invoke-virtual {v1, v2, v3}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    .line 1782695
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1782696
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->finish()V

    .line 1782697
    :goto_0
    return-void

    .line 1782698
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->s:LX/5vm;

    sget-object v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    const-string v2, "extra_profile_pic_expiration"

    const-wide/16 v4, 0x0

    invoke-virtual {p3, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string v4, "staging_ground_photo_caption"

    invoke-virtual {p3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "upload"

    iget-object v6, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    iget-object v7, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->x:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, LX/5vm;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1782699
    if-nez v0, :cond_1

    .line 1782700
    const-string v0, "Failed to obtain logged in user"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-direct {p0, v0, v1}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 1782701
    :cond_1
    const-string v1, "force_create_new_activity"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1782702
    iget-object v1, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->r:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1782703
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->finish()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1782704
    const-string v0, "key_uri"

    iget-object v1, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->z:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1782705
    const-string v0, "key_has_launched_preview"

    iget-boolean v1, p0, Lcom/facebook/timeline/stagingground/ProfilePictureShareActivity;->A:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1782706
    return-void
.end method
