.class public abstract Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

.field public q:LX/0h5;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1740356
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()LX/B3T;
.end method

.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 1

    .prologue
    .line 1740357
    instance-of v0, p1, Lcom/facebook/heisman/category/SingleCategoryFragment;

    if-eqz v0, :cond_0

    .line 1740358
    check-cast p1, Lcom/facebook/heisman/category/SingleCategoryFragment;

    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->a()LX/B3T;

    move-result-object v0

    .line 1740359
    iput-object v0, p1, Lcom/facebook/heisman/category/SingleCategoryFragment;->c:LX/B3T;

    .line 1740360
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1740361
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1740362
    invoke-virtual {p0}, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1740363
    const-string v1, "heisman_pivot_intent_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1740364
    const-string v1, "heisman_pivot_intent_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    .line 1740365
    :goto_0
    move-object v0, v1

    .line 1740366
    iput-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->p:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    .line 1740367
    const v0, 0x7f03107a

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->setContentView(I)V

    .line 1740368
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1740369
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1740370
    new-instance v1, LX/BQl;

    invoke-direct {v1, p0}, LX/BQl;-><init>(Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1740371
    move-object v0, v0

    .line 1740372
    iput-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->q:LX/0h5;

    .line 1740373
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v1, "single_category_fragment"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1740374
    iget-object v0, p0, Lcom/facebook/timeline/stagingground/ProfilePictureOverlaySingleCategoryActivity;->p:Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    invoke-static {v0}, Lcom/facebook/heisman/category/SingleCategoryFragment;->a(Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;)Lcom/facebook/heisman/category/SingleCategoryFragment;

    move-result-object v0

    .line 1740375
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    const v2, 0x7f0d2767

    const-string v3, "single_category_fragment"

    invoke-virtual {v1, v2, v0, v3}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1740376
    :cond_0
    return-void

    .line 1740377
    :cond_1
    const-string v1, "entry_point"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1740378
    const-string v2, "heisman_category_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1740379
    new-instance v3, LX/B4N;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v3, p1, v1}, LX/B4N;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v2}, LX/B4N;->c(Ljava/lang/String;)LX/B4N;

    move-result-object v1

    invoke-virtual {v1}, LX/B4N;->a()Lcom/facebook/heisman/intent/ProfilePictureOverlayPivotIntentData;

    move-result-object v1

    goto :goto_0
.end method
