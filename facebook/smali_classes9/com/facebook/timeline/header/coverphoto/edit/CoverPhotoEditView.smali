.class public Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;
.super Landroid/widget/ImageView;
.source ""


# instance fields
.field private a:LX/03V;

.field private b:LX/BPE;

.field public c:Ljava/lang/String;

.field public d:F

.field public e:F

.field private f:Landroid/graphics/Matrix;

.field public g:I

.field public h:I

.field private i:I

.field private j:I

.field private k:F

.field private l:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1781102
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1781103
    invoke-direct {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->b()V

    .line 1781104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1781099
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1781100
    invoke-direct {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->b()V

    .line 1781101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1781096
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1781097
    invoke-direct {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->b()V

    .line 1781098
    return-void
.end method

.method public static synthetic a(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;F)F
    .locals 1

    .prologue
    .line 1781095
    iget v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    return v0
.end method

.method private a(LX/03V;LX/BPE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1781092
    iput-object p1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a:LX/03V;

    .line 1781093
    iput-object p2, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->b:LX/BPE;

    .line 1781094
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {v1}, LX/BPE;->a(LX/0QB;)LX/BPE;

    move-result-object v1

    check-cast v1, LX/BPE;

    invoke-direct {p0, v0, v1}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a(LX/03V;LX/BPE;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;FF)V
    .locals 2

    .prologue
    .line 1781026
    new-instance v0, Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->f:Landroid/graphics/Matrix;

    invoke-direct {v0, v1}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 1781027
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 1781028
    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->setImageMatrix(Landroid/graphics/Matrix;)V

    .line 1781029
    return-void
.end method

.method public static a$redex0(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;Landroid/graphics/Bitmap;)V
    .locals 11

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v9, 0x0

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    .line 1781060
    iput-object p1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->l:Landroid/graphics/Bitmap;

    .line 1781061
    invoke-virtual {p0, p1}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1781062
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->f:Landroid/graphics/Matrix;

    .line 1781063
    iget-object v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->c:Ljava/lang/String;

    invoke-static {v0}, LX/2Qx;->b(Ljava/lang/String;)I

    move-result v0

    .line 1781064
    if-lez v0, :cond_1

    .line 1781065
    new-instance v1, LX/7U9;

    invoke-direct {v1, p1, v0}, LX/7U9;-><init>(Landroid/graphics/Bitmap;I)V

    .line 1781066
    invoke-virtual {v1}, LX/7U9;->e()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->i:I

    .line 1781067
    invoke-virtual {v1}, LX/7U9;->d()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->j:I

    .line 1781068
    iget-object v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->f:Landroid/graphics/Matrix;

    invoke-virtual {v1}, LX/7U9;->c()Landroid/graphics/Matrix;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1781069
    :goto_0
    iget-object v8, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->f:Landroid/graphics/Matrix;

    iget v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->g:I

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->h:I

    iget v2, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->i:I

    iget v3, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->j:I

    move-wide v6, v4

    invoke-static/range {v0 .. v7}, LX/7U8;->a(IIIIDD)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1781070
    iget v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->g:I

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->h:I

    iget v2, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->i:I

    iget v3, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->j:I

    .line 1781071
    int-to-float v4, v2

    int-to-float v5, v3

    div-float/2addr v4, v5

    .line 1781072
    int-to-float v5, v0

    int-to-float v6, v1

    div-float/2addr v5, v6

    .line 1781073
    cmpl-float v4, v4, v5

    if-lez v4, :cond_3

    .line 1781074
    int-to-float v4, v1

    int-to-float v5, v3

    div-float/2addr v4, v5

    .line 1781075
    :goto_1
    move v0, v4

    .line 1781076
    iput v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    .line 1781077
    iget v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->i:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->g:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    mul-float v3, v10, v0

    .line 1781078
    neg-float v2, v3

    .line 1781079
    iget v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->j:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->h:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    mul-float v5, v10, v0

    .line 1781080
    neg-float v4, v5

    .line 1781081
    iget v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 1781082
    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    invoke-static {v1, v5}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 1781083
    invoke-static {v0, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1781084
    invoke-static {v1, v4}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 1781085
    invoke-static {p0, v0, v1}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a$redex0(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;FF)V

    .line 1781086
    new-instance v0, LX/BPu;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/BPu;-><init>(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;FFFF)V

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1781087
    iget-object v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->b:LX/BPE;

    new-instance v2, LX/BPF;

    cmpl-float v0, v5, v9

    if-gtz v0, :cond_0

    cmpl-float v0, v3, v9

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_2
    invoke-direct {v2, v0}, LX/BPF;-><init>(Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1781088
    return-void

    .line 1781089
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->i:I

    .line 1781090
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->j:I

    goto/16 :goto_0

    .line 1781091
    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    int-to-float v4, v0

    int-to-float v5, v2

    div-float/2addr v4, v5

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1781058
    iget-object v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a:LX/03V;

    const-string v1, "editcoverphoto_decodeimage"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1781059
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1781056
    const-class v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    invoke-static {v0, p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1781057
    return-void
.end method

.method public static synthetic c(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;F)F
    .locals 1

    .prologue
    .line 1781055
    iget v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1781050
    invoke-virtual {p0, v1}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1781051
    iget-object v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->l:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1781052
    iget-object v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->l:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1781053
    iput-object v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->l:Landroid/graphics/Bitmap;

    .line 1781054
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1781041
    iput-object p1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->c:Ljava/lang/String;

    .line 1781042
    iput p2, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->g:I

    .line 1781043
    iput p3, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->h:I

    .line 1781044
    invoke-virtual {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1781045
    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->h:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1781046
    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1781047
    new-instance v0, LX/BPv;

    invoke-direct {v0, p0}, LX/BPv;-><init>(Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;)V

    .line 1781048
    invoke-virtual {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getContext()Landroid/content/Context;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 1781049
    return-void
.end method

.method public getFocusX()F
    .locals 5

    .prologue
    .line 1781037
    const/high16 v0, 0x3f000000    # 0.5f

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->d:F

    iget v2, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    iget v3, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->i:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 1781038
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1781039
    iget-object v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a:LX/03V;

    const-string v2, "CoverPhotoEditView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "focusX is NaN. scale:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "image width: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->i:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1781040
    :cond_0
    return v0
.end method

.method public getFocusY()F
    .locals 5

    .prologue
    .line 1781033
    const/high16 v0, 0x3f000000    # 0.5f

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->e:F

    iget v2, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    iget v3, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->j:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 1781034
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1781035
    iget-object v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a:LX/03V;

    const-string v2, "CoverPhotoEditView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "focusY is NaN. scale:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "image height: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->j:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1781036
    :cond_0
    return v0
.end method

.method public getNormalizedCropBounds()Landroid/graphics/RectF;
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 1781030
    iget v0, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->g:I

    int-to-float v0, v0

    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->i:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    .line 1781031
    iget v1, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->h:I

    int-to-float v1, v1

    iget v2, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->j:I

    int-to-float v2, v2

    iget v3, p0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->k:F

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    .line 1781032
    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getFocusX()F

    move-result v3

    div-float v4, v0, v6

    sub-float/2addr v3, v4

    invoke-virtual {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getFocusY()F

    move-result v4

    div-float v5, v1, v6

    sub-float/2addr v4, v5

    invoke-virtual {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getFocusX()F

    move-result v5

    div-float/2addr v0, v6

    add-float/2addr v0, v5

    invoke-virtual {p0}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->getFocusY()F

    move-result v5

    div-float/2addr v1, v6

    add-float/2addr v1, v5

    invoke-direct {v2, v3, v4, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2
.end method
