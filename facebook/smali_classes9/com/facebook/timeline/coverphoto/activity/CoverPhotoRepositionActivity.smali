.class public Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field public A:LX/AzQ;

.field public B:LX/0ad;

.field private p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/63b;

.field private r:LX/BPE;

.field private s:LX/1B1;

.field private t:Landroid/support/v4/app/Fragment;

.field private u:Lcom/facebook/auth/viewercontext/ViewerContext;

.field public v:LX/0h5;

.field private w:LX/0jo;

.field public x:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field private z:Ljava/lang/String;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1780664
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a(LX/BPE;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0jo;LX/63c;LX/AzQ;LX/0ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BPE;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;",
            ">;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/0jo;",
            "LX/63c;",
            "LX/AzQ;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1780665
    iput-object p1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->r:LX/BPE;

    .line 1780666
    iput-object p2, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->p:LX/0Ot;

    .line 1780667
    iput-object p3, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->u:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1780668
    iput-object p4, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->w:LX/0jo;

    .line 1780669
    const v0, 0x7f0815bd

    invoke-virtual {p5, v0}, LX/63c;->a(I)LX/63b;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->q:LX/63b;

    .line 1780670
    iput-object p6, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->A:LX/AzQ;

    .line 1780671
    iput-object p7, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->B:LX/0ad;

    .line 1780672
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 8

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v7

    move-object v0, p0

    check-cast v0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;

    invoke-static {v7}, LX/BPE;->a(LX/0QB;)LX/BPE;

    move-result-object v1

    check-cast v1, LX/BPE;

    const/16 v2, 0x36aa

    invoke-static {v7, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-static {v7}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    check-cast v3, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v7}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v4

    check-cast v4, LX/0jo;

    const-class v5, LX/63c;

    invoke-interface {v7, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/63c;

    invoke-static {v7}, LX/AzQ;->b(LX/0QB;)LX/AzQ;

    move-result-object v6

    check-cast v6, LX/AzQ;

    invoke-static {v7}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct/range {v0 .. v7}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->a(LX/BPE;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/0jo;LX/63c;LX/AzQ;LX/0ad;)V

    return-void
.end method

.method public static b(Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1780673
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->t:Landroid/support/v4/app/Fragment;

    check-cast v0, LX/BP1;

    invoke-interface {v0}, LX/BP1;->a()Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;

    move-result-object v3

    .line 1780674
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;

    iget-object v4, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->u:Lcom/facebook/auth/viewercontext/ViewerContext;

    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->y:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v0, v3, v4, v1}, Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoHandler;->a(Lcom/facebook/timeline/profilepiccoverphotoupload/SetCoverPhotoParams;Lcom/facebook/auth/viewercontext/ViewerContext;Z)V

    .line 1780675
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1780676
    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->y:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    if-eqz v1, :cond_0

    .line 1780677
    const-string v1, "prompt_entry_point_analytics_extra"

    iget-object v3, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->y:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1780678
    const-string v1, "did_use_prompt_extra"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1780679
    :cond_0
    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->z:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1780680
    const-string v1, "prompt_object_class_name_extra"

    iget-object v2, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1780681
    :cond_1
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->setResult(ILandroid/content/Intent;)V

    .line 1780682
    invoke-virtual {p0}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->finish()V

    .line 1780683
    return-void

    .line 1780684
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1780685
    const-string v0, "set_cover_photo"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1780686
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1780687
    invoke-static {p0, p0}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1780688
    const v0, 0x7f0314d8

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->setContentView(I)V

    .line 1780689
    invoke-virtual {p0}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1780690
    const-string v0, "session_id"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->x:Ljava/lang/String;

    .line 1780691
    const-string v0, "prompt_entry_point_analytics_extra"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/productionprompts/logging/PromptAnalytics;

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->y:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1780692
    const-string v0, "prompt_object_class_name_extra"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->z:Ljava/lang/String;

    .line 1780693
    const-string v0, "target_fragment"

    const/4 v2, -0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1780694
    iget-object v2, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->w:LX/0jo;

    invoke-interface {v2, v0}, LX/0jo;->a(I)LX/0jq;

    move-result-object v0

    .line 1780695
    invoke-interface {v0, v1}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->t:Landroid/support/v4/app/Fragment;

    .line 1780696
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    const v1, 0x7f0d002f

    iget-object v2, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->t:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    .line 1780697
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 1780698
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1780699
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    .line 1780700
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setShowDividers(Z)V

    .line 1780701
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1780702
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    new-instance v1, LX/BP2;

    invoke-direct {v1, p0}, LX/BP2;-><init>(Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1780703
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->q:LX/63b;

    invoke-virtual {v1}, LX/63b;->a()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1780704
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->v:LX/0h5;

    new-instance v1, LX/BP3;

    invoke-direct {v1, p0}, LX/BP3;-><init>(Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1780705
    :cond_0
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->s:LX/1B1;

    .line 1780706
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->s:LX/1B1;

    new-instance v1, LX/BP6;

    invoke-direct {v1, p0}, LX/BP6;-><init>(Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;)V

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 1780707
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->s:LX/1B1;

    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->r:LX/BPE;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 1780708
    return-void
.end method

.method public final onBackPressed()V
    .locals 2

    .prologue
    .line 1780709
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1780710
    iget-object v0, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->A:LX/AzQ;

    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/AzQ;->g(Ljava/lang/String;)V

    .line 1780711
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    .line 1780712
    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x6098e023

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1780713
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1780714
    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->s:LX/1B1;

    if-eqz v1, :cond_0

    .line 1780715
    iget-object v1, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->s:LX/1B1;

    iget-object v2, p0, Lcom/facebook/timeline/coverphoto/activity/CoverPhotoRepositionActivity;->r:LX/BPE;

    invoke-virtual {v1, v2}, LX/1B1;->b(LX/0b4;)V

    .line 1780716
    :cond_0
    const/16 v1, 0x23

    const v2, 0x7f03b7ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
