.class public final Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BRr;


# direct methods
.method public constructor <init>(LX/BRr;)V
    .locals 0

    .prologue
    .line 1784466
    iput-object p1, p0, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$1;->a:LX/BRr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1784467
    iget-object v0, p0, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$1;->a:LX/BRr;

    iget-boolean v0, v0, LX/BRr;->h:Z

    if-eqz v0, :cond_0

    .line 1784468
    :goto_0
    return-void

    .line 1784469
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$1;->a:LX/BRr;

    const/4 p0, 0x0

    .line 1784470
    iget-object v1, v0, LX/BRr;->d:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    .line 1784471
    iget-object v2, v1, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->a:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-object v2, v2

    .line 1784472
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f050003

    invoke-static {v3, v4}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v3

    check-cast v3, Landroid/animation/ObjectAnimator;

    .line 1784473
    invoke-virtual {v3, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1784474
    move-object v3, v3

    .line 1784475
    invoke-static {v2}, LX/BRm;->b(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v4

    .line 1784476
    iget-object v5, v0, LX/BRr;->b:Landroid/content/res/Resources;

    const v6, 0x7f0c005b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v5

    invoke-virtual {v1, v5}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->setBackgroundAlpha(I)V

    .line 1784477
    invoke-virtual {v1, p0}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->setVisibility(I)V

    .line 1784478
    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1784479
    new-instance v5, LX/BRo;

    invoke-direct {v5, v0, v2, v1}, LX/BRo;-><init>(LX/BRr;Landroid/view/View;Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;)V

    invoke-virtual {v4, v5}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1784480
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1784481
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/animation/Animator;

    aput-object v3, v2, p0

    const/4 v3, 0x1

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1784482
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 1784483
    goto :goto_0
.end method
