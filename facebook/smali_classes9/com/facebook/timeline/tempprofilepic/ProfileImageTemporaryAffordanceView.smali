.class public Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/fbui/widget/text/ImageWithTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1784432
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1784433
    invoke-direct {p0}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->a()V

    .line 1784434
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1784429
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1784430
    invoke-direct {p0}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->a()V

    .line 1784431
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1784426
    const v0, 0x7f0314ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1784427
    const v0, 0x7f0d2f5b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    iput-object v0, p0, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->a:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1784428
    return-void
.end method


# virtual methods
.method public getTimerTextView()Lcom/facebook/fbui/widget/text/ImageWithTextView;
    .locals 1

    .prologue
    .line 1784417
    iget-object v0, p0, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->a:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    return-object v0
.end method

.method public setBackgroundAlpha(I)V
    .locals 2

    .prologue
    .line 1784422
    invoke-virtual {p0}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const-string v1, "Do not call setBackgroundAlpha if the view does not have a background drawable"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1784423
    invoke-virtual {p0}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1784424
    invoke-virtual {p0}, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->invalidate()V

    .line 1784425
    return-void
.end method

.method public setTimerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1784420
    iget-object v0, p0, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->a:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1784421
    return-void
.end method

.method public setTimerText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1784418
    iget-object v0, p0, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->a:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1784419
    return-void
.end method
