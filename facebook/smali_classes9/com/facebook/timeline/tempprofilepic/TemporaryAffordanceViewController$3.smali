.class public final Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BRr;


# direct methods
.method public constructor <init>(LX/BRr;)V
    .locals 0

    .prologue
    .line 1784496
    iput-object p1, p0, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$3;->a:LX/BRr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1784497
    iget-object v0, p0, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$3;->a:LX/BRr;

    iget-boolean v0, v0, LX/BRr;->h:Z

    if-eqz v0, :cond_0

    .line 1784498
    :goto_0
    return-void

    .line 1784499
    :cond_0
    iget-object v0, p0, Lcom/facebook/timeline/tempprofilepic/TemporaryAffordanceViewController$3;->a:LX/BRr;

    .line 1784500
    iget-object v1, v0, LX/BRr;->d:LX/0zw;

    invoke-virtual {v1}, LX/0zw;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;

    .line 1784501
    iget-object v2, v1, Lcom/facebook/timeline/tempprofilepic/ProfileImageTemporaryAffordanceView;->a:Lcom/facebook/fbui/widget/text/ImageWithTextView;

    move-object v2, v2

    .line 1784502
    invoke-static {v2}, LX/BRm;->c(Landroid/view/View;)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 1784503
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f050004

    invoke-static {v3, v4}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v3

    check-cast v3, Landroid/animation/ObjectAnimator;

    .line 1784504
    invoke-virtual {v3, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 1784505
    move-object v1, v3

    .line 1784506
    new-instance v3, LX/BRp;

    invoke-direct {v3, v0}, LX/BRp;-><init>(LX/BRr;)V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1784507
    new-instance v3, LX/BRq;

    invoke-direct {v3, v0}, LX/BRq;-><init>(LX/BRr;)V

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1784508
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1784509
    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 p0, 0x0

    aput-object v2, v4, p0

    const/4 v2, 0x1

    aput-object v1, v4, v2

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 1784510
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 1784511
    goto :goto_0
.end method
