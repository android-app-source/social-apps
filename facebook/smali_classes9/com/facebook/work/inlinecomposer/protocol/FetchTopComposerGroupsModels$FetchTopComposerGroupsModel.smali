.class public final Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6f21c11d
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1795830
    const-class v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1795854
    const-class v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1795852
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1795853
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1795846
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1795847
    invoke-virtual {p0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1795848
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1795849
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1795850
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1795851
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1795838
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1795839
    invoke-virtual {p0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1795840
    invoke-virtual {p0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    .line 1795841
    invoke-virtual {p0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1795842
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;

    .line 1795843
    iput-object v0, v1, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->e:Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    .line 1795844
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1795845
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAccountUser"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1795836
    iget-object v0, p0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->e:Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    iput-object v0, p0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->e:Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    .line 1795837
    iget-object v0, p0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;->e:Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel$AccountUserModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1795833
    new-instance v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;

    invoke-direct {v0}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$FetchTopComposerGroupsModel;-><init>()V

    .line 1795834
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1795835
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1795832
    const v0, -0x2693ddff

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1795831
    const v0, -0x6747e1ce

    return v0
.end method
