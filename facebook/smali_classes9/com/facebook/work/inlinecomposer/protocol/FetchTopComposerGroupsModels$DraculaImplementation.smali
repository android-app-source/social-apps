.class public final Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1795483
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1795484
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1795481
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1795482
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 1795485
    if-nez p1, :cond_0

    .line 1795486
    :goto_0
    return v0

    .line 1795487
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1795488
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1795489
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1795490
    const v2, -0x36c34bc9

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1795491
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1795492
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1795493
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1795494
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v1

    .line 1795495
    const v2, 0x4941894c    # 792724.75f

    invoke-static {p0, v1, v2, p3}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v1

    .line 1795496
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1795497
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1795498
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1795499
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1795500
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1795501
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1795502
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1795503
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x36c34bc9 -> :sswitch_1
        -0x2ecdfc63 -> :sswitch_0
        0x4941894c -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1795518
    new-instance v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1795504
    sparse-switch p2, :sswitch_data_0

    .line 1795505
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1795506
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1795507
    const v1, -0x36c34bc9

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1795508
    :goto_0
    :sswitch_1
    return-void

    .line 1795509
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1795510
    const v1, 0x4941894c    # 792724.75f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x36c34bc9 -> :sswitch_2
        -0x2ecdfc63 -> :sswitch_0
        0x4941894c -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1795511
    if-eqz p1, :cond_0

    .line 1795512
    invoke-static {p0, p1, p2}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;

    move-result-object v1

    .line 1795513
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;

    .line 1795514
    if-eq v0, v1, :cond_0

    .line 1795515
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1795516
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1795517
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1795474
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1795475
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1795476
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1795477
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1795478
    :cond_0
    iput-object p1, p0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->a:LX/15i;

    .line 1795479
    iput p2, p0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->b:I

    .line 1795480
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1795473
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1795472
    new-instance v0, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1795469
    iget v0, p0, LX/1vt;->c:I

    .line 1795470
    move v0, v0

    .line 1795471
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1795466
    iget v0, p0, LX/1vt;->c:I

    .line 1795467
    move v0, v0

    .line 1795468
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1795463
    iget v0, p0, LX/1vt;->b:I

    .line 1795464
    move v0, v0

    .line 1795465
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1795460
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1795461
    move-object v0, v0

    .line 1795462
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1795448
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1795449
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1795450
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1795451
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1795452
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1795453
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1795454
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1795455
    invoke-static {v3, v9, v2}, Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/work/inlinecomposer/protocol/FetchTopComposerGroupsModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1795456
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1795457
    iget v0, p0, LX/1vt;->c:I

    .line 1795458
    move v0, v0

    .line 1795459
    return v0
.end method
