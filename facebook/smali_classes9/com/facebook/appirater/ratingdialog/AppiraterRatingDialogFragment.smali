.class public Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0Sh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0WV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/29r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ba8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ba2;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BaA;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ba5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:Z

.field private v:Z

.field private w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/BZw;",
            "LX/BZx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1798279
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1798280
    iput-boolean v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->u:Z

    .line 1798281
    iput-boolean v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->v:Z

    .line 1798282
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->w:Ljava/util/Map;

    .line 1798283
    return-void
.end method

.method private b()I
    .locals 3

    .prologue
    .line 1798369
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1798370
    const-string v1, "rating"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public static c(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZw;)LX/BZx;
    .locals 2

    .prologue
    .line 1798364
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->w:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BZx;

    .line 1798365
    if-nez v0, :cond_0

    .line 1798366
    new-instance v0, LX/BZx;

    invoke-direct {v0, p0, p1}, LX/BZx;-><init>(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZw;)V

    .line 1798367
    iget-object v1, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->w:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1798368
    :cond_0
    return-object v0
.end method

.method private k()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1798362
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1798363
    const-string v1, "rating_comment"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()LX/BZw;
    .locals 3

    .prologue
    .line 1798360
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1798361
    const-string v1, "current_screen"

    sget-object v2, LX/BZw;->STAR_RATING:LX/BZw;

    invoke-virtual {v2}, LX/BZw;->toInt()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, LX/BZw;->fromInt(I)LX/BZw;

    move-result-object v0

    return-object v0
.end method

.method private m()LX/BZx;
    .locals 1

    .prologue
    .line 1798359
    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->l()LX/BZw;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->c(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZw;)LX/BZx;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1798355
    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->m()LX/BZx;

    move-result-object v0

    .line 1798356
    invoke-virtual {v0}, LX/BZx;->a()Landroid/view/View;

    move-result-object v1

    .line 1798357
    invoke-virtual {v0}, LX/BZx;->b()V

    .line 1798358
    new-instance v0, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/BZw;)V
    .locals 3

    .prologue
    .line 1798345
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->m:LX/0Sh;

    if-nez v0, :cond_0

    .line 1798346
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Called go to screen before Fragment.onCreate was called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1798347
    :cond_0
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->m:LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->a()V

    .line 1798348
    iget-boolean v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->u:Z

    move v0, v0

    .line 1798349
    if-nez v0, :cond_2

    .line 1798350
    :cond_1
    :goto_0
    return-void

    .line 1798351
    :cond_2
    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->l()LX/BZw;

    move-result-object v0

    .line 1798352
    if-eq v0, p1, :cond_1

    .line 1798353
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->u:Z

    .line 1798354
    iget-object v1, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->p:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;

    invoke-direct {v2, p0, p1, v0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;-><init>(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZw;LX/BZw;)V

    const v0, -0x5c30b971

    invoke-static {v1, v2, v0}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1798342
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->v:Z

    .line 1798343
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1798344
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x2079adfd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1798339
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1798340
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    move-object v4, p0

    check-cast v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    invoke-static {p1}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v5

    check-cast v5, LX/0Sh;

    invoke-static {p1}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v6

    check-cast v6, LX/0WV;

    invoke-static {p1}, LX/29r;->a(LX/0QB;)LX/29r;

    move-result-object v7

    check-cast v7, LX/29r;

    invoke-static {p1}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v8

    check-cast v8, Landroid/os/Handler;

    const/16 v9, 0x1781

    invoke-static {p1, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x177f

    invoke-static {p1, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x1782

    invoke-static {p1, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v1, 0x1780

    invoke-static {p1, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v5, v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->m:LX/0Sh;

    iput-object v6, v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->n:LX/0WV;

    iput-object v7, v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->o:LX/29r;

    iput-object v8, v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->p:Landroid/os/Handler;

    iput-object v9, v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->q:LX/0Ot;

    iput-object v10, v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->r:LX/0Ot;

    iput-object v11, v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->s:LX/0Ot;

    iput-object p1, v4, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->t:LX/0Ot;

    .line 1798341
    const/16 v1, 0x2b

    const v2, -0x298f5d26

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 5

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x4c56044

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1798331
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->w:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BZx;

    .line 1798332
    iget-object v3, v0, LX/BZx;->c:LX/BZy;

    if-eqz v3, :cond_0

    .line 1798333
    iget-object v3, v0, LX/BZx;->c:LX/BZy;

    .line 1798334
    const/4 v4, 0x0

    iput-object v4, v3, LX/BZy;->a:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    .line 1798335
    :cond_0
    const/4 v3, 0x0

    iput-object v3, v0, LX/BZx;->c:LX/BZy;

    .line 1798336
    goto :goto_0

    .line 1798337
    :cond_1
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroy()V

    .line 1798338
    const v0, -0x2d15dccb

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3e768f81

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1798327
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->u:Z

    .line 1798328
    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->m()LX/BZx;

    move-result-object v1

    invoke-virtual {v1}, LX/BZx;->c()V

    .line 1798329
    invoke-super {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDestroyView()V

    .line 1798330
    const/16 v1, 0x2b

    const v2, -0x6793a7af

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 11

    .prologue
    .line 1798291
    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->l()LX/BZw;

    move-result-object v0

    .line 1798292
    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->b()I

    move-result v1

    .line 1798293
    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->k()Ljava/lang/String;

    move-result-object v2

    .line 1798294
    iget-object v3, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->n:LX/0WV;

    invoke-virtual {v3}, LX/0WV;->b()I

    move-result v3

    .line 1798295
    iget-boolean v4, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->v:Z

    .line 1798296
    new-instance v5, LX/BZq;

    invoke-direct {v5}, LX/BZq;-><init>()V

    .line 1798297
    iput v1, v5, LX/BZq;->a:I

    .line 1798298
    move-object v5, v5

    .line 1798299
    iput-object v2, v5, LX/BZq;->b:Ljava/lang/String;

    .line 1798300
    move-object v2, v5

    .line 1798301
    int-to-long v6, v3

    .line 1798302
    iput-wide v6, v2, LX/BZq;->d:J

    .line 1798303
    move-object v2, v2

    .line 1798304
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1798305
    iput-wide v8, v2, LX/BZq;->e:J

    .line 1798306
    move-object v8, v2

    .line 1798307
    move-object v2, v8

    .line 1798308
    sget-object v3, LX/BZu;->a:[I

    invoke-virtual {v0}, LX/BZw;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 1798309
    :goto_0
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->o:LX/29r;

    .line 1798310
    new-instance v1, Lcom/facebook/appirater/api/AppRaterReport;

    invoke-direct {v1, v2}, Lcom/facebook/appirater/api/AppRaterReport;-><init>(LX/BZq;)V

    move-object v1, v1

    .line 1798311
    iput-object v1, v0, LX/29r;->r:Lcom/facebook/appirater/api/AppRaterReport;

    .line 1798312
    iget-object v2, v0, LX/29r;->c:LX/29s;

    .line 1798313
    sget-object v3, LX/BZn;->c:LX/0Tn;

    invoke-static {v2, v3, v1}, LX/29s;->a(LX/29s;LX/0Tn;Ljava/lang/Object;)V

    .line 1798314
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/29r;->a(Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;)V

    .line 1798315
    invoke-static {v0}, LX/29r;->m(LX/29r;)V

    .line 1798316
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1798317
    return-void

    .line 1798318
    :pswitch_0
    if-eqz v4, :cond_0

    sget-object v0, LX/BZr;->STARS_DISMISS:LX/BZr;

    :goto_1
    invoke-virtual {v2, v0}, LX/BZq;->a(LX/BZr;)LX/BZq;

    goto :goto_0

    :cond_0
    sget-object v0, LX/BZr;->STARS_BACKGROUND:LX/BZr;

    goto :goto_1

    .line 1798319
    :pswitch_1
    if-eqz v4, :cond_1

    sget-object v0, LX/BZr;->STARS_LOWRATING_CANCEL:LX/BZr;

    :goto_2
    invoke-virtual {v2, v0}, LX/BZq;->a(LX/BZr;)LX/BZq;

    goto :goto_0

    :cond_1
    sget-object v0, LX/BZr;->STARS_LOWRATING_SUBMIT:LX/BZr;

    goto :goto_2

    .line 1798320
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->o:LX/29r;

    invoke-virtual {v0}, LX/29r;->d()Lcom/facebook/appirater/api/FetchISRConfigResult;

    move-result-object v0

    .line 1798321
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/appirater/api/FetchISRConfigResult;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 1798322
    :cond_2
    sget-object v0, LX/BZr;->STARS_STARCHOSEN:LX/BZr;

    invoke-virtual {v2, v0}, LX/BZq;->a(LX/BZr;)LX/BZq;

    goto :goto_0

    .line 1798323
    :cond_3
    iget v0, v0, Lcom/facebook/appirater/api/FetchISRConfigResult;->maxStarsForFeedback:I

    if-gt v1, v0, :cond_4

    .line 1798324
    sget-object v0, LX/BZr;->STARS_LOWRATING_SUBMIT:LX/BZr;

    invoke-virtual {v2, v0}, LX/BZq;->a(LX/BZr;)LX/BZq;

    goto :goto_0

    .line 1798325
    :cond_4
    sget-object v0, LX/BZr;->STARS_STARCHOSEN:LX/BZr;

    invoke-virtual {v2, v0}, LX/BZq;->a(LX/BZr;)LX/BZq;

    goto :goto_0

    .line 1798326
    :pswitch_3
    if-eqz v4, :cond_5

    sget-object v0, LX/BZr;->STARS_HIGHRATING_NOTHANKS:LX/BZr;

    :goto_3
    invoke-virtual {v2, v0}, LX/BZq;->a(LX/BZr;)LX/BZq;

    goto :goto_0

    :cond_5
    sget-object v0, LX/BZr;->STARS_HIGHRATING_GOTOSTORE:LX/BZr;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1798287
    new-instance v0, Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;

    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->b()I

    move-result v1

    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->l()LX/BZw;

    move-result-object v3

    invoke-virtual {v3}, LX/BZw;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 1798288
    iget-object v1, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->o:LX/29r;

    invoke-virtual {v1, v0}, LX/29r;->a(Lcom/facebook/appirater/ratingdialog/RatingDialogSaveState;)V

    .line 1798289
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1798290
    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1798284
    invoke-super {p0, p1, p2}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1798285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->u:Z

    .line 1798286
    return-void
.end method
