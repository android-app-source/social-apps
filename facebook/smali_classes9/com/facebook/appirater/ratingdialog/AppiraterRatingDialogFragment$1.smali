.class public final Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/BZw;

.field public final synthetic b:LX/BZw;

.field public final synthetic c:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZw;LX/BZw;)V
    .locals 0

    .prologue
    .line 1798197
    iput-object p1, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->c:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iput-object p2, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->a:LX/BZw;

    iput-object p3, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->b:LX/BZw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1798198
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->c:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iget-object v1, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->a:LX/BZw;

    .line 1798199
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 1798200
    const-string v3, "current_screen"

    invoke-virtual {v1}, LX/BZw;->toInt()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1798201
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->c:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iget-object v1, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->b:LX/BZw;

    invoke-static {v0, v1}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->c(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZw;)LX/BZx;

    move-result-object v0

    .line 1798202
    invoke-virtual {v0}, LX/BZx;->a()Landroid/view/View;

    move-result-object v1

    .line 1798203
    iget-object v2, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->c:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    iget-object v3, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->a:LX/BZw;

    invoke-static {v2, v3}, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;->c(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZw;)LX/BZx;

    move-result-object v2

    .line 1798204
    invoke-virtual {v2}, LX/BZx;->a()Landroid/view/View;

    move-result-object v3

    .line 1798205
    iget-object v4, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->c:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    .line 1798206
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    const/high16 v6, 0x41200000    # 10.0f

    const/4 v7, 0x0

    invoke-direct {v5, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1798207
    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c005f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    div-int/lit8 v6, v6, 0x2

    int-to-long v7, v6

    invoke-virtual {v5, v7, v8}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1798208
    new-instance v6, LX/BZv;

    invoke-direct {v6, v4, v0}, LX/BZv;-><init>(Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;LX/BZx;)V

    invoke-virtual {v5, v6}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1798209
    move-object v0, v5

    .line 1798210
    invoke-virtual {v1, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1798211
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->c:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    .line 1798212
    new-instance v5, Landroid/view/animation/AlphaAnimation;

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v5, v6, v7}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1798213
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c005f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v6

    int-to-long v7, v6

    invoke-virtual {v5, v7, v8}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 1798214
    move-object v0, v5

    .line 1798215
    invoke-virtual {v3, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 1798216
    invoke-virtual {v2}, LX/BZx;->b()V

    .line 1798217
    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1798218
    iget-object v0, p0, Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment$1;->c:Lcom/facebook/appirater/ratingdialog/AppiraterRatingDialogFragment;

    .line 1798219
    iget-object v1, v0, Landroid/support/v4/app/DialogFragment;->f:Landroid/app/Dialog;

    move-object v0, v1

    .line 1798220
    check-cast v0, LX/2EJ;

    invoke-virtual {v0, v3}, LX/2EJ;->a(Landroid/view/View;)V

    .line 1798221
    return-void
.end method
