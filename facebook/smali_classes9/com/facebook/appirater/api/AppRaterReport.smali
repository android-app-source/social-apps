.class public Lcom/facebook/appirater/api/AppRaterReport;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/appirater/api/AppRaterReportDeserializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/appirater/api/AppRaterReport;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final buildNumber:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "build_number"
    .end annotation
.end field

.field public final lastEvent:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_event"
    .end annotation
.end field

.field public final lastEventCompletedAtMillis:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "last_event_completed_at_millis"
    .end annotation
.end field

.field public final reviewText:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "review_text"
    .end annotation
.end field

.field public final starRating:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "star_rating"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1798068
    const-class v0, Lcom/facebook/appirater/api/AppRaterReportDeserializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1798069
    new-instance v0, LX/BZp;

    invoke-direct {v0}, LX/BZp;-><init>()V

    sput-object v0, Lcom/facebook/appirater/api/AppRaterReport;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 1798045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1798046
    const/4 v0, -0x1

    iput v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->starRating:I

    .line 1798047
    iput-object v1, p0, Lcom/facebook/appirater/api/AppRaterReport;->reviewText:Ljava/lang/String;

    .line 1798048
    iput-object v1, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEvent:Ljava/lang/String;

    .line 1798049
    iput-wide v2, p0, Lcom/facebook/appirater/api/AppRaterReport;->buildNumber:J

    .line 1798050
    iput-wide v2, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEventCompletedAtMillis:J

    .line 1798051
    return-void
.end method

.method public constructor <init>(LX/BZq;)V
    .locals 2

    .prologue
    .line 1798061
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1798062
    iget v0, p1, LX/BZq;->a:I

    iput v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->starRating:I

    .line 1798063
    iget-object v0, p1, LX/BZq;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->reviewText:Ljava/lang/String;

    .line 1798064
    iget-object v0, p1, LX/BZq;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEvent:Ljava/lang/String;

    .line 1798065
    iget-wide v0, p1, LX/BZq;->d:J

    iput-wide v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->buildNumber:J

    .line 1798066
    iget-wide v0, p1, LX/BZq;->e:J

    iput-wide v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEventCompletedAtMillis:J

    .line 1798067
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1798070
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1798071
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->starRating:I

    .line 1798072
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->reviewText:Ljava/lang/String;

    .line 1798073
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEvent:Ljava/lang/String;

    .line 1798074
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->buildNumber:J

    .line 1798075
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEventCompletedAtMillis:J

    .line 1798076
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 4

    .prologue
    .line 1798060
    iget v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->starRating:I

    if-ltz v0, :cond_0

    iget-wide v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEventCompletedAtMillis:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1798059
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1798058
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppRaterReport:\nStar Rating: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/facebook/appirater/api/AppRaterReport;->starRating:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nReview Text: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/appirater/api/AppRaterReport;->reviewText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nLast Event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEvent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nBuild Number: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/appirater/api/AppRaterReport;->buildNumber:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nLast Event Completed At: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEventCompletedAtMillis:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1798052
    iget v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->starRating:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1798053
    iget-object v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->reviewText:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1798054
    iget-object v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEvent:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1798055
    iget-wide v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->buildNumber:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1798056
    iget-wide v0, p0, Lcom/facebook/appirater/api/AppRaterReport;->lastEventCompletedAtMillis:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1798057
    return-void
.end method
