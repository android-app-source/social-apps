.class public Lcom/facebook/fbui/widget/megaphone/Megaphone;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Lcom/facebook/resources/ui/FbTextView;

.field private d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

.field private e:Lcom/facebook/fbui/facepile/FacepileView;

.field private f:Lcom/facebook/resources/ui/FbTextView;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/widget/Button;

.field private k:Landroid/widget/Button;

.field private l:I

.field public m:LX/AhV;

.field private final n:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1703084
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1703085
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1703086
    const v0, 0x7f0102dd

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1703087
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1703088
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1703089
    new-instance v0, LX/AhU;

    invoke-direct {v0, p0}, LX/AhU;-><init>(Lcom/facebook/fbui/widget/megaphone/Megaphone;)V

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->n:Landroid/view/View$OnClickListener;

    .line 1703090
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1703091
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1703092
    const v0, 0x7f03060f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1703093
    const v0, 0x7f020922

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setBackgroundResource(I)V

    .line 1703094
    const v0, 0x7f0d10da

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1703095
    const v0, 0x7f0d10db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1703096
    const v0, 0x7f0d10dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1703097
    const v0, 0x7f0d10df

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    .line 1703098
    const v0, 0x7f0d10e0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->e:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1703099
    const v0, 0x7f0d10e1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 1703100
    const v0, 0x7f0d10ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->g:Landroid/view/View;

    .line 1703101
    const v0, 0x7f0d10e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->h:Landroid/view/View;

    .line 1703102
    const v0, 0x7f0d10e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->i:Landroid/view/View;

    .line 1703103
    const v0, 0x7f0d10e8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->j:Landroid/widget/Button;

    .line 1703104
    const v0, 0x7f0d10e7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->k:Landroid/widget/Button;

    .line 1703105
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1703106
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->l:I

    .line 1703107
    sget-object v0, LX/03r;->Megaphone:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1703108
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1703109
    if-lez v1, :cond_0

    .line 1703110
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(I)V

    .line 1703111
    :goto_0
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1703112
    if-lez v1, :cond_1

    .line 1703113
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(I)V

    .line 1703114
    :goto_1
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1703115
    if-lez v1, :cond_2

    .line 1703116
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(I)V

    .line 1703117
    :goto_2
    const/16 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1703118
    if-lez v1, :cond_3

    .line 1703119
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSecondaryButtonText(I)V

    .line 1703120
    :goto_3
    const/16 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1703121
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1703122
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1703123
    return-void

    .line 1703124
    :cond_0
    const/16 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1703125
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1703126
    :cond_1
    const/16 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1703127
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSubtitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 1703128
    :cond_2
    const/16 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1703129
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 1703130
    :cond_3
    const/16 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1703131
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setSecondaryButtonText(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method


# virtual methods
.method public final a(II)V
    .locals 1

    .prologue
    .line 1703132
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 1703133
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1703134
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1703135
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1703136
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->j:Landroid/widget/Button;

    invoke-static {v0}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1703137
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->k:Landroid/widget/Button;

    invoke-static {v0}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1703180
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {v0}, LX/191;->a(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method public final h()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 1703138
    const v0, 0x7f0d10e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->i:Landroid/view/View;

    .line 1703139
    const v0, 0x7f0d10e5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->j:Landroid/widget/Button;

    .line 1703140
    const v0, 0x7f0d10e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->k:Landroid/widget/Button;

    .line 1703141
    const v0, 0x7f0d10e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->g:Landroid/view/View;

    .line 1703142
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->g:Landroid/view/View;

    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1703143
    const v0, 0x7f0d10dd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1703144
    const v0, 0x7f0d10de

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 1703145
    const v0, 0x7f020925

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setBackgroundResource(I)V

    .line 1703146
    const v0, 0x7f0d10dc

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1703147
    const v0, 0x7f0d10e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1703148
    const v0, 0x7f0d10db

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1703149
    const v0, 0x7f0d10e6

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1703150
    const v0, 0x7f0d10ea

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1703151
    return-void
.end method

.method public setDividerBackground(I)V
    .locals 1

    .prologue
    .line 1703022
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->h:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1703023
    return-void
.end method

.method public setFacepileDrawables(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1703152
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSocialContextContainer(Z)V

    .line 1703153
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->e:Lcom/facebook/fbui/facepile/FacepileView;

    iget v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->l:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceSize(I)V

    .line 1703154
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->e:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceDrawables(Ljava/util/List;)V

    .line 1703155
    return-void

    .line 1703156
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setFacepileUrls(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1703157
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSocialContextContainer(Z)V

    .line 1703158
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->e:Lcom/facebook/fbui/facepile/FacepileView;

    iget v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->l:I

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceSize(I)V

    .line 1703159
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->e:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 1703160
    return-void

    .line 1703161
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 1703162
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1703163
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1703164
    return-void

    .line 1703165
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImageResource(I)V
    .locals 1

    .prologue
    .line 1703166
    if-lez p1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1703167
    return-void

    .line 1703168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setImageUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1703169
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 1703170
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1703171
    return-void
.end method

.method public setImageView(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1703172
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailView(Landroid/view/View;)V

    .line 1703173
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->a:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 1703174
    return-void

    .line 1703175
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnDismissListener(LX/AhV;)V
    .locals 0

    .prologue
    .line 1703176
    iput-object p1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->m:LX/AhV;

    .line 1703177
    return-void
.end method

.method public setOnPrimaryButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1703082
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->j:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1703083
    return-void
.end method

.method public setOnSecondaryButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1703178
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->k:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1703179
    return-void
.end method

.method public setPrimaryButtonText(I)V
    .locals 1

    .prologue
    .line 1703046
    if-eqz p1, :cond_0

    .line 1703047
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->j:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 1703048
    :cond_0
    return-void
.end method

.method public setPrimaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1703024
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->j:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1703025
    return-void
.end method

.method public setSecondaryButtonText(I)V
    .locals 1

    .prologue
    .line 1703026
    if-eqz p1, :cond_0

    .line 1703027
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->k:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(I)V

    .line 1703028
    :cond_0
    return-void
.end method

.method public setSecondaryButtonText(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1703029
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->k:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 1703030
    return-void
.end method

.method public setShowButtonsContainer(Z)V
    .locals 2

    .prologue
    .line 1703031
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->i:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1703032
    return-void

    .line 1703033
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setShowCloseButton(Z)V
    .locals 2

    .prologue
    .line 1703034
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->g:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1703035
    return-void

    .line 1703036
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setShowPrimaryButton(Z)V
    .locals 2

    .prologue
    .line 1703037
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->j:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1703038
    return-void

    .line 1703039
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setShowSecondaryButton(Z)V
    .locals 2

    .prologue
    .line 1703040
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->k:Landroid/widget/Button;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 1703041
    return-void

    .line 1703042
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setShowSocialContextContainer(Z)V
    .locals 2

    .prologue
    .line 1703043
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->d:Lcom/facebook/fbui/widget/layout/ImageBlockLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setVisibility(I)V

    .line 1703044
    return-void

    .line 1703045
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setShowSubtitle(Z)V
    .locals 2

    .prologue
    .line 1703049
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1703050
    return-void

    .line 1703051
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setShowTitle(Z)V
    .locals 2

    .prologue
    .line 1703052
    iget-object v1, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1703053
    return-void

    .line 1703054
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSocialContext(I)V
    .locals 1

    .prologue
    .line 1703055
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1703056
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSocialContextContainer(Z)V

    .line 1703057
    return-void

    .line 1703058
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSocialContext(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1703059
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1703060
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/megaphone/Megaphone;->setShowSocialContextContainer(Z)V

    .line 1703061
    return-void

    .line 1703062
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setSocialContextMaxLines(I)V
    .locals 2

    .prologue
    .line 1703063
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 1703064
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->f:Lcom/facebook/resources/ui/FbTextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1703065
    return-void
.end method

.method public setSubtitle(I)V
    .locals 1

    .prologue
    .line 1703066
    if-eqz p1, :cond_0

    .line 1703067
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1703068
    :cond_0
    return-void
.end method

.method public setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1703069
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1703070
    return-void
.end method

.method public setSubtitleMaxLines(I)V
    .locals 2

    .prologue
    .line 1703071
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 1703072
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->c:Lcom/facebook/resources/ui/FbTextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1703073
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 1703074
    if-eqz p1, :cond_0

    .line 1703075
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1703076
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1703077
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1703078
    return-void
.end method

.method public setTitleMaxLines(I)V
    .locals 2

    .prologue
    .line 1703079
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 1703080
    iget-object v0, p0, Lcom/facebook/fbui/widget/megaphone/Megaphone;->b:Lcom/facebook/resources/ui/FbTextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1703081
    return-void
.end method
