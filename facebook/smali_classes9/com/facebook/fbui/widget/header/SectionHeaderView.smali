.class public Lcom/facebook/fbui/widget/header/SectionHeaderView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:[I


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1702511
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x101014f
        0x1010129
        0x1010129
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1702515
    const/4 v0, 0x0

    invoke-direct {p0, p1, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702516
    invoke-direct {p0, p1, v1}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1702517
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1702512
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702513
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1702514
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1702492
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702493
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1702494
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1702495
    const v0, 0x7f030614

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1702496
    const v0, 0x7f0d0396

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->b:Landroid/widget/TextView;

    .line 1702497
    const v0, 0x7f0d10f0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->c:Landroid/view/View;

    .line 1702498
    const v0, 0x7f0d10f1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->d:Landroid/view/View;

    .line 1702499
    sget-object v0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->a:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1702500
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1702501
    if-eqz v1, :cond_0

    .line 1702502
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setTitleText(I)V

    .line 1702503
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1702504
    if-eqz v1, :cond_1

    .line 1702505
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setTopDivider(I)V

    .line 1702506
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1702507
    if-eqz v1, :cond_2

    .line 1702508
    invoke-virtual {p0, v1}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setBottomDivider(I)V

    .line 1702509
    :cond_2
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1702510
    return-void
.end method


# virtual methods
.method public setBottomDivider(I)V
    .locals 1

    .prologue
    .line 1702484
    iget-object v0, p0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1702485
    return-void
.end method

.method public setTitleText(I)V
    .locals 1

    .prologue
    .line 1702486
    iget-object v0, p0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 1702487
    return-void
.end method

.method public setTitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1702488
    iget-object v0, p0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1702489
    return-void
.end method

.method public setTopDivider(I)V
    .locals 1

    .prologue
    .line 1702490
    iget-object v0, p0, Lcom/facebook/fbui/widget/header/SectionHeaderView;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1702491
    return-void
.end method
