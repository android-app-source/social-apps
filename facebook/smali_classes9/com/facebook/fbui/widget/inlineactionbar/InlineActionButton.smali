.class public Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;
.super Lcom/facebook/fbui/widget/text/ImageWithTextView;
.source ""

# interfaces
.implements LX/3uq;
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:[I

.field private static final b:[I


# instance fields
.field public c:LX/3uw;

.field private d:LX/3v3;

.field private e:Landroid/content/res/ColorStateList;

.field private f:Z

.field private g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1702726
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->a:[I

    .line 1702727
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101009f

    aput v2, v0, v1

    sput-object v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->b:[I

    return-void

    .line 1702728
    :array_0
    .array-data 4
        0x101009f
        0x10100a0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1702729
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;)V

    .line 1702730
    iput-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->f:Z

    .line 1702731
    iput-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->g:Z

    .line 1702732
    invoke-direct {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->c()V

    .line 1702733
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1702716
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1702717
    iput-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->f:Z

    .line 1702718
    iput-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->g:Z

    .line 1702719
    invoke-direct {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->c()V

    .line 1702720
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1702734
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/ImageWithTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1702735
    iput-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->f:Z

    .line 1702736
    iput-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->g:Z

    .line 1702737
    invoke-direct {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->c()V

    .line 1702738
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1702739
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setGravity(I)V

    .line 1702740
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setSingleLine()V

    .line 1702741
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1702742
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0d79

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1702743
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setCompoundDrawablePadding(I)V

    .line 1702744
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setPadding(IIII)V

    .line 1702745
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setOrientation(I)V

    .line 1702746
    invoke-virtual {p0, p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1702747
    invoke-virtual {p0, v2}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setFocusable(Z)V

    .line 1702748
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setFocusableInTouchMode(Z)V

    .line 1702749
    return-void
.end method


# virtual methods
.method public final a(LX/3v3;)V
    .locals 1

    .prologue
    .line 1702750
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->a(LX/3v3;I)V

    .line 1702751
    return-void
.end method

.method public final a(LX/3v3;I)V
    .locals 1

    .prologue
    .line 1702752
    iput-object p1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->d:LX/3v3;

    .line 1702753
    invoke-virtual {p1}, LX/3v3;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    .line 1702754
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setText(Ljava/lang/CharSequence;)V

    .line 1702755
    invoke-virtual {p1}, LX/3v3;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 1702756
    invoke-virtual {p1}, LX/3v3;->isEnabled()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setEnabled(Z)V

    .line 1702757
    invoke-virtual {p1}, LX/3v3;->isCheckable()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setCheckable(Z)V

    .line 1702758
    invoke-virtual {p1}, LX/3v3;->isChecked()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setChecked(Z)V

    .line 1702759
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1702760
    const/4 v0, 0x0

    return v0
.end method

.method public final canScrollHorizontally(I)Z
    .locals 1

    .prologue
    .line 1702761
    const/4 v0, 0x0

    return v0
.end method

.method public final drawableStateChanged()V
    .locals 4

    .prologue
    .line 1702762
    invoke-super {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->drawableStateChanged()V

    .line 1702763
    iget-object v0, p0, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a:Landroid/graphics/drawable/Drawable;

    move-object v0, v0

    .line 1702764
    if-nez v0, :cond_0

    .line 1702765
    :goto_0
    return-void

    .line 1702766
    :cond_0
    iget-object v1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->e:Landroid/content/res/ColorStateList;

    if-eqz v1, :cond_1

    .line 1702767
    iget-object v1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->e:Landroid/content/res/ColorStateList;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getDrawableState()[I

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/ColorStateList;->getColorForState([II)I

    move-result v1

    .line 1702768
    new-instance v2, Landroid/graphics/PorterDuffColorFilter;

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v2, v1, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0

    .line 1702769
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public getItemData()LX/3v3;
    .locals 1

    .prologue
    .line 1702725
    iget-object v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->d:LX/3v3;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x59288b05

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1702770
    iget-object v1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->c:LX/3uw;

    if-eqz v1, :cond_0

    .line 1702771
    iget-object v1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->c:LX/3uw;

    iget-object v2, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->d:LX/3v3;

    invoke-interface {v1, v2}, LX/3uw;->a(LX/3v3;)Z

    .line 1702772
    :cond_0
    const v1, -0x7235e5af

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 1702675
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->f:Z

    if-nez v0, :cond_0

    .line 1702676
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 1702677
    :goto_0
    return-object v0

    .line 1702678
    :cond_0
    add-int/lit8 v0, p1, 0x2

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onCreateDrawableState(I)[I

    move-result-object v1

    .line 1702679
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->g:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->a:[I

    :goto_1
    invoke-static {v1, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->mergeDrawableStates([I[I)[I

    move-object v0, v1

    .line 1702680
    goto :goto_0

    .line 1702681
    :cond_1
    sget-object v0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->b:[I

    goto :goto_1
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x7f07026e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1702682
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 1702683
    if-nez v1, :cond_0

    .line 1702684
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0d77

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p1

    .line 1702685
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onMeasure(II)V

    .line 1702686
    const/16 v1, 0x2d

    const v2, -0x6385876c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPreDraw()Z
    .locals 1

    .prologue
    .line 1702687
    invoke-super {p0}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->onPreDraw()Z

    .line 1702688
    const/4 v0, 0x1

    return v0
.end method

.method public setBackgroundResource(I)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1702689
    const/4 v0, 0x4

    new-array v0, v0, [I

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getPaddingLeft()I

    move-result v1

    aput v1, v0, v2

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getPaddingTop()I

    move-result v1

    aput v1, v0, v3

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getPaddingRight()I

    move-result v1

    aput v1, v0, v4

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->getPaddingBottom()I

    move-result v1

    aput v1, v0, v5

    .line 1702690
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setBackgroundResource(I)V

    .line 1702691
    aget v1, v0, v2

    aget v2, v0, v3

    aget v3, v0, v4

    aget v0, v0, v5

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setPadding(IIII)V

    .line 1702692
    return-void
.end method

.method public setCheckable(Z)V
    .locals 1

    .prologue
    .line 1702693
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->f:Z

    if-eq v0, p1, :cond_0

    .line 1702694
    iput-boolean p1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->f:Z

    .line 1702695
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->refreshDrawableState()V

    .line 1702696
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->invalidate()V

    .line 1702697
    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 1702698
    iget-boolean v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->g:Z

    if-eq v0, p1, :cond_0

    .line 1702699
    iput-boolean p1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->g:Z

    .line 1702700
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->refreshDrawableState()V

    .line 1702701
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->invalidate()V

    .line 1702702
    :cond_0
    return-void
.end method

.method public setGlyphColor(I)V
    .locals 1

    .prologue
    .line 1702703
    invoke-static {p1}, Landroid/content/res/ColorStateList;->valueOf(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setGlyphColor(Landroid/content/res/ColorStateList;)V

    .line 1702704
    return-void
.end method

.method public setGlyphColor(Landroid/content/res/ColorStateList;)V
    .locals 1

    .prologue
    .line 1702705
    iget-object v0, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->e:Landroid/content/res/ColorStateList;

    if-eq v0, p1, :cond_0

    .line 1702706
    iput-object p1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->e:Landroid/content/res/ColorStateList;

    .line 1702707
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->refreshDrawableState()V

    .line 1702708
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->invalidate()V

    .line 1702709
    :cond_0
    return-void
.end method

.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .prologue
    .line 1702710
    if-eqz p1, :cond_0

    .line 1702711
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 1702712
    :cond_0
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1702713
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->refreshDrawableState()V

    .line 1702714
    invoke-virtual {p0}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->invalidate()V

    .line 1702715
    return-void
.end method

.method public setItemInvoker(LX/3uw;)V
    .locals 0

    .prologue
    .line 1702721
    iput-object p1, p0, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->c:LX/3uw;

    .line 1702722
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 1702723
    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/inlineactionbar/InlineActionButton;->setText(Ljava/lang/CharSequence;)V

    .line 1702724
    return-void
.end method
