.class public final Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/AUE;

.field private final b:LX/AUD;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/AUD",
            "<TD;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/AUE;LX/AUD;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/AUD",
            "<TD;>;)V"
        }
    .end annotation

    .prologue
    .line 1677815
    iput-object p1, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->a:LX/AUE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1677816
    iput-object p2, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->b:LX/AUD;

    .line 1677817
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1677818
    :try_start_0
    iget-object v0, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->b:LX/AUD;

    iget-object v1, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->a:LX/AUE;

    invoke-virtual {v1}, LX/AUE;->b()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, LX/AUD;->b:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1677819
    iget-object v0, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->a:LX/AUE;

    iget-object v0, v0, LX/AUE;->b:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$PostResultRunnable;

    iget-object v2, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->a:LX/AUE;

    iget-object v3, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->b:LX/AUD;

    invoke-direct {v1, v2, v3}, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$PostResultRunnable;-><init>(LX/AUE;LX/AUD;)V

    const v2, -0x3de457de

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 1677820
    return-void

    .line 1677821
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->a:LX/AUE;

    iget-object v1, v1, LX/AUE;->b:Landroid/os/Handler;

    new-instance v2, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$PostResultRunnable;

    iget-object v3, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->a:LX/AUE;

    iget-object v4, p0, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$LoadRunnable;->b:LX/AUD;

    invoke-direct {v2, v3, v4}, Lcom/facebook/crudolib/dbquery/loader/inprocess/ExecutorLoader$PostResultRunnable;-><init>(LX/AUE;LX/AUD;)V

    const v3, -0x63e1f98d

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    throw v0
.end method
