.class public Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Landroid/widget/LinearLayout;

.field private c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:Lcom/facebook/widget/text/BetterTextView;

.field private e:Landroid/widget/LinearLayout;

.field private f:Lcom/facebook/fbui/glyph/GlyphView;

.field private g:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1821271
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1821272
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->a()V

    .line 1821273
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821268
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1821269
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->a()V

    .line 1821270
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const v3, -0x6f6b64

    .line 1821256
    const-class v0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;

    invoke-static {v0, p0}, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1821257
    const v0, 0x7f0304a8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1821258
    const v0, 0x7f0d0da3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->b:Landroid/widget/LinearLayout;

    .line 1821259
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0da6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1821260
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->b:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0da7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1821261
    const v0, 0x7f0d0da4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->e:Landroid/widget/LinearLayout;

    .line 1821262
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->e:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0da6

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1821263
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->a:LX/0wM;

    const v2, 0x7f0209c5

    invoke-virtual {v1, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1821264
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->e:Landroid/widget/LinearLayout;

    const v1, 0x7f0d0da7

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->g:Lcom/facebook/widget/text/BetterTextView;

    .line 1821265
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0812ad

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1821266
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->g:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1821267
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(LX/BnW;IZ)V
    .locals 3

    .prologue
    .line 1821246
    if-eqz p3, :cond_0

    const v0, -0xa76f01

    .line 1821247
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p1, LX/BnW;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1821248
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 1821249
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1821250
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->b:Landroid/widget/LinearLayout;

    iget-object v1, p1, LX/BnW;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1821251
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->b:Landroid/widget/LinearLayout;

    iget-object v1, p1, LX/BnW;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1821252
    return-void

    .line 1821253
    :cond_0
    const v0, -0x6f6b64

    goto :goto_0
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1821254
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCardBottomActionView;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1821255
    return-void
.end method
