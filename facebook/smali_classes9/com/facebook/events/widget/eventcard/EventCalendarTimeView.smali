.class public Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field public a:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0W9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Landroid/text/style/MetricAffectingSpan;

.field private d:Landroid/text/style/MetricAffectingSpan;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1821242
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 1821243
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a()V

    .line 1821244
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1821239
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1821240
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a()V

    .line 1821241
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1821236
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1821237
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a()V

    .line 1821238
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1821232
    const-class v0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    invoke-static {v0, p0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1821233
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0616

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->d:Landroid/text/style/MetricAffectingSpan;

    .line 1821234
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0617

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->c:Landroid/text/style/MetricAffectingSpan;

    .line 1821235
    return-void
.end method

.method private static a(Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;LX/6RZ;LX/0W9;)V
    .locals 0

    .prologue
    .line 1821245
    iput-object p1, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a:LX/6RZ;

    iput-object p2, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->b:LX/0W9;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;

    invoke-static {v1}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v0

    check-cast v0, LX/6RZ;

    invoke-static {v1}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;LX/6RZ;LX/0W9;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    .line 1821229
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->d:Landroid/text/style/MetricAffectingSpan;

    .line 1821230
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->c:Landroid/text/style/MetricAffectingSpan;

    .line 1821231
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v6, 0x11

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1821220
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move v0, v1

    .line 1821221
    :cond_1
    :goto_0
    return v0

    .line 1821222
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->b:LX/0W9;

    invoke-virtual {v3}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1821223
    iget-object v3, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1821224
    iput-object v2, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->e:Ljava/lang/String;

    .line 1821225
    new-instance v3, Landroid/text/SpannableStringBuilder;

    invoke-direct {v3, v2}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 1821226
    iget-object v4, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->c:Landroid/text/style/MetricAffectingSpan;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v3, v4, v1, v5, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1821227
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->d:Landroid/text/style/MetricAffectingSpan;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v3, v1, v4, v2, v6}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1821228
    invoke-virtual {p0, v3}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/Date;)Z
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    .line 1821217
    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a:LX/6RZ;

    invoke-virtual {v0, p1}, LX/6RZ;->d(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1821218
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a:LX/6RZ;

    invoke-virtual {v1, p1}, LX/6RZ;->c(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 1821219
    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/widget/eventcard/EventCalendarTimeView;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
