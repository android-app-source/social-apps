.class public Lcom/facebook/events/widget/eventcard/EventActionButtonView;
.super Lcom/facebook/fbui/glyph/GlyphView;
.source ""


# instance fields
.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:Z

.field private d:Landroid/graphics/Paint;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1821089
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1821090
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1821111
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/glyph/GlyphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1821112
    const-class v0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    invoke-static {v0, p0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1821113
    if-eqz p2, :cond_1

    .line 1821114
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->EventActionButtonAttrs:[I

    invoke-virtual {v0, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1821115
    if-eqz v0, :cond_1

    .line 1821116
    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->c:Z

    .line 1821117
    iget-boolean v1, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->c:Z

    if-eqz v1, :cond_0

    .line 1821118
    invoke-direct {p0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->b()V

    .line 1821119
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1821120
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->b:LX/0wM;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1821104
    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1821105
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->d:Landroid/graphics/Paint;

    .line 1821106
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->d:Landroid/graphics/Paint;

    const v2, 0x7f0a04c0

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1821107
    const v1, 0x7f0b0b5b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->f:I

    .line 1821108
    iget-object v1, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->d:Landroid/graphics/Paint;

    iget v2, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->f:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1821109
    const v1, 0x7f0b0b5c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->e:I

    .line 1821110
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1821101
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setVisibility(I)V

    .line 1821102
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1821103
    return-void
.end method

.method public final a(LX/BnW;)V
    .locals 1

    .prologue
    .line 1821097
    iget-object v0, p1, LX/BnW;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1821098
    iget-object v0, p1, LX/BnW;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1821099
    iget-object v0, p1, LX/BnW;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p0, v0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1821100
    return-void
.end method

.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 1821091
    invoke-super {p0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1821092
    iget-boolean v0, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->d:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    .line 1821093
    invoke-static {p0}, LX/0vv;->h(Landroid/view/View;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->getWidth()I

    move-result v0

    iget v1, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->f:I

    sub-int/2addr v0, v1

    .line 1821094
    :goto_0
    int-to-float v1, v0

    iget v2, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->e:I

    int-to-float v2, v2

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->e:I

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1821095
    :cond_0
    return-void

    .line 1821096
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
