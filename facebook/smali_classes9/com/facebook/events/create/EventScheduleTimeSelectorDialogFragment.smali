.class public Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:Ljava/util/Calendar;

.field public q:J

.field public r:LX/BjY;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1812771
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1812772
    return-void
.end method

.method public static a(JJ)Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;
    .locals 4

    .prologue
    .line 1812750
    new-instance v0, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    invoke-direct {v0}, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;-><init>()V

    .line 1812751
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1812752
    const-wide/16 v2, 0x0

    cmp-long v2, p0, v2

    if-lez v2, :cond_0

    .line 1812753
    const-string v2, "extra_scheduled_publish_time"

    invoke-virtual {v1, v2, p0, p1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1812754
    :cond_0
    const-string v2, "extra_event_start_time"

    invoke-virtual {v1, v2, p2, p3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 1812755
    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1812756
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    const/16 v1, 0x2e4

    invoke-static {v2, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v2}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {v2}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v2

    check-cast v2, LX/0kL;

    iput-object p0, p1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->m:LX/0Or;

    iput-object v1, p1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->n:LX/0SG;

    iput-object v2, p1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->o:LX/0kL;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 1812757
    new-instance v0, LX/Bji;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->p:Ljava/util/Calendar;

    iget-object v3, p0, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->r:LX/BjY;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Bji;-><init>(Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;Landroid/content/Context;Ljava/util/Calendar;LX/BjY;)V

    .line 1812758
    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x2

    const-wide/16 v4, 0x0

    const/16 v0, 0x2a

    const v1, 0x50d831a9

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1812759
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 1812760
    const-class v1, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;

    invoke-static {v1, p0}, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1812761
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->p:Ljava/util/Calendar;

    .line 1812762
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1812763
    if-eqz v1, :cond_1

    .line 1812764
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1812765
    const-string v2, "extra_scheduled_publish_time"

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1812766
    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1812767
    iget-object v1, p0, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->p:Ljava/util/Calendar;

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1812768
    :cond_0
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1812769
    const-string v2, "extra_event_start_time"

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/events/create/EventScheduleTimeSelectorDialogFragment;->q:J

    .line 1812770
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x450a7ec6

    invoke-static {v6, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
