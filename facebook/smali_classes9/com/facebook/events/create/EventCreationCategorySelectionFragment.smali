.class public Lcom/facebook/events/create/EventCreationCategorySelectionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Bjn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Bil;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:Z

.field public f:LX/Bik;

.field private g:Ljava/lang/String;

.field private h:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

.field private i:LX/Bim;

.field private j:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1810877
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1810878
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;

    invoke-static {v3}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v1

    check-cast v1, LX/0zG;

    new-instance v0, LX/Bjn;

    invoke-static {v3}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v3}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    invoke-direct {v0, v2, p0}, LX/Bjn;-><init>(LX/0tX;LX/1Ck;)V

    move-object v2, v0

    check-cast v2, LX/Bjn;

    const-class p0, LX/Bil;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Bil;

    iput-object v1, p1, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->a:LX/0zG;

    iput-object v2, p1, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->b:LX/Bjn;

    iput-object v3, p1, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->c:LX/Bil;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/create/EventCreationCategorySelectionFragment;Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;)V
    .locals 3

    .prologue
    .line 1810835
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1810836
    const-string v1, "extra_selected_category"

    invoke-static {v0, v1, p1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1810837
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1810838
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1810839
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1810860
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1810861
    const-class v0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1810862
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1810863
    const-string v1, "extra_title_bar_content"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->d:Ljava/lang/String;

    .line 1810864
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1810865
    const-string v1, "extra_is_subcateory"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->e:Z

    .line 1810866
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->c:LX/Bil;

    iget-boolean v1, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->e:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1810867
    new-instance p1, LX/Bik;

    const-class v2, LX/Bkb;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/Bkb;

    invoke-direct {p1, v1, v2}, LX/Bik;-><init>(Ljava/lang/Boolean;LX/Bkb;)V

    .line 1810868
    move-object v0, p1

    .line 1810869
    iput-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->f:LX/Bik;

    .line 1810870
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->e:Z

    if-eqz v0, :cond_0

    .line 1810871
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1810872
    const-string v1, "extra_category_group"

    invoke-static {v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->h:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    .line 1810873
    :goto_0
    new-instance v0, LX/Bim;

    invoke-direct {v0, p0}, LX/Bim;-><init>(Lcom/facebook/events/create/EventCreationCategorySelectionFragment;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->i:LX/Bim;

    .line 1810874
    return-void

    .line 1810875
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1810876
    const-string v1, "extra_page_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1810879
    const/16 v0, 0x6e

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1810880
    const-string v0, "extra_selected_category"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;

    .line 1810881
    invoke-static {p0, v0}, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->a$redex0(Lcom/facebook/events/create/EventCreationCategorySelectionFragment;Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;)V

    .line 1810882
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x1c7ccd0b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1810859
    const v1, 0x7f0304b8

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x1e304164

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x300805b7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1810845
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1810846
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->d:Ljava/lang/String;

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1810847
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->e:Z

    if-eqz v0, :cond_0

    .line 1810848
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->f:LX/Bik;

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->h:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;->a()LX/0Px;

    move-result-object v2

    .line 1810849
    iput-object v2, v0, LX/Bik;->c:Ljava/util/List;

    .line 1810850
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 1810851
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->f:LX/Bik;

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->i:LX/Bim;

    .line 1810852
    iput-object v2, v0, LX/Bik;->d:LX/Bim;

    .line 1810853
    const v0, 0x227ac5ea

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1810854
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->b:LX/Bjn;

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->g:Ljava/lang/String;

    new-instance v3, LX/Bin;

    invoke-direct {v3, p0}, LX/Bin;-><init>(Lcom/facebook/events/create/EventCreationCategorySelectionFragment;)V

    .line 1810855
    new-instance v4, LX/Bjl;

    invoke-direct {v4, v0, v2}, LX/Bjl;-><init>(LX/Bjn;Ljava/lang/String;)V

    .line 1810856
    new-instance v5, LX/Bjm;

    invoke-direct {v5, v0, v3}, LX/Bjm;-><init>(LX/Bjn;LX/Bin;)V

    .line 1810857
    iget-object v6, v0, LX/Bjn;->b:LX/1Ck;

    const-string v7, "fetchCategories"

    invoke-virtual {v6, v7, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1810858
    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1810840
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1810841
    const v0, 0x7f0d0de6

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->j:Landroid/support/v7/widget/RecyclerView;

    .line 1810842
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->j:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/1P1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1810843
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->j:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationCategorySelectionFragment;->f:LX/Bik;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1810844
    return-void
.end method
