.class public Lcom/facebook/events/create/EventCreationNikumanActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# static fields
.field private static final T:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public A:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Bmo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/Bjw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/Blh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:Lcom/facebook/auth/viewercontext/ViewerContext;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/Bjs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:LX/11u;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:Ljava/lang/String;
    .annotation runtime Lcom/facebook/work/config/community/WorkCommunityId;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:Ljava/lang/String;
    .annotation runtime Lcom/facebook/work/config/community/WorkCommunityName;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/BkO;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/BkU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:Lcom/facebook/events/create/EventCompositionModel;

.field private V:Lcom/facebook/events/model/Event;

.field private W:Landroid/widget/FrameLayout;

.field private X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

.field private Y:Landroid/widget/FrameLayout;

.field private Z:Lcom/facebook/events/create/ui/EventNameEditText;

.field private aA:Z

.field private aB:Z

.field private aC:Z

.field private aD:LX/Bkm;

.field public final aE:Landroid/os/Handler;

.field public final aF:Ljava/lang/Runnable;

.field private final aG:LX/BjB;

.field private final aH:LX/BjC;

.field private final aI:LX/BjE;

.field private final aJ:Landroid/view/View$OnClickListener;

.field private final aK:Landroid/view/View$OnClickListener;

.field private final aL:LX/Bis;

.field private final aM:LX/BjJ;

.field private final aN:LX/9el;

.field private final aO:LX/Biv;

.field private aa:Lcom/facebook/events/create/ui/EventHostSelector;

.field private ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

.field private ac:Landroid/widget/FrameLayout;

.field private ad:Lcom/facebook/events/create/ui/EventDescriptionText;

.field private ae:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

.field public af:Lcom/facebook/events/create/ui/ThemeSuggestifier;

.field public ag:Lcom/facebook/events/create/ui/CoverPhotoSelector;

.field public ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

.field private ai:Lcom/facebook/events/create/ui/CohostsSelector;

.field private aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

.field private ak:Lcom/facebook/events/create/ui/EventCategorySelector;

.field private al:Landroid/widget/FrameLayout;

.field private am:Lcom/facebook/widget/text/BetterEditTextView;

.field private an:Landroid/widget/LinearLayout;

.field private ao:Lcom/facebook/widget/text/BetterTextView;

.field private ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

.field private aq:Lcom/facebook/events/common/EventAnalyticsParams;

.field private ar:Lcom/facebook/events/common/ActionMechanism;

.field private as:Ljava/lang/Long;

.field private at:Ljava/lang/String;

.field private au:Ljava/lang/String;

.field private av:J

.field public aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

.field private ax:Z

.field public ay:Z

.field private az:Z

.field public p:LX/BiW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Bid;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Bij;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/Bn1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/Bih;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/Bjk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0So;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1811787
    const-class v0, Lcom/facebook/events/create/EventCreationNikumanActivity;

    sput-object v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->T:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1811775
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1811776
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aE:Landroid/os/Handler;

    .line 1811777
    new-instance v0, Lcom/facebook/events/create/EventCreationNikumanActivity$1;

    invoke-direct {v0, p0}, Lcom/facebook/events/create/EventCreationNikumanActivity$1;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aF:Ljava/lang/Runnable;

    .line 1811778
    new-instance v0, LX/BjB;

    invoke-direct {v0, p0}, LX/BjB;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aG:LX/BjB;

    .line 1811779
    new-instance v0, LX/BjD;

    invoke-direct {v0, p0}, LX/BjD;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aH:LX/BjC;

    .line 1811780
    new-instance v0, LX/BjF;

    invoke-direct {v0, p0}, LX/BjF;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aI:LX/BjE;

    .line 1811781
    new-instance v0, LX/BjG;

    invoke-direct {v0, p0}, LX/BjG;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aJ:Landroid/view/View$OnClickListener;

    .line 1811782
    new-instance v0, LX/BjH;

    invoke-direct {v0, p0}, LX/BjH;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aK:Landroid/view/View$OnClickListener;

    .line 1811783
    new-instance v0, LX/BjI;

    invoke-direct {v0, p0}, LX/BjI;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aL:LX/Bis;

    .line 1811784
    new-instance v0, LX/BjJ;

    invoke-direct {v0, p0}, LX/BjJ;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aM:LX/BjJ;

    .line 1811785
    new-instance v0, LX/BjK;

    invoke-direct {v0, p0}, LX/BjK;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aN:LX/9el;

    .line 1811786
    new-instance v0, LX/Biw;

    invoke-direct {v0, p0}, LX/Biw;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aO:LX/Biv;

    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    .line 1811773
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    const v0, 0x7f0d0de1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckedTextView;

    const v1, 0x7f0d0dcf

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v2, v3, v0, v1}, LX/Bih;->a(Lcom/facebook/events/create/EventCompositionModel;Lcom/facebook/resources/ui/FbCheckedTextView;Lcom/facebook/resources/ui/FbCheckedTextView;)V

    .line 1811774
    return-void
.end method

.method private B()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 1811760
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aa:Lcom/facebook/events/create/ui/EventHostSelector;

    if-eqz v0, :cond_0

    .line 1811761
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aa:Lcom/facebook/events/create/ui/EventHostSelector;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventHostSelector;->setVisibility(I)V

    .line 1811762
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ak:Lcom/facebook/events/create/ui/EventCategorySelector;

    if-eqz v0, :cond_1

    .line 1811763
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ak:Lcom/facebook/events/create/ui/EventCategorySelector;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventCategorySelector;->setVisibility(I)V

    .line 1811764
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->al:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_2

    .line 1811765
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->al:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1811766
    :cond_2
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ax:Z

    if-eqz v0, :cond_3

    .line 1811767
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811768
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v0, v1

    .line 1811769
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a()V

    .line 1811770
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Z)V

    .line 1811771
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Bih;->a(Z)V

    .line 1811772
    return-void
.end method

.method private C()V
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 1811743
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1811744
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1811745
    const-class v2, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {v1, v0, v3, v2}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1811746
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1811747
    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 1811748
    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 1811749
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1811750
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811751
    iget-object v4, v2, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v2, v4

    .line 1811752
    iput-object v0, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    .line 1811753
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1811754
    iput-object v0, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    .line 1811755
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-virtual {v0, v3}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Z)V

    .line 1811756
    iput-boolean v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ax:Z

    .line 1811757
    :cond_0
    :goto_1
    return-void

    .line 1811758
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 1811759
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ax:Z

    goto :goto_1
.end method

.method private D()V
    .locals 2

    .prologue
    .line 1811739
    const v0, 0x7f0d0ddb

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ac:Landroid/widget/FrameLayout;

    .line 1811740
    const v0, 0x7f0d0dc9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventDescriptionText;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ad:Lcom/facebook/events/create/ui/EventDescriptionText;

    .line 1811741
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ad:Lcom/facebook/events/create/ui/EventDescriptionText;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventDescriptionText;->a(Lcom/facebook/events/create/EventCompositionModel;)V

    .line 1811742
    return-void
.end method

.method private E()V
    .locals 6

    .prologue
    .line 1811733
    const v0, 0x7f0d0dc8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/location/LocationNikumanPicker;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    .line 1811734
    const v0, 0x7f020966

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(ILandroid/widget/TextView;)V

    .line 1811735
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811736
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    move-object v1, v2

    .line 1811737
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->B:LX/0Or;

    const/16 v4, 0x66

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->F()Z

    move-result v5

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a(Lcom/facebook/events/ui/location/EventLocationModel;LX/0Or;Landroid/app/Activity;IZ)V

    .line 1811738
    return-void
.end method

.method private F()Z
    .locals 2

    .prologue
    .line 1811657
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()Z
    .locals 2

    .prologue
    .line 1811731
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    sget-object v1, LX/01T;->GROUPS:LX/01T;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private H()Z
    .locals 1

    .prologue
    .line 1811730
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->at:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private I()V
    .locals 2

    .prologue
    .line 1811725
    const v0, 0x7f0d0dc5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ae:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    .line 1811726
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ae:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811727
    iget-object p0, v1, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    move-object v1, p0

    .line 1811728
    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/EventTimeModel;)V

    .line 1811729
    return-void
.end method

.method private J()V
    .locals 2

    .prologue
    .line 1811718
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    if-eqz v0, :cond_0

    .line 1811719
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    new-instance v1, LX/Bj4;

    invoke-direct {v1, p0}, LX/Bj4;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    .line 1811720
    iput-object v1, v0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a:LX/Bj3;

    .line 1811721
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    if-eqz v0, :cond_1

    .line 1811722
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    new-instance v1, LX/Bj6;

    invoke-direct {v1, p0}, LX/Bj6;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    .line 1811723
    iput-object v1, v0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a:LX/Bj5;

    .line 1811724
    :cond_1
    return-void
.end method

.method private K()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1811711
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    sget-object v3, LX/01T;->FB4A:LX/01T;

    if-ne v2, v3, :cond_2

    .line 1811712
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811713
    iget-object v3, v2, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v2, v3

    .line 1811714
    sget-object v3, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne v2, v3, :cond_1

    .line 1811715
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 1811716
    goto :goto_0

    .line 1811717
    :cond_2
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static L(Lcom/facebook/events/create/EventCreationNikumanActivity;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1811690
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811691
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1811692
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1811693
    const v2, 0x7f082174

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1811694
    :goto_0
    return v0

    .line 1811695
    :cond_0
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811696
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    move-object v2, v3

    .line 1811697
    invoke-virtual {v2}, Lcom/facebook/events/ui/date/EventTimeModel;->e()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1811698
    const v2, 0x7f082158

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1811699
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->K()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1811700
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1811701
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 1811702
    invoke-static {v2}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1811703
    const v2, 0x7f082156

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 1811704
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->w()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1811705
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811706
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    move-object v2, v3

    .line 1811707
    iget-object v3, v2, Lcom/facebook/events/create/ui/EventCategoryModel;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1811708
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1811709
    const v2, 0x7f082157

    invoke-static {p0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1811710
    goto :goto_0
.end method

.method public static M(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 9

    .prologue
    .line 1811668
    invoke-static {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->L(Lcom/facebook/events/create/EventCreationNikumanActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1811669
    :goto_0
    return-void

    .line 1811670
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->p:LX/BiW;

    .line 1811671
    iget-object v1, v0, LX/BiW;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v2, v0, LX/BiW;->a:LX/0Yj;

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 1811672
    new-instance v2, LX/4EM;

    invoke-direct {v2}, LX/4EM;-><init>()V

    .line 1811673
    invoke-direct {p0, v2}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(LX/4EM;)V

    .line 1811674
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->K()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1811675
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1811676
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 1811677
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1811678
    const-string v1, "event_buy_ticket_url"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811679
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->w()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1811680
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811681
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    move-object v0, v1

    .line 1811682
    iget-object v1, v0, Lcom/facebook/events/create/ui/EventCategoryModel;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1811683
    const-string v1, "category_name"

    invoke-virtual {v2, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811684
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4EM;->k(Ljava/lang/String;)LX/4EM;

    .line 1811685
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811686
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v5, v1

    .line 1811687
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->q:LX/Bid;

    invoke-interface {p0}, LX/0ew;->iC_()LX/0gc;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->N()LX/Bj7;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-virtual {v4}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->getHandler()Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->k()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1811688
    iget-object v6, v5, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    move-object v5, v6

    .line 1811689
    :goto_1
    iget-object v6, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v7, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ar:Lcom/facebook/events/common/ActionMechanism;

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->P()LX/0oG;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/Bid;->a(LX/0gc;LX/4EM;LX/Bj7;Landroid/os/Handler;Landroid/net/Uri;Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;LX/0oG;)V

    goto/16 :goto_0

    :cond_4
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private N()LX/Bj7;
    .locals 1

    .prologue
    .line 1811732
    new-instance v0, LX/Bj7;

    invoke-direct {v0, p0}, LX/Bj7;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    return-object v0
.end method

.method private O()LX/Bj9;
    .locals 3

    .prologue
    .line 1811663
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811664
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811665
    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Lcom/facebook/events/model/PrivacyType;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->C:LX/0ad;

    sget-short v1, LX/Bjt;->e:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1811666
    :cond_0
    const/4 v0, 0x0

    .line 1811667
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, LX/Bj9;

    invoke-direct {v0, p0}, LX/Bj9;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    goto :goto_0
.end method

.method private P()LX/0oG;
    .locals 19

    .prologue
    .line 1811658
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->getPickedLocation()Lcom/facebook/events/ui/location/EventLocationModel;

    move-result-object v2

    .line 1811659
    invoke-virtual {v2}, Lcom/facebook/events/ui/location/EventLocationModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/events/ui/location/EventLocationModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v13, 0x1

    .line 1811660
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->w:LX/1nQ;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->av:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-nez v2, :cond_2

    const-wide/16 v4, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v2}, Lcom/facebook/events/create/ui/EventNameEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v2}, Lcom/facebook/events/create/ui/EventNameEditText;->getLongestEverMaxLength()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ad:Lcom/facebook/events/create/ui/EventDescriptionText;

    invoke-virtual {v2}, Lcom/facebook/events/create/ui/EventDescriptionText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ae:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a()Z

    move-result v9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    invoke-virtual {v2}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    const/4 v10, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    invoke-virtual {v2}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v11, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a()Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ai:Lcom/facebook/events/create/ui/CohostsSelector;

    invoke-virtual {v2}, Lcom/facebook/events/create/ui/CohostsSelector;->a()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ag:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    invoke-virtual {v2}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    invoke-virtual {v2}, LX/BkU;->a()Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    invoke-virtual {v2}, LX/BkU;->b()Z

    move-result v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    invoke-virtual {v2}, LX/BkU;->c()Z

    move-result v18

    invoke-virtual/range {v3 .. v18}, LX/1nQ;->a(JIIIZZZZZZZZZZ)LX/0oG;

    move-result-object v2

    return-object v2

    .line 1811661
    :cond_1
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 1811662
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->z:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->av:J

    sub-long/2addr v4, v6

    goto/16 :goto_1

    :cond_3
    const/4 v10, 0x0

    goto :goto_2

    :cond_4
    const/4 v11, 0x0

    goto :goto_3
.end method

.method public static Q(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 3

    .prologue
    .line 1811853
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1811854
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v1}, Lcom/facebook/events/create/ui/EventNameEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1811855
    return-void
.end method

.method private R()Lcom/facebook/events/common/ActionMechanism;
    .locals 1

    .prologue
    .line 1812018
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->V:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    .line 1812019
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->COPY_EVENT:Lcom/facebook/events/common/ActionMechanism;

    .line 1812020
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->S()Lcom/facebook/events/common/ActionMechanism;

    move-result-object v0

    goto :goto_0
.end method

.method private S()Lcom/facebook/events/common/ActionMechanism;
    .locals 2

    .prologue
    .line 1812013
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1812014
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812015
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_0

    .line 1812016
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->PAGE_CREATE_DIALOG:Lcom/facebook/events/common/ActionMechanism;

    .line 1812017
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/events/common/ActionMechanism;->USER_CREATE_DIALOG:Lcom/facebook/events/common/ActionMechanism;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;
    .locals 15

    .prologue
    .line 1812010
    if-nez p1, :cond_0

    .line 1812011
    move-object/from16 v0, p3

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;

    move-result-object v4

    .line 1812012
    :goto_0
    return-object v4

    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/model/Event;->z()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    const/4 v10, 0x0

    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/model/Event;->u()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/model/Event;->v()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/model/Event;->x()Ljava/lang/String;

    move-result-object v14

    move-object v4, p0

    move-object/from16 v5, p4

    move-object/from16 v6, p3

    move-object/from16 v7, p5

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    invoke-static/range {v4 .. v14}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/model/Event;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    goto :goto_0

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/events/model/Event;->z()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Long;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 1812009
    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-static/range {v0 .. v7}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/Long;)Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 1812008
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move-object v5, v2

    move-object v6, p3

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    invoke-static/range {v0 .. v10}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/model/Event;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/model/Event;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1811988
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1811989
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1811990
    const-string v1, "extras_event_action_mechanism"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1811991
    if-eqz p4, :cond_0

    .line 1811992
    const-string v1, "extra_event_model"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1811993
    :cond_0
    if-eqz p5, :cond_1

    .line 1811994
    const-string v1, "extra_key_event_ticket_url"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1811995
    :cond_1
    if-eqz p2, :cond_2

    .line 1811996
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1811997
    :cond_2
    if-eqz p6, :cond_3

    .line 1811998
    const-string v1, "extra_page_event_host_id"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1811999
    :cond_3
    invoke-static {p7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1812000
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1812001
    :cond_4
    invoke-static {p8}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1812002
    const-string v1, "extra_group_name"

    invoke-virtual {v0, v1, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1812003
    :cond_5
    if-eqz p9, :cond_6

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-eq p9, v1, :cond_6

    .line 1812004
    const-string v1, "extra_group_visibility"

    invoke-virtual {v0, v1, p9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1812005
    :cond_6
    invoke-static {p10}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1812006
    const-string v1, "extra_parent_group_name"

    invoke-virtual {v0, v1, p10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1812007
    :cond_7
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;
    .locals 11

    .prologue
    .line 1811987
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    move-object/from16 v10, p7

    invoke-static/range {v0 .. v10}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Long;Lcom/facebook/events/common/ActionMechanism;Lcom/facebook/events/model/Event;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a(ILandroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1811984
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->R:LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, -0x423e37

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1811985
    invoke-virtual {p2, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1811986
    return-void
.end method

.method private a(LX/4EM;)V
    .locals 10

    .prologue
    const-wide/16 v6, 0x0

    .line 1811856
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    .line 1811857
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1811858
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->R()Lcom/facebook/events/common/ActionMechanism;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1811859
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 1811860
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1811861
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "events_creation_prompt_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1811862
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1811863
    const-string v2, "event_promotion_id"

    invoke-virtual {p1, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811864
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    invoke-virtual {v1}, LX/Bn1;->d()Ljava/lang/String;

    move-result-object v1

    .line 1811865
    new-instance v2, LX/4EL;

    invoke-direct {v2}, LX/4EL;-><init>()V

    .line 1811866
    invoke-virtual {v2, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1811867
    const-string v0, "context"

    invoke-virtual {p1, v0, v2}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1811868
    move-object v2, p1

    .line 1811869
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->as:Ljava/lang/Long;

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->H:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1811870
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1811871
    :goto_1
    const-string v3, "actor_id"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811872
    move-object v0, v2

    .line 1811873
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811874
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1811875
    const-string v3, "name"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811876
    move-object v0, v0

    .line 1811877
    const-string v2, "event_visibility"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811878
    move-object v0, v0

    .line 1811879
    const-string v2, "PRIVACY_LOCKED"

    .line 1811880
    const-string v3, "privacy_update_policy"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811881
    move-object v2, v0

    .line 1811882
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811883
    iget-object v3, v0, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    move-object v0, v3

    .line 1811884
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "ONLY_ADMINS"

    .line 1811885
    :goto_2
    const-string v3, "can_post_policy"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811886
    move-object v0, v2

    .line 1811887
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811888
    iget-boolean v3, v2, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    move v2, v3

    .line 1811889
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 1811890
    const-string v3, "post_approval_required"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1811891
    move-object v0, v0

    .line 1811892
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811893
    iget-boolean v3, v2, LX/Bn1;->f:Z

    move v2, v3

    .line 1811894
    invoke-static {v2}, Lcom/facebook/events/create/EventCreationNikumanActivity;->e(Z)Ljava/lang/String;

    move-result-object v2

    .line 1811895
    const-string v3, "invite_policy"

    invoke-virtual {v0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811896
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811897
    iget-object v2, v0, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    move-object v0, v2

    .line 1811898
    invoke-virtual {v0}, Lcom/facebook/events/ui/date/EventTimeModel;->d()Ljava/sql/Date;

    move-result-object v2

    .line 1811899
    iget-object v3, v0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v3, v3

    .line 1811900
    iget-boolean v4, v0, Lcom/facebook/events/ui/date/EventTimeModel;->b:Z

    move v4, v4

    .line 1811901
    invoke-static {v2, v3, v4}, LX/Bm4;->a(Ljava/util/Date;Ljava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v2

    .line 1811902
    const-string v3, "start_time"

    invoke-virtual {p1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811903
    invoke-virtual {v0}, Lcom/facebook/events/ui/date/EventTimeModel;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1811904
    invoke-virtual {v0}, Lcom/facebook/events/ui/date/EventTimeModel;->h()Ljava/sql/Date;

    move-result-object v2

    .line 1811905
    iget-object v3, v0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v3, v3

    .line 1811906
    invoke-static {v2, v3}, LX/Bm4;->a(Ljava/util/Date;Ljava/util/TimeZone;)Ljava/lang/String;

    move-result-object v2

    .line 1811907
    const-string v3, "end_time"

    invoke-virtual {p1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811908
    :cond_1
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v2}, Lcom/facebook/events/create/EventCompositionModel;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1811909
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811910
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1811911
    const-string v3, "description"

    invoke-virtual {p1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811912
    :cond_2
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811913
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    move-object v2, v3

    .line 1811914
    iget-wide v8, v2, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    move-wide v4, v8

    .line 1811915
    cmp-long v3, v4, v6

    if-lez v3, :cond_c

    .line 1811916
    iget-wide v8, v2, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    move-wide v2, v8

    .line 1811917
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    .line 1811918
    const-string v3, "location_id"

    invoke-virtual {p1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811919
    :cond_3
    :goto_3
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v2}, Lcom/facebook/events/create/EventCompositionModel;->o()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1811920
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811921
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    move-object v2, v3

    .line 1811922
    iget-object v3, v2, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    move-object v2, v3

    .line 1811923
    const-string v3, "add_host_ids"

    invoke-virtual {p1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1811924
    :cond_4
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811925
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v2, v3

    .line 1811926
    invoke-virtual {v2}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->j()Z

    move-result v2

    if-eqz v2, :cond_d

    .line 1811927
    new-instance v2, LX/4Dr;

    invoke-direct {v2}, LX/4Dr;-><init>()V

    .line 1811928
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811929
    iget-object v4, v3, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v3, v4

    .line 1811930
    iget-object v4, v3, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    move-object v3, v4

    .line 1811931
    invoke-virtual {v2, v3}, LX/4Dr;->a(Ljava/lang/String;)LX/4Dr;

    .line 1811932
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811933
    iget-object v4, v3, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v3, v4

    .line 1811934
    invoke-virtual {v3}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4Dr;->b(Ljava/lang/String;)LX/4Dr;

    .line 1811935
    invoke-virtual {p1, v2}, LX/4EM;->a(LX/4Dr;)LX/4EM;

    .line 1811936
    :cond_5
    :goto_4
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->at:Ljava/lang/String;

    if-eqz v2, :cond_e

    .line 1811937
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->at:Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/4EM;->k(Ljava/lang/String;)LX/4EM;

    .line 1811938
    :cond_6
    :goto_5
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811939
    iget-object v2, v1, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v1, v2

    .line 1811940
    invoke-direct {p0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Lcom/facebook/events/model/PrivacyType;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1811941
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811942
    iget-boolean v2, v1, Lcom/facebook/events/create/EventCompositionModel;->p:Z

    move v1, v2

    .line 1811943
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1811944
    const-string v2, "save_as_draft"

    invoke-virtual {p1, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1811945
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811946
    iget-boolean v2, v1, Lcom/facebook/events/create/EventCompositionModel;->p:Z

    move v1, v2

    .line 1811947
    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811948
    iget-wide v8, v1, Lcom/facebook/events/create/EventCompositionModel;->q:J

    move-wide v2, v8

    .line 1811949
    cmp-long v1, v2, v6

    if-lez v1, :cond_7

    .line 1811950
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811951
    iget-wide v8, v1, Lcom/facebook/events/create/EventCompositionModel;->q:J

    move-wide v2, v8

    .line 1811952
    iget-object v1, v0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v0, v1

    .line 1811953
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    const/4 v4, 0x0

    invoke-static {v1, v0, v4}, LX/Bm4;->a(Ljava/util/Date;Ljava/util/TimeZone;Z)Ljava/lang/String;

    move-result-object v1

    move-object v0, v1

    .line 1811954
    const-string v1, "scheduled_publish_time"

    invoke-virtual {p1, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811955
    :cond_7
    return-void

    .line 1811956
    :cond_8
    new-instance v1, LX/4EG;

    invoke-direct {v1}, LX/4EG;-><init>()V

    .line 1811957
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1811958
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ar:Lcom/facebook/events/common/ActionMechanism;

    if-eqz v2, :cond_9

    .line 1811959
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ar:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v2}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1811960
    :cond_9
    invoke-static {v1, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    goto/16 :goto_0

    .line 1811961
    :cond_a
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->as:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_b
    const-string v0, "ALL"

    goto/16 :goto_2

    .line 1811962
    :cond_c
    iget-object v3, v2, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v3, v3

    .line 1811963
    if-eqz v3, :cond_3

    .line 1811964
    iget-object v3, v2, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1811965
    const-string v3, "location_name"

    invoke-virtual {p1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811966
    goto/16 :goto_3

    .line 1811967
    :cond_d
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811968
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v2, v3

    .line 1811969
    invoke-virtual {v2}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1811970
    new-instance v2, LX/4Dr;

    invoke-direct {v2}, LX/4Dr;-><init>()V

    .line 1811971
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811972
    iget-object v4, v3, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v3, v4

    .line 1811973
    iget-object v4, v3, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    move-object v3, v4

    .line 1811974
    invoke-virtual {v2, v3}, LX/4Dr;->c(Ljava/lang/String;)LX/4Dr;

    .line 1811975
    invoke-virtual {p1, v2}, LX/4EM;->a(LX/4Dr;)LX/4EM;

    goto/16 :goto_4

    .line 1811976
    :cond_e
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->H:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1811977
    iget-boolean v3, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v2, v3

    .line 1811978
    if-eqz v2, :cond_f

    .line 1811979
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->H:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1811980
    iget-object v2, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1811981
    invoke-virtual {p1, v1}, LX/4EM;->k(Ljava/lang/String;)LX/4EM;

    goto/16 :goto_5

    .line 1811982
    :cond_f
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->D:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "COMMUNITY"

    if-ne v1, v2, :cond_6

    .line 1811983
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->L:Ljava/lang/String;

    invoke-virtual {p1, v1}, LX/4EM;->k(Ljava/lang/String;)LX/4EM;

    goto/16 :goto_5
.end method

.method private static a(Lcom/facebook/events/create/EventCreationNikumanActivity;LX/BiW;LX/Bid;LX/Bij;LX/6RZ;LX/Bl6;LX/Bn1;LX/Bih;LX/1nQ;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/Bjk;LX/0So;LX/01T;LX/0Or;LX/0ad;Ljava/lang/Boolean;LX/Bjw;LX/Blh;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/Bjs;LX/0Or;LX/11u;Ljava/lang/String;Ljava/lang/String;LX/0hB;LX/0tX;LX/1Ck;LX/BkO;LX/0wM;LX/BkU;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/create/EventCreationNikumanActivity;",
            "LX/BiW;",
            "LX/Bid;",
            "LX/Bij;",
            "LX/6RZ;",
            "LX/Bl6;",
            "LX/Bn1;",
            "LX/Bih;",
            "LX/1nQ;",
            "Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;",
            "LX/Bjk;",
            "LX/0So;",
            "LX/01T;",
            "LX/0Or",
            "<",
            "LX/Bmo;",
            ">;",
            "LX/0ad;",
            "Ljava/lang/Boolean;",
            "Lcom/facebook/events/create/navigation/EventCreationNavHandler;",
            "LX/Blh;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            "LX/Bjs;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/11u;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0hB;",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/BkO;",
            "LX/0wM;",
            "LX/BkU;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1811804
    iput-object p1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->p:LX/BiW;

    iput-object p2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->q:LX/Bid;

    iput-object p3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->r:LX/Bij;

    iput-object p4, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->s:LX/6RZ;

    iput-object p5, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->t:LX/Bl6;

    iput-object p6, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    iput-object p7, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    iput-object p8, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->w:LX/1nQ;

    iput-object p9, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->x:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    iput-object p10, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->y:LX/Bjk;

    iput-object p11, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->z:LX/0So;

    iput-object p12, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    iput-object p13, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->B:LX/0Or;

    iput-object p14, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->C:LX/0ad;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->D:Ljava/lang/Boolean;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->E:LX/Bjw;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->F:LX/Blh;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->G:LX/0Ot;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->H:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->I:LX/Bjs;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->J:LX/0Or;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->K:LX/11u;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->L:Ljava/lang/String;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->M:Ljava/lang/String;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->N:LX/0hB;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->O:LX/0tX;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->P:LX/1Ck;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Q:LX/BkO;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->R:LX/0wM;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 33

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v32

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/events/create/EventCreationNikumanActivity;

    invoke-static/range {v32 .. v32}, LX/BiW;->a(LX/0QB;)LX/BiW;

    move-result-object v3

    check-cast v3, LX/BiW;

    invoke-static/range {v32 .. v32}, LX/Bid;->a(LX/0QB;)LX/Bid;

    move-result-object v4

    check-cast v4, LX/Bid;

    invoke-static/range {v32 .. v32}, LX/Bij;->a(LX/0QB;)LX/Bij;

    move-result-object v5

    check-cast v5, LX/Bij;

    invoke-static/range {v32 .. v32}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v6

    check-cast v6, LX/6RZ;

    invoke-static/range {v32 .. v32}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v7

    check-cast v7, LX/Bl6;

    invoke-static/range {v32 .. v32}, LX/Bn1;->a(LX/0QB;)LX/Bn1;

    move-result-object v8

    check-cast v8, LX/Bn1;

    invoke-static/range {v32 .. v32}, LX/Bih;->a(LX/0QB;)LX/Bih;

    move-result-object v9

    check-cast v9, LX/Bih;

    invoke-static/range {v32 .. v32}, LX/1nQ;->a(LX/0QB;)LX/1nQ;

    move-result-object v10

    check-cast v10, LX/1nQ;

    invoke-static/range {v32 .. v32}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->a(LX/0QB;)Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    move-result-object v11

    check-cast v11, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    invoke-static/range {v32 .. v32}, LX/Bjk;->a(LX/0QB;)LX/Bjk;

    move-result-object v12

    check-cast v12, LX/Bjk;

    invoke-static/range {v32 .. v32}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v13

    check-cast v13, LX/0So;

    invoke-static/range {v32 .. v32}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v14

    check-cast v14, LX/01T;

    const/16 v15, 0x1bb3

    move-object/from16 v0, v32

    invoke-static {v0, v15}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v15

    invoke-static/range {v32 .. v32}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v16

    check-cast v16, LX/0ad;

    invoke-static/range {v32 .. v32}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v17

    check-cast v17, Ljava/lang/Boolean;

    invoke-static/range {v32 .. v32}, LX/Bjw;->a(LX/0QB;)LX/Bjw;

    move-result-object v18

    check-cast v18, LX/Bjw;

    invoke-static/range {v32 .. v32}, LX/Blh;->a(LX/0QB;)LX/Blh;

    move-result-object v19

    check-cast v19, LX/Blh;

    const/16 v20, 0x2eb

    move-object/from16 v0, v32

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-static/range {v32 .. v32}, LX/0eQ;->a(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v21

    check-cast v21, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static/range {v32 .. v32}, LX/Bjs;->a(LX/0QB;)LX/Bjs;

    move-result-object v22

    check-cast v22, LX/Bjs;

    const/16 v23, 0x12cb

    move-object/from16 v0, v32

    move/from16 v1, v23

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v23

    invoke-static/range {v32 .. v32}, LX/11u;->a(LX/0QB;)LX/11u;

    move-result-object v24

    check-cast v24, LX/11u;

    invoke-static/range {v32 .. v32}, LX/4ox;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    invoke-static/range {v32 .. v32}, LX/4oy;->a(LX/0QB;)Ljava/lang/String;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    invoke-static/range {v32 .. v32}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v27

    check-cast v27, LX/0hB;

    invoke-static/range {v32 .. v32}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v28

    check-cast v28, LX/0tX;

    invoke-static/range {v32 .. v32}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v29

    check-cast v29, LX/1Ck;

    invoke-static/range {v32 .. v32}, LX/BkO;->a(LX/0QB;)LX/BkO;

    move-result-object v30

    check-cast v30, LX/BkO;

    invoke-static/range {v32 .. v32}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v31

    check-cast v31, LX/0wM;

    invoke-static/range {v32 .. v32}, LX/BkU;->a(LX/0QB;)LX/BkU;

    move-result-object v32

    check-cast v32, LX/BkU;

    invoke-static/range {v2 .. v32}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Lcom/facebook/events/create/EventCreationNikumanActivity;LX/BiW;LX/Bid;LX/Bij;LX/6RZ;LX/Bl6;LX/Bn1;LX/Bih;LX/1nQ;Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;LX/Bjk;LX/0So;LX/01T;LX/0Or;LX/0ad;Ljava/lang/Boolean;LX/Bjw;LX/Blh;LX/0Ot;Lcom/facebook/auth/viewercontext/ViewerContext;LX/Bjs;LX/0Or;LX/11u;Ljava/lang/String;Ljava/lang/String;LX/0hB;LX/0tX;LX/1Ck;LX/BkO;LX/0wM;LX/BkU;)V

    return-void
.end method

.method private a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 4

    .prologue
    .line 1811839
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->E:LX/Bjw;

    .line 1811840
    const v1, 0x7f0d00bc

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, LX/0h5;

    .line 1811841
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v2

    const v3, 0x7f0d01a2

    .line 1811842
    iput v3, v2, LX/108;->a:I

    .line 1811843
    move-object v2, v2

    .line 1811844
    iput-object p1, v2, LX/108;->g:Ljava/lang/String;

    .line 1811845
    move-object v2, v2

    .line 1811846
    const/4 v3, -0x2

    .line 1811847
    iput v3, v2, LX/108;->h:I

    .line 1811848
    move-object v2, v2

    .line 1811849
    invoke-virtual {v2}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v2

    .line 1811850
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1811851
    new-instance v2, LX/Bjv;

    invoke-direct {v2, v0, p2}, LX/Bjv;-><init>(LX/Bjw;Landroid/view/View$OnClickListener;)V

    invoke-interface {v1, v2}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1811852
    return-void
.end method

.method private a(Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;)Z
    .locals 2

    .prologue
    .line 1811376
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-nez v0, :cond_0

    if-nez p1, :cond_0

    .line 1811377
    const/4 v0, 0x1

    .line 1811378
    :goto_0
    return v0

    .line 1811379
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 1811380
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 1811381
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/events/model/PrivacyType;)Z
    .locals 2

    .prologue
    .line 1811838
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    if-eq p1, v0, :cond_0

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;J)V
    .locals 5

    .prologue
    .line 1811821
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_1

    .line 1811822
    sget-object v0, LX/0ax;->B:Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1811823
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {v0, p0, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 1811824
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->finish()V

    .line 1811825
    return-void

    .line 1811826
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    sget-object v1, LX/01T;->GROUPS:LX/01T;

    if-eq v0, v1, :cond_0

    .line 1811827
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->V:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_2

    .line 1811828
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->F:LX/Blh;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->V:Lcom/facebook/events/model/Event;

    .line 1811829
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1811830
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 1811831
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1811832
    iget-object v4, v0, LX/Blh;->d:LX/0id;

    sget-object p1, LX/Blh;->a:Ljava/lang/String;

    invoke-virtual {v4, p0, p1}, LX/0id;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 1811833
    const/4 v4, 0x0

    invoke-static {v0, v1, v3, v4}, LX/Blh;->a(LX/Blh;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;

    move-result-object v4

    .line 1811834
    const-string p1, "extra_original_event_id"

    invoke-virtual {v4, p1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1811835
    iget-object p1, v0, LX/Blh;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p1, v4, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1811836
    goto :goto_0

    .line 1811837
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->F:LX/Blh;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {v0, p0, v1, v2}, LX/Blh;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/events/common/EventActionContext;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/events/model/PrivacyType;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1811812
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->H()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1811813
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->I:LX/Bjs;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aL:LX/Bis;

    invoke-virtual {v0, v1, v2}, LX/Bjs;->a(Ljava/lang/String;LX/Bis;)V

    .line 1811814
    :cond_0
    :goto_0
    return-void

    .line 1811815
    :cond_1
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-nez v0, :cond_0

    .line 1811816
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->I:LX/Bjs;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aM:LX/BjJ;

    .line 1811817
    new-instance v2, LX/Bjq;

    invoke-direct {v2, v0, p2}, LX/Bjq;-><init>(LX/Bjs;Ljava/lang/String;)V

    .line 1811818
    new-instance v3, LX/Bjr;

    invoke-direct {v3, v0, v1}, LX/Bjr;-><init>(LX/Bjs;LX/BjJ;)V

    .line 1811819
    iget-object p0, v0, LX/Bjs;->d:LX/1Ck;

    const-string p1, "fetchSinglePage"

    invoke-virtual {p0, p1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1811820
    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1811810
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "extra_group_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_parent_group_name"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, LX/Bn1;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;)V

    .line 1811811
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1811805
    const v0, 0x7f0d0dc1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/CoverPhotoSelector;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ag:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    .line 1811806
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ag:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->setVisibility(I)V

    .line 1811807
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ag:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    new-instance v1, LX/Bix;

    invoke-direct {v1, p0}, LX/Bix;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1811808
    return-void

    .line 1811809
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/events/model/PrivacyType;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1811788
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->A:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    .line 1811789
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->l()V

    .line 1811790
    invoke-static {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->o$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    .line 1811791
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->t()V

    .line 1811792
    :cond_0
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne p1, v0, :cond_3

    .line 1811793
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->H()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1811794
    invoke-static {p0, v3}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811795
    :goto_0
    invoke-static {p0, v2}, Lcom/facebook/events/create/EventCreationNikumanActivity;->c$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811796
    :goto_1
    return-void

    .line 1811797
    :cond_2
    invoke-static {p0, v2}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811798
    invoke-static {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->z(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    goto :goto_0

    .line 1811799
    :cond_3
    invoke-static {p0, v2}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811800
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->B()V

    .line 1811801
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    if-eq p1, v0, :cond_4

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    if-ne p1, v0, :cond_5

    .line 1811802
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    invoke-virtual {v0, v3}, LX/Bih;->c(Z)V

    .line 1811803
    :cond_5
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ay:Z

    invoke-static {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->c$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1811187
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aJ:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p1, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1811188
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1811371
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Z)V

    .line 1811372
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aD:LX/Bkm;

    invoke-virtual {v0, p1}, LX/Bkm;->a(Z)V

    .line 1811373
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aD:LX/Bkm;

    invoke-virtual {v0, p1}, LX/Bkm;->b(Z)V

    .line 1811374
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ag:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(ZZ)V

    .line 1811375
    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1811358
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1811359
    :goto_0
    return-void

    .line 1811360
    :cond_0
    new-instance v0, LX/BnO;

    invoke-direct {v0}, LX/BnO;-><init>()V

    .line 1811361
    const-string v1, "theme_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1811362
    const-string v1, "theme_height"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1811363
    const-string v1, "theme_width"

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->N:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1811364
    new-instance v1, LX/BnO;

    invoke-direct {v1}, LX/BnO;-><init>()V

    move-object v1, v1

    .line 1811365
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1811366
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 1811367
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 1811368
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->O:LX/0tX;

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1811369
    new-instance v1, LX/Bj2;

    invoke-direct {v1, p0}, LX/Bj2;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    .line 1811370
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->P:LX/1Ck;

    const-string v3, "fetch_single_theme"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1811340
    if-eqz p1, :cond_3

    .line 1811341
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ac:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1811342
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ai:Lcom/facebook/events/create/ui/CohostsSelector;

    invoke-virtual {v0, v2}, Lcom/facebook/events/create/ui/CohostsSelector;->setVisibility(I)V

    .line 1811343
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aA:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setVisibility(I)V

    .line 1811344
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ae:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iget-boolean v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->az:Z

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v0, v2}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->setVisibility(I)V

    .line 1811345
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1811346
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->W:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1811347
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ao:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aA:Z

    if-eqz v0, :cond_2

    const v0, 0x7f08215d

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1811348
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 1811349
    goto :goto_0

    .line 1811350
    :cond_2
    const v0, 0x7f08215c

    goto :goto_1

    .line 1811351
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ac:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1811352
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->F()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->H()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1811353
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ai:Lcom/facebook/events/create/ui/CohostsSelector;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/CohostsSelector;->setVisibility(I)V

    .line 1811354
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setVisibility(I)V

    .line 1811355
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ae:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->setVisibility(I)V

    .line 1811356
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->an:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1811357
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->W:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1811299
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1811300
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    sget-object v2, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0, v2, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1811301
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    invoke-virtual {v0}, LX/Bn1;->b()Ljava/lang/String;

    .line 1811302
    return-void

    .line 1811303
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->G()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1811304
    const v0, 0x7f0d0def

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    .line 1811305
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    .line 1811306
    iput-boolean v2, v0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->e:Z

    .line 1811307
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->D:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1811308
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    .line 1811309
    iput-boolean v2, v0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->f:Z

    .line 1811310
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->M:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(ZLjava/lang/String;)V

    .line 1811311
    :cond_1
    const v0, 0x7f0d0dce

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    .line 1811312
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->D:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f082146

    :goto_1
    invoke-virtual {v3, v0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->setText(I)V

    .line 1811313
    const v0, 0x7f020851

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    invoke-direct {p0, v0, v3}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(ILandroid/widget/TextView;)V

    .line 1811314
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    iget-object v4, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    invoke-virtual {v0, v3, v4}, LX/Bn1;->a(Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;)V

    .line 1811315
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    const v0, 0x7f0d0dd3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;

    invoke-virtual {v3, v0}, LX/Bn1;->a(Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;)V

    .line 1811316
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "group_id"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "extra_group_name"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1811317
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "group_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->at:Ljava/lang/String;

    .line 1811318
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "extra_group_visibility"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1811319
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_group_visibility"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-static {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    goto/16 :goto_0

    .line 1811320
    :cond_3
    const v0, 0x7f082145

    goto :goto_1

    .line 1811321
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->at:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->e(Ljava/lang/String;)V

    .line 1811322
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->CLOSED:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-static {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    .line 1811323
    invoke-static {p0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    goto/16 :goto_0

    .line 1811324
    :cond_5
    if-eqz p1, :cond_8

    .line 1811325
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    const-string v0, "EVENT_PRIVACY_TYPE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/PrivacyType;->valueOf(Ljava/lang/String;)Lcom/facebook/events/model/PrivacyType;

    move-result-object v4

    const-string v0, "EVENT_CAN_GUEST_INVITE_FRIENDS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_2
    invoke-virtual {v3, v4, v0}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1811326
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    iget-boolean v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aC:Z

    if-nez v3, :cond_7

    :goto_3
    invoke-virtual {v0, v1}, LX/Bn1;->a(Z)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    .line 1811327
    goto :goto_2

    :cond_7
    move v1, v2

    .line 1811328
    goto :goto_3

    .line 1811329
    :cond_8
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aB:Z

    if-eqz v0, :cond_9

    .line 1811330
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    invoke-virtual {v0}, LX/Bn1;->a()V

    .line 1811331
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;)V

    goto/16 :goto_0

    .line 1811332
    :cond_9
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811333
    iget-object v3, v0, Lcom/facebook/events/create/EventCompositionModel;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v3

    .line 1811334
    if-nez v0, :cond_a

    .line 1811335
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    sget-object v2, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0, v2, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    goto/16 :goto_0

    .line 1811336
    :cond_a
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811337
    iget-object v4, v3, Lcom/facebook/events/create/EventCompositionModel;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v3, v4

    .line 1811338
    invoke-virtual {v0, v3, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1811339
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    iget-boolean v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aC:Z

    if-nez v3, :cond_b

    :goto_4
    invoke-virtual {v0, v1}, LX/Bn1;->a(Z)V

    goto/16 :goto_0

    :cond_b
    move v1, v2

    goto :goto_4
.end method

.method private static d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1811294
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1811295
    const/4 v0, 0x1

    .line 1811296
    :goto_0
    return v0

    .line 1811297
    :cond_0
    sget-object v0, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1811298
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    goto :goto_0
.end method

.method public static d$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1811286
    const v0, 0x7f0d0dd4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 1811287
    const v1, 0x7f0d0dd2

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 1811288
    if-eqz p1, :cond_0

    .line 1811289
    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1811290
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1811291
    :goto_0
    return-void

    .line 1811292
    :cond_0
    invoke-virtual {v1, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1811293
    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private static e(Z)Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/EventInvitePolicyEnum;
    .end annotation

    .prologue
    .line 1811285
    if-eqz p0, :cond_0

    const-string v0, "CAN_INVITE_FRIENDS"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "CANNOT_INVITE_FRIENDS"

    goto :goto_0
.end method

.method private e(Landroid/os/Bundle;)V
    .locals 12

    .prologue
    const-wide/16 v8, 0x0

    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 1811250
    const-string v0, "events_creation_story_cache_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1811251
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->x:Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;

    const-string v4, "events_creation_story_cache_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/facebook/notifications/provider/GraphQLNotificationsContentProviderHelper;->b(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1811252
    if-nez v0, :cond_1

    .line 1811253
    :cond_0
    :goto_0
    return-void

    .line 1811254
    :cond_1
    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1811255
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1811256
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->a()LX/0Px;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    .line 1811257
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v5, -0x55b63fa

    if-ne v4, v5, :cond_0

    .line 1811258
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aY()Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;

    move-result-object v5

    .line 1811259
    if-eqz v5, :cond_7

    .line 1811260
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->k()Ljava/lang/String;

    move-result-object v4

    .line 1811261
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1811262
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->j()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    .line 1811263
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->j()Lcom/facebook/graphql/model/GraphQLEventThemePhoto;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEventThemePhoto;->k()Ljava/lang/String;

    move-result-object v1

    .line 1811264
    :goto_1
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTemporalEventInfo;->a()J

    move-result-wide v2

    move-wide v10, v2

    move-object v2, v0

    move-object v3, v1

    move-wide v0, v10

    .line 1811265
    :goto_2
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 1811266
    iget-object v5, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811267
    iput-object v4, v5, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    .line 1811268
    :cond_2
    cmp-long v4, v0, v8

    if-ltz v4, :cond_3

    .line 1811269
    iget-object v4, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811270
    iget-object v5, v4, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    move-object v4, v5

    .line 1811271
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v0, v1, v6}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-virtual {v4, v0, v1, v8, v9}, Lcom/facebook/events/ui/date/EventTimeModel;->a(JJ)Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1811272
    :cond_3
    if-eqz v2, :cond_5

    .line 1811273
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811274
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v0, v1

    .line 1811275
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1811276
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    .line 1811277
    iput-object v3, v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    .line 1811278
    goto/16 :goto_0

    .line 1811279
    :cond_4
    const-string v0, "events_creation_prefill_title"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1811280
    const-string v0, "events_creation_prefill_start_time"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1811281
    const-string v0, "events_creation_prefill_theme_id"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1811282
    const-string v0, "events_creation_prefill_theme_url"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-wide v10, v2

    move-object v2, v0

    move-object v3, v4

    move-wide v0, v10

    move-object v4, v5

    goto :goto_2

    .line 1811283
    :cond_5
    if-eqz v3, :cond_0

    .line 1811284
    invoke-direct {p0, v3}, Lcom/facebook/events/create/EventCreationNikumanActivity;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_1

    :cond_7
    move-object v4, v1

    move-object v10, v1

    move-object v11, v1

    move-wide v0, v2

    move-object v2, v10

    move-object v3, v11

    goto :goto_2
.end method

.method private e(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1811240
    new-instance v0, LX/Bjx;

    invoke-direct {v0}, LX/Bjx;-><init>()V

    .line 1811241
    const-string v1, "group_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1811242
    new-instance v1, LX/Bjx;

    invoke-direct {v1}, LX/Bjx;-><init>()V

    move-object v1, v1

    .line 1811243
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 1811244
    iget-object v2, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v2

    .line 1811245
    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    .line 1811246
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->O:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 1811247
    new-instance v1, LX/BjA;

    invoke-direct {v1, p0}, LX/BjA;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    .line 1811248
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->P:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Fetch group visibility for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1811249
    return-void
.end method

.method public static f(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V
    .locals 20

    .prologue
    .line 1811234
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-virtual {v2}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->getPickedLocation()Lcom/facebook/events/ui/location/EventLocationModel;

    move-result-object v2

    .line 1811235
    invoke-virtual {v2}, Lcom/facebook/events/ui/location/EventLocationModel;->a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/events/ui/location/EventLocationModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v13, 0x1

    .line 1811236
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->w:LX/1nQ;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->av:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_2

    const-wide/16 v4, 0x0

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v3}, Lcom/facebook/events/create/ui/EventNameEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v3}, Lcom/facebook/events/create/ui/EventNameEditText;->getLongestEverMaxLength()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ad:Lcom/facebook/events/create/ui/EventDescriptionText;

    invoke-virtual {v3}, Lcom/facebook/events/create/ui/EventDescriptionText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ae:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-virtual {v3}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a()Z

    move-result v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aj:Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;

    invoke-virtual {v3}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v10, 0x1

    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->X:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    invoke-virtual {v3}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v11, 0x1

    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-virtual {v3}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a()Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ai:Lcom/facebook/events/create/ui/CohostsSelector;

    invoke-virtual {v3}, Lcom/facebook/events/create/ui/CohostsSelector;->a()Z

    move-result v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ag:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    invoke-virtual {v3}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    invoke-virtual {v3}, LX/BkU;->a()Z

    move-result v16

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    invoke-virtual {v3}, LX/BkU;->b()Z

    move-result v17

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    invoke-virtual {v3}, LX/BkU;->c()Z

    move-result v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->V:Lcom/facebook/events/model/Event;

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_4
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, LX/Bn1;->b(Z)Ljava/lang/String;

    move-result-object v19

    move/from16 v3, p1

    invoke-virtual/range {v2 .. v19}, LX/1nQ;->a(ZJIIIZZZZZZZZZZLjava/lang/String;)V

    .line 1811237
    return-void

    .line 1811238
    :cond_1
    const/4 v13, 0x0

    goto/16 :goto_0

    .line 1811239
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->z:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/facebook/events/create/EventCreationNikumanActivity;->av:J

    sub-long/2addr v4, v6

    goto/16 :goto_1

    :cond_3
    const/4 v10, 0x0

    goto :goto_2

    :cond_4
    const/4 v11, 0x0

    goto :goto_3

    :cond_5
    const/4 v3, 0x0

    goto :goto_4
.end method

.method private l()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1811216
    const v0, 0x7f0d0dd8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/ThemeSuggestifier;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->af:Lcom/facebook/events/create/ui/ThemeSuggestifier;

    .line 1811217
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->C:LX/0ad;

    sget-short v1, LX/Bjt;->h:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811218
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v0, v1

    .line 1811219
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->g()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811220
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811221
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811222
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811223
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811224
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811225
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_1

    .line 1811226
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->af:Lcom/facebook/events/create/ui/ThemeSuggestifier;

    invoke-virtual {v0, v2}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->setVisibility(I)V

    .line 1811227
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aI:LX/BjE;

    .line 1811228
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventNameEditText;->g:LX/BjE;

    .line 1811229
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->af:Lcom/facebook/events/create/ui/ThemeSuggestifier;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aG:LX/BjB;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->setOnThemeSelectedListener(LX/BjB;)V

    .line 1811230
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->af:Lcom/facebook/events/create/ui/ThemeSuggestifier;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aH:LX/BjC;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->setOnOptionSelectedListener(LX/BjC;)V

    .line 1811231
    :goto_0
    return-void

    .line 1811232
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->af:Lcom/facebook/events/create/ui/ThemeSuggestifier;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->setVisibility(I)V

    .line 1811233
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventNameEditText;->a()V

    goto :goto_0
.end method

.method private m()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1811210
    const v0, 0x7f0d0de0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/CohostsSelector;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ai:Lcom/facebook/events/create/ui/CohostsSelector;

    .line 1811211
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ai:Lcom/facebook/events/create/ui/CohostsSelector;

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811212
    iget-object v3, v0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    move-object v3, v3

    .line 1811213
    const/16 v4, 0x69

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->F()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->H()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v4, v0, v1}, Lcom/facebook/events/create/ui/CohostsSelector;->a(Lcom/facebook/events/create/ui/EventCohostsModel;IZZ)V

    .line 1811214
    return-void

    :cond_0
    move v0, v1

    .line 1811215
    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1811206
    const v0, 0x7f0d0dde

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->an:Landroid/widget/LinearLayout;

    .line 1811207
    const v0, 0x7f0d0ddf

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ao:Lcom/facebook/widget/text/BetterTextView;

    .line 1811208
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->an:Landroid/widget/LinearLayout;

    new-instance v1, LX/Biy;

    invoke-direct {v1, p0}, LX/Biy;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1811209
    return-void
.end method

.method public static synthetic o(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 0

    .prologue
    .line 1811205
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public static o$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 8

    .prologue
    .line 1811191
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811192
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811193
    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Lcom/facebook/events/model/PrivacyType;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->C:LX/0ad;

    sget-short v1, LX/Bjt;->f:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1811194
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 1811195
    :cond_0
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->p()V

    .line 1811196
    :goto_0
    return-void

    .line 1811197
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811198
    iget-boolean v1, v0, Lcom/facebook/events/create/EventCompositionModel;->p:Z

    move v0, v1

    .line 1811199
    if-eqz v0, :cond_3

    .line 1811200
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811201
    iget-wide v6, v0, Lcom/facebook/events/create/EventCompositionModel;->q:J

    move-wide v2, v6

    .line 1811202
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    const v0, 0x7f082163

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v0, 0x7f082165

    goto :goto_1

    .line 1811203
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082164

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1811204
    :cond_4
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082159

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 1811189
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08215a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/Biz;

    invoke-direct {v1, p0}, LX/Biz;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 1811190
    return-void
.end method

.method public static q(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 2

    .prologue
    .line 1811176
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    if-nez v0, :cond_0

    .line 1811177
    const v0, 0x7f0d0de4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1811178
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    .line 1811179
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811180
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->c:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811181
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    new-instance v1, LX/Bj1;

    invoke-direct {v1, p0}, LX/Bj1;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    .line 1811182
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->e:LX/Bj0;

    .line 1811183
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setVisibility(I)V

    .line 1811184
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->r()V

    .line 1811185
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->requestFocus()Z

    .line 1811186
    return-void
.end method

.method private r()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1811577
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventNameEditText;->setFocusable(Z)V

    .line 1811578
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ad:Lcom/facebook/events/create/ui/EventDescriptionText;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventDescriptionText;->setFocusable(Z)V

    .line 1811579
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    if-eqz v0, :cond_0

    .line 1811580
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setFocusable(Z)V

    .line 1811581
    :cond_0
    return-void
.end method

.method public static s(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1811649
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventNameEditText;->setFocusableInTouchMode(Z)V

    .line 1811650
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventNameEditText;->setFocusable(Z)V

    .line 1811651
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ad:Lcom/facebook/events/create/ui/EventDescriptionText;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventDescriptionText;->setFocusableInTouchMode(Z)V

    .line 1811652
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ad:Lcom/facebook/events/create/ui/EventDescriptionText;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventDescriptionText;->setFocusable(Z)V

    .line 1811653
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    if-eqz v0, :cond_0

    .line 1811654
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setFocusableInTouchMode(Z)V

    .line 1811655
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setFocusable(Z)V

    .line 1811656
    :cond_0
    return-void
.end method

.method private t()V
    .locals 8

    .prologue
    .line 1811631
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    if-nez v0, :cond_0

    .line 1811632
    :goto_0
    return-void

    .line 1811633
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811634
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811635
    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Lcom/facebook/events/model/PrivacyType;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1811636
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setVisibility(I)V

    .line 1811637
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811638
    iget-boolean v1, v0, Lcom/facebook/events/create/EventCompositionModel;->p:Z

    move v0, v1

    .line 1811639
    if-eqz v0, :cond_3

    .line 1811640
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811641
    iget-wide v6, v0, Lcom/facebook/events/create/EventCompositionModel;->q:J

    move-wide v0, v6

    .line 1811642
    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 1811643
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082162

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setSubtitleText(Ljava/lang/String;)V

    goto :goto_0

    .line 1811644
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->s:LX/6RZ;

    const/4 v1, 0x0

    new-instance v2, Ljava/util/Date;

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811645
    iget-wide v6, v3, Lcom/facebook/events/create/EventCompositionModel;->q:J

    move-wide v4, v6

    .line 1811646
    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1811647
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082167

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setSubtitleText(Ljava/lang/String;)V

    goto :goto_0

    .line 1811648
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082160

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setSubtitleText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private u()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1811613
    const v0, 0x7f0d0dd9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventHostSelector;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aa:Lcom/facebook/events/create/ui/EventHostSelector;

    .line 1811614
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aa:Lcom/facebook/events/create/ui/EventHostSelector;

    const/16 v3, 0x6c

    .line 1811615
    iput v3, v0, Lcom/facebook/events/create/ui/EventHostSelector;->f:I

    .line 1811616
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aa:Lcom/facebook/events/create/ui/EventHostSelector;

    invoke-virtual {v0, v2}, Lcom/facebook/events/create/ui/EventHostSelector;->setVisibility(I)V

    .line 1811617
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-eqz v0, :cond_1

    .line 1811618
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    .line 1811619
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->m()Ljava/lang/String;

    move-result-object v4

    .line 1811620
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->C()V

    .line 1811621
    iget-object v5, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aa:Lcom/facebook/events/create/ui/EventHostSelector;

    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aB:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v5, v3, v4, v1, v0}, Lcom/facebook/events/create/ui/EventHostSelector;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 1811622
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1811623
    goto :goto_0

    .line 1811624
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->H()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1811625
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->J:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1811626
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v3, v3

    .line 1811627
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->J:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    .line 1811628
    iput-boolean v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ax:Z

    .line 1811629
    iget-object v4, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aa:Lcom/facebook/events/create/ui/EventHostSelector;

    iget-boolean v5, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aB:Z

    if-nez v5, :cond_2

    :goto_2
    invoke-virtual {v4, v3, v0, v2, v1}, Lcom/facebook/events/create/ui/EventHostSelector;->a(Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    .line 1811630
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aa:Lcom/facebook/events/create/ui/EventHostSelector;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventHostSelector;->setVisibility(I)V

    goto :goto_1
.end method

.method private v()V
    .locals 5

    .prologue
    .line 1811607
    const v0, 0x7f0d0dda

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventCategorySelector;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ak:Lcom/facebook/events/create/ui/EventCategorySelector;

    .line 1811608
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1811609
    :goto_0
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ak:Lcom/facebook/events/create/ui/EventCategorySelector;

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_1
    const/16 v4, 0x6d

    invoke-virtual {v2, v3, v1, v0, v4}, Lcom/facebook/events/create/ui/EventCategorySelector;->a(Lcom/facebook/events/create/EventCompositionModel;Ljava/lang/String;ZI)V

    .line 1811610
    return-void

    .line 1811611
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1811612
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private w()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1811602
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-eqz v0, :cond_1

    .line 1811603
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 1811604
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 1811605
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 1811606
    invoke-virtual {v3, v0, v2}, LX/15i;->j(II)I

    move-result v0

    if-lez v0, :cond_2

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private x()V
    .locals 2

    .prologue
    .line 1811594
    const v0, 0x7f0d0ddc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->al:Landroid/widget/FrameLayout;

    .line 1811595
    const v0, 0x7f0d0ddd

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1811596
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-eqz v0, :cond_1

    .line 1811597
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->al:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1811598
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->au:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1811599
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->am:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->au:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1811600
    :cond_0
    :goto_0
    return-void

    .line 1811601
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->al:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private y()V
    .locals 2

    .prologue
    .line 1811590
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->H()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1811591
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;)V

    .line 1811592
    :goto_0
    return-void

    .line 1811593
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;)V

    goto :goto_0
.end method

.method public static z(Lcom/facebook/events/create/EventCreationNikumanActivity;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1811582
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->y()V

    .line 1811583
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->u()V

    .line 1811584
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->v()V

    .line 1811585
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->x()V

    .line 1811586
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->H()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1811587
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    invoke-virtual {v0, v1}, LX/Bih;->c(Z)V

    .line 1811588
    :goto_0
    return-void

    .line 1811589
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    invoke-virtual {v0, v1}, LX/Bih;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1811382
    const-string v0, "event_composer"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1811461
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1811462
    invoke-static {p0, p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1811463
    const v0, 0x7f0400c8

    const v1, 0x7f040030

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->overridePendingTransition(II)V

    .line 1811464
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->y:LX/Bjk;

    .line 1811465
    iget-object v1, v0, LX/Bjk;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v2, v0, LX/Bjk;->a:LX/0Yj;

    invoke-interface {v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 1811466
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v9

    .line 1811467
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_event_model"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->V:Lcom/facebook/events/model/Event;

    .line 1811468
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1811469
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->as:Ljava/lang/Long;

    .line 1811470
    :cond_0
    const v0, 0x7f0304b7

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->setContentView(I)V

    .line 1811471
    if-nez p1, :cond_9

    .line 1811472
    new-instance v0, Lcom/facebook/events/create/EventCompositionModel;

    invoke-direct {v0}, Lcom/facebook/events/create/EventCompositionModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811473
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/EventCompositionModel;->a()V

    .line 1811474
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_privacy_string"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1811475
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1811476
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 1811477
    :try_start_0
    invoke-static {v0}, Lcom/facebook/events/model/PrivacyType;->valueOf(Ljava/lang/String;)Lcom/facebook/events/model/PrivacyType;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1811478
    :goto_0
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_2

    .line 1811479
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811480
    iput-object v0, v1, Lcom/facebook/events/create/EventCompositionModel;->e:Lcom/facebook/events/model/PrivacyType;

    .line 1811481
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_2

    .line 1811482
    invoke-static {p0, v0, v6}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/events/model/PrivacyType;Ljava/lang/String;)V

    .line 1811483
    invoke-static {p0, v7}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811484
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->V:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_8

    .line 1811485
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->V:Lcom/facebook/events/model/Event;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/EventCompositionModel;->a(Lcom/facebook/events/model/Event;)V

    .line 1811486
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_key_event_ticket_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->au:Ljava/lang/String;

    .line 1811487
    :goto_1
    const v0, 0x7f0d31d6

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1811488
    const v1, 0x7f0d0c8b

    invoke-virtual {p0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 1811489
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->K:LX/11u;

    .line 1811490
    iput-object v0, v2, LX/11u;->e:Landroid/view/ViewStub;

    .line 1811491
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->K:LX/11u;

    invoke-virtual {v0, v1}, LX/11u;->b(Landroid/view/ViewStub;)V

    .line 1811492
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->E:LX/Bjw;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aK:Landroid/view/View$OnClickListener;

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aJ:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, p0, v1, v2}, LX/Bjw;->a(Lcom/facebook/base/activity/FbFragmentActivity;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    .line 1811493
    const v0, 0x7f0d0dd7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    .line 1811494
    const v0, 0x7f0d0dd5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->W:Landroid/widget/FrameLayout;

    .line 1811495
    const v0, 0x7f0d0de3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Y:Landroid/widget/FrameLayout;

    .line 1811496
    const v0, 0x7f0d0dc4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventNameEditText;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    .line 1811497
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->r:LX/Bij;

    .line 1811498
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventNameEditText;->h:LX/Bii;

    .line 1811499
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventNameEditText;->a(Lcom/facebook/events/create/EventCompositionModel;)V

    .line 1811500
    if-eqz p1, :cond_3

    .line 1811501
    const-string v0, "PAGE_EVENT_HOST"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1811502
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_page_event_host_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1811503
    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_page_event_host_id"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1811504
    if-eqz v0, :cond_4

    .line 1811505
    iput-boolean v7, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aB:Z

    .line 1811506
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Lcom/facebook/events/model/PrivacyType;Ljava/lang/String;)V

    .line 1811507
    invoke-static {p0, v7}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811508
    :cond_4
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->D()V

    .line 1811509
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->E()V

    .line 1811510
    invoke-direct {p0, p1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->d(Landroid/os/Bundle;)V

    .line 1811511
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->b()V

    .line 1811512
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->l()V

    .line 1811513
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->m()V

    .line 1811514
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->n()V

    .line 1811515
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->A()V

    .line 1811516
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aN:LX/9el;

    iget-object v4, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aO:LX/Biv;

    invoke-virtual {v0, v1, v2, v4}, LX/BkU;->a(Lcom/facebook/events/create/EventCompositionModel;LX/9el;LX/Biv;)V

    .line 1811517
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811518
    iget-object v2, v1, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v1, v2

    .line 1811519
    iput-object v1, v0, LX/BkU;->k:Lcom/facebook/events/model/PrivacyType;

    .line 1811520
    if-eqz p1, :cond_5

    .line 1811521
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    invoke-virtual {v0, p1}, LX/BkU;->b(Landroid/os/Bundle;)V

    .line 1811522
    :cond_5
    new-instance v0, LX/Bkm;

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ag:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    iget-object v4, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Y:Landroid/widget/FrameLayout;

    const/16 v5, 0x12c

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/Bkm;-><init>(Landroid/content/Context;Lcom/facebook/events/create/ui/CoverPhotoSelector;Landroid/widget/LinearLayout;Landroid/widget/FrameLayout;I)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aD:LX/Bkm;

    .line 1811523
    const v0, 0x7f0d0dd6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    .line 1811524
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811525
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v1, v2

    .line 1811526
    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aD:LX/Bkm;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Lcom/facebook/events/create/ui/EventCoverPhotoModel;LX/Bkm;)V

    .line 1811527
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811528
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811529
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_a

    .line 1811530
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    invoke-virtual {v0, v7}, LX/Bih;->a(Z)V

    .line 1811531
    :goto_2
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->I()V

    .line 1811532
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->w:LX/1nQ;

    .line 1811533
    const-string v1, "event_composer_open"

    const-string v2, "event_composer"

    invoke-static {v0, v1, v2}, LX/1nQ;->f(LX/1nQ;Ljava/lang/String;Ljava/lang/String;)V

    .line 1811534
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->z:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->av:J

    .line 1811535
    const-string v0, "extra_ref_module"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1811536
    new-instance v1, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v2, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3, v6}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1811537
    const-string v0, "extras_event_action_mechanism"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/ActionMechanism;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ar:Lcom/facebook/events/common/ActionMechanism;

    .line 1811538
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ar:Lcom/facebook/events/common/ActionMechanism;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ar:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_3
    iput-object v0, v1, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    .line 1811539
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->J()V

    .line 1811540
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811541
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v0, v1

    .line 1811542
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->g()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->b(Z)V

    .line 1811543
    if-nez p1, :cond_f

    .line 1811544
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811545
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811546
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_e

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->C:LX/0ad;

    sget-short v1, LX/Bjt;->b:S

    invoke-interface {v0, v1, v8}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_e

    move v0, v7

    :goto_4
    iput-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ay:Z

    .line 1811547
    :goto_5
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ay:Z

    if-eqz v0, :cond_6

    .line 1811548
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->C:LX/0ad;

    sget-short v1, LX/Bjt;->d:S

    invoke-interface {v0, v1, v8}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->az:Z

    .line 1811549
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->C:LX/0ad;

    sget-short v1, LX/Bjt;->c:S

    invoke-interface {v0, v1, v8}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aA:Z

    .line 1811550
    :cond_6
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ay:Z

    invoke-static {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->c$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811551
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->w:LX/1nQ;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aq:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->e:Ljava/lang/String;

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->R()Lcom/facebook/events/common/ActionMechanism;

    move-result-object v3

    .line 1811552
    iget-object v4, v0, LX/1nQ;->i:LX/0Zb;

    const-string v5, "view"

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 1811553
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1811554
    const-string v5, "event_composer"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    iget-object v5, v0, LX/1nQ;->j:LX/0kv;

    iget-object v6, v0, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v5, v6}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "ref_module"

    invoke-virtual {v4, v5, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "source_module"

    const-string v6, "event_composer"

    invoke-virtual {v4, v5, v6}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "ref_mechanism"

    invoke-virtual {v4, v5, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "mechanism"

    invoke-virtual {v4, v5, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4}, LX/0oG;->d()V

    .line 1811555
    :cond_7
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Q:LX/BkO;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811556
    iput-object v1, v0, LX/BkO;->d:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811557
    invoke-static {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->o$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    .line 1811558
    return-void

    :catch_0
    move-object v0, v6

    goto/16 :goto_0

    .line 1811559
    :cond_8
    iput-boolean v7, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aC:Z

    .line 1811560
    invoke-direct {p0, v9}, Lcom/facebook/events/create/EventCreationNikumanActivity;->e(Landroid/os/Bundle;)V

    goto/16 :goto_1

    .line 1811561
    :cond_9
    const-string v0, "EVENT_COMPOSITION_MODEL"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/EventCompositionModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811562
    const-string v0, "EVENT_TICKET_URL"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->au:Ljava/lang/String;

    .line 1811563
    const-string v0, "IS_ALLOWING_PRIVACY_TOGGLING"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aC:Z

    goto/16 :goto_1

    .line 1811564
    :cond_a
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811565
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811566
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811567
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811568
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811569
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1811570
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_c

    .line 1811571
    :cond_b
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    invoke-virtual {v0, v7}, LX/Bih;->c(Z)V

    .line 1811572
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    invoke-virtual {v0, v8}, LX/Bih;->b(Z)V

    goto/16 :goto_2

    .line 1811573
    :cond_c
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->v:LX/Bih;

    invoke-virtual {v0, v8}, LX/Bih;->a(Z)V

    goto/16 :goto_2

    .line 1811574
    :cond_d
    const-string v0, "unknown"

    goto/16 :goto_3

    :cond_e
    move v0, v8

    .line 1811575
    goto/16 :goto_4

    .line 1811576
    :cond_f
    const-string v0, "IS_SHOWING_COLLAPSED_FLOW"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ay:Z

    goto/16 :goto_5
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 1811458
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1811459
    const v0, 0x7f0400b6

    const v1, 0x7f0400ca

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->overridePendingTransition(II)V

    .line 1811460
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    .line 1811437
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1811438
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 1811439
    :cond_0
    :goto_0
    return-void

    .line 1811440
    :cond_1
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1811441
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ab:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-virtual {v0, p3}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1811442
    :sswitch_1
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ah:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-virtual {v0, p1, p3}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1811443
    :sswitch_2
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ai:Lcom/facebook/events/create/ui/CohostsSelector;

    invoke-virtual {v0, p3}, Lcom/facebook/events/create/ui/CohostsSelector;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1811444
    :sswitch_3
    const-string v0, "selected_host"

    invoke-static {p3, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1811445
    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->a(Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1811446
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->B()V

    .line 1811447
    iput-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1811448
    invoke-static {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->z(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    goto :goto_0

    .line 1811449
    :sswitch_4
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ak:Lcom/facebook/events/create/ui/EventCategorySelector;

    invoke-virtual {v0, p3}, Lcom/facebook/events/create/ui/EventCategorySelector;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1811450
    :sswitch_5
    const-string v0, "extra_save_as_draft"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 1811451
    const-string v1, "extra_scheduled_publish_time"

    const-wide/16 v2, 0x0

    invoke-virtual {p3, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1811452
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811453
    iput-boolean v0, v1, Lcom/facebook/events/create/EventCompositionModel;->p:Z

    .line 1811454
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811455
    iput-wide v2, v0, Lcom/facebook/events/create/EventCompositionModel;->q:J

    .line 1811456
    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->t()V

    .line 1811457
    invoke-static {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->o$redex0(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_0
        0x67 -> :sswitch_1
        0x68 -> :sswitch_1
        0x69 -> :sswitch_2
        0x6c -> :sswitch_3
        0x6d -> :sswitch_4
        0x6f -> :sswitch_5
        0x26b9 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onBackPressed()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1811416
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1811417
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ap:Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->a()V

    .line 1811418
    :goto_0
    return-void

    .line 1811419
    :cond_0
    invoke-static {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->Q(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    .line 1811420
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811421
    iget-object v3, v0, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    move-object v0, v3

    .line 1811422
    iget-object v3, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v3, v3

    .line 1811423
    if-nez v3, :cond_1

    .line 1811424
    iget-object v3, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v0, v3

    .line 1811425
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    move v0, v2

    .line 1811426
    :goto_1
    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811427
    iget-object v4, v3, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    move-object v3, v4

    .line 1811428
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v3}, Lcom/facebook/events/create/EventCompositionModel;->c()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ae:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    .line 1811429
    iget-boolean v4, v3, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->h:Z

    move v3, v4

    .line 1811430
    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v3}, Lcom/facebook/events/create/EventCompositionModel;->g()Z

    move-result v3

    if-nez v3, :cond_2

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/EventCompositionModel;->o()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1811431
    :cond_2
    :goto_2
    if-nez v2, :cond_5

    .line 1811432
    invoke-static {p0, v1}, Lcom/facebook/events/create/EventCreationNikumanActivity;->f(Lcom/facebook/events/create/EventCreationNikumanActivity;Z)V

    .line 1811433
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1811434
    goto :goto_1

    :cond_4
    move v2, v1

    .line 1811435
    goto :goto_2

    .line 1811436
    :cond_5
    new-instance v0, LX/Bj8;

    invoke-direct {v0, p0}, LX/Bj8;-><init>(Lcom/facebook/events/create/EventCreationNikumanActivity;)V

    invoke-direct {p0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->O()LX/Bj9;

    move-result-object v1

    invoke-static {p0, v0, v1}, LX/BjN;->a(Landroid/app/Activity;LX/Bj8;LX/Bj9;)V

    goto :goto_0
.end method

.method public final onEnterAnimationComplete()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1811414
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->r:LX/Bij;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->Z:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v0, v1}, LX/Bij;->b(Lcom/facebook/events/create/ui/EventNameEditText;)V

    .line 1811415
    return-void
.end method

.method public final onPause()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x2d85e46b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1811409
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onPause()V

    .line 1811410
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->y:LX/Bjk;

    .line 1811411
    iget-object v2, v1, LX/Bjk;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object v4, v1, LX/Bjk;->a:LX/0Yj;

    invoke-interface {v2, v4}, Lcom/facebook/performancelogger/PerformanceLogger;->f(LX/0Yj;)V

    .line 1811412
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->K:LX/11u;

    invoke-virtual {v1}, LX/11u;->f()V

    .line 1811413
    const/16 v1, 0x23

    const v2, 0x300e49f1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1811404
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1811405
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    .line 1811406
    iget-object p1, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v0, p1

    .line 1811407
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->g()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventCreationNikumanActivity;->b(Z)V

    .line 1811408
    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x15f5db4e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1811401
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onResume()V

    .line 1811402
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->K:LX/11u;

    invoke-virtual {v1}, LX/11u;->d()V

    .line 1811403
    const/16 v1, 0x23

    const v2, 0x267255a5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1811383
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1811384
    const-string v0, "PAGE_EVENT_HOST"

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aw:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1811385
    const-string v0, "EVENT_COMPOSITION_MODEL"

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->U:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1811386
    const-string v0, "EVENT_TICKET_URL"

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->au:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811387
    const-string v1, "EVENT_PRIVACY_TYPE"

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811388
    iget-object v2, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v2

    .line 1811389
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1811390
    const-string v1, "EVENT_CAN_GUEST_INVITE_FRIENDS"

    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811391
    iget-boolean v2, v0, LX/Bn1;->f:Z

    move v0, v2

    .line 1811392
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1811393
    const-string v0, "IS_SHOWING_COLLAPSED_FLOW"

    iget-boolean v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->ay:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1811394
    const-string v0, "IS_ALLOWING_PRIVACY_TOGGLING"

    iget-boolean v1, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->aC:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1811395
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->S:LX/BkU;

    invoke-virtual {v0, p1}, LX/BkU;->a(Landroid/os/Bundle;)V

    .line 1811396
    return-void

    .line 1811397
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationNikumanActivity;->u:LX/Bn1;

    .line 1811398
    iget-object v2, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v2

    .line 1811399
    invoke-virtual {v0}, Lcom/facebook/events/model/PrivacyType;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1811400
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
