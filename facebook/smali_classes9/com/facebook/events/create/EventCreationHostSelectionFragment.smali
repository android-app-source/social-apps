.class public Lcom/facebook/events/create/EventCreationHostSelectionFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Bjs;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Biq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Ljava/lang/String;

.field public e:LX/Bip;

.field public f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/Bir;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1811011
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1811012
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    invoke-static {v3}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v1

    check-cast v1, LX/0zG;

    invoke-static {v3}, LX/Bjs;->b(LX/0QB;)LX/Bjs;

    move-result-object v2

    check-cast v2, LX/Bjs;

    const-class p0, LX/Biq;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/Biq;

    iput-object v1, p1, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->a:LX/0zG;

    iput-object v2, p1, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->b:LX/Bjs;

    iput-object v3, p1, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->c:LX/Biq;

    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 3

    .prologue
    .line 1810996
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1810997
    const-string v1, "selected_host"

    iget-object v2, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->g:Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1810998
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1810999
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1811000
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1811001
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1811002
    const-class v0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;

    invoke-static {v0, p0}, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1811003
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1811004
    const-string v1, "extra_event_host_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->d:Ljava/lang/String;

    .line 1811005
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->c:LX/Biq;

    iget-object v1, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->d:Ljava/lang/String;

    .line 1811006
    new-instance v2, LX/Bip;

    const/16 p1, 0x12cb

    invoke-static {v0, p1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p1

    invoke-direct {v2, v1, p1}, LX/Bip;-><init>(Ljava/lang/String;LX/0Or;)V

    .line 1811007
    move-object v0, v2

    .line 1811008
    iput-object v0, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->e:LX/Bip;

    .line 1811009
    new-instance v0, LX/Bir;

    invoke-direct {v0, p0}, LX/Bir;-><init>(Lcom/facebook/events/create/EventCreationHostSelectionFragment;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->h:LX/Bir;

    .line 1811010
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x45cf5f8e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1810995
    const v1, 0x7f0304bc

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x5e9538fe

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x6851e2e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1810991
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1810992
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->a:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v2, 0x7f08214c

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 1810993
    iget-object v0, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->b:LX/Bjs;

    const/4 v2, 0x0

    new-instance v3, LX/Bit;

    invoke-direct {v3, p0}, LX/Bit;-><init>(Lcom/facebook/events/create/EventCreationHostSelectionFragment;)V

    invoke-virtual {v0, v2, v3}, LX/Bjs;->a(Ljava/lang/String;LX/Bis;)V

    .line 1810994
    const/16 v0, 0x2b

    const v2, -0x670657f8

    invoke-static {v4, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1810982
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1810983
    const v0, 0x7f0d0dea

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    .line 1810984
    new-instance v1, LX/1P1;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1P1;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1810985
    new-instance v1, LX/Bkn;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a011a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1555

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iget-object v4, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->e:LX/Bip;

    const v5, 0x7f0d01a1

    invoke-direct {v1, v2, v3, v4, v5}, LX/Bkn;-><init>(IILX/1OM;I)V

    .line 1810986
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1553

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1810987
    iput v2, v1, LX/Bkn;->d:I

    .line 1810988
    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1810989
    iget-object v1, p0, Lcom/facebook/events/create/EventCreationHostSelectionFragment;->e:LX/Bip;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1810990
    return-void
.end method
