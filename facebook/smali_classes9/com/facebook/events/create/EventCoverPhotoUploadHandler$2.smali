.class public final Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/util/HashSet;

.field public final synthetic b:LX/8O7;

.field public final synthetic c:LX/8OL;

.field public final synthetic d:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic e:LX/Bib;


# direct methods
.method public constructor <init>(LX/Bib;Ljava/util/HashSet;LX/8O7;LX/8OL;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 1810554
    iput-object p1, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->e:LX/Bib;

    iput-object p2, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->a:Ljava/util/HashSet;

    iput-object p3, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->b:LX/8O7;

    iput-object p4, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->c:LX/8OL;

    iput-object p5, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->d:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1810555
    :try_start_0
    iget-object v0, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->e:LX/Bib;

    iget-object v0, v0, LX/Bib;->a:LX/8OJ;

    iget-object v1, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->a:Ljava/util/HashSet;

    iget-object v2, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->b:LX/8O7;

    iget-object v3, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->c:LX/8OL;

    iget-object v4, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->e:LX/Bib;

    iget-object v4, v4, LX/Bib;->h:LX/73w;

    iget-object v5, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->e:LX/Bib;

    iget-object v5, v5, LX/Bib;->h:LX/73w;

    const-string v6, "2.0"

    invoke-virtual {v5, v6}, LX/73w;->j(Ljava/lang/String;)LX/74b;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->e:LX/Bib;

    iget-object v7, v7, LX/Bib;->f:LX/8Ne;

    const-class v8, LX/Bib;

    invoke-static {v8}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/8OJ;->a(Ljava/util/Collection;LX/8O7;LX/8OL;LX/73w;LX/74b;Lcom/facebook/photos/upload/operation/UploadOperation;LX/8Ne;Lcom/facebook/common/callercontext/CallerContext;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1810556
    :goto_0
    return-void

    .line 1810557
    :catch_0
    move-exception v0

    .line 1810558
    iget-object v1, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->e:LX/Bib;

    iget-object v1, v1, LX/Bib;->d:LX/03V;

    const-class v2, LX/Bib;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to upload group cover photo"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1810559
    iget-object v1, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$2;->d:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v1, v0}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method
