.class public Lcom/facebook/events/create/ui/CoverPhotoSelector;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# static fields
.field private static i:I

.field private static j:I


# instance fields
.field private a:Landroid/app/Activity;

.field private b:LX/0iA;

.field private c:LX/3kp;

.field public d:Z

.field private e:Z

.field private f:Z

.field private g:LX/0hs;

.field private h:Z

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1814386
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1814387
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->f:Z

    .line 1814388
    invoke-static {p1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(Landroid/content/Context;)V

    .line 1814389
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1814382
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1814383
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->f:Z

    .line 1814384
    invoke-static {p1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(Landroid/content/Context;)V

    .line 1814385
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1814378
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814379
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->f:Z

    .line 1814380
    invoke-static {p1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(Landroid/content/Context;)V

    .line 1814381
    return-void
.end method

.method private a(Landroid/app/Activity;LX/0iA;LX/3kp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1814374
    iput-object p1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a:Landroid/app/Activity;

    .line 1814375
    iput-object p2, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->b:LX/0iA;

    .line 1814376
    iput-object p3, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->c:LX/3kp;

    .line 1814377
    return-void
.end method

.method private static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 1814370
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1814371
    const v1, 0x7f0b1542

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/facebook/events/create/ui/CoverPhotoSelector;->i:I

    .line 1814372
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-double v0, v0

    const-wide v2, 0x3ffc7ae147ae147bL    # 1.78

    div-double/2addr v0, v2

    sget v2, Lcom/facebook/events/create/ui/CoverPhotoSelector;->i:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    double-to-int v0, v0

    sput v0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->j:I

    .line 1814373
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;

    invoke-static {v2}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-static {v2}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v1

    check-cast v1, LX/0iA;

    invoke-static {v2}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v2

    check-cast v2, LX/3kp;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(Landroid/app/Activity;LX/0iA;LX/3kp;)V

    return-void
.end method

.method private b(Z)Landroid/animation/ObjectAnimator;
    .locals 5

    .prologue
    .line 1814365
    if-eqz p1, :cond_0

    sget v0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->j:I

    int-to-float v0, v0

    move v1, v0

    .line 1814366
    :goto_0
    if-eqz p1, :cond_1

    sget v0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->i:I

    int-to-float v0, v0

    .line 1814367
    :goto_1
    const-string v2, "y"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v1, v3, v4

    const/4 v1, 0x1

    aput v0, v3, v1

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    return-object v0

    .line 1814368
    :cond_0
    sget v0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->i:I

    int-to-float v0, v0

    move v1, v0

    goto :goto_0

    .line 1814369
    :cond_1
    sget v0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->j:I

    int-to-float v0, v0

    goto :goto_1
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 1814363
    iget-object v0, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->b:LX/0iA;

    sget-object v1, LX/3kl;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v2, LX/3kl;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kl;

    .line 1814364
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/3kl;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "3819"

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(ZI)V
    .locals 4

    .prologue
    .line 1814390
    invoke-direct {p0, p1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->b(Z)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1814391
    invoke-direct {p0, p1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->b(Z)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1814392
    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1814393
    new-instance v1, LX/BkV;

    invoke-direct {v1, p0, p1}, LX/BkV;-><init>(Lcom/facebook/events/create/ui/CoverPhotoSelector;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1814394
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1814395
    return-void
.end method

.method public final a(ZZ)V
    .locals 2

    .prologue
    .line 1814356
    if-nez p2, :cond_0

    .line 1814357
    if-eqz p1, :cond_1

    .line 1814358
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1814359
    sget v1, Lcom/facebook/events/create/ui/CoverPhotoSelector;->j:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 1814360
    :cond_0
    :goto_0
    return-void

    .line 1814361
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 1814362
    sget v1, Lcom/facebook/events/create/ui/CoverPhotoSelector;->i:I

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1814355
    iget-boolean v0, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->k:Z

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1814353
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->k:Z

    .line 1814354
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2c

    const v1, 0x3e40c66d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1814338
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onAttachedToWindow()V

    .line 1814339
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->c:LX/3kp;

    .line 1814340
    iput v4, v1, LX/3kp;->b:I

    .line 1814341
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->c:LX/3kp;

    sget-object v2, LX/7vb;->c:LX/0Tn;

    invoke-virtual {v1, v2}, LX/3kp;->a(LX/0Tn;)V

    .line 1814342
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->c:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->h:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->e:Z

    if-eqz v1, :cond_0

    .line 1814343
    new-instance v1, LX/0hs;

    iget-object v2, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a:Landroid/app/Activity;

    invoke-direct {v1, v2, v4}, LX/0hs;-><init>(Landroid/content/Context;I)V

    iput-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->g:LX/0hs;

    .line 1814344
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->g:LX/0hs;

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082136

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 1814345
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->g:LX/0hs;

    sget-object v2, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v1, v2}, LX/0ht;->a(LX/3AV;)V

    .line 1814346
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->g:LX/0hs;

    const/4 v2, -0x1

    .line 1814347
    iput v2, v1, LX/0hs;->t:I

    .line 1814348
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->g:LX/0hs;

    invoke-virtual {v1, p0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1814349
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->h:Z

    .line 1814350
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->c:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->a()V

    .line 1814351
    iget-object v1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->b:LX/0iA;

    invoke-virtual {v1}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v1

    const-string v2, "3819"

    invoke-virtual {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 1814352
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x2e3ef2fc

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishInflate()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3b5f8025

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1814333
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onFinishInflate()V

    .line 1814334
    const-class v1, Lcom/facebook/events/create/ui/CoverPhotoSelector;

    invoke-static {v1, p0}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1814335
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->setFocusable(Z)V

    .line 1814336
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082137

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1814337
    const/16 v1, 0x2d

    const v2, 0x6afdae2a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1814326
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/os/Bundle;

    if-eq v0, v1, :cond_0

    .line 1814327
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1814328
    :goto_0
    return-void

    .line 1814329
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 1814330
    const-string v0, "coverPhotoSelectorSuperState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1814331
    const-string v0, "showRemoveButton"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->d:Z

    .line 1814332
    const-string v0, "hasClickedOnCoverPhotoSelector"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->k:Z

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1814320
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1814321
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1814322
    const-string v2, "coverPhotoSelectorSuperState"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1814323
    const-string v0, "showRemoveButton"

    iget-boolean v2, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->d:Z

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1814324
    const-string v0, "hasClickedOnCoverPhotoSelector"

    iget-boolean v2, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->k:Z

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1814325
    return-object v1
.end method

.method public setShowAdvancedCoverPhotoOptions(Z)V
    .locals 0

    .prologue
    .line 1814318
    iput-boolean p1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->e:Z

    .line 1814319
    return-void
.end method

.method public setShowChooseExistingPhotoOption(Z)V
    .locals 0

    .prologue
    .line 1814316
    iput-boolean p1, p0, Lcom/facebook/events/create/ui/CoverPhotoSelector;->f:Z

    .line 1814317
    return-void
.end method
