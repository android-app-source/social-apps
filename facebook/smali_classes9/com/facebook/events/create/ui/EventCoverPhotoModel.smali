.class public Lcom/facebook/events/create/ui/EventCoverPhotoModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/create/ui/EventCoverPhotoModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Landroid/net/Uri;

.field public d:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1814497
    new-instance v0, LX/BkY;

    invoke-direct {v0}, LX/BkY;-><init>()V

    sput-object v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1814498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1814499
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1814500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1814501
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    .line 1814502
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    .line 1814503
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    .line 1814504
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d:Landroid/net/Uri;

    .line 1814505
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1814506
    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    .line 1814507
    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    .line 1814508
    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    .line 1814509
    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d:Landroid/net/Uri;

    .line 1814510
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1814511
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1814512
    const/4 v0, 0x0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1814513
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1814514
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 1814515
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1814516
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1814517
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1814518
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1814519
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1814520
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1814521
    return-void
.end method
