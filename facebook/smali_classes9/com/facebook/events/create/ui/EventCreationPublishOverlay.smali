.class public Lcom/facebook/events/create/ui/EventCreationPublishOverlay;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/events/create/EventCompositionModel;

.field private d:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public e:LX/Bj0;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1814720
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1814721
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->b()V

    .line 1814722
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1814717
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1814718
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->b()V

    .line 1814719
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1814714
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814715
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->b()V

    .line 1814716
    return-void
.end method

.method private static a(Lcom/facebook/events/create/ui/EventCreationPublishOverlay;LX/0wM;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1814713
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->a:LX/0wM;

    iput-object p2, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->b:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-static {v1}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->a(Lcom/facebook/events/create/ui/EventCreationPublishOverlay;LX/0wM;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 1814706
    const-class v0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;

    invoke-static {v0, p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1814707
    const v0, 0x7f0304c2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1814708
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00b2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1814709
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->e()V

    .line 1814710
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setClickable(Z)V

    .line 1814711
    invoke-virtual {p0, p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1814712
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 1814690
    const v0, 0x7f0d0dee

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->d:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1814691
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->a:LX/0wM;

    const v1, 0x7f0207fd

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1814692
    iget-object v1, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->d:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1814693
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->d:Lcom/facebook/fbui/widget/contentview/ContentView;

    new-instance v1, LX/Bkh;

    invoke-direct {v1, p0}, LX/Bkh;-><init>(Lcom/facebook/events/create/ui/EventCreationPublishOverlay;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1814694
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1814702
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->setVisibility(I)V

    .line 1814703
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->e:LX/Bj0;

    if-eqz v0, :cond_0

    .line 1814704
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->e:LX/Bj0;

    invoke-interface {v0}, LX/Bj0;->a()V

    .line 1814705
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x2fb660bb

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1814700
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->a()V

    .line 1814701
    const v1, -0x60757828    # -5.867001E-20f

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnPublishOverlayDismissedListener(LX/Bj0;)V
    .locals 0

    .prologue
    .line 1814698
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->e:LX/Bj0;

    .line 1814699
    return-void
.end method

.method public setSubtitleText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1814695
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->d:Lcom/facebook/fbui/widget/contentview/ContentView;

    if-eqz v0, :cond_0

    .line 1814696
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationPublishOverlay;->d:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 1814697
    :cond_0
    return-void
.end method
