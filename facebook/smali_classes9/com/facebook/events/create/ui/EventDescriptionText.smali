.class public Lcom/facebook/events/create/ui/EventDescriptionText;
.super Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;
.source ""


# instance fields
.field private b:Lcom/facebook/events/create/EventCompositionModel;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1814746
    invoke-direct {p0, p1, p2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1814747
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventDescriptionText;->setSaveEnabled(Z)V

    .line 1814748
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/create/ui/EventDescriptionText;)V
    .locals 2

    .prologue
    .line 1814749
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventDescriptionText;->b:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v1

    .line 1814750
    iput-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    .line 1814751
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/create/EventCompositionModel;)V
    .locals 1

    .prologue
    .line 1814738
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventDescriptionText;->b:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814739
    new-instance v0, LX/Bki;

    invoke-direct {v0, p0}, LX/Bki;-><init>(Lcom/facebook/events/create/ui/EventDescriptionText;)V

    .line 1814740
    iput-object v0, p0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->b:LX/8oi;

    .line 1814741
    new-instance v0, LX/Bkj;

    invoke-direct {v0, p0}, LX/Bkj;-><init>(Lcom/facebook/events/create/ui/EventDescriptionText;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventDescriptionText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1814742
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventDescriptionText;->b:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814743
    iget-object p1, v0, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    move-object v0, p1

    .line 1814744
    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventDescriptionText;->setText(Ljava/lang/CharSequence;)V

    .line 1814745
    return-void
.end method

.method public getDescriptionAsTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 2

    .prologue
    .line 1814731
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventDescriptionText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1814732
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 1814733
    move-object v0, v0

    .line 1814734
    invoke-virtual {p0}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getMentionsEntityRanges()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 1814735
    iput-object v1, v0, LX/173;->e:LX/0Px;

    .line 1814736
    move-object v0, v0

    .line 1814737
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    return-object v0
.end method
