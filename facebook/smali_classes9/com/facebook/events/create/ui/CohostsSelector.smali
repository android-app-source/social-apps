.class public Lcom/facebook/events/create/ui/CohostsSelector;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/facebook/events/create/ui/EventCohostsModel;

.field private b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Z

.field private d:Lcom/facebook/content/SecureContextHelper;

.field private e:Landroid/app/Activity;

.field private f:I

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1814157
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/events/create/ui/CohostsSelector;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1814158
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1814159
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/events/create/ui/CohostsSelector;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814160
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1814161
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814162
    invoke-direct {p0}, Lcom/facebook/events/create/ui/CohostsSelector;->b()V

    .line 1814163
    return-void
.end method

.method private a(Lcom/facebook/content/SecureContextHelper;Landroid/app/Activity;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1814164
    iput-object p1, p0, Lcom/facebook/events/create/ui/CohostsSelector;->d:Lcom/facebook/content/SecureContextHelper;

    .line 1814165
    iput-object p2, p0, Lcom/facebook/events/create/ui/CohostsSelector;->e:Landroid/app/Activity;

    .line 1814166
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/create/ui/CohostsSelector;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/create/ui/CohostsSelector;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/create/ui/CohostsSelector;->a(Lcom/facebook/content/SecureContextHelper;Landroid/app/Activity;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1814132
    const-class v0, Lcom/facebook/events/create/ui/CohostsSelector;

    invoke-static {v0, p0}, Lcom/facebook/events/create/ui/CohostsSelector;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1814133
    const v0, 0x7f030550

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1814134
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/CohostsSelector;->setFocusable(Z)V

    .line 1814135
    const v0, 0x7f0d0eef

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1814136
    invoke-virtual {p0, p0}, Lcom/facebook/events/create/ui/CohostsSelector;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1814137
    return-void
.end method

.method private e()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1814138
    iget-object v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->a:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1814139
    iget-object v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    move-object v3, v1

    .line 1814140
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    move v1, v2

    .line 1814141
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1814142
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1814143
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1814144
    :cond_0
    invoke-static {}, Lcom/facebook/tagging/conversion/FriendSelectorConfig;->newBuilder()LX/A5S;

    move-result-object v0

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 1814145
    iput-object v1, v0, LX/A5S;->j:LX/0Px;

    .line 1814146
    move-object v0, v0

    .line 1814147
    const v1, 0x7f082135

    .line 1814148
    iput v1, v0, LX/A5S;->b:I

    .line 1814149
    move-object v0, v0

    .line 1814150
    iget-boolean v1, p0, Lcom/facebook/events/create/ui/CohostsSelector;->c:Z

    if-eqz v1, :cond_1

    const/4 v2, 0x1

    .line 1814151
    :cond_1
    iput-boolean v2, v0, LX/A5S;->d:Z

    .line 1814152
    move-object v0, v0

    .line 1814153
    invoke-virtual {v0}, LX/A5S;->a()Lcom/facebook/tagging/conversion/FriendSelectorConfig;

    move-result-object v0

    .line 1814154
    iget-object v1, p0, Lcom/facebook/events/create/ui/CohostsSelector;->e:Landroid/app/Activity;

    invoke-static {v1, v0}, Lcom/facebook/tagging/conversion/FriendSuggestionsAndSelectorActivity;->a(Landroid/content/Context;Lcom/facebook/tagging/conversion/FriendSelectorConfig;)Landroid/content/Intent;

    move-result-object v0

    .line 1814155
    iget-object v1, p0, Lcom/facebook/events/create/ui/CohostsSelector;->d:Lcom/facebook/content/SecureContextHelper;

    iget v2, p0, Lcom/facebook/events/create/ui/CohostsSelector;->f:I

    iget-object v3, p0, Lcom/facebook/events/create/ui/CohostsSelector;->e:Landroid/app/Activity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1814156
    return-void
.end method

.method private f()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1814076
    iget-object v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->a:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1814077
    iget v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->c:I

    move v1, v1

    .line 1814078
    iget-object v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->a:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1814079
    iget-object v2, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->b:Ljava/lang/String;

    move-object v0, v2

    .line 1814080
    if-lez v1, :cond_0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1814081
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->b:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1814082
    :goto_0
    return-void

    .line 1814083
    :cond_1
    if-le v1, v5, :cond_2

    .line 1814084
    add-int/lit8 v1, v1, -0x1

    .line 1814085
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/CohostsSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0101

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1814086
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/CohostsSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f082134

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1814087
    iget-object v2, p0, Lcom/facebook/events/create/ui/CohostsSelector;->b:Lcom/facebook/resources/ui/FbTextView;

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v0, v3, v6

    invoke-static {v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    .line 1814113
    iget-object v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->a:Lcom/facebook/events/create/ui/EventCohostsModel;

    const/4 v2, 0x0

    .line 1814114
    const-string v1, "profiles"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1814115
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/facebook/events/create/ui/CohostsSelector;->f()V

    .line 1814116
    return-void

    .line 1814117
    :cond_1
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1814118
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    .line 1814119
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->b:Ljava/lang/String;

    .line 1814120
    iput v2, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->c:I

    .line 1814121
    const-string v1, "profiles"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v3

    .line 1814122
    if-eqz v3, :cond_3

    .line 1814123
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 1814124
    array-length v5, v3

    move v1, v2

    :goto_1
    if-ge v1, v5, :cond_2

    aget-wide v7, v3, v1

    .line 1814125
    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1814126
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1814127
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    .line 1814128
    :cond_3
    const-string v1, "full_profiles"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 1814129
    invoke-static {v3}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1814130
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/model/FacebookProfile;

    iget-object v1, v1, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->b:Ljava/lang/String;

    .line 1814131
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    iput v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->c:I

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/create/ui/EventCohostsModel;IZZ)V
    .locals 1

    .prologue
    .line 1814088
    iput-object p1, p0, Lcom/facebook/events/create/ui/CohostsSelector;->a:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1814089
    iput p2, p0, Lcom/facebook/events/create/ui/CohostsSelector;->f:I

    .line 1814090
    iput-boolean p4, p0, Lcom/facebook/events/create/ui/CohostsSelector;->c:Z

    .line 1814091
    invoke-direct {p0}, Lcom/facebook/events/create/ui/CohostsSelector;->f()V

    .line 1814092
    if-eqz p3, :cond_0

    .line 1814093
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/CohostsSelector;->setVisibility(I)V

    .line 1814094
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1814095
    iget-boolean v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->g:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x2

    const v0, 0xeed525d

    invoke-static {v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1814096
    iput-boolean v1, p0, Lcom/facebook/events/create/ui/CohostsSelector;->g:Z

    .line 1814097
    invoke-direct {p0}, Lcom/facebook/events/create/ui/CohostsSelector;->e()V

    .line 1814098
    const v1, 0x1af517b3

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1814099
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/os/Bundle;

    if-eq v0, v1, :cond_0

    .line 1814100
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1814101
    :goto_0
    return-void

    .line 1814102
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 1814103
    const-string v0, "cohostsSlectorSuperState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1814104
    const-string v0, "hasClickedOnCohostSelector"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->g:Z

    .line 1814105
    const-string v0, "includeViewerAsCohostChoice"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/CohostsSelector;->c:Z

    .line 1814106
    invoke-direct {p0}, Lcom/facebook/events/create/ui/CohostsSelector;->f()V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1814107
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1814108
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1814109
    const-string v2, "cohostsSlectorSuperState"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1814110
    const-string v0, "hasClickedOnCohostSelector"

    iget-boolean v2, p0, Lcom/facebook/events/create/ui/CohostsSelector;->g:Z

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1814111
    const-string v0, "includeViewerAsCohostChoice"

    iget-boolean v2, p0, Lcom/facebook/events/create/ui/CohostsSelector;->c:Z

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1814112
    return-object v1
.end method
