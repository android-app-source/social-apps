.class public Lcom/facebook/events/create/ui/EventHostSelector;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:Z

.field private e:Ljava/lang/String;

.field public f:I

.field private g:Z

.field private h:Lcom/facebook/fbui/glyph/GlyphView;

.field private i:Lcom/facebook/resources/ui/FbTextView;

.field private j:Landroid/text/style/MetricAffectingSpan;

.field private k:Landroid/text/style/MetricAffectingSpan;

.field private l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1814791
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1814792
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->a()V

    .line 1814793
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1814805
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1814806
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->a()V

    .line 1814807
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1814802
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814803
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->a()V

    .line 1814804
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1814794
    const-class v0, Lcom/facebook/events/create/ui/EventHostSelector;

    invoke-static {v0, p0}, Lcom/facebook/events/create/ui/EventHostSelector;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1814795
    const v0, 0x7f0304c1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1814796
    const v0, 0x7f0d0dec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->h:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1814797
    const v0, 0x7f0d0ded

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->i:Lcom/facebook/resources/ui/FbTextView;

    .line 1814798
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e08ad

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->j:Landroid/text/style/MetricAffectingSpan;

    .line 1814799
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e08ae

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->k:Landroid/text/style/MetricAffectingSpan;

    .line 1814800
    invoke-virtual {p0, p0}, Lcom/facebook/events/create/ui/EventHostSelector;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1814801
    return-void
.end method

.method private a(LX/0Or;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1814788
    iput-object p2, p0, Lcom/facebook/events/create/ui/EventHostSelector;->m:Lcom/facebook/content/SecureContextHelper;

    .line 1814789
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->l:LX/0Or;

    .line 1814790
    return-void
.end method

.method private static a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 1814784
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1814785
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1814786
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, p2, v0, v1, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1814787
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/create/ui/EventHostSelector;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/create/ui/EventHostSelector;

    const/16 v1, 0xc

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v1, v0}, Lcom/facebook/events/create/ui/EventHostSelector;->a(LX/0Or;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1814781
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->h:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1814782
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->i:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->getHostText()Landroid/text/SpannableStringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1814783
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    .line 1814776
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 1814777
    const-string v0, "target_fragment"

    sget-object v2, LX/0cQ;->EVENT_CREATE_HOST_SELECTION_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1814778
    const-string v0, "extra_event_host_id"

    iget-object v2, p0, Lcom/facebook/events/create/ui/EventHostSelector;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1814779
    iget-object v2, p0, Lcom/facebook/events/create/ui/EventHostSelector;->m:Lcom/facebook/content/SecureContextHelper;

    iget v3, p0, Lcom/facebook/events/create/ui/EventHostSelector;->f:I

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1814780
    return-void
.end method

.method private getHostText()Landroid/text/SpannableStringBuilder;
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "BadMethodUse-java.lang.String.length"
        }
    .end annotation

    .prologue
    const/16 v3, 0x11

    .line 1814768
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1814769
    iget-object v1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1814770
    iget-object v1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/create/ui/EventHostSelector;->j:Landroid/text/style/MetricAffectingSpan;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/create/ui/EventHostSelector;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V

    .line 1814771
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1814772
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 1814773
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1814774
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/create/ui/EventHostSelector;->k:Landroid/text/style/MetricAffectingSpan;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/create/ui/EventHostSelector;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)V

    .line 1814775
    :cond_2
    return-object v0
.end method

.method private getStartDrawable()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1814752
    iget-boolean v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->d:Z

    if-eqz v0, :cond_0

    .line 1814753
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020744

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1814754
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02089b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 1814760
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->e:Ljava/lang/String;

    .line 1814761
    iput-object p2, p0, Lcom/facebook/events/create/ui/EventHostSelector;->a:Ljava/lang/String;

    .line 1814762
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08214b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->b:Ljava/lang/String;

    .line 1814763
    iput-boolean p3, p0, Lcom/facebook/events/create/ui/EventHostSelector;->d:Z

    .line 1814764
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->getStartDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventHostSelector;->c:Landroid/graphics/drawable/Drawable;

    .line 1814765
    iput-boolean p4, p0, Lcom/facebook/events/create/ui/EventHostSelector;->g:Z

    .line 1814766
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->b()V

    .line 1814767
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x610d0ccf

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1814757
    iget-boolean v1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->g:Z

    if-eqz v1, :cond_0

    .line 1814758
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventHostSelector;->e()V

    .line 1814759
    :cond_0
    const v1, -0x75622e03

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setEventHostSelectionActivityId(I)V
    .locals 0

    .prologue
    .line 1814755
    iput p1, p0, Lcom/facebook/events/create/ui/EventHostSelector;->f:I

    .line 1814756
    return-void
.end method
