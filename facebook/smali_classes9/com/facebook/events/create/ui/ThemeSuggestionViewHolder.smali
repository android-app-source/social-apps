.class public Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final r:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public m:Lcom/facebook/resources/ui/FbTextView;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Landroid/view/View$OnClickListener;

.field public q:LX/BjB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1815118
    const-class v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->r:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1815119
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1815120
    const v0, 0x7f0d0f57

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1815121
    const v0, 0x7f0d2e3e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->m:Lcom/facebook/resources/ui/FbTextView;

    .line 1815122
    new-instance v0, LX/Bku;

    invoke-direct {v0, p0}, LX/Bku;-><init>(Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;)V

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestionViewHolder;->p:Landroid/view/View$OnClickListener;

    .line 1815123
    return-void
.end method
