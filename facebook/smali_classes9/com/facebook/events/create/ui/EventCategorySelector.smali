.class public Lcom/facebook/events/create/ui/EventCategorySelector;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/facebook/events/create/EventCompositionModel;

.field private b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private c:I

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:LX/17Y;

.field private f:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1814443
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 1814444
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->b()V

    .line 1814445
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1814446
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1814447
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->b()V

    .line 1814448
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1814449
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814450
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->b()V

    .line 1814451
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1814452
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->a:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814453
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    move-object v0, v1

    .line 1814454
    iget-object v1, v0, Lcom/facebook/events/create/ui/EventCategoryModel;->b:Ljava/lang/String;

    move-object v0, v1

    .line 1814455
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1814456
    const-string v0, ""

    invoke-direct {p0, v0}, Lcom/facebook/events/create/ui/EventCategorySelector;->setCategoryLabel(Ljava/lang/CharSequence;)V

    .line 1814457
    :goto_0
    return-void

    .line 1814458
    :cond_0
    invoke-direct {p0, v0}, Lcom/facebook/events/create/ui/EventCategorySelector;->setCategoryLabel(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(LX/17Y;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1814459
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->e:LX/17Y;

    .line 1814460
    iput-object p2, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->f:Lcom/facebook/content/SecureContextHelper;

    .line 1814461
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/create/ui/EventCategorySelector;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/create/ui/EventCategorySelector;

    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/create/ui/EventCategorySelector;->a(LX/17Y;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1814438
    const-class v0, Lcom/facebook/events/create/ui/EventCategorySelector;

    invoke-static {v0, p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1814439
    const v0, 0x7f0304ad

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1814440
    const v0, 0x7f0d0db3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1814441
    invoke-virtual {p0, p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1814442
    return-void
.end method

.method private e()V
    .locals 5

    .prologue
    .line 1814432
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->e:LX/17Y;

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/0ax;->cw:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 1814433
    const-string v0, "extra_page_id"

    iget-object v2, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1814434
    const-string v0, "extra_title_bar_content"

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082154

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1814435
    const-string v0, "extra_is_subcateory"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1814436
    iget-object v2, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->f:Lcom/facebook/content/SecureContextHelper;

    iget v3, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->c:I

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1814437
    return-void
.end method

.method private setCategoryLabel(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1814428
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1814429
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->d:Lcom/facebook/resources/ui/FbTextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1814430
    :goto_0
    return-void

    .line 1814431
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 1814410
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->a:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814411
    iget-object v1, v0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    move-object v0, v1

    .line 1814412
    const-string v1, "extra_selected_category"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1814413
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->a()V

    .line 1814414
    return-void

    .line 1814415
    :cond_1
    const-string v1, "extra_selected_category"

    invoke-static {p1, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;

    .line 1814416
    if-eqz v1, :cond_0

    .line 1814417
    invoke-virtual {v1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;->j()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/events/create/ui/EventCategoryModel;->b:Ljava/lang/String;

    .line 1814418
    invoke-virtual {v1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel$CategoriesModel;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCategoryModel;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/events/create/EventCompositionModel;Ljava/lang/String;ZI)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1814421
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->a:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814422
    iput-object p2, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->b:Ljava/lang/String;

    .line 1814423
    iput p4, p0, Lcom/facebook/events/create/ui/EventCategorySelector;->c:I

    .line 1814424
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->a()V

    .line 1814425
    if-eqz p3, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventCategorySelector;->setVisibility(I)V

    .line 1814426
    return-void

    .line 1814427
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x173c655

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1814419
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCategorySelector;->e()V

    .line 1814420
    const v1, -0x5bfabedc

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
