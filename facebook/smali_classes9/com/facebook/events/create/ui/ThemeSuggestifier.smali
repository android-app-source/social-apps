.class public Lcom/facebook/events/create/ui/ThemeSuggestifier;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# static fields
.field private static final h:[Ljava/lang/String;

.field private static final i:Ljava/lang/String;


# instance fields
.field public a:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/25T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1Ml;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final g:Landroid/net/Uri;

.field private j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

.field public k:LX/Bkr;

.field private l:Lcom/facebook/resources/ui/FbTextView;

.field public m:Z

.field private n:Z

.field private o:Landroid/animation/ValueAnimator;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1815032
    new-array v0, v5, [Ljava/lang/String;

    const-string v1, "date_added"

    aput-object v1, v0, v4

    const-string v1, "_data"

    aput-object v1, v0, v3

    sput-object v0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->h:[Ljava/lang/String;

    .line 1815033
    const-string v0, "%s = %d AND (LOWER(%s) LIKE \'%%dcim%%\' or LOWER(%s) LIKE \'%%camera%%\')"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "media_type"

    aput-object v2, v1, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const-string v2, "_data"

    aput-object v2, v1, v5

    const/4 v2, 0x3

    const-string v3, "_data"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1815027
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1815028
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->g:Landroid/net/Uri;

    .line 1815029
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->m:Z

    .line 1815030
    invoke-direct {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->b()V

    .line 1815031
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1815022
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1815023
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->g:Landroid/net/Uri;

    .line 1815024
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->m:Z

    .line 1815025
    invoke-direct {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->b()V

    .line 1815026
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1815017
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1815018
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->g:Landroid/net/Uri;

    .line 1815019
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->m:Z

    .line 1815020
    invoke-direct {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->b()V

    .line 1815021
    return-void
.end method

.method private static a(Lcom/facebook/events/create/ui/ThemeSuggestifier;LX/0tX;LX/1Ck;LX/25T;Landroid/content/Context;LX/1Ml;LX/0hB;)V
    .locals 0

    .prologue
    .line 1815016
    iput-object p1, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->a:LX/0tX;

    iput-object p2, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->b:LX/1Ck;

    iput-object p3, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->c:LX/25T;

    iput-object p4, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->d:Landroid/content/Context;

    iput-object p5, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->e:LX/1Ml;

    iput-object p6, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->f:LX/0hB;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 7

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v6

    move-object v0, p0

    check-cast v0, Lcom/facebook/events/create/ui/ThemeSuggestifier;

    invoke-static {v6}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {v6}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {v6}, LX/25T;->a(LX/0QB;)LX/25T;

    move-result-object v3

    check-cast v3, LX/25T;

    const-class v4, Landroid/content/Context;

    invoke-interface {v6, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v6}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v5

    check-cast v5, LX/1Ml;

    invoke-static {v6}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v6

    check-cast v6, LX/0hB;

    invoke-static/range {v0 .. v6}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->a(Lcom/facebook/events/create/ui/ThemeSuggestifier;LX/0tX;LX/1Ck;LX/25T;Landroid/content/Context;LX/1Ml;LX/0hB;)V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)LX/BnN;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1815005
    invoke-static {}, LX/BnP;->a()LX/BnN;

    move-result-object v0

    .line 1815006
    const-string v1, "full_width"

    iget-object v2, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->f:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1815007
    const-string v1, "full_height"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1815008
    const-string v1, "half_width"

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b155f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1815009
    const-string v1, "half_height"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1815010
    const-string v1, "count"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 1815011
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    .line 1815012
    :cond_0
    new-instance v1, LX/4EX;

    invoke-direct {v1}, LX/4EX;-><init>()V

    .line 1815013
    invoke-virtual {v1, p1}, LX/4EX;->a(Ljava/lang/String;)LX/4EX;

    move-result-object v2

    invoke-virtual {v2, p2}, LX/4EX;->b(Ljava/lang/String;)LX/4EX;

    .line 1815014
    const-string v2, "event_info"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1815015
    :cond_1
    return-object v0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 1814995
    const-class v0, Lcom/facebook/events/create/ui/ThemeSuggestifier;

    invoke-static {v0, p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1814996
    const v0, 0x7f030522

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1814997
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->setOrientation(I)V

    .line 1814998
    const v0, 0x7f0d0e92

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1814999
    invoke-direct {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->c()V

    .line 1815000
    new-instance v0, LX/Bkr;

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->getPhotoUploadOptionImageUriString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/graphics/drawable/Drawable;->createFromPath(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02066f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/Bkr;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->k:LX/Bkr;

    .line 1815001
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->c:LX/25T;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1P1;->b(I)V

    .line 1815002
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->c:LX/25T;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1815003
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    iget-object v1, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->k:LX/Bkr;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1815004
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1814954
    const v0, 0x7f0d0e93

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1814955
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->l:Lcom/facebook/resources/ui/FbTextView;

    new-instance v1, LX/Bko;

    invoke-direct {v1, p0}, LX/Bko;-><init>(Lcom/facebook/events/create/ui/ThemeSuggestifier;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1814956
    return-void
.end method

.method private getPhotoUploadOptionImageUriString()Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1814988
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->e:LX/1Ml;

    const-string v1, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1814989
    :cond_0
    :goto_0
    return-object v4

    .line 1814990
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->g:Landroid/net/Uri;

    sget-object v2, Lcom/facebook/events/create/ui/ThemeSuggestifier;->h:[Ljava/lang/String;

    sget-object v3, Lcom/facebook/events/create/ui/ThemeSuggestifier;->i:Ljava/lang/String;

    const-string v5, "date_added DESC LIMIT 1"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1814991
    if-eqz v1, :cond_0

    .line 1814992
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1814993
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    goto :goto_1

    .line 1814994
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static getThemeSuggestifierAnimator(Lcom/facebook/events/create/ui/ThemeSuggestifier;)Landroid/animation/ValueAnimator;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1814982
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->o:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 1814983
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1814984
    const/4 v1, 0x2

    new-array v1, v1, [I

    aput v2, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b155c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    aput v3, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->o:Landroid/animation/ValueAnimator;

    .line 1814985
    iget-object v1, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->o:Landroid/animation/ValueAnimator;

    new-instance v2, LX/Bkq;

    invoke-direct {v2, p0, v0}, LX/Bkq;-><init>(Lcom/facebook/events/create/ui/ThemeSuggestifier;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 1814986
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->o:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 1814987
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->o:Landroid/animation/ValueAnimator;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1814973
    iget-boolean v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->n:Z

    if-nez v0, :cond_0

    .line 1814974
    invoke-static {p0}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->getThemeSuggestifierAnimator(Lcom/facebook/events/create/ui/ThemeSuggestifier;)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 1814975
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->n:Z

    .line 1814976
    :cond_0
    invoke-static {}, LX/BnP;->a()LX/BnN;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-direct {p0, p1, p2}, Lcom/facebook/events/create/ui/ThemeSuggestifier;->b(Ljava/lang/String;Ljava/lang/String;)LX/BnN;

    move-result-object v1

    .line 1814977
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 1814978
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 1814979
    new-instance v1, LX/Bkp;

    invoke-direct {v1, p0}, LX/Bkp;-><init>(Lcom/facebook/events/create/ui/ThemeSuggestifier;)V

    .line 1814980
    iget-object v2, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->b:LX/1Ck;

    const-string v3, "FetchThemesForEvent"

    iget-object v4, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->a:LX/0tX;

    invoke-virtual {v4, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1814981
    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    const/16 v0, 0x2c

    const v1, -0xfd9ebbb

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1814965
    iget-object v1, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    if-eqz v1, :cond_0

    .line 1814966
    iget-object v1, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1814967
    iput-object v2, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->j:Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    .line 1814968
    :cond_0
    iput-object v2, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->l:Lcom/facebook/resources/ui/FbTextView;

    .line 1814969
    iput-object v2, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->k:LX/Bkr;

    .line 1814970
    iput-object v2, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->o:Landroid/animation/ValueAnimator;

    .line 1814971
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 1814972
    const/16 v1, 0x2d

    const v2, 0x750ef674

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnOptionSelectedListener(LX/BjC;)V
    .locals 1

    .prologue
    .line 1814961
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->k:LX/Bkr;

    if-eqz v0, :cond_0

    .line 1814962
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->k:LX/Bkr;

    .line 1814963
    iput-object p1, v0, LX/Bkr;->h:LX/BjC;

    .line 1814964
    :cond_0
    return-void
.end method

.method public setOnThemeSelectedListener(LX/BjB;)V
    .locals 1

    .prologue
    .line 1814957
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->k:LX/Bkr;

    if-eqz v0, :cond_0

    .line 1814958
    iget-object v0, p0, Lcom/facebook/events/create/ui/ThemeSuggestifier;->k:LX/Bkr;

    .line 1814959
    iput-object p1, v0, LX/Bkr;->g:LX/BjB;

    .line 1814960
    :cond_0
    return-void
.end method
