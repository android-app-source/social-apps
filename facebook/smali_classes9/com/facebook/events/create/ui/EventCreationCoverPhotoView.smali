.class public Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final g:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

.field private c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private d:Landroid/view/View;

.field private e:Landroid/animation/ObjectAnimator;

.field private f:LX/Bkm;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1814557
    const-class v0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    const-string v1, "event_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1814641
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1814642
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1814639
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814640
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1814635
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814636
    const-class v0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-static {v0, p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1814637
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a()V

    .line 1814638
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1814630
    const v0, 0x7f0304ba

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 1814631
    const v0, 0x7f0d0de7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1814632
    const v0, 0x7f0d0de8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->d:Landroid/view/View;

    .line 1814633
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1814634
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a:LX/0ad;

    return-void
.end method

.method public static b(Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1814615
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1814616
    iget-object v2, v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d:Landroid/net/Uri;

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1814617
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1814618
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1814619
    iget-object v3, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d:Landroid/net/Uri;

    move-object v2, v3

    .line 1814620
    sget-object v3, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1814621
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->g()Z

    move-result v2

    .line 1814622
    iget-object v3, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    if-eqz v2, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1814623
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a:LX/0ad;

    sget-short v2, LX/Bjt;->a:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1814624
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1814625
    :cond_0
    return-void

    .line 1814626
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1814627
    iget-object v3, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    move-object v2, v3

    .line 1814628
    sget-object v3, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1

    .line 1814629
    :cond_2
    const/16 v0, 0x8

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 1814598
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1814599
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->h()V

    .line 1814600
    if-eqz p1, :cond_0

    .line 1814601
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->e:Landroid/animation/ObjectAnimator;

    new-instance v1, LX/Bkc;

    invoke-direct {v1, p0}, LX/Bkc;-><init>(Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1814602
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 1814603
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->f:LX/Bkm;

    .line 1814604
    iget-object v1, v0, LX/Bkm;->b:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    const/4 p0, 0x1

    iget p1, v0, LX/Bkm;->g:I

    invoke-virtual {v1, p0, p1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(ZI)V

    .line 1814605
    invoke-static {v0}, LX/Bkm;->d(LX/Bkm;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->reverse()V

    .line 1814606
    invoke-static {v0}, LX/Bkm;->e(LX/Bkm;)Landroid/animation/ValueAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->reverse()V

    .line 1814607
    :goto_0
    return-void

    .line 1814608
    :cond_0
    invoke-static {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b(Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;)V

    .line 1814609
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1814610
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->f:LX/Bkm;

    .line 1814611
    iget-object v1, v0, LX/Bkm;->b:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    const/4 p0, 0x0

    iget p1, v0, LX/Bkm;->g:I

    invoke-virtual {v1, p0, p1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(ZI)V

    .line 1814612
    invoke-static {v0}, LX/Bkm;->d(LX/Bkm;)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 1814613
    invoke-static {v0}, LX/Bkm;->e(LX/Bkm;)Landroid/animation/ValueAnimator;

    move-result-object v1

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->start()V

    .line 1814614
    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 1814597
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->g()Z

    move-result v0

    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->f()Z

    move-result v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 1814596
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 1814595
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 1814590
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->e:Landroid/animation/ObjectAnimator;

    if-nez v0, :cond_0

    .line 1814591
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->c:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const-string v1, "alpha"

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->e:Landroid/animation/ObjectAnimator;

    .line 1814592
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->e:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1814593
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->e:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    .line 1814594
    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 1814568
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    const/4 v3, 0x0

    .line 1814569
    sparse-switch p1, :sswitch_data_0

    .line 1814570
    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Z)V

    .line 1814571
    return-void

    .line 1814572
    :sswitch_0
    const-string v1, "photo"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1814573
    const-string v1, "photo"

    invoke-static {p2, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    .line 1814574
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1814575
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a()V

    .line 1814576
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->K()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    .line 1814577
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-static {v1}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    goto :goto_0

    .line 1814578
    :sswitch_1
    const-string v1, "extra_media_items"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1814579
    const-string v1, "extra_media_items"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1814580
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1814581
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a()V

    .line 1814582
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaItem;->f()Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    goto :goto_0

    .line 1814583
    :sswitch_2
    const-string v1, "extra_selected_theme_uri"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "extra_selected_theme_id"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1814584
    const-string v1, "extra_selected_theme_uri"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1814585
    const-string v2, "extra_selected_theme_id"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1814586
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1814587
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a()V

    .line 1814588
    iput-object v2, v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    .line 1814589
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x67 -> :sswitch_1
        0x68 -> :sswitch_2
        0x26b9 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Lcom/facebook/events/create/ui/EventCoverPhotoModel;LX/Bkm;)V
    .locals 1

    .prologue
    .line 1814564
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1814565
    iput-object p2, p0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->f:LX/Bkm;

    .line 1814566
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Z)V

    .line 1814567
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 1814558
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1814559
    :cond_0
    invoke-static {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b(Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;)V

    .line 1814560
    :goto_0
    return-void

    .line 1814561
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1814562
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b(Z)V

    goto :goto_0

    .line 1814563
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->b(Z)V

    goto :goto_0
.end method
