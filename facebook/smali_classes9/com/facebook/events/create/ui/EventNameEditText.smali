.class public Lcom/facebook/events/create/ui/EventNameEditText;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""


# instance fields
.field public b:LX/Bjk;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/events/create/EventCompositionModel;

.field public d:I

.field private e:Z

.field private f:I

.field public g:LX/BjE;

.field public h:LX/Bii;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1814866
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 1814867
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->d:I

    .line 1814868
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->e:Z

    .line 1814869
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventNameEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->f:I

    .line 1814870
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventNameEditText;->b()V

    .line 1814871
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1814860
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1814861
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->d:I

    .line 1814862
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->e:Z

    .line 1814863
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventNameEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->f:I

    .line 1814864
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventNameEditText;->b()V

    .line 1814865
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1814854
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1814855
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->d:I

    .line 1814856
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->e:Z

    .line 1814857
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventNameEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a010c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->f:I

    .line 1814858
    invoke-direct {p0}, Lcom/facebook/events/create/ui/EventNameEditText;->b()V

    .line 1814859
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/create/ui/EventNameEditText;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-static {v0}, LX/Bjk;->a(LX/0QB;)LX/Bjk;

    move-result-object v0

    check-cast v0, LX/Bjk;

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->b:LX/Bjk;

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1814849
    const-class v0, Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-static {v0, p0}, Lcom/facebook/events/create/ui/EventNameEditText;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1814850
    invoke-virtual {p0}, Lcom/facebook/events/create/ui/EventNameEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1550

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-static {p0, v2, v2, v0, v2}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 1814851
    iget v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->f:I

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventNameEditText;->setTextColor(I)V

    .line 1814852
    new-instance v0, LX/Bkk;

    invoke-direct {v0, p0}, LX/Bkk;-><init>(Lcom/facebook/events/create/ui/EventNameEditText;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventNameEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1814853
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1814847
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->g:LX/BjE;

    .line 1814848
    return-void
.end method

.method public final a(Lcom/facebook/events/create/EventCompositionModel;)V
    .locals 1

    .prologue
    .line 1814872
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventNameEditText;->c:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814873
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->c:Lcom/facebook/events/create/EventCompositionModel;

    .line 1814874
    iget-object p1, v0, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    move-object v0, p1

    .line 1814875
    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventNameEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1814876
    return-void
.end method

.method public getLongestEverMaxLength()I
    .locals 1

    .prologue
    .line 1814846
    iget v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->d:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 1814838
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onDraw(Landroid/graphics/Canvas;)V

    .line 1814839
    iget-boolean v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->e:Z

    if-eqz v0, :cond_0

    .line 1814840
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->b:LX/Bjk;

    .line 1814841
    iget-object v1, v0, LX/Bjk;->b:Lcom/facebook/performancelogger/PerformanceLogger;

    iget-object p1, v0, LX/Bjk;->a:LX/0Yj;

    invoke-interface {v1, p1}, Lcom/facebook/performancelogger/PerformanceLogger;->b(LX/0Yj;)V

    .line 1814842
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->e:Z

    .line 1814843
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->h:LX/Bii;

    if-eqz v0, :cond_1

    .line 1814844
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->h:LX/Bii;

    invoke-interface {v0, p0}, LX/Bii;->a(Lcom/facebook/events/create/ui/EventNameEditText;)V

    .line 1814845
    :cond_1
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1814831
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Landroid/os/Bundle;

    if-eq v0, v1, :cond_0

    .line 1814832
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1814833
    :goto_0
    return-void

    .line 1814834
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 1814835
    const-string v0, "eventNameEditTextSuperState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1814836
    const-string v0, "eventNameTextColor"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventNameEditText;->setEventNameTextColor(I)V

    .line 1814837
    const-string v0, "maxLengthEver"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->d:I

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 1814825
    invoke-super {p0}, Lcom/facebook/resources/ui/FbEditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1814826
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1814827
    const-string v2, "eventNameEditTextSuperState"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1814828
    const-string v0, "eventNameTextColor"

    iget v2, p0, Lcom/facebook/events/create/ui/EventNameEditText;->f:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1814829
    const-string v0, "maxLengthEver"

    iget v2, p0, Lcom/facebook/events/create/ui/EventNameEditText;->d:I

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1814830
    return-object v1
.end method

.method public setEventNameTextColor(I)V
    .locals 1

    .prologue
    .line 1814822
    iput p1, p0, Lcom/facebook/events/create/ui/EventNameEditText;->f:I

    .line 1814823
    iget v0, p0, Lcom/facebook/events/create/ui/EventNameEditText;->f:I

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/ui/EventNameEditText;->setTextColor(I)V

    .line 1814824
    return-void
.end method

.method public setOnDrawListener(LX/Bii;)V
    .locals 0

    .prologue
    .line 1814820
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventNameEditText;->h:LX/Bii;

    .line 1814821
    return-void
.end method

.method public setOnEventNameEditedListener(LX/BjE;)V
    .locals 0

    .prologue
    .line 1814818
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventNameEditText;->g:LX/BjE;

    .line 1814819
    return-void
.end method
