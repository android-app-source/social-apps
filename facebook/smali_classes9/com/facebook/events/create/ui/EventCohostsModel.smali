.class public Lcom/facebook/events/create/ui/EventCohostsModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/create/ui/EventCohostsModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1814474
    new-instance v0, LX/BkX;

    invoke-direct {v0}, LX/BkX;-><init>()V

    sput-object v0, Lcom/facebook/events/create/ui/EventCohostsModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1814475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1814476
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1814477
    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    .line 1814478
    return-void
.end method

.method public constructor <init>(LX/0Px;Ljava/lang/String;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 1814479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1814480
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1814481
    iput-object v0, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    .line 1814482
    iput-object p1, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    .line 1814483
    iput-object p2, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->b:Ljava/lang/String;

    .line 1814484
    iput p3, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->c:I

    .line 1814485
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1814486
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 1814487
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1814488
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1814489
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1814490
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1814491
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1814492
    iget v0, p0, Lcom/facebook/events/create/ui/EventCohostsModel;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1814493
    return-void
.end method
