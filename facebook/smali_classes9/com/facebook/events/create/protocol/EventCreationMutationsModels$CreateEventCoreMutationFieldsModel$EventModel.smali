.class public final Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813101
    const-class v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813110
    const-class v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1813108
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1813109
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1813102
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813103
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1813104
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1813105
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1813106
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813107
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1813087
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813088
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813089
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1813100
    new-instance v0, LX/Bk5;

    invoke-direct {v0, p1}, LX/Bk5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813111
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1813098
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1813099
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1813097
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1813094
    new-instance v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;

    invoke-direct {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;-><init>()V

    .line 1813095
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1813096
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1813093
    const v0, 0x6081a6ae

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1813092
    const v0, 0x403827a

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813090
    iget-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;->e:Ljava/lang/String;

    .line 1813091
    iget-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$CreateEventCoreMutationFieldsModel$EventModel;->e:Ljava/lang/String;

    return-object v0
.end method
