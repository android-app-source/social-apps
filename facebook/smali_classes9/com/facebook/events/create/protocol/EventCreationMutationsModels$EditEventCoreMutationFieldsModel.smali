.class public final Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x35d62b37
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813260
    const-class v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813236
    const-class v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1813237
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1813238
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1813239
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813240
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->a()Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1813241
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1813242
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1813243
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813244
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1813245
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813246
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->a()Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1813247
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->a()Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    .line 1813248
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->a()Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1813249
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;

    .line 1813250
    iput-object v0, v1, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->e:Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    .line 1813251
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813252
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813253
    iget-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->e:Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    iput-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->e:Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    .line 1813254
    iget-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;->e:Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1813255
    new-instance v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;

    invoke-direct {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel;-><init>()V

    .line 1813256
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1813257
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1813258
    const v0, 0xb7f1433

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1813259
    const v0, -0x323b5237    # -4.1246544E8f

    return v0
.end method
