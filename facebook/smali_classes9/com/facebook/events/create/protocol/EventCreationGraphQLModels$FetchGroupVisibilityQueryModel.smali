.class public final Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x714ccae2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813014
    const-class v0, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813013
    const-class v0, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1813011
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1813012
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V
    .locals 4

    .prologue
    .line 1813004
    iput-object p1, p0, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1813005
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1813006
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1813007
    if-eqz v0, :cond_0

    .line 1813008
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1813009
    :cond_0
    return-void

    .line 1813010
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1812978
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1812979
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v0

    .line 1812980
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1812981
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1812982
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1812983
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1813001
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813002
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813003
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1813000
    new-instance v0, LX/Bjz;

    invoke-direct {v0, p1}, LX/Bjz;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1812998
    iget-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 1812999
    iget-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->e:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1812992
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1812993
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->a()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1812994
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1812995
    const/4 v0, 0x0

    iput v0, p2, LX/18L;->c:I

    .line 1812996
    :goto_0
    return-void

    .line 1812997
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1812989
    const-string v0, "visibility"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1812990
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {p0, p2}, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;->a(Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    .line 1812991
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1812986
    new-instance v0, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;

    invoke-direct {v0}, Lcom/facebook/events/create/protocol/EventCreationGraphQLModels$FetchGroupVisibilityQueryModel;-><init>()V

    .line 1812987
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1812988
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1812985
    const v0, -0x14852d5d

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1812984
    const v0, 0x41e065f

    return v0
.end method
