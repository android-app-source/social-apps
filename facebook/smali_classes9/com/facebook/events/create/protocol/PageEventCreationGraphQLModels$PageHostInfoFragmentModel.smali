.class public final Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x1e551e9e
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813853
    const-class v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813852
    const-class v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1813850
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1813851
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1813836
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813837
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x73801991

    invoke-static {v1, v0, v2}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1813838
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x7842f8ef

    invoke-static {v2, v1, v3}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1813839
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1813840
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->m()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 1813841
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-static {p1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 1813842
    const/4 v5, 0x5

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 1813843
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v0}, LX/186;->b(II)V

    .line 1813844
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1813845
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1813846
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1813847
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1813848
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813849
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1813815
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813816
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1813817
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x73801991

    invoke-static {v2, v0, v3}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1813818
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1813819
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1813820
    iput v3, v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->e:I

    move-object v1, v0

    .line 1813821
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1813822
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x7842f8ef

    invoke-static {v2, v0, v3}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1813823
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1813824
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1813825
    iput v3, v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->f:I

    move-object v1, v0

    .line 1813826
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1813827
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1813828
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1813829
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    .line 1813830
    iput-object v0, v1, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1813831
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813832
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1813833
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 1813834
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_3
    move-object p0, v1

    .line 1813835
    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1813814
    new-instance v0, LX/BkG;

    invoke-direct {v0, p1}, LX/BkG;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813813
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->l()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1813809
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1813810
    const/4 v0, 0x0

    const v1, -0x73801991

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->e:I

    .line 1813811
    const/4 v0, 0x1

    const v1, 0x7842f8ef

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->f:I

    .line 1813812
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1813854
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1813855
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1813808
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1813793
    new-instance v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;

    invoke-direct {v0}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;-><init>()V

    .line 1813794
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1813795
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1813807
    const v0, -0x494e2833

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1813806
    const v0, 0x25d6af

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhoto"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1813804
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1813805
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getEventCategoryGroups"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813802
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1813803
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813800
    iget-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->g:Ljava/lang/String;

    .line 1813801
    iget-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813798
    iget-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->h:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->h:Ljava/lang/String;

    .line 1813799
    iget-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813796
    iget-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    const/4 v1, 0x4

    const-class v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    iput-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 1813797
    iget-object v0, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel;->i:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    return-object v0
.end method
