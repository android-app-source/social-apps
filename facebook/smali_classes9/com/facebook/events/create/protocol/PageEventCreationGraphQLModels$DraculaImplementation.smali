.class public final Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1813413
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1813414
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1813337
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1813338
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1813392
    if-nez p1, :cond_0

    move v0, v1

    .line 1813393
    :goto_0
    return v0

    .line 1813394
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1813395
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1813396
    :sswitch_0
    const-class v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1813397
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1813398
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1813399
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1813400
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1813401
    :sswitch_1
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1813402
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1813403
    const-class v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v4, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1813404
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1813405
    const/4 v3, 0x2

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1813406
    invoke-virtual {p3, v1, v2}, LX/186;->b(II)V

    .line 1813407
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1813408
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1813409
    :sswitch_2
    invoke-virtual {p0, p1, v1, v1}, LX/15i;->a(III)I

    move-result v0

    .line 1813410
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1813411
    invoke-virtual {p3, v1, v0, v1}, LX/186;->a(III)V

    .line 1813412
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x73801991 -> :sswitch_1
        0x1d4fc22c -> :sswitch_0
        0x7842f8ef -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1813391
    new-instance v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1813387
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1813388
    if-eqz v0, :cond_0

    .line 1813389
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1813390
    :cond_0
    return-void
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1813382
    if-eqz p0, :cond_0

    .line 1813383
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1813384
    if-eq v0, p0, :cond_0

    .line 1813385
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1813386
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1813373
    sparse-switch p2, :sswitch_data_0

    .line 1813374
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1813375
    :sswitch_0
    const-class v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageEventCategoriesQueryModel$EventCategoryGroupsModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1813376
    invoke-static {v0, p3}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1813377
    :goto_0
    :sswitch_1
    return-void

    .line 1813378
    :sswitch_2
    const-class v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultVect2FieldsModel;

    .line 1813379
    invoke-static {v0, p3}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1813380
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$PageHostInfoFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1813381
    invoke-static {v0, p3}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x73801991 -> :sswitch_2
        0x1d4fc22c -> :sswitch_0
        0x7842f8ef -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1813372
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1813370
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1813371
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1813365
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1813366
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1813367
    :cond_0
    iput-object p1, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1813368
    iput p2, p0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->b:I

    .line 1813369
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1813364
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1813363
    new-instance v0, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1813360
    iget v0, p0, LX/1vt;->c:I

    .line 1813361
    move v0, v0

    .line 1813362
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1813357
    iget v0, p0, LX/1vt;->c:I

    .line 1813358
    move v0, v0

    .line 1813359
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1813354
    iget v0, p0, LX/1vt;->b:I

    .line 1813355
    move v0, v0

    .line 1813356
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813351
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1813352
    move-object v0, v0

    .line 1813353
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1813342
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1813343
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1813344
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1813345
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1813346
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1813347
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1813348
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1813349
    invoke-static {v3, v9, v2}, Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/create/protocol/PageEventCreationGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1813350
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1813339
    iget v0, p0, LX/1vt;->c:I

    .line 1813340
    move v0, v0

    .line 1813341
    return v0
.end method
