.class public final Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x6b5e2fe2
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813222
    const-class v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1813221
    const-class v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1813219
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1813220
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1813213
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813214
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1813215
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1813216
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1813217
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813218
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 0

    .prologue
    .line 1813198
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1813199
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1813200
    return-object p0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1813212
    new-instance v0, LX/Bk6;

    invoke-direct {v0, p1}, LX/Bk6;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813211
    invoke-virtual {p0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 0

    .prologue
    .line 1813209
    invoke-virtual {p2}, LX/18L;->a()V

    .line 1813210
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 0

    .prologue
    .line 1813208
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1813205
    new-instance v0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;

    invoke-direct {v0}, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;-><init>()V

    .line 1813206
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1813207
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1813204
    const v0, -0x6402d057

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1813203
    const v0, 0x403827a

    return v0
.end method

.method public final j()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1813201
    iget-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->e:Ljava/lang/String;

    .line 1813202
    iget-object v0, p0, Lcom/facebook/events/create/protocol/EventCreationMutationsModels$EditEventCoreMutationFieldsModel$EventModel;->e:Ljava/lang/String;

    return-object v0
.end method
