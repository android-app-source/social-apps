.class public final Lcom/facebook/events/create/EventCoverPhotoUploadHandler$1$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/photos/upload/operation/UploadRecord;

.field public final synthetic b:LX/BiZ;


# direct methods
.method public constructor <init>(LX/BiZ;Lcom/facebook/photos/upload/operation/UploadRecord;)V
    .locals 0

    .prologue
    .line 1810529
    iput-object p1, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$1$1;->b:LX/BiZ;

    iput-object p2, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$1$1;->a:Lcom/facebook/photos/upload/operation/UploadRecord;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1810530
    iget-object v0, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$1$1;->b:LX/BiZ;

    iget-object v1, v0, LX/BiZ;->e:LX/Bib;

    iget-object v0, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$1$1;->b:LX/BiZ;

    iget-wide v2, v0, LX/BiZ;->b:J

    iget-object v0, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$1$1;->a:Lcom/facebook/photos/upload/operation/UploadRecord;

    iget-wide v4, v0, Lcom/facebook/photos/upload/operation/UploadRecord;->fbid:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$1$1;->b:LX/BiZ;

    iget-object v5, v0, LX/BiZ;->c:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v0, p0, Lcom/facebook/events/create/EventCoverPhotoUploadHandler$1$1;->b:LX/BiZ;

    iget-object v6, v0, LX/BiZ;->d:Lcom/facebook/events/common/ActionMechanism;

    .line 1810531
    new-instance v0, LX/4Dr;

    invoke-direct {v0}, LX/4Dr;-><init>()V

    .line 1810532
    invoke-virtual {v0, v4}, LX/4Dr;->a(Ljava/lang/String;)LX/4Dr;

    .line 1810533
    new-instance v7, LX/4EG;

    invoke-direct {v7}, LX/4EG;-><init>()V

    .line 1810534
    iget-object v8, v5, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v7, v8}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1810535
    iget-object v8, v5, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    if-nez v8, :cond_0

    .line 1810536
    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    .line 1810537
    :goto_0
    new-instance v8, LX/4EL;

    invoke-direct {v8}, LX/4EL;-><init>()V

    .line 1810538
    invoke-virtual {v8, v7}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1810539
    new-instance v7, LX/4EN;

    invoke-direct {v7}, LX/4EN;-><init>()V

    invoke-virtual {v7, v8}, LX/4EN;->a(LX/4EL;)LX/4EN;

    move-result-object v7

    move-object v7, v7

    .line 1810540
    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/4EN;->a(Ljava/lang/String;)LX/4EN;

    move-result-object v8

    invoke-virtual {v8, v0}, LX/4EN;->a(LX/4Dr;)LX/4EN;

    .line 1810541
    invoke-static {}, LX/Bk4;->b()LX/Bk3;

    move-result-object v0

    const-string v8, "input"

    invoke-virtual {v0, v8, v7}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Bk3;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1810542
    iget-object v7, v1, LX/Bib;->e:LX/0tX;

    invoke-virtual {v7, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1810543
    new-instance v7, LX/Bia;

    invoke-direct {v7, v1, v2, v3}, LX/Bia;-><init>(LX/Bib;J)V

    .line 1810544
    iget-object v8, v1, LX/Bib;->i:LX/1Ck;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string p0, "tasks-editEvent:"

    invoke-direct {v9, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9, v0, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1810545
    return-void

    .line 1810546
    :cond_0
    new-instance v8, LX/4EG;

    invoke-direct {v8}, LX/4EG;-><init>()V

    .line 1810547
    iget-object v9, v5, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v8, v9}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1810548
    if-eqz v6, :cond_1

    .line 1810549
    invoke-virtual {v6}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1810550
    :cond_1
    invoke-static {v8, v7}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    goto :goto_0
.end method
