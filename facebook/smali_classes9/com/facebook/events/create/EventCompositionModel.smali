.class public Lcom/facebook/events/create/EventCompositionModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/create/EventCompositionModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/events/create/EventCompositionModel;

.field private b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/events/model/PrivacyType;

.field private f:Z

.field public g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

.field public h:Lcom/facebook/events/ui/date/EventTimeModel;

.field public i:Lcom/facebook/events/ui/location/EventLocationModel;

.field public j:Lcom/facebook/events/create/ui/EventCohostsModel;

.field public k:Lcom/facebook/events/create/ui/EventCategoryModel;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field public n:LX/03R;

.field public o:Z

.field public p:Z

.field public q:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1810511
    new-instance v0, LX/BiX;

    invoke-direct {v0}, LX/BiX;-><init>()V

    sput-object v0, Lcom/facebook/events/create/EventCompositionModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1810503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810504
    new-instance v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1810505
    new-instance v0, Lcom/facebook/events/ui/date/EventTimeModel;

    invoke-direct {v0}, Lcom/facebook/events/ui/date/EventTimeModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1810506
    new-instance v0, Lcom/facebook/events/ui/location/EventLocationModel;

    invoke-direct {v0}, Lcom/facebook/events/ui/location/EventLocationModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1810507
    new-instance v0, Lcom/facebook/events/create/ui/EventCohostsModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCohostsModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1810508
    new-instance v0, Lcom/facebook/events/create/ui/EventCategoryModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCategoryModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    .line 1810509
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    .line 1810510
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1810389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810390
    new-instance v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1810391
    new-instance v0, Lcom/facebook/events/ui/date/EventTimeModel;

    invoke-direct {v0}, Lcom/facebook/events/ui/date/EventTimeModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1810392
    new-instance v0, Lcom/facebook/events/ui/location/EventLocationModel;

    invoke-direct {v0}, Lcom/facebook/events/ui/location/EventLocationModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1810393
    new-instance v0, Lcom/facebook/events/create/ui/EventCohostsModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCohostsModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1810394
    new-instance v0, Lcom/facebook/events/create/ui/EventCategoryModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCategoryModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    .line 1810395
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    .line 1810396
    const-class v0, Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/EventCompositionModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    .line 1810397
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->b:Ljava/lang/String;

    .line 1810398
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    .line 1810399
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    .line 1810400
    const-class v0, Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyType;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->e:Lcom/facebook/events/model/PrivacyType;

    .line 1810401
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCompositionModel;->f:Z

    .line 1810402
    const-class v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1810403
    const-class v0, Lcom/facebook/events/ui/date/EventTimeModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/EventTimeModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1810404
    const-class v0, Lcom/facebook/events/ui/location/EventLocationModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/location/EventLocationModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1810405
    const-class v0, Lcom/facebook/events/create/ui/EventCohostsModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventCohostsModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1810406
    const-class v0, Lcom/facebook/events/create/ui/EventCategoryModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventCategoryModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    .line 1810407
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->l:Ljava/lang/String;

    .line 1810408
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->m:Ljava/lang/String;

    .line 1810409
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    .line 1810410
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    .line 1810411
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventCompositionModel;->p:Z

    .line 1810412
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/create/EventCompositionModel;->q:J

    .line 1810413
    return-void
.end method

.method public constructor <init>(Lcom/facebook/events/model/Event;)V
    .locals 1

    .prologue
    .line 1810513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1810514
    new-instance v0, Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1810515
    new-instance v0, Lcom/facebook/events/ui/date/EventTimeModel;

    invoke-direct {v0}, Lcom/facebook/events/ui/date/EventTimeModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1810516
    new-instance v0, Lcom/facebook/events/ui/location/EventLocationModel;

    invoke-direct {v0}, Lcom/facebook/events/ui/location/EventLocationModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1810517
    new-instance v0, Lcom/facebook/events/create/ui/EventCohostsModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCohostsModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1810518
    new-instance v0, Lcom/facebook/events/create/ui/EventCategoryModel;

    invoke-direct {v0}, Lcom/facebook/events/create/ui/EventCategoryModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    .line 1810519
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    .line 1810520
    invoke-virtual {p0, p1}, Lcom/facebook/events/create/EventCompositionModel;->a(Lcom/facebook/events/model/Event;)V

    .line 1810521
    return-void
.end method

.method public static a(LX/0Px;LX/0Px;)LX/0Px;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1810512
    invoke-static {p0}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    invoke-static {p1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v1

    invoke-static {v0, v1}, LX/0RA;->c(Ljava/util/Set;Ljava/util/Set;)LX/0Ro;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/03R;)Lcom/facebook/events/create/EventCompositionModel;
    .locals 0

    .prologue
    .line 1810494
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1810495
    iput-object p1, p0, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    .line 1810496
    return-object p0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 1810497
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 1810498
    invoke-virtual {v1, p0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1810499
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 1810500
    const-class v0, Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/EventCompositionModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    .line 1810501
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 1810502
    return-void
.end method

.method public final a(Lcom/facebook/events/model/Event;)V
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    .line 1810441
    iget-object v2, p1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v2, v2

    .line 1810442
    iput-object v2, p0, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    .line 1810443
    iget-object v2, p1, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v2, v2

    .line 1810444
    iput-object v2, p0, Lcom/facebook/events/create/EventCompositionModel;->e:Lcom/facebook/events/model/PrivacyType;

    .line 1810445
    iget-object v2, p1, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v2, v2

    .line 1810446
    invoke-static {v2}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    .line 1810447
    iget-object v2, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    .line 1810448
    iget-object v3, p1, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    move-object v3, v3

    .line 1810449
    iput-object v3, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    .line 1810450
    move-object v2, v2

    .line 1810451
    iget-object v3, p1, Lcom/facebook/events/model/Event;->X:Landroid/net/Uri;

    move-object v3, v3

    .line 1810452
    iput-object v3, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    .line 1810453
    move-object v2, v2

    .line 1810454
    iget-object v3, p1, Lcom/facebook/events/model/Event;->Y:Landroid/net/Uri;

    move-object v3, v3

    .line 1810455
    iput-object v3, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d:Landroid/net/Uri;

    .line 1810456
    iget-object v2, p0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    .line 1810457
    iget-object v3, p1, Lcom/facebook/events/model/Event;->ak:Ljava/lang/String;

    move-object v3, v3

    .line 1810458
    iput-object v3, v2, Lcom/facebook/events/create/ui/EventCategoryModel;->b:Ljava/lang/String;

    .line 1810459
    move-object v2, v2

    .line 1810460
    iget-object v3, p1, Lcom/facebook/events/model/Event;->al:Ljava/lang/String;

    move-object v3, v3

    .line 1810461
    iput-object v3, v2, Lcom/facebook/events/create/ui/EventCategoryModel;->a:Ljava/lang/String;

    .line 1810462
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_1

    move-wide v4, v0

    .line 1810463
    :goto_0
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_2

    move-wide v6, v0

    .line 1810464
    :goto_1
    iget-object v1, p0, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1810465
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->N:Z

    move v2, v0

    .line 1810466
    iget-object v0, p1, Lcom/facebook/events/model/Event;->M:Ljava/util/TimeZone;

    move-object v3, v0

    .line 1810467
    invoke-virtual/range {v1 .. v7}, Lcom/facebook/events/ui/date/EventTimeModel;->a(ZLjava/util/TimeZone;JJ)Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1810468
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1810469
    iget-wide v8, p1, Lcom/facebook/events/model/Event;->P:J

    move-wide v2, v8

    .line 1810470
    iput-wide v2, v0, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    .line 1810471
    move-object v0, v0

    .line 1810472
    iget-object v1, p1, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v1, v1

    .line 1810473
    iput-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    .line 1810474
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1810475
    iget-object v1, p1, Lcom/facebook/events/model/Event;->q:LX/0Px;

    move-object v1, v1

    .line 1810476
    if-nez v1, :cond_0

    .line 1810477
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1810478
    :cond_0
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    .line 1810479
    move-object v0, v0

    .line 1810480
    iget-object v1, p1, Lcom/facebook/events/model/Event;->o:Ljava/lang/String;

    move-object v1, v1

    .line 1810481
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->b:Ljava/lang/String;

    .line 1810482
    move-object v0, v0

    .line 1810483
    iget v1, p1, Lcom/facebook/events/model/Event;->p:I

    move v1, v1

    .line 1810484
    iput v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->c:I

    .line 1810485
    iget-object v0, p1, Lcom/facebook/events/model/Event;->k:LX/03R;

    move-object v0, v0

    .line 1810486
    iput-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    .line 1810487
    iget-boolean v0, p1, Lcom/facebook/events/model/Event;->j:Z

    move v0, v0

    .line 1810488
    iput-boolean v0, p0, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    .line 1810489
    iget-wide v8, p1, Lcom/facebook/events/model/Event;->A:J

    move-wide v0, v8

    .line 1810490
    iput-wide v0, p0, Lcom/facebook/events/create/EventCompositionModel;->q:J

    .line 1810491
    return-void

    .line 1810492
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->M()J

    move-result-wide v4

    goto :goto_0

    .line 1810493
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/events/model/Event;->O()J

    move-result-wide v6

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 1810440
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1810439
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 1810438
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 1810432
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1810433
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    iget-object v0, v0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1810434
    iget-object v1, v0, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    move-object v0, v1

    .line 1810435
    iget-object v1, p0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1810436
    iget-object p0, v1, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    move-object v1, p0

    .line 1810437
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1810414
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1810415
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1810416
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1810417
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1810418
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->e:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1810419
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCompositionModel;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1810420
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1810421
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1810422
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1810423
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1810424
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1810425
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1810426
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->m:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1810427
    iget-object v0, p0, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1810428
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1810429
    iget-boolean v0, p0, Lcom/facebook/events/create/EventCompositionModel;->p:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1810430
    iget-wide v0, p0, Lcom/facebook/events/create/EventCompositionModel;->q:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1810431
    return-void
.end method
