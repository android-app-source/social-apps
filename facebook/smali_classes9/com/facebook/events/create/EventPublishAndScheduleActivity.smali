.class public Lcom/facebook/events/create/EventPublishAndScheduleActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field private p:Lcom/facebook/widget/text/BetterTextView;

.field private q:Lcom/facebook/widget/text/BetterTextView;

.field private r:Lcom/facebook/widget/text/BetterTextView;

.field private s:Z

.field public t:J

.field public u:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1812682
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1812643
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1812644
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1812645
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setShowDividers(Z)V

    .line 1812646
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1812647
    new-instance v1, LX/BjW;

    invoke-direct {v1, p0}, LX/BjW;-><init>(Lcom/facebook/events/create/EventPublishAndScheduleActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1812648
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/create/EventPublishAndScheduleActivity;ZJ)V
    .locals 4

    .prologue
    .line 1812675
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1812676
    const-string v1, "extra_save_as_draft"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1812677
    if-eqz p1, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v1, p2, v2

    if-eqz v1, :cond_0

    .line 1812678
    const-string v1, "extra_scheduled_publish_time"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1812679
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->setResult(ILandroid/content/Intent;)V

    .line 1812680
    invoke-virtual {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->finish()V

    .line 1812681
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1812672
    const v0, 0x7f0d0e47

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1812673
    iget-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->p:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/BjX;

    invoke-direct {v1, p0}, LX/BjX;-><init>(Lcom/facebook/events/create/EventPublishAndScheduleActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1812674
    return-void
.end method

.method public static b(Lcom/facebook/events/create/EventPublishAndScheduleActivity;Z)V
    .locals 2

    .prologue
    .line 1812683
    const-wide/16 v0, -0x1

    invoke-static {p0, p1, v0, v1}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->a$redex0(Lcom/facebook/events/create/EventPublishAndScheduleActivity;ZJ)V

    .line 1812684
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1812669
    const v0, 0x7f0d0e48

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 1812670
    iget-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->q:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Bja;

    invoke-direct {v1, p0}, LX/Bja;-><init>(Lcom/facebook/events/create/EventPublishAndScheduleActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1812671
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1812666
    const v0, 0x7f0d0e49

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->r:Lcom/facebook/widget/text/BetterTextView;

    .line 1812667
    iget-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->r:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/Bjb;

    invoke-direct {v1, p0}, LX/Bjb;-><init>(Lcom/facebook/events/create/EventPublishAndScheduleActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1812668
    return-void
.end method

.method private n()V
    .locals 4

    .prologue
    .line 1812660
    iget-boolean v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->s:Z

    if-nez v0, :cond_0

    .line 1812661
    iget-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1812662
    :goto_0
    return-void

    .line 1812663
    :cond_0
    iget-wide v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->t:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1812664
    iget-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->q:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0

    .line 1812665
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->r:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 6
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, -0x1

    .line 1812649
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1812650
    const v0, 0x7f030507

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->setContentView(I)V

    .line 1812651
    invoke-direct {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->a()V

    .line 1812652
    invoke-virtual {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_save_as_draft"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->s:Z

    .line 1812653
    invoke-virtual {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_scheduled_publish_time"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->t:J

    .line 1812654
    invoke-virtual {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_event_start_time"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->u:J

    .line 1812655
    invoke-direct {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->b()V

    .line 1812656
    invoke-direct {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->l()V

    .line 1812657
    invoke-direct {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->m()V

    .line 1812658
    invoke-direct {p0}, Lcom/facebook/events/create/EventPublishAndScheduleActivity;->n()V

    .line 1812659
    return-void
.end method
