.class public Lcom/facebook/events/create/EventEditNikumanActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""

# interfaces
.implements LX/0f2;


# instance fields
.field public A:LX/0SI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Or;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/BkU;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:Lcom/facebook/events/create/EventCompositionModel;

.field public J:Lcom/facebook/events/model/Event;

.field public K:Lcom/facebook/events/model/Event;

.field private L:Ljava/lang/String;

.field private M:Lcom/facebook/events/common/EventAnalyticsParams;

.field private N:Ljava/lang/String;

.field private O:Lcom/facebook/events/common/ActionMechanism;

.field private P:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

.field private Q:LX/0h5;

.field private R:Landroid/widget/FrameLayout;

.field private S:Lcom/facebook/events/create/ui/EventNameEditText;

.field private T:Lcom/facebook/events/create/ui/EventDescriptionText;

.field private U:Lcom/facebook/events/ui/location/LocationNikumanPicker;

.field private V:Lcom/facebook/events/create/ui/EventCategorySelector;

.field private W:Landroid/widget/FrameLayout;

.field private X:Lcom/facebook/widget/text/BetterEditTextView;

.field private Y:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

.field public Z:Lcom/facebook/events/create/ui/CoverPhotoSelector;

.field private aa:Lcom/facebook/events/create/ui/CohostsSelector;

.field public ab:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

.field private ac:Landroid/widget/LinearLayout;

.field private ad:Lcom/facebook/resources/ui/FbButton;

.field private ae:LX/Bkm;

.field private final af:LX/9el;

.field private final ag:LX/Biv;

.field public p:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Bky;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Bib;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/Bij;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/Bm7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Bmo;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/Bn1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/Bih;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1812433
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1812434
    new-instance v0, LX/BjO;

    invoke-direct {v0, p0}, LX/BjO;-><init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->af:LX/9el;

    .line 1812435
    new-instance v0, LX/BjP;

    invoke-direct {v0, p0}, LX/BjP;-><init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ag:LX/Biv;

    return-void
.end method

.method private A()LX/4EN;
    .locals 3

    .prologue
    .line 1812362
    new-instance v0, LX/4EG;

    invoke-direct {v0}, LX/4EG;-><init>()V

    .line 1812363
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->M:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1812364
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->O:Lcom/facebook/events/common/ActionMechanism;

    if-eqz v1, :cond_0

    .line 1812365
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->O:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v1}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1812366
    :cond_0
    new-instance v1, LX/4EG;

    invoke-direct {v1}, LX/4EG;-><init>()V

    .line 1812367
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->M:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v2, v2, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1812368
    new-instance v2, LX/4EL;

    invoke-direct {v2}, LX/4EL;-><init>()V

    .line 1812369
    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1812370
    new-instance v0, LX/4EN;

    invoke-direct {v0}, LX/4EN;-><init>()V

    invoke-virtual {v0, v2}, LX/4EN;->a(LX/4EL;)LX/4EN;

    move-result-object v0

    return-object v0
.end method

.method private B()Lcom/facebook/events/model/Event;
    .locals 5

    .prologue
    .line 1812371
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812372
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812373
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812374
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812375
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_1

    .line 1812376
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812377
    iget-object v1, v0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812378
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812379
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    move-object v1, v2

    .line 1812380
    new-instance v2, LX/7vC;

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    invoke-direct {v2, v3}, LX/7vC;-><init>(Lcom/facebook/events/model/Event;)V

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812381
    iget-object v4, v3, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    move-object v3, v4

    .line 1812382
    iput-object v3, v2, LX/7vC;->b:Ljava/lang/String;

    .line 1812383
    move-object v2, v2

    .line 1812384
    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->T:Lcom/facebook/events/create/ui/EventDescriptionText;

    invoke-virtual {v3}, Lcom/facebook/events/create/ui/EventDescriptionText;->getDescriptionAsTextWithEntities()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/7ng;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-result-object v3

    .line 1812385
    iput-object v3, v2, LX/7vC;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    .line 1812386
    move-object v2, v2

    .line 1812387
    iget-boolean v3, v1, Lcom/facebook/events/ui/date/EventTimeModel;->b:Z

    move v3, v3

    .line 1812388
    iput-boolean v3, v2, LX/7vC;->L:Z

    .line 1812389
    move-object v2, v2

    .line 1812390
    iget-object v3, v1, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v3, v3

    .line 1812391
    iput-object v3, v2, LX/7vC;->K:Ljava/util/TimeZone;

    .line 1812392
    move-object v2, v2

    .line 1812393
    invoke-virtual {v1}, Lcom/facebook/events/ui/date/EventTimeModel;->d()Ljava/sql/Date;

    move-result-object v3

    .line 1812394
    iput-object v3, v2, LX/7vC;->I:Ljava/util/Date;

    .line 1812395
    move-object v2, v2

    .line 1812396
    invoke-virtual {v1}, Lcom/facebook/events/ui/date/EventTimeModel;->h()Ljava/sql/Date;

    move-result-object v1

    .line 1812397
    iput-object v1, v2, LX/7vC;->J:Ljava/util/Date;

    .line 1812398
    move-object v1, v2

    .line 1812399
    iput-object v0, v1, LX/7vC;->g:Lcom/facebook/events/model/PrivacyType;

    .line 1812400
    move-object v0, v1

    .line 1812401
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812402
    iget-boolean v2, v1, LX/Bn1;->f:Z

    move v1, v2

    .line 1812403
    iput-boolean v1, v0, LX/7vC;->h:Z

    .line 1812404
    move-object v0, v0

    .line 1812405
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812406
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    move-object v1, v2

    .line 1812407
    invoke-virtual {v0, v1}, LX/7vC;->a(LX/03R;)LX/7vC;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812408
    iget-boolean v2, v1, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    move v1, v2

    .line 1812409
    iput-boolean v1, v0, LX/7vC;->j:Z

    .line 1812410
    move-object v0, v0

    .line 1812411
    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->a(LX/7vC;)V

    .line 1812412
    invoke-virtual {v0}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v0

    return-object v0

    .line 1812413
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812414
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812415
    goto :goto_0
.end method

.method private C()V
    .locals 3

    .prologue
    .line 1812416
    const v0, 0x7f082175

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    .line 1812417
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "edit_progress_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1812418
    return-void
.end method

.method public static D(Lcom/facebook/events/create/EventEditNikumanActivity;)V
    .locals 3

    .prologue
    .line 1812419
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 1812420
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->S:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v1}, Lcom/facebook/events/create/ui/EventNameEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1812421
    return-void
.end method

.method private E()Z
    .locals 2

    .prologue
    .line 1812422
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->A:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 1812423
    iget-object v1, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v1

    .line 1812424
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812425
    iget-object p0, v1, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    move-object v1, p0

    .line 1812426
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static F(Lcom/facebook/events/create/EventEditNikumanActivity;)V
    .locals 2

    .prologue
    .line 1812427
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 1812428
    if-nez v0, :cond_1

    .line 1812429
    :cond_0
    :goto_0
    return-void

    .line 1812430
    :cond_1
    const-string v1, "edit_progress_dialog"

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/DialogFragment;

    .line 1812431
    if-eqz v0, :cond_0

    .line 1812432
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    goto :goto_0
.end method

.method public static G(Lcom/facebook/events/create/EventEditNikumanActivity;)V
    .locals 4

    .prologue
    .line 1812436
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->E:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 1812437
    const-string v1, "event_id"

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812438
    iget-object v3, v2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v3

    .line 1812439
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1812440
    const-string v1, "extras_event_analytics_params"

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->M:Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1812441
    const-string v1, "extra_is_event_canceled"

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812442
    iget-boolean v3, v2, Lcom/facebook/events/model/Event;->y:Z

    move v2, v3

    .line 1812443
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1812444
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_CANCEL_EVENT_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1812445
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->F:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x6f

    invoke-interface {v1, v0, v2, p0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1812446
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/events/model/Event;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1812447
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1812448
    const-string v1, "extras_event"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1812449
    const-string v1, "extras_event_ticket_url"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1812450
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1812451
    const-string v1, "extras_event_action_mechanism"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1812452
    return-object v0
.end method

.method private a(ILandroid/widget/TextView;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1812621
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->C:LX/0wM;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    const v2, -0x423e37

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1812622
    invoke-virtual {p2, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1812623
    return-void
.end method

.method private a(LX/4EN;)V
    .locals 4

    .prologue
    .line 1812453
    new-instance v1, LX/4Dr;

    invoke-direct {v1}, LX/4Dr;-><init>()V

    .line 1812454
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812455
    iget-object v2, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v2, v2

    .line 1812456
    iget-object v0, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1812457
    const-string v3, "0"

    invoke-static {v0, v3}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/4Dr;->a(Ljava/lang/String;)LX/4Dr;

    .line 1812458
    invoke-virtual {v2}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->f()Ljava/lang/String;

    move-result-object v0

    const-string v3, "0"

    invoke-static {v0, v3}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/4Dr;->b(Ljava/lang/String;)LX/4Dr;

    .line 1812459
    iget-object v0, v2, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1812460
    const-string v2, "0"

    invoke-static {v0, v2}, LX/0Qh;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, LX/4Dr;->c(Ljava/lang/String;)LX/4Dr;

    .line 1812461
    invoke-virtual {p1, v1}, LX/4EN;->a(LX/4Dr;)LX/4EN;

    .line 1812462
    return-void
.end method

.method private a(LX/7vC;)V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    .line 1812463
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812464
    iget-object v3, v0, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    move-object v3, v3

    .line 1812465
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812466
    iget-wide v9, v0, Lcom/facebook/events/model/Event;->P:J

    move-wide v6, v9

    .line 1812467
    invoke-static {v6, v7}, Lcom/facebook/events/model/Event;->b(J)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812468
    iget-wide v9, v0, Lcom/facebook/events/model/Event;->P:J

    move-wide v6, v9

    .line 1812469
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1812470
    iget-wide v9, v3, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    move-wide v6, v9

    .line 1812471
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v0, v6}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 1812472
    :goto_0
    iget-object v6, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812473
    iget-wide v9, v6, Lcom/facebook/events/model/Event;->P:J

    move-wide v6, v9

    .line 1812474
    invoke-static {v6, v7}, Lcom/facebook/events/model/Event;->b(J)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1812475
    iget-wide v9, v3, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    move-wide v6, v9

    .line 1812476
    invoke-static {v6, v7}, Lcom/facebook/events/model/Event;->b(J)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812477
    iget-object v7, v6, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v6, v7

    .line 1812478
    iget-object v7, v3, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v7, v7

    .line 1812479
    invoke-static {v6, v7}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1812480
    :goto_1
    if-nez v0, :cond_0

    if-eqz v1, :cond_3

    .line 1812481
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    .line 1812482
    goto :goto_0

    :cond_2
    move v1, v2

    .line 1812483
    goto :goto_1

    .line 1812484
    :cond_3
    iget-wide v9, v3, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    move-wide v0, v9

    .line 1812485
    invoke-static {v0, v1}, Lcom/facebook/events/model/Event;->b(J)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1812486
    iget-wide v9, v3, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    move-wide v0, v9

    .line 1812487
    iget-object v2, v3, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v2, v2

    .line 1812488
    const/4 v10, 0x0

    .line 1812489
    iput-wide v0, p1, LX/7vC;->N:J

    .line 1812490
    iput-object v2, p1, LX/7vC;->O:Ljava/lang/String;

    .line 1812491
    iput-object v10, p1, LX/7vC;->M:LX/7vL;

    .line 1812492
    iput-object v10, p1, LX/7vC;->P:Ljava/lang/String;

    .line 1812493
    iput-object v10, p1, LX/7vC;->Q:Ljava/util/TimeZone;

    .line 1812494
    const/4 v9, 0x0

    iput v9, p1, LX/7vC;->R:I

    .line 1812495
    iput-object v10, p1, LX/7vC;->S:Ljava/lang/String;

    .line 1812496
    goto :goto_2

    .line 1812497
    :cond_4
    invoke-virtual {p1}, LX/7vC;->a()LX/7vC;

    .line 1812498
    iget-object v0, v3, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v6, v0

    .line 1812499
    if-eqz v6, :cond_0

    .line 1812500
    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->j()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;

    move-result-object v7

    .line 1812501
    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1812502
    iput-wide v0, p1, LX/7vC;->N:J

    .line 1812503
    move-object v0, p1

    .line 1812504
    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 1812505
    iput-object v1, v0, LX/7vC;->O:Ljava/lang/String;

    .line 1812506
    move-object v8, v0

    .line 1812507
    if-eqz v7, :cond_5

    invoke-virtual {v7}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->a()D

    move-result-wide v0

    move-wide v2, v0

    :goto_3
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$LocationModel;->b()D

    move-result-wide v0

    :goto_4
    invoke-virtual {v8, v2, v3, v0, v1}, LX/7vC;->a(DD)LX/7vC;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    if-nez v0, :cond_7

    const/4 v0, 0x0

    .line 1812508
    :goto_5
    iput-object v0, v1, LX/7vC;->P:Ljava/lang/String;

    .line 1812509
    goto :goto_2

    :cond_5
    move-wide v2, v4

    goto :goto_3

    :cond_6
    move-wide v0, v4

    goto :goto_4

    :cond_7
    invoke-virtual {v6}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->c()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel$AddressModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_5
.end method

.method private static a(Lcom/facebook/events/create/EventEditNikumanActivity;LX/0TD;LX/Bky;LX/Bib;LX/Bij;LX/Bl6;LX/Bm7;LX/0Or;LX/Bn1;LX/Bih;LX/1Ck;LX/0tX;LX/0SI;LX/01T;LX/0wM;Ljava/lang/Boolean;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/BkU;LX/1nQ;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/create/EventEditNikumanActivity;",
            "LX/0TD;",
            "LX/Bky;",
            "LX/Bib;",
            "LX/Bij;",
            "LX/Bl6;",
            "LX/Bm7;",
            "LX/0Or",
            "<",
            "LX/Bmo;",
            ">;",
            "LX/Bn1;",
            "LX/Bih;",
            "LX/1Ck;",
            "LX/0tX;",
            "LX/0SI;",
            "LX/01T;",
            "LX/0wM;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/BkU;",
            "LX/1nQ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1812510
    iput-object p1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->p:LX/0TD;

    iput-object p2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->q:LX/Bky;

    iput-object p3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->r:LX/Bib;

    iput-object p4, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->s:LX/Bij;

    iput-object p5, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->t:LX/Bl6;

    iput-object p6, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->u:LX/Bm7;

    iput-object p7, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->v:LX/0Or;

    iput-object p8, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    iput-object p9, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->x:LX/Bih;

    iput-object p10, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->y:LX/1Ck;

    iput-object p11, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->z:LX/0tX;

    iput-object p12, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->A:LX/0SI;

    iput-object p13, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->B:LX/01T;

    iput-object p14, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->C:LX/0wM;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->D:Ljava/lang/Boolean;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->E:LX/0Or;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->F:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->G:LX/BkU;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->H:LX/1nQ;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 22

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v21

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/events/create/EventEditNikumanActivity;

    invoke-static/range {v21 .. v21}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v3

    check-cast v3, LX/0TD;

    invoke-static/range {v21 .. v21}, LX/Bky;->a(LX/0QB;)LX/Bky;

    move-result-object v4

    check-cast v4, LX/Bky;

    invoke-static/range {v21 .. v21}, LX/Bib;->a(LX/0QB;)LX/Bib;

    move-result-object v5

    check-cast v5, LX/Bib;

    invoke-static/range {v21 .. v21}, LX/Bij;->a(LX/0QB;)LX/Bij;

    move-result-object v6

    check-cast v6, LX/Bij;

    invoke-static/range {v21 .. v21}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v7

    check-cast v7, LX/Bl6;

    invoke-static/range {v21 .. v21}, LX/Bm7;->a(LX/0QB;)LX/Bm7;

    move-result-object v8

    check-cast v8, LX/Bm7;

    const/16 v9, 0x1bb3

    move-object/from16 v0, v21

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    invoke-static/range {v21 .. v21}, LX/Bn1;->a(LX/0QB;)LX/Bn1;

    move-result-object v10

    check-cast v10, LX/Bn1;

    invoke-static/range {v21 .. v21}, LX/Bih;->a(LX/0QB;)LX/Bih;

    move-result-object v11

    check-cast v11, LX/Bih;

    invoke-static/range {v21 .. v21}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    invoke-static/range {v21 .. v21}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v13

    check-cast v13, LX/0tX;

    invoke-static/range {v21 .. v21}, LX/0WG;->a(LX/0QB;)LX/0SI;

    move-result-object v14

    check-cast v14, LX/0SI;

    invoke-static/range {v21 .. v21}, LX/15N;->a(LX/0QB;)LX/01T;

    move-result-object v15

    check-cast v15, LX/01T;

    invoke-static/range {v21 .. v21}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v16

    check-cast v16, LX/0wM;

    invoke-static/range {v21 .. v21}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v17

    check-cast v17, Ljava/lang/Boolean;

    const/16 v18, 0xc

    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v18

    invoke-static/range {v21 .. v21}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v19

    check-cast v19, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v21 .. v21}, LX/BkU;->a(LX/0QB;)LX/BkU;

    move-result-object v20

    check-cast v20, LX/BkU;

    invoke-static/range {v21 .. v21}, LX/1nQ;->a(LX/0QB;)LX/1nQ;

    move-result-object v21

    check-cast v21, LX/1nQ;

    invoke-static/range {v2 .. v21}, Lcom/facebook/events/create/EventEditNikumanActivity;->a(Lcom/facebook/events/create/EventEditNikumanActivity;LX/0TD;LX/Bky;LX/Bib;LX/Bij;LX/Bl6;LX/Bm7;LX/0Or;LX/Bn1;LX/Bih;LX/1Ck;LX/0tX;LX/0SI;LX/01T;LX/0wM;Ljava/lang/Boolean;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/BkU;LX/1nQ;)V

    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1812511
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ab:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Z)V

    .line 1812512
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ae:LX/Bkm;

    invoke-virtual {v0, p1}, LX/Bkm;->a(Z)V

    .line 1812513
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ae:LX/Bkm;

    invoke-virtual {v0, p1}, LX/Bkm;->b(Z)V

    .line 1812514
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Z:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->a(ZZ)V

    .line 1812515
    return-void
.end method

.method private b()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 1812516
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812517
    iget-object v2, v1, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, v2

    .line 1812518
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812519
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->c:Ljava/lang/String;

    move-object v2, v3

    .line 1812520
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1812521
    :cond_0
    :goto_0
    return v0

    .line 1812522
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812523
    iget-object v2, v1, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v1, v2

    .line 1812524
    invoke-static {v1}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812525
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->d:Ljava/lang/String;

    move-object v2, v3

    .line 1812526
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1812527
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812528
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v1, v2

    .line 1812529
    invoke-virtual {v1}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->k()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812530
    iget-object v3, v2, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    move-object v2, v3

    .line 1812531
    iget-object v3, v1, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    move-object v1, v3

    .line 1812532
    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1812533
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812534
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    move-object v1, v2

    .line 1812535
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812536
    iget-wide v6, v2, Lcom/facebook/events/model/Event;->P:J

    move-wide v2, v6

    .line 1812537
    iget-wide v6, v1, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    move-wide v4, v6

    .line 1812538
    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812539
    iget-object v3, v2, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v2, v3

    .line 1812540
    iget-object v3, v1, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v1, v3

    .line 1812541
    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1812542
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812543
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    move-object v1, v2

    .line 1812544
    iget-boolean v2, v1, Lcom/facebook/events/ui/date/EventTimeModel;->b:Z

    move v2, v2

    .line 1812545
    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812546
    iget-boolean v4, v3, Lcom/facebook/events/model/Event;->N:Z

    move v3, v4

    .line 1812547
    if-ne v2, v3, :cond_0

    .line 1812548
    if-eqz v2, :cond_4

    .line 1812549
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 1812550
    invoke-virtual {v1}, Lcom/facebook/events/ui/date/EventTimeModel;->d()Ljava/sql/Date;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    invoke-virtual {v3}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1812551
    :cond_2
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812552
    iget-boolean v2, v1, Lcom/facebook/events/model/Event;->h:Z

    move v1, v2

    .line 1812553
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812554
    iget-boolean v3, v2, LX/Bn1;->f:Z

    move v2, v3

    .line 1812555
    if-ne v1, v2, :cond_0

    .line 1812556
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->u()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v1}, Lcom/facebook/events/create/EventCompositionModel;->o()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1812557
    :cond_3
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812558
    iget-boolean v2, v1, Lcom/facebook/events/model/Event;->j:Z

    move v1, v2

    .line 1812559
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812560
    iget-boolean v3, v2, Lcom/facebook/events/create/EventCompositionModel;->o:Z

    move v2, v3

    .line 1812561
    if-ne v1, v2, :cond_0

    .line 1812562
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812563
    iget-object v2, v1, Lcom/facebook/events/model/Event;->k:LX/03R;

    move-object v1, v2

    .line 1812564
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812565
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->n:LX/03R;

    move-object v2, v3

    .line 1812566
    if-ne v1, v2, :cond_0

    .line 1812567
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1812568
    :cond_4
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->L()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/EventTimeModel;->d()Ljava/sql/Date;

    move-result-object v3

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    invoke-virtual {v2}, Lcom/facebook/events/model/Event;->N()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/events/ui/date/EventTimeModel;->h()Ljava/sql/Date;

    move-result-object v1

    invoke-static {v2, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1812569
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1812570
    const/4 v0, 0x1

    .line 1812571
    :goto_0
    return v0

    .line 1812572
    :cond_0
    sget-object v0, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 1812573
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    goto :goto_0
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1812574
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1812575
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Q:LX/0h5;

    .line 1812576
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const/4 v1, 0x1

    .line 1812577
    iput v1, v0, LX/108;->a:I

    .line 1812578
    move-object v0, v0

    .line 1812579
    const v1, 0x7f08215b

    invoke-virtual {p0, v1}, Lcom/facebook/events/create/EventEditNikumanActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1812580
    iput-object v1, v0, LX/108;->g:Ljava/lang/String;

    .line 1812581
    move-object v0, v0

    .line 1812582
    const/4 v1, -0x2

    .line 1812583
    iput v1, v0, LX/108;->h:I

    .line 1812584
    move-object v0, v0

    .line 1812585
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    .line 1812586
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Q:LX/0h5;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1812587
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Q:LX/0h5;

    new-instance v1, LX/BjQ;

    invoke-direct {v1, p0}, LX/BjQ;-><init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setTitlebarAsModal(Landroid/view/View$OnClickListener;)V

    .line 1812588
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Q:LX/0h5;

    new-instance v1, LX/BjR;

    invoke-direct {v1, p0}, LX/BjR;-><init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1812589
    const v0, 0x7f0d0dd7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ac:Landroid/widget/LinearLayout;

    .line 1812590
    const v0, 0x7f0d0de3

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->R:Landroid/widget/FrameLayout;

    .line 1812591
    const v0, 0x7f0d0dc4

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventNameEditText;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->S:Lcom/facebook/events/create/ui/EventNameEditText;

    .line 1812592
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->S:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->s:LX/Bij;

    .line 1812593
    iput-object v1, v0, Lcom/facebook/events/create/ui/EventNameEditText;->h:LX/Bii;

    .line 1812594
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->S:Lcom/facebook/events/create/ui/EventNameEditText;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventNameEditText;->a(Lcom/facebook/events/create/EventCompositionModel;)V

    .line 1812595
    const v0, 0x7f0d0dc9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventDescriptionText;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->T:Lcom/facebook/events/create/ui/EventDescriptionText;

    .line 1812596
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->T:Lcom/facebook/events/create/ui/EventDescriptionText;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/EventDescriptionText;->a(Lcom/facebook/events/create/EventCompositionModel;)V

    .line 1812597
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->l()V

    .line 1812598
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->m()V

    .line 1812599
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->n()V

    .line 1812600
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->o()V

    .line 1812601
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->w()V

    .line 1812602
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->p()V

    .line 1812603
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->q()V

    .line 1812604
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->r()V

    .line 1812605
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->s()V

    .line 1812606
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->G:LX/BkU;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->af:LX/9el;

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ag:LX/Biv;

    invoke-virtual {v0, v1, v2, v3}, LX/BkU;->a(Lcom/facebook/events/create/EventCompositionModel;LX/9el;LX/Biv;)V

    .line 1812607
    if-eqz p1, :cond_0

    .line 1812608
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->G:LX/BkU;

    invoke-virtual {v0, p1}, LX/BkU;->b(Landroid/os/Bundle;)V

    .line 1812609
    :cond_0
    new-instance v0, LX/Bkm;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Z:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ac:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->R:Landroid/widget/FrameLayout;

    const/16 v5, 0x12c

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/Bkm;-><init>(Landroid/content/Context;Lcom/facebook/events/create/ui/CoverPhotoSelector;Landroid/widget/LinearLayout;Landroid/widget/FrameLayout;I)V

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ae:LX/Bkm;

    .line 1812610
    const v0, 0x7f0d0dd6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ab:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    .line 1812611
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ab:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812612
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v1, v2

    .line 1812613
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ae:LX/Bkm;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(Lcom/facebook/events/create/ui/EventCoverPhotoModel;LX/Bkm;)V

    .line 1812614
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->x()V

    .line 1812615
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1812616
    const v0, 0x7f0d0dc5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Y:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    .line 1812617
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Y:Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812618
    iget-object p0, v1, Lcom/facebook/events/create/EventCompositionModel;->h:Lcom/facebook/events/ui/date/EventTimeModel;

    move-object v1, p0

    .line 1812619
    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/EventTimeModel;)V

    .line 1812620
    return-void
.end method

.method private m()V
    .locals 6

    .prologue
    .line 1812347
    const v0, 0x7f0d0dc8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/location/LocationNikumanPicker;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->U:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    .line 1812348
    const v0, 0x7f020966

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->U:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/create/EventEditNikumanActivity;->a(ILandroid/widget/TextView;)V

    .line 1812349
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->U:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812350
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->i:Lcom/facebook/events/ui/location/EventLocationModel;

    move-object v1, v2

    .line 1812351
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->v:LX/0Or;

    const/16 v4, 0x66

    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->t()Z

    move-result v5

    move-object v3, p0

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a(Lcom/facebook/events/ui/location/EventLocationModel;LX/0Or;Landroid/app/Activity;IZ)V

    .line 1812352
    return-void
.end method

.method private n()V
    .locals 5

    .prologue
    .line 1812353
    const v0, 0x7f0d0dda

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/EventCategorySelector;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->V:Lcom/facebook/events/create/ui/EventCategorySelector;

    .line 1812354
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812355
    iget-object v1, v0, Lcom/facebook/events/model/Event;->ak:Ljava/lang/String;

    move-object v0, v1

    .line 1812356
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 1812357
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->V:Lcom/facebook/events/create/ui/EventCategorySelector;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812358
    iget-object v4, v3, Lcom/facebook/events/model/Event;->v:Ljava/lang/String;

    move-object v3, v4

    .line 1812359
    const/16 v4, 0x6d

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/facebook/events/create/ui/EventCategorySelector;->a(Lcom/facebook/events/create/EventCompositionModel;Ljava/lang/String;ZI)V

    .line 1812360
    return-void

    .line 1812361
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 2

    .prologue
    .line 1812101
    const v0, 0x7f0d0ddc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->W:Landroid/widget/FrameLayout;

    .line 1812102
    const v0, 0x7f0d0ddd

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterEditTextView;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->X:Lcom/facebook/widget/text/BetterEditTextView;

    .line 1812103
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->W:Landroid/widget/FrameLayout;

    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1812104
    return-void

    .line 1812105
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private p()V
    .locals 2

    .prologue
    .line 1812106
    const v0, 0x7f0d0dc1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/CoverPhotoSelector;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Z:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    .line 1812107
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Z:Lcom/facebook/events/create/ui/CoverPhotoSelector;

    new-instance v1, LX/BjS;

    invoke-direct {v1, p0}, LX/BjS;-><init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/events/create/ui/CoverPhotoSelector;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1812108
    return-void
.end method

.method private q()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1812109
    const v0, 0x7f0d0de0

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/ui/CohostsSelector;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->aa:Lcom/facebook/events/create/ui/CohostsSelector;

    .line 1812110
    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->aa:Lcom/facebook/events/create/ui/CohostsSelector;

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812111
    iget-object v4, v0, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    move-object v4, v4

    .line 1812112
    const/16 v5, 0x69

    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->u()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->E()Z

    move-result v6

    if-nez v6, :cond_1

    :goto_1
    invoke-virtual {v3, v4, v5, v0, v1}, Lcom/facebook/events/create/ui/CohostsSelector;->a(Lcom/facebook/events/create/ui/EventCohostsModel;IZZ)V

    .line 1812113
    return-void

    :cond_0
    move v0, v2

    .line 1812114
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method private r()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1812115
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->x:LX/Bih;

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    const v0, 0x7f0d0de1

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbCheckedTextView;

    const v1, 0x7f0d0dcf

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbCheckedTextView;

    invoke-virtual {v2, v3, v0, v1}, LX/Bih;->a(Lcom/facebook/events/create/EventCompositionModel;Lcom/facebook/resources/ui/FbCheckedTextView;Lcom/facebook/resources/ui/FbCheckedTextView;)V

    .line 1812116
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812117
    iget-object v1, v0, Lcom/facebook/events/model/Event;->m:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-object v0, v1

    .line 1812118
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->INTERESTED:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eq v0, v1, :cond_0

    .line 1812119
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->x:LX/Bih;

    invoke-virtual {v0, v4}, LX/Bih;->a(Z)V

    .line 1812120
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812121
    iget-boolean v1, v0, Lcom/facebook/events/model/Event;->i:Z

    move v0, v1

    .line 1812122
    if-nez v0, :cond_2

    .line 1812123
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->x:LX/Bih;

    invoke-virtual {v0, v4}, LX/Bih;->a(Z)V

    .line 1812124
    :cond_2
    return-void
.end method

.method private s()V
    .locals 2

    .prologue
    .line 1812125
    const v0, 0x7f0d0de2

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ad:Lcom/facebook/resources/ui/FbButton;

    .line 1812126
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ad:Lcom/facebook/resources/ui/FbButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1812127
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ad:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/BjT;

    invoke-direct {v1, p0}, LX/BjT;-><init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1812128
    return-void
.end method

.method private t()Z
    .locals 2

    .prologue
    .line 1812129
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812130
    iget-object v1, v0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812131
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812132
    iget-object v1, v0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812133
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private u()Z
    .locals 1

    .prologue
    .line 1812134
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812135
    iget-object p0, v0, Lcom/facebook/events/model/Event;->r:Ljava/lang/String;

    move-object v0, p0

    .line 1812136
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()Z
    .locals 2

    .prologue
    .line 1812137
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812138
    iget-object v1, v0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812139
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812140
    iget-object v1, v0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812141
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812142
    iget-object v1, v0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812143
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private w()V
    .locals 3

    .prologue
    .line 1812144
    const v0, 0x7f0d0dce

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->P:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    .line 1812145
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->P:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->D:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f082146

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->setText(I)V

    .line 1812146
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->P:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    invoke-virtual {v0, v1, v2}, LX/Bn1;->a(Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;)V

    .line 1812147
    return-void

    .line 1812148
    :cond_0
    const v0, 0x7f082145

    goto :goto_0
.end method

.method private x()V
    .locals 2

    .prologue
    .line 1812149
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->P:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    if-eqz v0, :cond_0

    .line 1812150
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->P:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    new-instance v1, LX/BjU;

    invoke-direct {v1, p0}, LX/BjU;-><init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    .line 1812151
    iput-object v1, v0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a:LX/Bj5;

    .line 1812152
    :cond_0
    return-void
.end method

.method private y()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1812153
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->Q:LX/0h5;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812154
    iget-object v3, v2, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1812155
    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1812156
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812157
    iget-object v2, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v0, v2

    .line 1812158
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->g()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->b(Z)V

    .line 1812159
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->X:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->L:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1812160
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812161
    iget-object v2, v0, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v2

    .line 1812162
    if-eqz v0, :cond_1

    .line 1812163
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812164
    iget-boolean v3, v0, Lcom/facebook/events/model/Event;->l:Z

    move v0, v3

    .line 1812165
    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->v()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0}, LX/Bn1;->a(Z)V

    .line 1812166
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812167
    iget-object v3, v2, Lcom/facebook/events/model/Event;->u:Ljava/lang/String;

    move-object v2, v3

    .line 1812168
    iput-object v2, v0, LX/Bn1;->m:Ljava/lang/String;

    .line 1812169
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812170
    iget-object v3, v2, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v2, v3

    .line 1812171
    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812172
    iget-boolean v4, v3, Lcom/facebook/events/model/Event;->h:Z

    move v3, v4

    .line 1812173
    invoke-virtual {v0, v2, v3, v1}, LX/Bn1;->a(Lcom/facebook/events/model/PrivacyType;ZZ)V

    .line 1812174
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->P:Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812175
    iget-boolean v2, v1, Lcom/facebook/events/model/Event;->h:Z

    move v1, v2

    .line 1812176
    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->setChecked(Z)V

    .line 1812177
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->G:LX/BkU;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812178
    iget-object v2, v1, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v1, v2

    .line 1812179
    iput-object v1, v0, LX/BkU;->k:Lcom/facebook/events/model/PrivacyType;

    .line 1812180
    return-void

    :cond_2
    move v0, v1

    .line 1812181
    goto :goto_0
.end method

.method public static z(Lcom/facebook/events/create/EventEditNikumanActivity;)V
    .locals 11

    .prologue
    .line 1812182
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->C()V

    .line 1812183
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->B()Lcom/facebook/events/model/Event;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->K:Lcom/facebook/events/model/Event;

    .line 1812184
    invoke-virtual {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->q:LX/Bky;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->K:Lcom/facebook/events/model/Event;

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->p:LX/0TD;

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/data/EventsProvider;->a(Landroid/content/ContentResolver;LX/Bky;Lcom/facebook/events/model/Event;LX/0TD;)V

    .line 1812185
    new-instance v0, Lcom/facebook/events/protocol/EditEventParams;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->M:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v1, v1, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->K:Lcom/facebook/events/model/Event;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/events/protocol/EditEventParams;-><init>(Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;)V

    .line 1812186
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->A()LX/4EN;

    move-result-object v8

    .line 1812187
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812188
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v3, v2

    .line 1812189
    invoke-virtual {v3}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1812190
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->r:LX/Bib;

    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ab:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-virtual {v2}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->getHandler()Landroid/os/Handler;

    move-result-object v2

    .line 1812191
    iget-object v4, v3, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->c:Landroid/net/Uri;

    move-object v3, v4

    .line 1812192
    iget-object v4, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->K:Lcom/facebook/events/model/Event;

    .line 1812193
    iget-object v5, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1812194
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->M:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v7, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->O:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual/range {v1 .. v7}, LX/Bib;->a(Landroid/os/Handler;Landroid/net/Uri;JLcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionMechanism;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1812195
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v1}, Lcom/facebook/events/create/EventCompositionModel;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1812196
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812197
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    invoke-static {v2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1812198
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    iget-object v2, v2, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1812199
    iget-object v3, v2, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    move-object v2, v3

    .line 1812200
    iget-object v3, v1, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1812201
    iget-object v1, v3, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    move-object v3, v1

    .line 1812202
    invoke-static {v3, v2}, Lcom/facebook/events/create/EventCompositionModel;->a(LX/0Px;LX/0Px;)LX/0Px;

    move-result-object v2

    move-object v1, v2

    .line 1812203
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812204
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1812205
    iget-object v3, v2, Lcom/facebook/events/create/EventCompositionModel;->a:Lcom/facebook/events/create/EventCompositionModel;

    iget-object v3, v3, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1812206
    iget-object v4, v3, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    move-object v3, v4

    .line 1812207
    iget-object v4, v2, Lcom/facebook/events/create/EventCompositionModel;->j:Lcom/facebook/events/create/ui/EventCohostsModel;

    .line 1812208
    iget-object v2, v4, Lcom/facebook/events/create/ui/EventCohostsModel;->a:LX/0Px;

    move-object v4, v2

    .line 1812209
    invoke-static {v3, v4}, Lcom/facebook/events/create/EventCompositionModel;->a(LX/0Px;LX/0Px;)LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 1812210
    invoke-static {v1}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1812211
    const-string v3, "add_host_ids"

    invoke-virtual {v8, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1812212
    :cond_1
    invoke-static {v2}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1812213
    const-string v1, "remove_host_ids"

    invoke-virtual {v8, v1, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 1812214
    :cond_2
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->t()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1812215
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812216
    iget-object v2, v1, Lcom/facebook/events/create/EventCompositionModel;->k:Lcom/facebook/events/create/ui/EventCategoryModel;

    move-object v1, v2

    .line 1812217
    iget-object v2, v1, Lcom/facebook/events/create/ui/EventCategoryModel;->a:Ljava/lang/String;

    move-object v1, v2

    .line 1812218
    const-string v2, "category_name"

    invoke-virtual {v8, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812219
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->X:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->X:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1812220
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->X:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 1812221
    invoke-static {v1}, Lcom/facebook/events/create/EventEditNikumanActivity;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1812222
    const v0, 0x7f082156

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1812223
    invoke-static {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->F(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    .line 1812224
    :goto_1
    return-void

    .line 1812225
    :cond_3
    iget-object v1, v3, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1812226
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812227
    iget-object v4, v2, Lcom/facebook/events/model/Event;->W:Ljava/lang/String;

    move-object v2, v4

    .line 1812228
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v3}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1812229
    :cond_4
    invoke-direct {p0, v8}, Lcom/facebook/events/create/EventEditNikumanActivity;->a(LX/4EN;)V

    goto/16 :goto_0

    .line 1812230
    :cond_5
    const-string v2, "event_buy_ticket_url"

    invoke-virtual {v8, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812231
    :cond_6
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->a:Ljava/lang/String;

    move-object v9, v9

    .line 1812232
    invoke-virtual {v8, v9}, LX/4EN;->a(Ljava/lang/String;)LX/4EN;

    .line 1812233
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->b:Ljava/lang/String;

    if-eqz v9, :cond_7

    .line 1812234
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->b:Ljava/lang/String;

    .line 1812235
    const-string v10, "name"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812236
    :cond_7
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    if-eqz v9, :cond_8

    .line 1812237
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-static {v9, v10}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    .line 1812238
    const-string v10, "location_id"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812239
    :cond_8
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->f:Ljava/lang/Boolean;

    if-eqz v9, :cond_9

    .line 1812240
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->f:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_11

    const-string v9, ""

    .line 1812241
    :goto_2
    const-string v10, "location_name"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812242
    :cond_9
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->g:Ljava/lang/String;

    if-eqz v9, :cond_a

    .line 1812243
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->g:Ljava/lang/String;

    .line 1812244
    const-string v10, "start_time"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812245
    :cond_a
    iget-boolean v9, v0, Lcom/facebook/events/protocol/EditEventParams;->h:Z

    if-eqz v9, :cond_b

    .line 1812246
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->i:Ljava/lang/String;

    if-eqz v9, :cond_b

    .line 1812247
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->i:Ljava/lang/String;

    .line 1812248
    const-string v10, "end_time"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812249
    :cond_b
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    if-eqz v9, :cond_c

    .line 1812250
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_12

    const-string v9, ""

    .line 1812251
    :goto_3
    const-string v10, "description"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812252
    :cond_c
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    if-eqz v9, :cond_d

    .line 1812253
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    invoke-static {v9}, Lcom/facebook/events/model/PrivacyType;->getPrivacyTypeForEventEditInputData(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;

    move-result-object v9

    .line 1812254
    const-string v10, "event_visibility"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812255
    :cond_d
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    if-eqz v9, :cond_e

    .line 1812256
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    if-eqz v9, :cond_13

    const-string v9, "CAN_INVITE_FRIENDS"

    .line 1812257
    :goto_4
    const-string v10, "invite_policy"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812258
    :cond_e
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->l:LX/03R;

    invoke-virtual {v9}, LX/03R;->isSet()Z

    move-result v9

    if-eqz v9, :cond_f

    .line 1812259
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->l:LX/03R;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, LX/03R;->asBoolean(Z)Z

    move-result v9

    if-eqz v9, :cond_14

    const-string v9, "ONLY_ADMINS"

    .line 1812260
    :goto_5
    const-string v10, "can_post_policy"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1812261
    :cond_f
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->m:Ljava/lang/Boolean;

    if-eqz v9, :cond_10

    .line 1812262
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->m:Ljava/lang/Boolean;

    .line 1812263
    const-string v10, "post_approval_required"

    invoke-virtual {v8, v10, v9}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1812264
    :cond_10
    invoke-static {}, LX/Bk4;->b()LX/Bk3;

    move-result-object v0

    const-string v1, "input"

    invoke-virtual {v0, v1, v8}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/Bk3;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 1812265
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->z:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1812266
    new-instance v1, LX/BjV;

    invoke-direct {v1, p0}, LX/BjV;-><init>(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    .line 1812267
    iget-object v2, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->y:LX/1Ck;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "tasks-editEvent:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812268
    iget-object v5, v4, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v4, v5

    .line 1812269
    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_1

    .line 1812270
    :cond_11
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->e:Ljava/lang/String;

    goto/16 :goto_2

    .line 1812271
    :cond_12
    iget-object v9, v0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    goto/16 :goto_3

    .line 1812272
    :cond_13
    const-string v9, "CANNOT_INVITE_FRIENDS"

    goto :goto_4

    .line 1812273
    :cond_14
    const-string v9, "ALL"

    goto :goto_5
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1812274
    const-string v0, "event_edit_composer"

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1812309
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1812310
    invoke-static {p0, p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1812311
    const v0, 0x7f0400c8

    const v1, 0x7f040030

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/create/EventEditNikumanActivity;->overridePendingTransition(II)V

    .line 1812312
    const v0, 0x7f0304b7

    invoke-virtual {p0, v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->setContentView(I)V

    .line 1812313
    invoke-virtual {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extras_event"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/Event;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812314
    if-nez p1, :cond_1

    .line 1812315
    new-instance v0, Lcom/facebook/events/create/EventCompositionModel;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    invoke-direct {v0, v1}, Lcom/facebook/events/create/EventCompositionModel;-><init>(Lcom/facebook/events/model/Event;)V

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812316
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {v0}, Lcom/facebook/events/create/EventCompositionModel;->a()V

    .line 1812317
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extras_event_ticket_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->L:Ljava/lang/String;

    .line 1812318
    invoke-direct {p0, p1}, Lcom/facebook/events/create/EventEditNikumanActivity;->d(Landroid/os/Bundle;)V

    .line 1812319
    invoke-virtual {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 1812320
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->y()V

    .line 1812321
    const-string v1, "extra_ref_module"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->N:Ljava/lang/String;

    .line 1812322
    new-instance v1, Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v2, Lcom/facebook/events/common/EventActionContext;->a:Lcom/facebook/events/common/EventActionContext;

    iget-object v3, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->N:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventActionContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->M:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1812323
    const-string v1, "extras_event_action_mechanism"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/ActionMechanism;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->O:Lcom/facebook/events/common/ActionMechanism;

    .line 1812324
    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->H:LX/1nQ;

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->J:Lcom/facebook/events/model/Event;

    .line 1812325
    iget-object v2, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1812326
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->M:Lcom/facebook/events/common/EventAnalyticsParams;

    iget-object v3, v0, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->O:Lcom/facebook/events/common/ActionMechanism;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->O:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1812327
    :goto_1
    iget-object v4, v1, LX/1nQ;->i:LX/0Zb;

    const-string v5, "event_composer_edit_open"

    const/4 p1, 0x0

    invoke-interface {v4, v5, p1}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v4

    .line 1812328
    invoke-virtual {v4}, LX/0oG;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1812329
    const-string v5, "event_edit_composer"

    invoke-virtual {v4, v5}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/0oG;->c(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "Event"

    invoke-virtual {v4, v5}, LX/0oG;->b(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    iget-object v5, v1, LX/1nQ;->j:LX/0kv;

    iget-object p1, v1, LX/1nQ;->g:Landroid/content/Context;

    invoke-virtual {v5, p1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0oG;->e(Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "ref_mechanism"

    invoke-virtual {v4, v5, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    const-string v5, "ref_module"

    invoke-virtual {v4, v5, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v4

    invoke-virtual {v4}, LX/0oG;->d()V

    .line 1812330
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812331
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812332
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_3

    .line 1812333
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->x:LX/Bih;

    invoke-virtual {v0, v7}, LX/Bih;->a(Z)V

    .line 1812334
    :goto_2
    return-void

    .line 1812335
    :cond_1
    const-string v0, "EVENT_COMPOSITION_MODEL"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/create/EventCompositionModel;

    iput-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    goto/16 :goto_0

    .line 1812336
    :cond_2
    const-string v0, "unknown"

    goto :goto_1

    .line 1812337
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812338
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812339
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812340
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812341
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->w:LX/Bn1;

    .line 1812342
    iget-object v1, v0, LX/Bn1;->e:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v1

    .line 1812343
    sget-object v1, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v1, :cond_5

    .line 1812344
    :cond_4
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->x:LX/Bih;

    invoke-virtual {v0, v7}, LX/Bih;->c(Z)V

    .line 1812345
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->x:LX/Bih;

    invoke-virtual {v0, v6}, LX/Bih;->b(Z)V

    goto :goto_2

    .line 1812346
    :cond_5
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->x:LX/Bih;

    invoke-virtual {v0, v6}, LX/Bih;->a(Z)V

    goto :goto_2
.end method

.method public final finish()V
    .locals 2

    .prologue
    .line 1812275
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 1812276
    const v0, 0x7f0400b6

    const v1, 0x7f0400ca

    invoke-virtual {p0, v0, v1}, Lcom/facebook/events/create/EventEditNikumanActivity;->overridePendingTransition(II)V

    .line 1812277
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 1812278
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1812279
    if-eq p2, v3, :cond_0

    .line 1812280
    :goto_0
    return-void

    .line 1812281
    :cond_0
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1812282
    :sswitch_0
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->U:Lcom/facebook/events/ui/location/LocationNikumanPicker;

    invoke-virtual {v0, p3}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1812283
    :sswitch_1
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->ab:Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;

    invoke-virtual {v0, p1, p3}, Lcom/facebook/events/create/ui/EventCreationCoverPhotoView;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1812284
    :sswitch_2
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->aa:Lcom/facebook/events/create/ui/CohostsSelector;

    invoke-virtual {v0, p3}, Lcom/facebook/events/create/ui/CohostsSelector;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1812285
    :sswitch_3
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->V:Lcom/facebook/events/create/ui/EventCategorySelector;

    invoke-virtual {v0, p3}, Lcom/facebook/events/create/ui/EventCategorySelector;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 1812286
    :sswitch_4
    if-eqz p3, :cond_1

    const-string v0, "extra_event_cancel_state"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1812287
    const-string v0, "extra_event_cancel_state"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 1812288
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1812289
    const-string v2, "extra_event_cancel_state"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1812290
    invoke-virtual {p0, v3, v1}, Lcom/facebook/events/create/EventEditNikumanActivity;->setResult(ILandroid/content/Intent;)V

    .line 1812291
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->finish()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x66 -> :sswitch_0
        0x67 -> :sswitch_1
        0x68 -> :sswitch_1
        0x69 -> :sswitch_2
        0x6d -> :sswitch_3
        0x6f -> :sswitch_4
        0x26b9 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 1812292
    invoke-static {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->D(Lcom/facebook/events/create/EventEditNikumanActivity;)V

    .line 1812293
    invoke-direct {p0}, Lcom/facebook/events/create/EventEditNikumanActivity;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1812294
    const/4 v0, 0x0

    .line 1812295
    invoke-static {p0, v0, v0}, LX/BjN;->a(Landroid/app/Activity;LX/Bj8;LX/Bj9;)V

    .line 1812296
    :goto_0
    return-void

    .line 1812297
    :cond_0
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public final onEnterAnimationComplete()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 1812298
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->s:LX/Bij;

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->S:Lcom/facebook/events/create/ui/EventNameEditText;

    invoke-virtual {v0, v1}, LX/Bij;->b(Lcom/facebook/events/create/ui/EventNameEditText;)V

    .line 1812299
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1812300
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 1812301
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    .line 1812302
    iget-object p1, v0, Lcom/facebook/events/create/EventCompositionModel;->g:Lcom/facebook/events/create/ui/EventCoverPhotoModel;

    move-object v0, p1

    .line 1812303
    invoke-virtual {v0}, Lcom/facebook/events/create/ui/EventCoverPhotoModel;->g()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/events/create/EventEditNikumanActivity;->b(Z)V

    .line 1812304
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1812305
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1812306
    const-string v0, "EVENT_COMPOSITION_MODEL"

    iget-object v1, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->I:Lcom/facebook/events/create/EventCompositionModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1812307
    iget-object v0, p0, Lcom/facebook/events/create/EventEditNikumanActivity;->G:LX/BkU;

    invoke-virtual {v0, p1}, LX/BkU;->a(Landroid/os/Bundle;)V

    .line 1812308
    return-void
.end method
