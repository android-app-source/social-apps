.class public abstract Lcom/facebook/events/protocol/ContextParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# instance fields
.field public final a:Lcom/facebook/events/common/EventActionContext;
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Blt;)V
    .locals 1

    .prologue
    .line 1816730
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816731
    iget-object v0, p1, LX/Blt;->a:Lcom/facebook/events/common/EventActionContext;

    iput-object v0, p0, Lcom/facebook/events/protocol/ContextParams;->a:Lcom/facebook/events/common/EventActionContext;

    .line 1816732
    return-void
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1816721
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1816722
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "action_context"

    .line 1816723
    iget-object v3, p0, Lcom/facebook/events/protocol/ContextParams;->a:Lcom/facebook/events/common/EventActionContext;

    move-object v3, v3

    .line 1816724
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->j:Lorg/json/JSONObject;

    if-nez v4, :cond_0

    .line 1816725
    new-instance v11, Lorg/json/JSONObject;

    const-string v5, "source"

    iget-object v6, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v6}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const-string v7, "ref"

    iget-object v8, v3, Lcom/facebook/events/common/EventActionContext;->f:Lcom/facebook/events/common/ActionSource;

    invoke-virtual {v8}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string v9, "app_started_by_launcher"

    iget-boolean v10, v3, Lcom/facebook/events/common/EventActionContext;->i:Z

    if-eqz v10, :cond_1

    const/4 v10, 0x1

    :goto_0
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static/range {v5 .. v10}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v5

    invoke-direct {v11, v5}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    move-object v4, v11

    .line 1816726
    iput-object v4, v3, Lcom/facebook/events/common/EventActionContext;->j:Lorg/json/JSONObject;

    .line 1816727
    :cond_0
    iget-object v4, v3, Lcom/facebook/events/common/EventActionContext;->j:Lorg/json/JSONObject;

    invoke-virtual {v4}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 1816728
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816729
    return-object v0

    :cond_1
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1816733
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1816719
    iget-object v0, p0, Lcom/facebook/events/protocol/ContextParams;->a:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1816720
    return-void
.end method
