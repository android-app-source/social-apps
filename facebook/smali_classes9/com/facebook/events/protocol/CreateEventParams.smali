.class public Lcom/facebook/events/protocol/CreateEventParams;
.super Lcom/facebook/events/protocol/ContextParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/protocol/CreateEventParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/Long;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:J

.field public final e:Ljava/util/Date;

.field public final f:Z

.field public final g:Ljava/util/Date;

.field public final h:Ljava/util/TimeZone;

.field public final i:Lcom/facebook/events/model/EventType;

.field public final j:Lcom/facebook/events/model/PrivacyType;

.field public final k:Z

.field public final l:Ljava/lang/String;

.field public final m:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1816815
    new-instance v0, LX/Blv;

    invoke-direct {v0}, LX/Blv;-><init>()V

    sput-object v0, Lcom/facebook/events/protocol/CreateEventParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Blw;)V
    .locals 2

    .prologue
    .line 1816836
    invoke-direct {p0, p1}, Lcom/facebook/events/protocol/ContextParams;-><init>(LX/Blt;)V

    .line 1816837
    iget-object v0, p1, LX/Blw;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->a:Ljava/lang/Long;

    .line 1816838
    iget-object v0, p1, LX/Blw;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->b:Ljava/lang/String;

    .line 1816839
    iget-object v0, p1, LX/Blw;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->c:Ljava/lang/String;

    .line 1816840
    iget-wide v0, p1, LX/Blw;->e:J

    iput-wide v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->d:J

    .line 1816841
    iget-object v0, p1, LX/Blw;->f:Ljava/util/Date;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->e:Ljava/util/Date;

    .line 1816842
    iget-boolean v0, p1, LX/Blw;->g:Z

    iput-boolean v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->f:Z

    .line 1816843
    iget-object v0, p1, LX/Blw;->h:Ljava/util/Date;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->g:Ljava/util/Date;

    .line 1816844
    iget-object v0, p1, LX/Blw;->i:Ljava/util/TimeZone;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->h:Ljava/util/TimeZone;

    .line 1816845
    iget-object v0, p1, LX/Blw;->n:Lcom/facebook/events/model/EventType;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->i:Lcom/facebook/events/model/EventType;

    .line 1816846
    iget-object v0, p1, LX/Blw;->j:Lcom/facebook/events/model/PrivacyType;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    .line 1816847
    iget-boolean v0, p1, LX/Blw;->k:Z

    iput-boolean v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->k:Z

    .line 1816848
    iget-object v0, p1, LX/Blw;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->l:Ljava/lang/String;

    .line 1816849
    iget-boolean v0, p1, LX/Blw;->m:Z

    iput-boolean v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->m:Z

    .line 1816850
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1816835
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1816816
    invoke-super {p0, p1, p2}, Lcom/facebook/events/protocol/ContextParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1816817
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1816818
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816819
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816820
    iget-wide v4, p0, Lcom/facebook/events/protocol/CreateEventParams;->d:J

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1816821
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->e:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1816822
    iget-boolean v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->f:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1816823
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->g:Ljava/util/Date;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 1816824
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->g:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 1816825
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->g:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1816826
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->h:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816827
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->i:Lcom/facebook/events/model/EventType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1816828
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1816829
    iget-boolean v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->k:Z

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816830
    iget-object v0, p0, Lcom/facebook/events/protocol/CreateEventParams;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816831
    return-void

    :cond_1
    move v0, v2

    .line 1816832
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1816833
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1816834
    goto :goto_2
.end method
