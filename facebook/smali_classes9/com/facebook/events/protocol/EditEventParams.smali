.class public Lcom/facebook/events/protocol/EditEventParams;
.super Lcom/facebook/events/protocol/ContextParams;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/protocol/EditEventParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/Long;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/Boolean;

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/events/model/PrivacyType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/Boolean;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:LX/03R;

.field public m:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1816994
    new-instance v0, LX/Bly;

    invoke-direct {v0}, LX/Bly;-><init>()V

    sput-object v0, Lcom/facebook/events/protocol/EditEventParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1816995
    new-instance v0, LX/Blt;

    invoke-direct {v0, p1}, LX/Blt;-><init>(Landroid/os/Parcel;)V

    invoke-direct {p0, v0}, Lcom/facebook/events/protocol/ContextParams;-><init>(LX/Blt;)V

    .line 1816996
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->l:LX/03R;

    .line 1816997
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->a:Ljava/lang/String;

    .line 1816998
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->b:Ljava/lang/String;

    .line 1816999
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    .line 1817000
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    .line 1817001
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->e:Ljava/lang/String;

    .line 1817002
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 1817003
    const/4 v3, -0x1

    if-eq v0, v3, :cond_0

    .line 1817004
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->f:Ljava/lang/Boolean;

    .line 1817005
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->g:Ljava/lang/String;

    .line 1817006
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/events/protocol/EditEventParams;->h:Z

    .line 1817007
    iget-boolean v0, p0, Lcom/facebook/events/protocol/EditEventParams;->h:Z

    if-eqz v0, :cond_1

    .line 1817008
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->i:Ljava/lang/String;

    .line 1817009
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 1817010
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/events/model/PrivacyType;->valueOf(Ljava/lang/String;)Lcom/facebook/events/model/PrivacyType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    .line 1817011
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 1817012
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    .line 1817013
    :cond_3
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-static {v0}, LX/03R;->fromDbValue(I)LX/03R;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->l:LX/03R;

    .line 1817014
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 1817015
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    :goto_4
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->m:Ljava/lang/Boolean;

    .line 1817016
    :cond_4
    return-void

    .line 1817017
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    move v0, v2

    .line 1817018
    goto :goto_1

    :cond_7
    move v0, v2

    .line 1817019
    goto :goto_2

    :cond_8
    move v0, v2

    .line 1817020
    goto :goto_3

    :cond_9
    move v1, v2

    .line 1817021
    goto :goto_4
.end method

.method public constructor <init>(Lcom/facebook/events/common/EventActionContext;Lcom/facebook/events/model/Event;Lcom/facebook/events/model/Event;)V
    .locals 11

    .prologue
    .line 1816917
    new-instance v0, LX/Blt;

    invoke-direct {v0}, LX/Blt;-><init>()V

    .line 1816918
    iput-object p1, v0, LX/Blt;->a:Lcom/facebook/events/common/EventActionContext;

    .line 1816919
    move-object v0, v0

    .line 1816920
    invoke-direct {p0, v0}, Lcom/facebook/events/protocol/ContextParams;-><init>(LX/Blt;)V

    .line 1816921
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->l:LX/03R;

    .line 1816922
    iget-object v0, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1816923
    iget-object v1, p3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v1

    .line 1816924
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1816925
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Old and new events need to have same Event Id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1816926
    iget-object v2, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1816927
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 1816928
    iget-object v2, p3, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v2, v2

    .line 1816929
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1816930
    :cond_0
    iget-object v0, p2, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v0

    .line 1816931
    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->a:Ljava/lang/String;

    .line 1816932
    iget-object v0, p2, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v0, v0

    .line 1816933
    iget-object v1, p3, Lcom/facebook/events/model/Event;->b:Ljava/lang/String;

    move-object v1, v1

    .line 1816934
    invoke-direct {p0, v0, v1}, Lcom/facebook/events/protocol/EditEventParams;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1816935
    iget-object v0, p2, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v0, v0

    .line 1816936
    iget-object v1, p3, Lcom/facebook/events/model/Event;->c:Lcom/facebook/events/graphql/EventsGraphQLModels$EventCommonTextWithEntitiesModel;

    move-object v1, v1

    .line 1816937
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1816938
    if-eqz v1, :cond_1

    invoke-interface {v1}, LX/175;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1816939
    :cond_1
    const-string v2, ""

    iput-object v2, p0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    .line 1816940
    :cond_2
    :goto_0
    const-wide/16 v5, 0x0

    .line 1816941
    iget-wide v9, p2, Lcom/facebook/events/model/Event;->P:J

    move-wide v3, v9

    .line 1816942
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 1816943
    iget-wide v9, p3, Lcom/facebook/events/model/Event;->P:J

    move-wide v7, v9

    .line 1816944
    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1816945
    iget-wide v9, p2, Lcom/facebook/events/model/Event;->P:J

    move-wide v3, v9

    .line 1816946
    invoke-static {v3, v4}, Lcom/facebook/events/model/Event;->b(J)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1816947
    iget-object v3, p2, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v3, v3

    .line 1816948
    iget-object v4, p3, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v4, v4

    .line 1816949
    invoke-static {v3, v4}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    .line 1816950
    :cond_3
    :goto_1
    invoke-static {p2}, LX/Bm4;->a(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v0

    .line 1816951
    invoke-static {p3}, LX/Bm4;->a(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v1

    .line 1816952
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1816953
    iput-object v1, p0, Lcom/facebook/events/protocol/EditEventParams;->g:Ljava/lang/String;

    .line 1816954
    :cond_4
    invoke-static {p2}, LX/Bm4;->b(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v1

    .line 1816955
    invoke-static {p3}, LX/Bm4;->b(Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v0

    .line 1816956
    invoke-static {v1, v0}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1816957
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/events/protocol/EditEventParams;->h:Z

    .line 1816958
    if-nez v0, :cond_5

    const-string v0, "0"

    :cond_5
    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->i:Ljava/lang/String;

    .line 1816959
    :cond_6
    iget-object v0, p2, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v0, v0

    .line 1816960
    iget-object v1, p3, Lcom/facebook/events/model/Event;->f:Lcom/facebook/events/model/PrivacyType;

    move-object v1, v1

    .line 1816961
    if-eqz v0, :cond_7

    if-eqz v1, :cond_7

    if-eq v0, v1, :cond_7

    .line 1816962
    iput-object v1, p0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    .line 1816963
    :cond_7
    iget-boolean v0, p2, Lcom/facebook/events/model/Event;->h:Z

    move v0, v0

    .line 1816964
    iget-boolean v1, p3, Lcom/facebook/events/model/Event;->h:Z

    move v1, v1

    .line 1816965
    if-eq v0, v1, :cond_8

    .line 1816966
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    .line 1816967
    :cond_8
    iget-object v0, p2, Lcom/facebook/events/model/Event;->k:LX/03R;

    move-object v0, v0

    .line 1816968
    iget-object v1, p3, Lcom/facebook/events/model/Event;->k:LX/03R;

    move-object v1, v1

    .line 1816969
    if-eq v0, v1, :cond_9

    .line 1816970
    iget-object v0, p3, Lcom/facebook/events/model/Event;->k:LX/03R;

    move-object v0, v0

    .line 1816971
    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->l:LX/03R;

    .line 1816972
    :cond_9
    iget-boolean v0, p2, Lcom/facebook/events/model/Event;->j:Z

    move v0, v0

    .line 1816973
    iget-boolean v1, p3, Lcom/facebook/events/model/Event;->j:Z

    move v1, v1

    .line 1816974
    if-eq v0, v1, :cond_a

    .line 1816975
    iget-boolean v0, p3, Lcom/facebook/events/model/Event;->j:Z

    move v0, v0

    .line 1816976
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->m:Ljava/lang/Boolean;

    .line 1816977
    :cond_a
    return-void

    .line 1816978
    :cond_b
    invoke-static {v1}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 1816979
    :cond_c
    iget-wide v9, p3, Lcom/facebook/events/model/Event;->P:J

    move-wide v3, v9

    .line 1816980
    invoke-static {v3, v4}, Lcom/facebook/events/model/Event;->b(J)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 1816981
    iget-wide v9, p3, Lcom/facebook/events/model/Event;->P:J

    move-wide v3, v9

    .line 1816982
    :goto_2
    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    .line 1816983
    iget-object v3, p0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    .line 1816984
    iget-object v3, p3, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v3, v3

    .line 1816985
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/events/protocol/EditEventParams;->f:Ljava/lang/Boolean;

    .line 1816986
    iget-object v3, p3, Lcom/facebook/events/model/Event;->Q:Ljava/lang/String;

    move-object v3, v3

    .line 1816987
    iput-object v3, p0, Lcom/facebook/events/protocol/EditEventParams;->e:Ljava/lang/String;

    goto/16 :goto_1

    :cond_d
    move-wide v3, v5

    .line 1816988
    goto :goto_2
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1816989
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1816990
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "New name of event can not be empty."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1816991
    :cond_0
    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1816992
    iput-object p2, p0, Lcom/facebook/events/protocol/EditEventParams;->b:Ljava/lang/String;

    .line 1816993
    :cond_1
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1816862
    if-eqz p2, :cond_0

    .line 1816863
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816864
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 1816900
    invoke-super {p0}, Lcom/facebook/events/protocol/ContextParams;->a()Ljava/util/List;

    move-result-object v1

    .line 1816901
    const-string v0, "name"

    iget-object v2, p0, Lcom/facebook/events/protocol/EditEventParams;->b:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/facebook/events/protocol/EditEventParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1816902
    const-string v0, "location_id"

    iget-object v2, p0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    invoke-static {v1, v0, v2}, Lcom/facebook/events/protocol/EditEventParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1816903
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->f:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1816904
    const-string v2, "location"

    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-static {v1, v2, v0}, Lcom/facebook/events/protocol/EditEventParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1816905
    :cond_0
    const-string v0, "start_time"

    iget-object v2, p0, Lcom/facebook/events/protocol/EditEventParams;->g:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/facebook/events/protocol/EditEventParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1816906
    iget-boolean v0, p0, Lcom/facebook/events/protocol/EditEventParams;->h:Z

    if-eqz v0, :cond_1

    .line 1816907
    const-string v0, "end_time"

    iget-object v2, p0, Lcom/facebook/events/protocol/EditEventParams;->i:Ljava/lang/String;

    invoke-static {v1, v0, v2}, Lcom/facebook/events/protocol/EditEventParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1816908
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1816909
    const-string v2, "description"

    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_1
    invoke-static {v1, v2, v0}, Lcom/facebook/events/protocol/EditEventParams;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1816910
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    if-eqz v0, :cond_3

    .line 1816911
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "privacy_type"

    iget-object v3, p0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    invoke-static {v3}, LX/Bm4;->a(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816912
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1816913
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "can_guests_invite_friends"

    iget-object v3, p0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1816914
    :cond_4
    return-object v1

    .line 1816915
    :cond_5
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->e:Ljava/lang/String;

    goto :goto_0

    .line 1816916
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1816865
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1816866
    invoke-super {p0, p1, p2}, Lcom/facebook/events/protocol/ContextParams;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1816867
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816868
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816869
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816870
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    if-nez v0, :cond_5

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816871
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 1816872
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1816873
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816874
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->f:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    .line 1816875
    const/4 v0, -0x1

    move-object v3, p1

    .line 1816876
    :goto_1
    invoke-virtual {v3, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816877
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816878
    iget-boolean v0, p0, Lcom/facebook/events/protocol/EditEventParams;->h:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816879
    iget-boolean v0, p0, Lcom/facebook/events/protocol/EditEventParams;->h:Z

    if-eqz v0, :cond_1

    .line 1816880
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816881
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    if-nez v0, :cond_9

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816882
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    if-eqz v0, :cond_2

    .line 1816883
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->j:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0}, Lcom/facebook/events/model/PrivacyType;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816884
    :cond_2
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816885
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 1816886
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v2

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816887
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->l:LX/03R;

    invoke-virtual {v0}, LX/03R;->getDbValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816888
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->m:Ljava/lang/Boolean;

    if-nez v0, :cond_c

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816889
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1816890
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_d

    :goto_7
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816891
    :cond_4
    return-void

    :cond_5
    move v0, v2

    .line 1816892
    goto/16 :goto_0

    .line 1816893
    :cond_6
    iget-object v0, p0, Lcom/facebook/events/protocol/EditEventParams;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    move-object v3, p1

    goto :goto_1

    :cond_7
    move v0, v1

    move-object v3, p1

    goto :goto_1

    :cond_8
    move v0, v1

    .line 1816894
    goto :goto_2

    :cond_9
    move v0, v2

    .line 1816895
    goto :goto_3

    :cond_a
    move v0, v2

    .line 1816896
    goto :goto_4

    :cond_b
    move v0, v1

    .line 1816897
    goto :goto_5

    :cond_c
    move v0, v2

    .line 1816898
    goto :goto_6

    :cond_d
    move v2, v1

    .line 1816899
    goto :goto_7
.end method
