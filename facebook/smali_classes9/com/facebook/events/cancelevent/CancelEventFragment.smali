.class public Lcom/facebook/events/cancelevent/CancelEventFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""

# interfaces
.implements LX/0fj;


# instance fields
.field public a:LX/7vP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0zG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Bl6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Landroid/content/ContentResolver;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Bky;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field private j:Lcom/facebook/events/common/EventAnalyticsParams;

.field private k:Lcom/facebook/user/model/User;

.field public l:I

.field public m:I

.field public n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

.field private o:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public p:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field public q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

.field private r:Lcom/facebook/resources/ui/FbButton;

.field public s:Landroid/support/v4/app/DialogFragment;

.field private final t:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$CancelEventMutationModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final u:LX/0Ve;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ve",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/events/graphql/EventsMutationsModels$EventSoftCancelMutationModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1810329
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1810330
    new-instance v0, LX/BiN;

    invoke-direct {v0, p0}, LX/BiN;-><init>(Lcom/facebook/events/cancelevent/CancelEventFragment;)V

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->t:LX/0Ve;

    .line 1810331
    new-instance v0, LX/BiO;

    invoke-direct {v0, p0}, LX/BiO;-><init>(Lcom/facebook/events/cancelevent/CancelEventFragment;)V

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->u:LX/0Ve;

    return-void
.end method

.method public static c$redex0(Lcom/facebook/events/cancelevent/CancelEventFragment;)V
    .locals 8

    .prologue
    .line 1810297
    iget v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->m:I

    iget v1, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->l:I

    if-ne v0, v1, :cond_1

    .line 1810298
    :cond_0
    :goto_0
    return-void

    .line 1810299
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    if-nez v0, :cond_2

    .line 1810300
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "EventActionContext not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1810301
    :cond_2
    const/4 v2, 0x1

    .line 1810302
    iget v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->m:I

    if-ne v0, v2, :cond_5

    const v0, 0x7f082aea

    .line 1810303
    :goto_1
    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(IZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->s:Landroid/support/v4/app/DialogFragment;

    .line 1810304
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->s:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 1810305
    iget v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->m:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 1810306
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->a:LX/7vP;

    iget-object v1, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;->getEncodedText()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->CANCEL_EVENT_FLOW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v4}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1810307
    new-instance v5, LX/4EG;

    invoke-direct {v5}, LX/4EG;-><init>()V

    .line 1810308
    iget-object v6, v3, Lcom/facebook/events/common/EventAnalyticsParams;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1810309
    new-instance v6, LX/4EG;

    invoke-direct {v6}, LX/4EG;-><init>()V

    .line 1810310
    iget-object v7, v3, Lcom/facebook/events/common/EventAnalyticsParams;->d:Ljava/lang/String;

    invoke-virtual {v6, v7}, LX/4EG;->a(Ljava/lang/String;)LX/4EG;

    .line 1810311
    invoke-virtual {v6, v4}, LX/4EG;->b(Ljava/lang/String;)LX/4EG;

    .line 1810312
    new-instance v7, LX/4EL;

    invoke-direct {v7}, LX/4EL;-><init>()V

    .line 1810313
    invoke-static {v5, v6}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/4EL;->a(Ljava/util/List;)LX/4EL;

    .line 1810314
    new-instance v5, LX/4EV;

    invoke-direct {v5}, LX/4EV;-><init>()V

    .line 1810315
    const-string v6, "event_id"

    invoke-virtual {v5, v6, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1810316
    move-object v5, v5

    .line 1810317
    const-string v6, "context"

    invoke-virtual {v5, v6, v7}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 1810318
    move-object v5, v5

    .line 1810319
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 1810320
    const-string v6, "cancellation_reason"

    invoke-virtual {v5, v6, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1810321
    :cond_3
    new-instance v6, LX/7uL;

    invoke-direct {v6}, LX/7uL;-><init>()V

    move-object v6, v6

    .line 1810322
    const-string v7, "input"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/7uL;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 1810323
    iget-object v6, v0, LX/7vP;->a:LX/0tX;

    invoke-virtual {v6, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v0, v5

    .line 1810324
    iget-object v1, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->d:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cancel_event_task"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->u:LX/0Ve;

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0

    .line 1810325
    :cond_4
    iget v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->m:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1810326
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->a:LX/7vP;

    iget-object v1, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    sget-object v3, Lcom/facebook/events/common/ActionMechanism;->CANCEL_EVENT_FLOW:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v3}, Lcom/facebook/events/common/ActionMechanism;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/7vP;->a(Ljava/lang/String;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 1810327
    iget-object v1, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->d:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cancel_event_task"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->t:LX/0Ve;

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0

    .line 1810328
    :cond_5
    const v0, 0x7f082ae9

    goto/16 :goto_1
.end method

.method public static final k(Lcom/facebook/events/cancelevent/CancelEventFragment;)V
    .locals 3

    .prologue
    .line 1810287
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->s:Landroid/support/v4/app/DialogFragment;

    if-eqz v0, :cond_0

    .line 1810288
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->s:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V

    .line 1810289
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->s:Landroid/support/v4/app/DialogFragment;

    .line 1810290
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1810291
    const-string v1, "extra_event_cancel_state"

    iget v2, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->l:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1810292
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    .line 1810293
    if-eqz v1, :cond_1

    .line 1810294
    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 1810295
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 1810296
    :cond_1
    return-void
.end method


# virtual methods
.method public final S_()Z
    .locals 1

    .prologue
    .line 1810332
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->u:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 1810333
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->t:LX/0Ve;

    invoke-interface {v0}, LX/0Ve;->dispose()V

    .line 1810334
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 1810274
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1810275
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/events/cancelevent/CancelEventFragment;

    invoke-static {v0}, LX/7vP;->b(LX/0QB;)LX/7vP;

    move-result-object v3

    check-cast v3, LX/7vP;

    invoke-static {v0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object v4

    check-cast v4, LX/0zG;

    const/16 v5, 0x12cb

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v7

    check-cast v7, LX/Bl6;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v8

    check-cast v8, LX/0TD;

    invoke-static {v0}, LX/0cd;->b(LX/0QB;)Landroid/content/ContentResolver;

    move-result-object p1

    check-cast p1, Landroid/content/ContentResolver;

    invoke-static {v0}, LX/Bky;->b(LX/0QB;)LX/Bky;

    move-result-object v0

    check-cast v0, LX/Bky;

    iput-object v3, v2, Lcom/facebook/events/cancelevent/CancelEventFragment;->a:LX/7vP;

    iput-object v4, v2, Lcom/facebook/events/cancelevent/CancelEventFragment;->b:LX/0zG;

    iput-object v5, v2, Lcom/facebook/events/cancelevent/CancelEventFragment;->c:LX/0Or;

    iput-object v6, v2, Lcom/facebook/events/cancelevent/CancelEventFragment;->d:LX/1Ck;

    iput-object v7, v2, Lcom/facebook/events/cancelevent/CancelEventFragment;->e:LX/Bl6;

    iput-object v8, v2, Lcom/facebook/events/cancelevent/CancelEventFragment;->f:LX/0TD;

    iput-object p1, v2, Lcom/facebook/events/cancelevent/CancelEventFragment;->g:Landroid/content/ContentResolver;

    iput-object v0, v2, Lcom/facebook/events/cancelevent/CancelEventFragment;->h:LX/Bky;

    .line 1810276
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1810277
    const-string v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->i:Ljava/lang/String;

    .line 1810278
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1810279
    const-string v1, "extras_event_analytics_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/EventAnalyticsParams;

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->j:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 1810280
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->k:Lcom/facebook/user/model/User;

    .line 1810281
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1810282
    const-string v1, "extra_is_event_canceled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1810283
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->l:I

    .line 1810284
    iget v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->l:I

    iput v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->m:I

    .line 1810285
    return-void

    .line 1810286
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x4ec07f95

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1810258
    const v1, 0x7f030239

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, 0x3c230814

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3471908b    # -1.866929E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1810271
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1810272
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->b:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const v2, 0x7f082ae1

    invoke-interface {v0, v2}, LX/0h5;->setTitle(I)V

    .line 1810273
    const/16 v0, 0x2b

    const v2, 0xeec1997

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1810259
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1810260
    const v0, 0x7f0d0897

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->o:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 1810261
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->o:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->k:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 1810262
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->o:Lcom/facebook/fbui/widget/contentview/ContentView;

    iget-object v1, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->k:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 1810263
    const v0, 0x7f0d0895

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->p:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 1810264
    const v0, 0x7f0d0896

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    .line 1810265
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->p:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    new-instance v1, LX/BiQ;

    invoke-direct {v1, p0}, LX/BiQ;-><init>(Lcom/facebook/events/cancelevent/CancelEventFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810266
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->q:Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    new-instance v1, LX/BiR;

    invoke-direct {v1, p0}, LX/BiR;-><init>(Lcom/facebook/events/cancelevent/CancelEventFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/CheckedContentView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810267
    const v0, 0x7f0d0898

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->n:Lcom/facebook/tagging/ui/MentionsAutoCompleteTextView;

    .line 1810268
    const v0, 0x7f0d0899

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->r:Lcom/facebook/resources/ui/FbButton;

    .line 1810269
    iget-object v0, p0, Lcom/facebook/events/cancelevent/CancelEventFragment;->r:Lcom/facebook/resources/ui/FbButton;

    new-instance v1, LX/BiP;

    invoke-direct {v1, p0}, LX/BiP;-><init>(Lcom/facebook/events/cancelevent/CancelEventFragment;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1810270
    return-void
.end method
