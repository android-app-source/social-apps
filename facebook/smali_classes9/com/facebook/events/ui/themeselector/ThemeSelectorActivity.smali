.class public Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public A:I

.field public B:Z

.field public final C:LX/1DI;

.field public p:LX/BnE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Bn3;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/Bn2;

.field public s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public t:Lcom/facebook/fbui/pagerindicator/TabbedViewPagerIndicator;

.field public u:LX/BnD;

.field public v:Landroid/support/v4/view/ViewPager;

.field public w:Landroid/view/ViewStub;

.field public x:Landroid/view/View;

.field public y:Ljava/lang/String;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1820085
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1820086
    new-instance v0, LX/BnH;

    invoke-direct {v0, p0}, LX/BnH;-><init>(Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;)V

    iput-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->C:LX/1DI;

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1820079
    invoke-static {p0}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1820080
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 1820081
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/0h5;->setShowDividers(Z)V

    .line 1820082
    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0h5;->setHasBackButton(Z)V

    .line 1820083
    new-instance v1, LX/BnI;

    invoke-direct {v1, p0}, LX/BnI;-><init>(Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;)V

    invoke-interface {v0, v1}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1820084
    return-void
.end method

.method private static a(Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;LX/BnE;LX/Bn3;)V
    .locals 0

    .prologue
    .line 1820078
    iput-object p1, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->p:LX/BnE;

    iput-object p2, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->q:LX/Bn3;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;

    const-class v0, LX/BnE;

    invoke-interface {v1, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/BnE;

    const-class v2, LX/Bn3;

    invoke-interface {v1, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/Bn3;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->a(Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;LX/BnE;LX/Bn3;)V

    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 1820073
    iget-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->p:LX/BnE;

    new-instance v1, LX/BnJ;

    invoke-direct {v1, p0}, LX/BnJ;-><init>(Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;)V

    .line 1820074
    new-instance v4, LX/BnD;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-direct {v4, v2, v3, v1}, LX/BnD;-><init>(LX/0tX;LX/1Ck;LX/BnJ;)V

    .line 1820075
    move-object v0, v4

    .line 1820076
    iput-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->u:LX/BnD;

    .line 1820077
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1820057
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1820058
    invoke-static {p0, p0}, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1820059
    const v0, 0x7f030594

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->setContentView(I)V

    .line 1820060
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1820061
    const-string v1, "extra_theme_selector_event_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1820062
    const-string v1, "extra_theme_selector_event_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->y:Ljava/lang/String;

    .line 1820063
    :cond_0
    const-string v1, "extra_theme_selector_event_description"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1820064
    const-string v1, "extra_theme_selector_event_description"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->z:Ljava/lang/String;

    .line 1820065
    :cond_1
    const-string v1, "extra_show_full_width_themes"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->A:I

    .line 1820066
    invoke-direct {p0}, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->a()V

    .line 1820067
    const v0, 0x7f0d0a95

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1820068
    const v0, 0x7f0d0f5b

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->w:Landroid/view/ViewStub;

    .line 1820069
    invoke-direct {p0}, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->b()V

    .line 1820070
    iget-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->u:LX/BnD;

    invoke-virtual {v0}, LX/BnD;->a()V

    .line 1820071
    iget-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->s:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1820072
    return-void
.end method

.method public final onDestroy()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x22

    const v1, 0x507d7fd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1820047
    invoke-super {p0}, Lcom/facebook/base/activity/FbFragmentActivity;->onDestroy()V

    .line 1820048
    iget-object v1, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->r:LX/Bn2;

    if-eqz v1, :cond_2

    .line 1820049
    iget-object v1, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->r:LX/Bn2;

    .line 1820050
    iget-object v2, v1, LX/Bn2;->e:Ljava/util/Set;

    if-eqz v2, :cond_1

    .line 1820051
    iget-object v2, v1, LX/Bn2;->e:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Bn8;

    .line 1820052
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, LX/Bn8;->b(Landroid/view/ViewGroup;)V

    goto :goto_0

    .line 1820053
    :cond_0
    iget-object v2, v1, LX/Bn2;->e:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->clear()V

    .line 1820054
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->r:LX/Bn2;

    .line 1820055
    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/events/ui/themeselector/ThemeSelectorActivity;->B:Z

    .line 1820056
    const/16 v1, 0x23

    const v2, -0x42a1a020

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
