.class public Lcom/facebook/events/ui/themeselector/ThemeViewHolder;
.super LX/1a1;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/String;

.field public o:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1820097
    const-class v0, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1820098
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 1820099
    const v0, 0x7f0d0f57

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1820100
    new-instance v0, LX/BnK;

    invoke-direct {v0, p0}, LX/BnK;-><init>(Lcom/facebook/events/ui/themeselector/ThemeViewHolder;)V

    iput-object v0, p0, Lcom/facebook/events/ui/themeselector/ThemeViewHolder;->o:Landroid/view/View$OnClickListener;

    .line 1820101
    return-void
.end method
