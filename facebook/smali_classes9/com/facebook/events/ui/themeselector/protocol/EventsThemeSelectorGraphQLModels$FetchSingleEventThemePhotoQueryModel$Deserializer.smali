.class public final Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1820616
    const-class v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;

    new-instance v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1820617
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1820618
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1820619
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1820620
    const/4 v2, 0x0

    .line 1820621
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_7

    .line 1820622
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1820623
    :goto_0
    move v1, v2

    .line 1820624
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1820625
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1820626
    new-instance v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;

    invoke-direct {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;-><init>()V

    .line 1820627
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1820628
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1820629
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1820630
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1820631
    :cond_0
    return-object v1

    .line 1820632
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1820633
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_6

    .line 1820634
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1820635
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1820636
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, p0, :cond_2

    if-eqz v5, :cond_2

    .line 1820637
    const-string v6, "__type__"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    const-string v6, "__typename"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1820638
    :cond_3
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v4

    goto :goto_1

    .line 1820639
    :cond_4
    const-string v6, "id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1820640
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    goto :goto_1

    .line 1820641
    :cond_5
    const-string v6, "themeListImage"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1820642
    const/4 v5, 0x0

    .line 1820643
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v6, :cond_b

    .line 1820644
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1820645
    :goto_2
    move v1, v5

    .line 1820646
    goto :goto_1

    .line 1820647
    :cond_6
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1820648
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1820649
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1820650
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1820651
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_7
    move v1, v2

    move v3, v2

    move v4, v2

    goto :goto_1

    .line 1820652
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1820653
    :cond_9
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, p0, :cond_a

    .line 1820654
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 1820655
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1820656
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_9

    if-eqz v6, :cond_9

    .line 1820657
    const-string p0, "uri"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 1820658
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto :goto_3

    .line 1820659
    :cond_a
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, LX/186;->c(I)V

    .line 1820660
    invoke-virtual {v0, v5, v1}, LX/186;->b(II)V

    .line 1820661
    invoke-virtual {v0}, LX/186;->d()I

    move-result v5

    goto :goto_2

    :cond_b
    move v1, v5

    goto :goto_3
.end method
