.class public final Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1820380
    const-class v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;

    new-instance v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1820381
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1820432
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1820382
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1820383
    const/4 v2, 0x0

    .line 1820384
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1820385
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1820386
    :goto_0
    move v1, v2

    .line 1820387
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1820388
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1820389
    new-instance v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;

    invoke-direct {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;-><init>()V

    .line 1820390
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1820391
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1820392
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1820393
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1820394
    :cond_0
    return-object v1

    .line 1820395
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1820396
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1820397
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1820398
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1820399
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1820400
    const-string v4, "event_themes"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1820401
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1820402
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_c

    .line 1820403
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1820404
    :goto_2
    move v1, v3

    .line 1820405
    goto :goto_1

    .line 1820406
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1820407
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1820408
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1820409
    :cond_5
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v8, p0, :cond_a

    .line 1820410
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v8

    .line 1820411
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1820412
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_5

    if-eqz v8, :cond_5

    .line 1820413
    const-string p0, "count"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_6

    .line 1820414
    invoke-virtual {p1}, LX/15w;->E()I

    move-result v1

    move v7, v1

    move v1, v4

    goto :goto_3

    .line 1820415
    :cond_6
    const-string p0, "nodes"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_8

    .line 1820416
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1820417
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->START_ARRAY:LX/15z;

    if-ne v8, p0, :cond_7

    .line 1820418
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v8

    sget-object p0, LX/15z;->END_ARRAY:LX/15z;

    if-eq v8, p0, :cond_7

    .line 1820419
    invoke-static {p1, v0}, LX/BnU;->b(LX/15w;LX/186;)I

    move-result v8

    .line 1820420
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1820421
    :cond_7
    invoke-static {v6, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 1820422
    goto :goto_3

    .line 1820423
    :cond_8
    const-string p0, "page_info"

    invoke-virtual {v8, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 1820424
    invoke-static {p1, v0}, LX/5Mh;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_3

    .line 1820425
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1820426
    :cond_a
    const/4 v8, 0x3

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 1820427
    if-eqz v1, :cond_b

    .line 1820428
    invoke-virtual {v0, v3, v7, v3}, LX/186;->a(III)V

    .line 1820429
    :cond_b
    invoke-virtual {v0, v4, v6}, LX/186;->b(II)V

    .line 1820430
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 1820431
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_c
    move v1, v3

    move v5, v3

    move v6, v3

    move v7, v3

    goto/16 :goto_3
.end method
