.class public final Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1820235
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1820236
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1820233
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1820234
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 1820194
    if-nez p1, :cond_0

    .line 1820195
    :goto_0
    return v0

    .line 1820196
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1820197
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1820198
    :sswitch_0
    const-class v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1820199
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1820200
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1820201
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1820202
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1820203
    :sswitch_1
    invoke-virtual {p0, p1, v0, v0}, LX/15i;->a(III)I

    move-result v1

    .line 1820204
    const-class v2, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    invoke-virtual {p0, p1, v5, v2}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v2

    invoke-static {v2}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v2

    .line 1820205
    invoke-static {p3, v2}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v2

    .line 1820206
    invoke-virtual {p0, p1, v6}, LX/15i;->p(II)I

    move-result v3

    .line 1820207
    const v4, 0x4e363642    # 7.6425229E8f

    invoke-static {p0, v3, v4, p3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v3

    .line 1820208
    const/4 v4, 0x3

    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1820209
    invoke-virtual {p3, v0, v1, v0}, LX/186;->a(III)V

    .line 1820210
    invoke-virtual {p3, v5, v2}, LX/186;->b(II)V

    .line 1820211
    invoke-virtual {p3, v6, v3}, LX/186;->b(II)V

    .line 1820212
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1820213
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1820214
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1820215
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1820216
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1820217
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1820218
    :sswitch_3
    const-class v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel$ThemeTagsModel$ThemeTagsNodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v1

    invoke-static {v1}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v1

    .line 1820219
    invoke-static {p3, v1}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v1

    .line 1820220
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1820221
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1820222
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1820223
    :sswitch_4
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1820224
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1820225
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1820226
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1820227
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1820228
    :sswitch_5
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1820229
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1820230
    invoke-virtual {p3, v5}, LX/186;->c(I)V

    .line 1820231
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1820232
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x280c7f34 -> :sswitch_3
        -0x11ea753d -> :sswitch_2
        0x16efa710 -> :sswitch_4
        0x177163e2 -> :sswitch_0
        0x4233daab -> :sswitch_5
        0x767d2dc3 -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1820193
    new-instance v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0Px;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(",
            "LX/0Px",
            "<TT;>;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1820189
    invoke-static {p0, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v0

    .line 1820190
    if-eqz v0, :cond_0

    .line 1820191
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1820192
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1820178
    sparse-switch p2, :sswitch_data_0

    .line 1820179
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1820180
    :sswitch_0
    const-class v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeCategoriesModel$EventCoverPhotoThemeCategoriesModel$NodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1820181
    invoke-static {v0, p3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1820182
    :goto_0
    :sswitch_1
    return-void

    .line 1820183
    :sswitch_2
    const/4 v0, 0x1

    const-class v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1820184
    invoke-static {v0, p3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    .line 1820185
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1820186
    const v1, 0x4e363642    # 7.6425229E8f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    .line 1820187
    :sswitch_3
    const-class v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel$ThemeTagsModel$ThemeTagsNodesModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->e(IILjava/lang/Class;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, LX/17G;->a(Ljava/util/Iterator;)LX/0Px;

    move-result-object v0

    .line 1820188
    invoke-static {v0, p3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/0Px;LX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x280c7f34 -> :sswitch_3
        -0x11ea753d -> :sswitch_1
        0x16efa710 -> :sswitch_1
        0x177163e2 -> :sswitch_0
        0x4233daab -> :sswitch_1
        0x767d2dc3 -> :sswitch_2
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1820172
    if-eqz p1, :cond_0

    .line 1820173
    invoke-static {p0, p1, p2}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    move-result-object v1

    .line 1820174
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    .line 1820175
    if-eq v0, v1, :cond_0

    .line 1820176
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1820177
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1820171
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1820138
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1820139
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1820166
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1820167
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1820168
    :cond_0
    iput-object p1, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1820169
    iput p2, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->b:I

    .line 1820170
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1820165
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1820164
    new-instance v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1820161
    iget v0, p0, LX/1vt;->c:I

    .line 1820162
    move v0, v0

    .line 1820163
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1820158
    iget v0, p0, LX/1vt;->c:I

    .line 1820159
    move v0, v0

    .line 1820160
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1820155
    iget v0, p0, LX/1vt;->b:I

    .line 1820156
    move v0, v0

    .line 1820157
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1820152
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1820153
    move-object v0, v0

    .line 1820154
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1820143
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1820144
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1820145
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1820146
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1820147
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1820148
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1820149
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1820150
    invoke-static {v3, v9, v2}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1820151
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1820140
    iget v0, p0, LX/1vt;->c:I

    .line 1820141
    move v0, v0

    .line 1820142
    return v0
.end method
