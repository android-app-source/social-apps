.class public final Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1820665
    const-class v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;

    new-instance v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1820666
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1820664
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;LX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 1820668
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1820669
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 p0, 0x0

    .line 1820670
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1820671
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1820672
    if-eqz v2, :cond_0

    .line 1820673
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820674
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1820675
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1820676
    if-eqz v2, :cond_1

    .line 1820677
    const-string p0, "id"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820678
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1820679
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1820680
    if-eqz v2, :cond_3

    .line 1820681
    const-string p0, "themeListImage"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820682
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1820683
    const/4 p0, 0x0

    invoke-virtual {v1, v2, p0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object p0

    .line 1820684
    if-eqz p0, :cond_2

    .line 1820685
    const-string v0, "uri"

    invoke-virtual {p1, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820686
    invoke-virtual {p1, p0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1820687
    :cond_2
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1820688
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1820689
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1820667
    check-cast p1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel$Serializer;->a(Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$FetchSingleEventThemePhotoQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
