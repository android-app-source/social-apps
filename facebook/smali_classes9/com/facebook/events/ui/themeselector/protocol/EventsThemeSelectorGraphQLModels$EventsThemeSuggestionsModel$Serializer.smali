.class public final Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1820555
    const-class v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;

    new-instance v1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1820556
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1820557
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1820558
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1820559
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1820560
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1820561
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1820562
    if-eqz v2, :cond_4

    .line 1820563
    const-string v3, "event_themes"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820564
    const/4 v3, 0x0

    .line 1820565
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1820566
    invoke-virtual {v1, v2, v3, v3}, LX/15i;->a(III)I

    move-result v3

    .line 1820567
    if-eqz v3, :cond_0

    .line 1820568
    const-string p0, "count"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820569
    invoke-virtual {p1, v3}, LX/0nX;->b(I)V

    .line 1820570
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1820571
    if-eqz v3, :cond_2

    .line 1820572
    const-string p0, "nodes"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820573
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1820574
    const/4 p0, 0x0

    :goto_0
    invoke-virtual {v1, v3}, LX/15i;->c(I)I

    move-result v0

    if-ge p0, v0, :cond_1

    .line 1820575
    invoke-virtual {v1, v3, p0}, LX/15i;->q(II)I

    move-result v0

    invoke-static {v1, v0, p1, p2}, LX/BnU;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1820576
    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    .line 1820577
    :cond_1
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1820578
    :cond_2
    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, LX/15i;->g(II)I

    move-result v3

    .line 1820579
    if-eqz v3, :cond_3

    .line 1820580
    const-string p0, "page_info"

    invoke-virtual {p1, p0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1820581
    invoke-static {v1, v3, p1}, LX/5Mh;->a(LX/15i;ILX/0nX;)V

    .line 1820582
    :cond_3
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1820583
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1820584
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1820585
    check-cast p1, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$Serializer;->a(Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel;LX/0nX;LX/0my;)V

    return-void
.end method
