.class public final Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x5e8893ad
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel$Serializer;
.end annotation


# instance fields
.field private e:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1820553
    const-class v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1820552
    const-class v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1820550
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1820551
    return-void
.end method

.method private m()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThemeTags"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1820548
    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1820549
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->h:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 6

    .prologue
    .line 1820536
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1820537
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v2, -0x11ea753d

    invoke-static {v1, v0, v2}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1820538
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1820539
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const v4, 0x16efa710

    invoke-static {v3, v2, v4}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    move-result-object v2

    invoke-static {p1, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v2

    .line 1820540
    invoke-direct {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->m()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const v5, -0x280c7f34

    invoke-static {v4, v3, v5}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1820541
    const/4 v4, 0x4

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 1820542
    const/4 v4, 0x0

    invoke-virtual {p1, v4, v0}, LX/186;->b(II)V

    .line 1820543
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1820544
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1820545
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1820546
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1820547
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1820514
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1820515
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1820516
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x11ea753d

    invoke-static {v2, v0, v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1820517
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1820518
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    .line 1820519
    iput v3, v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->e:I

    move-object v1, v0

    .line 1820520
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1820521
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x16efa710

    invoke-static {v2, v0, v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1820522
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1820523
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    .line 1820524
    iput v3, v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->g:I

    move-object v1, v0

    .line 1820525
    :cond_1
    invoke-direct {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 1820526
    invoke-direct {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x280c7f34

    invoke-static {v2, v0, v3}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_2
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 1820527
    invoke-direct {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1820528
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    .line 1820529
    iput v3, v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->h:I

    move-object v1, v0

    .line 1820530
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1820531
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    .line 1820532
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 1820533
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 1820534
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0

    :cond_3
    move-object p0, v1

    .line 1820535
    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1820554
    invoke-virtual {p0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1820509
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1820510
    const/4 v0, 0x0

    const v1, -0x11ea753d

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->e:I

    .line 1820511
    const/4 v0, 0x2

    const v1, 0x16efa710

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->g:I

    .line 1820512
    const/4 v0, 0x3

    const v1, -0x280c7f34

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->h:I

    .line 1820513
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1820506
    new-instance v0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;

    invoke-direct {v0}, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;-><init>()V

    .line 1820507
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1820508
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1820498
    const v0, -0x513df8a1

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1820505
    const v0, -0x3543e3fd    # -6163969.5f

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getCoverPhotoImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1820503
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1820504
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->e:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1820501
    iget-object v0, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->f:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->f:Ljava/lang/String;

    .line 1820502
    iget-object v0, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final l()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getThemeListImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1820499
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1820500
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/events/ui/themeselector/protocol/EventsThemeSelectorGraphQLModels$EventsThemeSuggestionsModel$EventThemesModel$NodesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
