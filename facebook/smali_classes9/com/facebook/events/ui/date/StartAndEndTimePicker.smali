.class public Lcom/facebook/events/ui/date/StartAndEndTimePicker;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/events/ui/date/DatePickerView;

.field public c:Lcom/facebook/events/ui/date/TimePickerView;

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/events/ui/date/DatePickerView;

.field public f:Lcom/facebook/events/ui/date/TimePickerView;

.field private g:Ljava/util/TimeZone;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1818578
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1818579
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->h:Z

    .line 1818580
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a()V

    .line 1818581
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1818574
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1818575
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->h:Z

    .line 1818576
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a()V

    .line 1818577
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1818570
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1818571
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->h:Z

    .line 1818572
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a()V

    .line 1818573
    return-void
.end method

.method private static a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/TimeZone;)Ljava/util/Date;
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 1818558
    invoke-static {p2}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 1818559
    iget-object v1, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    move-object v3, v1

    .line 1818560
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 1818561
    if-nez v3, :cond_0

    .line 1818562
    const/4 v0, 0x0

    .line 1818563
    :goto_0
    return-object v0

    .line 1818564
    :cond_0
    iget-object v1, p1, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v1, v1

    .line 1818565
    if-nez v1, :cond_1

    .line 1818566
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 1818567
    :goto_1
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v0

    goto :goto_0

    .line 1818568
    :cond_1
    iget-object v1, p1, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v5, v1

    .line 1818569
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v4, 0xb

    invoke-virtual {v5, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    goto :goto_1
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1818471
    const-class v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-static {v0, p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1818472
    const v0, 0x7f0313b9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1818473
    const v0, 0x7f0d2d7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/DatePickerView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818474
    const v0, 0x7f0d2d80

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/TimePickerView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818475
    const v0, 0x7f0d2d81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->d:Landroid/view/View;

    .line 1818476
    const v0, 0x7f0d2d82

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/DatePickerView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818477
    const v0, 0x7f0d2d83

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/TimePickerView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818478
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c()V

    .line 1818479
    return-void
.end method

.method private static a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V
    .locals 1

    .prologue
    .line 1818552
    invoke-static {p3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 1818553
    invoke-virtual {v0, p2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1818554
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    .line 1818555
    invoke-virtual {p1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818556
    invoke-static {p1, p2, p3}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    .line 1818557
    return-void
.end method

.method public static a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V
    .locals 2

    .prologue
    .line 1818548
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1818549
    if-eqz p1, :cond_0

    .line 1818550
    invoke-virtual {p2, p1}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Ljava/util/TimeZone;->getDisplayName(ZI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setAppendedText(Ljava/lang/String;)V

    .line 1818551
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a:LX/0kL;

    return-void
.end method

.method private a(Ljava/util/Date;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1818532
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 1818533
    if-eqz p1, :cond_0

    .line 1818534
    invoke-virtual {v0, p1}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1818535
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    .line 1818536
    if-nez p1, :cond_2

    .line 1818537
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 1818538
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 1818539
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 1818540
    const/16 v1, 0xb

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1818541
    :cond_1
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818542
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1818543
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getStartDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    .line 1818544
    return-void

    .line 1818545
    :cond_2
    if-eqz p2, :cond_1

    .line 1818546
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0}, Lcom/facebook/events/ui/date/TimePickerView;->a()V

    .line 1818547
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;)V
    .locals 0

    .prologue
    .line 1818582
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/DatePickerView;->a()V

    .line 1818583
    invoke-virtual {p1}, Lcom/facebook/events/ui/date/TimePickerView;->a()V

    .line 1818584
    return-void
.end method

.method public static b(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V
    .locals 10

    .prologue
    const-wide/32 v8, 0x48190800

    const/4 v6, 0x1

    .line 1818453
    iput-boolean v6, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->h:Z

    .line 1818454
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getEndDate()Ljava/util/Date;

    move-result-object v0

    .line 1818455
    if-nez v0, :cond_1

    .line 1818456
    :cond_0
    :goto_0
    return-void

    .line 1818457
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getStartDate()Ljava/util/Date;

    move-result-object v1

    .line 1818458
    invoke-virtual {v1, v0}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v2

    if-ltz v2, :cond_2

    .line 1818459
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/32 v4, 0xa4cb80

    add-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1818460
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 1818461
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1818462
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    .line 1818463
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818464
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082176

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_0

    .line 1818465
    :cond_2
    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    cmp-long v0, v2, v8

    if-lez v0, :cond_0

    .line 1818466
    new-instance v0, Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    add-long/2addr v2, v8

    const-wide/32 v4, 0x5265c00

    sub-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1818467
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 1818468
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1818469
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    .line 1818470
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a:LX/0kL;

    new-instance v1, LX/27k;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f082177

    new-array v4, v6, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-wide/16 v6, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto/16 :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1818480
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818481
    iput-boolean v2, v0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    .line 1818482
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    new-instance v1, LX/BmS;

    invoke-direct {v1, p0}, LX/BmS;-><init>(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V

    .line 1818483
    iput-object v1, v0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    .line 1818484
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818485
    iput-boolean v2, v0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    .line 1818486
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    new-instance v1, LX/BmT;

    invoke-direct {v1, p0}, LX/BmT;-><init>(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V

    .line 1818487
    iput-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 1818488
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818489
    iput-boolean v3, v0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    .line 1818490
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    new-instance v1, LX/BmU;

    invoke-direct {v1, p0}, LX/BmU;-><init>(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V

    .line 1818491
    iput-object v1, v0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    .line 1818492
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818493
    iput-boolean v3, v0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    .line 1818494
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    new-instance v1, LX/BmV;

    invoke-direct {v1, p0}, LX/BmV;-><init>(Lcom/facebook/events/ui/date/StartAndEndTimePicker;)V

    .line 1818495
    iput-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 1818496
    return-void
.end method


# virtual methods
.method public getEndDate()Ljava/util/Date;
    .locals 3

    .prologue
    .line 1818497
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method public getIsDayEvent()Z
    .locals 1

    .prologue
    .line 1818498
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818499
    iget-object p0, v0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v0, p0

    .line 1818500
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStartDate()Ljava/util/Date;
    .locals 3

    .prologue
    .line 1818501
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/TimeZone;)Ljava/util/Date;

    move-result-object v0

    .line 1818502
    if-nez v0, :cond_0

    .line 1818503
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 1818504
    :cond_0
    return-object v0
.end method

.method public getTimeZone()Ljava/util/TimeZone;
    .locals 2

    .prologue
    .line 1818505
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->g:Ljava/util/TimeZone;

    if-nez v0, :cond_0

    .line 1818506
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Should not be getting time zone before it has been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1818507
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->g:Ljava/util/TimeZone;

    return-object v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1818508
    instance-of v0, p1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;

    if-nez v0, :cond_0

    .line 1818509
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1818510
    :goto_0
    return-void

    .line 1818511
    :cond_0
    check-cast p1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;

    .line 1818512
    invoke-virtual {p1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1818513
    iget-object v0, p1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->a:Ljava/util/Date;

    iget-boolean v1, p1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->b:Z

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Ljava/util/Date;Z)V

    .line 1818514
    iget-object v0, p1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->c:Ljava/util/Date;

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->setEndDate(Ljava/util/Date;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1818515
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1818516
    new-instance v1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1818517
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getStartDate()Ljava/util/Date;

    move-result-object v0

    .line 1818518
    iput-object v0, v1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->a:Ljava/util/Date;

    .line 1818519
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getIsDayEvent()Z

    move-result v0

    .line 1818520
    iput-boolean v0, v1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->b:Z

    .line 1818521
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getEndDate()Ljava/util/Date;

    move-result-object v0

    .line 1818522
    iput-object v0, v1, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->c:Ljava/util/Date;

    .line 1818523
    return-object v1
.end method

.method public setEndDate(Ljava/util/Date;)V
    .locals 3

    .prologue
    .line 1818524
    if-nez p1, :cond_0

    .line 1818525
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-static {v0, v1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->b(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;)V

    .line 1818526
    :goto_0
    return-void

    .line 1818527
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, p1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    goto :goto_0
.end method

.method public setTimeZone(Ljava/util/TimeZone;)V
    .locals 3

    .prologue
    .line 1818528
    iput-object p1, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->g:Ljava/util/TimeZone;

    .line 1818529
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getStartDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    .line 1818530
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getEndDate()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->getTimeZone()Ljava/util/TimeZone;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimePicker;->a(Lcom/facebook/events/ui/date/TimePickerView;Ljava/util/Date;Ljava/util/TimeZone;)V

    .line 1818531
    return-void
.end method
