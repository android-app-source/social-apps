.class public Lcom/facebook/events/ui/date/EventTimeModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/ui/date/EventTimeModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/util/TimeZone;

.field public b:Z

.field public c:Ljava/util/TimeZone;

.field public d:J

.field public e:J


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1817974
    new-instance v0, LX/BmA;

    invoke-direct {v0}, LX/BmA;-><init>()V

    sput-object v0, Lcom/facebook/events/ui/date/EventTimeModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 1817937
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1817938
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->a:Ljava/util/TimeZone;

    .line 1817939
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/events/ui/date/EventTimeModel;->a:Ljava/util/TimeZone;

    move-object v1, p0

    move-wide v6, v4

    invoke-virtual/range {v1 .. v7}, Lcom/facebook/events/ui/date/EventTimeModel;->a(ZLjava/util/TimeZone;JJ)Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1817940
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1817967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1817968
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->a:Ljava/util/TimeZone;

    .line 1817969
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->b:Z

    .line 1817970
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    .line 1817971
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    .line 1817972
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->e:J

    .line 1817973
    return-void
.end method

.method private a(J)V
    .locals 6

    .prologue
    .line 1817957
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    :goto_0
    iput-wide p1, p0, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    .line 1817958
    return-void

    .line 1817959
    :cond_0
    const/4 v5, 0x0

    .line 1817960
    iget-object v3, p0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    invoke-static {v3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v3

    .line 1817961
    const/16 v4, 0xc

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 1817962
    const/16 v4, 0xd

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 1817963
    const/16 v4, 0xe

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->set(II)V

    .line 1817964
    const/16 v4, 0xb

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 1817965
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    move-wide p1, v3

    .line 1817966
    goto :goto_0
.end method

.method private b(J)V
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 1817951
    iget-boolean v2, p0, Lcom/facebook/events/ui/date/EventTimeModel;->b:Z

    move v2, v2

    .line 1817952
    if-eqz v2, :cond_1

    .line 1817953
    iget-wide v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    const-wide/32 v2, 0x5265bff

    add-long p1, v0, v2

    .line 1817954
    :cond_0
    :goto_0
    iput-wide p1, p0, Lcom/facebook/events/ui/date/EventTimeModel;->e:J

    .line 1817955
    return-void

    .line 1817956
    :cond_1
    cmp-long v2, p1, v0

    if-gtz v2, :cond_0

    move-wide p1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(JJ)Lcom/facebook/events/ui/date/EventTimeModel;
    .locals 1

    .prologue
    .line 1817948
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/ui/date/EventTimeModel;->a(J)V

    .line 1817949
    invoke-direct {p0, p3, p4}, Lcom/facebook/events/ui/date/EventTimeModel;->b(J)V

    .line 1817950
    return-object p0
.end method

.method public final a(ZLjava/util/TimeZone;JJ)Lcom/facebook/events/ui/date/EventTimeModel;
    .locals 1

    .prologue
    .line 1817975
    iput-boolean p1, p0, Lcom/facebook/events/ui/date/EventTimeModel;->b:Z

    .line 1817976
    if-nez p2, :cond_0

    iget-object p2, p0, Lcom/facebook/events/ui/date/EventTimeModel;->a:Ljava/util/TimeZone;

    :cond_0
    iput-object p2, p0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    .line 1817977
    invoke-direct {p0, p3, p4}, Lcom/facebook/events/ui/date/EventTimeModel;->a(J)V

    .line 1817978
    invoke-direct {p0, p5, p6}, Lcom/facebook/events/ui/date/EventTimeModel;->b(J)V

    .line 1817979
    return-object p0
.end method

.method public final d()Ljava/sql/Date;
    .locals 4

    .prologue
    .line 1817947
    new-instance v0, Ljava/sql/Date;

    iget-wide v2, p0, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    invoke-direct {v0, v2, v3}, Ljava/sql/Date;-><init>(J)V

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1817946
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 6

    .prologue
    .line 1817943
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 1817944
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 1817945
    iget-wide v2, p0, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 4

    .prologue
    .line 1817942
    iget-wide v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Ljava/sql/Date;
    .locals 4

    .prologue
    .line 1817941
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventTimeModel;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/sql/Date;

    iget-wide v2, p0, Lcom/facebook/events/ui/date/EventTimeModel;->e:J

    invoke-direct {v0, v2, v3}, Ljava/sql/Date;-><init>(J)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1817932
    iget-boolean v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1817933
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    invoke-virtual {v0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1817934
    iget-wide v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1817935
    iget-wide v0, p0, Lcom/facebook/events/ui/date/EventTimeModel;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1817936
    return-void
.end method
