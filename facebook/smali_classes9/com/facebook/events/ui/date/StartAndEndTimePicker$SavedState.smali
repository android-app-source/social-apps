.class public final Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;
.super Landroid/view/View$BaseSavedState;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Ljava/util/Date;

.field public b:Z

.field public c:Ljava/util/Date;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1818429
    new-instance v0, LX/BmW;

    invoke-direct {v0}, LX/BmW;-><init>()V

    sput-object v0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1818430
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1818431
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 1818432
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->a:Ljava/util/Date;

    .line 1818433
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->b:Z

    .line 1818434
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 1818435
    new-instance v0, Ljava/util/Date;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->c:Ljava/util/Date;

    .line 1818436
    :cond_1
    return-void

    .line 1818437
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1818438
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1818439
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1818440
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1818441
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1818442
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->a:Ljava/util/Date;

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1818443
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->a:Ljava/util/Date;

    if-eqz v0, :cond_0

    .line 1818444
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->a:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1818445
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->b:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1818446
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->c:Ljava/util/Date;

    if-nez v0, :cond_4

    :goto_2
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 1818447
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->c:Ljava/util/Date;

    if-eqz v0, :cond_1

    .line 1818448
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimePicker$SavedState;->c:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1818449
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1818450
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1818451
    goto :goto_1

    :cond_4
    move v1, v2

    .line 1818452
    goto :goto_2
.end method
