.class public Lcom/facebook/events/ui/date/TimePickerView;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements LX/BmY;


# instance fields
.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/BmM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/BmO;

.field public e:Ljava/util/Calendar;

.field public f:Z

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1818589
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 1818590
    iput-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    .line 1818591
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    .line 1818592
    iput-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->g:Ljava/lang/String;

    .line 1818593
    invoke-direct {p0}, Lcom/facebook/events/ui/date/TimePickerView;->c()V

    .line 1818594
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1818631
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1818632
    iput-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    .line 1818633
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    .line 1818634
    iput-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->g:Ljava/lang/String;

    .line 1818635
    invoke-direct {p0}, Lcom/facebook/events/ui/date/TimePickerView;->c()V

    .line 1818636
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1818637
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1818638
    iput-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    .line 1818639
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    .line 1818640
    iput-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->g:Ljava/lang/String;

    .line 1818641
    invoke-direct {p0}, Lcom/facebook/events/ui/date/TimePickerView;->c()V

    .line 1818642
    return-void
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 1818643
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    .line 1818644
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 1818645
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 1818646
    invoke-direct {p0}, Lcom/facebook/events/ui/date/TimePickerView;->b()V

    .line 1818647
    return-void
.end method

.method private static a(Lcom/facebook/events/ui/date/TimePickerView;LX/0Or;LX/BmM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/ui/date/TimePickerView;",
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;",
            "LX/BmM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1818648
    iput-object p1, p0, Lcom/facebook/events/ui/date/TimePickerView;->b:LX/0Or;

    iput-object p2, p0, Lcom/facebook/events/ui/date/TimePickerView;->c:LX/BmM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/ui/date/TimePickerView;

    const/16 v1, 0x2e4

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const-class v2, LX/BmM;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/BmM;

    invoke-static {p0, v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->a(Lcom/facebook/events/ui/date/TimePickerView;LX/0Or;LX/BmM;)V

    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 1818649
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    if-nez v0, :cond_0

    .line 1818650
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setText(Ljava/lang/CharSequence;)V

    .line 1818651
    :goto_0
    return-void

    .line 1818652
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11S;

    .line 1818653
    sget-object v1, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    iget-object v2, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v1

    .line 1818654
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1818655
    iget-object v2, p0, Lcom/facebook/events/ui/date/TimePickerView;->g:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1818656
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/TimePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f082180

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    iget-object v4, p0, Lcom/facebook/events/ui/date/TimePickerView;->g:Ljava/lang/String;

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1818657
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    .line 1818658
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->g:Ljava/lang/String;

    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    add-int v3, v2, v0

    .line 1818659
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 1818660
    new-instance v1, Landroid/text/style/AbsoluteSizeSpan;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/TimePickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b004e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-direct {v1, v4}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    const/16 v4, 0x11

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 1818661
    :cond_1
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 1818625
    const-class v0, Lcom/facebook/events/ui/date/TimePickerView;

    invoke-static {v0, p0}, Lcom/facebook/events/ui/date/TimePickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1818626
    invoke-virtual {p0, p0}, Lcom/facebook/events/ui/date/TimePickerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1818627
    return-void
.end method

.method public static d(Lcom/facebook/events/ui/date/TimePickerView;)V
    .locals 2

    .prologue
    .line 1818628
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    if-eqz v0, :cond_0

    .line 1818629
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    iget-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    invoke-interface {v0, v1}, LX/BmO;->a(Ljava/util/Calendar;)V

    .line 1818630
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1818622
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    .line 1818623
    invoke-direct {p0}, Lcom/facebook/events/ui/date/TimePickerView;->b()V

    .line 1818624
    return-void
.end method

.method public final a(Landroid/text/format/Time;)V
    .locals 2

    .prologue
    .line 1818618
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 1818619
    iget v0, p1, Landroid/text/format/Time;->hour:I

    iget v1, p1, Landroid/text/format/Time;->minute:I

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->a(II)V

    .line 1818620
    :cond_0
    invoke-static {p0}, Lcom/facebook/events/ui/date/TimePickerView;->d(Lcom/facebook/events/ui/date/TimePickerView;)V

    .line 1818621
    return-void
.end method

.method public getPickedTime()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 1818617
    iget-object v0, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/16 v3, 0xb

    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x5e0e5970

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1818604
    iget-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    if-nez v1, :cond_0

    .line 1818605
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    .line 1818606
    iget-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    iget-object v2, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    rem-int/lit8 v2, v2, 0x18

    invoke-virtual {v1, v3, v2}, Ljava/util/Calendar;->set(II)V

    .line 1818607
    iget-object v1, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    const/16 v2, 0xc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljava/util/Calendar;->set(II)V

    .line 1818608
    :cond_0
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 1818609
    iget-object v2, p0, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 1818610
    iget-object v2, p0, Lcom/facebook/events/ui/date/TimePickerView;->c:LX/BmM;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/TimePickerView;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/1lB;->HOUR_MINUTE_STYLE:LX/1lB;

    .line 1818611
    new-instance v6, LX/BmL;

    invoke-static {v2}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v11

    check-cast v11, LX/11S;

    move-object v7, v3

    move-object v8, v1

    move-object v9, p0

    move-object v10, v4

    invoke-direct/range {v6 .. v11}, LX/BmL;-><init>(Landroid/content/Context;Landroid/text/format/Time;LX/BmY;LX/1lB;LX/11S;)V

    .line 1818612
    move-object v1, v6

    .line 1818613
    iget-boolean v2, p0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    if-eqz v2, :cond_1

    .line 1818614
    const/4 v2, -0x2

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/TimePickerView;->getContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f082178

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/BmX;

    invoke-direct {v4, p0}, LX/BmX;-><init>(Lcom/facebook/events/ui/date/TimePickerView;)V

    invoke-virtual {v1, v2, v3, v4}, LX/2EJ;->a(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1818615
    :cond_1
    invoke-virtual {v1}, LX/BmL;->show()V

    .line 1818616
    const v1, 0x4d21f99b    # 1.6984312E8f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAppendedText(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1818601
    iput-object p1, p0, Lcom/facebook/events/ui/date/TimePickerView;->g:Ljava/lang/String;

    .line 1818602
    invoke-direct {p0}, Lcom/facebook/events/ui/date/TimePickerView;->b()V

    .line 1818603
    return-void
.end method

.method public setIsClearable(Z)V
    .locals 0

    .prologue
    .line 1818599
    iput-boolean p1, p0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    .line 1818600
    return-void
.end method

.method public setOnCalendarTimePickedListener(LX/BmO;)V
    .locals 0

    .prologue
    .line 1818597
    iput-object p1, p0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 1818598
    return-void
.end method

.method public setTime(Ljava/util/Calendar;)V
    .locals 2

    .prologue
    .line 1818595
    const/16 v0, 0xb

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/16 v1, 0xc

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->a(II)V

    .line 1818596
    return-void
.end method
