.class public Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public A:Z

.field public B:Ljava/lang/String;

.field public C:Ljava/lang/String;

.field public D:Ljava/lang/Boolean;

.field public E:Ljava/util/Calendar;

.field private final F:Landroid/view/View$OnClickListener;

.field private final G:Landroid/view/View$OnClickListener;

.field public p:LX/6RZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/Bma;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:Lcom/facebook/uicontrib/calendar/CalendarView;

.field public s:Lcom/facebook/resources/ui/FbTextView;

.field public t:Lcom/facebook/resources/ui/FbTextView;

.field public u:Lcom/facebook/widget/CustomLinearLayout;

.field public v:Lcom/facebook/widget/CustomLinearLayout;

.field public w:Lcom/facebook/fbui/glyph/GlyphView;

.field public x:Ljava/util/Calendar;

.field public y:Ljava/util/Calendar;

.field public z:Landroid/widget/TimePicker;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1818152
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1818153
    new-instance v0, LX/BmB;

    invoke-direct {v0, p0}, LX/BmB;-><init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->F:Landroid/view/View$OnClickListener;

    .line 1818154
    new-instance v0, LX/BmC;

    invoke-direct {v0, p0}, LX/BmC;-><init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->G:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Calendar;Z)Landroid/content/Intent;
    .locals 2
    .param p2    # Ljava/util/Calendar;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1818145
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1818146
    invoke-static {p1}, Ljunit/framework/Assert;->assertNotNull(Ljava/lang/Object;)V

    .line 1818147
    const-string v1, "extra_start_time"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1818148
    if-eqz p2, :cond_0

    .line 1818149
    const-string v1, "extra_end_time"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 1818150
    :cond_0
    const-string v1, "extra_is_selecting_second_date"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1818151
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1818142
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    new-instance v1, LX/BmF;

    invoke-direct {v1, p0}, LX/BmF;-><init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V

    .line 1818143
    iput-object v1, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->N:LX/BmE;

    .line 1818144
    return-void
.end method

.method private static a(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;LX/6RZ;LX/Bma;)V
    .locals 0

    .prologue
    .line 1818141
    iput-object p1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->p:LX/6RZ;

    iput-object p2, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->q:LX/Bma;

    return-void
.end method

.method private a(Lcom/facebook/resources/ui/FbTextView;Z)V
    .locals 1

    .prologue
    .line 1818138
    if-eqz p1, :cond_0

    .line 1818139
    invoke-direct {p0, p2}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->b(Z)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1818140
    :cond_0
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;

    invoke-static {v1}, LX/6RZ;->a(LX/0QB;)LX/6RZ;

    move-result-object v0

    check-cast v0, LX/6RZ;

    new-instance p1, LX/Bma;

    invoke-direct {p1}, LX/Bma;-><init>()V

    move-object p1, p1

    move-object v1, p1

    check-cast v1, LX/Bma;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;LX/6RZ;LX/Bma;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;II)V
    .locals 2

    .prologue
    .line 1818133
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1818134
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 1818135
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    invoke-virtual {v0}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p2, :cond_1

    .line 1818136
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    .line 1818137
    :cond_1
    return-void
.end method

.method public static a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;Lcom/facebook/resources/ui/FbTextView;Ljava/util/Calendar;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1818125
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->D:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->p:LX/6RZ;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v1, v2}, LX/6RZ;->a(ZLjava/util/Date;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 1818126
    :goto_0
    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->t:Lcom/facebook/resources/ui/FbTextView;

    if-ne p1, v1, :cond_1

    .line 1818127
    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->B:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1818128
    :goto_1
    return-void

    .line 1818129
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->p:LX/6RZ;

    invoke-virtual {p2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    .line 1818130
    iget-object v4, v0, LX/6RZ;->e:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/11S;

    sget-object v5, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    invoke-interface {v4, v5, v6, v7}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v4

    move-object v0, v4

    .line 1818131
    goto :goto_0

    .line 1818132
    :cond_1
    invoke-virtual {p1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;Ljava/util/Calendar;II)V
    .locals 4

    .prologue
    .line 1818120
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1818121
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 1818122
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, p3}, Ljava/util/Calendar;->set(II)V

    .line 1818123
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->setDate(J)V

    .line 1818124
    return-void
.end method

.method private b(Z)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1818051
    new-instance v0, LX/BmD;

    invoke-direct {v0, p0, p1}, LX/BmD;-><init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;Z)V

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1818118
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->u:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v1, LX/BmG;

    invoke-direct {v1, p0}, LX/BmG;-><init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1818119
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1818116
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->w:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/BmH;

    invoke-direct {v1, p0}, LX/BmH;-><init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1818117
    return-void
.end method

.method private m()V
    .locals 5

    .prologue
    .line 1818101
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_start_time"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1818102
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->performClick()Z

    .line 1818103
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_start_time"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    .line 1818104
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    .line 1818105
    iget-object v2, v1, Lcom/facebook/uicontrib/calendar/CalendarView;->ab:Ljava/util/Calendar;

    move-object v1, v2

    .line 1818106
    invoke-virtual {v0, v1}, Ljava/util/Calendar;->before(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1818107
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(III)V

    .line 1818108
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->x:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->setDate(J)V

    .line 1818109
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_end_time"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1818110
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_end_time"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    .line 1818111
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    if-eqz v0, :cond_2

    .line 1818112
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->u:Lcom/facebook/widget/CustomLinearLayout;

    invoke-virtual {v0}, Lcom/facebook/widget/CustomLinearLayout;->performClick()Z

    .line 1818113
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_is_selecting_second_date"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->y:Ljava/util/Calendar;

    if-nez v0, :cond_4

    .line 1818114
    :cond_3
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->performClick()Z

    .line 1818115
    :cond_4
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1818099
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    new-instance v1, LX/BmI;

    invoke-direct {v1, p0}, LX/BmI;-><init>(Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setOnTimeChangedListener(Landroid/widget/TimePicker$OnTimeChangedListener;)V

    .line 1818100
    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 11
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1818052
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1818053
    invoke-static {p0, p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1818054
    const v0, 0x7f03054d

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->setContentView(I)V

    .line 1818055
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_calendar_picker_title"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->C:Ljava/lang/String;

    .line 1818056
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_enable_time_picker"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->D:Ljava/lang/Boolean;

    .line 1818057
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->q:LX/Bma;

    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->C:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f08217e

    invoke-virtual {p0, v1}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    const v1, 0x7f082173

    invoke-virtual {p0, v1}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v1, 0x7f0d0ee3

    invoke-virtual {p0, v1}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->findViewById(I)Landroid/view/View;

    iget-object v4, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->F:Landroid/view/View$OnClickListener;

    iget-object v5, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->G:Landroid/view/View$OnClickListener;

    move-object v1, p0

    const/4 p1, 0x1

    .line 1818058
    invoke-static {v1}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1818059
    const v8, 0x7f0d00bc

    invoke-virtual {v1, v8}, Lcom/facebook/base/activity/FbFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, LX/0h5;

    .line 1818060
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v9

    .line 1818061
    iput p1, v9, LX/108;->a:I

    .line 1818062
    move-object v9, v9

    .line 1818063
    iput-object v3, v9, LX/108;->g:Ljava/lang/String;

    .line 1818064
    move-object v9, v9

    .line 1818065
    const/4 v10, -0x2

    .line 1818066
    iput v10, v9, LX/108;->h:I

    .line 1818067
    move-object v9, v9

    .line 1818068
    invoke-virtual {v9}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v9

    .line 1818069
    invoke-interface {v8, p1}, LX/0h5;->setShowDividers(Z)V

    .line 1818070
    invoke-interface {v8, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1818071
    invoke-static {v9}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v9

    invoke-interface {v8, v9}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1818072
    const/4 v9, 0x0

    invoke-interface {v8, v9}, LX/0h5;->setHasBackButton(Z)V

    .line 1818073
    invoke-interface {v8, v4}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1818074
    new-instance v9, LX/BmZ;

    invoke-direct {v9, v0, v5}, LX/BmZ;-><init>(LX/Bma;Landroid/view/View$OnClickListener;)V

    invoke-interface {v8, v9}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1818075
    invoke-virtual {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082181

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->B:Ljava/lang/String;

    .line 1818076
    const v0, 0x7f0d0eeb

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TimePicker;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    .line 1818077
    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->z:Landroid/widget/TimePicker;

    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->D:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v6

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TimePicker;->setVisibility(I)V

    .line 1818078
    const v0, 0x7f0d0eea

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/calendar/CalendarView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    .line 1818079
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    .line 1818080
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    const/4 v1, 0x6

    const/4 v2, -0x7

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 1818081
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    invoke-virtual {v1, v7}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget-object v3, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->E:Ljava/util/Calendar;

    const/4 v4, 0x5

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/uicontrib/calendar/CalendarView;->a(III)V

    .line 1818082
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->r:Lcom/facebook/uicontrib/calendar/CalendarView;

    const-wide v2, 0x269fb2000L

    .line 1818083
    iput-wide v2, v0, Lcom/facebook/uicontrib/calendar/CalendarView;->aa:J

    .line 1818084
    invoke-direct {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a()V

    .line 1818085
    const v0, 0x7f0d0ee5

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 1818086
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->s:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v0, v6}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a(Lcom/facebook/resources/ui/FbTextView;Z)V

    .line 1818087
    const v0, 0x7f0d0ee8

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 1818088
    iget-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-direct {p0, v0, v7}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a(Lcom/facebook/resources/ui/FbTextView;Z)V

    .line 1818089
    const v0, 0x7f0d0ee7

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->v:Lcom/facebook/widget/CustomLinearLayout;

    .line 1818090
    const v0, 0x7f0d0ee6

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->u:Lcom/facebook/widget/CustomLinearLayout;

    .line 1818091
    invoke-direct {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->b()V

    .line 1818092
    const v0, 0x7f0d0ee9

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->w:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1818093
    invoke-direct {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->l()V

    .line 1818094
    invoke-direct {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->m()V

    .line 1818095
    invoke-direct {p0}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->n()V

    .line 1818096
    return-void

    .line 1818097
    :cond_0
    iget-object v2, p0, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->C:Ljava/lang/String;

    goto/16 :goto_0

    .line 1818098
    :cond_1
    const/16 v0, 0x8

    goto/16 :goto_1
.end method
