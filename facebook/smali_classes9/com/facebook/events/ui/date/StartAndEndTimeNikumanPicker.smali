.class public Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""


# instance fields
.field public a:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/events/ui/date/DatePickerView;

.field public c:Lcom/facebook/events/ui/date/TimePickerView;

.field public d:Landroid/view/View;

.field public e:Lcom/facebook/events/ui/date/DatePickerView;

.field public f:Lcom/facebook/events/ui/date/TimePickerView;

.field private g:Lcom/facebook/events/ui/date/EventTimeModel;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1818365
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 1818366
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->h:Z

    .line 1818367
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b()V

    .line 1818368
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1818369
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1818370
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->h:Z

    .line 1818371
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b()V

    .line 1818372
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1818361
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1818362
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->h:Z

    .line 1818363
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b()V

    .line 1818364
    return-void
.end method

.method private a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;)J
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v4, 0x2

    const/4 v2, 0x1

    .line 1818347
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818348
    iget-object v1, v0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v0, v1

    .line 1818349
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 1818350
    iget-object v1, p1, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    move-object v3, v1

    .line 1818351
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 1818352
    if-nez v3, :cond_0

    .line 1818353
    const-wide/16 v0, 0x0

    .line 1818354
    :goto_0
    return-wide v0

    .line 1818355
    :cond_0
    iget-object v1, p2, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v1, v1

    .line 1818356
    if-nez v1, :cond_1

    .line 1818357
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 1818358
    :goto_1
    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    goto :goto_0

    .line 1818359
    :cond_1
    iget-object v1, p2, Lcom/facebook/events/ui/date/TimePickerView;->e:Ljava/util/Calendar;

    move-object v5, v1

    .line 1818360
    invoke-virtual {v3, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-virtual {v3, v6}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/16 v4, 0xb

    invoke-virtual {v5, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/16 v6, 0xc

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Ljava/util/Calendar;->set(IIIII)V

    goto :goto_1
.end method

.method private a(Lcom/facebook/events/ui/date/TimePickerView;J)V
    .locals 4

    .prologue
    .line 1818342
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818343
    iget-object v1, v0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v0, v1

    .line 1818344
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-eqz v1, :cond_0

    .line 1818345
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p2, p3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/TimeZone;->inDaylightTime(Ljava/util/Date;)Z

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/TimeZone;->getDisplayName(ZI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setAppendedText(Ljava/lang/String;)V

    .line 1818346
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v0

    check-cast v0, LX/0kL;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a:LX/0kL;

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1818373
    const-class v0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;

    invoke-static {v0, p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1818374
    const v0, 0x7f0313b8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1818375
    const v0, 0x7f0d2d7f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/DatePickerView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818376
    const v0, 0x7f0d2d80

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/TimePickerView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818377
    const v0, 0x7f0d2d81

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->d:Landroid/view/View;

    .line 1818378
    const v0, 0x7f0d2d82

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/DatePickerView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818379
    const v0, 0x7f0d2d83

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/ui/date/TimePickerView;

    iput-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818380
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->d()V

    .line 1818381
    return-void
.end method

.method public static c(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V
    .locals 10

    .prologue
    const-wide/32 v6, 0x48190800

    const/4 v5, 0x1

    .line 1818313
    iput-boolean v5, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->h:Z

    .line 1818314
    invoke-static {p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->getEndTimestamp(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)J

    move-result-wide v0

    .line 1818315
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 1818316
    :goto_0
    return-void

    .line 1818317
    :cond_0
    invoke-static {p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->getStartTimestamp(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)J

    move-result-wide v2

    .line 1818318
    cmp-long v4, v2, v0

    if-ltz v4, :cond_2

    .line 1818319
    new-instance v0, Ljava/util/Date;

    const-wide/32 v4, 0xa4cb80

    add-long/2addr v2, v4

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1818320
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818321
    iget-object v2, v1, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v1, v2

    .line 1818322
    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 1818323
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1818324
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    .line 1818325
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818326
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082176

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 1818327
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818328
    iget-wide v8, v1, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    move-wide v2, v8

    .line 1818329
    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/TimePickerView;J)V

    .line 1818330
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818331
    iget-wide v8, v1, Lcom/facebook/events/ui/date/EventTimeModel;->e:J

    move-wide v2, v8

    .line 1818332
    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/TimePickerView;J)V

    goto :goto_0

    .line 1818333
    :cond_2
    sub-long/2addr v0, v2

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    .line 1818334
    new-instance v0, Ljava/util/Date;

    add-long/2addr v2, v6

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 1818335
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818336
    iget-object v2, v1, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v1, v2

    .line 1818337
    invoke-static {v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v1

    .line 1818338
    invoke-virtual {v1, v0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 1818339
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    .line 1818340
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0, v1}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818341
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a:LX/0kL;

    new-instance v1, LX/27k;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f082177

    new-array v4, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-wide/16 v6, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    goto :goto_1
.end method

.method private d()V
    .locals 4
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1818296
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818297
    iput-boolean v2, v0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    .line 1818298
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    new-instance v1, LX/BmN;

    invoke-direct {v1, p0}, LX/BmN;-><init>(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V

    .line 1818299
    iput-object v1, v0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    .line 1818300
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818301
    iput-boolean v2, v0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    .line 1818302
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    new-instance v1, LX/BmP;

    invoke-direct {v1, p0}, LX/BmP;-><init>(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V

    .line 1818303
    iput-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 1818304
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    .line 1818305
    iput-boolean v3, v0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    .line 1818306
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    new-instance v1, LX/BmQ;

    invoke-direct {v1, p0}, LX/BmQ;-><init>(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V

    .line 1818307
    iput-object v1, v0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    .line 1818308
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    .line 1818309
    iput-boolean v3, v0, Lcom/facebook/events/ui/date/TimePickerView;->f:Z

    .line 1818310
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    new-instance v1, LX/BmR;

    invoke-direct {v1, p0}, LX/BmR;-><init>(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V

    .line 1818311
    iput-object v1, v0, Lcom/facebook/events/ui/date/TimePickerView;->d:LX/BmO;

    .line 1818312
    return-void
.end method

.method public static e(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)V
    .locals 6

    .prologue
    .line 1818294
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v2, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-direct {p0, v1, v2}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;)J

    move-result-wide v2

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v4, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-direct {p0, v1, v4}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;)J

    move-result-wide v4

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/events/ui/date/EventTimeModel;->a(JJ)Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818295
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 1818262
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818263
    iget-object v1, v0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v0, v1

    .line 1818264
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 1818265
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818266
    iget-wide v4, v1, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    move-wide v2, v4

    .line 1818267
    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1818268
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    .line 1818269
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818270
    iget-boolean v2, v1, Lcom/facebook/events/ui/date/EventTimeModel;->b:Z

    move v1, v2

    .line 1818271
    if-eqz v1, :cond_0

    .line 1818272
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0}, Lcom/facebook/events/ui/date/TimePickerView;->a()V

    .line 1818273
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1818274
    :goto_0
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818275
    iget-wide v4, v1, Lcom/facebook/events/ui/date/EventTimeModel;->d:J

    move-wide v2, v4

    .line 1818276
    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/TimePickerView;J)V

    .line 1818277
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    invoke-virtual {v0}, Lcom/facebook/events/ui/date/EventTimeModel;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1818278
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818279
    iget-object v1, v0, Lcom/facebook/events/ui/date/EventTimeModel;->c:Ljava/util/TimeZone;

    move-object v0, v1

    .line 1818280
    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    .line 1818281
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818282
    iget-wide v4, v1, Lcom/facebook/events/ui/date/EventTimeModel;->e:J

    move-wide v2, v4

    .line 1818283
    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1818284
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/DatePickerView;->setDate(Ljava/util/Calendar;)V

    .line 1818285
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818286
    :goto_1
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818287
    iget-wide v4, v1, Lcom/facebook/events/ui/date/EventTimeModel;->e:J

    move-wide v2, v4

    .line 1818288
    invoke-direct {p0, v0, v2, v3}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/TimePickerView;J)V

    .line 1818289
    return-void

    .line 1818290
    :cond_0
    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v1, v0}, Lcom/facebook/events/ui/date/TimePickerView;->setTime(Ljava/util/Calendar;)V

    .line 1818291
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1818292
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    invoke-virtual {v0}, Lcom/facebook/events/ui/date/DatePickerView;->a()V

    .line 1818293
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-virtual {v0}, Lcom/facebook/events/ui/date/TimePickerView;->a()V

    goto :goto_1
.end method

.method public static getEndTimestamp(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)J
    .locals 2

    .prologue
    .line 1818261
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->e:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static getStartTimestamp(Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;)J
    .locals 2

    .prologue
    .line 1818260
    iget-object v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->b:Lcom/facebook/events/ui/date/DatePickerView;

    iget-object v1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->c:Lcom/facebook/events/ui/date/TimePickerView;

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->a(Lcom/facebook/events/ui/date/DatePickerView;Lcom/facebook/events/ui/date/TimePickerView;)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(Lcom/facebook/events/ui/date/EventTimeModel;)V
    .locals 0

    .prologue
    .line 1818256
    iput-object p1, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->g:Lcom/facebook/events/ui/date/EventTimeModel;

    .line 1818257
    invoke-direct {p0}, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->f()V

    .line 1818258
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1818259
    iget-boolean v0, p0, Lcom/facebook/events/ui/date/StartAndEndTimeNikumanPicker;->h:Z

    return v0
.end method
