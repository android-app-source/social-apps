.class public Lcom/facebook/events/ui/date/DatePickerView;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Landroid/content/DialogInterface$OnDismissListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/11S;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/Bm9;

.field public d:Ljava/util/Calendar;

.field public e:Z

.field public f:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1817907
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 1817908
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    .line 1817909
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    .line 1817910
    invoke-direct {p0}, Lcom/facebook/events/ui/date/DatePickerView;->b()V

    .line 1817911
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1817921
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1817922
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    .line 1817923
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    .line 1817924
    invoke-direct {p0}, Lcom/facebook/events/ui/date/DatePickerView;->b()V

    .line 1817925
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1817916
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1817917
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    .line 1817918
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    .line 1817919
    invoke-direct {p0}, Lcom/facebook/events/ui/date/DatePickerView;->b()V

    .line 1817920
    return-void
.end method

.method private a(III)V
    .locals 4

    .prologue
    .line 1817912
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    .line 1817913
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/Calendar;->set(III)V

    .line 1817914
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11S;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/DatePickerView;->getTimeFormatStyle()LX/1lB;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/date/DatePickerView;->setText(Ljava/lang/CharSequence;)V

    .line 1817915
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/events/ui/date/DatePickerView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/events/ui/date/DatePickerView;

    const/16 v1, 0x2e4

    invoke-static {v0, v1}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->b:LX/0Or;

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1817904
    const-class v0, Lcom/facebook/events/ui/date/DatePickerView;

    invoke-static {v0, p0}, Lcom/facebook/events/ui/date/DatePickerView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1817905
    invoke-virtual {p0, p0}, Lcom/facebook/events/ui/date/DatePickerView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1817906
    return-void
.end method

.method public static c(Lcom/facebook/events/ui/date/DatePickerView;)V
    .locals 2

    .prologue
    .line 1817901
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    if-eqz v0, :cond_0

    .line 1817902
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    iget-object v1, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    invoke-interface {v0, v1}, LX/Bm9;->a(Ljava/util/Calendar;)V

    .line 1817903
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1817926
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    .line 1817927
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/date/DatePickerView;->setText(Ljava/lang/CharSequence;)V

    .line 1817928
    return-void
.end method

.method public getPickedDate()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 1817900
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    return-object v0
.end method

.method public getTimeFormatStyle()LX/1lB;
    .locals 1

    .prologue
    .line 1817899
    sget-object v0, LX/1lB;->EVENTS_RELATIVE_DATE_STYLE:LX/1lB;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x2

    const v0, 0x21fd23d

    invoke-static {v7, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 1817889
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    if-nez v0, :cond_0

    .line 1817890
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    .line 1817891
    :cond_0
    new-instance v0, LX/D97;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/DatePickerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e08bd

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    iget-object v2, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v3

    iget-object v2, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v2, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    const/4 v5, 0x5

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, LX/D97;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 1817892
    invoke-virtual {v0, p0}, Landroid/app/DatePickerDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 1817893
    iget-boolean v1, p0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    if-eqz v1, :cond_1

    .line 1817894
    const/4 v1, -0x2

    invoke-virtual {p0}, Lcom/facebook/events/ui/date/DatePickerView;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f082178

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/Bm8;

    invoke-direct {v3, p0}, LX/Bm8;-><init>(Lcom/facebook/events/ui/date/DatePickerView;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/DatePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1817895
    :cond_1
    iget-wide v2, p0, Lcom/facebook/events/ui/date/DatePickerView;->f:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 1817896
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/events/ui/date/DatePickerView;->f:J

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 1817897
    :cond_2
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    .line 1817898
    const v0, 0x49887d10    # 1118114.0f

    invoke-static {v7, v7, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDateSet(Landroid/widget/DatePicker;III)V
    .locals 1

    .prologue
    .line 1817885
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 1817886
    invoke-direct {p0, p2, p3, p4}, Lcom/facebook/events/ui/date/DatePickerView;->a(III)V

    .line 1817887
    :cond_0
    invoke-static {p0}, Lcom/facebook/events/ui/date/DatePickerView;->c(Lcom/facebook/events/ui/date/DatePickerView;)V

    .line 1817888
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 1817873
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 1817874
    iget-object v0, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iget-object v1, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/events/ui/date/DatePickerView;->d:Ljava/util/Calendar;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/events/ui/date/DatePickerView;->a(III)V

    .line 1817875
    :cond_0
    invoke-static {p0}, Lcom/facebook/events/ui/date/DatePickerView;->c(Lcom/facebook/events/ui/date/DatePickerView;)V

    .line 1817876
    return-void
.end method

.method public setDate(Ljava/util/Calendar;)V
    .locals 3

    .prologue
    .line 1817883
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    const/4 v2, 0x5

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/events/ui/date/DatePickerView;->a(III)V

    .line 1817884
    return-void
.end method

.method public setIsClearable(Z)V
    .locals 0

    .prologue
    .line 1817881
    iput-boolean p1, p0, Lcom/facebook/events/ui/date/DatePickerView;->e:Z

    .line 1817882
    return-void
.end method

.method public setMinDate(J)V
    .locals 1

    .prologue
    .line 1817879
    iput-wide p1, p0, Lcom/facebook/events/ui/date/DatePickerView;->f:J

    .line 1817880
    return-void
.end method

.method public setOnCalendarDatePickedListener(LX/Bm9;)V
    .locals 0

    .prologue
    .line 1817877
    iput-object p1, p0, Lcom/facebook/events/ui/date/DatePickerView;->c:LX/Bm9;

    .line 1817878
    return-void
.end method
