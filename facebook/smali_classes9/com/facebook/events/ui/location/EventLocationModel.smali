.class public Lcom/facebook/events/ui/location/EventLocationModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/ui/location/EventLocationModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public b:J

.field public c:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1818951
    new-instance v0, LX/Bml;

    invoke-direct {v0}, LX/Bml;-><init>()V

    sput-object v0, Lcom/facebook/events/ui/location/EventLocationModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1818934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1818935
    iput-object v2, p0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1818936
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    .line 1818937
    iput-object v2, p0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    .line 1818938
    return-void
.end method

.method public constructor <init>(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1818946
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1818947
    iput-object p1, p0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1818948
    iput-wide p2, p0, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    .line 1818949
    iput-object p4, p0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    .line 1818950
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;
    .locals 1

    .prologue
    .line 1818945
    iget-object v0, p0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1818944
    iget-object v0, p0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1818943
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1818939
    iget-object v0, p0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1818940
    iget-wide v0, p0, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1818941
    iget-object v0, p0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1818942
    return-void
.end method
