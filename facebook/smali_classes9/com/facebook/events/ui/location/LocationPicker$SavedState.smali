.class public final Lcom/facebook/events/ui/location/LocationPicker$SavedState;
.super Landroid/view/View$BaseSavedState;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/ui/location/LocationPicker$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

.field public b:J

.field public c:Ljava/lang/String;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1819043
    new-instance v0, LX/Bmn;

    invoke-direct {v0}, LX/Bmn;-><init>()V

    sput-object v0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1819028
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1819029
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1819030
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->b:J

    .line 1819031
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->c:Ljava/lang/String;

    .line 1819032
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->d:Z

    .line 1819033
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1819042
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/location/LocationPicker$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1819040
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1819041
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1819034
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1819035
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1819036
    iget-wide v0, p0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1819037
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1819038
    iget-boolean v0, p0, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819039
    return-void
.end method
