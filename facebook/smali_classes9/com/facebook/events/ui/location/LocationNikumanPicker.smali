.class public Lcom/facebook/events/ui/location/LocationNikumanPicker;
.super Lcom/facebook/fbui/widget/text/BadgeTextView;
.source ""


# instance fields
.field public a:Lcom/facebook/events/ui/location/EventLocationModel;

.field public b:Landroid/app/Activity;

.field public c:Z

.field public d:Z

.field public e:I

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Bmo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1819003
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;)V

    .line 1819004
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->b()V

    .line 1819005
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1818963
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1818964
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->b()V

    .line 1818965
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1819000
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1819001
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->b()V

    .line 1819002
    return-void
.end method

.method private b()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1818992
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setFocusable(Z)V

    .line 1818993
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setCursorVisible(Z)V

    .line 1818994
    const v0, 0x7f082af0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setHint(I)V

    .line 1818995
    invoke-virtual {p0, v1}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setSingleLine(Z)V

    .line 1818996
    invoke-virtual {p0, v1}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setLines(I)V

    .line 1818997
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1818998
    new-instance v0, LX/Bmm;

    invoke-direct {v0, p0}, LX/Bmm;-><init>(Lcom/facebook/events/ui/location/LocationNikumanPicker;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1818999
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 1819006
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819007
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1819008
    if-eqz v0, :cond_0

    .line 1819009
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819010
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1819011
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setText(Ljava/lang/CharSequence;)V

    .line 1819012
    invoke-virtual {p0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0a66

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    .line 1819013
    :goto_0
    return-void

    .line 1819014
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819015
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1819016
    if-eqz v0, :cond_1

    .line 1819017
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819018
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1819019
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setText(Ljava/lang/CharSequence;)V

    .line 1819020
    invoke-virtual {p0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0a66

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 1819021
    :cond_1
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->setText(Ljava/lang/CharSequence;)V

    .line 1819022
    invoke-virtual {p0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0a65

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 6

    .prologue
    .line 1818975
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    const-wide/16 v3, -0x1

    const/4 v5, 0x0

    .line 1818976
    const-string v1, "extra_xed_location"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1818977
    if-nez v1, :cond_2

    .line 1818978
    const-string v1, "extra_place"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1818979
    const-string v1, "extra_place"

    invoke-static {p1, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iput-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1818980
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    if-nez v1, :cond_0

    move-wide v1, v3

    :goto_0
    iput-wide v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    .line 1818981
    iput-object v5, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    .line 1818982
    :goto_1
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->c()V

    .line 1818983
    return-void

    .line 1818984
    :cond_0
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    invoke-virtual {v1}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->bv_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    goto :goto_0

    .line 1818985
    :cond_1
    const-string v1, "extra_location_text"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1818986
    iput-object v5, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1818987
    iput-wide v3, v0, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    .line 1818988
    const-string v1, "extra_location_text"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    goto :goto_1

    .line 1818989
    :cond_2
    iput-object v5, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1818990
    iput-wide v3, v0, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    .line 1818991
    iput-object v5, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public final a(Lcom/facebook/events/ui/location/EventLocationModel;LX/0Or;Landroid/app/Activity;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/ui/location/EventLocationModel;",
            "LX/0Or",
            "<",
            "LX/Bmo;",
            ">;",
            "Landroid/app/Activity;",
            "IZ)V"
        }
    .end annotation

    .prologue
    .line 1818968
    iput-object p1, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1818969
    iput-object p2, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->f:LX/0Or;

    .line 1818970
    iput-object p3, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->b:Landroid/app/Activity;

    .line 1818971
    iput p4, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->e:I

    .line 1818972
    iput-boolean p5, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->d:Z

    .line 1818973
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationNikumanPicker;->c()V

    .line 1818974
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1818967
    iget-boolean v0, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->c:Z

    return v0
.end method

.method public getPickedLocation()Lcom/facebook/events/ui/location/EventLocationModel;
    .locals 1

    .prologue
    .line 1818966
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationNikumanPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    return-object v0
.end method
