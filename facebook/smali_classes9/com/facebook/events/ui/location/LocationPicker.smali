.class public Lcom/facebook/events/ui/location/LocationPicker;
.super Lcom/facebook/fbui/widget/text/BadgeTextView;
.source ""


# instance fields
.field private a:Lcom/facebook/events/ui/location/EventLocationModel;

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1819105
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;)V

    .line 1819106
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationPicker;->a()V

    .line 1819107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1819102
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1819103
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationPicker;->a()V

    .line 1819104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1819099
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/text/BadgeTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1819100
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationPicker;->a()V

    .line 1819101
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1819090
    new-instance v0, Lcom/facebook/events/ui/location/EventLocationModel;

    invoke-direct {v0}, Lcom/facebook/events/ui/location/EventLocationModel;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819091
    invoke-virtual {p0, v1}, Lcom/facebook/events/ui/location/LocationPicker;->setFocusable(Z)V

    .line 1819092
    invoke-virtual {p0, v1}, Lcom/facebook/events/ui/location/LocationPicker;->setCursorVisible(Z)V

    .line 1819093
    const v0, 0x7f082af0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationPicker;->setHint(I)V

    .line 1819094
    invoke-virtual {p0, v2}, Lcom/facebook/events/ui/location/LocationPicker;->setSingleLine(Z)V

    .line 1819095
    invoke-virtual {p0, v2}, Lcom/facebook/events/ui/location/LocationPicker;->setLines(I)V

    .line 1819096
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationPicker;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 1819097
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationPicker;->b()V

    .line 1819098
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 1819044
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819045
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1819046
    if-eqz v0, :cond_0

    .line 1819047
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819048
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v1

    .line 1819049
    invoke-virtual {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationPicker;->setText(Ljava/lang/CharSequence;)V

    .line 1819050
    invoke-virtual {p0}, Lcom/facebook/events/ui/location/LocationPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0a66

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    .line 1819051
    :goto_0
    return-void

    .line 1819052
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819053
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1819054
    if-eqz v0, :cond_1

    .line 1819055
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819056
    iget-object v1, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v0, v1

    .line 1819057
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationPicker;->setText(Ljava/lang/CharSequence;)V

    .line 1819058
    invoke-virtual {p0}, Lcom/facebook/events/ui/location/LocationPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0a66

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 1819059
    :cond_1
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/location/LocationPicker;->setText(Ljava/lang/CharSequence;)V

    .line 1819060
    invoke-virtual {p0}, Lcom/facebook/events/ui/location/LocationPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e0a65

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method


# virtual methods
.method public getPickedLocation()Lcom/facebook/events/ui/location/EventLocationModel;
    .locals 1

    .prologue
    .line 1819089
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    return-object v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 5

    .prologue
    .line 1819081
    instance-of v0, p1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;

    if-nez v0, :cond_0

    .line 1819082
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/text/BadgeTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1819083
    :goto_0
    return-void

    .line 1819084
    :cond_0
    check-cast p1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;

    .line 1819085
    invoke-virtual {p1}, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1819086
    new-instance v0, Lcom/facebook/events/ui/location/EventLocationModel;

    iget-object v1, p1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    iget-wide v2, p1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->b:J

    iget-object v4, p1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/events/ui/location/EventLocationModel;-><init>(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;JLjava/lang/String;)V

    iput-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819087
    iget-boolean v0, p1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->d:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->b:Z

    .line 1819088
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationPicker;->b()V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    .prologue
    .line 1819066
    invoke-super {p0}, Lcom/facebook/fbui/widget/text/BadgeTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1819067
    new-instance v1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/events/ui/location/LocationPicker$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1819068
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819069
    iget-object v2, v0, Lcom/facebook/events/ui/location/EventLocationModel;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-object v0, v2

    .line 1819070
    invoke-static {v0}, Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;->a(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v0

    .line 1819071
    iput-object v0, v1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->a:Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    .line 1819072
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819073
    iget-wide v4, v0, Lcom/facebook/events/ui/location/EventLocationModel;->b:J

    move-wide v2, v4

    .line 1819074
    iput-wide v2, v1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->b:J

    .line 1819075
    iget-object v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819076
    iget-object v2, v0, Lcom/facebook/events/ui/location/EventLocationModel;->c:Ljava/lang/String;

    move-object v0, v2

    .line 1819077
    iput-object v0, v1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->c:Ljava/lang/String;

    .line 1819078
    iget-boolean v0, p0, Lcom/facebook/events/ui/location/LocationPicker;->b:Z

    .line 1819079
    iput-boolean v0, v1, Lcom/facebook/events/ui/location/LocationPicker$SavedState;->d:Z

    .line 1819080
    return-object v1
.end method

.method public setHasClickedOnLocationPicker(Z)V
    .locals 0

    .prologue
    .line 1819064
    iput-boolean p1, p0, Lcom/facebook/events/ui/location/LocationPicker;->b:Z

    .line 1819065
    return-void
.end method

.method public setPickedLocation(Lcom/facebook/events/ui/location/EventLocationModel;)V
    .locals 0

    .prologue
    .line 1819061
    if-nez p1, :cond_0

    new-instance p1, Lcom/facebook/events/ui/location/EventLocationModel;

    invoke-direct {p1}, Lcom/facebook/events/ui/location/EventLocationModel;-><init>()V

    :cond_0
    iput-object p1, p0, Lcom/facebook/events/ui/location/LocationPicker;->a:Lcom/facebook/events/ui/location/EventLocationModel;

    .line 1819062
    invoke-direct {p0}, Lcom/facebook/events/ui/location/LocationPicker;->b()V

    .line 1819063
    return-void
.end method
