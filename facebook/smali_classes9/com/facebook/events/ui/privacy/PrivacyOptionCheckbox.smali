.class public Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;
.super Lcom/facebook/resources/ui/FbCheckedTextView;
.source ""


# instance fields
.field public a:LX/Bj5;

.field public b:Z

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1819283
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbCheckedTextView;-><init>(Landroid/content/Context;)V

    .line 1819284
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->b()V

    .line 1819285
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1819280
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1819281
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->b()V

    .line 1819282
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1819277
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbCheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1819278
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->b()V

    .line 1819279
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1819275
    new-instance v0, LX/Bmr;

    invoke-direct {v0, p0}, LX/Bmr;-><init>(Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;)V

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1819276
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/PrivacyType;Z)V
    .locals 1

    .prologue
    .line 1819251
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    if-ne p1, v0, :cond_0

    .line 1819252
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->setVisibility(I)V

    .line 1819253
    invoke-virtual {p0, p2}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->setChecked(Z)V

    .line 1819254
    :goto_0
    return-void

    .line 1819255
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1819274
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->b:Z

    return v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1819265
    instance-of v0, p1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;

    if-nez v0, :cond_1

    .line 1819266
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbCheckedTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1819267
    :cond_0
    :goto_0
    return-void

    .line 1819268
    :cond_1
    check-cast p1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;

    .line 1819269
    invoke-virtual {p1}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbCheckedTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1819270
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->c:Z

    .line 1819271
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;->b:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->b:Z

    .line 1819272
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a:LX/Bj5;

    if-eqz v0, :cond_0

    .line 1819273
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a:LX/Bj5;

    iget-boolean v1, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->c:Z

    invoke-interface {v0, v1}, LX/Bj5;->a(Z)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1819258
    invoke-super {p0}, Lcom/facebook/resources/ui/FbCheckedTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1819259
    new-instance v1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1819260
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->c:Z

    .line 1819261
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;->a:Z

    .line 1819262
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->b:Z

    .line 1819263
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox$SavedState;->b:Z

    .line 1819264
    return-object v1
.end method

.method public setOnPrivacyOptionToggledListener(LX/Bj5;)V
    .locals 0

    .prologue
    .line 1819256
    iput-object p1, p0, Lcom/facebook/events/ui/privacy/PrivacyOptionCheckbox;->a:LX/Bj5;

    .line 1819257
    return-void
.end method
