.class public Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/widget/text/BetterTextView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private final c:LX/1OB;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1819218
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1819219
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->c:LX/1OB;

    .line 1819220
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a()V

    .line 1819221
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1819195
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1819196
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->c:LX/1OB;

    .line 1819197
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a()V

    .line 1819198
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1819214
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1819215
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->c:LX/1OB;

    .line 1819216
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a()V

    .line 1819217
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1819208
    const v0, 0x7f0304c5

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1819209
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->setOrientation(I)V

    .line 1819210
    const v0, 0x7f0d0df0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 1819211
    const v0, 0x7f0d0df1

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1819212
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->b:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Bmq;

    invoke-direct {v1, p0}, LX/Bmq;-><init>(Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1819213
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1819222
    if-nez p1, :cond_0

    .line 1819223
    const/4 v0, 0x0

    .line 1819224
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    if-eq p1, v0, :cond_1

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne p1, v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b0e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b0f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1819204
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1819205
    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->setVisibility(I)V

    .line 1819206
    return-void

    .line 1819207
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1819201
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 1819202
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->c:LX/1OB;

    invoke-virtual {v0}, LX/1OB;->a()V

    .line 1819203
    return-void
.end method

.method public setOnDrawListenerTo(LX/0fu;)V
    .locals 1

    .prologue
    .line 1819199
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationPrivacyEducationWidgetRedesign;->c:LX/1OB;

    invoke-virtual {v0, p1}, LX/1OB;->b(LX/0fu;)V

    .line 1819200
    return-void
.end method
