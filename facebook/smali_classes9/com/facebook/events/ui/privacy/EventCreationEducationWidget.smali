.class public Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""


# instance fields
.field private a:Landroid/text/style/TextAppearanceSpan;

.field private b:Landroid/text/style/TextAppearanceSpan;

.field private final c:LX/1OB;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1819188
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1819189
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->c:LX/1OB;

    .line 1819190
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a()V

    .line 1819191
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1819184
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1819185
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->c:LX/1OB;

    .line 1819186
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a()V

    .line 1819187
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1819180
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1819181
    new-instance v0, LX/1OB;

    invoke-direct {v0}, LX/1OB;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->c:LX/1OB;

    .line 1819182
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a()V

    .line 1819183
    return-void
.end method

.method private static a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;
    .locals 2

    .prologue
    .line 1819176
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    .line 1819177
    invoke-virtual {p0, p1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1819178
    invoke-virtual {p0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    invoke-virtual {p0, p2, v0, v1, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1819179
    return-object p0
.end method

.method private a(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1819137
    sget-object v0, LX/Bmp;->a:[I

    invoke-virtual {p1}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819138
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1819139
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b05

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819140
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b08

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819141
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b0a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819142
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b0c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a()V
    .locals 3

    .prologue
    .line 1819173
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e08d6

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->b:Landroid/text/style/TextAppearanceSpan;

    .line 1819174
    new-instance v0, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e08d7

    invoke-direct {v0, v1, v2}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a:Landroid/text/style/TextAppearanceSpan;

    .line 1819175
    return-void
.end method

.method private b(Lcom/facebook/events/model/PrivacyType;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1819165
    sget-object v0, LX/Bmp;->a:[I

    invoke-virtual {p1}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819166
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1819167
    :pswitch_0
    if-eqz p2, :cond_0

    .line 1819168
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b06

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819169
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b07

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819170
    :pswitch_1
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b09

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819171
    :pswitch_2
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b0b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1819172
    :pswitch_3
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082b0d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/PrivacyType;Z)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/16 v5, 0x11

    const/4 v4, 0x0

    .line 1819152
    if-nez p1, :cond_1

    .line 1819153
    :cond_0
    :goto_0
    return-object v0

    .line 1819154
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a(Lcom/facebook/events/model/PrivacyType;)Ljava/lang/String;

    move-result-object v1

    .line 1819155
    invoke-direct {p0, p1, p2}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->b(Lcom/facebook/events/model/PrivacyType;Z)Ljava/lang/String;

    move-result-object v2

    .line 1819156
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1819157
    const v0, 0x7f020cc9

    invoke-static {p0, v0, v4, v4, v4}, LX/4lM;->a(Landroid/widget/TextView;IIII)V

    .line 1819158
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 1819159
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1819160
    iget-object v3, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->b:Landroid/text/style/TextAppearanceSpan;

    invoke-static {v0, v1, v3, v5}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;

    .line 1819161
    :cond_2
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1819162
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_3

    .line 1819163
    const-string v1, "\n"

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 1819164
    :cond_3
    iget-object v1, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a:Landroid/text/style/TextAppearanceSpan;

    invoke-static {v0, v2, v1, v5}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;Ljava/lang/Object;I)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 1819148
    invoke-virtual {p0, p1}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->setText(Ljava/lang/CharSequence;)V

    .line 1819149
    if-nez p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->setVisibility(I)V

    .line 1819150
    return-void

    .line 1819151
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1819145
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1819146
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->c:LX/1OB;

    invoke-virtual {v0}, LX/1OB;->a()V

    .line 1819147
    return-void
.end method

.method public setOnDrawListenerTo(LX/0fu;)V
    .locals 1

    .prologue
    .line 1819143
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/EventCreationEducationWidget;->c:LX/1OB;

    invoke-virtual {v0, p1}, LX/1OB;->b(LX/0fu;)V

    .line 1819144
    return-void
.end method
