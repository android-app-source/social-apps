.class public final Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;
.super Landroid/view/View$BaseSavedState;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/events/model/PrivacyType;

.field public b:Z

.field public c:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1819313
    new-instance v0, LX/Bmv;

    invoke-direct {v0}, LX/Bmv;-><init>()V

    sput-object v0, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1819308
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1819309
    const-class v0, Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyType;

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->a:Lcom/facebook/events/model/PrivacyType;

    .line 1819310
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->b:Z

    .line 1819311
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->c:Z

    .line 1819312
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1819300
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1819306
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1819307
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1819301
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1819302
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->a:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1819303
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819304
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819305
    return-void
.end method
