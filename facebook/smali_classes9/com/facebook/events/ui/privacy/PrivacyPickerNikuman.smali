.class public Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public a:LX/Bj3;

.field public b:Lcom/facebook/events/model/PrivacyType;

.field public c:Z

.field private d:Z

.field public e:Z

.field public f:Z

.field private g:Z

.field private h:Z

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:Landroid/graphics/Path;

.field private n:Landroid/graphics/Paint;

.field private o:Z

.field public p:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1819456
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;)V

    .line 1819457
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->f:Z

    .line 1819458
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->h:Z

    .line 1819459
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(Landroid/content/Context;)V

    .line 1819460
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1819543
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1819544
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->f:Z

    .line 1819545
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->h:Z

    .line 1819546
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(Landroid/content/Context;)V

    .line 1819547
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1819548
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1819549
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->f:Z

    .line 1819550
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->h:Z

    .line 1819551
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(Landroid/content/Context;)V

    .line 1819552
    return-void
.end method

.method private static a(Lcom/facebook/events/model/PrivacyType;)I
    .locals 3

    .prologue
    .line 1819553
    sget-object v0, LX/Bmw;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819554
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819555
    :pswitch_0
    const v0, 0x7f082af5

    .line 1819556
    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f082af4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/facebook/events/model/PrivacyType;Z)I
    .locals 3

    .prologue
    .line 1819557
    sget-object v0, LX/Bmw;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819558
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819559
    :pswitch_1
    const v0, 0x7f082af8

    .line 1819560
    :goto_0
    return v0

    .line 1819561
    :pswitch_2
    const v0, 0x7f082afb

    goto :goto_0

    .line 1819562
    :pswitch_3
    if-eqz p1, :cond_0

    .line 1819563
    const v0, 0x7f082afe

    goto :goto_0

    .line 1819564
    :cond_0
    const v0, 0x7f082b01

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static a(II)Landroid/graphics/Path;
    .locals 3

    .prologue
    .line 1819565
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 1819566
    int-to-float v1, p1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1819567
    int-to-float v1, p1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    int-to-float v2, p0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 1819568
    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    .line 1819569
    return-object v0
.end method

.method private a(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1819570
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->d:Z

    if-eqz v0, :cond_2

    .line 1819571
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    if-ne p2, v0, :cond_1

    .line 1819572
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v0

    .line 1819573
    :goto_0
    invoke-static {p2}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->d(Lcom/facebook/events/model/PrivacyType;)I

    move-result v1

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1819574
    :goto_1
    invoke-virtual {v0, v2}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 1819575
    iget-object v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b:Lcom/facebook/events/model/PrivacyType;

    if-ne p2, v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c:Z

    if-ne v1, p3, :cond_0

    .line 1819576
    invoke-virtual {v0, v2}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 1819577
    :cond_0
    new-instance v1, LX/Bmx;

    invoke-direct {v1, p0, p2, p3}, LX/Bmx;-><init>(Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;Lcom/facebook/events/model/PrivacyType;Z)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1819578
    return-void

    .line 1819579
    :cond_1
    invoke-static {p2}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(Lcom/facebook/events/model/PrivacyType;)I

    move-result v0

    invoke-static {p2}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c(Lcom/facebook/events/model/PrivacyType;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/5OG;->a(II)LX/3Ai;

    move-result-object v0

    goto :goto_0

    .line 1819580
    :cond_2
    invoke-static {p2}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b(Lcom/facebook/events/model/PrivacyType;)I

    move-result v0

    invoke-static {p2, p3}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(Lcom/facebook/events/model/PrivacyType;Z)I

    move-result v1

    invoke-virtual {p1, v0, v1}, LX/5OG;->a(II)LX/3Ai;

    move-result-object v0

    .line 1819581
    invoke-static {p2, p3}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b(Lcom/facebook/events/model/PrivacyType;Z)I

    move-result v1

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method private a(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1819629
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->setCursorVisible(Z)V

    .line 1819630
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->setFocusable(Z)V

    .line 1819631
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->setLines(I)V

    .line 1819632
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b(Landroid/content/Context;)V

    .line 1819633
    invoke-virtual {p0, p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1819634
    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    .line 1819582
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1819583
    iget v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->j:I

    .line 1819584
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getWidth()I

    move-result v1

    .line 1819585
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getHeight()I

    move-result v2

    .line 1819586
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingLeft()I

    move-result v3

    .line 1819587
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingRight()I

    move-result v4

    .line 1819588
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getScrollX()I

    move-result v5

    .line 1819589
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getScrollY()I

    move-result v6

    .line 1819590
    int-to-float v5, v5

    int-to-float v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1819591
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getLayout()Landroid/text/Layout;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/text/Layout;->getLineWidth(I)F

    move-result v5

    float-to-int v5, v5

    .line 1819592
    invoke-static {}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b()Z

    move-result v6

    if-eqz v6, :cond_0

    add-int v0, v5, v3

    iget v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->i:I

    add-int/2addr v0, v1

    .line 1819593
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getTotalPaddingTop()I

    move-result v1

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getTotalPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->k:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->l:I

    add-int/2addr v1, v2

    .line 1819594
    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1819595
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->m:Landroid/graphics/Path;

    iget-object v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->n:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1819596
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1819597
    return-void

    .line 1819598
    :cond_0
    sub-int/2addr v1, v5

    sub-int/2addr v1, v4

    sub-int v0, v1, v0

    iget v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->i:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private static b(Lcom/facebook/events/model/PrivacyType;)I
    .locals 3

    .prologue
    .line 1819599
    sget-object v0, LX/Bmw;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819600
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819601
    :pswitch_1
    const v0, 0x7f082af5

    .line 1819602
    :goto_0
    return v0

    .line 1819603
    :pswitch_2
    const v0, 0x7f082af9

    goto :goto_0

    .line 1819604
    :pswitch_3
    const v0, 0x7f082aff

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private static b(Lcom/facebook/events/model/PrivacyType;Z)I
    .locals 3

    .prologue
    .line 1819605
    sget-object v0, LX/Bmw;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819606
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819607
    :pswitch_1
    const v0, 0x7f0208b2

    .line 1819608
    :goto_0
    return v0

    .line 1819609
    :pswitch_2
    const v0, 0x7f0208be

    goto :goto_0

    .line 1819610
    :pswitch_3
    if-eqz p1, :cond_0

    .line 1819611
    const v0, 0x7f02088f

    goto :goto_0

    .line 1819612
    :cond_0
    const v0, 0x7f020850

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1819613
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1819614
    const v1, 0x7f0b1e2a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->i:I

    .line 1819615
    const v1, 0x7f0b1e2b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->j:I

    .line 1819616
    const v1, 0x7f0b1e2c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->k:I

    .line 1819617
    const v1, 0x7f0b1e2d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->l:I

    .line 1819618
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->n:Landroid/graphics/Paint;

    .line 1819619
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->n:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 1819620
    iget v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->k:I

    iget v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->j:I

    invoke-static {v0, v1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(II)Landroid/graphics/Path;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->m:Landroid/graphics/Path;

    .line 1819621
    return-void
.end method

.method private static b()Z
    .locals 1

    .prologue
    .line 1819622
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/3rK;->a(Ljava/util/Locale;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Lcom/facebook/events/model/PrivacyType;)I
    .locals 3

    .prologue
    .line 1819539
    sget-object v0, LX/Bmw;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819540
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819541
    :pswitch_0
    const v0, 0x7f082af8

    .line 1819542
    :goto_0
    return v0

    :pswitch_1
    const v0, 0x7f082af6

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static d(Lcom/facebook/events/model/PrivacyType;)I
    .locals 3

    .prologue
    .line 1819623
    sget-object v0, LX/Bmw;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819624
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819625
    :pswitch_1
    const v0, 0x7f0208b2

    .line 1819626
    :goto_0
    return v0

    .line 1819627
    :pswitch_2
    const v0, 0x7f020850

    goto :goto_0

    .line 1819628
    :pswitch_3
    const v0, 0x7f02078b

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/PrivacyType;ZZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1819534
    iput-object p1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b:Lcom/facebook/events/model/PrivacyType;

    .line 1819535
    iput-boolean p2, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c:Z

    .line 1819536
    iput-boolean p3, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->d:Z

    .line 1819537
    invoke-virtual {p0, p4}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->setText(Ljava/lang/CharSequence;)V

    .line 1819538
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 1819531
    iput-boolean p1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->o:Z

    .line 1819532
    iput-object p2, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->p:Ljava/lang/String;

    .line 1819533
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1819530
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->h:Z

    return v0
.end method

.method public getCommunityName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1819529
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->p:Ljava/lang/String;

    return-object v0
.end method

.method public getCompoundPaddingLeft()I
    .locals 2

    .prologue
    .line 1819525
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingLeft()I

    move-result v0

    .line 1819526
    invoke-static {}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1819527
    iget v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->i:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->j:I

    add-int/2addr v0, v1

    .line 1819528
    :cond_0
    return v0
.end method

.method public getCompoundPaddingRight()I
    .locals 2

    .prologue
    .line 1819521
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->getCompoundPaddingRight()I

    move-result v0

    .line 1819522
    invoke-static {}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1819523
    iget v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->i:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->j:I

    add-int/2addr v0, v1

    .line 1819524
    :cond_0
    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x2

    const v1, -0x44e5f373

    invoke-static {v0, v5, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1819506
    iput-boolean v5, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->h:Z

    .line 1819507
    new-instance v2, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1819508
    invoke-virtual {v2}, LX/5OM;->c()LX/5OG;

    move-result-object v3

    .line 1819509
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    iget-boolean v4, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c:Z

    invoke-direct {p0, v3, v0, v4}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1819510
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->e:Z

    if-eqz v0, :cond_0

    .line 1819511
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_GUESTS:Lcom/facebook/events/model/PrivacyType;

    iget-boolean v4, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c:Z

    invoke-direct {p0, v3, v0, v4}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1819512
    :cond_0
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->f:Z

    if-eqz v0, :cond_1

    .line 1819513
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b:Lcom/facebook/events/model/PrivacyType;

    sget-object v4, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v4, :cond_3

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    .line 1819514
    :goto_0
    iget-boolean v4, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c:Z

    invoke-direct {p0, v3, v0, v4}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1819515
    :cond_1
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->o:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->p:Ljava/lang/String;

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1819516
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->COMMUNITY:Lcom/facebook/events/model/PrivacyType;

    iget-boolean v4, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c:Z

    invoke-direct {p0, v3, v0, v4}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1819517
    :cond_2
    invoke-virtual {v2, v5}, LX/5OM;->a(Z)V

    .line 1819518
    invoke-virtual {v2, p0}, LX/0ht;->f(Landroid/view/View;)V

    .line 1819519
    const v0, -0x3dec6b65

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 1819520
    :cond_3
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->USER_PUBLIC:Lcom/facebook/events/model/PrivacyType;

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1819502
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1819503
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->g:Z

    if-nez v0, :cond_0

    .line 1819504
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a(Landroid/graphics/Canvas;)V

    .line 1819505
    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 1819488
    instance-of v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;

    if-nez v0, :cond_1

    .line 1819489
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1819490
    :cond_0
    :goto_0
    return-void

    .line 1819491
    :cond_1
    check-cast p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;

    .line 1819492
    invoke-virtual {p1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1819493
    iget-object v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->a:Lcom/facebook/events/model/PrivacyType;

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b:Lcom/facebook/events/model/PrivacyType;

    .line 1819494
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->b:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c:Z

    .line 1819495
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->c:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->g:Z

    .line 1819496
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->d:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->d:Z

    .line 1819497
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->e:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->h:Z

    .line 1819498
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->f:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->e:Z

    .line 1819499
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->g:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->f:Z

    .line 1819500
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a:LX/Bj3;

    if-eqz v0, :cond_0

    .line 1819501
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a:LX/Bj3;

    iget-object v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b:Lcom/facebook/events/model/PrivacyType;

    invoke-interface {v0, v1}, LX/Bj3;->a(Lcom/facebook/events/model/PrivacyType;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1819471
    invoke-super {p0}, Lcom/facebook/resources/ui/FbTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1819472
    new-instance v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1819473
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->b:Lcom/facebook/events/model/PrivacyType;

    .line 1819474
    iput-object v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->a:Lcom/facebook/events/model/PrivacyType;

    .line 1819475
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->c:Z

    .line 1819476
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->b:Z

    .line 1819477
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->g:Z

    .line 1819478
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->c:Z

    .line 1819479
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->d:Z

    .line 1819480
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->d:Z

    .line 1819481
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->h:Z

    .line 1819482
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->e:Z

    .line 1819483
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->e:Z

    .line 1819484
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->f:Z

    .line 1819485
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->f:Z

    .line 1819486
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->g:Z

    .line 1819487
    return-object v1
.end method

.method public setIsShowingOpenInviteOption(Z)V
    .locals 0

    .prologue
    .line 1819469
    iput-boolean p1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->e:Z

    .line 1819470
    return-void
.end method

.method public setIsShowingPublicOption(Z)V
    .locals 0

    .prologue
    .line 1819467
    iput-boolean p1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->f:Z

    .line 1819468
    return-void
.end method

.method public setOnPrivacyChangedListener(LX/Bj3;)V
    .locals 0

    .prologue
    .line 1819465
    iput-object p1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->a:LX/Bj3;

    .line 1819466
    return-void
.end method

.method public setPrivacyLocked(Z)V
    .locals 1

    .prologue
    .line 1819461
    iput-boolean p1, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->g:Z

    .line 1819462
    if-eqz p1, :cond_0

    .line 1819463
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1819464
    :cond_0
    return-void
.end method
