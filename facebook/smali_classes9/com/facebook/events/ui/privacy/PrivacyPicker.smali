.class public Lcom/facebook/events/ui/privacy/PrivacyPicker;
.super Lcom/facebook/resources/ui/FbEditText;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public b:Lcom/facebook/events/model/PrivacyType;

.field public c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1819386
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;)V

    .line 1819387
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->d:Z

    .line 1819388
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b()V

    .line 1819389
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1819405
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1819406
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->d:Z

    .line 1819407
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b()V

    .line 1819408
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1819401
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1819402
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->d:Z

    .line 1819403
    invoke-direct {p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b()V

    .line 1819404
    return-void
.end method

.method private static a(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)LX/3Ai;
    .locals 2

    .prologue
    .line 1819398
    invoke-static {p1, p2}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b(Lcom/facebook/events/model/PrivacyType;Z)I

    move-result v0

    invoke-static {p1, p2}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->c(Lcom/facebook/events/model/PrivacyType;Z)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/5OG;->a(II)LX/3Ai;

    move-result-object v0

    .line 1819399
    invoke-static {p1, p2}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->d(Lcom/facebook/events/model/PrivacyType;Z)I

    move-result v1

    invoke-virtual {v0, v1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1819400
    return-object v0
.end method

.method private a(Lcom/facebook/events/model/PrivacyType;Z)V
    .locals 1

    .prologue
    .line 1819391
    iput-object p1, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    .line 1819392
    iput-boolean p2, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->c:Z

    .line 1819393
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    invoke-static {v0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->a(Lcom/facebook/events/model/PrivacyType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1819394
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->setVisibility(I)V

    .line 1819395
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    .line 1819396
    :goto_0
    return-void

    .line 1819397
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->a()V

    goto :goto_0
.end method

.method private static a(Lcom/facebook/events/model/PrivacyType;)Z
    .locals 1

    .prologue
    .line 1819390
    if-eqz p0, :cond_0

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->GROUP:Lcom/facebook/events/model/PrivacyType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->PAGE:Lcom/facebook/events/model/PrivacyType;

    if-eq p0, v0, :cond_0

    sget-object v0, Lcom/facebook/events/model/PrivacyType;->FRIENDS_OF_FRIENDS:Lcom/facebook/events/model/PrivacyType;

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/facebook/events/model/PrivacyType;Z)I
    .locals 3

    .prologue
    .line 1819314
    sget-object v0, LX/Bmt;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819315
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819316
    :pswitch_1
    const v0, 0x7f082af3

    .line 1819317
    :goto_0
    return v0

    .line 1819318
    :pswitch_2
    const v0, 0x7f082af9

    goto :goto_0

    .line 1819319
    :pswitch_3
    if-eqz p1, :cond_0

    .line 1819320
    const v0, 0x7f082afc

    goto :goto_0

    .line 1819321
    :cond_0
    const v0, 0x7f082aff

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1819379
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->setCursorVisible(Z)V

    .line 1819380
    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->setFocusable(Z)V

    .line 1819381
    invoke-virtual {p0, v1}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->setLines(I)V

    .line 1819382
    invoke-virtual {p0, p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1819383
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    invoke-direct {p0, v0, v1}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->a(Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1819384
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->a()V

    .line 1819385
    return-void
.end method

.method private b(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1819373
    invoke-static {p1, p2, p3}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->a(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)LX/3Ai;

    move-result-object v0

    .line 1819374
    invoke-virtual {v0, v2}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 1819375
    iget-object v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    if-ne p2, v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->c:Z

    if-ne v1, p3, :cond_0

    .line 1819376
    invoke-virtual {v0, v2}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 1819377
    :cond_0
    new-instance v1, LX/Bmu;

    invoke-direct {v1, p0, p2, p3}, LX/Bmu;-><init>(Lcom/facebook/events/ui/privacy/PrivacyPicker;Lcom/facebook/events/model/PrivacyType;Z)V

    invoke-virtual {v0, v1}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 1819378
    return-void
.end method

.method private static c(Lcom/facebook/events/model/PrivacyType;Z)I
    .locals 3

    .prologue
    .line 1819409
    sget-object v0, LX/Bmt;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819410
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819411
    :pswitch_1
    const v0, 0x7f082af7

    .line 1819412
    :goto_0
    return v0

    .line 1819413
    :pswitch_2
    const v0, 0x7f082afa

    goto :goto_0

    .line 1819414
    :pswitch_3
    if-eqz p1, :cond_0

    .line 1819415
    const v0, 0x7f082afd

    goto :goto_0

    .line 1819416
    :cond_0
    const v0, 0x7f082b00

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static d(Lcom/facebook/events/model/PrivacyType;Z)I
    .locals 3

    .prologue
    .line 1819365
    sget-object v0, LX/Bmt;->a:[I

    invoke-virtual {p0}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819366
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported events privacy type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1819367
    :pswitch_1
    const v0, 0x7f0208b2

    .line 1819368
    :goto_0
    return v0

    .line 1819369
    :pswitch_2
    const v0, 0x7f0208be

    goto :goto_0

    .line 1819370
    :pswitch_3
    if-eqz p1, :cond_0

    .line 1819371
    const v0, 0x7f02088f

    goto :goto_0

    .line 1819372
    :cond_0
    const v0, 0x7f020850

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1819361
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1819362
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    iget-boolean v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->c:Z

    invoke-static {v0, v1}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b(Lcom/facebook/events/model/PrivacyType;Z)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->setText(I)V

    .line 1819363
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    iget-boolean v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->c:Z

    invoke-static {v0, v1}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->d(Lcom/facebook/events/model/PrivacyType;Z)I

    move-result v0

    invoke-virtual {p0, v2, v2, v0, v2}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 1819364
    :cond_0
    return-void
.end method

.method public getCanGuestsInviteFriends()Z
    .locals 1

    .prologue
    .line 1819360
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->c:Z

    return v0
.end method

.method public getEventCreateInputDataPrivacyType()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/EventPrivacyEnum;
    .end annotation

    .prologue
    .line 1819353
    sget-object v0, LX/Bmt;->a:[I

    iget-object v1, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v1}, Lcom/facebook/events/model/PrivacyType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1819354
    const-string v0, "GROUP"

    :goto_0
    return-object v0

    .line 1819355
    :pswitch_0
    const-string v0, "FRIENDS_OF_FRIENDS"

    goto :goto_0

    .line 1819356
    :pswitch_1
    const-string v0, "FRIENDS_OF_GUESTS"

    goto :goto_0

    .line 1819357
    :pswitch_2
    const-string v0, "INVITE_ONLY"

    goto :goto_0

    .line 1819358
    :pswitch_3
    const-string v0, "PAGE"

    goto :goto_0

    .line 1819359
    :pswitch_4
    const-string v0, "USER_PUBLIC"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public getPopoverMenuWindow()LX/5OM;
    .locals 5

    .prologue
    .line 1819345
    new-instance v1, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 1819346
    const v0, 0x7f082af1

    invoke-virtual {v1, v0}, LX/5OM;->c(I)V

    .line 1819347
    invoke-virtual {v1}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 1819348
    sget-object v0, Lcom/facebook/events/model/PrivacyType;->SELECTABLE_PRIVACY_TYPES:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyType;

    .line 1819349
    sget-object v4, Lcom/facebook/events/model/PrivacyType;->INVITE_ONLY:Lcom/facebook/events/model/PrivacyType;

    if-ne v0, v4, :cond_0

    .line 1819350
    const/4 v4, 0x0

    invoke-direct {p0, v2, v0, v4}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)V

    .line 1819351
    :cond_0
    const/4 v4, 0x1

    invoke-direct {p0, v2, v0, v4}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b(LX/5OG;Lcom/facebook/events/model/PrivacyType;Z)V

    goto :goto_0

    .line 1819352
    :cond_1
    return-object v1
.end method

.method public getPrivacyType()Lcom/facebook/events/model/PrivacyType;
    .locals 1

    .prologue
    .line 1819344
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const v0, -0x1508e52b

    invoke-static {v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1819339
    iput-boolean v2, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->d:Z

    .line 1819340
    invoke-virtual {p0}, Lcom/facebook/events/ui/privacy/PrivacyPicker;->getPopoverMenuWindow()LX/5OM;

    move-result-object v1

    .line 1819341
    invoke-virtual {v1, v2}, LX/5OM;->a(Z)V

    .line 1819342
    invoke-virtual {v1, p0}, LX/0ht;->a(Landroid/view/View;)V

    .line 1819343
    const v1, -0x1f31bdb2

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 1819331
    instance-of v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;

    if-nez v0, :cond_0

    .line 1819332
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1819333
    :goto_0
    return-void

    .line 1819334
    :cond_0
    check-cast p1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;

    .line 1819335
    invoke-virtual {p1}, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/facebook/resources/ui/FbEditText;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 1819336
    iget-object v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->a:Lcom/facebook/events/model/PrivacyType;

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    .line 1819337
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->b:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->c:Z

    .line 1819338
    iget-boolean v0, p1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->c:Z

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->d:Z

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 1819322
    invoke-super {p0}, Lcom/facebook/resources/ui/FbEditText;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 1819323
    new-instance v1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1819324
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->b:Lcom/facebook/events/model/PrivacyType;

    .line 1819325
    iput-object v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->a:Lcom/facebook/events/model/PrivacyType;

    .line 1819326
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->c:Z

    .line 1819327
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->b:Z

    .line 1819328
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPicker;->d:Z

    .line 1819329
    iput-boolean v0, v1, Lcom/facebook/events/ui/privacy/PrivacyPicker$SavedState;->c:Z

    .line 1819330
    return-object v1
.end method
