.class public final Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;
.super Landroid/view/View$BaseSavedState;
.source ""


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/events/model/PrivacyType;

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1819446
    new-instance v0, LX/Bmy;

    invoke-direct {v0}, LX/Bmy;-><init>()V

    sput-object v0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1819447
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1819448
    const-class v0, Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/model/PrivacyType;

    iput-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->a:Lcom/facebook/events/model/PrivacyType;

    .line 1819449
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->b:Z

    .line 1819450
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->c:Z

    .line 1819451
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->d:Z

    .line 1819452
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->e:Z

    .line 1819453
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->f:Z

    .line 1819454
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->g:Z

    .line 1819455
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1819445
    invoke-direct {p0, p1}, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 1819434
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1819435
    return-void
.end method


# virtual methods
.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1819436
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1819437
    iget-object v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->a:Lcom/facebook/events/model/PrivacyType;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1819438
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->b:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819439
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->c:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819440
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->d:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819441
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->e:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819442
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->f:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819443
    iget-boolean v0, p0, Lcom/facebook/events/ui/privacy/PrivacyPickerNikuman$SavedState;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1819444
    return-void
.end method
