.class public Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/Bls;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:Lcom/facebook/base/activity/FbFragmentActivity;

.field private s:Lcom/facebook/widget/listview/BetterListView;

.field private t:LX/Blj;

.field public u:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/events/planning/CalendarRange;",
            ">;"
        }
    .end annotation
.end field

.field private v:I

.field private final w:Landroid/view/View$OnClickListener;

.field private final x:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1816534
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1816535
    new-instance v0, LX/Blk;

    invoke-direct {v0, p0}, LX/Blk;-><init>(Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;)V

    iput-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->w:Landroid/view/View$OnClickListener;

    .line 1816536
    new-instance v0, LX/Bll;

    invoke-direct {v0, p0}, LX/Bll;-><init>(Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;)V

    iput-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->x:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 1816537
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1816538
    return-object v0
.end method

.method public static a(Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1816539
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1816540
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 1816541
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v4}, Ljava/util/Calendar;->add(II)V

    .line 1816542
    iget-object v1, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->r:Lcom/facebook/base/activity/FbFragmentActivity;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/facebook/events/ui/date/EventsCalendarDatePickerActivity;->a(Landroid/content/Context;Ljava/util/Calendar;Ljava/util/Calendar;Z)Landroid/content/Intent;

    move-result-object v1

    .line 1816543
    const-string v0, "extra_enable_time_picker"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1816544
    iget-object v2, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->q:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x6e

    iget-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->r:Lcom/facebook/base/activity/FbFragmentActivity;

    const-class v4, Lcom/facebook/base/activity/FbFragmentActivity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 1816545
    return-void
.end method

.method private static a(Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;LX/Bls;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0

    .prologue
    .line 1816546
    iput-object p1, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->p:LX/Bls;

    iput-object p2, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->q:Lcom/facebook/content/SecureContextHelper;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;

    new-instance v0, LX/Bls;

    invoke-direct {v0}, LX/Bls;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/Bls;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0, v0, v1}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->a(Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;LX/Bls;Lcom/facebook/content/SecureContextHelper;)V

    return-void
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1816547
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1816548
    iput-object p0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->r:Lcom/facebook/base/activity/FbFragmentActivity;

    .line 1816549
    invoke-static {p0, p0}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1816550
    const v0, 0x7f030f8a

    invoke-virtual {p0, v0}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->setContentView(I)V

    .line 1816551
    invoke-virtual {p0}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_poll_times"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1816552
    invoke-virtual {p0}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_poll_times"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->u:Ljava/util/ArrayList;

    .line 1816553
    :goto_0
    new-instance v0, LX/Blj;

    iget-object v1, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->u:Ljava/util/ArrayList;

    const/16 v2, 0x6f

    invoke-direct {v0, p0, v1, v2}, LX/Blj;-><init>(Landroid/content/Context;Ljava/util/ArrayList;I)V

    iput-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->t:LX/Blj;

    .line 1816554
    const v0, 0x7f0d2586

    invoke-virtual {p0, v0}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    iput-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->s:Lcom/facebook/widget/listview/BetterListView;

    .line 1816555
    iget-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->s:Lcom/facebook/widget/listview/BetterListView;

    const v1, 0x7f0d2587    # 1.87616E38f

    invoke-virtual {p0, v1}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 1816556
    iget-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->s:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->t:LX/Blj;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1816557
    const v0, 0x7f0d2588

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    .line 1816558
    new-instance v1, LX/Blm;

    invoke-direct {v1, p0}, LX/Blm;-><init>(Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1816559
    iget-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->p:LX/Bls;

    const v1, 0x7f082aeb

    invoke-virtual {p0, v1}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    const v1, 0x7f082aec

    invoke-virtual {p0, v1}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v1, 0x7f0d2585

    invoke-virtual {p0, v1}, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->findViewById(I)Landroid/view/View;

    iget-object v4, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->w:Landroid/view/View$OnClickListener;

    iget-object v5, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->x:Landroid/view/View$OnClickListener;

    move-object v1, p0

    const/4 p1, 0x1

    .line 1816560
    invoke-static {v1}, LX/63Z;->b(Landroid/app/Activity;)Z

    .line 1816561
    const v6, 0x7f0d00bc

    invoke-virtual {v1, v6}, Lcom/facebook/base/activity/FbFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, LX/0h5;

    .line 1816562
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v7

    .line 1816563
    iput p1, v7, LX/108;->a:I

    .line 1816564
    move-object v7, v7

    .line 1816565
    iput-object v3, v7, LX/108;->g:Ljava/lang/String;

    .line 1816566
    move-object v7, v7

    .line 1816567
    const/4 p0, -0x2

    .line 1816568
    iput p0, v7, LX/108;->h:I

    .line 1816569
    move-object v7, v7

    .line 1816570
    invoke-virtual {v7}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v7

    .line 1816571
    invoke-interface {v6, p1}, LX/0h5;->setShowDividers(Z)V

    .line 1816572
    invoke-interface {v6, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1816573
    invoke-static {v7}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0h5;->setButtonSpecs(Ljava/util/List;)V

    .line 1816574
    const/4 v7, 0x0

    invoke-interface {v6, v7}, LX/0h5;->setHasBackButton(Z)V

    .line 1816575
    invoke-interface {v6, v4}, LX/0h5;->a(Landroid/view/View$OnClickListener;)V

    .line 1816576
    new-instance v7, LX/Blr;

    invoke-direct {v7, v0, v5}, LX/Blr;-><init>(LX/Bls;Landroid/view/View$OnClickListener;)V

    invoke-interface {v6, v7}, LX/0h5;->setOnToolbarButtonListener(LX/63W;)V

    .line 1816577
    return-void

    .line 1816578
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->u:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 1816579
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/base/activity/FbFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 1816580
    if-ne p2, v5, :cond_0

    const/16 v0, 0x6e

    if-ne p1, v0, :cond_0

    .line 1816581
    const-string v0, "extra_start_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1816582
    const-string v0, "extra_start_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move-object v1, v0

    .line 1816583
    :goto_0
    const-string v0, "extra_end_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1816584
    const-string v0, "extra_end_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    .line 1816585
    :goto_1
    iget-object v3, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->u:Ljava/util/ArrayList;

    new-instance v4, Lcom/facebook/events/planning/CalendarRange;

    invoke-direct {v4, v1, v0}, Lcom/facebook/events/planning/CalendarRange;-><init>(Ljava/util/Calendar;Ljava/util/Calendar;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1816586
    iget-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->t:LX/Blj;

    const v1, 0x6b54556a

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1816587
    :cond_0
    if-ne p2, v5, :cond_2

    const/16 v0, 0x6f

    if-ne p1, v0, :cond_2

    .line 1816588
    const-string v0, "extra_start_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1816589
    const-string v0, "extra_start_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move-object v1, v0

    .line 1816590
    :goto_2
    const-string v0, "extra_end_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1816591
    const-string v0, "extra_end_time"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    move-object v2, v0

    .line 1816592
    :cond_1
    iget-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->u:Ljava/util/ArrayList;

    iget v3, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->v:I

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/planning/CalendarRange;

    .line 1816593
    iput-object v1, v0, Lcom/facebook/events/planning/CalendarRange;->a:Ljava/util/Calendar;

    .line 1816594
    iget-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->u:Ljava/util/ArrayList;

    iget v1, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->v:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/planning/CalendarRange;

    .line 1816595
    iput-object v2, v0, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    .line 1816596
    iget-object v0, p0, Lcom/facebook/events/planning/EventsPlanningPollCreationActivity;->t:LX/Blj;

    const v1, -0x57acc644

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1816597
    :cond_2
    return-void

    :cond_3
    move-object v1, v2

    goto :goto_2

    :cond_4
    move-object v0, v2

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_0
.end method
