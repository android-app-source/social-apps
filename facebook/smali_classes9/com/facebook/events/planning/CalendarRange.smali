.class public Lcom/facebook/events/planning/CalendarRange;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Ljava/util/Calendar;

.field public b:Ljava/util/Calendar;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1816510
    new-instance v0, LX/Bli;

    invoke-direct {v0}, LX/Bli;-><init>()V

    sput-object v0, Lcom/facebook/events/planning/CalendarRange;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1816505
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816506
    invoke-static {p1}, Lcom/facebook/events/planning/CalendarRange;->b(Landroid/os/Parcel;)Ljava/util/Calendar;

    move-result-object v0

    .line 1816507
    invoke-static {p1}, Lcom/facebook/events/planning/CalendarRange;->b(Landroid/os/Parcel;)Ljava/util/Calendar;

    move-result-object v1

    .line 1816508
    invoke-static {p0, v0, v1}, Lcom/facebook/events/planning/CalendarRange;->a(Lcom/facebook/events/planning/CalendarRange;Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 1816509
    return-void
.end method

.method public constructor <init>(Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 0

    .prologue
    .line 1816502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816503
    invoke-static {p0, p1, p2}, Lcom/facebook/events/planning/CalendarRange;->a(Lcom/facebook/events/planning/CalendarRange;Ljava/util/Calendar;Ljava/util/Calendar;)V

    .line 1816504
    return-void
.end method

.method public static a(Lcom/facebook/events/planning/CalendarRange;Ljava/util/Calendar;Ljava/util/Calendar;)V
    .locals 0

    .prologue
    .line 1816499
    iput-object p1, p0, Lcom/facebook/events/planning/CalendarRange;->a:Ljava/util/Calendar;

    .line 1816500
    iput-object p2, p0, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    .line 1816501
    return-void
.end method

.method public static b(Landroid/os/Parcel;)Ljava/util/Calendar;
    .locals 4

    .prologue
    .line 1816494
    invoke-static {p0}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    .line 1816495
    if-eqz v0, :cond_0

    .line 1816496
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1816497
    invoke-virtual {p0}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1816498
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1816484
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1816485
    iget-object v0, p0, Lcom/facebook/events/planning/CalendarRange;->a:Ljava/util/Calendar;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1816486
    iget-object v0, p0, Lcom/facebook/events/planning/CalendarRange;->a:Ljava/util/Calendar;

    if-eqz v0, :cond_0

    .line 1816487
    iget-object v0, p0, Lcom/facebook/events/planning/CalendarRange;->a:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {p1, v4, v5}, Landroid/os/Parcel;->writeLong(J)V

    .line 1816488
    :cond_0
    iget-object v0, p0, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    if-eqz v0, :cond_3

    :goto_1
    invoke-static {p1, v1}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1816489
    iget-object v0, p0, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    if-eqz v0, :cond_1

    .line 1816490
    iget-object v0, p0, Lcom/facebook/events/planning/CalendarRange;->b:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1816491
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 1816492
    goto :goto_0

    :cond_3
    move v1, v2

    .line 1816493
    goto :goto_1
.end method
