.class public Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Blc;

.field public b:Ljava/lang/Integer;

.field public c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1816343
    new-instance v0, LX/Bld;

    invoke-direct {v0}, LX/Bld;-><init>()V

    sput-object v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Blc;)V
    .locals 0

    .prologue
    .line 1816340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816341
    iput-object p1, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    .line 1816342
    return-void
.end method

.method public constructor <init>(LX/Blc;Ljava/lang/Integer;I)V
    .locals 0

    .prologue
    .line 1816344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816345
    iput-object p1, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    .line 1816346
    iput-object p2, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    .line 1816347
    iput p3, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->c:I

    .line 1816348
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1816335
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816336
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/Blc;->valueOf(Ljava/lang/String;)LX/Blc;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    .line 1816337
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readValue(Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    .line 1816338
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->c:I

    .line 1816339
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1816334
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1816330
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->a:LX/Blc;

    invoke-virtual {v0}, LX/Blc;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816331
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->b:Ljava/lang/Integer;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeValue(Ljava/lang/Object;)V

    .line 1816332
    iget v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1816333
    return-void
.end method
