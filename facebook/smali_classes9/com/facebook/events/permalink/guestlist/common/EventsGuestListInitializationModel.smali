.class public Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/facebook/events/common/EventActionContext;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field private d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

.field private e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

.field public f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

.field public g:Z

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;",
            ">;"
        }
    .end annotation
.end field

.field public i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1816432
    new-instance v0, LX/Blf;

    invoke-direct {v0}, LX/Blf;-><init>()V

    sput-object v0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/Blg;)V
    .locals 1

    .prologue
    .line 1816433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816434
    iget-object v0, p1, LX/Blg;->a:Lcom/facebook/events/common/EventActionContext;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->a:Lcom/facebook/events/common/EventActionContext;

    .line 1816435
    iget-object v0, p1, LX/Blg;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->b:Ljava/lang/String;

    .line 1816436
    iget-object v0, p1, LX/Blg;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->c:Ljava/lang/String;

    .line 1816437
    iget-object v0, p1, LX/Blg;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1816438
    iget-object v0, p1, LX/Blg;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1816439
    iget-object v0, p1, LX/Blg;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1816440
    iget-boolean v0, p1, LX/Blg;->g:Z

    iput-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->g:Z

    .line 1816441
    iget-object v0, p1, LX/Blg;->h:LX/0Px;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->h:LX/0Px;

    .line 1816442
    iget-boolean v0, p1, LX/Blg;->i:Z

    iput-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->i:Z

    .line 1816443
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 5

    .prologue
    .line 1816391
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1816392
    const-class v0, Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/common/EventActionContext;

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->a:Lcom/facebook/events/common/EventActionContext;

    .line 1816393
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->b:Ljava/lang/String;

    .line 1816394
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->c:Ljava/lang/String;

    .line 1816395
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 1816396
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 1816397
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 1816398
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->g:Z

    .line 1816399
    const-class v0, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 1816400
    if-nez v0, :cond_0

    .line 1816401
    const/4 v1, 0x0

    .line 1816402
    :goto_0
    move-object v0, v1

    .line 1816403
    iput-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->h:LX/0Px;

    .line 1816404
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->i:Z

    .line 1816405
    return-void

    .line 1816406
    :cond_0
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1816407
    array-length v4, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v1, v0, v2

    .line 1816408
    check-cast v1, Lcom/facebook/events/permalink/guestlist/common/EventGuestSingleListModel;

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1816409
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1816410
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final d()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;
    .locals 1

    .prologue
    .line 1816431
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1816430
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;
    .locals 1

    .prologue
    .line 1816429
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1816411
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->a:Lcom/facebook/events/common/EventActionContext;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1816412
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816413
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816414
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->d:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816415
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->e:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLConnectionStyle;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816416
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->f:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1816417
    iget-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->g:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1816418
    iget-object v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->h:LX/0Px;

    .line 1816419
    if-nez v0, :cond_3

    .line 1816420
    const/4 v1, 0x0

    .line 1816421
    :goto_2
    move-object v0, v1

    .line 1816422
    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 1816423
    iget-boolean v0, p0, Lcom/facebook/events/permalink/guestlist/common/EventsGuestListInitializationModel;->i:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1816424
    return-void

    :cond_1
    move-object v0, v1

    .line 1816425
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 1816426
    goto :goto_1

    .line 1816427
    :cond_3
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    .line 1816428
    new-array v1, v1, [Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Landroid/os/Parcelable;

    goto :goto_2
.end method
