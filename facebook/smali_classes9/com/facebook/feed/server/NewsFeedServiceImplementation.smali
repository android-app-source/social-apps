.class public Lcom/facebook/feed/server/NewsFeedServiceImplementation;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0qU;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0oy;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0SG;

.field private final g:LX/0pX;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0SG;LX/0pX;LX/0oy;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/ApiMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0qU;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0SG;",
            "LX/0pX;",
            "LX/0oy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1711125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1711126
    iput-object p1, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a:LX/0Ot;

    .line 1711127
    iput-object p2, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->b:LX/0Ot;

    .line 1711128
    iput-object p3, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->c:LX/0Ot;

    .line 1711129
    iput-object p7, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->d:LX/0oy;

    .line 1711130
    iput-object p4, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->e:LX/0Or;

    .line 1711131
    iput-object p5, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->f:LX/0SG;

    .line 1711132
    iput-object p6, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->g:LX/0pX;

    .line 1711133
    return-void
.end method

.method private static a(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Lcom/facebook/api/feed/FetchFeedParams;LX/0rn;)LX/2VK;
    .locals 12

    .prologue
    const/4 v2, 0x1

    .line 1711134
    iget-object v0, p2, Lcom/facebook/api/feed/FetchFeedParams;->b:Lcom/facebook/api/feedtype/FeedType;

    move-object v0, v0

    .line 1711135
    invoke-virtual {v0}, Lcom/facebook/api/feedtype/FeedType;->e()LX/0pL;

    move-result-object v0

    sget-object v1, LX/0pL;->NO_CACHE:LX/0pL;

    if-eq v0, v1, :cond_2

    move v1, v2

    .line 1711136
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->d:LX/0oy;

    .line 1711137
    iget v8, v0, LX/0oy;->e:I

    const/4 v9, -0x1

    if-ne v8, v9, :cond_0

    .line 1711138
    iget-object v8, v0, LX/0oy;->a:LX/0W3;

    sget-wide v10, LX/0X5;->gp:J

    const/4 v9, 0x3

    invoke-interface {v8, v10, v11, v9}, LX/0W4;->a(JI)I

    move-result v8

    iput v8, v0, LX/0oy;->e:I

    .line 1711139
    :cond_0
    iget v8, v0, LX/0oy;->e:I

    move v0, v8

    .line 1711140
    iget v3, p2, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v3, v3

    .line 1711141
    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 1711142
    iget-object v0, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 1711143
    iget-object v3, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v3

    .line 1711144
    move-object v3, v0

    .line 1711145
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/18V;

    invoke-virtual {v0}, LX/18V;->a()LX/2VK;

    move-result-object v0

    sget-object v5, LX/2VL;->STREAMING:LX/2VL;

    invoke-interface {v0, v5}, LX/2VK;->a(LX/2VL;)LX/2VK;

    move-result-object v0

    .line 1711146
    iget-object v5, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->f:LX/0SG;

    invoke-static {v3, v5}, LX/0rh;->a(Ljava/lang/String;LX/0SG;)Ljava/lang/String;

    move-result-object v5

    .line 1711147
    new-instance v6, LX/0rT;

    invoke-direct {v6}, LX/0rT;-><init>()V

    invoke-virtual {v6, p2}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v6

    .line 1711148
    iput v4, v6, LX/0rT;->c:I

    .line 1711149
    move-object v6, v6

    .line 1711150
    sget-object v7, LX/0rU;->CHUNKED_INITIAL:LX/0rU;

    .line 1711151
    iput-object v7, v6, LX/0rT;->k:LX/0rU;

    .line 1711152
    move-object v6, v6

    .line 1711153
    iput-object v5, v6, LX/0rT;->h:Ljava/lang/String;

    .line 1711154
    move-object v5, v6

    .line 1711155
    invoke-virtual {v5}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v5

    .line 1711156
    invoke-static {p3, v5}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v5

    const-string v6, "first-fetch"

    .line 1711157
    iput-object v6, v5, LX/2Vk;->c:Ljava/lang/String;

    .line 1711158
    move-object v5, v5

    .line 1711159
    invoke-static {p0, p1, v1}, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Z)LX/4cn;

    move-result-object v6

    .line 1711160
    iput-object v6, v5, LX/2Vk;->f:LX/4cn;

    .line 1711161
    move-object v5, v5

    .line 1711162
    invoke-virtual {v5, v2}, LX/2Vk;->a(Z)LX/2Vk;

    move-result-object v5

    invoke-virtual {v5}, LX/2Vk;->a()LX/2Vj;

    move-result-object v5

    .line 1711163
    invoke-interface {v0, v5}, LX/2VK;->a(LX/2Vj;)V

    .line 1711164
    iget v5, p2, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v5, v5

    .line 1711165
    if-ge v4, v5, :cond_1

    .line 1711166
    new-instance v5, LX/0rT;

    invoke-direct {v5}, LX/0rT;-><init>()V

    invoke-virtual {v5, p2}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v5

    .line 1711167
    iget v6, p2, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v6, v6

    .line 1711168
    sub-int v4, v6, v4

    .line 1711169
    iput v4, v5, LX/0rT;->c:I

    .line 1711170
    move-object v4, v5

    .line 1711171
    const-string v5, "{result=first-fetch:$.viewer.news_feed.page_info.end_cursor}"

    .line 1711172
    iput-object v5, v4, LX/0rT;->f:Ljava/lang/String;

    .line 1711173
    move-object v4, v4

    .line 1711174
    sget-object v5, LX/0rU;->CHUNKED_REMAINDER:LX/0rU;

    .line 1711175
    iput-object v5, v4, LX/0rT;->k:LX/0rU;

    .line 1711176
    move-object v4, v4

    .line 1711177
    iget-object v5, p0, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->f:LX/0SG;

    invoke-static {v3, v5}, LX/0rh;->a(Ljava/lang/String;LX/0SG;)Ljava/lang/String;

    move-result-object v3

    .line 1711178
    iput-object v3, v4, LX/0rT;->h:Ljava/lang/String;

    .line 1711179
    move-object v3, v4

    .line 1711180
    invoke-virtual {v3}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    .line 1711181
    invoke-static {p3, v3}, LX/2Vj;->a(LX/0e6;Ljava/lang/Object;)LX/2Vk;

    move-result-object v3

    const-string v4, "second-fetch"

    .line 1711182
    iput-object v4, v3, LX/2Vk;->c:Ljava/lang/String;

    .line 1711183
    move-object v3, v3

    .line 1711184
    invoke-static {p0, p1, v1}, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Z)LX/4cn;

    move-result-object v1

    .line 1711185
    iput-object v1, v3, LX/2Vk;->f:LX/4cn;

    .line 1711186
    move-object v1, v3

    .line 1711187
    const-string v3, "first-fetch:$.viewer.news_feed.page_info.end_cursor"

    .line 1711188
    iput-object v3, v1, LX/2Vk;->e:Ljava/lang/String;

    .line 1711189
    move-object v1, v1

    .line 1711190
    invoke-virtual {v1, v2}, LX/2Vk;->a(Z)LX/2Vk;

    move-result-object v1

    invoke-virtual {v1}, LX/2Vk;->a()LX/2Vj;

    move-result-object v1

    invoke-interface {v0, v1}, LX/2VK;->a(LX/2Vj;)V

    .line 1711191
    :cond_1
    return-object v0

    .line 1711192
    :cond_2
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_0

    .line 1711193
    :cond_3
    const-string v0, "0"

    move-object v3, v0

    goto/16 :goto_1
.end method

.method private static a(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Z)LX/4cn;
    .locals 1

    .prologue
    .line 1711194
    new-instance v0, LX/Amc;

    invoke-direct {v0, p0, p1, p2}, LX/Amc;-><init>(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Z)V

    return-object v0
.end method

.method public static a(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Lcom/facebook/api/feed/FetchFeedParams;LX/14U;LX/0rn;)Lcom/facebook/api/feed/FetchFeedResult;
    .locals 9
    .param p2    # Lcom/facebook/api/feed/FetchFeedParams;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1711195
    invoke-static {p0, p1, p2, p4}, Lcom/facebook/feed/server/NewsFeedServiceImplementation;->a(Lcom/facebook/feed/server/NewsFeedServiceImplementation;LX/1qH;Lcom/facebook/api/feed/FetchFeedParams;LX/0rn;)LX/2VK;

    move-result-object v3

    .line 1711196
    :try_start_0
    iget-object v0, p2, Lcom/facebook/api/feed/FetchFeedParams;->k:Lcom/facebook/common/callercontext/CallerContext;

    move-object v0, v0

    .line 1711197
    invoke-static {v0}, LX/129;->b(Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 1711198
    if-nez v0, :cond_3

    .line 1711199
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    move-object v2, v0

    .line 1711200
    :goto_0
    const-string v1, "fetch-feed-batch"

    .line 1711201
    iget-object v0, p2, Lcom/facebook/api/feed/FetchFeedParams;->f:LX/0gf;

    move-object v0, v0

    .line 1711202
    invoke-virtual {v0}, LX/0gf;->isManual()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, "-ptr"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v2, v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Lcom/facebook/common/callercontext/CallerContext;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    .line 1711203
    invoke-interface {v3, v1, v0, p3}, LX/2VK;->a(Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;LX/14U;)V

    .line 1711204
    const-string v0, "first-fetch"

    invoke-interface {v3, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedResult;

    const-string v1, "second-fetch"

    invoke-interface {v3, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/feed/FetchFeedResult;

    invoke-virtual {v0, p2, v1}, Lcom/facebook/api/feed/FetchFeedResult;->a(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1711205
    :goto_2
    return-object v0

    :cond_0
    move-object v0, v1

    .line 1711206
    goto :goto_1

    .line 1711207
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 1711208
    const-string v0, "first-fetch"

    invoke-interface {v3, v0}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/feed/FetchFeedResult;

    .line 1711209
    const-string v1, "second-fetch"

    invoke-interface {v3, v1}, LX/2VK;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/api/feed/FetchFeedResult;

    .line 1711210
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    .line 1711211
    throw v2

    .line 1711212
    :cond_1
    if-nez v0, :cond_2

    .line 1711213
    :goto_3
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    .line 1711214
    iget-object v2, v1, Lcom/facebook/api/feed/FetchFeedResult;->b:Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-object v2, v2

    .line 1711215
    iget-object v3, v1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 1711216
    iget-wide v7, v1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v4, v7

    .line 1711217
    iget-boolean v6, v1, Lcom/facebook/api/feed/FetchFeedResult;->c:Z

    move v6, v6

    .line 1711218
    move-object v1, p2

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    goto :goto_2

    :cond_2
    move-object v1, v0

    .line 1711219
    goto :goto_3

    :cond_3
    move-object v2, v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/server/NewsFeedServiceImplementation;
    .locals 9

    .prologue
    .line 1711220
    new-instance v1, Lcom/facebook/feed/server/NewsFeedServiceImplementation;

    const/16 v2, 0xb79

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x788

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x764

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x12cb

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {p0}, LX/0pX;->a(LX/0QB;)LX/0pX;

    move-result-object v7

    check-cast v7, LX/0pX;

    invoke-static {p0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v8

    check-cast v8, LX/0oy;

    invoke-direct/range {v1 .. v8}, Lcom/facebook/feed/server/NewsFeedServiceImplementation;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0SG;LX/0pX;LX/0oy;)V

    .line 1711221
    move-object v0, v1

    .line 1711222
    return-object v0
.end method
