.class public Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;
.super LX/1OM;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/AlY;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:LX/1kG;

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BMP;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/Alh;

.field private final e:LX/1E1;

.field public final f:LX/B5l;

.field private final g:LX/0ad;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/media/util/model/MediaModel;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/1Ad;

.field public j:LX/1o9;

.field public k:Landroid/content/Context;

.field public l:I

.field public m:Landroid/widget/FrameLayout$LayoutParams;

.field public n:Ljava/lang/String;

.field public o:Landroid/graphics/drawable/Drawable;

.field public p:Landroid/graphics/drawable/Drawable;

.field public q:Landroid/widget/RelativeLayout$LayoutParams;

.field private r:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public s:LX/1RN;

.field public t:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "LX/1aZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1709841
    const-class v0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1Ad;Landroid/content/Context;LX/1kG;LX/0Or;LX/Alh;LX/1E1;LX/B5l;LX/0ad;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ad;",
            "Landroid/content/Context;",
            "LX/1kG;",
            "LX/0Or",
            "<",
            "LX/BMP;",
            ">;",
            "LX/Alh;",
            "LX/1E1;",
            "LX/B5l;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1709842
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 1709843
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->i:LX/1Ad;

    .line 1709844
    iput-object p2, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->k:Landroid/content/Context;

    .line 1709845
    iput-object p3, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->b:LX/1kG;

    .line 1709846
    iput-object p4, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->c:LX/0Or;

    .line 1709847
    iput-object p5, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->d:LX/Alh;

    .line 1709848
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->r:Ljava/util/ArrayList;

    .line 1709849
    iput-object p6, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->e:LX/1E1;

    .line 1709850
    iput-object p7, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->f:LX/B5l;

    .line 1709851
    iput-object p8, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->g:LX/0ad;

    .line 1709852
    return-void
.end method

.method public static a(Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;Landroid/net/Uri;LX/1o9;)LX/1aZ;
    .locals 2
    .param p0    # Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1709853
    if-nez p1, :cond_0

    .line 1709854
    const/4 v0, 0x0

    .line 1709855
    :goto_0
    return-object v0

    .line 1709856
    :cond_0
    invoke-static {p1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v0

    .line 1709857
    iput-object p2, v0, LX/1bX;->c:LX/1o9;

    .line 1709858
    move-object v0, v0

    .line 1709859
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1bX;->a(Z)LX/1bX;

    move-result-object v0

    invoke-virtual {v0}, LX/1bX;->n()LX/1bf;

    move-result-object v0

    .line 1709860
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->i:LX/1Ad;

    invoke-virtual {v1, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;
    .locals 9

    .prologue
    .line 1709861
    new-instance v0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v3

    check-cast v3, LX/1kG;

    const/16 v4, 0x2ff5

    invoke-static {p0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/Alh;->b(LX/0QB;)LX/Alh;

    move-result-object v5

    check-cast v5, LX/Alh;

    invoke-static {p0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v6

    check-cast v6, LX/1E1;

    invoke-static {p0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v7

    check-cast v7, LX/B5l;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v0 .. v8}, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;-><init>(LX/1Ad;Landroid/content/Context;LX/1kG;LX/0Or;LX/Alh;LX/1E1;LX/B5l;LX/0ad;)V

    .line 1709862
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    .line 1709863
    const/4 v4, 0x0

    .line 1709864
    sget-object v0, LX/AlZ;->VIDEO:LX/AlZ;

    invoke-virtual {v0}, LX/AlZ;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 1709865
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03126f

    invoke-virtual {v0, v1, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 1709866
    :goto_0
    const v0, 0x7f0d0883

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1709867
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setAspectRatio(F)V

    .line 1709868
    iget-object v2, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->i:LX/1Ad;

    sget-object v3, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    .line 1709869
    invoke-virtual {v0, v2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1709870
    iget-object v2, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081b2e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1709871
    sget-object v2, LX/AlZ;->VIDEO:LX/AlZ;

    invoke-virtual {v2}, LX/AlZ;->ordinal()I

    move-result v2

    if-ne p2, v2, :cond_2

    .line 1709872
    iget-object v2, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->g:LX/0ad;

    sget-short v3, LX/32h;->k:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1709873
    iget-object v2, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->q:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1709874
    :cond_0
    new-instance v0, LX/Ala;

    invoke-direct {v0, p0, v1}, LX/Ala;-><init>(Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;Landroid/view/View;)V

    .line 1709875
    :goto_1
    return-object v0

    .line 1709876
    :cond_1
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03126e

    invoke-virtual {v0, v1, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 1709877
    :cond_2
    iget-object v2, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->g:LX/0ad;

    sget-short v3, LX/32h;->k:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1709878
    iget-object v2, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->m:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1709879
    :cond_3
    new-instance v0, LX/AlY;

    invoke-direct {v0, p0, v1}, LX/AlY;-><init>(Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;Landroid/view/View;)V

    goto :goto_1
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1709880
    check-cast p1, LX/AlY;

    .line 1709881
    iget-object v0, p1, LX/AlY;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1709882
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, p2, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1709883
    if-eqz v1, :cond_0

    .line 1709884
    const/4 v1, 0x0

    sget-object v2, LX/1po;->UNKNOWN:LX/1po;

    invoke-virtual {p1, v1, v2}, LX/AlY;->a(Landroid/net/Uri;LX/1po;)V

    .line 1709885
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->p:Landroid/graphics/drawable/Drawable;

    sget-object v2, LX/1Up;->e:LX/1Up;

    invoke-virtual {v0, v1, v2}, LX/1af;->a(Landroid/graphics/drawable/Drawable;LX/1Up;)V

    .line 1709886
    :goto_1
    return-void

    .line 1709887
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 1709888
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 1709889
    iget-object v1, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v1, v1

    .line 1709890
    iget-object v2, v0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    move-object v2, v2

    .line 1709891
    sget-object v3, LX/1po;->VIDEO:LX/1po;

    if-ne v2, v3, :cond_1

    .line 1709892
    check-cast p1, LX/Ala;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    .line 1709893
    iget v3, v0, Lcom/facebook/media/util/model/MediaModel;->c:I

    move v0, v3

    .line 1709894
    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v2

    long-to-int v0, v2

    invoke-static {v0}, LX/BV7;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, LX/Ala;->a(Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_1

    .line 1709895
    :cond_1
    sget-object v0, LX/1po;->PHOTO:LX/1po;

    invoke-virtual {p1, v1, v0}, LX/AlY;->a(Landroid/net/Uri;LX/1po;)V

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 1709896
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 1709897
    sget-object v0, LX/AlZ;->MORE_MEDIA:LX/AlZ;

    invoke-virtual {v0}, LX/AlZ;->ordinal()I

    move-result v0

    .line 1709898
    :goto_0
    return v0

    .line 1709899
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 1709900
    iget-object v1, v0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    move-object v0, v1

    .line 1709901
    sget-object v1, LX/1po;->VIDEO:LX/1po;

    if-ne v0, v1, :cond_1

    .line 1709902
    sget-object v0, LX/AlZ;->VIDEO:LX/AlZ;

    invoke-virtual {v0}, LX/AlZ;->ordinal()I

    move-result v0

    goto :goto_0

    .line 1709903
    :cond_1
    sget-object v0, LX/AlZ;->PHOTO:LX/AlZ;

    invoke-virtual {v0}, LX/AlZ;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1709904
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1709905
    const/4 v0, 0x0

    .line 1709906
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
