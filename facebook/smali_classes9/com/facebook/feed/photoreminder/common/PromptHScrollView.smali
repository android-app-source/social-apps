.class public Lcom/facebook/feed/photoreminder/common/PromptHScrollView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# instance fields
.field private i:Landroid/content/Context;

.field private j:LX/1P1;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1710228
    invoke-direct {p0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;)V

    .line 1710229
    invoke-direct {p0, p1}, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->a(Landroid/content/Context;)V

    .line 1710230
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1710225
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1710226
    invoke-direct {p0, p1}, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->a(Landroid/content/Context;)V

    .line 1710227
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1710216
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1710217
    invoke-direct {p0, p1}, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->a(Landroid/content/Context;)V

    .line 1710218
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1710219
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->i:Landroid/content/Context;

    .line 1710220
    new-instance v0, LX/1P0;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->i:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v2}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    iput-object v0, p0, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->j:LX/1P1;

    .line 1710221
    new-instance v0, LX/62Z;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b1179

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-direct {v0, v1, v2}, LX/62Z;-><init>(II)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1710222
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->j:LX/1P1;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1710223
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/feed/photoreminder/common/PromptHScrollView;->setVisibility(I)V

    .line 1710224
    return-void
.end method
