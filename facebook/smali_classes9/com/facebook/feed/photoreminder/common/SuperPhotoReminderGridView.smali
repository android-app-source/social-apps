.class public Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# instance fields
.field private i:Landroid/content/Context;

.field private j:LX/3wu;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1710282
    invoke-direct {p0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;)V

    .line 1710283
    invoke-direct {p0, p1}, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;->a(Landroid/content/Context;)V

    .line 1710284
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1710285
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1710286
    invoke-direct {p0, p1}, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;->a(Landroid/content/Context;)V

    .line 1710287
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1710288
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1710289
    invoke-direct {p0, p1}, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;->a(Landroid/content/Context;)V

    .line 1710290
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1710291
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;->i:Landroid/content/Context;

    .line 1710292
    new-instance v0, LX/3wu;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;->i:Landroid/content/Context;

    const/4 v2, 0x4

    invoke-direct {v0, v1, v2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;->j:LX/3wu;

    .line 1710293
    new-instance v0, LX/62Y;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1190

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-direct {v0, v1}, LX/62Y;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 1710294
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/common/SuperPhotoReminderGridView;->j:LX/3wu;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1710295
    const/4 v0, 0x1

    .line 1710296
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 1710297
    return-void
.end method
