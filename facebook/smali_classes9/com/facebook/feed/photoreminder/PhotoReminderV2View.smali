.class public Lcom/facebook/feed/photoreminder/PhotoReminderV2View;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements LX/AkB;
.implements LX/AkM;


# instance fields
.field public a:LX/Alj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1kG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1E1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/1lR;

.field private e:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1710183
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1710184
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 1710186
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1710187
    const-class v0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    invoke-static {v0, p0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1710188
    const v0, 0x7f030f4c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1710189
    new-instance v1, LX/0zw;

    const v0, 0x7f0d1121

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    .line 1710190
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    .line 1710191
    iput-object v1, v0, LX/Alj;->a:LX/0zw;

    .line 1710192
    return-void
.end method

.method private static a(Lcom/facebook/feed/photoreminder/PhotoReminderV2View;LX/Alj;LX/1kG;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/photoreminder/PhotoReminderV2View;",
            "LX/Alj;",
            "LX/1kG;",
            "LX/0Or",
            "<",
            "LX/1E1;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1710185
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    iput-object p2, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->b:LX/1kG;

    iput-object p3, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->c:LX/0Or;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 4

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;

    new-instance v0, LX/Alj;

    invoke-direct {v0}, LX/Alj;-><init>()V

    move-object v0, v0

    move-object v0, v0

    check-cast v0, LX/Alj;

    invoke-static {v2}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v1

    check-cast v1, LX/1kG;

    const/16 v3, 0xfcd

    invoke-static {v2, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a(Lcom/facebook/feed/photoreminder/PhotoReminderV2View;LX/Alj;LX/1kG;LX/0Or;)V

    return-void
.end method

.method private b(LX/1lR;)V
    .locals 1

    .prologue
    .line 1710179
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1710180
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->a(LX/1lR;)V

    .line 1710181
    :goto_0
    return-void

    .line 1710182
    :cond_1
    invoke-direct {p0, p1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->c(LX/1lR;)V

    goto :goto_0
.end method

.method private c(LX/1lR;)V
    .locals 1

    .prologue
    .line 1710167
    invoke-direct {p0, p1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setupViewForPhotoReminder(LX/1lR;)V

    .line 1710168
    iget-boolean v0, p1, LX/1lR;->k:Z

    move v0, v0

    .line 1710169
    if-eqz v0, :cond_1

    .line 1710170
    :cond_0
    :goto_0
    return-void

    .line 1710171
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    .line 1710172
    iput-object p1, v0, LX/Alj;->b:LX/1lR;

    .line 1710173
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    invoke-virtual {v0}, LX/Alj;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1710174
    iget-boolean v0, p1, LX/1lR;->j:Z

    move v0, v0

    .line 1710175
    if-nez v0, :cond_0

    .line 1710176
    invoke-virtual {p1}, LX/1lR;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1710177
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->a(LX/1lR;)V

    goto :goto_0

    .line 1710178
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    invoke-virtual {v0}, LX/Alj;->a()V

    goto :goto_0
.end method

.method private setupViewForPhotoReminder(LX/1lR;)V
    .locals 3

    .prologue
    .line 1710193
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    new-instance v1, LX/Ali;

    invoke-direct {v1, p0, p1}, LX/Ali;-><init>(Lcom/facebook/feed/photoreminder/PhotoReminderV2View;LX/1lR;)V

    .line 1710194
    iget-object v2, v0, LX/Alj;->a:LX/0zw;

    invoke-virtual {v2}, LX/0zw;->a()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    .line 1710195
    iput-object v1, v2, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->i:LX/Ali;

    .line 1710196
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    .line 1710197
    iput-object p1, v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    .line 1710198
    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1710163
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    .line 1710164
    invoke-virtual {v0, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->setVisibility(I)V

    .line 1710165
    invoke-virtual {p0, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setVisibility(I)V

    .line 1710166
    return-object p0
.end method

.method public final a(LX/1lR;)V
    .locals 2

    .prologue
    .line 1710157
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1710158
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->d:LX/1lR;

    .line 1710159
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->d:LX/1lR;

    .line 1710160
    iput-object v1, v0, LX/Alj;->b:LX/1lR;

    .line 1710161
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->d:LX/1lR;

    invoke-direct {p0, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->b(LX/1lR;)V

    .line 1710162
    return-void
.end method

.method public getCollapseAnimator()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1710154
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    .line 1710155
    iget-object p0, v0, LX/Ale;->e:Landroid/animation/ValueAnimator;

    move-object v0, p0

    .line 1710156
    goto :goto_0
.end method

.method public getExpandAnimator()Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 1710136
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1E1;

    invoke-virtual {v0}, LX/1E1;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    .line 1710137
    iget-object p0, v0, LX/Ale;->d:Landroid/animation/ValueAnimator;

    move-object v0, p0

    .line 1710138
    goto :goto_0
.end method

.method public getPhotoTray()Landroid/view/View;
    .locals 1

    .prologue
    .line 1710153
    const v0, 0x7f0d250b

    invoke-virtual {p0, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x5238124b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1710145
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onDetachedFromWindow()V

    .line 1710146
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->d:LX/1lR;

    invoke-direct {p0, v1}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setupViewForPhotoReminder(LX/1lR;)V

    .line 1710147
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    iget-object v2, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->d:LX/1lR;

    .line 1710148
    iput-object v2, v1, LX/Alj;->b:LX/1lR;

    .line 1710149
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->a:LX/Alj;

    invoke-virtual {v1}, LX/Alj;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1710150
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->d:LX/1lR;

    const/4 v2, 0x0

    .line 1710151
    iput-boolean v2, v1, LX/1lR;->k:Z

    .line 1710152
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x3c7831bd

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onMeasure(II)V
    .locals 2

    .prologue
    .line 1710141
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1710142
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onMeasure(II)V

    .line 1710143
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-virtual {v0}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->getMinimumHeight()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->setMeasuredDimension(II)V

    .line 1710144
    return-void
.end method

.method public setPromptSession(LX/1RN;)V
    .locals 1

    .prologue
    .line 1710139
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderV2View;->e:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->setPromptSession(LX/1RN;)V

    .line 1710140
    return-void
.end method
