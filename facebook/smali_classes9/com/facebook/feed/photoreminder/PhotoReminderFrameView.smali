.class public Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;
.super LX/Ale;
.source ""


# instance fields
.field public b:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1kG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Alf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:LX/1lR;

.field public i:LX/Ali;

.field public j:Ljava/lang/String;

.field private k:LX/1RN;

.field private l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1710053
    invoke-direct {p0, p1}, LX/Ale;-><init>(Landroid/content/Context;)V

    .line 1710054
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1710051
    invoke-direct {p0, p1, p2}, LX/Ale;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1710052
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1710049
    invoke-direct {p0, p1, p2, p3}, LX/Ale;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1710050
    return-void
.end method

.method private static a(Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1kG;LX/Alf;Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;LX/0ad;)V
    .locals 0

    .prologue
    .line 1710015
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p2, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->c:LX/1kG;

    iput-object p3, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->d:LX/Alf;

    iput-object p4, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->e:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    iput-object p5, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->f:LX/0ad;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 6

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v5

    move-object v0, p0

    check-cast v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-static {v5}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v5}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v2

    check-cast v2, LX/1kG;

    invoke-static {v5}, LX/Alf;->a(LX/0QB;)LX/Alf;

    move-result-object v3

    check-cast v3, LX/Alf;

    invoke-static {v5}, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->b(LX/0QB;)Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    invoke-static {v5}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static/range {v0 .. v5}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->a(Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/1kG;LX/Alf;Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;LX/0ad;)V

    return-void
.end method


# virtual methods
.method public final a(LX/1lR;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1710029
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    .line 1710030
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->e:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1710031
    invoke-virtual {p1}, LX/1lR;->a()LX/0Px;

    move-result-object v0

    .line 1710032
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->f:LX/0ad;

    sget-short v2, LX/32h;->k:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/16 v2, 0x8

    if-le v1, v2, :cond_0

    .line 1710033
    invoke-virtual {p1}, LX/1lR;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v3, v1}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    .line 1710034
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->e:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    .line 1710035
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->b:LX/1kG;

    invoke-virtual {v2}, LX/1kG;->f()I

    move-result v2

    iput v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->l:I

    .line 1710036
    iput-object v0, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    .line 1710037
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->l:I

    iget p0, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->l:I

    invoke-direct {v2, v3, p0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iput-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->m:Landroid/widget/FrameLayout$LayoutParams;

    .line 1710038
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget v3, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->l:I

    iget p0, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->l:I

    invoke-direct {v2, v3, p0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->q:Landroid/widget/RelativeLayout$LayoutParams;

    .line 1710039
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->k:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p0, 0x7f0a05e6

    invoke-virtual {v3, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->o:Landroid/graphics/drawable/Drawable;

    .line 1710040
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020f36

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->p:Landroid/graphics/drawable/Drawable;

    .line 1710041
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->t:Ljava/util/HashMap;

    if-eqz v2, :cond_2

    .line 1710042
    :cond_1
    return-void

    .line 1710043
    :cond_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->t:Ljava/util/HashMap;

    .line 1710044
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->j:LX/1o9;

    if-nez v2, :cond_3

    .line 1710045
    new-instance v2, LX/1o9;

    iget v3, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->l:I

    iget p0, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->l:I

    invoke-direct {v2, v3, p0}, LX/1o9;-><init>(II)V

    iput-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->j:LX/1o9;

    .line 1710046
    :cond_3
    iget-object v2, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/media/util/model/MediaModel;

    .line 1710047
    iget-object p0, v2, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v2, p0

    .line 1710048
    iget-object p0, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->t:Ljava/util/HashMap;

    iget-object v0, v1, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->j:LX/1o9;

    invoke-static {v1, v2, v0}, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->a(Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;Landroid/net/Uri;LX/1o9;)LX/1aZ;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1710024
    const-class v0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;

    invoke-static {v0, p0}, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1710025
    invoke-super {p0}, LX/Ale;->b()V

    .line 1710026
    iget-object v0, p0, LX/Ale;->b:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 1710027
    iput-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->l:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1710028
    return-void
.end method

.method public final e()Z
    .locals 3

    .prologue
    .line 1710023
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->f:LX/0ad;

    sget-short v1, LX/32h;->k:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public final f()Landroid/animation/ValueAnimator;
    .locals 2

    .prologue
    .line 1710020
    invoke-super {p0}, LX/Ale;->f()Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1710021
    new-instance v1, LX/Alb;

    invoke-direct {v1, p0}, LX/Alb;-><init>(Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1710022
    return-object v0
.end method

.method public final g()Landroid/animation/ValueAnimator;
    .locals 2

    .prologue
    .line 1710017
    invoke-super {p0}, LX/Ale;->g()Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 1710018
    new-instance v1, LX/Alc;

    invoke-direct {v1, p0}, LX/Alc;-><init>(Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1710019
    return-object v0
.end method

.method public getImageTrayHeight()I
    .locals 1

    .prologue
    .line 1710016
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->c:LX/1kG;

    invoke-virtual {v0}, LX/1kG;->g()I

    move-result v0

    return v0
.end method

.method public getMediaReminderMode()LX/1lR;
    .locals 1

    .prologue
    .line 1709987
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    return-object v0
.end method

.method public getSessionId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1709988
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 1709989
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    .line 1709990
    iget-boolean p0, v0, LX/1lR;->h:Z

    move v0, p0

    .line 1709991
    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1709992
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    .line 1709993
    iget-boolean p0, v0, LX/1lR;->k:Z

    move v0, p0

    .line 1709994
    return v0
.end method

.method public final j()Landroid/animation/ObjectAnimator;
    .locals 2

    .prologue
    .line 1709995
    invoke-super {p0}, LX/Ale;->j()Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 1709996
    new-instance v1, LX/Ald;

    invoke-direct {v1, p0}, LX/Ald;-><init>(Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1709997
    return-object v0
.end method

.method public setCallback(LX/Ali;)V
    .locals 0

    .prologue
    .line 1709998
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->i:LX/Ali;

    .line 1709999
    return-void
.end method

.method public setHasBeenShown(Z)V
    .locals 1

    .prologue
    .line 1710000
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    .line 1710001
    iput-boolean p1, v0, LX/1lR;->h:Z

    .line 1710002
    return-void
.end method

.method public setIsAnimationRunning(Z)V
    .locals 1

    .prologue
    .line 1710003
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    .line 1710004
    iput-boolean p1, v0, LX/1lR;->k:Z

    .line 1710005
    return-void
.end method

.method public setMediaReminderModel(LX/1lR;)V
    .locals 0

    .prologue
    .line 1710006
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->h:LX/1lR;

    .line 1710007
    return-void
.end method

.method public setPromptSession(LX/1RN;)V
    .locals 1

    .prologue
    .line 1710008
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->k:LX/1RN;

    .line 1710009
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->e:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    .line 1710010
    iput-object p1, v0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->s:LX/1RN;

    .line 1710011
    return-void
.end method

.method public setSessionID(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1710012
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/PhotoReminderFrameView;->e:Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;

    .line 1710013
    iput-object p1, v0, Lcom/facebook/feed/photoreminder/PhotoGalleryScrollAdapter;->n:Ljava/lang/String;

    .line 1710014
    return-void
.end method
