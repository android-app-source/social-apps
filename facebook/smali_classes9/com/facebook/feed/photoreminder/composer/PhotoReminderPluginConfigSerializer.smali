.class public Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1710363
    const-class v0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;

    new-instance v1, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1710364
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1710365
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1710366
    if-nez p0, :cond_0

    .line 1710367
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1710368
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1710369
    invoke-static {p0, p1, p2}, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigSerializer;->b(Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;LX/0nX;LX/0my;)V

    .line 1710370
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1710371
    return-void
.end method

.method private static b(Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1710372
    const-string v0, "prompt_entry_point_analytics"

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1710373
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1710374
    check-cast p1, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigSerializer;->a(Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
