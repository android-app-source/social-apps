.class public Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "prompt_entry_point_analytics"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1710341
    const-class v0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1710340
    const-class v0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfigSerializer;

    return-object v0
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1710337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710338
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1710339
    return-void
.end method

.method private constructor <init>(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V
    .locals 0

    .prologue
    .line 1710333
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1710334
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    .line 1710335
    invoke-virtual {p0}, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;->a()V

    .line 1710336
    return-void
.end method

.method public static a(Lcom/facebook/productionprompts/logging/PromptAnalytics;)Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;
    .locals 1

    .prologue
    .line 1710332
    new-instance v0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;

    invoke-direct {v0, p0}, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;-><init>(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1710330
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1710331
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1710328
    const-string v0, "PHOTO_REMINDER_PERSIST_KEY"

    return-object v0
.end method

.method public final c()Lcom/facebook/productionprompts/logging/PromptAnalytics;
    .locals 1
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .prologue
    .line 1710329
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/composer/PhotoReminderPluginConfig;->mPromptAnalytics:Lcom/facebook/productionprompts/logging/PromptAnalytics;

    return-object v0
.end method
