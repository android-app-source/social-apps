.class public Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Qa;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/1Ri;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final c:LX/1kG;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/1kG;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSmallPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptLargePartDefinition;",
            ">;",
            "LX/1kG;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710380
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1710381
    iput-object p1, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;->a:LX/0Ot;

    .line 1710382
    iput-object p2, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;->b:LX/0Ot;

    .line 1710383
    iput-object p3, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;->c:LX/1kG;

    .line 1710384
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;
    .locals 6

    .prologue
    .line 1710385
    const-class v1, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;

    monitor-enter v1

    .line 1710386
    :try_start_0
    sget-object v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1710387
    sput-object v2, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1710388
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1710389
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1710390
    new-instance v4, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;

    const/16 v3, 0x6a9

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x6a8

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1kG;->a(LX/0QB;)LX/1kG;

    move-result-object v3

    check-cast v3, LX/1kG;

    invoke-direct {v4, v5, p0, v3}, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;-><init>(LX/0Ot;LX/0Ot;LX/1kG;)V

    .line 1710391
    move-object v0, v4

    .line 1710392
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1710393
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1710394
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1710395
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1710375
    check-cast p2, LX/1Ri;

    .line 1710376
    iget-object v0, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;->a:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;->b:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 1710377
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1710378
    check-cast p1, LX/1Ri;

    .line 1710379
    invoke-static {p1}, LX/1kG;->a(LX/1Ri;)Z

    move-result v0

    return v0
.end method
