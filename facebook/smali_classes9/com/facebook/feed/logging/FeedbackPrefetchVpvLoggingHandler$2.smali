.class public final Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/Runnable;

.field public final synthetic d:LX/AkS;


# direct methods
.method public constructor <init>(LX/AkS;Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 1709098
    iput-object p1, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->d:LX/AkS;

    iput-object p2, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->a:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p3, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->c:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1709099
    iget-object v0, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->d:LX/AkS;

    iget-object v0, v0, LX/AkS;->b:LX/0tn;

    iget-object v1, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->a:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1709100
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    .line 1709101
    if-eqz v2, :cond_1

    invoke-static {v0, v1}, LX/0tn;->b(LX/0tn;Lcom/facebook/graphql/model/GraphQLStory;)LX/0ul;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    .line 1709102
    iget-object v0, v3, LX/0ul;->a:Lcom/facebook/api/prefetch/GraphQLPrefetchController;

    iget-object v1, v3, LX/0ul;->b:LX/0uk;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/api/prefetch/GraphQLPrefetchController;->a(LX/0uk;Ljava/lang/String;)Z

    move-result v0

    move v2, v0

    .line 1709103
    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 1709104
    if-eqz v0, :cond_0

    .line 1709105
    iget-object v0, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->d:LX/AkS;

    iget-object v0, v0, LX/AkS;->f:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1709106
    iget-object v0, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->d:LX/AkS;

    iget-object v0, v0, LX/AkS;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/facebook/feed/logging/FeedbackPrefetchVpvLoggingHandler$2;->c:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 1709107
    :cond_0
    return-void

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
