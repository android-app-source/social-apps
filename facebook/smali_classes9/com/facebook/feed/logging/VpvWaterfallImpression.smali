.class public Lcom/facebook/feed/logging/VpvWaterfallImpression;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/logging/VpvWaterfallImpressionSerializer;
.end annotation


# instance fields
.field public stage:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "stage"
    .end annotation
.end field

.field public trackingCodes:LX/162;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tracking"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1709172
    const-class v0, Lcom/facebook/feed/logging/VpvWaterfallImpressionSerializer;

    return-object v0
.end method

.method public constructor <init>(LX/162;I)V
    .locals 0
    .param p1    # LX/162;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1709173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709174
    iput-object p1, p0, Lcom/facebook/feed/logging/VpvWaterfallImpression;->trackingCodes:LX/162;

    .line 1709175
    iput p2, p0, Lcom/facebook/feed/logging/VpvWaterfallImpression;->stage:I

    .line 1709176
    return-void
.end method
