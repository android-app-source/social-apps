.class public final Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/30r;

.field private b:Lcom/facebook/api/feedtype/FeedType;

.field private c:I


# direct methods
.method public constructor <init>(LX/30r;Lcom/facebook/api/feedtype/FeedType;I)V
    .locals 0

    .prologue
    .line 1709031
    iput-object p1, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->a:LX/30r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709032
    iput-object p2, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 1709033
    iput p3, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->c:I

    .line 1709034
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    .line 1709035
    iget-object v0, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->a:LX/30r;

    iget-object v0, v0, LX/30r;->c:LX/0Zb;

    iget-object v2, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->b:Lcom/facebook/api/feedtype/FeedType;

    iget v3, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->c:I

    iget-object v4, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->a:LX/30r;

    iget-object v5, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 1709036
    iget-object v6, v4, LX/30r;->b:LX/0pn;

    const/4 v7, 0x1

    .line 1709037
    const/4 v4, 0x0

    invoke-static {v6, v5, v4, v7}, LX/0pn;->a(LX/0pn;Lcom/facebook/api/feedtype/FeedType;IZ)I

    move-result v4

    move v6, v4

    .line 1709038
    move v4, v6

    .line 1709039
    iget-object v5, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->a:LX/30r;

    iget-object v6, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 1709040
    iget-object v7, v5, LX/30r;->b:LX/0pn;

    invoke-virtual {v7, v6}, LX/0pn;->d(Lcom/facebook/api/feedtype/FeedType;)I

    move-result v7

    move v5, v7

    .line 1709041
    iget-object v6, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->a:LX/30r;

    iget-object v7, p0, Lcom/facebook/feed/logging/CacheStateLogger$CacheStateLoggingTask;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 1709042
    iget-object v8, v6, LX/30r;->b:LX/0pn;

    invoke-virtual {v8, v7}, LX/0pn;->e(Lcom/facebook/api/feedtype/FeedType;)J

    move-result-wide v8

    move-wide v6, v8

    .line 1709043
    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "feed_db_cache_state"

    invoke-direct {v8, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "feed_type"

    invoke-virtual {v8, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v1, "num_new_stories"

    invoke-virtual {v8, v1, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v1, "num_unseen_stories_in_cache"

    invoke-virtual {v8, v1, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v1, "num_total_stories_in_cache"

    invoke-virtual {v8, v1, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v1, "num_gap_rows_in_cache"

    invoke-virtual {v8, v1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string v1, "native_newsfeed"

    .line 1709044
    iput-object v1, v8, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1709045
    move-object v8, v8

    .line 1709046
    move-object v8, v8

    .line 1709047
    move-object v1, v8

    .line 1709048
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1709049
    return-void
.end method
