.class public final Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/1B5;


# direct methods
.method public constructor <init>(LX/1B5;LX/0Px;)V
    .locals 0

    .prologue
    .line 1709094
    iput-object p1, p0, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;->b:LX/1B5;

    iput-object p2, p0, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 1709077
    iget-object v0, p0, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;->b:LX/1B5;

    iget-object v1, p0, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;->a:LX/0Px;

    .line 1709078
    :try_start_0
    iget-object v2, v0, LX/1B5;->g:LX/0lB;

    invoke-virtual {v2, v1}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1709079
    :goto_0
    move-object v0, v2

    .line 1709080
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v2, "viewport_waterfall"

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "par"

    invoke-virtual {v1, v2, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "native_newsfeed"

    .line 1709081
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1709082
    move-object v1, v1

    .line 1709083
    move-object v0, v1

    .line 1709084
    iget-object v1, p0, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;->b:LX/1B5;

    iget-object v1, v1, LX/1B5;->y:LX/0Uh;

    const/16 v2, 0x46c

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1709085
    iget-object v1, p0, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;->b:LX/1B5;

    iget-object v1, v1, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->d(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1709086
    :goto_1
    return-void

    .line 1709087
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/logging/FeedUnitImpressionLoggerController$1;->b:LX/1B5;

    iget-object v1, v1, LX/1B5;->d:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1

    .line 1709088
    :catch_0
    move-exception v2

    .line 1709089
    iget-object v3, v0, LX/1B5;->h:LX/03V;

    const-string v4, "JSON exception in VpvWaterfall aggregation"

    const-string v5, ""

    invoke-static {v4, v5}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v4

    .line 1709090
    iput-object v2, v4, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1709091
    move-object v2, v4

    .line 1709092
    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/03V;->a(LX/0VG;)V

    .line 1709093
    const/4 v2, 0x0

    goto :goto_0
.end method
