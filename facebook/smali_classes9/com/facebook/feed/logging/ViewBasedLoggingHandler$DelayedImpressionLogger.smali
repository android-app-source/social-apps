.class public final Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/1B2;

.field private b:Lcom/facebook/graphql/model/Sponsorable;

.field private c:Lcom/facebook/graphql/model/BaseImpression;


# direct methods
.method public constructor <init>(LX/1B2;Lcom/facebook/graphql/model/Sponsorable;Lcom/facebook/graphql/model/BaseImpression;)V
    .locals 0

    .prologue
    .line 1709161
    iput-object p1, p0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;->a:LX/1B2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1709162
    iput-object p2, p0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;->b:Lcom/facebook/graphql/model/Sponsorable;

    .line 1709163
    iput-object p3, p0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;->c:Lcom/facebook/graphql/model/BaseImpression;

    .line 1709164
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1709165
    iget-object v0, p0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;->a:LX/1B2;

    iget-object v1, p0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;->b:Lcom/facebook/graphql/model/Sponsorable;

    iget-object v2, p0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;->c:Lcom/facebook/graphql/model/BaseImpression;

    invoke-static {v0, v1, v2}, LX/1B2;->a$redex0(LX/1B2;Lcom/facebook/graphql/model/Sponsorable;Lcom/facebook/graphql/model/BaseImpression;)V

    .line 1709166
    iget-object v0, p0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;->a:LX/1B2;

    iget-object v0, v0, LX/1B2;->q:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/feed/logging/ViewBasedLoggingHandler$DelayedImpressionLogger;->b:Lcom/facebook/graphql/model/Sponsorable;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1709167
    return-void
.end method
