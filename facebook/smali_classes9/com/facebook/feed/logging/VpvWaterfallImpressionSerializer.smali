.class public Lcom/facebook/feed/logging/VpvWaterfallImpressionSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/logging/VpvWaterfallImpression;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1709186
    const-class v0, Lcom/facebook/feed/logging/VpvWaterfallImpression;

    new-instance v1, Lcom/facebook/feed/logging/VpvWaterfallImpressionSerializer;

    invoke-direct {v1}, Lcom/facebook/feed/logging/VpvWaterfallImpressionSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1709187
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1709177
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/logging/VpvWaterfallImpression;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1709188
    if-nez p0, :cond_0

    .line 1709189
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1709190
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1709191
    invoke-static {p0, p1, p2}, Lcom/facebook/feed/logging/VpvWaterfallImpressionSerializer;->b(Lcom/facebook/feed/logging/VpvWaterfallImpression;LX/0nX;LX/0my;)V

    .line 1709192
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1709193
    return-void
.end method

.method private static b(Lcom/facebook/feed/logging/VpvWaterfallImpression;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1709179
    const-string v0, "stage"

    .line 1709180
    iget v1, p0, Lcom/facebook/feed/logging/VpvWaterfallImpression;->stage:I

    move v1, v1

    .line 1709181
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1709182
    const-string v0, "tracking"

    .line 1709183
    iget-object v1, p0, Lcom/facebook/feed/logging/VpvWaterfallImpression;->trackingCodes:LX/162;

    move-object v1, v1

    .line 1709184
    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;LX/0gT;)V

    .line 1709185
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1709178
    check-cast p1, Lcom/facebook/feed/logging/VpvWaterfallImpression;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/logging/VpvWaterfallImpressionSerializer;->a(Lcom/facebook/feed/logging/VpvWaterfallImpression;LX/0nX;LX/0my;)V

    return-void
.end method
