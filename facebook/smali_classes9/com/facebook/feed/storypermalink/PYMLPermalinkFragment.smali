.class public Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;
.super Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;
.source ""


# instance fields
.field private Q:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

.field public a:LX/BFC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0pf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/BFR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1829139
    invoke-direct {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;

    new-instance v3, LX/BFC;

    invoke-static {p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v1

    check-cast v1, LX/0lC;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-direct {v3, v1, v2}, LX/BFC;-><init>(LX/0lC;LX/1Ck;)V

    move-object v1, v3

    check-cast v1, LX/BFC;

    invoke-static {p0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v2

    check-cast v2, LX/0pf;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-static {p0}, LX/BFR;->b(LX/0QB;)LX/BFR;

    move-result-object p0

    check-cast p0, LX/BFR;

    iput-object v1, p1, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->a:LX/BFC;

    iput-object v2, p1, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->b:LX/0pf;

    iput-object v3, p1, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->c:LX/0Uh;

    iput-object p0, p1, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->d:LX/BFR;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1829179
    const-string v0, "ad_preview_permalink"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1829176
    const-class v0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;

    invoke-static {v0, p0}, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1829177
    invoke-super {p0, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Landroid/os/Bundle;)V

    .line 1829178
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 3

    .prologue
    .line 1829171
    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    iput-object v0, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->Q:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 1829172
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->Q:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->b(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1829173
    invoke-virtual {p0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829174
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->b:LX/0pf;

    invoke-static {v0, p1, v2}, LX/BFR;->a(LX/1ZF;Lcom/facebook/graphql/model/FeedUnit;LX/0pf;)V

    .line 1829175
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1829170
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {p0, p1}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    return-void
.end method

.method public final b()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1829180
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1829181
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->Q:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->Q:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1829182
    const-string v1, "story_id"

    iget-object v2, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->Q:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1829183
    :cond_0
    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1829168
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->Q:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1829169
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 1829158
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->m()V

    .line 1829159
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->a:LX/BFC;

    .line 1829160
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->ah:Lcom/facebook/permalink/PermalinkParams;

    move-object v1, v1

    .line 1829161
    new-instance v2, LX/BtR;

    invoke-direct {v2, p0}, LX/BtR;-><init>(Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;)V

    .line 1829162
    iget-object v3, v1, Lcom/facebook/permalink/PermalinkParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 1829163
    :try_start_0
    iget-object v4, v0, LX/BFC;->a:LX/0lC;

    const-class p0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v4, v3, p0}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 1829164
    invoke-interface {v2, v3}, LX/0TF;->onSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1829165
    :goto_0
    return-void

    .line 1829166
    :catch_0
    move-exception v3

    .line 1829167
    invoke-interface {v2, v3}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final getDebugInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1829157
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->Q:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->c(Lcom/facebook/graphql/model/FeedUnit;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x5575a6ad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1829153
    invoke-super {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->onDestroy()V

    .line 1829154
    iget-object v1, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->a:LX/BFC;

    .line 1829155
    iget-object v2, v1, LX/BFC;->b:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 1829156
    const/16 v1, 0x2b

    const v2, -0x16080654

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1829140
    invoke-super {p0, p1, p2}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1829141
    iget-object v0, p0, Lcom/facebook/feed/storypermalink/PYMLPermalinkFragment;->c:LX/0Uh;

    const/16 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1829142
    :goto_0
    return-void

    .line 1829143
    :cond_0
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1829144
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0917

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1829145
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->k()LX/0g8;

    move-result-object v2

    .line 1829146
    invoke-static {v0, v1}, LX/BtP;->a(Landroid/content/Context;I)LX/BtP;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0g8;->d(Landroid/view/View;)V

    .line 1829147
    new-instance v3, LX/BtP;

    invoke-direct {v3, v0}, LX/BtP;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v3}, LX/0g8;->d(Landroid/view/View;)V

    .line 1829148
    invoke-static {v0, v1}, LX/BtP;->a(Landroid/content/Context;I)LX/BtP;

    move-result-object v1

    invoke-interface {v2, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 1829149
    new-instance v1, LX/BtP;

    invoke-direct {v1, v0}, LX/BtP;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 1829150
    invoke-interface {v2}, LX/0g8;->v()V

    .line 1829151
    const v0, 0x7f0d009d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    .line 1829152
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
