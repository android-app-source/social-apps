.class public Lcom/facebook/feed/storypermalink/AdPreviewPermalinkFragment;
.super Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1829123
    invoke-direct {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1829110
    const-string v0, "ad_preview_permalink"

    return-object v0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1829111
    invoke-super {p0, p1, p2}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1829112
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1829113
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0917

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1829114
    invoke-virtual {p0}, Lcom/facebook/feed/storypermalink/StoryPermalinkFragment;->k()LX/0g8;

    move-result-object v2

    .line 1829115
    invoke-static {v0, v1}, LX/BtP;->a(Landroid/content/Context;I)LX/BtP;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0g8;->d(Landroid/view/View;)V

    .line 1829116
    new-instance v3, LX/BtP;

    invoke-direct {v3, v0}, LX/BtP;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v3}, LX/0g8;->d(Landroid/view/View;)V

    .line 1829117
    invoke-static {v0, v1}, LX/BtP;->a(Landroid/content/Context;I)LX/BtP;

    move-result-object v1

    invoke-interface {v2, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 1829118
    new-instance v1, LX/BtP;

    invoke-direct {v1, v0}, LX/BtP;-><init>(Landroid/content/Context;)V

    invoke-interface {v2, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 1829119
    invoke-interface {v2}, LX/0g8;->v()V

    .line 1829120
    const v0, 0x7f0d009d

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    .line 1829121
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1829122
    return-void
.end method
