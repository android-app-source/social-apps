.class public Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/88f;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigSerializer;
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final mComposerHint:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "composer_hint"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1708232
    const-class v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1708218
    const-class v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1708231
    const-class v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 1708228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708229
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    .line 1708230
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1708224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1708225
    iput-object p1, p0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    .line 1708226
    invoke-virtual {p0}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a()V

    .line 1708227
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;
    .locals 1

    .prologue
    .line 1708223
    new-instance v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    invoke-direct {v0, p0}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1708221
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1708222
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1708220
    sget-object v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1708219
    iget-object v0, p0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    return-object v0
.end method
