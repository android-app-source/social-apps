.class public Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1708263
    const-class v0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    new-instance v1, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigSerializer;

    invoke-direct {v1}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1708264
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1708265
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1708257
    if-nez p0, :cond_0

    .line 1708258
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1708259
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1708260
    invoke-static {p0, p1, p2}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigSerializer;->b(Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;LX/0nX;LX/0my;)V

    .line 1708261
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1708262
    return-void
.end method

.method private static b(Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;LX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 1708255
    const-string v0, "composer_hint"

    iget-object v1, p0, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;->mComposerHint:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1708256
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1708254
    check-cast p1, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfigSerializer;->a(Lcom/facebook/feed/goodfriends/composer/GoodFriendsComposerPluginConfig;LX/0nX;LX/0my;)V

    return-void
.end method
