.class public Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Qa;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/1Ri;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field private final a:LX/1E1;

.field private final b:LX/1E0;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/v3/ProductionPromptSmallPartDefintion",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1E1;LX/1E0;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1E1;",
            "LX/1E0;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/v3/ProductionPromptSmallPartDefintion;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/v3/ClipboardPromptSmallPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptTombstonePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/souvenirs/prompt/v3/SouvenirPromptSmallPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/photoreminder/v3/MediaReminderPromptSelector;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/photos/creativeediting/swipeable/prompt/v3/FramePromptSmallPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/suggestedcoverphotos/prompt/v3/SuggestedCoverPhotosPromptLargePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptLargePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/friendsharing/meme/prompt/v3/MemePromptSmallPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/productionprompts/v3/LowConfidencePromptComponentPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/storyline/fb4a/prompt/StorylineSmallPromptComponentSpecPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708888
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1708889
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->a:LX/1E1;

    .line 1708890
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->b:LX/1E0;

    .line 1708891
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->c:LX/0Ot;

    .line 1708892
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->d:LX/0Ot;

    .line 1708893
    iput-object p7, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->e:LX/0Ot;

    .line 1708894
    iput-object p8, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->f:LX/0Ot;

    .line 1708895
    iput-object p5, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->g:LX/0Ot;

    .line 1708896
    iput-object p6, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->h:LX/0Ot;

    .line 1708897
    iput-object p9, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->i:LX/0Ot;

    .line 1708898
    iput-object p10, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->j:LX/0Ot;

    .line 1708899
    iput-object p11, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->k:LX/0Ot;

    .line 1708900
    iput-object p12, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->l:LX/0Ot;

    .line 1708901
    iput-object p13, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->m:LX/0Ot;

    .line 1708902
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;
    .locals 3

    .prologue
    .line 1708880
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;

    monitor-enter v1

    .line 1708881
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1708882
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1708883
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708884
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1708885
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1708886
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1708887
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;
    .locals 14

    .prologue
    .line 1708878
    new-instance v0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;

    invoke-static {p0}, LX/1E1;->b(LX/0QB;)LX/1E1;

    move-result-object v1

    check-cast v1, LX/1E1;

    invoke-static {p0}, LX/1E0;->b(LX/0QB;)LX/1E0;

    move-result-object v2

    check-cast v2, LX/1E0;

    const/16 v3, 0xfe3

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0xfe2

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x667

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xa99

    invoke-static {p0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1cb6

    invoke-static {p0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0xf26

    invoke-static {p0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xa9b

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xa93

    invoke-static {p0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0xa94

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3003

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x35ab

    invoke-static {p0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;-><init>(LX/1E1;LX/1E0;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 1708879
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1708875
    check-cast p2, LX/1Ri;

    .line 1708876
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->g:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->e:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->m:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->d:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->f:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->h:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->i:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->j:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->k:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->l:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->c:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 1708877
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1708874
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->b:LX/1E0;

    invoke-virtual {v0}, LX/1E0;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/v3/PromptGroupPartDefinition;->a:LX/1E1;

    invoke-virtual {v0}, LX/1E1;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
