.class public Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;
.super Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final j:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private k:Landroid/widget/ImageView;

.field private l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1708597
    const-class v0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->j:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1708598
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1708599
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1708600
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1708601
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1708602
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1708603
    const v0, 0x7f030641

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 1708604
    const v0, 0x7f0d112b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->k:Landroid/widget/ImageView;

    .line 1708605
    const v0, 0x7f0d111c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1708606
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020aaa

    invoke-static {v0, v1}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1708607
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b11ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1708608
    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setThumbnailPadding(I)V

    .line 1708609
    invoke-virtual {p0, v0, v0, v0, v0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->setPadding(IIII)V

    .line 1708610
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b08fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b08fa

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->d(II)V

    .line 1708611
    const v0, 0x7f0e077d

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 1708612
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 1708613
    return-void
.end method


# virtual methods
.method public setIcon(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1708614
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1708615
    return-void
.end method

.method public setIconClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1708616
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->k:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1708617
    return-void
.end method

.method public setProfilePhotoUri(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1708618
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/feed/inlinecomposer/multirow/ScrollAwayComposerView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1708619
    return-void
.end method
