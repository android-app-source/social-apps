.class public Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/1aQ;",
        "E::",
        "LX/1Pr;",
        ":",
        "LX/1Qa;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Ak0;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Ak7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/24B;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;LX/Ak7;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;LX/24B;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708636
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1708637
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    .line 1708638
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->b:LX/Ak7;

    .line 1708639
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1708640
    iput-object p4, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1708641
    iput-object p5, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->e:Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    .line 1708642
    iput-object p6, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    .line 1708643
    iput-object p7, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->g:LX/24B;

    .line 1708644
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;
    .locals 11

    .prologue
    .line 1708645
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    monitor-enter v1

    .line 1708646
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1708647
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1708648
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708649
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1708650
    new-instance v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    invoke-static {v0}, LX/Ak7;->a(LX/0QB;)LX/Ak7;

    move-result-object v5

    check-cast v5, LX/Ak7;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v10

    check-cast v10, LX/24B;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;LX/Ak7;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;LX/24B;)V

    .line 1708651
    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    invoke-static {v0}, LX/Ak7;->a(LX/0QB;)LX/Ak7;

    move-result-object v5

    check-cast v5, LX/Ak7;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    invoke-static {v0}, LX/24B;->b(LX/0QB;)LX/24B;

    move-result-object v10

    check-cast v10, LX/24B;

    .line 1708652
    iput-object v4, v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    iput-object v5, v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->b:LX/Ak7;

    iput-object v6, v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    iput-object v7, v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iput-object v8, v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->e:Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    iput-object v9, v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    iput-object v10, v3, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->g:LX/24B;

    .line 1708653
    move-object v0, v3

    .line 1708654
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1708655
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1708656
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1708657
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1708658
    check-cast p2, LX/Ak0;

    check-cast p3, LX/1Pr;

    .line 1708659
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->c:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->h:LX/1Ua;

    invoke-direct {v1, v2}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1708660
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->b:LX/Ak7;

    iget-object v1, p2, LX/Ak0;->a:LX/1Ri;

    invoke-virtual {v0, v1}, LX/Ak7;->a(LX/1Ri;)Landroid/view/View$OnClickListener;

    .line 1708661
    const v0, 0x7f0d0bde

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->b:LX/Ak7;

    iget-object v3, p2, LX/Ak0;->a:LX/1Ri;

    invoke-virtual {v2, v3}, LX/Ak7;->a(LX/1Ri;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-interface {p1, v0, v1, v2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1708662
    iget-boolean v0, p2, LX/Ak0;->b:Z

    if-eqz v0, :cond_0

    .line 1708663
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->e:Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    iget-object v1, p2, LX/Ak0;->a:LX/1Ri;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1708664
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->a:Lcom/facebook/feed/inlinecomposer/multirow/common/InlineComposerPromptFlyoutTitleBarPartDefinition;

    new-instance v1, LX/24R;

    iget-object v2, p2, LX/Ak0;->a:LX/1Ri;

    iget-object v2, v2, LX/1Ri;->c:LX/AkL;

    invoke-direct {v1, v2}, LX/24R;-><init>(LX/AkL;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1708665
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/BaseV3PromptPartDefinition;->f:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptImpressionLoggerPartDefinition;

    new-instance v1, LX/24N;

    iget-object v2, p2, LX/Ak0;->a:LX/1Ri;

    iget-object v2, v2, LX/1Ri;->c:LX/AkL;

    iget-object v3, p2, LX/Ak0;->a:LX/1Ri;

    iget-object v3, v3, LX/1Ri;->a:LX/1RN;

    iget-object v4, p2, LX/Ak0;->a:LX/1Ri;

    iget-object v4, v4, LX/1Ri;->b:LX/0jW;

    invoke-direct {v1, v2, v3, p3, v4}, LX/24N;-><init>(LX/AkL;LX/1RN;LX/1Pr;LX/0jW;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1708666
    const/4 v0, 0x0

    return-object v0
.end method
