.class public Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Ak3;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field public final b:LX/BMP;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/BMP;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708696
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1708697
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1708698
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;->b:LX/BMP;

    .line 1708699
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;
    .locals 5

    .prologue
    .line 1708700
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    monitor-enter v1

    .line 1708701
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1708702
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1708703
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708704
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1708705
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/BMP;->b(LX/0QB;)LX/BMP;

    move-result-object v4

    check-cast v4, LX/BMP;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/BMP;)V

    .line 1708706
    move-object v0, p0

    .line 1708707
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1708708
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1708709
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1708710
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1708711
    check-cast p2, LX/Ak3;

    .line 1708712
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v1, LX/Ak2;

    invoke-direct {v1, p0, p2}, LX/Ak2;-><init>(Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;LX/Ak3;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1708713
    const/4 v0, 0x0

    return-object v0
.end method
