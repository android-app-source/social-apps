.class public Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1Ri;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/B5l;

.field private final b:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;


# direct methods
.method public constructor <init>(LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708681
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1708682
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;->a:LX/B5l;

    .line 1708683
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    .line 1708684
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;
    .locals 5

    .prologue
    .line 1708670
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    monitor-enter v1

    .line 1708671
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1708672
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1708673
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708674
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1708675
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;

    invoke-static {v0}, LX/B5l;->b(LX/0QB;)LX/B5l;

    move-result-object v3

    check-cast v3, LX/B5l;

    invoke-static {v0}, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;-><init>(LX/B5l;Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;)V

    .line 1708676
    move-object v0, p0

    .line 1708677
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1708678
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1708679
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1708680
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1708667
    check-cast p2, LX/1Ri;

    .line 1708668
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;->b:Lcom/facebook/feed/inlinecomposer/multirow/common/PromptActionHandlePartDefinition;

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/common/DefaultPromptActionHandlePartDefinition;->a:LX/B5l;

    iget-object v2, p2, LX/1Ri;->a:LX/1RN;

    invoke-virtual {v1, v2}, LX/B5l;->a(LX/1RN;)LX/B5n;

    move-result-object v1

    invoke-virtual {v1}, LX/B5n;->a()LX/B5p;

    move-result-object v1

    invoke-static {v1, p2}, LX/Ak3;->a(LX/B5p;LX/1Ri;)LX/Ak3;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1708669
    const/4 v0, 0x0

    return-object v0
.end method
