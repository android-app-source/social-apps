.class public Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/1bR;",
        "Landroid/graphics/drawable/Drawable;",
        "LX/1PW;",
        "Lcom/facebook/widget/FbImageView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Landroid/content/res/Resources;

.field public final b:LX/0wM;

.field private final c:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0wM;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1708536
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1708537
    iput-object p1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->a:Landroid/content/res/Resources;

    .line 1708538
    iput-object p2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->b:LX/0wM;

    .line 1708539
    iput-object p3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->c:LX/03V;

    .line 1708540
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;
    .locals 6

    .prologue
    .line 1708507
    const-class v1, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;

    monitor-enter v1

    .line 1708508
    :try_start_0
    sget-object v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1708509
    sput-object v2, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1708510
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1708511
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1708512
    new-instance p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v4

    check-cast v4, LX/0wM;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;-><init>(Landroid/content/res/Resources;LX/0wM;LX/03V;)V

    .line 1708513
    move-object v0, p0

    .line 1708514
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1708515
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1708516
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1708517
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1708527
    check-cast p2, LX/1bR;

    .line 1708528
    if-eqz p2, :cond_0

    sget-object v0, LX/1bR;->NONE:LX/1bR;

    if-ne p2, v0, :cond_1

    .line 1708529
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->c:LX/03V;

    const-string v1, "inline_composer_glyphs_incorrect_type"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Called with incorrect glyph type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708530
    const/4 v0, 0x0

    .line 1708531
    :goto_0
    return-object v0

    .line 1708532
    :cond_1
    new-instance v0, LX/ASa;

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->a:Landroid/content/res/Resources;

    const v2, 0x7f0202f9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->a:Landroid/content/res/Resources;

    const v3, 0x7f0202f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->a:Landroid/content/res/Resources;

    const p1, 0x7f0202fa

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/ASa;-><init>(LX/0Px;I)V

    .line 1708533
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->b:LX/0wM;

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/multirow/InlineComposerGlyphsPartDefinition;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a00e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/0wM;->a(I)Landroid/graphics/ColorFilter;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ASa;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1708534
    move-object v0, v0

    .line 1708535
    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x39d12a66

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1708521
    check-cast p2, Landroid/graphics/drawable/Drawable;

    check-cast p4, Lcom/facebook/widget/FbImageView;

    .line 1708522
    if-nez p2, :cond_0

    .line 1708523
    const/16 v1, 0x8

    invoke-virtual {p4, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1708524
    :goto_0
    const/16 v1, 0x1f

    const v2, 0x59897132

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1708525
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1708526
    invoke-virtual {p4, p2}, Lcom/facebook/widget/FbImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1708518
    check-cast p4, Lcom/facebook/widget/FbImageView;

    .line 1708519
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Lcom/facebook/widget/FbImageView;->setVisibility(I)V

    .line 1708520
    return-void
.end method
