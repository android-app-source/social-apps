.class public Lcom/facebook/feed/inlinecomposer/InlineComposerView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public final c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public final d:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

.field public final e:[Lcom/facebook/fbui/widget/text/ImageWithTextView;

.field public final f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final g:Landroid/widget/TextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1708489
    const-class v0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;

    const-string v1, "inline_composer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 1708490
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1708491
    const v0, 0x7f030632

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1708492
    invoke-virtual {p0, v3}, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->setOrientation(I)V

    .line 1708493
    const v0, 0x7f0d1123

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 1708494
    const v0, 0x7f0d1124

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 1708495
    const v0, 0x7f0d1125

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->d:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    .line 1708496
    const v0, 0x7f0d111c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1708497
    const v0, 0x7f0d111e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->g:Landroid/widget/TextView;

    .line 1708498
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/fbui/widget/text/ImageWithTextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->b:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->c:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->d:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    aput-object v1, v0, v4

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->e:[Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 1708499
    iget-object v1, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->e:[Lcom/facebook/fbui/widget/text/ImageWithTextView;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1708500
    const p1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v3, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageScale(F)V

    .line 1708501
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1708502
    :cond_0
    invoke-virtual {p0, v4}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setShowSegmentedDividers(I)V

    .line 1708503
    invoke-virtual {p0}, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 1708504
    return-void
.end method


# virtual methods
.method public setCheckInButtonVisibility(I)V
    .locals 1

    .prologue
    .line 1708505
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/InlineComposerView;->d:Lcom/facebook/fbui/widget/text/GlyphWithTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/GlyphWithTextView;->setVisibility(I)V

    .line 1708506
    return-void
.end method
