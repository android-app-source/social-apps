.class public Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public c:Landroid/view/View;

.field public d:Lcom/facebook/widget/text/BetterTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1708985
    const-class v0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1708986
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1708987
    const v0, 0x7f030912

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1708988
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->setOrientation(I)V

    .line 1708989
    const v0, 0x7f0d1741

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1708990
    const v0, 0x7f0d1742

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->c:Landroid/view/View;

    .line 1708991
    const v0, 0x7f0d1743

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->d:Lcom/facebook/widget/text/BetterTextView;

    .line 1708992
    return-void
.end method


# virtual methods
.method public setImage(Landroid/net/Uri;)V
    .locals 2

    .prologue
    .line 1708993
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1708994
    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1708995
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1708996
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    .prologue
    .line 1708997
    iget-object v0, p0, Lcom/facebook/feed/inlinecomposer/work/WorkGroupPogView;->d:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setTextColor(I)V

    .line 1708998
    return-void
.end method
