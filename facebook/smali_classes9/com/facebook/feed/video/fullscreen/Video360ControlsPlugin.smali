.class public Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;
.super LX/7N3;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field public o:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final p:LX/3Ie;

.field private q:LX/3IC;

.field private r:LX/Bvq;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1832961
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832962
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832959
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832960
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1832951
    invoke-direct {p0, p1, p2, p3}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832952
    const-class v0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1832953
    new-instance v0, LX/3Ie;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/3Ie;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->p:LX/3Ie;

    .line 1832954
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->p:LX/3Ie;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->addView(Landroid/view/View;I)V

    .line 1832955
    new-instance v0, LX/Bvq;

    invoke-direct {v0, p0}, LX/Bvq;-><init>(Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->r:LX/Bvq;

    .line 1832956
    new-instance v0, LX/3IC;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->r:LX/Bvq;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->r:LX/Bvq;

    invoke-direct {v0, p1, v1, v2}, LX/3IC;-><init>(Landroid/content/Context;LX/3IA;LX/3IB;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->q:LX/3IC;

    .line 1832957
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-static {p0}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object p0

    check-cast p0, LX/19m;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->o:LX/19m;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1832963
    invoke-super {p0, p1, p2}, LX/7N3;->a(LX/2pa;Z)V

    .line 1832964
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->p:LX/3Ie;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    .line 1832965
    iget-object p0, v0, LX/2oy;->h:Ljava/util/List;

    iget-object p2, v0, LX/3Ie;->o:LX/3If;

    invoke-interface {p0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1832966
    iget-object p0, v0, LX/2oy;->h:Ljava/util/List;

    iget-object p2, v0, LX/3Ie;->e:LX/3Ee;

    invoke-interface {p0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1832967
    iget-object p0, v0, LX/2oy;->i:LX/2oj;

    if-eqz p0, :cond_0

    .line 1832968
    iget-object p0, v0, LX/2oy;->i:LX/2oj;

    iget-object p2, v0, LX/3Ie;->o:LX/3If;

    invoke-virtual {p0, p2}, LX/0b4;->b(LX/0b2;)Z

    .line 1832969
    iget-object p0, v0, LX/2oy;->i:LX/2oj;

    iget-object p2, v0, LX/3Ie;->e:LX/3Ee;

    invoke-virtual {p0, p2}, LX/0b4;->b(LX/0b2;)Z

    .line 1832970
    :cond_0
    iget-object p0, v0, LX/3Ie;->d:Lcom/facebook/resources/ui/FbButton;

    const/4 p2, 0x0

    invoke-virtual {p0, p2}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 1832971
    invoke-static {v0, p1, v1}, LX/3Ie;->b(LX/3Ie;LX/2pa;LX/04G;)Z

    .line 1832972
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1832958
    const v0, 0x7f03062b

    return v0
.end method

.method public final onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1832950
    iget-boolean v0, p0, LX/7N3;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x1c82cbcd

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1832949
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;->q:LX/3IC;

    invoke-virtual {v1, p1}, LX/3IC;->a(Landroid/view/MotionEvent;)Z

    move-result v1

    const v2, -0x62859a72

    invoke-static {v3, v3, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return v1
.end method
