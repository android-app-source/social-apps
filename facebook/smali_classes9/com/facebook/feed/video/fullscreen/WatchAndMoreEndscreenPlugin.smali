.class public Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;
.super Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field public f:LX/2nF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1833465
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1833466
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1833467
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833468
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1833469
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833470
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;

    invoke-static {v0}, LX/2nF;->a(LX/0QB;)LX/2nF;

    move-result-object v0

    check-cast v0, LX/2nF;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;->f:LX/2nF;

    .line 1833471
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 4
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1833472
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;->f:LX/2nF;

    .line 1833473
    iget-object v1, v0, LX/2nF;->a:LX/0ad;

    sget-short v2, LX/D7l;->e:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1833474
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;->f:LX/2nF;

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2nF;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1833475
    iget-object v0, p0, LX/3Ga;->a:LX/2pa;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->b(LX/2pa;)Z

    move-result v0

    return v0
.end method
