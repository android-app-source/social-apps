.class public Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field private a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1830914
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1830915
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1830916
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830917
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1830918
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830919
    const v0, 0x7f03073a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1830920
    const v0, 0x7f0d134d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;

    .line 1830921
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1830922
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->d()V

    .line 1830923
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->b()V

    .line 1830924
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1830925
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;->a:Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->c()V

    .line 1830926
    return-void
.end method
