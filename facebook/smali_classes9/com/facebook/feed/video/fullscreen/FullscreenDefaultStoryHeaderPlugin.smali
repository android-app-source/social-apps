.class public Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;
.super LX/3Gb;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pn;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1VD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/components/ComponentView;

.field private c:LX/1Qj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1831165
    invoke-direct {p0, p1}, LX/3Gb;-><init>(Landroid/content/Context;)V

    .line 1831166
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->g()V

    .line 1831167
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1831171
    invoke-direct {p0, p1, p2}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831172
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->g()V

    .line 1831173
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1831168
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831169
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->g()V

    .line 1831170
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;

    const/16 v1, 0x71f

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->a:LX/0Ot;

    return-void
.end method

.method private g()V
    .locals 1

    .prologue
    .line 1831174
    const-class v0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1831175
    const v0, 0x7f030736

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1831176
    const v0, 0x7f0d1348

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->b:Lcom/facebook/components/ComponentView;

    .line 1831177
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1831162
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->b:Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_0

    .line 1831163
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->b:Lcom/facebook/components/ComponentView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setVisibility(I)V

    .line 1831164
    :cond_0
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1831159
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->b:Lcom/facebook/components/ComponentView;

    if-eqz v0, :cond_0

    .line 1831160
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->b:Lcom/facebook/components/ComponentView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/components/ComponentView;->setVisibility(I)V

    .line 1831161
    :cond_0
    return-void
.end method

.method private j()V
    .locals 4

    .prologue
    .line 1831141
    new-instance v0, LX/BuW;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, LX/1PU;->b:LX/1PY;

    invoke-direct {v0, p0, v1, v2, v3}, LX/BuW;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->c:LX/1Qj;

    .line 1831142
    return-void
.end method

.method private setupHeader(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    .line 1831151
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->i()V

    .line 1831152
    new-instance v1, LX/1De;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 1831153
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1VD;

    invoke-virtual {v0, v1}, LX/1VD;->c(LX/1De;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1X4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1X4;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->c:LX/1Qj;

    invoke-virtual {v0, v2}, LX/1X4;->a(LX/1Pb;)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1X4;->g(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1X4;->d(Z)LX/1X4;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/1X4;->e(Z)LX/1X4;

    move-result-object v0

    .line 1831154
    invoke-static {p1}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1831155
    const v2, 0x7f0e0a02

    invoke-virtual {v0, v2}, LX/1X4;->n(I)LX/1X4;

    .line 1831156
    :cond_0
    invoke-static {v1, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 1831157
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->b:Lcom/facebook/components/ComponentView;

    invoke-virtual {v1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1831158
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1831143
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831144
    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 1831145
    :goto_0
    if-eqz v0, :cond_1

    if-nez v1, :cond_1

    .line 1831146
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->j()V

    .line 1831147
    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->setupHeader(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1831148
    :goto_1
    return-void

    .line 1831149
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1831150
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDefaultStoryHeaderPlugin;->h()V

    goto :goto_1
.end method
