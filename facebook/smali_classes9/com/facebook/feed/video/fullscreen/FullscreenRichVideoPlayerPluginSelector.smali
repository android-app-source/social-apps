.class public Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final n:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1831416
    const-class v0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/19m;LX/0Uh;LX/Ac6;LX/1b4;Ljava/lang/Boolean;Z)V
    .locals 14
    .param p6    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1831382
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 1831383
    new-instance v2, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1831384
    new-instance v3, Lcom/facebook/video/player/plugins/SubtitlePlugin;

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;)V

    .line 1831385
    new-instance v4, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-static {}, LX/3Hk;->a()I

    move-result v5

    invoke-direct {v1, p1, v5}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v4, v1}, Lcom/facebook/facecastdisplay/LiveEventsPlugin;-><init>(Landroid/content/Context;)V

    .line 1831386
    new-instance v5, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v5, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1831387
    new-instance v6, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;

    invoke-direct {v6, p1}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;-><init>(Landroid/content/Context;)V

    .line 1831388
    new-instance v7, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;-><init>(Landroid/content/Context;)V

    .line 1831389
    new-instance v8, LX/Bvl;

    invoke-direct {v8, p1}, LX/Bvl;-><init>(Landroid/content/Context;)V

    .line 1831390
    new-instance v9, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;

    invoke-direct {v9, p1}, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;-><init>(Landroid/content/Context;)V

    .line 1831391
    invoke-virtual/range {p5 .. p5}, LX/1b4;->ab()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, LX/BvW;

    invoke-direct {v1, p1}, LX/BvW;-><init>(Landroid/content/Context;)V

    .line 1831392
    :goto_0
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->n:LX/0Uh;

    .line 1831393
    invoke-virtual/range {p6 .. p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    iput-boolean v10, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->k:Z

    .line 1831394
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    new-instance v11, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v12, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v11, p1, v12}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v10

    iput-object v10, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->b:LX/0Px;

    .line 1831395
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    iget-object v11, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v10, v11}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    new-instance v11, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-direct {v11, p1}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    new-instance v11, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;

    invoke-direct {v11, p1}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v10

    iput-object v10, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->c:LX/0Px;

    .line 1831396
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    iget-object v11, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v10, v11}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    .line 1831397
    new-instance v11, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;

    invoke-direct {v11, p1}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    invoke-virtual {v11, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-direct {v12, p1}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    sget-object v13, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v12, p1, v13}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    invoke-direct {v12, p1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, LX/Buh;

    invoke-direct {v12, p1}, LX/Buh;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    invoke-virtual {v11, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, LX/BvH;

    invoke-direct {v12, p1}, LX/BvH;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;

    invoke-direct {v12, p1}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    invoke-virtual {v11, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, LX/Bv4;

    invoke-direct {v12, p1}, LX/Bv4;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-direct {v12, p1}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, LX/BvE;

    invoke-direct {v12, p1}, LX/BvE;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    invoke-direct {v12, p1}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v11

    new-instance v12, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    invoke-direct {v12, p1}, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v11, v12}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1831398
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v10

    iput-object v10, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->e:LX/0Px;

    .line 1831399
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    iget-object v11, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v10, v11}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v10

    new-instance v11, LX/7N2;

    invoke-direct {v11, p1}, LX/7N2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    new-instance v11, LX/7Ng;

    invoke-direct {v11, p1}, LX/7Ng;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    new-instance v11, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-direct {v11, p1}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    new-instance v11, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-direct {v11, p1}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v10

    iput-object v10, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->d:LX/0Px;

    .line 1831400
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    iget-object v11, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v10, v11}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v10

    new-instance v11, LX/7Ng;

    invoke-direct {v11, p1}, LX/7Ng;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v11}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v10

    invoke-virtual {v10, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/BuP;

    invoke-direct {v7, p1}, LX/BuP;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/Bv4;

    invoke-direct {v7, p1}, LX/Bv4;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/AcB;

    invoke-direct {v7, p1}, LX/AcB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/7My;

    invoke-direct {v7, p1}, LX/7My;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->i:LX/0Px;

    .line 1831401
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    iget-object v7, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v6, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/7Ng;

    invoke-direct {v7, p1}, LX/7Ng;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/BuP;

    invoke-direct {v7, p1}, LX/BuP;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/video/player/plugins/Video360HeadingPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/AcB;

    invoke-direct {v7, p1}, LX/AcB;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/7My;

    invoke-direct {v7, p1}, LX/7My;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    iput-object v6, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->j:LX/0Px;

    .line 1831402
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    iget-object v7, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v6, v7}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/3Hz;

    invoke-direct {v7, p1}, LX/3Hz;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    sget-object v10, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v7, p1, v10}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    invoke-direct {v7, p1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    new-instance v7, LX/Buk;

    invoke-direct {v7, p1}, LX/Buk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v6

    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->f:LX/0Px;

    .line 1831403
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    iget-object v4, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v1, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/Bu2;

    invoke-direct {v2, p1}, LX/Bu2;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;->h:LX/0Px;

    .line 1831404
    return-void

    .line 1831405
    :cond_0
    new-instance v1, LX/BvQ;

    invoke-direct {v1, p1}, LX/BvQ;-><init>(Landroid/content/Context;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 1

    .prologue
    .line 1831406
    const-class v0, LX/BvQ;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-nez v0, :cond_0

    const-class v0, LX/BvW;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1831407
    :cond_0
    sget-object v0, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    .line 1831408
    :goto_0
    return-object v0

    .line 1831409
    :cond_1
    const-class v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1831410
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    goto :goto_0

    .line 1831411
    :cond_2
    const-class v0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1831412
    sget-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 1831413
    :cond_3
    const-class v0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1831414
    sget-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    goto :goto_0

    .line 1831415
    :cond_4
    sget-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    goto :goto_0
.end method
