.class public Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;
.super LX/4nk;
.source ""


# instance fields
.field private final c:LX/Btt;

.field public d:LX/0kb;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1829611
    invoke-direct {p0, p1, p2}, LX/4nk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1829612
    const-class v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1829613
    new-instance v0, LX/Btt;

    invoke-direct {v0, p0, p1}, LX/Btt;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->c:LX/Btt;

    .line 1829614
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;

    invoke-static {v0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v0

    check-cast v0, LX/0kb;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->d:LX/0kb;

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 1829609
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->c:LX/Btt;

    invoke-virtual {v0}, LX/14l;->a()V

    .line 1829610
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1829607
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->c:LX/Btt;

    invoke-virtual {v0}, LX/14l;->b()V

    .line 1829608
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1829602
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->d:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    .line 1829603
    if-eqz v0, :cond_0

    .line 1829604
    invoke-virtual {p0}, LX/4nk;->a()V

    .line 1829605
    :goto_0
    return-void

    .line 1829606
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenNetworkBanner;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08003f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/4nk;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method
