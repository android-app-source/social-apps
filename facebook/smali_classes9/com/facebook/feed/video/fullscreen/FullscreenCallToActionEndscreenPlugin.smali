.class public Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;
.super Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field private final f:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1831136
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831137
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1831134
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831135
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1831131
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831132
    new-instance v0, LX/BuV;

    invoke-direct {v0, p0}, LX/BuV;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;->f:Landroid/view/View$OnClickListener;

    .line 1831133
    return-void
.end method


# virtual methods
.method public a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1
    .param p1    # Lcom/facebook/graphql/model/GraphQLStoryAttachment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1831130
    if-eqz p1, :cond_0

    invoke-static {p1}, LX/2v7;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCallToActionEndscreenReplayClickListener()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1831129
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;->f:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1831127
    const/4 v0, 0x1

    return v0
.end method

.method public r()Z
    .locals 1

    .prologue
    .line 1831128
    iget-object v0, p0, LX/3Ga;->a:LX/2pa;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/video/base/CallToActionEndscreenBasePlugin;->b(LX/2pa;)Z

    move-result v0

    return v0
.end method
