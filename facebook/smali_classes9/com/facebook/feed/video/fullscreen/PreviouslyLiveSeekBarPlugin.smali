.class public Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;
.super LX/7Mz;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ">",
        "LX/7Mz",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public o:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final p:Landroid/view/ViewStub;

.field public q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Landroid/widget/SeekBar;

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1832179
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832180
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832177
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832178
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1832170
    invoke-direct {p0, p1, p2, p3}, LX/7Mz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832171
    const v0, 0x7f0d2c04

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->p:Landroid/view/ViewStub;

    .line 1832172
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->r:Landroid/widget/SeekBar;

    .line 1832173
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->s:Z

    .line 1832174
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvK;

    invoke-direct {v1, p0}, LX/BvK;-><init>(Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832175
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BvJ;

    invoke-direct {v1, p0}, LX/BvJ;-><init>(Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832176
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    invoke-static {v0}, LX/Ac6;->b(LX/0QB;)LX/Ac6;

    move-result-object v0

    check-cast v0, LX/Ac6;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->o:LX/Ac6;

    return-void
.end method

.method private b(LX/2pa;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1832181
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    if-nez v0, :cond_0

    .line 1832182
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->p:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    .line 1832183
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    invoke-virtual {v0, v2}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->setVisibility(I)V

    .line 1832184
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->setVideoId(Ljava/lang/String;)V

    .line 1832185
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    .line 1832186
    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->h()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v1}, LX/2pb;->k()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 1832187
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    invoke-virtual {v1, v0}, LX/AeE;->setCursor(F)V

    .line 1832188
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->s:Z

    if-eqz v0, :cond_1

    .line 1832189
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    invoke-virtual {v0}, LX/AeE;->b()V

    .line 1832190
    iput-boolean v2, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->s:Z

    .line 1832191
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 1832167
    invoke-super {p0, p1, p2}, LX/7Mz;->a(LX/2pa;Z)V

    .line 1832168
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->b(LX/2pa;)V

    .line 1832169
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 1832164
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    if-eqz v0, :cond_0

    .line 1832165
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    int-to-float v1, p1

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->r:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getMax()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, LX/AeE;->setCursor(F)V

    .line 1832166
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1832160
    invoke-super {p0}, LX/7Mz;->d()V

    .line 1832161
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    if-eqz v0, :cond_0

    .line 1832162
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/heatmap/HeatmapView;->a()V

    .line 1832163
    :cond_0
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1832158
    const-class v0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1832159
    const v0, 0x7f031009

    return v0
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 1832155
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    if-eqz v0, :cond_0

    .line 1832156
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/AeE;->b(Z)V

    .line 1832157
    :cond_0
    return-void
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 1832152
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    if-eqz v0, :cond_0

    .line 1832153
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/PreviouslyLiveSeekBarPlugin;->q:Lcom/facebook/facecastdisplay/heatmap/HeatmapView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/AeE;->a(Z)V

    .line 1832154
    :cond_0
    return-void
.end method
