.class public Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;
.super LX/2oy;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field public a:LX/1nA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/2yI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/AjP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/view/View;

.field public f:Landroid/widget/ImageView;

.field public n:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1831117
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831118
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1831104
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831105
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1831106
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831107
    const-class v0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1831108
    const v0, 0x7f030735

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1831109
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BuU;

    invoke-direct {v1, p0, p0}, LX/BuU;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831110
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BuT;

    invoke-direct {v1, p0, p0}, LX/BuT;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;LX/2oy;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831111
    const v0, 0x7f0d1345

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    .line 1831112
    const v0, 0x7f0d1346

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->f:Landroid/widget/ImageView;

    .line 1831113
    const v0, 0x7f0d1347

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 1831114
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    sget-object v1, LX/1vY;->GENERIC_CALL_TO_ACTION_BUTTON:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1831115
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    const v1, 0x7f0d0083

    const-string v2, "video_cta_full_screen_click"

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1831116
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;

    invoke-static {p0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v1

    check-cast v1, LX/1nA;

    invoke-static {p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v2

    check-cast v2, LX/0bH;

    const-class v3, LX/2yI;

    invoke-interface {p0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/2yI;

    invoke-static {p0}, LX/AjP;->b(LX/0QB;)LX/AjP;

    move-result-object p0

    check-cast p0, LX/AjP;

    iput-object v1, p1, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->a:LX/1nA;

    iput-object v2, p1, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->b:LX/0bH;

    iput-object v3, p1, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->c:LX/2yI;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->d:LX/AjP;

    return-void
.end method

.method public static a$redex0(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;Z)V
    .locals 2

    .prologue
    .line 1831098
    if-eqz p1, :cond_0

    .line 1831099
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->f:Landroid/widget/ImageView;

    const v1, 0x7f021a51

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1831100
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->n:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f081781

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1831101
    :goto_0
    return-void

    .line 1831102
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->f:Landroid/widget/ImageView;

    const v1, 0x7f021a50

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1831103
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->n:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7f08103f

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    goto :goto_0
.end method

.method private static b(LX/2pa;)Z
    .locals 2

    .prologue
    .line 1831097
    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "MultiShareGraphQLSubStoryPropsKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/2pa;->b:LX/0P1;

    const-string v1, "MultiShareGraphQLSubStoryIndexKey"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;)V
    .locals 2

    .prologue
    .line 1831095
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1831096
    return-void
.end method

.method public static setupCallToActionButton(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1831055
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831056
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1831057
    const v1, -0x1e53800c

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1831058
    if-eqz v1, :cond_5

    invoke-static {v0}, LX/2v7;->j(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1831059
    const v1, -0x1e53800c

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1831060
    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->k()Lcom/facebook/graphql/model/GraphQLAppStoreApplication;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    if-eq v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->y()Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;->APP_WITH_PRODUCT:Lcom/facebook/graphql/enums/GraphQLStoryActionLinkDestinationType;

    if-ne v1, v2, :cond_6

    :cond_0
    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 1831061
    if-eqz v1, :cond_5

    :cond_1
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 1831062
    if-eqz v1, :cond_2

    .line 1831063
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831064
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, -0x1e53800c

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1831065
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->a:LX/1nA;

    invoke-virtual {v1, p1, v0}, LX/1nA;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLStoryActionLink;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 1831066
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1831067
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    const v2, 0x7f021a3a

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1831068
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->f:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1831069
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setAllCaps(Z)V

    .line 1831070
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1831071
    :goto_2
    return-void

    .line 1831072
    :cond_2
    const v1, 0x46a1c4a4

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    if-eqz v1, :cond_7

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 1831073
    if-eqz v1, :cond_3

    .line 1831074
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831075
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    const v1, 0x46a1c4a4

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    .line 1831076
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->c:LX/2yI;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, LX/2yI;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;LX/1Pq;)LX/B6B;

    move-result-object v1

    .line 1831077
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    new-instance v3, LX/BuR;

    invoke-direct {v3, p0, v1}, LX/BuR;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1831078
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    const v2, 0x7f021a3a

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1831079
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->f:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1831080
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->n:Lcom/facebook/resources/ui/FbTextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setAllCaps(Z)V

    .line 1831081
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->aZ()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1831082
    goto :goto_2

    .line 1831083
    :cond_3
    invoke-static {v0}, LX/2v7;->f(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1831084
    const/4 v3, 0x0

    .line 1831085
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831086
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1831087
    const v1, -0x22a42d2a    # -9.8999738E17f

    invoke-static {v0, v1}, LX/1VX;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;I)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    .line 1831088
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    new-instance v2, LX/BuS;

    invoke-direct {v2, p0, v0, p1}, LX/BuS;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;Lcom/facebook/graphql/model/GraphQLPage;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1831089
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->e:Landroid/view/View;

    const v2, 0x7f0213b4

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1831090
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1831091
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->n:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setAllCaps(Z)V

    .line 1831092
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v0

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->a$redex0(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;Z)V

    .line 1831093
    goto/16 :goto_2

    .line 1831094
    :cond_4
    invoke-virtual {p0}, LX/2oy;->n()V

    goto/16 :goto_2

    :cond_5
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 1831021
    if-eqz p2, :cond_0

    .line 1831022
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->h(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;)V

    .line 1831023
    :cond_0
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->b(LX/2pa;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1831024
    :cond_1
    invoke-virtual {p0}, LX/2oy;->n()V

    .line 1831025
    :goto_0
    return-void

    .line 1831026
    :cond_2
    const/4 v0, 0x0

    .line 1831027
    iget-object v1, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "GraphQLStoryProps"

    invoke-virtual {v1, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1831028
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831029
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1831030
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1831031
    :cond_3
    :goto_1
    if-eqz v0, :cond_6

    .line 1831032
    invoke-static {v0}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1831033
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 1831034
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->d:LX/AjP;

    .line 1831035
    const/4 p1, 0x0

    invoke-virtual {v2, v1, p1}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    .line 1831036
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->d:LX/AjP;

    new-instance v2, LX/BuQ;

    invoke-direct {v2, p0}, LX/BuQ;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;)V

    .line 1831037
    iput-object v2, v1, LX/AjP;->c:LX/AjN;

    .line 1831038
    :cond_4
    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->setupCallToActionButton(Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    goto :goto_0

    .line 1831039
    :cond_5
    invoke-static {p1}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->b(LX/2pa;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1831040
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "MultiShareGraphQLSubStoryPropsKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 1831041
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v2, "MultiShareGraphQLSubStoryIndexKey"

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .line 1831042
    instance-of v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/03g;->a(Z)V

    move-object v0, v1

    .line 1831043
    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831044
    iget-object p2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p2

    .line 1831045
    instance-of v0, v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 1831046
    instance-of v0, v2, Ljava/lang/Integer;

    invoke-static {v0}, LX/03g;->a(Z)V

    .line 1831047
    check-cast v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831048
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831049
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1831050
    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object p2

    move-object v0, v2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    move-object v0, v0

    .line 1831051
    goto :goto_1

    .line 1831052
    :cond_6
    invoke-virtual {p0}, LX/2oy;->n()V

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1831053
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionButtonPlugin;->d:LX/AjP;

    invoke-virtual {v0}, LX/AjP;->a()V

    .line 1831054
    return-void
.end method
