.class public Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1833565
    const-class v0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 6
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1833566
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 1833567
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->k:Z

    .line 1833568
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1833569
    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1833570
    new-instance v2, Lcom/facebook/video/player/plugins/SubtitlePlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;)V

    .line 1833571
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    new-instance v4, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v5, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v4, p1, v5}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->b:LX/0Px;

    .line 1833572
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    iget-object v4, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v3, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v3, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v3, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v3, LX/7N4;

    invoke-direct {v3, p1}, LX/7N4;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->c:LX/0Px;

    .line 1833573
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v3, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    new-instance v3, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->d:LX/0Px;

    .line 1833574
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->j:LX/0Px;

    .line 1833575
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->i:LX/0Px;

    .line 1833576
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->e:LX/0Px;

    .line 1833577
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchInCanvasRichVideoPluginSelector;->f:LX/0Px;

    .line 1833578
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 1

    .prologue
    .line 1833579
    const-class v0, LX/BvQ;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1833580
    sget-object v0, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    .line 1833581
    :goto_0
    return-object v0

    .line 1833582
    :cond_0
    const-class v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1833583
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    goto :goto_0

    .line 1833584
    :cond_1
    const-class v0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1833585
    sget-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 1833586
    :cond_2
    const-class v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1833587
    sget-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    goto :goto_0

    .line 1833588
    :cond_3
    sget-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    goto :goto_0
.end method
