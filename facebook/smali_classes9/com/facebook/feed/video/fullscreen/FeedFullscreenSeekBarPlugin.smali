.class public Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;
.super Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lg;",
        ":",
        "LX/7Lk;",
        ">",
        "Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private A:LX/5OM;

.field private B:Z

.field private C:Z

.field public D:Ljava/lang/String;

.field public E:Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

.field public F:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private G:Landroid/widget/SeekBar;

.field private H:Lcom/facebook/resources/ui/FbTextView;

.field private I:Lcom/facebook/resources/ui/FbTextView;

.field public o:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Landroid/support/v4/app/FragmentActivity;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Or;
    .annotation runtime Lcom/facebook/feed/annotations/IsInNewPlayerOldUIClosedCaptioningGateKeeper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/video/fullscreen/VideoDeleteController;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/00H;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final y:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

.field private final z:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1830444
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1830445
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1830511
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830512
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1830501
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830502
    const-class v0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1830503
    const v0, 0x7f0d110c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->y:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    .line 1830504
    const v0, 0x7f0d0c13

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->z:Landroid/view/View;

    .line 1830505
    const v0, 0x7f0d132a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->G:Landroid/widget/SeekBar;

    .line 1830506
    const v0, 0x7f0d132b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->H:Lcom/facebook/resources/ui/FbTextView;

    .line 1830507
    const v0, 0x7f0d1329

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->I:Lcom/facebook/resources/ui/FbTextView;

    .line 1830508
    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->E:Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    .line 1830509
    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->F:LX/0Px;

    .line 1830510
    return-void
.end method

.method private static a(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;LX/17Y;Lcom/facebook/content/SecureContextHelper;Landroid/support/v4/app/FragmentActivity;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0TD;LX/03V;LX/00H;LX/0wM;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;",
            "LX/17Y;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/video/fullscreen/VideoDeleteController;",
            ">;",
            "LX/0TD;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/00H;",
            "LX/0wM;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1830500
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->o:LX/17Y;

    iput-object p2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->p:Lcom/facebook/content/SecureContextHelper;

    iput-object p3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->q:Landroid/support/v4/app/FragmentActivity;

    iput-object p4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->r:LX/0Or;

    iput-object p5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->s:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object p6, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->t:LX/0Ot;

    iput-object p7, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->u:LX/0TD;

    iput-object p8, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->v:LX/03V;

    iput-object p9, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->w:LX/00H;

    iput-object p10, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->x:LX/0wM;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 11

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v10

    move-object v0, p0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-static {v10}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v1

    check-cast v1, LX/17Y;

    invoke-static {v10}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v10}, LX/1No;->b(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/FragmentActivity;

    const/16 v4, 0x149d

    invoke-static {v10, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v10}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v6, 0x1d6f

    invoke-static {v10, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {v10}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static {v10}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    const-class v9, LX/00H;

    invoke-interface {v10, v9}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/00H;

    invoke-static {v10}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v10

    check-cast v10, LX/0wM;

    invoke-static/range {v0 .. v10}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->a(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;LX/17Y;Lcom/facebook/content/SecureContextHelper;Landroid/support/v4/app/FragmentActivity;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/0TD;LX/03V;LX/00H;LX/0wM;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1830499
    if-eqz p0, :cond_0

    check-cast p0, Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;Landroid/view/View;)V
    .locals 3

    .prologue
    .line 1830494
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    new-instance v1, LX/Bu4;

    invoke-direct {v1, p0}, LX/Bu4;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;)V

    .line 1830495
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 1830496
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v1, LX/2os;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LX/2os;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/2oj;->a(LX/2ol;)V

    .line 1830497
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    invoke-virtual {v0, p1}, LX/0ht;->f(Landroid/view/View;)V

    .line 1830498
    return-void
.end method

.method private j()V
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1830472
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->w:LX/00H;

    .line 1830473
    iget-object v3, v0, LX/00H;->j:LX/01T;

    move-object v0, v3

    .line 1830474
    sget-object v3, LX/01T;->FB4A:LX/01T;

    if-ne v0, v3, :cond_0

    move v0, v1

    .line 1830475
    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    instance-of v0, v0, LX/7Lk;

    if-eqz v0, :cond_1

    .line 1830476
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->y:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/plugins/OverflowMenuPlugin;->setVisibility(I)V

    .line 1830477
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->z:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1830478
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 1830479
    goto :goto_0

    .line 1830480
    :cond_1
    new-instance v0, LX/6WS;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, LX/6WS;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    .line 1830481
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->C:Z

    if-eqz v0, :cond_5

    .line 1830482
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    const v3, 0x7f081044

    invoke-virtual {v0, v1, v2, v3}, LX/5OG;->a(III)LX/3Ai;

    move-result-object v0

    const v3, 0x7f020818

    invoke-virtual {v0, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move v0, v1

    .line 1830483
    :goto_2
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->k()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->z()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1830484
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    const v3, 0x7f081045

    invoke-virtual {v0, v5, v1, v3}, LX/5OG;->a(III)LX/3Ai;

    move-result-object v0

    const v3, 0x7f0207ff

    invoke-virtual {v0, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move v0, v1

    .line 1830485
    :cond_2
    iget-boolean v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->B:Z

    if-eqz v3, :cond_4

    .line 1830486
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    invoke-virtual {v0}, LX/5OM;->c()LX/5OG;

    move-result-object v0

    const/4 v3, 0x3

    const v4, 0x7f08104c

    invoke-virtual {v0, v3, v5, v4}, LX/5OG;->a(III)LX/3Ai;

    move-result-object v0

    const v3, 0x7f020a0b

    invoke-virtual {v0, v3}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 1830487
    :goto_3
    if-eqz v1, :cond_3

    .line 1830488
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->z:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1830489
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->z:Landroid/view/View;

    new-instance v1, LX/Bu3;

    invoke-direct {v1, p0}, LX/Bu3;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1830490
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    new-instance v1, LX/BuA;

    invoke-direct {v1, p0}, LX/BuA;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;)V

    .line 1830491
    iput-object v1, v0, LX/5OM;->p:LX/5OO;

    .line 1830492
    goto :goto_1

    .line 1830493
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->z:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method private k()Z
    .locals 2

    .prologue
    .line 1830471
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->r:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private z()Z
    .locals 1

    .prologue
    .line 1830470
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->F:LX/0Px;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->F:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 1830453
    invoke-super {p0, p1, p2}, Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;->a(LX/2pa;Z)V

    .line 1830454
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    .line 1830455
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "SubtitlesLocalesKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->F:LX/0Px;

    .line 1830456
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "ShowDeleteOptionKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->a(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->B:Z

    .line 1830457
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "ShowReportOptionKey"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->a(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->C:Z

    .line 1830458
    :cond_0
    if-eqz p2, :cond_1

    .line 1830459
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->j()V

    .line 1830460
    :cond_1
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->D:Ljava/lang/String;

    .line 1830461
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    instance-of v0, v0, LX/7Lk;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Lg;

    check-cast v0, LX/7Lk;

    invoke-interface {v0}, LX/7Lk;->n()LX/1SX;

    move-result-object v0

    instance-of v0, v0, LX/Bur;

    if-eqz v0, :cond_2

    .line 1830462
    iget-object v0, p0, LX/3Gb;->n:LX/7Lf;

    check-cast v0, LX/7Lg;

    check-cast v0, LX/7Lk;

    invoke-interface {v0}, LX/7Lk;->n()LX/1SX;

    move-result-object v0

    check-cast v0, LX/Bur;

    .line 1830463
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->F:LX/0Px;

    .line 1830464
    iput-object v1, v0, LX/Bur;->v:LX/0Px;

    .line 1830465
    iget-object v1, p0, LX/2oy;->i:LX/2oj;

    .line 1830466
    iput-object v1, v0, LX/Bur;->w:LX/2oj;

    .line 1830467
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->y:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    iget-object v1, p0, LX/3Gb;->n:LX/7Lf;

    invoke-virtual {v0, v1}, LX/3Gb;->setEnvironment(LX/7Lf;)V

    .line 1830468
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->y:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    iget-object v2, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v1, v2, p1}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1830469
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1830447
    invoke-super {p0}, Lcom/facebook/video/player/plugins/FullscreenSeekBarPlugin;->d()V

    .line 1830448
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->E:Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    .line 1830449
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    if-eqz v0, :cond_0

    .line 1830450
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->A:LX/5OM;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1830451
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;->y:Lcom/facebook/video/player/plugins/OverflowMenuPlugin;

    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 1830452
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1830446
    const v0, 0x7f03062a

    return v0
.end method
