.class public Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;
.super LX/3Gb;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lf;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pn;",
        ">",
        "LX/3Gb",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public a:LX/20g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/20e;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1XK;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1WX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/20K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1Wo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/1Qj;

.field private r:Lcom/facebook/components/ComponentView;

.field private s:Landroid/widget/LinearLayout;

.field private t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

.field private u:LX/21T;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1831321
    invoke-direct {p0, p1}, LX/3Gb;-><init>(Landroid/content/Context;)V

    .line 1831322
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->g()V

    .line 1831323
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1831377
    invoke-direct {p0, p1, p2}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831378
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->g()V

    .line 1831379
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1831374
    invoke-direct {p0, p1, p2, p3}, LX/3Gb;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831375
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->g()V

    .line 1831376
    return-void
.end method

.method private static a(Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;LX/20g;LX/20e;LX/1zf;LX/0Ot;LX/0Ot;LX/20K;LX/0hB;LX/1Wo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;",
            "LX/20g;",
            "LX/20e;",
            "LX/1zf;",
            "LX/0Ot",
            "<",
            "LX/1XK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1WX;",
            ">;",
            "LX/20K;",
            "LX/0hB;",
            "LX/1Wo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1831373
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->a:LX/20g;

    iput-object p2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->b:LX/20e;

    iput-object p3, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->c:LX/1zf;

    iput-object p4, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->d:LX/0Ot;

    iput-object p5, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->e:LX/0Ot;

    iput-object p6, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->f:LX/20K;

    iput-object p7, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->o:LX/0hB;

    iput-object p8, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->p:LX/1Wo;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 10

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v8

    move-object v0, p0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;

    invoke-static {v8}, LX/20g;->a(LX/0QB;)LX/20g;

    move-result-object v1

    check-cast v1, LX/20g;

    invoke-static {v8}, LX/20e;->b(LX/0QB;)LX/20e;

    move-result-object v2

    check-cast v2, LX/20e;

    invoke-static {v8}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v3

    check-cast v3, LX/1zf;

    const/16 v4, 0x853

    invoke-static {v8, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x7b3

    invoke-static {v8, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {v8}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v6

    check-cast v6, LX/20K;

    invoke-static {v8}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v7

    check-cast v7, LX/0hB;

    const-class v9, LX/1Wo;

    invoke-interface {v8, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/1Wo;

    invoke-static/range {v0 .. v8}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->a(Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;LX/20g;LX/20e;LX/1zf;LX/0Ot;LX/0Ot;LX/20K;LX/0hB;LX/1Wo;)V

    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1831365
    const-class v0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1831366
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0a03

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 1831367
    const v1, 0x7f03073c

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1831368
    const v0, 0x7f0d134f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/components/ComponentView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->r:Lcom/facebook/components/ComponentView;

    .line 1831369
    const v0, 0x7f0d134e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->s:Landroid/widget/LinearLayout;

    .line 1831370
    const v0, 0x7f0d1350

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    .line 1831371
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->p:LX/1Wo;

    sget-object v1, LX/21S;->INLINE:LX/21S;

    invoke-virtual {v0, v1}, LX/1Wo;->a(LX/21S;)LX/21T;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->u:LX/21T;

    .line 1831372
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1831380
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->s:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1831381
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1831363
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->s:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1831364
    return-void
.end method

.method private j()V
    .locals 4

    .prologue
    .line 1831361
    new-instance v0, LX/Buc;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, LX/1PU;->b:LX/1PY;

    invoke-direct {v0, p0, v1, v2, v3}, LX/Buc;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->q:LX/1Qj;

    .line 1831362
    return-void
.end method

.method private setupBlingBar(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1831354
    new-instance v1, LX/1De;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0e0a03

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v1, v0}, LX/1De;-><init>(Landroid/content/Context;)V

    .line 1831355
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1XK;

    invoke-virtual {v0, v1}, LX/1XK;->c(LX/1De;)LX/1XM;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/1XM;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XM;

    move-result-object v0

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->q:LX/1Qj;

    invoke-virtual {v0, v2}, LX/1XM;->a(LX/1Po;)LX/1XM;

    move-result-object v2

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1WX;

    const/4 v3, 0x0

    const v4, 0x7f0e0a01

    invoke-virtual {v0, v1, v3, v4}, LX/1WX;->a(LX/1De;II)LX/1XJ;

    move-result-object v3

    .line 1831356
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831357
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v3, v0}, LX/1XJ;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1XJ;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/1XJ;->c(Z)LX/1XJ;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/1XM;->a(LX/1X5;)LX/1XM;

    move-result-object v0

    .line 1831358
    invoke-static {v1, v0}, LX/1dV;->a(LX/1De;LX/1X5;)LX/1me;

    move-result-object v0

    invoke-virtual {v0}, LX/1me;->b()LX/1dV;

    move-result-object v0

    .line 1831359
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->r:Lcom/facebook/components/ComponentView;

    invoke-virtual {v1, v0}, Lcom/facebook/components/ComponentView;->setComponent(LX/1dV;)V

    .line 1831360
    return-void
.end method

.method private setupFooter(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1831350
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->i()V

    .line 1831351
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->setupBlingBar(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1831352
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->setupReactionsFooter(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1831353
    return-void
.end method

.method private setupReactionsFooter(Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1831337
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->a:LX/20g;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->q:LX/1Qj;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, LX/20g;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Po;Z)LX/21I;

    move-result-object v10

    .line 1831338
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1831339
    move-object v9, v0

    check-cast v9, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1831340
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->u:LX/21T;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v1

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v2

    invoke-static {v9}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, LX/21T;->a(ZZZ)LX/21Y;

    move-result-object v11

    .line 1831341
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->o:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v11, v0}, LX/21Y;->a(I)LX/21c;

    move-result-object v12

    .line 1831342
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v1, v10, LX/21I;->g:Ljava/util/EnumMap;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    iget-object v3, v10, LX/21I;->c:LX/20Z;

    invoke-static {v0, v1, v2, v3}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 1831343
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    iget-object v2, v10, LX/21I;->d:LX/21M;

    iget-object v3, v10, LX/21I;->e:LX/0wd;

    iget-object v4, v10, LX/21I;->f:LX/20z;

    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->c:LX/1zf;

    sget-object v6, LX/20I;->DARK:LX/20I;

    iget-object v7, v10, LX/21I;->i:LX/0Px;

    iget-object v8, v10, LX/21I;->j:LX/21H;

    invoke-static/range {v0 .. v8}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V

    .line 1831344
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v11, v12}, LX/21Y;->a(LX/21c;)[F

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtonWeights([F)V

    .line 1831345
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-static {v12}, LX/21c;->hasIcons(LX/21c;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setShowIcons(Z)V

    .line 1831346
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->b:LX/20e;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, v1, v9}, LX/20e;->a(LX/1wK;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1831347
    iget-boolean v0, v10, LX/21I;->a:Z

    if-eqz v0, :cond_0

    .line 1831348
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    sget-object v1, LX/20X;->COMMENT:LX/20X;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20X;)Landroid/view/View;

    move-result-object v0

    iget-object v1, v10, LX/21I;->b:LX/20b;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1831349
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1831326
    invoke-static {p1}, LX/393;->a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1831327
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v0, :cond_2

    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1831328
    :goto_0
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    if-eqz v2, :cond_0

    if-nez p2, :cond_0

    .line 1831329
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->f:LX/20K;

    invoke-static {v2, v3}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V

    .line 1831330
    :cond_0
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->q:LX/1Qj;

    if-nez v2, :cond_1

    .line 1831331
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->j()V

    .line 1831332
    :cond_1
    if-eqz v1, :cond_3

    if-nez v0, :cond_3

    .line 1831333
    invoke-direct {p0, v1}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->setupFooter(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1831334
    :goto_1
    return-void

    .line 1831335
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 1831336
    :cond_3
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->h()V

    goto :goto_1
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1831324
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->t:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenPurpleRainStoryFooterPlugin;->f:LX/20K;

    invoke-static {v0, v1}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V

    .line 1831325
    return-void
.end method
