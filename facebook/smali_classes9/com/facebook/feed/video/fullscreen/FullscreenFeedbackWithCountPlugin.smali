.class public Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;
.super LX/BuK;
.source ""


# instance fields
.field private final s:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1831250
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831251
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1831252
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831253
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1831254
    invoke-direct {p0, p1, p2, p3}, LX/BuK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831255
    const v0, 0x7f0d119e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;->s:Landroid/view/View;

    .line 1831256
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bua;

    invoke-direct {v1, p0}, LX/Bua;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831257
    return-void
.end method

.method public static c(Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;I)V
    .locals 2

    .prologue
    .line 1831258
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 1831259
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;->s:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1831260
    :goto_0
    return-void

    .line 1831261
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;->s:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1831262
    invoke-super {p0, p1, p2}, LX/BuK;->a(LX/2pa;Z)V

    .line 1831263
    if-eqz p2, :cond_0

    .line 1831264
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;->c(Lcom/facebook/feed/video/fullscreen/FullscreenFeedbackWithCountPlugin;I)V

    .line 1831265
    :cond_0
    return-void
.end method
