.class public Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;
.super LX/3Ga;
.source ""

# interfaces
.implements LX/Abf;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Abg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Abd;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/AVV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/view/View;

.field private f:Lcom/facebook/fbui/facepile/FacepileView;

.field private o:Lcom/facebook/widget/text/BetterTextView;

.field private p:Lcom/facebook/widget/text/BetterTextView;

.field private q:Lcom/facebook/widget/text/BetterTextView;

.field private r:Landroid/animation/Animator;

.field private s:Z

.field private t:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private u:Lcom/facebook/graphql/model/GraphQLStoryAttachment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private v:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1832562
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832563
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832667
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832668
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1832659
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832660
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1832661
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b:LX/0Ot;

    .line 1832662
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1832663
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->c:LX/0Ot;

    .line 1832664
    const-class v0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1832665
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bvc;

    invoke-direct {v1, p0}, LX/Bvc;-><init>(Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1832666
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;

    const/16 v2, 0x1bf2

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 p0, 0x1bf1

    invoke-static {v1, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v1}, LX/AVV;->a(LX/0QB;)LX/AVV;

    move-result-object v1

    check-cast v1, LX/AVV;

    iput-object v2, p1, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b:LX/0Ot;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->c:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->d:LX/AVV;

    return-void
.end method

.method private b(LX/Abe;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1832639
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->v:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->u:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-nez v0, :cond_1

    .line 1832640
    :cond_0
    :goto_0
    return-void

    .line 1832641
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->v:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1832642
    sget-object v0, LX/Bvb;->a:[I

    invoke-virtual {p1}, LX/Abe;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1832643
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->u:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1832644
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1832645
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abd;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->v:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v0, v2}, LX/Abd;->c(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)J

    move-result-wide v2

    invoke-static {v2, v3}, LX/3Hk;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1832646
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->u:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1832647
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1832648
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, LX/3Hk;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1832649
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->r:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 1832650
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->r:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 1832651
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setAlpha(F)V

    .line 1832652
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1832653
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->v:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->G()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1832654
    :pswitch_3
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->j(Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;)V

    goto :goto_0

    .line 1832655
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1832656
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->o:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->u:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1832657
    sget-object v0, LX/Abe;->TIMED_OUT:LX/Abe;

    if-ne p1, v0, :cond_0

    .line 1832658
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->j(Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static j(Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;)V
    .locals 2

    .prologue
    .line 1832632
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 1832633
    if-eqz v0, :cond_0

    .line 1832634
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1832635
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    invoke-virtual {v0}, LX/Abg;->b()V

    .line 1832636
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->r:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 1832637
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setAlpha(F)V

    .line 1832638
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 3

    .prologue
    .line 1832629
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 1832630
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1, p2}, LX/3Hk;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1832631
    return-void
.end method

.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1832595
    invoke-super {p0, p1, p2}, LX/3Ga;->a(LX/2pa;Z)V

    .line 1832596
    if-nez p2, :cond_1

    invoke-static {p1}, LX/393;->p(LX/2pa;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1832597
    :cond_0
    :goto_0
    return-void

    .line 1832598
    :cond_1
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->j(Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;)V

    .line 1832599
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    .line 1832600
    if-eqz v0, :cond_0

    .line 1832601
    invoke-static {p1}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->t:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1832602
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->t:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 1832603
    invoke-static {p1}, LX/393;->c(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->u:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1832604
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->u:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 1832605
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->u:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1832606
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v1

    .line 1832607
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1832608
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->e:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1832609
    :cond_2
    invoke-static {v1}, LX/Abd;->d(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Ljava/lang/String;

    move-result-object v2

    .line 1832610
    invoke-static {v1}, LX/Abd;->e(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)Ljava/lang/String;

    move-result-object v1

    .line 1832611
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1832612
    if-eqz v2, :cond_3

    .line 1832613
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1832614
    :cond_3
    if-eqz v1, :cond_4

    .line 1832615
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1832616
    :cond_4
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->f:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1832617
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->v:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1832618
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->v:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v0, v1}, LX/Abg;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V

    .line 1832619
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->v:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->s()Z

    move-result v1

    .line 1832620
    iput-boolean v1, v0, LX/Abg;->f:Z

    .line 1832621
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    invoke-virtual {v0}, LX/Abg;->a()LX/Abe;

    move-result-object v0

    .line 1832622
    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b(LX/Abe;)V

    .line 1832623
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->t:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    .line 1832624
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1832625
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1832626
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->q:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1832627
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->q:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1832628
    :cond_5
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->q:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public final a(LX/Abe;)V
    .locals 0

    .prologue
    .line 1832593
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b(LX/Abe;)V

    .line 1832594
    return-void
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 1832592
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1832589
    invoke-super {p0}, LX/3Ga;->d()V

    .line 1832590
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->j(Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;)V

    .line 1832591
    return-void
.end method

.method public final dH_()V
    .locals 3

    .prologue
    .line 1832583
    invoke-super {p0}, LX/3Ga;->dH_()V

    .line 1832584
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 1832585
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->s:Z

    if-eqz v0, :cond_0

    .line 1832586
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->d:LX/AVV;

    const-string v1, "app foregrounded"

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->t:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, v2}, LX/AVV;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1832587
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->s:Z

    .line 1832588
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 1832582
    const v0, 0x7f031267

    return v0
.end method

.method public getStubLayout()I
    .locals 1

    .prologue
    .line 1832581
    const v0, 0x7f031266

    return v0
.end method

.method public final s()V
    .locals 3

    .prologue
    .line 1832575
    invoke-super {p0}, LX/3Ga;->s()V

    .line 1832576
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 1832577
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1832578
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->d:LX/AVV;

    const-string v1, "app backgrounded"

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->t:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v1, v2}, LX/AVV;->b(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1832579
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->s:Z

    .line 1832580
    return-void
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 1832574
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1832564
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->e:Landroid/view/View;

    .line 1832565
    const v0, 0x7f0d2b3a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->f:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1832566
    const v0, 0x7f0d2b3b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->o:Lcom/facebook/widget/text/BetterTextView;

    .line 1832567
    const v0, 0x7f0d2b3c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    .line 1832568
    const v0, 0x7f0d2b3d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->q:Lcom/facebook/widget/text/BetterTextView;

    .line 1832569
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->f:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setReverseFacesZIndex(Z)V

    .line 1832570
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->p:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0}, LX/Abd;->a(Ljava/lang/Object;)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->r:Landroid/animation/Animator;

    .line 1832571
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveLobbyInfoPlugin;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    .line 1832572
    iput-object p0, v0, LX/Abg;->g:LX/Abf;

    .line 1832573
    return-void
.end method
