.class public Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;
.super LX/2oy;
.source ""


# instance fields
.field private a:Landroid/view/View;

.field public b:Lcom/facebook/resources/ui/FbTextView;

.field private c:Landroid/view/View;

.field public d:Z

.field private e:Ljava/lang/String;

.field private f:LX/BuZ;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1831197
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831198
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1831199
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831200
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1831201
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831202
    const v0, 0x7f030737

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1831203
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->g()V

    .line 1831204
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->c:Landroid/view/View;

    new-instance v1, LX/BuX;

    invoke-direct {v1, p0}, LX/BuX;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1831205
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BuY;

    invoke-direct {v1, p0}, LX/BuY;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831206
    return-void
.end method

.method public static c(Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;I)V
    .locals 2

    .prologue
    .line 1831207
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->c:Landroid/view/View;

    const/4 v0, 0x1

    if-le p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1831208
    return-void

    .line 1831209
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 1831210
    const v0, 0x7f0d1349

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->a:Landroid/view/View;

    .line 1831211
    const v0, 0x7f0d0842

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->b:Lcom/facebook/resources/ui/FbTextView;

    .line 1831212
    const v0, 0x7f0d134a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->c:Landroid/view/View;

    .line 1831213
    return-void
.end method

.method public static h(Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 1831214
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->d:Z

    if-eqz v0, :cond_0

    .line 1831215
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->b:Lcom/facebook/resources/ui/FbTextView;

    const v1, 0x7fffffff

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 1831216
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1831217
    :goto_0
    return-void

    .line 1831218
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1831219
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1831220
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1831221
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1831222
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->b:Lcom/facebook/resources/ui/FbTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setMaxLines(I)V

    .line 1831223
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->b:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbTextView;->getLineCount()I

    move-result v0

    .line 1831224
    if-nez v0, :cond_3

    .line 1831225
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->f:LX/BuZ;

    if-nez v1, :cond_2

    .line 1831226
    new-instance v1, LX/BuZ;

    invoke-direct {v1, p0}, LX/BuZ;-><init>(Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;)V

    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->f:LX/BuZ;

    .line 1831227
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->f:LX/BuZ;

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1831228
    :cond_3
    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->c(Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;I)V

    goto :goto_0
.end method

.method private setDescription(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 2

    .prologue
    .line 1831229
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->e:Ljava/lang/String;

    .line 1831230
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->b:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1831231
    return-void

    .line 1831232
    :cond_0
    invoke-static {p1}, LX/1z5;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1831233
    if-nez p2, :cond_0

    .line 1831234
    :goto_0
    return-void

    .line 1831235
    :cond_0
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831236
    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 1831237
    :goto_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 1831238
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->a:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1831239
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1831240
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->setDescription(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1831241
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->h(Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;)V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 1831242
    goto :goto_1

    .line 1831243
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenDescriptionPlugin;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
