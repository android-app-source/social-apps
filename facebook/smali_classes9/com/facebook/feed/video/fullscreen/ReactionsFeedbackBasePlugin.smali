.class public Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;
.super LX/2oy;
.source ""


# instance fields
.field private A:LX/20Z;

.field private B:LX/0wd;

.field private C:LX/21M;

.field public a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wW;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/20w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1zf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/20h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/20K;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/20r;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/20s;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/1Wo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/1EP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

.field private final x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

.field private y:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private z:LX/21T;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1832517
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832518
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832515
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832516
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1832508
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832509
    const-class v0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1832510
    const v0, 0x7f031185

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1832511
    const v0, 0x7f0d293a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    .line 1832512
    const v0, 0x7f0d0a48

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1832513
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->g()V

    .line 1832514
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/20z;
    .locals 3

    .prologue
    .line 1832507
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->c:LX/20w;

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    const-string v2, "video"

    invoke-virtual {v0, p1, v1, v2}, LX/20w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/20z;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;LX/0Or;LX/0wW;LX/20w;LX/1zf;LX/20h;LX/20K;LX/20r;LX/20s;LX/1Wo;LX/0Ot;LX/0Ot;LX/0Zb;LX/1EP;LX/1Kf;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/0wW;",
            "LX/20w;",
            "LX/1zf;",
            "LX/20h;",
            "LX/20K;",
            "LX/20r;",
            "LX/20s;",
            "LX/1Wo;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1nI;",
            ">;",
            "LX/0Zb;",
            "LX/1EP;",
            "LX/1Kf;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1832506
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->a:LX/0Or;

    iput-object p2, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->b:LX/0wW;

    iput-object p3, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->c:LX/20w;

    iput-object p4, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->d:LX/1zf;

    iput-object p5, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->e:LX/20h;

    iput-object p6, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->f:LX/20K;

    iput-object p7, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->n:LX/20r;

    iput-object p8, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->o:LX/20s;

    iput-object p9, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->p:LX/1Wo;

    iput-object p10, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->q:LX/0Ot;

    iput-object p11, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->r:LX/0Ot;

    iput-object p12, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->s:LX/0Zb;

    iput-object p13, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->t:LX/1EP;

    iput-object p14, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->u:LX/1Kf;

    iput-object p15, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->v:LX/0Ot;

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 12

    .prologue
    .line 1832483
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->o()Z

    move-result v9

    .line 1832484
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->c()Z

    move-result v10

    .line 1832485
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832486
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1832487
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/214;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v11

    .line 1832488
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/20X;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 1832489
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->a:LX/0Or;

    invoke-static {v9, v10, v11, v0, v1}, LX/212;->a(ZZZLjava/util/EnumMap;LX/0Or;)V

    .line 1832490
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->A:LX/20Z;

    invoke-static {v1, v0, p1, v2}, LX/212;->a(LX/1wK;Ljava/util/EnumMap;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20Z;)V

    .line 1832491
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->C:LX/21M;

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->B:LX/0wd;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832492
    iget-object v4, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v4

    .line 1832493
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLFeedback;)LX/20z;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->d:LX/1zf;

    sget-object v6, LX/20I;->DARK:LX/20I;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->d:LX/1zf;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v7

    invoke-virtual {v1, v7}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v7

    const/4 v8, 0x0

    move-object v1, p1

    invoke-static/range {v0 .. v8}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V

    .line 1832494
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    sget-object v1, LX/20H;->DEFAULT:LX/20H;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20H;Z)V

    .line 1832495
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->z:LX/21T;

    invoke-virtual {v0, v9, v10, v11}, LX/21T;->a(ZZZ)LX/21Y;

    move-result-object v0

    .line 1832496
    sget-object v1, LX/21c;->ICONS_EQUAL_WIDTH:LX/21c;

    .line 1832497
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, v1}, LX/21Y;->a(LX/21c;)[F

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setButtonWeights([F)V

    .line 1832498
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setShowIcons(Z)V

    .line 1832499
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-virtual {v0, p1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setReactorsCount(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1832500
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-static {p1}, LX/16z;->e(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setCommentsCount(I)V

    .line 1832501
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-static {p1}, LX/16z;->l(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setSharesCount(I)V

    .line 1832502
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832503
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1832504
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/0sa;->e(Lcom/facebook/graphql/model/GraphQLStory;)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setViewsCount(I)V

    .line 1832505
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 17

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v15

    move-object/from16 v0, p0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;

    const/16 v1, 0x13a4

    invoke-static {v15, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-static {v15}, LX/0wW;->b(LX/0QB;)LX/0wW;

    move-result-object v2

    check-cast v2, LX/0wW;

    const-class v3, LX/20w;

    invoke-interface {v15, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/20w;

    invoke-static {v15}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v4

    check-cast v4, LX/1zf;

    invoke-static {v15}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v5

    check-cast v5, LX/20h;

    invoke-static {v15}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v6

    check-cast v6, LX/20K;

    invoke-static {v15}, LX/20r;->a(LX/0QB;)LX/20r;

    move-result-object v7

    check-cast v7, LX/20r;

    invoke-static {v15}, LX/20s;->a(LX/0QB;)LX/20s;

    move-result-object v8

    check-cast v8, LX/20s;

    const-class v9, LX/1Wo;

    invoke-interface {v15, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/1Wo;

    const/16 v10, 0x3567

    invoke-static {v15, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x7bc

    invoke-static {v15, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static {v15}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v12

    check-cast v12, LX/0Zb;

    invoke-static {v15}, LX/1EP;->a(LX/0QB;)LX/1EP;

    move-result-object v13

    check-cast v13, LX/1EP;

    invoke-static {v15}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v14

    check-cast v14, LX/1Kf;

    const/16 v16, 0xbc6

    invoke-static/range {v15 .. v16}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v0 .. v15}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->a(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;LX/0Or;LX/0wW;LX/20w;LX/1zf;LX/20h;LX/20K;LX/20r;LX/20s;LX/1Wo;LX/0Ot;LX/0Ot;LX/0Zb;LX/1EP;LX/1Kf;LX/0Ot;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;Z)V
    .locals 7

    .prologue
    .line 1832444
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getFeedback(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1832445
    if-nez v3, :cond_0

    .line 1832446
    :goto_0
    return-void

    .line 1832447
    :cond_0
    new-instance v0, LX/8qL;

    invoke-direct {v0}, LX/8qL;-><init>()V

    .line 1832448
    iput-object v3, v0, LX/8qL;->a:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 1832449
    move-object v0, v0

    .line 1832450
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v1

    .line 1832451
    iput-object v1, v0, LX/8qL;->d:Ljava/lang/String;

    .line 1832452
    move-object v0, v0

    .line 1832453
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v1

    .line 1832454
    iput-object v1, v0, LX/8qL;->e:Ljava/lang/String;

    .line 1832455
    move-object v0, v0

    .line 1832456
    invoke-static {v3}, LX/16z;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)Z

    move-result v1

    .line 1832457
    iput-boolean v1, v0, LX/8qL;->p:Z

    .line 1832458
    move-object v0, v0

    .line 1832459
    new-instance v1, LX/21A;

    invoke-direct {v1}, LX/21A;-><init>()V

    const-string v2, "video"

    .line 1832460
    iput-object v2, v1, LX/21A;->c:Ljava/lang/String;

    .line 1832461
    move-object v1, v1

    .line 1832462
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getTrackingCodes(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)LX/162;

    move-result-object v2

    .line 1832463
    iput-object v2, v1, LX/21A;->a:LX/162;

    .line 1832464
    move-object v1, v1

    .line 1832465
    sget-object v2, LX/21D;->FULLSCREEN_VIDEO_PLAYER:LX/21D;

    .line 1832466
    iput-object v2, v1, LX/21A;->i:LX/21D;

    .line 1832467
    move-object v1, v1

    .line 1832468
    invoke-virtual {v1}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v1

    .line 1832469
    iput-object v1, v0, LX/8qL;->g:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1832470
    move-object v0, v0

    .line 1832471
    iput-boolean p1, v0, LX/8qL;->i:Z

    .line 1832472
    move-object v1, v0

    .line 1832473
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832474
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1832475
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1832476
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832477
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1832478
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1832479
    iput-object v0, v1, LX/8qL;->c:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832480
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v2, "comment"

    invoke-virtual {v0, v2}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1832481
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1nI;

    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1}, LX/8qL;->a()Lcom/facebook/ufiservices/flyout/FeedbackParams;

    move-result-object v1

    invoke-interface {v0, v2, v1}, LX/1nI;->a(Landroid/content/Context;Lcom/facebook/ufiservices/flyout/FeedbackParams;)V

    .line 1832482
    iget-object v6, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->s:LX/0Zb;

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getTrackingCodes(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)LX/162;

    move-result-object v4

    const-string v5, "video"

    invoke-static/range {v0 .. v5}, LX/1EP;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v6, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto/16 :goto_0
.end method

.method private g()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1832519
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->h()LX/20Z;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->A:LX/20Z;

    .line 1832520
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->i()LX/21M;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->C:LX/21M;

    .line 1832521
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    new-instance v1, LX/BvX;

    invoke-direct {v1, p0}, LX/BvX;-><init>(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1832522
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    .line 1832523
    iput-boolean v6, v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->s:Z

    .line 1832524
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->p:LX/1Wo;

    sget-object v1, LX/21S;->INLINE:LX/21S;

    invoke-virtual {v0, v1}, LX/1Wo;->a(LX/21S;)LX/21T;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->z:LX/21T;

    .line 1832525
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->b:LX/0wW;

    invoke-virtual {v0}, LX/0wW;->a()LX/0wd;

    move-result-object v0

    const-wide/high16 v2, 0x4026000000000000L    # 11.0

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, LX/0wT;->b(DD)LX/0wT;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0wd;->a(LX/0wT;)LX/0wd;

    move-result-object v0

    .line 1832526
    iput-boolean v6, v0, LX/0wd;->c:Z

    .line 1832527
    move-object v0, v0

    .line 1832528
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->B:LX/0wd;

    .line 1832529
    return-void
.end method

.method private getBaseContext()Landroid/content/Context;
    .locals 2

    .prologue
    .line 1832404
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1832405
    invoke-static {v0}, LX/31Z;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    check-cast v0, LX/31Z;

    invoke-virtual {v0}, LX/31Z;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public static getFeedback(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)Lcom/facebook/graphql/model/GraphQLFeedback;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1832406
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    .line 1832407
    const/4 v0, 0x0

    .line 1832408
    :goto_0
    return-object v0

    .line 1832409
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832410
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1832411
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1832412
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832413
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1832414
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0

    .line 1832415
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832416
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1832417
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    goto :goto_0
.end method

.method public static getTrackingCodes(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)LX/162;
    .locals 1

    .prologue
    .line 1832418
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->g()Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    return-object v0
.end method

.method private h()LX/20Z;
    .locals 1

    .prologue
    .line 1832419
    new-instance v0, LX/BvY;

    invoke-direct {v0, p0}, LX/BvY;-><init>(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)V

    return-object v0
.end method

.method private i()LX/21M;
    .locals 1

    .prologue
    .line 1832420
    new-instance v0, LX/BvZ;

    invoke-direct {v0, p0}, LX/BvZ;-><init>(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)V

    return-object v0
.end method

.method public static j(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)V
    .locals 4

    .prologue
    .line 1832421
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getFeedback(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1832422
    if-nez v0, :cond_0

    .line 1832423
    :goto_0
    return-void

    .line 1832424
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->d:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v0

    move-object v1, v0

    .line 1832425
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->d:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1832426
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v2, "like_main"

    invoke-virtual {v0, v2}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1832427
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->C:LX/21M;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v3}, LX/21M;->a(Landroid/view/View;LX/1zt;LX/0Ve;)V

    .line 1832428
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setReaction(LX/1zt;)V

    goto :goto_0

    .line 1832429
    :cond_2
    sget-object v0, LX/1zt;->c:LX/1zt;

    move-object v1, v0

    goto :goto_1
.end method

.method public static k(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)V
    .locals 6

    .prologue
    .line 1832430
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3RX;

    const-string v1, "share"

    invoke-virtual {v0, v1}, LX/3RX;->a(Ljava/lang/String;)V

    .line 1832431
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->u:LX/1Kf;

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->v:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v4, LX/21D;->FULLSCREEN_VIDEO_PLAYER:LX/21D;

    const-string v5, "fullscreenRichVideoPlayer"

    invoke-interface {v0, v3, v4, v5}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getBaseContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v1, v2, v0, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    .line 1832432
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1832433
    invoke-static {p1}, LX/393;->a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->y:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1832434
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->getFeedback(Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1832435
    if-eqz v0, :cond_0

    .line 1832436
    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)V

    .line 1832437
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, v1}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setVisibility(I)V

    .line 1832438
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-virtual {v0, v1}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setVisibility(I)V

    .line 1832439
    :goto_0
    return-void

    .line 1832440
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setVisibility(I)V

    .line 1832441
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->x:Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;

    invoke-virtual {v0, v2}, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1832442
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->w:Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ReactionsFeedbackBasePlugin;->f:LX/20K;

    invoke-static {v0, v1}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V

    .line 1832443
    return-void
.end method
