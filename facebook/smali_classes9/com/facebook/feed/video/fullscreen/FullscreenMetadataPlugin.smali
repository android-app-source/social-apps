.class public Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;
.super LX/2oy;
.source ""


# instance fields
.field public a:Z

.field public b:LX/0xX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:Lcom/facebook/facecastdisplay/LiveMetadataView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1831307
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831308
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1831314
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831315
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1831309
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831310
    const-class v0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1831311
    const v0, 0x7f030739

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1831312
    const v0, 0x7f0d134c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/LiveMetadataView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->c:Lcom/facebook/facecastdisplay/LiveMetadataView;

    .line 1831313
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1831302
    invoke-static {p0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 1831303
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1831304
    :cond_0
    :goto_0
    return-object v0

    .line 1831305
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bb()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1831306
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;

    invoke-static {v0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v0

    check-cast v0, LX/0xX;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->b:LX/0xX;

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1831300
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->c:Lcom/facebook/facecastdisplay/LiveMetadataView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveMetadataView;->setVisibility(I)V

    .line 1831301
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 1831298
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->c:Lcom/facebook/facecastdisplay/LiveMetadataView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveMetadataView;->setVisibility(I)V

    .line 1831299
    return-void
.end method

.method private setFollowVideosButton(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    .line 1831266
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1831267
    if-nez v0, :cond_1

    .line 1831268
    :cond_0
    :goto_0
    return-void

    .line 1831269
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1831270
    if-eqz v1, :cond_0

    .line 1831271
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 1831272
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1831273
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->bh()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->a:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 1831274
    :goto_1
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->c:Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v2, v0, v1}, Lcom/facebook/facecastdisplay/LiveMetadataView;->a(Lcom/facebook/graphql/model/GraphQLActor;ZLjava/lang/String;)V

    goto :goto_0

    .line 1831275
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private setMetadata(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1831289
    invoke-static {p1}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 1831290
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->c:Lcom/facebook/facecastdisplay/LiveMetadataView;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v3, v0}, Lcom/facebook/facecastdisplay/LiveMetadataView;->setTitle(Ljava/lang/String;)V

    .line 1831291
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->c:Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveMetadataView;->g()V

    .line 1831292
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->c:Lcom/facebook/facecastdisplay/LiveMetadataView;

    invoke-static {p1}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/facecastdisplay/LiveMetadataView;->setSubtitle(Ljava/lang/String;)V

    .line 1831293
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->c:Lcom/facebook/facecastdisplay/LiveMetadataView;

    if-eqz v2, :cond_0

    invoke-static {v2}, LX/1xl;->b(Lcom/facebook/graphql/model/GraphQLActor;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveMetadataView;->setProfilePicture(Ljava/lang/String;)V

    .line 1831294
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->b:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1831295
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->setFollowVideosButton(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1831296
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 1831297
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1831276
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1831277
    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    if-eqz v1, :cond_1

    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->f:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 1831278
    :goto_0
    if-nez p2, :cond_2

    .line 1831279
    if-eqz v0, :cond_0

    .line 1831280
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1831281
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->setMetadata(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1831282
    :cond_0
    :goto_1
    return-void

    .line 1831283
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 1831284
    :cond_2
    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    .line 1831285
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->g()V

    .line 1831286
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1831287
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->setMetadata(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1

    .line 1831288
    :cond_3
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FullscreenMetadataPlugin;->h()V

    goto :goto_1
.end method
