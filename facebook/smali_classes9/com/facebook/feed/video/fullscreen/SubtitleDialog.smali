.class public Lcom/facebook/feed/video/fullscreen/SubtitleDialog;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Landroid/content/DialogInterface$OnDismissListener;

.field public n:LX/Bu6;

.field public o:Ljava/lang/String;

.field public p:LX/3J2;

.field public q:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public r:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1832874
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    .line 1832875
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/DialogInterface$OnDismissListener;LX/Bu6;LX/0Px;Lcom/facebook/prefs/shared/FbSharedPreferences;)Lcom/facebook/feed/video/fullscreen/SubtitleDialog;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Landroid/content/DialogInterface$OnDismissListener;",
            "LX/Bu6;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")",
            "Lcom/facebook/feed/video/fullscreen/SubtitleDialog;"
        }
    .end annotation

    .prologue
    .line 1832876
    new-instance v0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    invoke-direct {v0}, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;-><init>()V

    .line 1832877
    iput-object p0, v0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->o:Ljava/lang/String;

    .line 1832878
    move-object v0, v0

    .line 1832879
    iput-object p4, v0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 1832880
    move-object v0, v0

    .line 1832881
    iput-object p2, v0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->n:LX/Bu6;

    .line 1832882
    move-object v0, v0

    .line 1832883
    iput-object p1, v0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->m:Landroid/content/DialogInterface$OnDismissListener;

    .line 1832884
    move-object v0, v0

    .line 1832885
    iput-object p3, v0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->q:LX/0Px;

    .line 1832886
    move-object v0, v0

    .line 1832887
    return-object v0
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    invoke-static {p0}, LX/3J2;->a(LX/0QB;)LX/3J2;

    move-result-object p0

    check-cast p0, LX/3J2;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->p:LX/3J2;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 1832888
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    .line 1832889
    const-class v0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->a(Ljava/lang/Class;LX/02k;)V

    .line 1832890
    new-instance v0, LX/Bvo;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->q:LX/0Px;

    const v2, 0x7f080d88

    invoke-virtual {p0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, LX/Bvo;-><init>(Lcom/facebook/feed/video/fullscreen/SubtitleDialog;LX/0Px;Ljava/lang/String;)V

    .line 1832891
    new-instance v1, LX/0ju;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f080d86

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    .line 1832892
    iget-object v2, v0, LX/Bvo;->c:[Ljava/lang/String;

    move-object v2, v2

    .line 1832893
    iget v3, v0, LX/Bvo;->d:I

    new-instance v4, LX/Bvn;

    invoke-direct {v4, p0, v0}, LX/Bvn;-><init>(Lcom/facebook/feed/video/fullscreen/SubtitleDialog;LX/Bvo;)V

    invoke-virtual {v1, v2, v3, v4}, LX/0ju;->a([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080d87

    new-instance v2, LX/Bvm;

    invoke-direct {v2, p0}, LX/Bvm;-><init>(Lcom/facebook/feed/video/fullscreen/SubtitleDialog;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1832894
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1832895
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->m:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1832896
    return-void
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1832897
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1832898
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/SubtitleDialog;->m:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1832899
    return-void
.end method
