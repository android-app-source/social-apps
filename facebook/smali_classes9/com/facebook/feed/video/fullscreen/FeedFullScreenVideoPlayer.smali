.class public Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/0hE;
.implements LX/3FT;


# instance fields
.field public A:LX/15m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/0hy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final C:LX/3It;

.field private final D:Lcom/facebook/video/player/RichVideoPlayer;

.field private final E:Landroid/media/AudioManager;

.field private final F:Landroid/view/Window;

.field private final G:Landroid/os/Handler;

.field private final H:LX/Btz;

.field public I:Lcom/facebook/video/player/RichVideoPlayer;

.field public J:LX/2pa;

.field public K:LX/395;

.field private L:LX/098;

.field public M:Landroid/view/ViewGroup;

.field private N:Landroid/view/WindowManager$LayoutParams;

.field public O:LX/394;

.field private P:J

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Z

.field private U:Z

.field private V:Z

.field public W:Z

.field public a:LX/094;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aa:Lcom/facebook/graphql/model/GraphQLStory;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private ab:I

.field private ac:I

.field private ad:Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;

.field private ae:LX/Bu0;

.field public af:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field public ag:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field public ah:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private ai:I

.field private final aj:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final ak:LX/Bur;

.field public final al:LX/AjN;

.field private final am:LX/1NP;

.field private final an:LX/1NN;

.field public ao:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16I;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ap:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/37Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public aq:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D7p;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private ar:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AVV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private as:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Hf;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private at:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7OZ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1CX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/feed/annotations/IsInNewPlayerOldUIClosedCaptioningGateKeeper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Or;
    .annotation runtime Lcom/facebook/feed/annotations/IsInNewPlayerNewUIClosedCaptioningGateKeeper;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Bug;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/2mZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/19m;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/3Gg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:Landroid/view/WindowManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/Ac6;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/19j;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/BwE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/AjP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/Bud;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/3H4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/2mn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1830081
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1830082
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1830212
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830213
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v1, -0x1

    const/4 v6, 0x0

    .line 1830176
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830177
    new-instance v0, LX/Btu;

    invoke-direct {v0, p0}, LX/Btu;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->C:LX/3It;

    .line 1830178
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->U:Z

    .line 1830179
    iput-object v6, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ah:Ljava/util/Map;

    .line 1830180
    iput v7, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ai:I

    .line 1830181
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aj:Ljava/util/Map;

    .line 1830182
    new-instance v0, LX/Btv;

    invoke-direct {v0, p0}, LX/Btv;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->al:LX/AjN;

    .line 1830183
    new-instance v0, LX/Btw;

    invoke-direct {v0, p0}, LX/Btw;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->am:LX/1NP;

    .line 1830184
    new-instance v0, LX/Btx;

    invoke-direct {v0, p0}, LX/Btx;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->an:LX/1NN;

    .line 1830185
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1830186
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ao:LX/0Ot;

    .line 1830187
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1830188
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ap:LX/0Ot;

    .line 1830189
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1830190
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aq:LX/0Ot;

    .line 1830191
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1830192
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ar:LX/0Ot;

    .line 1830193
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1830194
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->as:LX/0Ot;

    .line 1830195
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1830196
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->at:LX/0Ot;

    .line 1830197
    const-class v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1830198
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    .line 1830199
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->E:Landroid/media/AudioManager;

    .line 1830200
    const v0, 0x7f0311f9

    invoke-static {p1, v0, p0}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1830201
    const v0, 0x7f0d0917

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->D:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1830202
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->D:Lcom/facebook/video/player/RichVideoPlayer;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1830203
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v3, 0x2

    const v4, 0x1000788

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->N:Landroid/view/WindowManager$LayoutParams;

    .line 1830204
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->N:Landroid/view/WindowManager$LayoutParams;

    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 1830205
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->G:Landroid/os/Handler;

    .line 1830206
    new-instance v0, LX/Btz;

    invoke-direct {v0, p0}, LX/Btz;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->H:LX/Btz;

    .line 1830207
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->s:LX/BwE;

    sget-object v1, LX/0wD;->NEWSFEED:LX/0wD;

    const-string v2, "video_fullscreen_player"

    invoke-virtual {v0, v6, v1, v2}, LX/BwE;->a(LX/1Pf;LX/0wD;Ljava/lang/String;)LX/Bur;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ak:LX/Bur;

    .line 1830208
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->t:LX/AjP;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->al:LX/AjN;

    .line 1830209
    iput-object v1, v0, LX/AjP;->c:LX/AjN;

    .line 1830210
    return-void

    :cond_0
    move-object v0, v6

    .line 1830211
    goto :goto_0
.end method

.method private A()Z
    .locals 2

    .prologue
    .line 1830175
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private B()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1830171
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    if-nez v1, :cond_1

    .line 1830172
    :cond_0
    :goto_0
    return v0

    .line 1830173
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->w:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v2}, LX/395;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v1

    .line 1830174
    if-eqz v1, :cond_0

    iget-object v1, v1, LX/D6v;->a:LX/2oN;

    sget-object v2, LX/2oN;->NONE:LX/2oN;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;)D
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 1830165
    const-wide/16 v0, 0x0

    .line 1830166
    if-eqz p1, :cond_0

    .line 1830167
    invoke-static {p1}, LX/182;->i(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1830168
    if-eqz v2, :cond_0

    .line 1830169
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->y:LX/2mn;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, LX/2mn;->d(Lcom/facebook/feed/rows/core/props/FeedProps;F)D

    move-result-wide v0

    .line 1830170
    :cond_0
    return-wide v0
.end method

.method public static synthetic a(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;Lcom/facebook/feed/rows/core/props/FeedProps;)D
    .locals 2

    .prologue
    .line 1830164
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)D

    move-result-wide v0

    return-wide v0
.end method

.method private a(LX/395;Z)LX/2pa;
    .locals 13

    .prologue
    .line 1830094
    iget-object v0, p1, LX/395;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v2, v0

    .line 1830095
    if-eqz v2, :cond_6

    invoke-static {v2}, LX/1WF;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1830096
    :goto_0
    if-eqz v2, :cond_7

    if-eqz v0, :cond_7

    invoke-static {v0}, LX/17E;->i(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-nez v0, :cond_7

    const/4 v0, 0x1

    .line 1830097
    :goto_1
    iget-object v1, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v1, v1

    .line 1830098
    if-eqz v1, :cond_8

    .line 1830099
    iget-object v1, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v1, v1

    .line 1830100
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->x()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 1830101
    :goto_2
    if-eqz v1, :cond_9

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1830102
    :goto_3
    if-eqz v0, :cond_a

    invoke-static {v2}, LX/1WF;->e(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1830103
    :goto_4
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->k:LX/2mZ;

    .line 1830104
    iget-object v3, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v3, v3

    .line 1830105
    invoke-virtual {v1, v2, v3}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v1

    .line 1830106
    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->W:Z

    invoke-virtual {v1, p2, v2, v3}, LX/3Im;->a(ZZZ)Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v6

    .line 1830107
    const-wide v2, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    .line 1830108
    const/4 v5, 0x0

    .line 1830109
    const/4 v4, 0x1

    .line 1830110
    const/4 v1, 0x0

    .line 1830111
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 1830112
    iget-object v8, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v8, v8

    .line 1830113
    if-eqz v8, :cond_e

    .line 1830114
    iget-object v4, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v4, v4

    .line 1830115
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v4

    .line 1830116
    iget-object v5, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v5, v5

    .line 1830117
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v5

    .line 1830118
    iget-object v8, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v8, v8

    .line 1830119
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v8

    if-eqz v8, :cond_b

    .line 1830120
    const-wide v2, 0x3ffc71c720000000L    # 1.7777777910232544

    .line 1830121
    :cond_0
    :goto_5
    iget-object v4, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v4, v4

    .line 1830122
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->ba()LX/0Px;

    move-result-object v4

    .line 1830123
    if-eqz v4, :cond_1

    .line 1830124
    invoke-virtual {v7, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 1830125
    :cond_1
    iget-object v4, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v4, v4

    .line 1830126
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->s()Z

    move-result v5

    .line 1830127
    iget-object v4, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v4, v4

    .line 1830128
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLVideo;->t()Z

    move-result v4

    .line 1830129
    iget-object v8, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v8, v8

    .line 1830130
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v8

    if-eqz v8, :cond_d

    .line 1830131
    iget-object v1, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v1, v1

    .line 1830132
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->R()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v1

    move v11, v4

    move v12, v5

    move-wide v4, v2

    move v2, v11

    move v3, v12

    .line 1830133
    :goto_6
    iget-object v8, p1, LX/395;->i:LX/1bf;

    move-object v8, v8

    .line 1830134
    new-instance v9, LX/0P2;

    invoke-direct {v9}, LX/0P2;-><init>()V

    .line 1830135
    const-string v10, "ShowDeleteOptionKey"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v9, v10, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v3

    const-string v10, "ShowReportOptionKey"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v10, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v3, "SubtitlesLocalesKey"

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v3, "ShowLiveCommentDialogFragment"

    .line 1830136
    iget-boolean v7, p1, LX/395;->k:Z

    move v7, v7

    .line 1830137
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1830138
    if-eqz v1, :cond_2

    .line 1830139
    const-string v2, "BlurredCoverImageParamsKey"

    invoke-virtual {v9, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1830140
    :cond_2
    if-eqz v8, :cond_3

    .line 1830141
    const-string v1, "CoverImageParamsKey"

    invoke-virtual {v9, v1, v8}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1830142
    :cond_3
    iget-object v1, p1, LX/395;->p:LX/7DJ;

    move-object v1, v1

    .line 1830143
    if-eqz v1, :cond_4

    .line 1830144
    const-string v2, "SphericalViewportStateKey"

    invoke-virtual {v9, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1830145
    :cond_4
    iget-object v1, p1, LX/395;->g:LX/0P1;

    move-object v1, v1

    .line 1830146
    invoke-static {v1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(LX/0P1;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1830147
    invoke-static {v9, v1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(LX/0P2;LX/0P1;)V

    .line 1830148
    :cond_5
    :goto_7
    const-string v0, "VideoPlayerViewSizeKey"

    sget-object v1, LX/3HY;->REGULAR:LX/3HY;

    invoke-virtual {v9, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1830149
    new-instance v0, LX/2pZ;

    invoke-direct {v0}, LX/2pZ;-><init>()V

    .line 1830150
    iput-object v6, v0, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1830151
    move-object v0, v0

    .line 1830152
    invoke-virtual {v0, v4, v5}, LX/2pZ;->a(D)LX/2pZ;

    move-result-object v0

    invoke-virtual {v9}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    return-object v0

    .line 1830153
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1830154
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 1830155
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 1830156
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_3

    :cond_a
    move-object v0, v1

    .line 1830157
    goto/16 :goto_4

    .line 1830158
    :cond_b
    if-lez v5, :cond_0

    if-lez v4, :cond_0

    .line 1830159
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    int-to-double v8, v4

    mul-double/2addr v2, v8

    int-to-double v4, v5

    div-double/2addr v2, v4

    goto/16 :goto_5

    .line 1830160
    :cond_c
    if-eqz v0, :cond_5

    .line 1830161
    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v9, v1, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1830162
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830163
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_7

    :cond_d
    move v11, v4

    move v12, v5

    move-wide v4, v2

    move v2, v11

    move v3, v12

    goto/16 :goto_6

    :cond_e
    move v11, v4

    move v12, v5

    move-wide v4, v2

    move v2, v11

    move v3, v12

    goto/16 :goto_6
.end method

.method private a(ZLX/7DJ;)LX/7Jv;
    .locals 6
    .param p2    # LX/7DJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1830005
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v0

    .line 1830006
    if-gez v0, :cond_0

    move v0, v1

    .line 1830007
    :cond_0
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getLastStartPosition()I

    move-result v2

    .line 1830008
    if-gez v2, :cond_1

    move v2, v1

    .line 1830009
    :cond_1
    if-le v2, v0, :cond_2

    move v2, v0

    .line 1830010
    :cond_2
    new-instance v5, LX/7Ju;

    invoke-direct {v5}, LX/7Ju;-><init>()V

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->r()Z

    move-result v3

    if-nez v3, :cond_4

    move v3, v4

    .line 1830011
    :goto_0
    iput-boolean v3, v5, LX/7Ju;->a:Z

    .line 1830012
    move-object v3, v5

    .line 1830013
    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1830014
    iput-object v5, v3, LX/7Ju;->f:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1830015
    move-object v3, v3

    .line 1830016
    if-nez p1, :cond_3

    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v5}, Lcom/facebook/video/player/RichVideoPlayer;->s()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 1830017
    :cond_3
    :goto_1
    iput-boolean v4, v3, LX/7Ju;->b:Z

    .line 1830018
    move-object v3, v3

    .line 1830019
    iput v0, v3, LX/7Ju;->c:I

    .line 1830020
    move-object v0, v3

    .line 1830021
    iput v2, v0, LX/7Ju;->d:I

    .line 1830022
    move-object v0, v0

    .line 1830023
    iput-boolean v1, v0, LX/7Ju;->e:Z

    .line 1830024
    move-object v0, v0

    .line 1830025
    sget-object v1, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    .line 1830026
    iput-object v1, v0, LX/7Ju;->h:LX/04g;

    .line 1830027
    move-object v0, v0

    .line 1830028
    iput-object p2, v0, LX/7Ju;->i:LX/7DJ;

    .line 1830029
    move-object v0, v0

    .line 1830030
    invoke-virtual {v0}, LX/7Ju;->a()LX/7Jv;

    move-result-object v0

    return-object v0

    :cond_4
    move v3, v1

    goto :goto_0

    :cond_5
    move v4, v1

    goto :goto_1
.end method

.method private static a(LX/0P2;LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "LX/0P1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1830083
    const-string v0, "MultiShareGraphQLSubStoryPropsKey"

    const-string v1, "MultiShareGraphQLSubStoryPropsKey"

    invoke-virtual {p1, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1830084
    const-string v0, "MultiShareGraphQLSubStoryIndexKey"

    const-string v1, "MultiShareGraphQLSubStoryIndexKey"

    invoke-virtual {p1, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 1830085
    return-void
.end method

.method private a(LX/7Jv;LX/04G;)V
    .locals 13

    .prologue
    .line 1829951
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v0}, LX/395;->s()LX/162;

    move-result-object v1

    .line 1829952
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ah:Ljava/util/Map;

    if-eqz v0, :cond_0

    .line 1829953
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ah:Ljava/util/Map;

    const-string v2, "swipe_count"

    iget v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ai:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1829954
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v3

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v2}, LX/395;->y()Ljava/lang/String;

    move-result-object v4

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v2}, LX/395;->q()LX/04D;

    move-result-object v5

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v2}, LX/395;->o()LX/04g;

    move-result-object v2

    iget-object v6, v2, LX/04g;->value:Ljava/lang/String;

    iget v7, p1, LX/7Jv;->c:I

    iget v8, p1, LX/7Jv;->d:I

    iget-object v9, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->L:LX/098;

    iget-object v10, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ah:Ljava/util/Map;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829955
    iget-object v11, v2, LX/395;->e:LX/0JG;

    move-object v11, v11

    .line 1829956
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829957
    iget-object v12, v2, LX/395;->f:Ljava/lang/String;

    move-object v12, v12

    .line 1829958
    move-object v2, p2

    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1829959
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ah:Ljava/util/Map;

    .line 1829960
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ai:I

    .line 1829961
    return-void
.end method

.method private static a(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;LX/094;LX/1CX;LX/0Or;LX/0Or;Ljava/lang/Boolean;LX/Bug;LX/0Uh;LX/0ad;LX/0Or;LX/0SG;LX/2mZ;LX/19m;LX/3Gg;Landroid/view/WindowManager;LX/1C2;LX/Ac6;LX/1b4;LX/19j;LX/BwE;LX/AjP;LX/0bH;LX/Bud;Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/3H4;LX/2mn;Lcom/facebook/content/SecureContextHelper;LX/15m;LX/0hy;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;",
            "LX/094;",
            "LX/1CX;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/Bug;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/0SG;",
            "LX/2mZ;",
            "LX/19m;",
            "LX/3Gg;",
            "Landroid/view/WindowManager;",
            "LX/1C2;",
            "LX/Ac6;",
            "LX/1b4;",
            "LX/19j;",
            "LX/BwE;",
            "LX/AjP;",
            "LX/0bH;",
            "LX/Bud;",
            "Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;",
            "LX/3H4;",
            "LX/2mn;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/15m;",
            "LX/0hy;",
            "LX/0Ot",
            "<",
            "LX/16I;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/37Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/D7p;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/AVV;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3Hf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/7OZ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1830080
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a:LX/094;

    iput-object p2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b:LX/1CX;

    iput-object p3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->c:LX/0Or;

    iput-object p4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->d:LX/0Or;

    iput-object p5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->e:Ljava/lang/Boolean;

    iput-object p6, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->f:LX/Bug;

    iput-object p7, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->g:LX/0Uh;

    iput-object p8, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->h:LX/0ad;

    iput-object p9, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->i:LX/0Or;

    iput-object p10, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->j:LX/0SG;

    iput-object p11, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->k:LX/2mZ;

    iput-object p12, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->l:LX/19m;

    iput-object p13, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->m:LX/3Gg;

    iput-object p14, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->n:Landroid/view/WindowManager;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->p:LX/Ac6;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->q:LX/1b4;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->r:LX/19j;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->s:LX/BwE;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->t:LX/AjP;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->u:LX/0bH;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->v:LX/Bud;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->w:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->x:LX/3H4;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->y:LX/2mn;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->z:Lcom/facebook/content/SecureContextHelper;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->A:LX/15m;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->B:LX/0hy;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ao:LX/0Ot;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ap:LX/0Ot;

    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aq:LX/0Ot;

    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ar:LX/0Ot;

    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->as:LX/0Ot;

    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->at:LX/0Ot;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 38

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v36

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-static/range {v36 .. v36}, LX/094;->a(LX/0QB;)LX/094;

    move-result-object v3

    check-cast v3, LX/094;

    invoke-static/range {v36 .. v36}, LX/1CX;->a(LX/0QB;)LX/1CX;

    move-result-object v4

    check-cast v4, LX/1CX;

    const/16 v5, 0x149d

    move-object/from16 v0, v36

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x149c

    move-object/from16 v0, v36

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static/range {v36 .. v36}, LX/3Gf;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v7

    check-cast v7, Ljava/lang/Boolean;

    invoke-static/range {v36 .. v36}, LX/Bug;->a(LX/0QB;)LX/Bug;

    move-result-object v8

    check-cast v8, LX/Bug;

    invoke-static/range {v36 .. v36}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v9

    check-cast v9, LX/0Uh;

    invoke-static/range {v36 .. v36}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    const/16 v11, 0x122d

    move-object/from16 v0, v36

    invoke-static {v0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static/range {v36 .. v36}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v12

    check-cast v12, LX/0SG;

    const-class v13, LX/2mZ;

    move-object/from16 v0, v36

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/2mZ;

    invoke-static/range {v36 .. v36}, LX/19m;->a(LX/0QB;)LX/19m;

    move-result-object v14

    check-cast v14, LX/19m;

    invoke-static/range {v36 .. v36}, LX/3Gg;->a(LX/0QB;)LX/3Gg;

    move-result-object v15

    check-cast v15, LX/3Gg;

    invoke-static/range {v36 .. v36}, LX/0q4;->a(LX/0QB;)Landroid/view/WindowManager;

    move-result-object v16

    check-cast v16, Landroid/view/WindowManager;

    invoke-static/range {v36 .. v36}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object v17

    check-cast v17, LX/1C2;

    invoke-static/range {v36 .. v36}, LX/Ac6;->a(LX/0QB;)LX/Ac6;

    move-result-object v18

    check-cast v18, LX/Ac6;

    invoke-static/range {v36 .. v36}, LX/1b4;->a(LX/0QB;)LX/1b4;

    move-result-object v19

    check-cast v19, LX/1b4;

    invoke-static/range {v36 .. v36}, LX/19j;->a(LX/0QB;)LX/19j;

    move-result-object v20

    check-cast v20, LX/19j;

    const-class v21, LX/BwE;

    move-object/from16 v0, v36

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v21

    check-cast v21, LX/BwE;

    invoke-static/range {v36 .. v36}, LX/AjP;->a(LX/0QB;)LX/AjP;

    move-result-object v22

    check-cast v22, LX/AjP;

    invoke-static/range {v36 .. v36}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v23

    check-cast v23, LX/0bH;

    const-class v24, LX/Bud;

    move-object/from16 v0, v36

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v24

    check-cast v24, LX/Bud;

    invoke-static/range {v36 .. v36}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v25

    check-cast v25, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    invoke-static/range {v36 .. v36}, LX/3H4;->a(LX/0QB;)LX/3H4;

    move-result-object v26

    check-cast v26, LX/3H4;

    invoke-static/range {v36 .. v36}, LX/2mn;->a(LX/0QB;)LX/2mn;

    move-result-object v27

    check-cast v27, LX/2mn;

    invoke-static/range {v36 .. v36}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v28

    check-cast v28, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v36 .. v36}, LX/15b;->a(LX/0QB;)LX/15m;

    move-result-object v29

    check-cast v29, LX/15m;

    invoke-static/range {v36 .. v36}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v30

    check-cast v30, LX/0hy;

    const/16 v31, 0x2cc

    move-object/from16 v0, v36

    move/from16 v1, v31

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v31

    const/16 v32, 0x1317

    move-object/from16 v0, v36

    move/from16 v1, v32

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v32

    const/16 v33, 0x3851

    move-object/from16 v0, v36

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v33

    const/16 v34, 0x1bcb

    move-object/from16 v0, v36

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v34

    const/16 v35, 0x1363

    move-object/from16 v0, v36

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v35

    const/16 v37, 0x37fd

    invoke-static/range {v36 .. v37}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v36

    invoke-static/range {v2 .. v36}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;LX/094;LX/1CX;LX/0Or;LX/0Or;Ljava/lang/Boolean;LX/Bug;LX/0Uh;LX/0ad;LX/0Or;LX/0SG;LX/2mZ;LX/19m;LX/3Gg;Landroid/view/WindowManager;LX/1C2;LX/Ac6;LX/1b4;LX/19j;LX/BwE;LX/AjP;LX/0bH;LX/Bud;Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;LX/3H4;LX/2mn;Lcom/facebook/content/SecureContextHelper;LX/15m;LX/0hy;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    return-void
.end method

.method private static a(LX/0P1;)Z
    .locals 1

    .prologue
    .line 1830079
    if-eqz p0, :cond_0

    const-string v0, "MultiShareGraphQLSubStoryPropsKey"

    invoke-virtual {p0, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "MultiShareGraphQLSubStoryIndexKey"

    invoke-virtual {p0, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;LX/2pa;)V
    .locals 4

    .prologue
    .line 1830073
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ad:Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v2, LX/Btz;

    invoke-direct {v2, p0}, LX/Btz;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    invoke-virtual {v0, v1, p1, v2}, LX/3Ge;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;LX/7Lf;)Lcom/facebook/video/player/RichVideoPlayer;

    .line 1830074
    invoke-static {p1}, LX/393;->m(LX/2pa;)Z

    move-result v0

    .line 1830075
    iget-object v1, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->h:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    .line 1830076
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/7OE;->TOP:LX/7OE;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    .line 1830077
    :goto_0
    return-void

    .line 1830078
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1830039
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 1830040
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    invoke-static {v0}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v5

    .line 1830041
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    invoke-virtual {v0}, LX/2pa;->g()LX/2pZ;

    move-result-object v6

    .line 1830042
    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v4

    .line 1830043
    const-string v0, "GraphQLStoryProps"

    invoke-virtual {v6, v0, v4}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 1830044
    invoke-static {p1}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1830045
    const/4 v0, 0x0

    .line 1830046
    if-eqz v1, :cond_5

    .line 1830047
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->g()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v7

    .line 1830048
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v1

    .line 1830049
    if-eq v7, v1, :cond_4

    .line 1830050
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->as:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Hf;

    invoke-virtual {v0, v7, v1}, LX/3Hf;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->h:LX/0ad;

    sget-short v8, LX/0ws;->eM:S

    invoke-interface {v0, v8, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1830051
    :cond_0
    :goto_0
    return-void

    .line 1830052
    :cond_1
    new-instance v0, LX/2oH;

    invoke-direct {v0}, LX/2oH;-><init>()V

    .line 1830053
    iget-object v8, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v8, v8, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v8}, LX/2oH;->a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2oH;

    .line 1830054
    iput-object v1, v0, LX/2oH;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    .line 1830055
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 1830056
    iput-object v0, v6, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1830057
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->as:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Hf;

    invoke-virtual {v0, v7, v1}, LX/3Hf;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1830058
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v1, v0, :cond_3

    .line 1830059
    invoke-direct {p0, v4}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)D

    move-result-wide v8

    .line 1830060
    iput-wide v8, v6, LX/2pZ;->e:D

    .line 1830061
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ar:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVV;

    const-string v4, "transitioned to live"

    invoke-virtual {v0, v4, p1}, LX/AVV;->b(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v0, v1

    move v4, v2

    move v1, v2

    .line 1830062
    :goto_1
    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1830063
    invoke-static {v5}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    .line 1830064
    invoke-static {p1}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v5

    .line 1830065
    if-eqz v0, :cond_2

    if-eqz v5, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n()J

    move-result-wide v8

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->n()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-eqz v0, :cond_2

    move v3, v2

    .line 1830066
    :cond_2
    const-string v0, "DidBroadcastStatusChangeKey"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 1830067
    const-string v0, "DidScheduledLiveStartTimeChangeKey"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v6, v0, v2}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 1830068
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1830069
    invoke-virtual {v6}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    .line 1830070
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1830071
    if-eqz v1, :cond_0

    .line 1830072
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_PLAYER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    goto/16 :goto_0

    :cond_3
    move-object v0, v1

    move v4, v2

    move v1, v3

    goto :goto_1

    :cond_4
    move-object v0, v1

    move v4, v3

    move v1, v3

    goto :goto_1

    :cond_5
    move v1, v3

    move v4, v3

    goto :goto_1
.end method

.method public static a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;Z)V
    .locals 5

    .prologue
    .line 1830031
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->R:Z

    if-nez v0, :cond_0

    .line 1830032
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->R:Z

    .line 1830033
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a:LX/094;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v1}, LX/395;->y()Ljava/lang/String;

    move-result-object v1

    const p0, 0x1d0003

    .line 1830034
    invoke-static {v1}, LX/094;->b(Ljava/lang/String;)I

    move-result v3

    .line 1830035
    iget-object v2, v0, LX/094;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, p0, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1830036
    iget-object v4, v0, LX/094;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    if-eqz p1, :cond_1

    const/4 v2, 0x2

    :goto_0
    invoke-interface {v4, p0, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 1830037
    :cond_0
    return-void

    .line 1830038
    :cond_1
    const/4 v2, 0x3

    goto :goto_0
.end method

.method public static a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;ZLX/04g;LX/04G;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 1830345
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ae:LX/Bu0;

    if-eqz v0, :cond_0

    .line 1830346
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ae:LX/Bu0;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/2oa;)V

    .line 1830347
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->c(LX/395;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1830348
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1830349
    iget-object v1, v0, LX/395;->h:LX/3FT;

    move-object v0, v1

    .line 1830350
    if-eqz v0, :cond_6

    .line 1830351
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1830352
    iget-object v1, v0, LX/395;->h:LX/3FT;

    move-object v0, v1

    .line 1830353
    invoke-interface {v0}, LX/3FT;->d()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    const-class v1, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    move-object v1, v0

    .line 1830354
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, v1, Lcom/facebook/video/player/plugins/Video360Plugin;->N:Z

    .line 1830355
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->l:LX/19m;

    .line 1830356
    iget-boolean v2, v0, LX/19m;->h:Z

    move v0, v2

    .line 1830357
    if-eqz v0, :cond_1

    .line 1830358
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1830359
    if-eqz v0, :cond_1

    instance-of v2, v0, Landroid/app/Activity;

    if-eqz v2, :cond_1

    .line 1830360
    check-cast v0, Landroid/app/Activity;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1830361
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/video/player/plugins/Video360Plugin;->getViewportState()LX/7DJ;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(ZLX/7DJ;)LX/7Jv;

    move-result-object v0

    .line 1830362
    :goto_1
    invoke-static {p0, v5}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;Z)V

    .line 1830363
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v2, 0x1

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1830364
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1830365
    iget-object v2, v1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v1, v2

    .line 1830366
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1830367
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->p()V

    .line 1830368
    :cond_2
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    iget-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->U:Z

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->q()Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    :goto_2
    invoke-virtual {v2, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1830369
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->f:LX/Bug;

    invoke-virtual {v1}, LX/Bug;->a()V

    .line 1830370
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->B()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1830371
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->w:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v2}, LX/395;->y()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v1

    .line 1830372
    invoke-virtual {v1}, LX/D6v;->j()LX/BSQ;

    move-result-object v1

    .line 1830373
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->x:LX/3H4;

    sget-object v3, LX/BSX;->FULLSCREEN_TO_INLINE:LX/BSX;

    invoke-virtual {v2, v3, v1}, LX/3H4;->a(LX/BSX;LX/BSQ;)V

    .line 1830374
    :cond_3
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->u()Z

    move-result v1

    if-nez v1, :cond_4

    .line 1830375
    invoke-direct {p0, v0, p3}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(LX/7Jv;LX/04G;)V

    .line 1830376
    :cond_4
    iput-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1830377
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->O:LX/394;

    if-eqz v1, :cond_5

    .line 1830378
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->O:LX/394;

    invoke-interface {v1, p2, v0}, LX/394;->a(LX/04g;LX/7Jv;)V

    .line 1830379
    iput-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->O:LX/394;

    .line 1830380
    :cond_5
    iput-boolean v5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->Q:Z

    .line 1830381
    iput-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1830382
    iput-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->L:LX/098;

    .line 1830383
    iput-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    .line 1830384
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->w()V

    .line 1830385
    return-void

    .line 1830386
    :cond_6
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->D:Lcom/facebook/video/player/RichVideoPlayer;

    const-class v1, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    move-object v1, v0

    goto/16 :goto_0

    .line 1830387
    :cond_7
    invoke-direct {p0, p1, v4}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(ZLX/7DJ;)LX/7Jv;

    move-result-object v0

    goto/16 :goto_1

    .line 1830388
    :cond_8
    sget-object v1, LX/04g;->BY_USER:LX/04g;

    goto :goto_2
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 1830222
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    invoke-virtual {v0, p1}, LX/89k;->f(Lcom/facebook/graphql/model/GraphQLStory;)LX/89k;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 1830223
    iput-object v1, v0, LX/89k;->b:Ljava/lang/String;

    .line 1830224
    move-object v0, v0

    .line 1830225
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 1830226
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->B:LX/0hy;

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 1830227
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, LX/0nt;->a(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 1830342
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->A()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1830343
    :cond_0
    :goto_0
    return-void

    .line 1830344
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    if-gtz p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(LX/395;)V
    .locals 7

    .prologue
    .line 1830322
    iget-boolean v0, p1, LX/395;->q:Z

    move v0, v0

    .line 1830323
    if-eqz v0, :cond_0

    .line 1830324
    iget-object v0, p1, LX/395;->r:Ljava/lang/String;

    move-object v0, v0

    .line 1830325
    if-nez v0, :cond_1

    .line 1830326
    :cond_0
    :goto_0
    return-void

    .line 1830327
    :cond_1
    invoke-virtual {p1}, LX/395;->y()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 1830328
    iget-object v1, p1, LX/395;->r:Ljava/lang/String;

    move-object v1, v1

    .line 1830329
    sget-object v2, LX/0zS;->c:LX/0zS;

    .line 1830330
    new-instance v3, LX/7OX;

    invoke-direct {v3}, LX/7OX;-><init>()V

    move-object v3, v3

    .line 1830331
    const-string v4, "video_id"

    invoke-virtual {v3, v4, v0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 1830332
    const-string v4, "feed_story_render_location"

    invoke-virtual {v3, v4, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 1830333
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const/4 v4, 0x1

    .line 1830334
    iput-boolean v4, v3, LX/0zO;->p:Z

    .line 1830335
    move-object v3, v3

    .line 1830336
    const-wide/16 v5, 0xa

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 1830337
    move-object v1, v3

    .line 1830338
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->at:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7OZ;

    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCreationStoryQueryCallback()LX/0TF;

    move-result-object v2

    .line 1830339
    iget-object v3, v0, LX/7OZ;->a:LX/0tX;

    invoke-virtual {v3, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 1830340
    iget-object v4, v0, LX/7OZ;->b:Ljava/util/concurrent/Executor;

    invoke-static {v3, v2, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 1830341
    goto :goto_0
.end method

.method private c(LX/395;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1830313
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->l:LX/19m;

    .line 1830314
    iget-boolean v3, v0, LX/19m;->d:Z

    move v0, v3

    .line 1830315
    if-eqz v0, :cond_2

    .line 1830316
    iget-object v0, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v0, v0

    .line 1830317
    if-eqz v0, :cond_2

    .line 1830318
    iget-object v0, p1, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v0, v0

    .line 1830319
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1830320
    :goto_0
    invoke-static {p1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->d(LX/395;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    .line 1830321
    goto :goto_0
.end method

.method private static d(LX/395;)Z
    .locals 1

    .prologue
    .line 1830311
    iget-object v0, p0, LX/395;->h:LX/3FT;

    move-object v0, v0

    .line 1830312
    instance-of v0, v0, LX/7gN;

    return v0
.end method

.method private e(I)V
    .locals 2

    .prologue
    .line 1830304
    iget v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ab:I

    if-ne v0, p1, :cond_1

    .line 1830305
    :cond_0
    :goto_0
    return-void

    .line 1830306
    :cond_1
    iput p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ab:I

    .line 1830307
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->V:Z

    if-eqz v0, :cond_0

    .line 1830308
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1830309
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    goto :goto_0

    .line 1830310
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/7OE;->TOP:LX/7OE;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    goto :goto_0
.end method

.method private static e(LX/395;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1830299
    iget-object v1, p0, LX/395;->h:LX/3FT;

    move-object v1, v1

    .line 1830300
    if-eqz v1, :cond_0

    .line 1830301
    invoke-interface {v1}, LX/3FT;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    .line 1830302
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->p()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1830303
    :cond_0
    return v0
.end method

.method private getCreationStoryQueryCallback()LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/video/protocol/VideoQueryModels$VideoCreationStoryFromVideoQueryModel;",
            ">;>;>;"
        }
    .end annotation

    .prologue
    .line 1830298
    new-instance v0, LX/Bty;

    invoke-direct {v0, p0}, LX/Bty;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 1830270
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->A:LX/15m;

    invoke-interface {v0}, LX/15m;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1830271
    :cond_0
    :goto_0
    return-void

    .line 1830272
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ap:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/37Y;

    invoke-virtual {v0}, LX/37Y;->g()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1830273
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    .line 1830274
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1830275
    iget-object v1, v0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v0, v1

    .line 1830276
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->bn()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->A:LX/15m;

    invoke-interface {v0}, LX/15m;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1830277
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    .line 1830278
    if-eqz v1, :cond_0

    iget-object v0, v1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    .line 1830279
    invoke-static {v1}, LX/393;->a(LX/2pa;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 1830280
    if-eqz v2, :cond_0

    .line 1830281
    new-instance v0, LX/7IL;

    invoke-direct {v0}, LX/7IL;-><init>()V

    move-object v0, v0

    .line 1830282
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v3

    .line 1830283
    iput v3, v0, LX/7IL;->c:I

    .line 1830284
    move-object v3, v0

    .line 1830285
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1830286
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1830287
    iput-object v0, v3, LX/7IL;->b:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1830288
    move-object v0, v3

    .line 1830289
    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1830290
    iput-object v1, v0, LX/7IL;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1830291
    move-object v1, v0

    .line 1830292
    iget-object v0, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1830293
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b(Lcom/facebook/graphql/model/GraphQLStory;)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1830294
    iput-object v0, v1, LX/7IL;->d:Landroid/app/PendingIntent;

    .line 1830295
    move-object v0, v1

    .line 1830296
    new-instance v1, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;

    invoke-direct {v1, v0}, Lcom/facebook/video/backgroundplay/control/model/ControlInitData;-><init>(LX/7IL;)V

    move-object v0, v1

    .line 1830297
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->A:LX/15m;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v2, v0}, LX/15m;->a(Landroid/content/Context;Lcom/facebook/video/backgroundplay/control/model/ControlInitData;)V

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 1830264
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1830265
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1830266
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1830267
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->addFlags(I)V

    .line 1830268
    :cond_0
    :goto_0
    return-void

    .line 1830269
    :cond_1
    const/16 v0, 0x504

    invoke-virtual {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method private l()V
    .locals 2

    .prologue
    .line 1830258
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 1830259
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1830260
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1830261
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 1830262
    :cond_0
    :goto_0
    return-void

    .line 1830263
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ac:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public static synthetic m(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)I
    .locals 2

    .prologue
    .line 1830257
    iget v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ai:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ai:I

    return v0
.end method

.method public static m(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 5

    .prologue
    .line 1830248
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    .line 1830249
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 1830250
    and-int/lit16 v0, v0, 0x400

    if-nez v0, :cond_0

    .line 1830251
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->k()V

    .line 1830252
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->G:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer$5;

    invoke-direct {v1, p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer$5;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    const-wide/16 v2, 0x7d0

    const v4, -0x58f88535

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1830253
    return-void

    .line 1830254
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    .line 1830255
    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_0

    .line 1830256
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->k()V

    goto :goto_0
.end method

.method public static n(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 3

    .prologue
    .line 1830243
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 1830244
    :cond_0
    :goto_0
    return-void

    .line 1830245
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->t:LX/AjP;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aa:Lcom/facebook/graphql/model/GraphQLStory;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/AjP;->a(Lcom/facebook/graphql/model/GraphQLStory;Z)V

    .line 1830246
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->u:LX/0bH;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->am:LX/1NP;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1830247
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->u:LX/0bH;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->an:LX/1NN;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0
.end method

.method public static o$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 2

    .prologue
    .line 1830239
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->t:LX/AjP;

    invoke-virtual {v0}, LX/AjP;->b()V

    .line 1830240
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->u:LX/0bH;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->am:LX/1NP;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1830241
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->u:LX/0bH;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->an:LX/1NN;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1830242
    return-void
.end method

.method private p()V
    .locals 5

    .prologue
    .line 1830228
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->P:J

    sub-long/2addr v0, v2

    .line 1830229
    const-wide/16 v2, 0xbb8

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 1830230
    :goto_0
    return-void

    .line 1830231
    :cond_0
    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    const-string v0, "111991945928016"

    move-object v1, v0

    .line 1830232
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    const-string v2, "video_id"

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1830233
    iget-object v4, v3, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v3, v4

    .line 1830234
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLVideo;->G()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gt;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gt;

    move-result-object v0

    .line 1830235
    iput-object v1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 1830236
    move-object v0, v0

    .line 1830237
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 1830238
    :cond_1
    const-string v0, "177744329299957"

    move-object v1, v0

    goto :goto_1
.end method

.method private q()V
    .locals 9

    .prologue
    .line 1830086
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    .line 1830087
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCurrentVolume()I

    move-result v5

    .line 1830088
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v0}, LX/395;->s()LX/162;

    move-result-object v1

    .line 1830089
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v6, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v6}, LX/395;->y()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v7}, LX/395;->q()LX/04D;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->L:LX/098;

    invoke-virtual/range {v0 .. v8}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1830090
    if-nez v5, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->S:Z

    if-nez v0, :cond_0

    .line 1830091
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->S:Z

    .line 1830092
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->s()V

    .line 1830093
    :cond_0
    return-void
.end method

.method private r()V
    .locals 10

    .prologue
    .line 1830214
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    .line 1830215
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCurrentVolume()I

    move-result v9

    .line 1830216
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v0}, LX/395;->s()LX/162;

    move-result-object v1

    .line 1830217
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCurrentVolume()I

    move-result v5

    iget-object v6, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v6}, LX/395;->y()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v7}, LX/395;->q()LX/04D;

    move-result-object v7

    iget-object v8, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->L:LX/098;

    invoke-virtual/range {v0 .. v8}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;IILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1830218
    if-lez v9, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->S:Z

    if-eqz v0, :cond_0

    .line 1830219
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->S:Z

    .line 1830220
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->s()V

    .line 1830221
    :cond_0
    return-void
.end method

.method private s()V
    .locals 8

    .prologue
    .line 1829769
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v4

    .line 1829770
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v0}, LX/395;->s()LX/162;

    move-result-object v1

    .line 1829771
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->S:Z

    if-eqz v0, :cond_0

    .line 1829772
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v6}, LX/395;->q()LX/04D;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->L:LX/098;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->a(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    .line 1829773
    :goto_0
    return-void

    .line 1829774
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    sget-object v3, LX/04g;->BY_USER:LX/04g;

    iget-object v3, v3, LX/04g;->value:Ljava/lang/String;

    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v5}, LX/395;->y()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v6}, LX/395;->q()LX/04D;

    move-result-object v6

    iget-object v7, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->L:LX/098;

    invoke-virtual/range {v0 .. v7}, LX/1C2;->b(LX/0lF;LX/04G;Ljava/lang/String;ILjava/lang/String;LX/04D;LX/098;)LX/1C2;

    goto :goto_0
.end method

.method private t()V
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 1829935
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v1}, LX/395;->p()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v1}, LX/395;->p()I

    move-result v7

    .line 1829936
    :goto_0
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v1}, LX/395;->u()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v0}, LX/395;->u()I

    move-result v8

    .line 1829937
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v0}, LX/395;->s()LX/162;

    move-result-object v1

    .line 1829938
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o:LX/1C2;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getPlayerType()LX/04G;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829939
    iget-object v4, v3, LX/395;->d:Lcom/facebook/video/analytics/VideoPlayerInfo;

    move-object v3, v4

    .line 1829940
    iget-object v4, v3, Lcom/facebook/video/analytics/VideoPlayerInfo;->a:LX/04G;

    move-object v3, v4

    .line 1829941
    iget-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v4}, LX/395;->y()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v5}, LX/395;->q()LX/04D;

    move-result-object v5

    iget-object v6, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v6}, LX/395;->o()LX/04g;

    move-result-object v6

    iget-object v6, v6, LX/04g;->value:Ljava/lang/String;

    iget-object v9, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->L:LX/098;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829942
    iget-object v12, v11, LX/395;->e:LX/0JG;

    move-object v11, v12

    .line 1829943
    iget-object v12, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829944
    iget-object p0, v12, LX/395;->f:Ljava/lang/String;

    move-object v12, p0

    .line 1829945
    invoke-virtual/range {v0 .. v12}, LX/1C2;->a(LX/0lF;LX/04G;LX/04G;Ljava/lang/String;LX/04D;Ljava/lang/String;IILX/098;Ljava/util/Map;LX/0JG;Ljava/lang/String;)LX/1C2;

    .line 1829946
    return-void

    :cond_0
    move v7, v0

    .line 1829947
    goto :goto_0

    :cond_1
    move v8, v0

    .line 1829948
    goto :goto_1
.end method

.method private u()Z
    .locals 1

    .prologue
    .line 1829934
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ap:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/37Y;

    invoke-virtual {v0}, LX/37Y;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ap:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/37Y;

    invoke-virtual {v0}, LX/37Y;->g()LX/38p;

    move-result-object v0

    invoke-virtual {v0}, LX/38p;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()V
    .locals 2

    .prologue
    .line 1829927
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 1829928
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->k()V

    .line 1829929
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->m(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    .line 1829930
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->x()V

    .line 1829931
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aj:Ljava/util/Map;

    invoke-static {p0, v0, v1}, LX/7QU;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/Map;)V

    .line 1829932
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1829933
    :cond_0
    return-void
.end method

.method private w()V
    .locals 2

    .prologue
    .line 1829918
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1829919
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->G:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1829920
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->l()V

    .line 1829921
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->aj:Ljava/util/Map;

    invoke-static {p0, v0, v1}, LX/7QU;->b(Landroid/view/View;Landroid/view/ViewGroup;Ljava/util/Map;)V

    .line 1829922
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    if-ne v0, v1, :cond_1

    .line 1829923
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->y(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    .line 1829924
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1829925
    :cond_0
    :goto_0
    return-void

    .line 1829926
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->n:Landroid/view/WindowManager;

    invoke-interface {v0, p0}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    goto :goto_0
.end method

.method private x()V
    .locals 5

    .prologue
    .line 1829913
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1829914
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getBottom()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1829915
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1829916
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->G:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer$7;

    invoke-direct {v1, p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer$7;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    const-wide/16 v2, 0x12c

    const v4, -0x6aa8ec67

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 1829917
    return-void
.end method

.method public static y(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 1829908
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1829909
    iget v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v1, v2, :cond_0

    .line 1829910
    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1829911
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1829912
    :cond_0
    return-void
.end method

.method private z()Z
    .locals 2

    .prologue
    .line 1829907
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v0, v1}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final synthetic a(LX/394;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1829904
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->O:LX/394;

    .line 1829905
    move-object v0, p0

    .line 1829906
    return-object v0
.end method

.method public final a(LX/395;)V
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 1829801
    iget-boolean v0, p1, LX/395;->l:Z

    move v10, v0

    .line 1829802
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->Q:Z

    if-eqz v0, :cond_1

    .line 1829803
    :cond_0
    :goto_0
    return-void

    .line 1829804
    :cond_1
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829805
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    iget-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->T:Z

    invoke-direct {p0, v0, v1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a(LX/395;Z)LX/2pa;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    .line 1829806
    new-instance v0, LX/AnR;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v2, v2, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-direct {v0, v1, v2}, LX/AnR;-><init>(LX/395;Lcom/facebook/video/engine/VideoPlayerParams;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->L:LX/098;

    .line 1829807
    iput-boolean v9, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->R:Z

    .line 1829808
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->e(LX/395;)Z

    move-result v11

    .line 1829809
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    if-eqz v0, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_2

    .line 1829810
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->F:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ac:I

    .line 1829811
    :cond_2
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->v()V

    .line 1829812
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->n(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    .line 1829813
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a:LX/094;

    invoke-virtual {p1}, LX/395;->y()Ljava/lang/String;

    move-result-object v1

    .line 1829814
    iget-object v2, v0, LX/094;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x1d0003

    invoke-static {v1}, LX/094;->b(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "reuse_player_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1829815
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a:LX/094;

    invoke-virtual {p1}, LX/395;->y()Ljava/lang/String;

    move-result-object v1

    .line 1829816
    iget-object v2, v0, LX/094;->a:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v3, 0x1d0003

    invoke-static {v1}, LX/094;->b(Ljava/lang/String;)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "new_player_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v3, v4, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 1829817
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->j:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->P:J

    .line 1829818
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829819
    iget-object v1, v0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v12, v1

    .line 1829820
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVideo;->aW()Z

    move-result v0

    if-eqz v0, :cond_e

    move v7, v8

    .line 1829821
    :goto_1
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v0

    if-nez v0, :cond_f

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVideo;->af()Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->m:LX/3Gg;

    .line 1829822
    iget-object v1, v0, LX/3Gg;->a:LX/0ad;

    sget-short v2, LX/0ws;->dH:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    move v0, v1

    .line 1829823
    if-eqz v0, :cond_f

    move v0, v8

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->W:Z

    .line 1829824
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->f:LX/Bug;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829825
    iget-object v2, v1, LX/395;->h:LX/3FT;

    move-object v1, v2

    .line 1829826
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    sget-object v4, LX/Bue;->FULLSCREEN:LX/Bue;

    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->H:LX/Btz;

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object v2, p0

    invoke-virtual/range {v0 .. v6}, LX/Bug;->a(LX/3FT;LX/3FT;LX/2pa;LX/Bue;LX/7Lf;Ljava/lang/Boolean;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1829827
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p1}, LX/395;->t()LX/04g;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 1829828
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->C:LX/3It;

    .line 1829829
    iput-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1829830
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p1}, LX/395;->q()LX/04D;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1829831
    new-instance v0, LX/Bu0;

    invoke-direct {v0, p0}, LX/Bu0;-><init>(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ae:LX/Bu0;

    .line 1829832
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ae:LX/Bu0;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/2oa;)V

    .line 1829833
    iput-boolean v8, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->Q:Z

    .line 1829834
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCurrentVolume()I

    move-result v0

    if-nez v0, :cond_10

    move v0, v8

    :goto_3
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->S:Z

    .line 1829835
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->O:LX/394;

    if-eqz v0, :cond_3

    .line 1829836
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->O:LX/394;

    .line 1829837
    iget-object v1, p1, LX/395;->j:LX/04g;

    move-object v1, v1

    .line 1829838
    invoke-interface {v0, v1}, LX/394;->a(LX/04g;)V

    .line 1829839
    :cond_3
    if-eqz v10, :cond_4

    .line 1829840
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ah:Ljava/util/Map;

    .line 1829841
    iput v9, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ai:I

    .line 1829842
    :cond_4
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    invoke-virtual {v0}, LX/2pa;->d()Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v0, v0, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0}, Lcom/facebook/video/engine/VideoPlayerParams;->b()Z

    move-result v0

    if-eqz v0, :cond_11

    move v1, v8

    .line 1829843
    :goto_4
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ab:I

    .line 1829844
    if-nez v1, :cond_12

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz v7, :cond_12

    :cond_5
    move v0, v8

    :goto_5
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->V:Z

    .line 1829845
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->V:Z

    if-eqz v0, :cond_13

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_13

    .line 1829846
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/7OE;->TOP:LX/7OE;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    .line 1829847
    :goto_6
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1829848
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->v:LX/Bud;

    invoke-virtual {v0, v10}, LX/Bud;->a(Z)Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ad:Lcom/facebook/feed/video/fullscreen/FullscreenRichVideoPlayerPluginSelector;

    .line 1829849
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1829850
    iget-object v2, v0, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v0, v2

    .line 1829851
    iget-object v2, v0, LX/2pb;->e:LX/2pg;

    .line 1829852
    iget-boolean v0, v2, LX/2pg;->d:Z

    move v2, v0

    .line 1829853
    move v2, v2

    .line 1829854
    if-eqz v11, :cond_6

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829855
    iget-object v3, v0, LX/395;->a:Lcom/facebook/graphql/model/GraphQLVideo;

    move-object v0, v3

    .line 1829856
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLVideo;->aa()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829857
    iget-object v3, v0, LX/395;->j:LX/04g;

    move-object v0, v3

    .line 1829858
    sget-object v3, LX/04g;->BY_USER:LX/04g;

    if-ne v0, v3, :cond_7

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->r:LX/19j;

    iget-boolean v0, v0, LX/19j;->v:Z

    if-eqz v0, :cond_7

    .line 1829859
    :cond_6
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p1}, LX/395;->p()I

    move-result v3

    sget-object v4, LX/04g;->BY_INLINE_FULLSCREEN_TRANSITION:LX/04g;

    invoke-virtual {v0, v3, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1829860
    :cond_7
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->c(LX/395;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1829861
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829862
    iget-object v3, v0, LX/395;->h:LX/3FT;

    move-object v0, v3

    .line 1829863
    if-eqz v0, :cond_14

    .line 1829864
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829865
    iget-object v3, v0, LX/395;->h:LX/3FT;

    move-object v0, v3

    .line 1829866
    invoke-interface {v0}, LX/3FT;->d()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    const-class v3, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    .line 1829867
    :goto_7
    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/facebook/video/player/plugins/Video360Plugin;->N:Z

    .line 1829868
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->l:LX/19m;

    .line 1829869
    iget-boolean v3, v0, LX/19m;->h:Z

    move v0, v3

    .line 1829870
    if-eqz v0, :cond_8

    .line 1829871
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 1829872
    if-eqz v0, :cond_8

    instance-of v3, v0, Landroid/app/Activity;

    if-eqz v3, :cond_8

    .line 1829873
    check-cast v0, Landroid/app/Activity;

    const/4 v3, 0x4

    invoke-virtual {v0, v3}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 1829874
    :cond_8
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ap:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/37Y;

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v4}, LX/395;->q()LX/04D;

    move-result-object v4

    invoke-static {v3, v4}, LX/7JO;->a(LX/2pa;LX/04D;)LX/7JN;

    move-result-object v3

    iget-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v4, v4, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    invoke-virtual {v0, v3, v4}, LX/37Y;->a(LX/7JN;Lcom/facebook/video/engine/VideoPlayerParams;)Z

    move-result v0

    .line 1829875
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v3

    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVideo;->r()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v4

    invoke-static {v3, v4}, LX/3In;->a(ZLcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v3

    .line 1829876
    invoke-static {v12}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLVideo;)Z

    move-result v4

    .line 1829877
    if-nez v0, :cond_9

    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->B()Z

    move-result v5

    if-nez v5, :cond_9

    if-nez v3, :cond_9

    if-nez v4, :cond_9

    .line 1829878
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1829879
    iget-object v4, p1, LX/395;->j:LX/04g;

    move-object v4, v4

    .line 1829880
    invoke-virtual {p1}, LX/395;->u()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;I)V

    .line 1829881
    :cond_9
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->B()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 1829882
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->w:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    invoke-virtual {v4}, LX/395;->y()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->e(Ljava/lang/String;)LX/D6v;

    move-result-object v3

    .line 1829883
    invoke-virtual {v3}, LX/D6v;->j()LX/BSQ;

    move-result-object v3

    .line 1829884
    iget-object v4, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->x:LX/3H4;

    sget-object v5, LX/BSX;->INLINE_TO_FULLSCREEN:LX/BSX;

    invoke-virtual {v4, v5, v3}, LX/3H4;->a(LX/BSX;LX/BSQ;)V

    .line 1829885
    :cond_a
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v3, :cond_0

    .line 1829886
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v4, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v3, v9, v4}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1829887
    invoke-virtual {v12}, Lcom/facebook/graphql/model/GraphQLVideo;->ag()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    invoke-virtual {v3}, LX/2pa;->b()Z

    move-result v3

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->q:LX/1b4;

    invoke-virtual {v3}, LX/1b4;->H()LX/6Ri;

    move-result-object v3

    .line 1829888
    iget-object v4, v3, LX/6Ri;->c:Ljava/lang/String;

    const-string v5, "letterbox"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    move v3, v4

    .line 1829889
    if-nez v3, :cond_b

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->J:LX/2pa;

    iget-object v1, v1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-boolean v1, v1, Lcom/facebook/video/engine/VideoPlayerParams;->k:Z

    if-nez v1, :cond_b

    move v9, v8

    .line 1829890
    :cond_b
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v9}, Lcom/facebook/video/player/RichVideoPlayer;->setShouldCropToFit(Z)V

    .line 1829891
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCurrentVolume()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b(I)V

    .line 1829892
    if-eqz v11, :cond_c

    if-eqz v2, :cond_c

    .line 1829893
    invoke-static {p0, v8}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;Z)V

    .line 1829894
    :cond_c
    if-nez v0, :cond_d

    .line 1829895
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->t()V

    .line 1829896
    :cond_d
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b(LX/395;)V

    goto/16 :goto_0

    :cond_e
    move v7, v9

    .line 1829897
    goto/16 :goto_1

    :cond_f
    move v0, v9

    .line 1829898
    goto/16 :goto_2

    :cond_10
    move v0, v9

    .line 1829899
    goto/16 :goto_3

    :cond_11
    move v1, v9

    .line 1829900
    goto/16 :goto_4

    :cond_12
    move v0, v9

    .line 1829901
    goto/16 :goto_5

    .line 1829902
    :cond_13
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/7OE;->CENTER:LX/7OE;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->setVideoPluginAlignment(LX/7OE;)V

    goto/16 :goto_6

    .line 1829903
    :cond_14
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->D:Lcom/facebook/video/player/RichVideoPlayer;

    const-class v3, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-virtual {v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/Video360Plugin;

    goto/16 :goto_7
.end method

.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 1829797
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1829798
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 1829799
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/facebook/widget/CustomFrameLayout;->attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1829800
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1829796
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->Q:Z

    return v0
.end method

.method public final b()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1829785
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1829786
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 1829787
    if-eqz v0, :cond_0

    .line 1829788
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    .line 1829789
    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v3

    if-lez v3, :cond_0

    const-string v3, "tip_jar_fragment"

    invoke-virtual {v0, v3}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 1829790
    const-string v2, "tip_jar_fragment"

    invoke-virtual {v0, v2}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;

    .line 1829791
    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/tipjar/LiveTipJarFragment;->d()V

    move v0, v1

    .line 1829792
    :goto_0
    return v0

    .line 1829793
    :cond_0
    sget-object v0, LX/04g;->BY_USER:LX/04g;

    sget-object v3, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-static {p0, v2, v0, v3}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;ZLX/04g;LX/04G;)V

    move v0, v1

    .line 1829794
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1829795
    goto :goto_0
.end method

.method public final c()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1829783
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->detachRecyclableViewFromParent(Landroid/view/View;)V

    .line 1829784
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final d()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1829782
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->D:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 1829779
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->v()V

    .line 1829780
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->n(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    .line 1829781
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 1829775
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCurrentVolume()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->S:Z

    .line 1829776
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->e(I)V

    .line 1829777
    return-void

    .line 1829778
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1829764
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->j()V

    .line 1829765
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;Z)V

    .line 1829766
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829767
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1829768
    :cond_0
    return-void
.end method

.method public getAdditionalPlugins()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1829985
    const/4 v0, 0x0

    return-object v0
.end method

.method public bridge synthetic getAdditionalPlugins()Ljava/util/List;
    .locals 1

    .prologue
    .line 1830003
    const/4 v0, 0x0

    move-object v0, v0

    .line 1830004
    return-object v0
.end method

.method public getCurrentVolume()I
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 1830000
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->E:Landroid/media/AudioManager;

    invoke-virtual {v0, v2}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 1830001
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->E:Landroid/media/AudioManager;

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    .line 1830002
    mul-int/lit8 v0, v0, 0x64

    div-int/2addr v0, v1

    return v0
.end method

.method public getFullScreenListener()LX/394;
    .locals 1

    .prologue
    .line 1829999
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->O:LX/394;

    return-object v0
.end method

.method public getPlayerType()LX/04G;
    .locals 1

    .prologue
    .line 1829998
    sget-object v0, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    return-object v0
.end method

.method public getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;
    .locals 1

    .prologue
    .line 1829997
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    return-object v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 1829994
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->w()V

    .line 1829995
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->o$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;)V

    .line 1829996
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1829993
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCurrentVolume()I

    move-result v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 1829986
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1829987
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->K:LX/395;

    .line 1829988
    iget-boolean v1, v0, LX/395;->m:Z

    move v0, v1

    .line 1829989
    if-eqz v0, :cond_0

    .line 1829990
    const/4 v0, 0x0

    sget-object v1, LX/04g;->BY_ORIENTATION_CHANGE:LX/04g;

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->a$redex0(Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;ZLX/04g;LX/04G;)V

    .line 1829991
    :goto_0
    return-void

    .line 1829992
    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->e(I)V

    goto :goto_0
.end method

.method public final requestLayout()V
    .locals 0

    .prologue
    .line 1829949
    invoke-super {p0}, Lcom/facebook/widget/CustomFrameLayout;->requestLayout()V

    .line 1829950
    return-void
.end method

.method public setAllowLooping(Z)V
    .locals 0

    .prologue
    .line 1829983
    iput-boolean p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->T:Z

    .line 1829984
    return-void
.end method

.method public setLogEnteringStartEvent(Z)V
    .locals 0

    .prologue
    .line 1829982
    return-void
.end method

.method public setLogExitingPauseEvent(Z)V
    .locals 0

    .prologue
    .line 1829980
    iput-boolean p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->U:Z

    .line 1829981
    return-void
.end method

.method public setNextStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1829978
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->af:LX/0QK;

    .line 1829979
    return-void
.end method

.method public setParentView(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 1829976
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->M:Landroid/view/ViewGroup;

    .line 1829977
    return-void
.end method

.method public setPreviousStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1829974
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->ag:LX/0QK;

    .line 1829975
    return-void
.end method

.method public final t_(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1829962
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->Q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->g:LX/0Uh;

    const/16 v1, 0x6

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1829963
    :cond_0
    :goto_0
    return-void

    .line 1829964
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getCurrentVolume()I

    move-result v0

    .line 1829965
    sparse-switch p1, :sswitch_data_0

    goto :goto_0

    .line 1829966
    :sswitch_0
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->r()V

    .line 1829967
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(I)V

    .line 1829968
    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b(I)V

    goto :goto_0

    .line 1829969
    :sswitch_1
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->c(I)V

    .line 1829970
    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b(I)V

    .line 1829971
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->q()V

    goto :goto_0

    .line 1829972
    :sswitch_2
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->I:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v2}, Lcom/facebook/video/player/RichVideoPlayer;->c(I)V

    .line 1829973
    invoke-direct {p0, v2}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->b(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x19 -> :sswitch_1
        0xa4 -> :sswitch_2
    .end sparse-switch
.end method
