.class public Lcom/facebook/feed/video/fullscreen/VideoFeedbackPopoverFragment;
.super Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;
.source ""


# instance fields
.field public m:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1833451
    invoke-direct {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final k()V
    .locals 3

    .prologue
    .line 1833452
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackPopoverFragment;->m:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    if-eqz v0, :cond_1

    .line 1833453
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackPopoverFragment;->m:Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    .line 1833454
    iget-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->Q:Z

    if-eqz v1, :cond_0

    .line 1833455
    invoke-static {v0}, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->y(Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;)V

    .line 1833456
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->P:Z

    .line 1833457
    iget-object v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v1

    iput v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->G:I

    .line 1833458
    iget-object v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1833459
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v1, v2

    .line 1833460
    iget-object v2, v1, LX/2pb;->y:LX/2qV;

    move-object v1, v2

    .line 1833461
    invoke-virtual {v1}, LX/2qV;->isPlayingState()Z

    move-result v1

    iput-boolean v1, v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->H:Z

    .line 1833462
    :cond_1
    invoke-super {p0}, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;->k()V

    .line 1833463
    return-void
.end method
