.class public Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;
.super LX/BuK;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# static fields
.field public static final v:Ljava/lang/String;


# instance fields
.field public s:LX/0tX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:Lcom/facebook/resources/ui/FbTextView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1830913
    const-class v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->v:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1830889
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1830890
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 1830907
    invoke-direct {p0, p1, p2}, LX/BuK;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1830908
    const-class v0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1830909
    const v0, 0x7f0d119f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->b:Landroid/view/ViewStub;

    .line 1830910
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->y:Ljava/util/ArrayList;

    .line 1830911
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BuM;

    invoke-direct {v1, p0}, LX/BuM;-><init>(Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1830912
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object p0

    check-cast p0, LX/1Ck;

    iput-object v1, p1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->s:LX/0tX;

    iput-object v2, p1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->t:LX/03V;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->u:LX/1Ck;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 2

    .prologue
    .line 1830894
    invoke-super {p0, p1, p2}, LX/BuK;->a(LX/2pa;Z)V

    .line 1830895
    iget-object v0, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830896
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830897
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1830898
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1830899
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->x:Ljava/lang/String;

    .line 1830900
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->x:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->y:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1830901
    iget-object v0, p0, LX/BuJ;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1830902
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1830903
    check-cast v0, LX/16n;

    invoke-virtual {p0, v0}, LX/BuJ;->a(LX/16n;)V

    .line 1830904
    :cond_1
    :goto_0
    return-void

    .line 1830905
    :cond_2
    if-nez p2, :cond_3

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/2oy;->j:LX/2pb;

    invoke-virtual {v0}, LX/2pb;->p()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1830906
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->g()V

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1830891
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedbackWithViewsAndCountPlugin;->u:LX/1Ck;

    const-string v1, "fetchVideoBroadcastPlayCount"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1830892
    invoke-super {p0}, LX/BuK;->d()V

    .line 1830893
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 1830886
    invoke-super {p0}, LX/BuK;->g()V

    .line 1830887
    iget-object v0, p0, LX/BuJ;->b:Landroid/view/ViewStub;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setVisibility(I)V

    .line 1830888
    return-void
.end method
