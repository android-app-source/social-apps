.class public Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;
.super LX/7MR;
.source ""


# annotations
.annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
.end annotation


# instance fields
.field public b:I

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Z

.field public e:Z

.field public f:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final n:Landroid/animation/Animator$AnimatorListener;

.field private final o:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1829595
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1829596
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1829593
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1829594
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1829586
    invoke-direct {p0, p1, p2, p3}, LX/7MR;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1829587
    new-instance v0, LX/Btq;

    invoke-direct {v0, p0}, LX/Btq;-><init>(Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->n:Landroid/animation/Animator$AnimatorListener;

    .line 1829588
    const-class v0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1829589
    const v0, 0x7f0d094f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->o:Landroid/widget/ImageView;

    .line 1829590
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->getPlayerStateChangedEventSubscriber()LX/2oa;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1829591
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Btr;

    invoke-direct {v1, p0}, LX/Btr;-><init>(Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1829592
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;

    invoke-static {p0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object p0

    check-cast p0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->f:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    return-void
.end method


# virtual methods
.method public final a(ILandroid/animation/Animator$AnimatorListener;)V
    .locals 2

    .prologue
    .line 1829582
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->f:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v0

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->d:Z

    if-eqz v0, :cond_1

    .line 1829583
    :cond_0
    :goto_0
    return-void

    .line 1829584
    :cond_1
    iput p1, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->b:I

    .line 1829585
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->o:Landroid/widget/ImageView;

    const/16 v1, 0xfa

    invoke-static {v0, v1, p1, p2}, LX/2pC;->a(Landroid/widget/ImageView;IILandroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public a(LX/2pa;Z)V
    .locals 1

    .prologue
    .line 1829576
    invoke-super {p0, p1, p2}, LX/7MR;->a(LX/2pa;Z)V

    .line 1829577
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->d:Z

    .line 1829578
    if-eqz p2, :cond_0

    .line 1829579
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->e:Z

    .line 1829580
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    .line 1829581
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1829574
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    .line 1829575
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 1829570
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->f:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v0

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->d:Z

    if-eqz v0, :cond_1

    .line 1829571
    :cond_0
    :goto_0
    return-void

    .line 1829572
    :cond_1
    invoke-super {p0}, LX/7MR;->f()V

    .line 1829573
    const v0, 0x7f020bc0

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->n:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->a(ILandroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 1829569
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1829568
    const v0, 0x7f030299

    return v0
.end method

.method public getPlayerStateChangedEventSubscriber()LX/2oa;
    .locals 2

    .prologue
    .line 1829567
    new-instance v0, LX/Bts;

    invoke-direct {v0, p0}, LX/Bts;-><init>(Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;)V

    return-object v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 1829564
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->o:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1829565
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->o:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 1829566
    return-void
.end method
