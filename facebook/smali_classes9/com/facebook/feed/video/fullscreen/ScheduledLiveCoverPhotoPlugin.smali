.class public Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;
.super LX/2oy;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final c:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public a:LX/1Ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Abd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1832549
    const-class v0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;

    const-string v1, "scheduled_live"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->c:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1832550
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1832551
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1832547
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832548
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1832552
    invoke-direct {p0, p1, p2, p3}, LX/2oy;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1832553
    const-class v0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1832554
    const v0, 0x7f03072b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 1832555
    const v0, 0x7f0d0b87

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1832556
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;

    invoke-static {p0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v1

    check-cast v1, LX/1Ad;

    invoke-static {p0}, LX/Abd;->a(LX/0QB;)LX/Abd;

    move-result-object p0

    check-cast p0, LX/Abd;

    iput-object v1, p1, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->a:LX/1Ad;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->b:LX/Abd;

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 1832544
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1832545
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1832546
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 4

    .prologue
    .line 1832530
    if-nez p2, :cond_1

    invoke-static {p1}, LX/393;->p(LX/2pa;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1832531
    :cond_0
    :goto_0
    return-void

    .line 1832532
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->g()V

    .line 1832533
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->i:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    .line 1832534
    if-eqz v0, :cond_0

    .line 1832535
    invoke-static {p1}, LX/393;->c(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1832536
    if-eqz v0, :cond_0

    .line 1832537
    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 1832538
    if-eqz v0, :cond_0

    .line 1832539
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->b:LX/Abd;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Abd;->f(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)LX/1bf;

    move-result-object v0

    .line 1832540
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->a:LX/1Ad;

    sget-object v3, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 1832541
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->d:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1832542
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/ScheduledLiveCoverPhotoPlugin;->g()V

    .line 1832543
    return-void
.end method
