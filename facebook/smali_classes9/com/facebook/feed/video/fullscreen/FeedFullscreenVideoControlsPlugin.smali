.class public Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;
.super LX/7N3;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lg;",
        ":",
        "LX/7Lk;",
        ">",
        "LX/7N3",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public o:LX/0xX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

.field private q:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1830532
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1830533
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1830530
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830531
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 1830525
    invoke-direct {p0, p1, p2, p3}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1830526
    const v0, 0x7f0d110d

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->p:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    .line 1830527
    const v0, 0x7f0d090b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->q:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    .line 1830528
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->q:Lcom/facebook/video/player/plugins/FullScreenCastPlugin;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->p:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/plugins/FullScreenCastPlugin;->setOtherControls(LX/7Mr;)V

    .line 1830529
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;

    invoke-static {p0}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object p0

    check-cast p0, LX/0xX;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->o:LX/0xX;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 3

    .prologue
    .line 1830521
    invoke-super {p0, p1, p2}, LX/7N3;->a(LX/2pa;Z)V

    .line 1830522
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->p:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v1, p0, LX/3Gb;->n:LX/7Lf;

    invoke-virtual {v0, v1}, LX/3Gb;->setEnvironment(LX/7Lf;)V

    .line 1830523
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->p:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    iget-object v1, p0, LX/2oy;->j:LX/2pb;

    iget-object v2, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v1, v2, p1}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1830524
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1830518
    invoke-super {p0}, LX/7N3;->d()V

    .line 1830519
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->p:Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-virtual {v0}, LX/2oy;->im_()V

    .line 1830520
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1830513
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->o:LX/0xX;

    if-nez v0, :cond_0

    .line 1830514
    const-class v0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1830515
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/FeedFullscreenVideoControlsPlugin;->o:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1830516
    const v0, 0x7f03062c

    .line 1830517
    :goto_0
    return v0

    :cond_1
    const v0, 0x7f03062b

    goto :goto_0
.end method
