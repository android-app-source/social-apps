.class public Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;
.super LX/3Ge;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final m:Lcom/facebook/common/callercontext/CallerContext;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1833564
    const-class v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 6
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/video/player/IsVideoSpecDisplayEnabled;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1833551
    invoke-direct {p0}, LX/3Ge;-><init>()V

    .line 1833552
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->k:Z

    .line 1833553
    new-instance v0, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1833554
    new-instance v1, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    .line 1833555
    new-instance v2, Lcom/facebook/video/player/plugins/SubtitlePlugin;

    invoke-direct {v2, p1}, Lcom/facebook/video/player/plugins/SubtitlePlugin;-><init>(Landroid/content/Context;)V

    .line 1833556
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    new-instance v4, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v5, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->m:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v4, p1, v5}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    iput-object v3, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->b:LX/0Px;

    .line 1833557
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    iget-object v4, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v3, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v3, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v3, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v3, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v3, Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;

    invoke-direct {v3, p1}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreEndscreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->c:LX/0Px;

    .line 1833558
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    iget-object v3, p0, LX/3Ge;->b:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v0

    new-instance v3, Lcom/facebook/video/player/plugins/Video360Plugin;

    invoke-direct {v3, p1}, Lcom/facebook/video/player/plugins/Video360Plugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/feed/video/fullscreen/FullScreenNetworkErrorBannerPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->d:LX/0Px;

    .line 1833559
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->j:LX/0Px;

    .line 1833560
    iget-object v0, p0, LX/3Ge;->d:LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->i:LX/0Px;

    .line 1833561
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->e:LX/0Px;

    .line 1833562
    iget-object v0, p0, LX/3Ge;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;->f:LX/0Px;

    .line 1833563
    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;
    .locals 3

    .prologue
    .line 1833539
    new-instance v2, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/3Gf;->b(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-direct {v2, v0, v1}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreRichVideoPlayerPluginSelector;-><init>(Landroid/content/Context;Ljava/lang/Boolean;)V

    .line 1833540
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/facebook/video/player/RichVideoPlayer;)LX/3J8;
    .locals 1

    .prologue
    .line 1833541
    const-class v0, LX/BvQ;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1833542
    sget-object v0, LX/3J8;->PREVIOUSLY_LIVE_VIDEO:LX/3J8;

    .line 1833543
    :goto_0
    return-object v0

    .line 1833544
    :cond_0
    const-class v0, Lcom/facebook/facecastdisplay/LiveEventsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1833545
    sget-object v0, LX/3J8;->LIVE_VIDEO:LX/3J8;

    goto :goto_0

    .line 1833546
    :cond_1
    const-class v0, Lcom/facebook/feed/video/fullscreen/Video360ControlsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1833547
    sget-object v0, LX/3J8;->REGULAR_360_VIDEO:LX/3J8;

    goto :goto_0

    .line 1833548
    :cond_2
    const-class v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-virtual {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(Ljava/lang/Class;)LX/2oy;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 1833549
    sget-object v0, LX/3J8;->REGULAR_VIDEO:LX/3J8;

    goto :goto_0

    .line 1833550
    :cond_3
    sget-object v0, LX/3J8;->UNKNOWN_VIDEO:LX/3J8;

    goto :goto_0
.end method
