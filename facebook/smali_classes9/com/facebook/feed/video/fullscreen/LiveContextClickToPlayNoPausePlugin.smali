.class public Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;
.super Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;
.source ""


# instance fields
.field public n:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final o:Landroid/animation/Animator$AnimatorListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1831565
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1831566
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1831567
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831568
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 1831569
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1831570
    new-instance v0, LX/Bul;

    invoke-direct {v0, p0}, LX/Bul;-><init>(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->o:Landroid/animation/Animator$AnimatorListener;

    .line 1831571
    const-class v0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1831572
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bum;

    invoke-direct {v1, p0}, LX/Bum;-><init>(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1831573
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;

    invoke-static {v0}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->a(LX/0QB;)Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->n:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    return-void
.end method

.method public static i(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;)V
    .locals 2

    .prologue
    .line 1831574
    iget-object v0, p0, LX/7MR;->a:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1831575
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->h()V

    .line 1831576
    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 0

    .prologue
    .line 1831577
    invoke-super {p0, p1, p2}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->a(LX/2pa;Z)V

    .line 1831578
    if-eqz p2, :cond_0

    .line 1831579
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->i(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;)V

    .line 1831580
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 1831581
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->n:Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/video/commercialbreak/CommercialBreakInfoTracker;->g(Ljava/lang/String;)LX/2oN;

    move-result-object v0

    sget-object v1, LX/2oN;->NONE:LX/2oN;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->d:Z

    if-eqz v0, :cond_1

    .line 1831582
    :cond_0
    :goto_0
    return-void

    .line 1831583
    :cond_1
    invoke-super {p0}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->f()V

    .line 1831584
    const v0, 0x7f020bc0

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;->o:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {p0, v0, v1}, Lcom/facebook/feed/video/fullscreen/ClickToPlayNoPausePlugin;->a(ILandroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public getPlayerStateChangedEventSubscriber()LX/2oa;
    .locals 2

    .prologue
    .line 1831585
    new-instance v0, LX/Bun;

    invoke-direct {v0, p0}, LX/Bun;-><init>(Lcom/facebook/feed/video/fullscreen/LiveContextClickToPlayNoPausePlugin;)V

    return-object v0
.end method
