.class public Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;
.super Lcom/facebook/feedback/ui/BaseFeedbackFragment;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final D:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public B:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/7I8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/9Dh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:Lcom/facebook/video/player/RichVideoPlayer;

.field public F:LX/BwG;

.field public G:I

.field public H:Z

.field private I:Lcom/facebook/feedback/ui/FeedbackHeaderView;

.field public J:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

.field public K:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field public L:LX/3It;

.field private M:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2oy;",
            ">;"
        }
    .end annotation
.end field

.field private N:Landroid/view/ViewGroup$LayoutParams;

.field public O:Z

.field public P:Z

.field public Q:Z

.field private final R:LX/3It;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1833402
    const-class v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->D:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1833403
    invoke-direct {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;-><init>()V

    .line 1833404
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->H:Z

    .line 1833405
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->O:Z

    .line 1833406
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->P:Z

    .line 1833407
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->Q:Z

    .line 1833408
    new-instance v0, LX/BwF;

    invoke-direct {v0, p0}, LX/BwF;-><init>(Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;)V

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->R:LX/3It;

    return-void
.end method

.method public static a(Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;Lcom/facebook/video/player/RichVideoPlayer;)V
    .locals 2

    .prologue
    .line 1833409
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1833410
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1833411
    invoke-static {p1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833412
    goto :goto_0

    .line 1833413
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    const/16 v2, 0x37b0

    invoke-static {v1, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const-class p0, LX/9Dh;

    invoke-interface {v1, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v1

    check-cast v1, LX/9Dh;

    iput-object v2, p1, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->B:LX/0Ot;

    iput-object v1, p1, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->C:LX/9Dh;

    return-void
.end method

.method public static y(Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1833414
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->L:LX/3It;

    .line 1833415
    iput-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1833416
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->o()Ljava/util/List;

    .line 1833417
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1833418
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1833419
    invoke-static {v2, v0}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833420
    goto :goto_0

    .line 1833421
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->F:LX/BwG;

    iget-object v1, v1, LX/BwG;->b:LX/2pa;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1833422
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1833423
    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomLinearLayout;->detachRecyclableViewFromParent(Landroid/view/View;)V

    .line 1833424
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->N:Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1833425
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->requestLayout()V

    .line 1833426
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->J:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->addView(Landroid/view/View;)V

    .line 1833427
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->K:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1833428
    iput-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->K:Ljava/util/List;

    .line 1833429
    iput-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->L:LX/3It;

    .line 1833430
    iput-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->N:Landroid/view/ViewGroup$LayoutParams;

    .line 1833431
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1833432
    const-string v0, "video"

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1833433
    const-class v0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1833434
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->F:LX/BwG;

    if-eqz v0, :cond_0

    .line 1833435
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->F:LX/BwG;

    iget v0, v0, LX/BwG;->c:I

    iput v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->G:I

    .line 1833436
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->F:LX/BwG;

    iget-boolean v0, v0, LX/BwG;->d:Z

    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->H:Z

    .line 1833437
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->F:LX/BwG;

    iget-object v0, v0, LX/BwG;->f:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->J:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    .line 1833438
    :cond_0
    iput-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->O:Z

    .line 1833439
    iput-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->P:Z

    .line 1833440
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->J:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->J:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    instance-of v0, v0, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7I8;

    iget-boolean v0, v0, LX/7I8;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->Q:Z

    .line 1833441
    invoke-super {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(Landroid/os/Bundle;)V

    .line 1833442
    return-void

    :cond_1
    move v0, v1

    .line 1833443
    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1833393
    const-string v0, "flyout_video_feedback_animation"

    return-object v0
.end method

.method public final m()LX/21B;
    .locals 1

    .prologue
    .line 1833392
    sget-object v0, LX/21B;->VIDEO_FEEDBACK:LX/21B;

    return-object v0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 1833394
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1833395
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1833396
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1833397
    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1833398
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1833399
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setVisibility(I)V

    .line 1833400
    :cond_1
    invoke-super {p0, p1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1833401
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2a

    const v1, -0x3fe15dfb

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1833381
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->r()Landroid/content/Context;

    move-result-object v1

    .line 1833382
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 1833383
    const v3, 0x7f031593

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1833384
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    .line 1833385
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    new-instance v4, Lcom/facebook/video/player/plugins/CoverImagePlugin;

    sget-object v5, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->D:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v4, v1, v5}, Lcom/facebook/video/player/plugins/CoverImagePlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833386
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    new-instance v4, LX/3Gh;

    invoke-direct {v4, v1}, LX/3Gh;-><init>(Landroid/content/Context;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833387
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    new-instance v4, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;

    invoke-direct {v4, v1}, Lcom/facebook/video/player/plugins/LoadingSpinnerPlugin;-><init>(Landroid/content/Context;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833388
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    new-instance v4, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;

    invoke-direct {v4, v1}, Lcom/facebook/video/player/plugins/ClickToPlayAnimationPlugin;-><init>(Landroid/content/Context;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833389
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    new-instance v4, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;

    invoke-direct {v4, v1}, Lcom/facebook/feed/video/fullscreen/FeedFullscreenSeekBarPlugin;-><init>(Landroid/content/Context;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833390
    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->M:Ljava/util/List;

    new-instance v4, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;

    invoke-direct {v4, v1}, Lcom/facebook/feed/video/fullscreen/FullscreenCallToActionEndscreenPlugin;-><init>(Landroid/content/Context;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833391
    const/16 v1, 0x2b

    const v3, -0x7c2626d8

    invoke-static {v6, v1, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x40d12e8c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1833372
    iget-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->P:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->Q:Z

    if-eqz v1, :cond_0

    .line 1833373
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->y(Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;)V

    .line 1833374
    :cond_0
    iget-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->Q:Z

    if-nez v1, :cond_1

    .line 1833375
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v2, 0x1

    sget-object v3, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v1, v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1833376
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->i()V

    .line 1833377
    :cond_1
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onDestroyView()V

    .line 1833378
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->I:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-virtual {p0, v1}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->b(LX/21l;)V

    .line 1833379
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->I:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    .line 1833380
    const/16 v1, 0x2b

    const v2, -0x62553d79

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x48b90f16    # 379000.7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1833368
    iget-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->P:Z

    if-nez v1, :cond_0

    .line 1833369
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->b(LX/04g;)V

    .line 1833370
    :cond_0
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onPause()V

    .line 1833371
    const/16 v1, 0x2b

    const v2, -0x18c4869f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3c8bf869

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1833361
    iget-boolean v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->H:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1833362
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v1, v2

    .line 1833363
    iget-object v2, v1, LX/2pb;->y:LX/2qV;

    move-object v1, v2

    .line 1833364
    invoke-virtual {v1}, LX/2qV;->isPlayingState()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1833365
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v2, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(LX/04g;)V

    .line 1833366
    :cond_0
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onResume()V

    .line 1833367
    const/16 v1, 0x2b

    const v2, -0x4c8515b9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x17bd694d

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1833358
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v1

    iput v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->G:I

    .line 1833359
    invoke-super {p0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onStop()V

    .line 1833360
    const/16 v1, 0x2b

    const v2, 0x2b19d424

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1833319
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7I8;

    iget-boolean v0, v0, LX/7I8;->b:Z

    if-eqz v0, :cond_1

    .line 1833320
    const v0, 0x7f0d30b1

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->I:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    .line 1833321
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->I:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->C:LX/9Dh;

    invoke-virtual {v1, p0}, LX/9Dh;->a(Lcom/facebook/base/fragment/FbFragment;)LX/9Dg;

    move-result-object v1

    .line 1833322
    iput-object v1, v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;->k:LX/9Df;

    .line 1833323
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->I:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    invoke-virtual {p0, v0}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->a(LX/21l;)V

    .line 1833324
    invoke-super {p0, p1, p2}, Lcom/facebook/feedback/ui/BaseFeedbackFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1833325
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->Q:Z

    if-nez v0, :cond_2

    .line 1833326
    const v0, 0x7f0d1338

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/player/RichVideoPlayer;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1833327
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/player/plugins/VideoPlugin;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/video/player/plugins/VideoPlugin;-><init>(Landroid/content/Context;)V

    .line 1833328
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833329
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->a(Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1833330
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->F:LX/BwG;

    iget-object v1, v1, LX/BwG;->b:LX/2pa;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->c(LX/2pa;)V

    .line 1833331
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1833332
    int-to-double v2, v0

    const-wide v4, 0x3ffc71c71c71c71cL    # 1.7777777777777777

    div-double/2addr v2, v4

    double-to-int v1, v2

    .line 1833333
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v2}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->N:Landroid/view/ViewGroup$LayoutParams;

    .line 1833334
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/facebook/video/player/RichVideoPlayer;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1833335
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->R:LX/3It;

    .line 1833336
    iput-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    .line 1833337
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->Q:Z

    if-nez v0, :cond_0

    .line 1833338
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    const/4 v1, 0x0

    sget-object v2, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 1833339
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    sget-object v1, LX/04G;->FULL_SCREEN_PLAYER:LX/04G;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerType(LX/04G;)V

    .line 1833340
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->F:LX/BwG;

    iget-object v1, v1, LX/BwG;->e:LX/04D;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 1833341
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    iget v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->G:I

    sget-object v2, LX/04g;->BY_FLYOUT:LX/04g;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/video/player/RichVideoPlayer;->a(ILX/04g;)V

    .line 1833342
    :cond_0
    return-void

    .line 1833343
    :cond_1
    const v0, 0x7f0d30b0

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedback/ui/FeedbackHeaderView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->I:Lcom/facebook/feedback/ui/FeedbackHeaderView;

    goto/16 :goto_0

    .line 1833344
    :cond_2
    const v0, 0x7f0d1338

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1833345
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->J:Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/feed/video/fullscreen/FeedFullScreenVideoPlayer;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    .line 1833346
    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->o()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->K:Ljava/util/List;

    .line 1833347
    iget-object v1, v0, Lcom/facebook/video/player/RichVideoPlayer;->F:LX/3It;

    move-object v1, v1

    .line 1833348
    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->L:LX/3It;

    .line 1833349
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1833350
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->a(Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;Lcom/facebook/video/player/RichVideoPlayer;)V

    .line 1833351
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/video/player/RichVideoPlayer;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomRelativeLayout;

    .line 1833352
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomRelativeLayout;->detachRecyclableViewFromParent(Landroid/view/View;)V

    .line 1833353
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->B:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7I8;

    iget-boolean v0, v0, LX/7I8;->b:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    move v1, v0

    .line 1833354
    :goto_2
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v0

    .line 1833355
    check-cast v0, Lcom/facebook/widget/CustomLinearLayout;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoFeedbackFragment;->E:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    invoke-virtual {v0, v2, v1, v3}, Lcom/facebook/widget/CustomLinearLayout;->attachRecyclableViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1833356
    goto/16 :goto_1

    .line 1833357
    :cond_3
    const/4 v0, 0x3

    move v1, v0

    goto :goto_2
.end method

.method public final r()Landroid/content/Context;
    .locals 3

    .prologue
    .line 1833318
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e0606

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 1833317
    const/4 v0, 0x0

    return v0
.end method
