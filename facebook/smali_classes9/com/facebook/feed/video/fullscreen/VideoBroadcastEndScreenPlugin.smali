.class public Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;
.super LX/3Ga;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/Abf;
.implements LX/Acx;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# static fields
.field public static final p:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public A:Landroid/view/ViewStub;

.field public B:Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

.field public C:LX/Bvv;

.field public D:Z

.field public E:Lcom/facebook/graphql/model/GraphQLStory;

.field public F:Lcom/facebook/graphql/model/GraphQLActor;

.field public G:Lcom/facebook/graphql/model/GraphQLActor;

.field public H:Lcom/facebook/graphql/model/GraphQLMedia;

.field public I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public J:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLExploreFeed;",
            ">;"
        }
    .end annotation
.end field

.field public K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

.field public L:Ljava/lang/String;

.field private M:Ljava/lang/String;

.field public N:Z

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Acy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Abg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AVV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1b4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:Landroid/view/View;

.field public r:Landroid/view/View;

.field public s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public u:Landroid/widget/TextView;

.field public v:Landroid/widget/TextView;

.field private w:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

.field public x:Landroid/widget/TextView;

.field public y:Landroid/widget/TextView;

.field public z:Landroid/view/ViewStub;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1833193
    const-class v0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->p:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1833191
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1833192
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1833189
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833190
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    .line 1833002
    invoke-direct {p0, p1, p2, p3}, LX/3Ga;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833003
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p3

    move-object v2, p0

    check-cast v2, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;

    const/16 v3, 0xc49

    invoke-static {p3, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x455

    invoke-static {p3, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p3}, LX/Acy;->b(LX/0QB;)LX/Acy;

    move-result-object v5

    check-cast v5, LX/Acy;

    const/16 p1, 0x1bf2

    invoke-static {p3, p1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    const/16 p2, 0x1bcb

    invoke-static {p3, p2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p2

    const/16 v0, 0x52d

    invoke-static {p3, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p3

    iput-object v3, v2, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->b:LX/0Ot;

    iput-object v4, v2, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c:LX/0Ot;

    iput-object v5, v2, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->d:LX/Acy;

    iput-object p1, v2, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->e:LX/0Ot;

    iput-object p2, v2, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->f:LX/0Ot;

    iput-object p3, v2, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->o:LX/0Ot;

    .line 1833004
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/Bvw;

    invoke-direct {v1, p0}, LX/Bvw;-><init>(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833005
    return-void
.end method

.method public static a$redex0(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;LX/Bvv;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1833096
    invoke-virtual {p0}, LX/3Ga;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1833097
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->C:LX/Bvv;

    if-eq v0, p1, :cond_7

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->D:Z

    .line 1833098
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->C:LX/Bvv;

    .line 1833099
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    if-eqz v0, :cond_0

    .line 1833100
    iget-object v0, p0, LX/2oy;->i:LX/2oj;

    new-instance v2, LX/7MG;

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->M:Ljava/lang/String;

    invoke-direct {v2, v3}, LX/7MG;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/2oj;->a(LX/2ol;)V

    .line 1833101
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->r:Landroid/view/View;

    new-instance v2, LX/Bvs;

    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/Bvs;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1833102
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_c

    .line 1833103
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1833104
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->o()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1833105
    const/16 p1, 0x8

    const/4 v5, 0x0

    .line 1833106
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->B:Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    if-nez v0, :cond_2

    .line 1833107
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->z:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1833108
    :cond_2
    const v0, 0x7f0d3063

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->B:Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    .line 1833109
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->B:Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->L:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->F:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0, v2, v3, v4}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLActor;)V

    .line 1833110
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->B:Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    invoke-virtual {v0, v5}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->setVisibility(I)V

    .line 1833111
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1833112
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->u:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833113
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->v:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 1833114
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->v:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833115
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833116
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833117
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->N:Z

    if-eqz v0, :cond_d

    const v0, 0x7f080d90

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 1833118
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->N:Z

    if-eqz v0, :cond_e

    const v0, 0x7f080d95

    :goto_3
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 1833119
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->d:LX/Acy;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->M:Ljava/lang/String;

    invoke-virtual {v0, p0, v2}, LX/Acy;->a(LX/Acx;Ljava/lang/String;)V

    .line 1833120
    :goto_4
    const/4 v5, 0x0

    .line 1833121
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->J:LX/0Px;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->J:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->J:LX/0Px;

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_f

    .line 1833122
    :cond_4
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_5

    .line 1833123
    iget-object v0, p0, LX/2oy;->k:Lcom/facebook/video/player/RichVideoPlayer;

    .line 1833124
    iget-object v2, v0, Lcom/facebook/video/player/RichVideoPlayer;->D:LX/2pa;

    move-object v0, v2

    .line 1833125
    invoke-static {v0}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1833126
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_5

    .line 1833127
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1833128
    :cond_5
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->u(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;)V

    .line 1833129
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->q:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1833130
    :cond_6
    return-void

    :cond_7
    move v0, v1

    .line 1833131
    goto/16 :goto_0

    .line 1833132
    :cond_8
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 1833133
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->F:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->F:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_11

    .line 1833134
    :cond_9
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1833135
    :goto_5
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aH()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    .line 1833136
    :goto_6
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->u:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833137
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1833138
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->v:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->aG()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833139
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->v:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833140
    :cond_a
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833141
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833142
    sget-object v0, LX/Bvu;->a:[I

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->C:LX/Bvv;

    invoke-virtual {v2}, LX/Bvv;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 1833143
    :cond_b
    :goto_7
    invoke-static {p0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->u(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;)V

    .line 1833144
    goto/16 :goto_4

    .line 1833145
    :cond_c
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1833146
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_1

    .line 1833147
    :cond_d
    const v0, 0x7f080d8f

    goto/16 :goto_2

    .line 1833148
    :cond_e
    const v0, 0x7f080d94

    goto/16 :goto_3

    .line 1833149
    :cond_f
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->A:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1833150
    const v0, 0x7f0d3075

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1833151
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->J:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    move v4, v5

    :goto_8
    if-ge v4, v6, :cond_4

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->J:LX/0Px;

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLExploreFeed;

    .line 1833152
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    if-eqz v3, :cond_10

    .line 1833153
    invoke-virtual {p0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const p1, 0x7f031584

    invoke-virtual {v3, p1, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1833154
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLExploreFeed;->l()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833155
    new-instance p1, LX/Bvt;

    invoke-direct {p1, p0, v2}, LX/Bvt;-><init>(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;Lcom/facebook/graphql/model/GraphQLExploreFeed;)V

    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1833156
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1833157
    :cond_10
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_8

    .line 1833158
    :cond_11
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->F:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aj()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1833159
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v3, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->p:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v0, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto/16 :goto_5

    .line 1833160
    :cond_12
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->F:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_6

    .line 1833161
    :pswitch_0
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->N:Z

    if-eqz v0, :cond_13

    const v0, 0x7f080d90

    :goto_9
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    .line 1833162
    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->N:Z

    if-eqz v0, :cond_14

    const v0, 0x7f080d95

    :goto_a
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_7

    .line 1833163
    :cond_13
    const v0, 0x7f080d8f

    goto :goto_9

    .line 1833164
    :cond_14
    const v0, 0x7f080d94

    goto :goto_a

    .line 1833165
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1833166
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_15

    .line 1833167
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->F()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833168
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833169
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->D:Z

    if-eqz v0, :cond_b

    .line 1833170
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVV;

    const-string v2, "transitioned to timed out"

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->E:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v2, v3}, LX/AVV;->b(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto/16 :goto_7

    .line 1833171
    :cond_15
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833172
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 1833173
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1833174
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_16

    .line 1833175
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->u()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833176
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->t()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833177
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->D:Z

    if-eqz v0, :cond_b

    .line 1833178
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVV;

    const-string v2, "transitioned to rescheduled"

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->E:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v2, v3}, LX/AVV;->b(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto/16 :goto_7

    .line 1833179
    :cond_16
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833180
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    .line 1833181
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setVisibility(I)V

    .line 1833182
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_17

    .line 1833183
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->E()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833184
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833185
    iget-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->D:Z

    if-eqz v0, :cond_b

    .line 1833186
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AVV;

    const-string v2, "transitioned to cancelled"

    iget-object v3, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->E:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, v2, v3}, LX/AVV;->b(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto/16 :goto_7

    .line 1833187
    :cond_17
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1833188
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_7

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(LX/Abe;)V
    .locals 1

    .prologue
    .line 1833093
    sget-object v0, LX/Abe;->TIMED_OUT:LX/Abe;

    if-ne p1, v0, :cond_0

    .line 1833094
    sget-object v0, LX/Bvv;->SCHEDULED_LIVE_TIMED_OUT:LX/Bvv;

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->a$redex0(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;LX/Bvv;)V

    .line 1833095
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(I)TT;"
        }
    .end annotation

    .prologue
    .line 1833092
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->q:Landroid/view/View;

    invoke-static {v0, p1}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 1833086
    iget-boolean v0, p0, LX/3Ga;->c:Z

    move v0, v0

    .line 1833087
    if-eqz v0, :cond_0

    .line 1833088
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->q:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1833089
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_1

    .line 1833090
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    invoke-virtual {v0}, LX/Abg;->b()V

    .line 1833091
    :cond_1
    return-void
.end method

.method public static u(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;)V
    .locals 6

    .prologue
    .line 1833078
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1833079
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->w:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->setVisibility(I)V

    .line 1833080
    :cond_0
    :goto_0
    return-void

    .line 1833081
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aB()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1833082
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->w:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->setVisibility(I)V

    .line 1833083
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->w:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aF()Z

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UNKNOWN"

    const-string v4, "UNKNOWN"

    iget-object v5, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1833084
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLActor;->aC()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1833085
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->w:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLActor;->aE()Z

    move-result v1

    const-string v2, "UNKNOWN"

    const-string v3, "UNKNOWN"

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;->a(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 0

    .prologue
    .line 1833077
    return-void
.end method

.method public final a(LX/2pa;Z)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1833032
    invoke-super {p0, p1, p2}, LX/3Ga;->a(LX/2pa;Z)V

    .line 1833033
    if-nez p2, :cond_1

    invoke-static {p1}, LX/393;->p(LX/2pa;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, LX/393;->q(LX/2pa;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1833034
    :cond_0
    :goto_0
    return-void

    .line 1833035
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->i()V

    .line 1833036
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 1833037
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "GraphQLStoryProps"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1833038
    if-eqz v0, :cond_0

    .line 1833039
    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->E:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1833040
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 1833041
    if-eq v1, v5, :cond_2

    .line 1833042
    iput-boolean v3, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->D:Z

    .line 1833043
    :cond_2
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 1833044
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->E:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1833045
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1833046
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->D()LX/0Px;

    move-result-object v0

    .line 1833047
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1833048
    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->F:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1833049
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->E:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->E:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->Y()Lcom/facebook/graphql/model/GraphQLFeedTopicContent;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedTopicContent;->a()LX/0Px;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->J:LX/0Px;

    .line 1833050
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    .line 1833051
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_3

    .line 1833052
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1b4;

    invoke-virtual {v0}, LX/1b4;->w()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->bQ()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v3

    :goto_2
    iput-boolean v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->N:Z

    .line 1833053
    :cond_3
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->M:Ljava/lang/String;

    .line 1833054
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->VOD_READY:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_5

    .line 1833055
    :cond_4
    sget-object v0, LX/Bvv;->PLAYBACK_COMPLETE:LX/Bvv;

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->a$redex0(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;LX/Bvv;)V

    .line 1833056
    :cond_5
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->aK()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->G:Lcom/facebook/graphql/model/GraphQLActor;

    .line 1833057
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->d:LX/Acy;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->M:Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/Acy;->a(LX/Acx;Ljava/lang/String;)V

    .line 1833058
    invoke-static {p1}, LX/393;->c(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1833059
    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;

    move-result-object v0

    .line 1833060
    if-nez v0, :cond_8

    :goto_3
    iput-object v2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    .line 1833061
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    if-eqz v0, :cond_0

    .line 1833062
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SCHEDULED_CANCELED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v1, :cond_9

    .line 1833063
    sget-object v0, LX/Bvv;->SCHEDULED_LIVE_CANCELLED:LX/Bvv;

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->a$redex0(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;LX/Bvv;)V

    goto/16 :goto_0

    :cond_6
    move-object v0, v2

    .line 1833064
    goto/16 :goto_1

    :cond_7
    move v0, v4

    .line 1833065
    goto :goto_2

    .line 1833066
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachmentStyleInfo;->R()Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    move-result-object v2

    goto :goto_3

    .line 1833067
    :cond_9
    invoke-static {p1}, LX/393;->q(LX/2pa;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1833068
    sget-object v0, LX/Bvv;->SCHEDULED_LIVE_RESCHEDULED:LX/Bvv;

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->a$redex0(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;LX/Bvv;)V

    goto/16 :goto_0

    .line 1833069
    :cond_a
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->H:Lcom/facebook/graphql/model/GraphQLMedia;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    invoke-static {v0}, LX/Abd;->a(Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1833070
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->I:Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;

    invoke-virtual {v0, v1}, LX/Abg;->a(Lcom/facebook/graphql/model/GraphQLVideoBroadcastSchedule;)V

    .line 1833071
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    .line 1833072
    iput-object p0, v0, LX/Abg;->g:LX/Abf;

    .line 1833073
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    .line 1833074
    iput-boolean v4, v0, LX/Abg;->f:Z

    .line 1833075
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Abg;

    invoke-virtual {v0}, LX/Abg;->a()LX/Abe;

    move-result-object v0

    .line 1833076
    invoke-direct {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->b(LX/Abe;)V

    goto/16 :goto_0
.end method

.method public final a(LX/Abe;)V
    .locals 0

    .prologue
    .line 1833030
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->b(LX/Abe;)V

    .line 1833031
    return-void
.end method

.method public final a(Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1    # Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1833025
    iput-object p1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    .line 1833026
    iput-object p2, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->L:Ljava/lang/String;

    .line 1833027
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->K:Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/donation/graphql/LiveVideoDonationFragmentModels$LiveDonationViewerFragmentModel;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->B:Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    if-eqz v0, :cond_0

    .line 1833028
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->B:Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;

    iget-object v1, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->L:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/donation/VideoBroadcastEndscreenDonationView;->a(Ljava/lang/String;)V

    .line 1833029
    :cond_0
    return-void
.end method

.method public final b(LX/2pa;)Z
    .locals 1

    .prologue
    .line 1833024
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 1833021
    invoke-direct {p0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->i()V

    .line 1833022
    invoke-super {p0}, LX/3Ga;->d()V

    .line 1833023
    return-void
.end method

.method public getLayoutToInflate()I
    .locals 1

    .prologue
    .line 1833020
    const v0, 0x7f031582

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1833019
    const/4 v0, 0x1

    return v0
.end method

.method public setupPlugin(LX/2pa;)V
    .locals 0

    .prologue
    .line 1833018
    return-void
.end method

.method public setupViews(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1833006
    const v0, 0x7f0d306a

    invoke-static {p1, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->q:Landroid/view/View;

    .line 1833007
    const v0, 0x7f0d306c

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->r:Landroid/view/View;

    .line 1833008
    const v0, 0x7f0d306b

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->s:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1833009
    const v0, 0x7f0d306d

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->t:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1833010
    const v0, 0x7f0d306e

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->u:Landroid/widget/TextView;

    .line 1833011
    const v0, 0x7f0d306f

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->v:Landroid/widget/TextView;

    .line 1833012
    const v0, 0x7f0d3070

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->w:Lcom/facebook/video/followvideos/VideoHomeFollowVideosButton;

    .line 1833013
    const v0, 0x7f0d3072

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->x:Landroid/widget/TextView;

    .line 1833014
    const v0, 0x7f0d3073

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->y:Landroid/widget/TextView;

    .line 1833015
    const v0, 0x7f0d3071

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->z:Landroid/view/ViewStub;

    .line 1833016
    const v0, 0x7f0d3074

    invoke-static {p0, v0}, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->c(Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/VideoBroadcastEndScreenPlugin;->A:Landroid/view/ViewStub;

    .line 1833017
    return-void
.end method
