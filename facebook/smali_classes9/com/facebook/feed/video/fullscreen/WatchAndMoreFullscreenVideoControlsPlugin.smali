.class public Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;
.super LX/7N3;
.source ""


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/7Lj;",
        ">",
        "LX/7N3",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public o:LX/1C2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final p:Landroid/view/View;

.field private final q:Lcom/facebook/fbui/glyph/GlyphView;

.field public r:Z

.field public s:Lcom/facebook/video/engine/VideoPlayerParams;

.field public t:LX/D8U;

.field public u:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1833502
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1833503
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1833504
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833505
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ConstructorMayLeakThis"
        }
    .end annotation

    .prologue
    .line 1833506
    invoke-direct {p0, p1, p2, p3}, LX/7N3;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833507
    const-class v0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1833508
    const v0, 0x7f0d3194

    invoke-virtual {p0, v0}, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->p:Landroid/view/View;

    .line 1833509
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->p:Landroid/view/View;

    const v1, 0x7f0d3195

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->q:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1833510
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BwJ;

    invoke-direct {v1, p0}, LX/BwJ;-><init>(Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833511
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;

    invoke-static {p0}, LX/1C2;->a(LX/0QB;)LX/1C2;

    move-result-object p0

    check-cast p0, LX/1C2;

    iput-object p0, p1, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->o:LX/1C2;

    return-void
.end method


# virtual methods
.method public final a(LX/2pa;Z)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1833512
    invoke-super {p0, p1, p2}, LX/7N3;->a(LX/2pa;Z)V

    .line 1833513
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->s:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 1833514
    iput-boolean v3, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->r:Z

    .line 1833515
    iget-object v0, p1, LX/2pa;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    iget-object v0, v0, Lcom/facebook/video/engine/VideoPlayerParams;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->u:Ljava/lang/String;

    .line 1833516
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "CanDismissWatchAndMoreVideoPlayer"

    invoke-virtual {v0, v1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "CanDismissWatchAndMoreVideoPlayer"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v1, "CanDismissWatchAndMoreVideoPlayer"

    invoke-virtual {v0, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 1833517
    :goto_0
    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v4, "CanCloseWatchAndMore"

    invoke-virtual {v0, v4}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v4, "CanCloseWatchAndMore"

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    iget-object v0, p1, LX/2pa;->b:LX/0P1;

    const-string v4, "CanCloseWatchAndMore"

    invoke-virtual {v0, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1833518
    :goto_1
    if-nez v1, :cond_2

    if-nez v2, :cond_2

    .line 1833519
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1833520
    :goto_2
    return-void

    :cond_0
    move v1, v3

    .line 1833521
    goto :goto_0

    :cond_1
    move v2, v3

    .line 1833522
    goto :goto_1

    .line 1833523
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->p:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1833524
    if-eqz v1, :cond_4

    .line 1833525
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const v2, 0x7f0207ee

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 1833526
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->p:Landroid/view/View;

    new-instance v2, LX/BwI;

    invoke-direct {v2, p0, v1}, LX/BwI;-><init>(Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;Z)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 1833527
    :cond_4
    if-eqz v2, :cond_3

    .line 1833528
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->q:Lcom/facebook/fbui/glyph/GlyphView;

    const v2, 0x7f020818

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    goto :goto_3
.end method

.method public final b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V
    .locals 2

    .prologue
    .line 1833529
    invoke-super {p0, p1, p2, p3}, LX/7N3;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    .line 1833530
    iget-object v0, p0, LX/7MM;->a:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2oy;

    .line 1833531
    invoke-virtual {v0, p1, p2, p3}, LX/2oy;->b(LX/2pb;Lcom/facebook/video/player/RichVideoPlayer;LX/2pa;)V

    goto :goto_0

    .line 1833532
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1833533
    invoke-super {p0}, LX/7N3;->d()V

    .line 1833534
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->p:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1833535
    iget-object v0, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->p:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1833536
    :cond_0
    iput-object v1, p0, Lcom/facebook/feed/video/fullscreen/WatchAndMoreFullscreenVideoControlsPlugin;->u:Ljava/lang/String;

    .line 1833537
    return-void
.end method

.method public getContentView()I
    .locals 1

    .prologue
    .line 1833538
    const v0, 0x7f03160e

    return v0
.end method
