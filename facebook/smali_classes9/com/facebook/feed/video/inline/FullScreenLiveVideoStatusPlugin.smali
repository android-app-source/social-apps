.class public Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;
.super Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;
.source ""

# interfaces
.implements LX/2dD;


# annotations
.annotation build Lcom/facebook/common/internal/DoNotStrip;
.end annotation


# static fields
.field private static final A:LX/0Tn;


# instance fields
.field private B:LX/BwU;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final C:LX/BwS;

.field public D:Z

.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3Go;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3HT;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1b4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1833890
    sget-object v0, LX/0Tm;->g:LX/0Tn;

    const-string v1, "viewer_watching_video_broadcast_tool_tip_has_shown"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->A:LX/0Tn;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation build Lcom/facebook/common/internal/DoNotStrip;
    .end annotation

    .prologue
    .line 1833888
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1833889
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1833886
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/Integer;)V

    .line 1833887
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1833881
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833882
    const-class v0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1833883
    iget-object v0, p0, LX/2oy;->h:Ljava/util/List;

    new-instance v1, LX/BwT;

    invoke-direct {v1, p0}, LX/BwT;-><init>(Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1833884
    new-instance v0, LX/BwS;

    invoke-direct {v0, p0}, LX/BwS;-><init>(Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;)V

    iput-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->C:LX/BwS;

    .line 1833885
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/3Go;->b(LX/0QB;)LX/3Go;

    move-result-object v2

    check-cast v2, LX/3Go;

    invoke-static {p0}, LX/3HT;->a(LX/0QB;)LX/3HT;

    move-result-object v3

    check-cast v3, LX/3HT;

    invoke-static {p0}, LX/1b4;->b(LX/0QB;)LX/1b4;

    move-result-object p0

    check-cast p0, LX/1b4;

    iput-object v1, p1, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v2, p1, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->b:LX/3Go;

    iput-object v3, p1, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->c:LX/3HT;

    iput-object p0, p1, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->d:LX/1b4;

    return-void
.end method

.method private k()V
    .locals 6

    .prologue
    .line 1833874
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->b:LX/3Go;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->x:Ljava/lang/String;

    const/4 v2, 0x0

    .line 1833875
    if-nez v1, :cond_0

    .line 1833876
    :goto_0
    return-void

    .line 1833877
    :cond_0
    new-instance v3, Lcom/facebook/facecastdisplay/protocol/LiveVideosWatchingEventsLoggingMethod$Params;

    invoke-direct {v3, v1, v2}, Lcom/facebook/facecastdisplay/protocol/LiveVideosWatchingEventsLoggingMethod$Params;-><init>(Ljava/lang/String;I)V

    .line 1833878
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 1833879
    const-string v5, "liveVideoLogWatchTimeParamsKey"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1833880
    iget-object v3, v0, LX/3Go;->b:LX/0aG;

    const-string v5, "live_video_log_watch_time"

    const p0, 0x4b9da64b    # 2.0663446E7f

    invoke-static {v3, v5, v4, p0}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v3

    invoke-interface {v3}, LX/1MF;->start()LX/1ML;

    goto :goto_0
.end method

.method public static u(Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;)V
    .locals 2

    .prologue
    .line 1833819
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->a(Z)V

    .line 1833820
    invoke-virtual {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->h()V

    .line 1833821
    return-void
.end method


# virtual methods
.method public final a(LX/2oN;)V
    .locals 2

    .prologue
    .line 1833870
    sget-object v0, LX/BwR;->b:[I

    invoke-virtual {p1}, LX/2oN;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1833871
    :goto_0
    return-void

    .line 1833872
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    sget-object v1, LX/3Hg;->LIVE:LX/3Hg;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIndicatorType(LX/3Hg;)V

    goto :goto_0

    .line 1833873
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    sget-object v1, LX/3Hg;->VIEWER_COMMERCIAL_BREAK_FULLSCREEN:LX/3Hg;

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setIndicatorType(LX/3Hg;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/2pa;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1833840
    invoke-super {p0, p1, p2}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->a(LX/2pa;Z)V

    .line 1833841
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->A:LX/0Tn;

    invoke-interface {v0, v3, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v3

    .line 1833842
    invoke-static {p1}, LX/393;->d(LX/2pa;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v4

    .line 1833843
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->au()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE_STOPPED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-eq v0, v5, :cond_0

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLMedia;->s()Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->SEAL_STARTED:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v0, v5, :cond_4

    :cond_0
    move v0, v1

    .line 1833844
    :goto_0
    if-nez v3, :cond_1

    if-eqz v0, :cond_5

    :cond_1
    invoke-static {p1}, LX/393;->b(LX/2pa;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-static {v0}, LX/18J;->a(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1833845
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->k()V

    .line 1833846
    :goto_1
    if-eqz p2, :cond_2

    .line 1833847
    invoke-static {p0}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->u(Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;)V

    .line 1833848
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->c:LX/3HT;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->C:LX/BwS;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1833849
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->setAlpha(F)V

    .line 1833850
    iput-boolean v2, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->D:Z

    .line 1833851
    :cond_2
    invoke-static {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w(Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1833852
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    .line 1833853
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->e:Landroid/animation/ObjectAnimator;

    move-object v0, v1

    .line 1833854
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1833855
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->c:Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;

    .line 1833856
    iget-object v1, v0, Lcom/facebook/facecastdisplay/LiveVideoStatusMessage;->a:Landroid/animation/ObjectAnimator;

    move-object v0, v1

    .line 1833857
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1833858
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 1833859
    goto :goto_0

    .line 1833860
    :cond_5
    new-instance v0, LX/BwU;

    invoke-virtual {p0}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v5, 0x2

    invoke-direct {v0, v3, v5}, LX/BwU;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    .line 1833861
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    const/16 v3, 0x1f40

    .line 1833862
    iput v3, v0, LX/0hs;->t:I

    .line 1833863
    iget-object v3, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->d:LX/1b4;

    invoke-virtual {v0, v4}, LX/1b4;->a(Lcom/facebook/graphql/model/GraphQLMedia;)Z

    move-result v0

    if-eqz v0, :cond_6

    const v0, 0x7f080dd5

    :goto_2
    invoke-virtual {v3, v0}, LX/0hs;->b(I)V

    .line 1833864
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0, v3}, LX/0ht;->c(Landroid/view/View;)V

    .line 1833865
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    .line 1833866
    iput-object p0, v0, LX/0ht;->H:LX/2dD;

    .line 1833867
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    invoke-virtual {v0}, LX/0ht;->d()V

    .line 1833868
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v3, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->A:LX/0Tn;

    invoke-interface {v0, v3, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    goto :goto_1

    .line 1833869
    :cond_6
    const v0, 0x7f080dd4

    goto :goto_2
.end method

.method public final a(LX/2qV;)V
    .locals 0

    .prologue
    .line 1833839
    return-void
.end method

.method public final a(LX/0ht;)Z
    .locals 1

    .prologue
    .line 1833837
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->k()V

    .line 1833838
    const/4 v0, 0x1

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1833826
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    .line 1833827
    iget-boolean v1, v0, LX/0ht;->r:Z

    move v0, v1

    .line 1833828
    if-eqz v0, :cond_0

    .line 1833829
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    const/4 v1, 0x0

    .line 1833830
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 1833831
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->B:LX/BwU;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 1833832
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->w:Lcom/facebook/facecastdisplay/LiveVideoStatusView;

    invoke-virtual {v0}, Lcom/facebook/facecastdisplay/LiveVideoStatusView;->d()V

    .line 1833833
    invoke-virtual {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->i()V

    .line 1833834
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->c:LX/3HT;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->C:LX/BwS;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1833835
    invoke-super {p0}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->d()V

    .line 1833836
    return-void
.end method

.method public getLayout()I
    .locals 1

    .prologue
    .line 1833825
    const v0, 0x7f03072d

    return v0
.end method

.method public final s_(I)V
    .locals 2

    .prologue
    .line 1833822
    invoke-super {p0, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;->s_(I)V

    .line 1833823
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FullScreenLiveVideoStatusPlugin;->c:LX/3HT;

    new-instance v1, LX/Adf;

    invoke-direct {v1, p1}, LX/Adf;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1833824
    return-void
.end method
