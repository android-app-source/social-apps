.class public final Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

.field public final synthetic c:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;

.field public final synthetic d:LX/Bwe;


# direct methods
.method public constructor <init>(LX/Bwe;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)V
    .locals 0

    .prologue
    .line 1834130
    iput-object p1, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->d:LX/Bwe;

    iput-object p2, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->b:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iput-object p4, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->c:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1834120
    iget-object v0, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->d:LX/Bwe;

    iget-object v0, v0, LX/Bwe;->a:LX/Bwf;

    iget-object v0, v0, LX/Bwf;->f:Ljava/util/Map;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1834121
    if-nez v0, :cond_1

    .line 1834122
    :cond_0
    :goto_0
    return-void

    .line 1834123
    :cond_1
    invoke-static {v0}, LX/17E;->v(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    .line 1834124
    if-eqz v1, :cond_0

    .line 1834125
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLMedia;->ao()Z

    move-result v1

    .line 1834126
    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->b:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;->LIVE:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    if-ne v1, v2, :cond_2

    .line 1834127
    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->d:LX/Bwe;

    iget-object v1, v1, LX/Bwe;->a:LX/Bwf;

    iget-object v1, v1, LX/Bwf;->g:LX/03V;

    sget-object v2, LX/Bwf;->a:Ljava/lang/String;

    const-string v3, "Live vod video turned into live again. "

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1834128
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->d:LX/Bwe;

    iget-object v1, v1, LX/Bwe;->a:LX/Bwf;

    iget-object v1, v1, LX/Bwf;->d:LX/3Hf;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->b:Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;

    iget-object v3, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->c:Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;

    invoke-virtual {v1, v0, v2, v3}, LX/3Hf;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/GraphQLVideoBroadcastStatus;Lcom/facebook/video/videohome/protocol/VideoHomeSubscriptionsModels$LiveVideoBroadcastStatusUpdateStoryFragmentModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1834129
    iget-object v1, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->d:LX/Bwe;

    iget-object v1, v1, LX/Bwe;->a:LX/Bwf;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/LiveVideoBroadcastStatusManager$LiveVideoBroadcastStatusListener$1;->a:Ljava/lang/String;

    invoke-static {v1, v2, v0}, LX/Bwf;->a$redex0(LX/Bwf;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method
