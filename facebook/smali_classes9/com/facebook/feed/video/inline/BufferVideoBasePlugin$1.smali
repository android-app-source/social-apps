.class public final Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2fs;

.field public final synthetic c:LX/BwQ;


# direct methods
.method public constructor <init>(LX/BwQ;Ljava/lang/String;LX/2fs;)V
    .locals 0

    .prologue
    .line 1833601
    iput-object p1, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    iput-object p2, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->b:LX/2fs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 1833602
    iget-object v0, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    iget-object v1, v1, LX/BwQ;->A:LX/2pa;

    invoke-static {v1}, LX/393;->f(LX/2pa;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1833603
    const/4 v0, 0x0

    .line 1833604
    iget-object v1, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->b:LX/2fs;

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    sget-object v2, LX/1A0;->DOWNLOAD_IN_PROGRESS:LX/1A0;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->b:LX/2fs;

    iget-object v1, v1, LX/2fs;->c:LX/1A0;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    iget-object v2, v2, LX/BwQ;->y:LX/2fs;

    iget-object v2, v2, LX/2fs;->c:LX/1A0;

    if-ne v1, v2, :cond_0

    .line 1833605
    const/4 v0, 0x1

    .line 1833606
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->b:LX/2fs;

    .line 1833607
    iput-object v2, v1, LX/BwQ;->y:LX/2fs;

    .line 1833608
    if-eqz v0, :cond_2

    .line 1833609
    iget-object v0, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    iget-object v0, v0, LX/BwQ;->u:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    .line 1833610
    invoke-static {v1}, LX/BwQ;->u(LX/BwQ;)Ljava/lang/String;

    move-result-object v2

    move-object v1, v2

    .line 1833611
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833612
    iget-object v0, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    iget-object v0, v0, LX/BwQ;->v:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    invoke-static {v1}, LX/BwQ;->getProgressString(LX/BwQ;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1833613
    iget-object v0, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    iget-object v0, v0, LX/BwQ;->t:Lcom/facebook/widget/FacebookProgressCircleViewAnimated;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    invoke-static {v1}, LX/BwQ;->getProgressPercent(LX/BwQ;)F

    move-result v1

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/facebook/widget/FacebookProgressCircleView;->setProgress(J)V

    .line 1833614
    :cond_1
    :goto_0
    return-void

    .line 1833615
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/video/inline/BufferVideoBasePlugin$1;->c:LX/BwQ;

    .line 1833616
    invoke-static {v0}, LX/BwQ;->v(LX/BwQ;)V

    .line 1833617
    goto :goto_0
.end method
