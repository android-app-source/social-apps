.class public Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;
.super Lcom/facebook/video/player/InlineVideoPlayer2;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public c:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0xX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/3GZ;

.field private h:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1833785
    const-class v0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1833788
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833789
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1833786
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833787
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1833760
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/video/player/InlineVideoPlayer2;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1833761
    const-class v0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;

    invoke-static {v0, p0}, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1833762
    invoke-direct {p0, p1}, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->a(Landroid/content/Context;)V

    .line 1833763
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 1833770
    new-instance v0, LX/3GZ;

    invoke-direct {v0, p1}, LX/3GZ;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->g:LX/3GZ;

    .line 1833771
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->g:LX/3GZ;

    .line 1833772
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833773
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/video/commercialbreak/plugins/LiveCommercialBreakPlugin;-><init>(Landroid/content/Context;)V

    .line 1833774
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833775
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;

    sget-object v2, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-direct {v1, p1, v2}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdTransitionPlugin;-><init>(Landroid/content/Context;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1833776
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833777
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/video/commercialbreak/plugins/InstreamVideoAdPlugin;-><init>(Landroid/content/Context;)V

    .line 1833778
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833779
    invoke-direct {p0}, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1833780
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, LX/3Gm;

    invoke-direct {v1, p1}, LX/3Gm;-><init>(Landroid/content/Context;)V

    .line 1833781
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833782
    :cond_0
    iget-object v0, p0, Lcom/facebook/video/player/InlineVideoPlayer2;->a:Lcom/facebook/video/player/RichVideoPlayer;

    new-instance v1, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;

    invoke-direct {v1, p1}, Lcom/facebook/feed/video/inline/LiveVideoStatusPlugin;-><init>(Landroid/content/Context;)V

    .line 1833783
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 1833784
    return-void
.end method

.method private static a(Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;LX/0ad;LX/0Uh;LX/0xX;)V
    .locals 0

    .prologue
    .line 1833790
    iput-object p1, p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->c:LX/0ad;

    iput-object p2, p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->d:LX/0Uh;

    iput-object p3, p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->e:LX/0xX;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;

    invoke-static {v2}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {v2}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {v2}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v2

    check-cast v2, LX/0xX;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->a(Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;LX/0ad;LX/0Uh;LX/0xX;)V

    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 1833769
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->e:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public setOnClickPlayerListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1833764
    invoke-super {p0, p1}, Lcom/facebook/video/player/InlineVideoPlayer2;->setOnClickPlayerListener(Landroid/view/View$OnClickListener;)V

    .line 1833765
    iput-object p1, p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->h:Landroid/view/View$OnClickListener;

    .line 1833766
    iget-object v0, p0, Lcom/facebook/feed/video/inline/FeedInlineVideoPlayer2;->g:LX/3GZ;

    .line 1833767
    iput-object p1, v0, LX/3GZ;->o:Landroid/view/View$OnClickListener;

    .line 1833768
    return-void
.end method
