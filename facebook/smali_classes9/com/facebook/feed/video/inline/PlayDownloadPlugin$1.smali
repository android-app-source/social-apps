.class public final Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Bwm;


# direct methods
.method public constructor <init>(LX/Bwm;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1834236
    iput-object p1, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    iput-object p2, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1834214
    iget-object v0, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    if-eqz v0, :cond_1

    .line 1834215
    iget-object v0, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    iget-object v0, v0, LX/2oy;->j:LX/2pb;

    .line 1834216
    iget-object v1, v0, LX/2pb;->y:LX/2qV;

    move-object v0, v1

    .line 1834217
    iget-object v1, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    iget-object v2, v2, LX/Bwm;->u:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/2qV;->ATTEMPT_TO_PLAY:LX/2qV;

    if-ne v0, v1, :cond_1

    .line 1834218
    iget-object v0, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    iget-object v0, v0, LX/Bwm;->q:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1834219
    iget-object v0, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    iget-object v0, v0, LX/Bwm;->r:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1834220
    iget-object v0, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    .line 1834221
    iget-object v3, v0, LX/BwQ;->b:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->C()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, LX/BwQ;->b:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->g()J

    move-result-wide v3

    iget-object v5, v0, LX/BwQ;->A:LX/2pa;

    invoke-static {v5}, LX/393;->e(LX/2pa;)J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_2

    .line 1834222
    :cond_0
    iget-object v3, v0, LX/BwQ;->w:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1834223
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    iget-boolean v0, v0, LX/Bwm;->w:Z

    if-nez v0, :cond_1

    .line 1834224
    iget-object v0, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    iget-object v0, v0, LX/Bwm;->o:LX/19v;

    iget-object v1, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->a:Ljava/lang/String;

    .line 1834225
    const/4 v2, 0x0

    const/4 v3, 0x0

    :try_start_0
    sget-object v4, LX/7Jd;->BUFFERING_OPTION_SHOWN:LX/7Jd;

    invoke-static {v0, v1, v2, v3, v4}, LX/19v;->a(LX/19v;Ljava/lang/String;LX/7Jg;LX/7Jf;LX/7Jd;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 1834226
    sget-object v3, LX/7Je;->DOWNLOAD_TYPE:LX/7Je;

    iget-object v3, v3, LX/7Je;->value:Ljava/lang/String;

    sget-object v4, LX/7Jc;->EPHEMERAL:LX/7Jc;

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1834227
    iget-object v3, v0, LX/19v;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1834228
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/video/inline/PlayDownloadPlugin$1;->b:LX/Bwm;

    const/4 v1, 0x1

    .line 1834229
    iput-boolean v1, v0, LX/Bwm;->w:Z

    .line 1834230
    :cond_1
    return-void

    .line 1834231
    :cond_2
    iget-object v3, v0, LX/BwQ;->w:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1834232
    iget-boolean v3, v0, LX/BwQ;->B:Z

    if-nez v3, :cond_3

    iget-object v3, v0, LX/BwQ;->b:LX/0tQ;

    invoke-virtual {v3}, LX/0tQ;->L()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1834233
    const/4 v3, 0x1

    iput-boolean v3, v0, LX/BwQ;->B:Z

    .line 1834234
    iget-object v3, v0, LX/BwQ;->q:Landroid/view/View$OnClickListener;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 1834235
    :cond_3
    invoke-static {v0}, LX/BwQ;->v(LX/BwQ;)V

    goto :goto_0

    :catch_0
    goto :goto_1
.end method
