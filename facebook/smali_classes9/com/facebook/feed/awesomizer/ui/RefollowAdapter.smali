.class public Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;
.super LX/Aij;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Aij",
        "<",
        "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final e:LX/Aie;

.field public final f:LX/11S;

.field private final g:LX/0wL;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1707301
    const-class v0, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;

    const-string v1, "feed_awesomizer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Aie;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/11S;LX/17Y;LX/0wL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1707253
    invoke-direct {p0, p2, p3, p5}, LX/Aij;-><init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 1707254
    iput-object p1, p0, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->e:LX/Aie;

    .line 1707255
    iput-object p4, p0, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->f:LX/11S;

    .line 1707256
    iput-object p6, p0, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->g:LX/0wL;

    .line 1707257
    return-void
.end method

.method private a(Ljava/lang/Object;Landroid/view/View;)V
    .locals 11

    .prologue
    .line 1707258
    check-cast p1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    move-object v0, p2

    .line 1707259
    check-cast v0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;

    .line 1707260
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 1707261
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->j()J

    move-result-wide v5

    .line 1707262
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v7, v8, :cond_1

    .line 1707263
    iget-object v7, p0, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->f:LX/11S;

    sget-object v8, LX/1lB;->FUZZY_RELATIVE_DATE_STYLE:LX/1lB;

    const-wide/16 v9, 0x3e8

    mul-long/2addr v5, v9

    invoke-interface {v7, v8, v5, v6}, LX/11S;->a(LX/1lB;J)Ljava/lang/String;

    move-result-object v5

    .line 1707264
    :goto_0
    move-object v1, v5

    .line 1707265
    invoke-virtual {v0, v1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarContextViewText(Ljava/lang/String;)V

    .line 1707266
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Z)V

    .line 1707267
    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v2

    invoke-static {v2}, LX/AiP;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    sget-object v4, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v0, v1, v2, v3, v4}, LX/Aif;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 1707268
    return-void

    .line 1707269
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    const v5, 0x7f08239d

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method private h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;
    .locals 2

    .prologue
    .line 1707270
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707271
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-lez p1, :cond_0

    .line 1707272
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707273
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->j()LX/0Px;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    .line 1707274
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 1707275
    instance-of v0, p1, LX/Aii;

    if-eqz v0, :cond_0

    .line 1707276
    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    move-result-object v0

    check-cast p1, LX/Aii;

    .line 1707277
    iget-object v1, p1, LX/Aii;->l:Landroid/view/View;

    move-object v1, v1

    .line 1707278
    invoke-direct {p0, v0, v1}, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->a(Ljava/lang/Object;Landroid/view/View;)V

    .line 1707279
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1707280
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707281
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    .line 1707282
    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->e:LX/Aie;

    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    move-result-object v4

    new-instance v5, LX/Aix;

    invoke-direct {v5, p0, v0}, LX/Aix;-><init>(Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;)V

    invoke-virtual {v3, v0, v4, v5}, LX/Aie;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;LX/0TF;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    move-result-object v0

    .line 1707283
    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1707284
    :cond_0
    :goto_0
    return-void

    .line 1707285
    :cond_1
    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    .line 1707286
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->k()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1707287
    invoke-virtual {v5, v4, v1}, LX/15i;->h(II)Z

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->a()I

    move-result v5

    invoke-virtual {p0, v0, v4, v5}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    .line 1707288
    iget-object v4, p0, LX/Aij;->a:LX/Aiv;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v3, v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, p2, v0}, LX/Aiv;->a(IZ)V

    .line 1707289
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;->g:LX/0wL;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v3, v4, :cond_3

    :goto_2
    invoke-static {p1, v1}, LX/Aij;->a(Landroid/view/View;Z)Ljava/lang/String;

    move-result-object v1

    .line 1707290
    invoke-static {v0, p1, v1}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1707291
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1707292
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1707293
    goto :goto_2
.end method

.method public final c(Landroid/view/ViewGroup;)LX/Aii;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;",
            ">.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1707294
    new-instance v0, LX/Aii;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030156

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aii;-><init>(LX/Aij;Landroid/view/View;)V

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1707295
    sget-object v0, LX/AiW;->REFOLLOW:LX/AiW;

    invoke-virtual {v0}, LX/AiW;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1707296
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707297
    if-eqz v0, :cond_0

    .line 1707298
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707299
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1707300
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
