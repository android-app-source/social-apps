.class public Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;
.super LX/Aij;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Aij",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;",
        ">;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final d:Ljava/lang/String;

.field public static final e:Lcom/facebook/common/callercontext/CallerContext;

.field public static f:Ljava/lang/String;


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:LX/Aie;

.field private final i:Landroid/content/ComponentName;

.field private final j:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1707037
    const-class v0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->d:Ljava/lang/String;

    .line 1707038
    const-class v0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;

    const-string v1, "feed_awesomizer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Aie;Landroid/content/ComponentName;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .param p3    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1707031
    invoke-direct {p0, p4, p5, p6}, LX/Aij;-><init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 1707032
    iput-object p1, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->g:Landroid/content/Context;

    .line 1707033
    iput-object p2, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->h:LX/Aie;

    .line 1707034
    iput-object p3, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->i:Landroid/content/ComponentName;

    .line 1707035
    iput-object p5, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->j:Lcom/facebook/content/SecureContextHelper;

    .line 1707036
    return-void
.end method

.method private h(I)LX/Aiq;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1707013
    invoke-virtual {p0, p1}, LX/Aij;->e(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1707014
    :goto_0
    return-object v0

    .line 1707015
    :cond_0
    invoke-virtual {p0, p1}, LX/Aij;->f(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1707016
    add-int/lit8 v0, p1, -0x1

    div-int/lit8 v1, v0, 0x7

    .line 1707017
    new-instance v2, LX/Aiq;

    sget-object v3, LX/Air;->TOPIC_HEADER:LX/Air;

    .line 1707018
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707019
    check-cast v0, LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, LX/Aiq;-><init>(LX/Air;Ljava/lang/Object;)V

    move-object v0, v2

    .line 1707020
    goto :goto_0

    .line 1707021
    :cond_1
    invoke-virtual {p0, p1}, LX/Aij;->g(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1707022
    new-instance v1, LX/Aiq;

    sget-object v2, LX/Air;->SEE_MORE:LX/Air;

    invoke-direct {v1, v2, v0}, LX/Aiq;-><init>(LX/Air;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_0

    .line 1707023
    :cond_2
    rem-int/lit8 v2, p1, 0x7

    .line 1707024
    sub-int v0, p1, v2

    div-int/lit8 v1, v0, 0x7

    .line 1707025
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707026
    check-cast v0, LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;->l()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel;

    move-result-object v0

    .line 1707027
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel;->a()I

    move-result v1

    if-nez v1, :cond_4

    .line 1707028
    :cond_3
    const/4 v0, 0x0

    .line 1707029
    :goto_1
    move-object v0, v0

    .line 1707030
    goto :goto_0

    :cond_4
    new-instance v1, LX/Aiq;

    sget-object v3, LX/Air;->PAGE_PROFILE:LX/Air;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel;->j()LX/0Px;

    move-result-object v0

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-direct {v1, v3, v0}, LX/Aiq;-><init>(LX/Air;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public static m(I)I
    .locals 1

    .prologue
    .line 1707012
    div-int/lit8 v0, p0, 0x7

    add-int/lit8 v0, v0, -0x1

    return v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)LX/Aii;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;",
            ">;>.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1707011
    new-instance v0, LX/Aii;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030155

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aii;-><init>(LX/Aij;Landroid/view/View;)V

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 1706981
    instance-of v0, p1, LX/Aii;

    if-eqz v0, :cond_0

    .line 1706982
    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->h(I)LX/Aiq;

    move-result-object v0

    check-cast p1, LX/Aii;

    .line 1706983
    iget-object v1, p1, LX/Aii;->l:Landroid/view/View;

    move-object v1, v1

    .line 1706984
    sget-object v2, LX/Aip;->a:[I

    iget-object v3, v0, LX/Aiq;->a:LX/Air;

    invoke-virtual {v3}, LX/Air;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 1706985
    iget-object v2, p0, LX/Aij;->c:LX/03V;

    sget-object v3, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Invalid discover section type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v0, LX/Aiq;->a:LX/Air;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " in the awesomizer discover adapter"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1706986
    :cond_0
    :goto_0
    return-void

    .line 1706987
    :pswitch_0
    iget-object v2, v0, LX/Aiq;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 1706988
    check-cast v1, Lcom/facebook/feed/awesomizer/ui/AwesomizerTopicHeaderView;

    invoke-virtual {v1, v2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerTopicHeaderView;->setTopicHeaderTitle(Ljava/lang/String;)V

    .line 1706989
    goto :goto_0

    .line 1706990
    :pswitch_1
    iget-object v2, v0, LX/Aiq;->b:Ljava/lang/Object;

    check-cast v2, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    const/4 v5, 0x0

    .line 1706991
    move-object v3, v1

    check-cast v3, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;

    .line 1706992
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 1706993
    invoke-virtual {v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v4

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v4, v7, :cond_1

    const/4 v4, 0x1

    .line 1706994
    :goto_1
    invoke-virtual {v3, v4}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Z)V

    .line 1706995
    if-eqz v4, :cond_2

    const v4, 0x7f0823a0

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1706996
    :goto_2
    invoke-virtual {v3, v4}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarContextViewText(Ljava/lang/String;)V

    .line 1706997
    invoke-virtual {v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1706998
    invoke-virtual {v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;->n()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    const v6, 0x25d6af

    sget-object p1, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v3, v7, v4, v6, p1}, LX/Aif;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 1706999
    invoke-virtual {v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;->k()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    move-result-object v4

    if-nez v4, :cond_3

    .line 1707000
    :goto_3
    goto :goto_0

    .line 1707001
    :pswitch_2
    check-cast v1, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;

    const-string v2, "More"

    const/4 v3, 0x0

    const v4, 0x25d6af

    sget-object v5, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v1, v2, v3, v4, v5}, LX/Aif;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 1707002
    goto :goto_0

    :cond_1
    move v4, v5

    .line 1707003
    goto :goto_1

    .line 1707004
    :cond_2
    const-string v4, ""

    goto :goto_2

    .line 1707005
    :cond_3
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v7

    .line 1707006
    invoke-virtual {v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;->k()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;->j()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result p2

    move v6, v5

    :goto_4
    if-ge v6, p2, :cond_5

    invoke-virtual {p1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel$NodesModel;

    .line 1707007
    invoke-virtual {v4}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel$NodesModel;->a()LX/1vs;

    move-result-object p0

    iget p0, p0, LX/1vs;->b:I

    if-eqz p0, :cond_4

    .line 1707008
    invoke-virtual {v4}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel$NodesModel;->a()LX/1vs;

    move-result-object v4

    iget-object p0, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {p0, v4, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1707009
    :cond_4
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_4

    .line 1707010
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;->k()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;->a()I

    move-result v4

    invoke-virtual {v3, v7, v4}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Ljava/util/List;I)V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 1706966
    invoke-virtual {p0, p2}, LX/Aij;->f(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1706967
    :goto_0
    return-void

    .line 1706968
    :cond_0
    invoke-virtual {p0, p2}, LX/Aij;->g(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1706969
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->i:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->AWESOMIZER_DISCOVER_TOPIC_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "session_id"

    sget-object v2, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "topic_id"

    .line 1706970
    iget-object v2, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v2, v2

    .line 1706971
    check-cast v2, LX/0Px;

    invoke-static {p2}, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->m(I)I

    move-result v3

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;

    invoke-virtual {v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;->j()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1706972
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "topic_name"

    .line 1706973
    iget-object v2, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v2, v2

    .line 1706974
    check-cast v2, LX/0Px;

    invoke-static {p2}, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->m(I)I

    move-result v3

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;

    invoke-virtual {v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;->k()Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 1706975
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1706976
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->j:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->g:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 1706977
    :cond_1
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1706978
    check-cast v0, LX/0Px;

    .line 1706979
    iget-object v2, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->h:LX/Aie;

    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->h(I)LX/Aiq;

    move-result-object v1

    iget-object v1, v1, LX/Aiq;->b:Ljava/lang/Object;

    check-cast v1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    new-instance v3, LX/Aio;

    invoke-direct {v3, p0, v0}, LX/Aio;-><init>(Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;LX/0Px;)V

    invoke-virtual {v2, v0, v1, v3}, LX/Aie;->a(LX/0Px;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;LX/0TF;)LX/0Px;

    move-result-object v0

    .line 1706980
    const/4 v1, 0x0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    goto/16 :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;)LX/Aii;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;",
            ">;>.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1706954
    new-instance v0, LX/Aii;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030156

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aii;-><init>(LX/Aij;Landroid/view/View;)V

    return-object v0
.end method

.method public final c(Landroid/view/ViewGroup;)LX/Aii;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;",
            ">;>.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1706965
    new-instance v0, LX/Aii;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030156

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aii;-><init>(LX/Aij;Landroid/view/View;)V

    return-object v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 1706964
    add-int/lit8 v0, p1, -0x1

    rem-int/lit8 v0, v0, 0x7

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1706963
    sget-object v0, LX/AiW;->DISCOVER:LX/AiW;

    invoke-virtual {v0}, LX/AiW;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g(I)Z
    .locals 1

    .prologue
    .line 1706962
    rem-int/lit8 v0, p1, 0x7

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 1706955
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1706956
    if-nez v0, :cond_0

    .line 1706957
    const/4 v0, 0x1

    .line 1706958
    :goto_0
    return v0

    .line 1706959
    :cond_0
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1706960
    check-cast v0, LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1706961
    mul-int/lit8 v1, v0, 0x6

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
