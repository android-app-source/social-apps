.class public abstract Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/facebook/base/fragment/FbFragment;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field public b:LX/0Vd;

.field public c:Ljava/lang/String;

.field public d:LX/3wu;

.field public e:Landroid/support/v7/widget/RecyclerView;

.field public f:D

.field public g:I

.field public h:LX/Aij;

.field public i:LX/Aiv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/1K9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1706857
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1706858
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->f:D

    .line 1706859
    const/4 v0, 0x0

    iput v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->g:I

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    invoke-static {p0}, LX/Aiv;->b(LX/0QB;)LX/Aiv;

    move-result-object v1

    check-cast v1, LX/Aiv;

    invoke-static {p0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object p0

    check-cast p0, LX/1K9;

    iput-object v1, p1, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    iput-object p0, p1, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->j:LX/1K9;

    return-void
.end method


# virtual methods
.method public abstract a(I)I
.end method

.method public abstract a(LX/0Vd;)V
.end method

.method public a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1706932
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 1706933
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1706934
    const v2, 0x7f01077a

    invoke-virtual {v0, v2, v1, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1706935
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1706936
    if-eqz v0, :cond_0

    .line 1706937
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-interface {v0, v1}, LX/1ZF;->x_(I)V

    .line 1706938
    invoke-interface {v0, v3}, LX/1ZF;->k_(Z)V

    .line 1706939
    :cond_0
    return-void
.end method

.method public abstract a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)V"
        }
    .end annotation
.end method

.method public b()I
    .locals 3

    .prologue
    const/16 v0, 0x63

    .line 1706926
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1706927
    iget-object v2, v1, LX/Aij;->g:Ljava/lang/Object;

    move-object v1, v2

    .line 1706928
    if-nez v1, :cond_0

    .line 1706929
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1706930
    iget v2, v1, LX/Aij;->i:I

    invoke-virtual {v1}, LX/1OM;->ij_()I

    move-result p0

    sub-int/2addr v2, p0

    add-int/lit8 v2, v2, 0x1

    move v1, v2

    .line 1706931
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public abstract c()I
.end method

.method public abstract d()V
.end method

.method public abstract e()LX/AiW;
.end method

.method public abstract k()I
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3b311cfe

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1706921
    const-class v0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;

    invoke-static {v0, p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1706922
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "session_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->c:Ljava/lang/String;

    .line 1706923
    new-instance v0, LX/Ain;

    invoke-direct {v0, p0}, LX/Ain;-><init>(Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;)V

    move-object v0, v0

    .line 1706924
    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->b:LX/0Vd;

    .line 1706925
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->c()I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030153

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x2b

    const v3, 0x20dde7a7

    invoke-static {v4, v2, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method

.method public final onDestroy()V
    .locals 15

    .prologue
    const/4 v8, 0x2

    const/16 v0, 0x2a

    const v1, 0x66911bbf

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 1706891
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1706892
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e()LX/AiW;

    move-result-object v1

    iget-wide v2, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->f:D

    iget-object v4, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v4}, LX/1OM;->ij_()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->g:I

    iget-object v6, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->c:Ljava/lang/String;

    const-wide v13, 0x408f400000000000L    # 1000.0

    .line 1706893
    new-instance v9, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v10, "feed_awesomizer_card_close"

    invoke-direct {v9, v10}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1706894
    const-string v10, "feed_awesomizer"

    .line 1706895
    iput-object v10, v9, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1706896
    const-string v10, "awesomizer_session_identifier"

    invoke-virtual {v9, v10, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706897
    const-string v10, "card_time_spent"

    iget-wide v11, v0, LX/Aiv;->l:J

    long-to-double v11, v11

    div-double/2addr v11, v13

    invoke-virtual {v9, v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706898
    const-string v10, "identifier"

    invoke-virtual {v1}, LX/AiW;->getIndex()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706899
    const-string v10, "initial_network_load_time"

    iget-wide v11, v0, LX/Aiv;->f:J

    long-to-double v11, v11

    div-double/2addr v11, v13

    invoke-virtual {v9, v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706900
    const-string v10, "max_scroll_distance"

    invoke-virtual {v9, v10, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706901
    const-string v10, "name"

    invoke-virtual {v1}, LX/AiW;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706902
    const-string v10, "profiles_appeared_count"

    invoke-virtual {v9, v10, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706903
    const-string v10, "profiles_selected_count"

    iget-object v11, v0, LX/Aiv;->o:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->size()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706904
    const-string v10, "profiles_selected_array"

    iget-object v11, v0, LX/Aiv;->o:Ljava/util/Set;

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706905
    const-string v10, "profiles_tapped_array"

    iget-object v11, v0, LX/Aiv;->n:Ljava/util/List;

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706906
    const-string v10, "profiles_tapped_count"

    iget-object v11, v0, LX/Aiv;->n:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706907
    const-string v10, "profiles_total_count"

    invoke-virtual {v9, v10, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706908
    const-string v10, "profiles_unselected_count"

    iget-object v11, v0, LX/Aiv;->p:Ljava/util/Set;

    invoke-interface {v11}, Ljava/util/Set;->size()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706909
    const-string v10, "profiles_unselected_array"

    iget-object v11, v0, LX/Aiv;->p:Ljava/util/Set;

    invoke-virtual {v9, v10, v11}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706910
    const-string v10, "total_time_spent"

    iget-wide v11, v0, LX/Aiv;->l:J

    long-to-double v11, v11

    div-double/2addr v11, v13

    invoke-virtual {v9, v10, v11, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706911
    iget-object v10, v0, LX/Aiv;->a:LX/0Zb;

    invoke-interface {v10, v9}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1706912
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->j:LX/1K9;

    new-instance v1, LX/6Vr;

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->k()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    .line 1706913
    iget-wide v10, v3, LX/Aiv;->l:J

    const-wide/16 v12, 0x2710

    cmp-long v10, v10, v12

    if-ltz v10, :cond_2

    const/4 v10, 0x1

    :goto_0
    move v9, v10

    .line 1706914
    if-nez v9, :cond_0

    iget-object v9, v3, LX/Aiv;->o:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, v3, LX/Aiv;->p:Ljava/util/Set;

    invoke-interface {v9}, Ljava/util/Set;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_1

    .line 1706915
    :cond_0
    const/4 v9, 0x1

    .line 1706916
    :goto_1
    move v3, v9

    .line 1706917
    iget-object v4, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    .line 1706918
    iget-wide v9, v4, LX/Aiv;->l:J

    move-wide v4, v9

    .line 1706919
    invoke-direct {v1, v2, v3, v4, v5}, LX/6Vr;-><init>(IZJ)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 1706920
    const/16 v0, 0x2b

    const v1, -0x1072afe7

    invoke-static {v8, v0, v1, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_1
    const/4 v9, 0x0

    goto :goto_1

    :cond_2
    const/4 v10, 0x0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x31040568

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1706888
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1706889
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    invoke-virtual {v1}, LX/Aiv;->d()V

    .line 1706890
    const/16 v1, 0x2b

    const v2, -0x69fc4032

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x23708a0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1706885
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1706886
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    invoke-virtual {v1}, LX/Aiv;->e()V

    .line 1706887
    const/16 v1, 0x2b

    const v2, 0x2017435c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1706860
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1706861
    invoke-virtual {p0, p1}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a(Landroid/view/View;)V

    .line 1706862
    const v0, 0x7f0d0636

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 1706863
    const v0, 0x7f0d05f7

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    .line 1706864
    new-instance v0, LX/3wu;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, LX/3wu;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d:LX/3wu;

    .line 1706865
    const v0, 0x7f0d0637

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1706866
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x0

    .line 1706867
    iput v1, v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->k:I

    .line 1706868
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->k:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 1706869
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d:LX/3wu;

    new-instance v1, LX/Aik;

    invoke-direct {v1, p0}, LX/Aik;-><init>(Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;)V

    .line 1706870
    iput-object v1, v0, LX/3wu;->h:LX/3wr;

    .line 1706871
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d:LX/3wu;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1706872
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1837

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 1706873
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, LX/Ail;

    invoke-direct {v1, p0}, LX/Ail;-><init>(Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setOnScrollListener(LX/1OX;)V

    .line 1706874
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    .line 1706875
    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 1706876
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 1706877
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d()V

    .line 1706878
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    .line 1706879
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1706880
    iput-object v0, v1, LX/Aij;->a:LX/Aiv;

    .line 1706881
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->i:LX/Aiv;

    .line 1706882
    iget-object v3, v0, LX/Aiv;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v3

    iput-wide v3, v0, LX/Aiv;->g:J

    .line 1706883
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->b:LX/0Vd;

    invoke-virtual {p0, v0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a(LX/0Vd;)V

    .line 1706884
    return-void
.end method
