.class public Lcom/facebook/feed/awesomizer/ui/AwesomizerTopicHeaderView;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1706738
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1706739
    invoke-direct {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerTopicHeaderView;->a()V

    .line 1706740
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1706741
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1706742
    invoke-direct {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerTopicHeaderView;->a()V

    .line 1706743
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1706744
    const v0, 0x7f030158

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1706745
    const v0, 0x7f0d0640

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerTopicHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 1706746
    return-void
.end method


# virtual methods
.method public setTopicHeaderTitle(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1706747
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerTopicHeaderView;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1706748
    return-void
.end method
