.class public Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Landroid/content/ComponentName;
    .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Aiv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/AiS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/17Y;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/AiR;

.field public g:Z

.field private final h:I

.field public final i:Ljava/util/Set;

.field public final j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1706340
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1706341
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->g:Z

    .line 1706342
    const/4 v0, 0x4

    iput v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->h:I

    .line 1706343
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->i:Ljava/util/Set;

    .line 1706344
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->j:Ljava/lang/String;

    .line 1706345
    return-void
.end method

.method private a(Landroid/view/View;II)V
    .locals 1

    .prologue
    .line 1706338
    new-instance v0, LX/AiU;

    invoke-direct {v0, p0, p3, p2}, LX/AiU;-><init>(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;II)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1706339
    return-void
.end method

.method public static a(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;Landroid/graphics/drawable/Drawable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1706334
    const v0, 0x7f0d1181

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 1706335
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    invoke-virtual {p1, v4, v4, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1706336
    invoke-virtual {v0, v3, p1, v3, v3}, Lcom/facebook/resources/ui/FbTextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1706337
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1706323
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1706324
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;

    invoke-static {v0}, LX/2lB;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-static {v0}, LX/Aiv;->b(LX/0QB;)LX/Aiv;

    move-result-object v4

    check-cast v4, LX/Aiv;

    const-class v5, LX/AiS;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/AiS;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object p1

    check-cast p1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v0

    check-cast v0, LX/17Y;

    iput-object v3, v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a:Landroid/content/ComponentName;

    iput-object v4, v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->b:LX/Aiv;

    iput-object v5, v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->c:LX/AiS;

    iput-object p1, v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->d:Lcom/facebook/content/SecureContextHelper;

    iput-object v0, v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->e:LX/17Y;

    .line 1706325
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->c:LX/AiS;

    new-instance v1, LX/AiT;

    invoke-direct {v1, p0}, LX/AiT;-><init>(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;)V

    .line 1706326
    new-instance v3, LX/AiR;

    invoke-static {v0}, LX/1K9;->a(LX/0QB;)LX/1K9;

    move-result-object v2

    check-cast v2, LX/1K9;

    invoke-direct {v3, v1, v2}, LX/AiR;-><init>(LX/0QK;LX/1K9;)V

    .line 1706327
    move-object v0, v3

    .line 1706328
    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->f:LX/AiR;

    .line 1706329
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->f:LX/AiR;

    .line 1706330
    const-class v1, LX/6Vr;

    new-instance v2, LX/AiQ;

    invoke-direct {v2, v0}, LX/AiQ;-><init>(LX/AiR;)V

    .line 1706331
    iget-object v3, v0, LX/AiR;->b:LX/1K9;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4, v2}, LX/1K9;->a(Ljava/lang/Class;Ljava/lang/Object;LX/6Ve;)LX/6Vi;

    move-result-object v3

    .line 1706332
    iget-object v4, v0, LX/AiR;->c:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1706333
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x79377bf4

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1706322
    const v1, 0x7f030651

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x2b

    const v3, -0x20a0d0da

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroy()V
    .locals 13

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0x3cf8ea9e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1706304
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1706305
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->b:LX/Aiv;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->j:Ljava/lang/String;

    const-wide v11, 0x408f400000000000L    # 1000.0

    .line 1706306
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "feed_awesomizer_home_close"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 1706307
    const-string v6, "feed_awesomizer"

    .line 1706308
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1706309
    const-string v6, "awesomizer_session_identifier"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706310
    const-string v6, "home_time_spent"

    iget-wide v7, v1, LX/Aiv;->j:J

    long-to-double v7, v7

    div-double/2addr v7, v11

    invoke-virtual {v5, v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706311
    const-string v6, "num_cards_available"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706312
    const-string v6, "num_cards_opened"

    iget v7, v1, LX/Aiv;->d:I

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706313
    const-string v6, "selected_cards"

    iget-object v7, v1, LX/Aiv;->m:Ljava/util/List;

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706314
    const-string v6, "time_to_select_first_card"

    iget-wide v7, v1, LX/Aiv;->e:J

    long-to-double v7, v7

    div-double/2addr v7, v11

    invoke-virtual {v5, v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706315
    const-string v6, "total_time_spent"

    iget-wide v7, v1, LX/Aiv;->j:J

    long-to-double v7, v7

    iget-wide v9, v1, LX/Aiv;->k:J

    long-to-double v9, v9

    add-double/2addr v7, v9

    div-double/2addr v7, v11

    invoke-virtual {v5, v6, v7, v8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;D)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 1706316
    iget-object v6, v1, LX/Aiv;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 1706317
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->f:LX/AiR;

    .line 1706318
    iget-object v2, v1, LX/AiR;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6Vi;

    .line 1706319
    iget-object v5, v1, LX/AiR;->b:LX/1K9;

    invoke-virtual {v5, v2}, LX/1K9;->a(LX/6Vi;)V

    goto :goto_0

    .line 1706320
    :cond_0
    iget-object v2, v1, LX/AiR;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1706321
    const/16 v1, 0x2b

    const v2, 0x4e59e1a7    # 9.1386106E8f

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x347e4ff4    # -1.6998424E7f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1706301
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1706302
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->b:LX/Aiv;

    invoke-virtual {v1}, LX/Aiv;->d()V

    .line 1706303
    const/16 v1, 0x2b

    const v2, -0x30ed0c3a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x34eb2e8e    # -9752946.0f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1706298
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1706299
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->b:LX/Aiv;

    invoke-virtual {v1}, LX/Aiv;->e()V

    .line 1706300
    const/16 v1, 0x2b

    const v2, 0x64022a7a

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x3b6231a0

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1706282
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onStart()V

    .line 1706283
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1706284
    if-eqz v0, :cond_0

    .line 1706285
    const v2, 0x7f082388

    invoke-interface {v0, v2}, LX/1ZF;->x_(I)V

    .line 1706286
    const/4 v2, 0x1

    invoke-interface {v0, v2}, LX/1ZF;->k_(Z)V

    .line 1706287
    :cond_0
    const/16 v0, 0x2b

    const v2, -0x3cbe4929

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1706288
    invoke-super {p0, p1, p2}, Lcom/facebook/base/fragment/FbFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 1706289
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1706290
    const v1, 0x7f020a4c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;Landroid/graphics/drawable/Drawable;)V

    .line 1706291
    const v0, 0x7f0d1182

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, LX/0cQ;->AWESOMIZER_SEEFIRST_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    sget-object v2, LX/AiW;->SEEFIRST:LX/AiW;

    invoke-virtual {v2}, LX/AiW;->getIndex()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a(Landroid/view/View;II)V

    .line 1706292
    const v0, 0x7f0d1183

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, LX/0cQ;->AWESOMIZER_UNFOLLOW_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    sget-object v2, LX/AiW;->UNFOLLOW:LX/AiW;

    invoke-virtual {v2}, LX/AiW;->getIndex()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a(Landroid/view/View;II)V

    .line 1706293
    const v0, 0x7f0d1184

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, LX/0cQ;->AWESOMIZER_REFOLLOW_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    sget-object v2, LX/AiW;->REFOLLOW:LX/AiW;

    invoke-virtual {v2}, LX/AiW;->getIndex()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a(Landroid/view/View;II)V

    .line 1706294
    const v0, 0x7f0d1185

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    sget-object v1, LX/0cQ;->AWESOMIZER_DISCOVER_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    sget-object v2, LX/AiW;->DISCOVER:LX/AiW;

    invoke-virtual {v2}, LX/AiW;->getIndex()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;->a(Landroid/view/View;II)V

    .line 1706295
    const v0, 0x7f0d1186

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    .line 1706296
    new-instance v1, LX/AiV;

    invoke-direct {v1, p0}, LX/AiV;-><init>(Lcom/facebook/feed/awesomizer/ui/AwesomizerFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1706297
    return-void
.end method
