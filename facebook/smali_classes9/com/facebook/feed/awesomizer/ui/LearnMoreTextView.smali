.class public Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;
.super Lcom/facebook/resources/ui/FbTextView;
.source ""

# interfaces
.implements LX/0wO;


# instance fields
.field private a:LX/2da;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1707237
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1707238
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1707239
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1707240
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1707241
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/FbTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1707242
    new-instance v0, LX/2da;

    invoke-direct {v0, p0}, LX/2da;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;->a:LX/2da;

    .line 1707243
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;->a:LX/2da;

    invoke-static {p0, v0}, LX/0vv;->a(Landroid/view/View;LX/0vn;)V

    .line 1707244
    return-void
.end method


# virtual methods
.method public final dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 1707245
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/LearnMoreTextView;->a:LX/2da;

    invoke-virtual {v0, p1}, LX/1cr;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1707246
    const/4 v0, 0x1

    .line 1707247
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
