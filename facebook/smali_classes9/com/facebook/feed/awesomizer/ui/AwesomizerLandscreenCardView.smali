.class public Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""


# instance fields
.field public j:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:Lcom/facebook/resources/ui/FbTextView;

.field private l:Lcom/facebook/fbui/glyph/GlyphView;

.field private m:Lcom/facebook/fbui/glyph/GlyphView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1706453
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1706454
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1706447
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1706448
    const-class v0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;

    invoke-static {v0, p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1706449
    sget-object v0, LX/03r;->AwesomizerLandscreenCardView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1706450
    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->a(Landroid/content/Context;Landroid/content/res/TypedArray;)V

    .line 1706451
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1706452
    return-void
.end method

.method private a(Landroid/content/Context;Landroid/content/res/TypedArray;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1706438
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0309b1

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1706439
    const v0, 0x7f0d18b2

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->k:Lcom/facebook/resources/ui/FbTextView;

    .line 1706440
    const v0, 0x7f0d18b3

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->l:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1706441
    const v0, 0x7f0d18b4

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1706442
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->k:Lcom/facebook/resources/ui/FbTextView;

    invoke-static {p1, p2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->b(Landroid/content/Context;Landroid/content/res/TypedArray;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1706443
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x4

    invoke-virtual {p2, v1, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 1706444
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->l:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/16 v2, 0x6

    invoke-virtual {p2, v2, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1706445
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->l:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->j:LX/0wM;

    const v2, 0x7f020dde

    const/16 v3, 0x4

    invoke-virtual {p2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v1, v2, v3, v4}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1706446
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->j:LX/0wM;

    return-void
.end method

.method private static b(Landroid/content/Context;Landroid/content/res/TypedArray;)Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 1706433
    const/16 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    .line 1706434
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1706435
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public setCardCheckVisibility(I)V
    .locals 1

    .prologue
    .line 1706436
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerLandscreenCardView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1706437
    return-void
.end method
