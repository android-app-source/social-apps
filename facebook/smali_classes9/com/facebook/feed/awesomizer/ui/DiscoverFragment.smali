.class public Lcom/facebook/feed/awesomizer/ui/DiscoverFragment;
.super Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment",
        "<",
        "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public k:LX/Aie;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1707039
    invoke-direct {p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/feed/awesomizer/ui/DiscoverFragment;

    invoke-static {v1}, LX/Aie;->b(LX/0QB;)LX/Aie;

    move-result-object v0

    check-cast v0, LX/Aie;

    new-instance v2, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;

    const-class v3, Landroid/content/Context;

    invoke-interface {v1, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v1}, LX/Aie;->b(LX/0QB;)LX/Aie;

    move-result-object v4

    check-cast v4, LX/Aie;

    invoke-static {v1}, LX/2lB;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v5

    check-cast v5, Landroid/content/ComponentName;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v8

    check-cast v8, LX/17Y;

    invoke-direct/range {v2 .. v8}, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;-><init>(Landroid/content/Context;LX/Aie;Landroid/content/ComponentName;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    move-object v1, v2

    check-cast v1, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverFragment;->k:LX/Aie;

    iput-object v1, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverFragment;->h:LX/Aij;

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1707070
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v0, p1}, LX/Aij;->e(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v0, p1}, LX/Aij;->f(I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 1707071
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d:LX/3wu;

    .line 1707072
    iget p0, v0, LX/3wu;->c:I

    move v0, p0

    .line 1707073
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(LX/0Vd;)V
    .locals 14

    .prologue
    .line 1707063
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverFragment;->k:LX/Aie;

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/DiscoverFragment;->b()I

    move-result v2

    .line 1707064
    const/4 v1, 0x5

    move v3, v1

    .line 1707065
    const/4 v1, 0x5

    move v4, v1

    .line 1707066
    const/16 v1, 0x1a

    move v5, v1

    .line 1707067
    move-object v1, p1

    .line 1707068
    iget-object v12, v0, LX/Aie;->f:LX/1Ck;

    const-string v13, "QUERY_AWESOMIZER_DISCOVER_TASK_ID"

    new-instance v6, LX/Aic;

    move-object v7, v0

    move v8, v2

    move v9, v3

    move v10, v4

    move v11, v5

    invoke-direct/range {v6 .. v11}, LX/Aic;-><init>(LX/Aie;IIII)V

    invoke-virtual {v12, v13, v6, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1707069
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1707046
    invoke-super {p0, p1}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a(Landroid/os/Bundle;)V

    .line 1707047
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/feed/awesomizer/ui/DiscoverFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1707048
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "session_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1707049
    sput-object v0, Lcom/facebook/feed/awesomizer/ui/DiscoverAdapter;->f:Ljava/lang/String;

    .line 1707050
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1707051
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707052
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel;->a()LX/0Px;

    move-result-object v0

    .line 1707053
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1707054
    :cond_0
    :goto_0
    move-object v0, v0

    .line 1707055
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v3}, LX/1OM;->ij_()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    .line 1707056
    return-void

    .line 1707057
    :cond_1
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 1707058
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;

    .line 1707059
    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;->l()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel;->l()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel;->a()I

    move-result p1

    if-lez p1, :cond_2

    .line 1707060
    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1707061
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 1707062
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1707045
    const/4 v0, 0x5

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1707044
    const v0, 0x7f0e098d

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1707042
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1707043
    return-void
.end method

.method public final e()LX/AiW;
    .locals 1

    .prologue
    .line 1707041
    sget-object v0, LX/AiW;->DISCOVER:LX/AiW;

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 1707040
    const v0, 0x7f0d1185

    return v0
.end method
