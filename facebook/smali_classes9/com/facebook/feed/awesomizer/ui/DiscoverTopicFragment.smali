.class public Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;
.super Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment",
        "<",
        "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public k:LX/Aie;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1707172
    invoke-direct {p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p1, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;

    invoke-static {v2}, LX/Aie;->b(LX/0QB;)LX/Aie;

    move-result-object v1

    check-cast v1, LX/Aie;

    new-instance v0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;

    invoke-static {v2}, LX/Aie;->b(LX/0QB;)LX/Aie;

    move-result-object v3

    check-cast v3, LX/Aie;

    invoke-static {v2}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object p0

    check-cast p0, LX/17Y;

    invoke-direct {v0, v3, v4, v5, p0}, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;-><init>(LX/Aie;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    move-object v2, v0

    check-cast v2, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;

    iput-object v1, p1, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->k:LX/Aie;

    iput-object v2, p1, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->h:LX/Aij;

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1707169
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v0, p1}, LX/Aij;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d:LX/3wu;

    .line 1707170
    iget p0, v0, LX/3wu;->c:I

    move v0, p0

    .line 1707171
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(LX/0Vd;)V
    .locals 7

    .prologue
    .line 1707159
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1707160
    iget-object v1, v0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v1

    .line 1707161
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    .line 1707162
    if-nez v0, :cond_0

    const/4 v6, 0x0

    .line 1707163
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->k:LX/Aie;

    iget-object v2, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->l:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->b()I

    move-result v3

    .line 1707164
    const/4 v1, 0x5

    move v4, v1

    .line 1707165
    const/16 v1, 0x1a

    move v5, v1

    .line 1707166
    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/Aie;->a(LX/0Vd;Ljava/lang/String;IIILjava/lang/String;)V

    .line 1707167
    return-void

    .line 1707168
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1707154
    invoke-super {p0, p1}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a(Landroid/os/Bundle;)V

    .line 1707155
    const-class v0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;

    invoke-static {v0, p0}, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1707156
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "topic_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->l:Ljava/lang/String;

    .line 1707157
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "topic_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->m:Ljava/lang/String;

    .line 1707158
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1707173
    const-class v0, LX/1ZF;

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1ZF;

    .line 1707174
    if-eqz v0, :cond_0

    .line 1707175
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicFragment;->m:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/1ZF;->a_(Ljava/lang/String;)V

    .line 1707176
    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1ZF;->k_(Z)V

    .line 1707177
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1707134
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1707135
    iget-object v1, v0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v1

    .line 1707136
    if-eqz v0, :cond_0

    .line 1707137
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707138
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1707139
    iget-object v2, v0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v2

    .line 1707140
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    .line 1707141
    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->j()LX/0Px;

    move-result-object v2

    .line 1707142
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->j()LX/0Px;

    move-result-object v3

    .line 1707143
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1707144
    invoke-virtual {v4, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1707145
    invoke-static {v1}, LX/Ahs;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;)LX/Ahs;

    move-result-object v3

    .line 1707146
    iput-object v2, v3, LX/Ahs;->b:LX/0Px;

    .line 1707147
    move-object v2, v3

    .line 1707148
    invoke-virtual {v2}, LX/Ahs;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    move-result-object v2

    move-object v0, v2

    .line 1707149
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1707150
    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, LX/15i;->h(II)Z

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->a()I

    move-result v2

    invoke-virtual {v3, v0, v1, v2}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    .line 1707151
    return-void

    .line 1707152
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707153
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 1707133
    const/16 v0, 0x14

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1707132
    const v0, 0x7f0e098f

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1707130
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1707131
    return-void
.end method

.method public final e()LX/AiW;
    .locals 1

    .prologue
    .line 1707129
    sget-object v0, LX/AiW;->DISCOVER:LX/AiW;

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 1707128
    const v0, 0x7f0d1185

    return v0
.end method
