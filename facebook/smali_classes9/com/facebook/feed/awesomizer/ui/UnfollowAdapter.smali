.class public Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;
.super LX/Aij;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Aij",
        "<",
        "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final f:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final d:LX/Aie;

.field private final e:LX/0wL;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1707458
    const-class v0, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;

    const-string v1, "feed_awesomizer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->f:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Aie;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0wL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1707459
    invoke-direct {p0, p2, p3, p4}, LX/Aij;-><init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 1707460
    iput-object p1, p0, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->d:LX/Aie;

    .line 1707461
    iput-object p5, p0, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->e:LX/0wL;

    .line 1707462
    return-void
.end method

.method private h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;
    .locals 2

    .prologue
    .line 1707463
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707464
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-lez p1, :cond_0

    .line 1707465
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707466
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;->j()LX/0Px;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;

    .line 1707467
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 1707468
    instance-of v0, p1, LX/Aii;

    if-eqz v0, :cond_0

    .line 1707469
    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v0

    check-cast p1, LX/Aii;

    .line 1707470
    iget-object v1, p1, LX/Aii;->l:Landroid/view/View;

    move-object v1, v1

    .line 1707471
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;

    move-object v2, v1

    .line 1707472
    check-cast v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;

    .line 1707473
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 1707474
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->j()I

    move-result v4

    .line 1707475
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne p0, p1, :cond_3

    .line 1707476
    if-eqz v4, :cond_2

    .line 1707477
    const p0, 0x7f0f0114

    const/4 p1, 0x1

    new-array p1, p1, [Ljava/lang/Object;

    const/4 p2, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p1, p2

    invoke-virtual {v3, p0, v4, p1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1707478
    :goto_0
    move-object v3, v4

    .line 1707479
    invoke-virtual {v2, v3}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarContextViewText(Ljava/lang/String;)V

    .line 1707480
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq v3, v4, :cond_1

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v2, v3}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Z)V

    .line 1707481
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v4

    invoke-static {v4}, LX/AiP;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result p0

    sget-object p1, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->f:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v2, v3, v4, p0, p1}, LX/Aif;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 1707482
    :cond_0
    return-void

    .line 1707483
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 1707484
    :cond_2
    const v4, 0x7f08239b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 1707485
    :cond_3
    const v4, 0x7f08239c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1707486
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707487
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;

    .line 1707488
    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->d:LX/Aie;

    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v4

    new-instance v5, LX/Aj1;

    invoke-direct {v5, p0, v0}, LX/Aj1;-><init>(Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;)V

    invoke-virtual {v3, v0, v4, v5}, LX/Aie;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;LX/0TF;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;

    move-result-object v0

    .line 1707489
    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    .line 1707490
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;->k()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1707491
    invoke-virtual {v5, v4, v1}, LX/15i;->h(II)Z

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;->a()I

    move-result v5

    invoke-virtual {p0, v0, v4, v5}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    .line 1707492
    iget-object v4, p0, LX/Aij;->a:LX/Aiv;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v3, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v4, p2, v0}, LX/Aiv;->a(IZ)V

    .line 1707493
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/UnfollowAdapter;->e:LX/0wL;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v3, v4, :cond_1

    :goto_1
    invoke-static {p1, v1}, LX/Aij;->a(Landroid/view/View;Z)Ljava/lang/String;

    move-result-object v1

    .line 1707494
    invoke-static {v0, p1, v1}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1707495
    return-void

    :cond_0
    move v0, v2

    .line 1707496
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1707497
    goto :goto_1
.end method

.method public final c(Landroid/view/ViewGroup;)LX/Aii;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;",
            ">.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1707498
    new-instance v0, LX/Aii;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030156

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aii;-><init>(LX/Aij;Landroid/view/View;)V

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1707499
    sget-object v0, LX/AiW;->UNFOLLOW:LX/AiW;

    invoke-virtual {v0}, LX/AiW;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1707500
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707501
    if-eqz v0, :cond_0

    .line 1707502
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707503
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerFollowingQueryModel$FollowedProfilesModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1707504
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
