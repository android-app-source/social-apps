.class public Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;
.super LX/Aij;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Aij",
        "<",
        "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final e:LX/Aie;

.field public final f:LX/1CX;

.field private final g:LX/0wL;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1707408
    const-class v0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;

    const-string v1, "feed_awesomizer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Aie;LX/1CX;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0wL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1707403
    invoke-direct {p0, p3, p4, p5}, LX/Aij;-><init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 1707404
    iput-object p2, p0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->f:LX/1CX;

    .line 1707405
    iput-object p1, p0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->e:LX/Aie;

    .line 1707406
    iput-object p6, p0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->g:LX/0wL;

    .line 1707407
    return-void
.end method


# virtual methods
.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 1707386
    instance-of v0, p1, LX/Aii;

    if-eqz v0, :cond_0

    .line 1707387
    invoke-virtual {p0, p2}, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v0

    check-cast p1, LX/Aii;

    .line 1707388
    iget-object v1, p1, LX/Aii;->l:Landroid/view/View;

    move-object v1, v1

    .line 1707389
    const/4 p2, 0x1

    const/4 p1, 0x0

    .line 1707390
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;

    move-object v2, v1

    .line 1707391
    check-cast v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;

    .line 1707392
    iput-boolean p2, v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->g:Z

    .line 1707393
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;->l()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 1707394
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;->n()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    sget-object p0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v2, v5, v3, v4, p0}, LX/Aif;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 1707395
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v3

    .line 1707396
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v3, v4, :cond_1

    .line 1707397
    invoke-virtual {v2, p1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarContextViewVisibility(I)V

    .line 1707398
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f08239e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarContextViewText(Ljava/lang/String;)V

    .line 1707399
    invoke-virtual {v2, p2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Z)V

    .line 1707400
    :cond_0
    :goto_0
    return-void

    .line 1707401
    :cond_1
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarContextViewVisibility(I)V

    .line 1707402
    invoke-virtual {v2, p1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Z)V

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1707372
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707373
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    .line 1707374
    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->e:LX/Aie;

    invoke-virtual {p0, p2}, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v4

    new-instance v5, LX/Aiz;

    invoke-direct {v5, p0, p2, p1, v0}, LX/Aiz;-><init>(Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;ILandroid/view/View;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;)V

    invoke-virtual {v3, v0, v4, v5}, LX/Aie;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;LX/0TF;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    move-result-object v0

    .line 1707375
    invoke-virtual {p0, p2}, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, p2}, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;

    move-result-object v3

    if-nez v3, :cond_1

    .line 1707376
    :cond_0
    :goto_0
    return-void

    .line 1707377
    :cond_1
    invoke-virtual {p0, p2}, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardProfileFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v3

    .line 1707378
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->k()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 1707379
    invoke-virtual {v5, v4, v1}, LX/15i;->h(II)Z

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->a()I

    move-result v5

    invoke-virtual {p0, v0, v4, v5}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    .line 1707380
    iget-object v4, p0, LX/Aij;->a:LX/Aiv;

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v3, v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v4, p2, v0}, LX/Aiv;->a(IZ)V

    .line 1707381
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/SeefirstAdapter;->g:LX/0wL;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v3, v4, :cond_3

    :goto_2
    invoke-static {p1, v1}, LX/Aij;->a(Landroid/view/View;Z)Ljava/lang/String;

    move-result-object v1

    .line 1707382
    invoke-static {v0, p1, v1}, LX/0wL;->b(LX/0wL;Landroid/view/View;Ljava/lang/CharSequence;)V

    .line 1707383
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1707384
    goto :goto_1

    :cond_3
    move v1, v2

    .line 1707385
    goto :goto_2
.end method

.method public final c(Landroid/view/ViewGroup;)LX/Aii;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;",
            ">.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1707371
    new-instance v0, LX/Aii;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030156

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aii;-><init>(LX/Aij;Landroid/view/View;)V

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1707370
    sget-object v0, LX/AiW;->SEEFIRST:LX/AiW;

    invoke-virtual {v0}, LX/AiW;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;
    .locals 2

    .prologue
    .line 1707365
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707366
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-ge p1, v0, :cond_0

    if-lez p1, :cond_0

    .line 1707367
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707368
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->j()LX/0Px;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel$EdgesModel;

    .line 1707369
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1707360
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707361
    if-eqz v0, :cond_0

    .line 1707362
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707363
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerSeefirstCardQueryModel$FollowedProfilesModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 1707364
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
