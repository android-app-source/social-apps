.class public Lcom/facebook/feed/awesomizer/ui/RefollowFragment;
.super Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment",
        "<",
        "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public k:LX/Aie;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1707341
    invoke-direct {p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 9

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/feed/awesomizer/ui/RefollowFragment;

    invoke-static {v1}, LX/Aie;->b(LX/0QB;)LX/Aie;

    move-result-object v0

    check-cast v0, LX/Aie;

    new-instance v2, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;

    invoke-static {v1}, LX/Aie;->b(LX/0QB;)LX/Aie;

    move-result-object v3

    check-cast v3, LX/Aie;

    invoke-static {v1}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/11R;->a(LX/0QB;)LX/11R;

    move-result-object v6

    check-cast v6, LX/11S;

    invoke-static {v1}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v7

    check-cast v7, LX/17Y;

    invoke-static {v1}, LX/0wL;->a(LX/0QB;)LX/0wL;

    move-result-object v8

    check-cast v8, LX/0wL;

    invoke-direct/range {v2 .. v8}, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;-><init>(LX/Aie;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/11S;LX/17Y;LX/0wL;)V

    move-object v1, v2

    check-cast v1, Lcom/facebook/feed/awesomizer/ui/RefollowAdapter;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/RefollowFragment;->k:LX/Aie;

    iput-object v1, p0, Lcom/facebook/feed/awesomizer/ui/RefollowFragment;->h:LX/Aij;

    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 1707338
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v0, p1}, LX/Aij;->e(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->d:LX/3wu;

    .line 1707339
    iget p0, v0, LX/3wu;->c:I

    move v0, p0

    .line 1707340
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(LX/0Vd;)V
    .locals 5

    .prologue
    .line 1707330
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1707331
    iget-object v1, v0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v1

    .line 1707332
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    .line 1707333
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 1707334
    :goto_0
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/RefollowFragment;->k:LX/Aie;

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->b()I

    move-result v2

    .line 1707335
    iget-object v3, v1, LX/Aie;->f:LX/1Ck;

    const-string v4, "QUERY_AWESOMIZER_REFOLLOWING_TASK_ID"

    new-instance p0, LX/Aia;

    invoke-direct {p0, v1, v2, v0}, LX/Aia;-><init>(LX/Aie;ILjava/lang/String;)V

    invoke-virtual {v3, v4, p0, p1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 1707336
    return-void

    .line 1707337
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1707327
    invoke-super {p0, p1}, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->a(Landroid/os/Bundle;)V

    .line 1707328
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lcom/facebook/feed/awesomizer/ui/RefollowFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1707329
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1707307
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1707308
    iget-object v1, v0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v1

    .line 1707309
    if-eqz v0, :cond_0

    .line 1707310
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707311
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    move-result-object v1

    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    .line 1707312
    iget-object v2, v0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v2

    .line 1707313
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    .line 1707314
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->j()LX/0Px;

    move-result-object v2

    .line 1707315
    invoke-virtual {v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->j()LX/0Px;

    move-result-object v3

    .line 1707316
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1707317
    invoke-virtual {v4, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v2

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 1707318
    invoke-static {v1}, LX/Ai2;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;)LX/Ai2;

    move-result-object v3

    .line 1707319
    iput-object v2, v3, LX/Ai2;->b:LX/0Px;

    .line 1707320
    move-object v2, v3

    .line 1707321
    invoke-virtual {v2}, LX/Ai2;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    move-result-object v2

    move-object v0, v2

    .line 1707322
    :goto_0
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1707323
    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v4}, LX/15i;->h(II)Z

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;->a()I

    move-result v2

    invoke-virtual {v3, v0, v1, v2}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    .line 1707324
    return-void

    .line 1707325
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 1707326
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerUnfollowingQueryModel$UnfollowedProfilesModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 1707306
    const v0, 0x7f0e098b

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1707304
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/BaseAwesomizerFragment;->h:LX/Aij;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1707305
    return-void
.end method

.method public final e()LX/AiW;
    .locals 1

    .prologue
    .line 1707303
    sget-object v0, LX/AiW;->REFOLLOW:LX/AiW;

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 1707302
    const v0, 0x7f0d1184

    return v0
.end method
