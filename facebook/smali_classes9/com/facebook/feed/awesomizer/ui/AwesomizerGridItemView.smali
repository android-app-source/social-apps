.class public Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field private c:Lcom/facebook/fbui/facepile/FacepileView;

.field private d:Lcom/facebook/resources/ui/FbTextView;

.field private e:Lcom/facebook/resources/ui/FbTextView;

.field private f:Z

.field public g:Z

.field private h:I

.field private i:I

.field private j:Landroid/graphics/drawable/Drawable;

.field private k:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1706416
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1706417
    iput-boolean v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->f:Z

    .line 1706418
    iput-boolean v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->g:Z

    .line 1706419
    invoke-direct {p0, p1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Landroid/content/Context;)V

    .line 1706420
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1706411
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1706412
    iput-boolean v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->f:Z

    .line 1706413
    iput-boolean v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->g:Z

    .line 1706414
    invoke-direct {p0, p1}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Landroid/content/Context;)V

    .line 1706415
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 1706395
    const-class v0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;

    invoke-static {v0, p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1706396
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030157

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 1706397
    const v0, 0x7f0d063c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1706398
    const v0, 0x7f0d063d

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/facepile/FacepileView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    .line 1706399
    const v0, 0x7f0d063e

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->d:Lcom/facebook/resources/ui/FbTextView;

    .line 1706400
    const v0, 0x7f0d063f

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    .line 1706401
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    .line 1706402
    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    .line 1706403
    new-instance v2, Landroid/util/TypedValue;

    invoke-direct {v2}, Landroid/util/TypedValue;-><init>()V

    .line 1706404
    const v3, 0x7f01077b

    invoke-virtual {v0, v3, v1, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1706405
    const v3, 0x7f01077c

    invoke-virtual {v0, v3, v2, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 1706406
    iget v0, v1, Landroid/util/TypedValue;->data:I

    iput v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->i:I

    .line 1706407
    iget v0, v2, Landroid/util/TypedValue;->data:I

    iput v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->h:I

    .line 1706408
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0217ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->j:Landroid/graphics/drawable/Drawable;

    .line 1706409
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a:LX/0wM;

    const v1, 0x7f0200f7

    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->k:Landroid/graphics/drawable/Drawable;

    .line 1706410
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v0

    check-cast v0, LX/0wM;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a:LX/0wM;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 1706393
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1706394
    return-void
.end method

.method public final a(Ljava/util/List;I)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 1706387
    const/16 v0, 0x66

    if-lt p2, v0, :cond_0

    .line 1706388
    const/16 p2, 0x63

    .line 1706389
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceCountForOverflow(I)V

    .line 1706390
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, p1}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceStrings(Ljava/util/List;)V

    .line 1706391
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->c:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 1706392
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    .line 1706421
    iput-boolean p1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->f:Z

    .line 1706422
    if-eqz p1, :cond_0

    .line 1706423
    iget v1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->h:I

    .line 1706424
    iget v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->i:I

    move v2, v0

    move v3, v1

    .line 1706425
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200ee

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    .line 1706426
    const v1, 0x7f0d31dc

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/GradientDrawable;

    .line 1706427
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b1834

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v4, v3}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 1706428
    invoke-virtual {p0, v2}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarContextViewTextColor(I)V

    .line 1706429
    invoke-virtual {p0, v0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarImageViewBorder(Landroid/graphics/drawable/Drawable;)V

    .line 1706430
    return-void

    .line 1706431
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 1706432
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a00e6

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    move v2, v0

    move v3, v1

    goto :goto_0
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 1706380
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1706381
    iget-boolean v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->g:Z

    if-eqz v0, :cond_0

    .line 1706382
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->j:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1706383
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->j:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1706384
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->k:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getRight()I

    move-result v1

    iget-object v2, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getRight()I

    move-result v3

    iget-object v4, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1706385
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1706386
    :cond_0
    return-void
.end method

.method public setAvatarContextViewText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1706366
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1706367
    return-void
.end method

.method public setAvatarContextViewTextColor(I)V
    .locals 1

    .prologue
    .line 1706378
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 1706379
    return-void
.end method

.method public setAvatarContextViewVisibility(I)V
    .locals 1

    .prologue
    .line 1706376
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->e:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1706377
    return-void
.end method

.method public setAvatarImageViewBorder(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 1706374
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1706375
    return-void
.end method

.method public setAvatarImageViewDrawable(I)V
    .locals 1

    .prologue
    .line 1706372
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageResource(I)V

    .line 1706373
    return-void
.end method

.method public setAvatarNameViewText(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1706370
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1706371
    return-void
.end method

.method public setShowStar(Z)V
    .locals 0

    .prologue
    .line 1706368
    iput-boolean p1, p0, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->g:Z

    .line 1706369
    return-void
.end method
