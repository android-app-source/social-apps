.class public Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;
.super LX/Aij;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Aij",
        "<",
        "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final e:LX/Aie;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1707127
    const-class v0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;

    const-string v1, "feed_awesomizer"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/Aie;LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1707124
    invoke-direct {p0, p2, p3, p4}, LX/Aij;-><init>(LX/03V;Lcom/facebook/content/SecureContextHelper;LX/17Y;)V

    .line 1707125
    iput-object p1, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;->e:LX/Aie;

    .line 1707126
    return-void
.end method

.method private h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;
    .locals 1

    .prologue
    .line 1707119
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707120
    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1707121
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707122
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;

    .line 1707123
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1a1;I)V
    .locals 8

    .prologue
    .line 1707097
    instance-of v0, p1, LX/Aii;

    if-eqz v0, :cond_0

    .line 1707098
    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;

    move-result-object v0

    check-cast p1, LX/Aii;

    .line 1707099
    iget-object v1, p1, LX/Aii;->l:Landroid/view/View;

    move-object v1, v1

    .line 1707100
    const/4 v4, 0x0

    .line 1707101
    move-object v2, v1

    check-cast v2, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;

    .line 1707102
    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 1707103
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->o()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v3

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v3, v6, :cond_1

    const/4 v3, 0x1

    .line 1707104
    :goto_0
    invoke-virtual {v2, v3}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Z)V

    .line 1707105
    if-eqz v3, :cond_2

    const v3, 0x7f0823a0

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1707106
    :goto_1
    invoke-virtual {v2, v3}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->setAvatarContextViewText(Ljava/lang/String;)V

    .line 1707107
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->l()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 1707108
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    const v5, 0x25d6af

    sget-object v7, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;->d:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v2, v6, v3, v5, v7}, LX/Aif;->a(Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;Ljava/lang/String;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;)V

    .line 1707109
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->k()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    move-result-object v3

    if-nez v3, :cond_3

    .line 1707110
    :cond_0
    :goto_2
    return-void

    :cond_1
    move v3, v4

    .line 1707111
    goto :goto_0

    .line 1707112
    :cond_2
    const-string v3, ""

    goto :goto_1

    .line 1707113
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 1707114
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->k()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;->j()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result p0

    move v5, v4

    :goto_3
    if-ge v5, p0, :cond_5

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel$NodesModel;

    .line 1707115
    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel$NodesModel;->a()LX/1vs;

    move-result-object p1

    iget p1, p1, LX/1vs;->b:I

    if-eqz p1, :cond_4

    .line 1707116
    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel$NodesModel;->a()LX/1vs;

    move-result-object v3

    iget-object p1, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {p1, v3, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1707117
    :cond_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 1707118
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;->k()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardConnectedFriendsFragmentModel$ConnectedFriendsModel;->a()I

    move-result v3

    invoke-virtual {v2, v6, v3}, Lcom/facebook/feed/awesomizer/ui/AwesomizerGridItemView;->a(Ljava/util/List;I)V

    goto :goto_2
.end method

.method public final a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 1707091
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707092
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    .line 1707093
    iget-object v1, p0, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;->e:LX/Aie;

    invoke-direct {p0, p2}, Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;->h(I)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;

    move-result-object v2

    new-instance v3, LX/Ait;

    invoke-direct {v3, p0, v0}, LX/Ait;-><init>(Lcom/facebook/feed/awesomizer/ui/DiscoverTopicAdapter;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;)V

    invoke-virtual {v1, v0, v2, v3}, LX/Aie;->a(Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;LX/0TF;)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    move-result-object v0

    .line 1707094
    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 1707095
    const/4 v3, 0x1

    invoke-virtual {v2, v1, v3}, LX/15i;->h(II)Z

    move-result v1

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->a()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/Aij;->a(Ljava/lang/Object;ZI)V

    .line 1707096
    return-void
.end method

.method public final c(Landroid/view/ViewGroup;)LX/Aii;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            ")",
            "LX/Aij",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;",
            ">.ViewHolderItem;"
        }
    .end annotation

    .prologue
    .line 1707090
    new-instance v0, LX/Aii;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030156

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, LX/Aii;-><init>(LX/Aij;Landroid/view/View;)V

    return-object v0
.end method

.method public final e(I)Z
    .locals 1

    .prologue
    .line 1707083
    const/4 v0, 0x0

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1707089
    sget-object v0, LX/AiW;->DISCOVER:LX/AiW;

    invoke-virtual {v0}, LX/AiW;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 1707084
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707085
    if-eqz v0, :cond_0

    .line 1707086
    iget-object v0, p0, LX/Aij;->g:Ljava/lang/Object;

    move-object v0, v0

    .line 1707087
    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    invoke-virtual {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 1707088
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
