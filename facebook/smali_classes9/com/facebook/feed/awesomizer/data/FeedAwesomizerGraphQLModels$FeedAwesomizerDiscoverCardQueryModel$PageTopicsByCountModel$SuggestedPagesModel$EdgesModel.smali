.class public final Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x2a38bb1a
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1704036
    const-class v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1704035
    const-class v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1704053
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1704054
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1704050
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1704051
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1704052
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 2

    .prologue
    .line 1704044
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1704045
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1704046
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, LX/186;->c(I)V

    .line 1704047
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 1704048
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1704049
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1704055
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1704056
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1704057
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    .line 1704058
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1704059
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;

    .line 1704060
    iput-object v0, v1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->e:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    .line 1704061
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1704062
    if-nez v1, :cond_1

    :goto_0
    return-object p0

    :cond_1
    move-object p0, v1

    goto :goto_0
.end method

.method public final a()Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNode"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1704042
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->e:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    const/4 v1, 0x0

    const-class v2, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->e:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    .line 1704043
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;->e:Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel$NodeModel;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1704039
    new-instance v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;

    invoke-direct {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverCardQueryModel$PageTopicsByCountModel$SuggestedPagesModel$EdgesModel;-><init>()V

    .line 1704040
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1704041
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1704038
    const v0, 0x50430153

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1704037
    const v0, -0x8dd621c

    return v0
.end method
