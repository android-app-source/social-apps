.class public final Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0xa146241
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel$Serializer;
.end annotation


# instance fields
.field private e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1703509
    const-class v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1703508
    const-class v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1703506
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1703507
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1703475
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->e:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->e:Ljava/lang/String;

    .line 1703476
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1703498
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1703499
    invoke-direct {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1703500
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, -0x25868dc6

    invoke-static {v2, v1, v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1703501
    const/4 v2, 0x2

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1703502
    const/4 v2, 0x0

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1703503
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1703504
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1703505
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1703488
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1703489
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 1703490
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x25868dc6

    invoke-static {v2, v0, v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1703491
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1703492
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1703493
    iput v3, v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->f:I

    .line 1703494
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1703495
    if-nez v0, :cond_0

    :goto_1
    return-object p0

    .line 1703496
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object p0, v0

    .line 1703497
    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1703487
    invoke-direct {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1703484
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1703485
    const/4 v0, 0x1

    const v1, -0x25868dc6

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->f:I

    .line 1703486
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1703481
    new-instance v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-direct {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;-><init>()V

    .line 1703482
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1703483
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1703480
    const v0, 0x5fbaac37

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1703479
    const v0, 0x4984e12

    return v0
.end method

.method public final j()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1703477
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1703478
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;->f:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
