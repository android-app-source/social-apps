.class public final Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1703418
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1703419
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1703416
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1703417
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1703364
    if-nez p1, :cond_0

    move v0, v1

    .line 1703365
    :goto_0
    return v0

    .line 1703366
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1703367
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1703368
    :sswitch_0
    const-class v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v1, v0}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1703369
    invoke-static {p3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1703370
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1703371
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1703372
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1703373
    :sswitch_1
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1703374
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1703375
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1703376
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1703377
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1703378
    :sswitch_2
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1703379
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1703380
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1703381
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1703382
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1703383
    :sswitch_3
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1703384
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1703385
    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v2

    .line 1703386
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1703387
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1703388
    invoke-virtual {p3, v3, v2}, LX/186;->a(IZ)V

    .line 1703389
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1703390
    :sswitch_4
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1703391
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1703392
    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v2

    .line 1703393
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1703394
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1703395
    invoke-virtual {p3, v3, v2}, LX/186;->a(IZ)V

    .line 1703396
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1703397
    :sswitch_5
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1703398
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1703399
    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1703400
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1703401
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1703402
    :sswitch_6
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1703403
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1703404
    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v2

    .line 1703405
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1703406
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1703407
    invoke-virtual {p3, v3, v2}, LX/186;->a(IZ)V

    .line 1703408
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    .line 1703409
    :sswitch_7
    invoke-virtual {p0, p1, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 1703410
    invoke-virtual {p3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    .line 1703411
    invoke-virtual {p0, p1, v3}, LX/15i;->b(II)Z

    move-result v2

    .line 1703412
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1703413
    invoke-virtual {p3, v1, v0}, LX/186;->b(II)V

    .line 1703414
    invoke-virtual {p3, v3, v2}, LX/186;->a(IZ)V

    .line 1703415
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3b40f235 -> :sswitch_4
        -0x25868dc6 -> :sswitch_1
        0x29491267 -> :sswitch_7
        0x40a9e7c5 -> :sswitch_0
        0x4237b83a -> :sswitch_5
        0x496ddb9b -> :sswitch_2
        0x64223180 -> :sswitch_6
        0x73703c0f -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1703363
    new-instance v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/0jT;LX/1jy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/0jT;",
            ">(TT;",
            "LX/1jy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1703358
    if-eqz p0, :cond_0

    .line 1703359
    invoke-interface {p1, p0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    .line 1703360
    if-eq v0, p0, :cond_0

    .line 1703361
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1703362
    :cond_0
    return-void
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1703353
    sparse-switch p2, :sswitch_data_0

    .line 1703354
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1703355
    :sswitch_0
    const/4 v0, 0x0

    const-class v1, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;

    invoke-virtual {p0, p1, v0, v1}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FBFeedAwesomizerProfileListCardProfileFragmentModel$CoverPhotoModel$PhotoModel;

    .line 1703356
    invoke-static {v0, p3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/0jT;LX/1jy;)V

    .line 1703357
    :sswitch_1
    return-void

    :sswitch_data_0
    .sparse-switch
        -0x3b40f235 -> :sswitch_1
        -0x25868dc6 -> :sswitch_1
        0x29491267 -> :sswitch_1
        0x40a9e7c5 -> :sswitch_0
        0x4237b83a -> :sswitch_1
        0x496ddb9b -> :sswitch_1
        0x64223180 -> :sswitch_1
        0x73703c0f -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1703319
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1703351
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1703352
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1703346
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1703347
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1703348
    :cond_0
    iput-object p1, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a:LX/15i;

    .line 1703349
    iput p2, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->b:I

    .line 1703350
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1703345
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1703344
    new-instance v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1703341
    iget v0, p0, LX/1vt;->c:I

    .line 1703342
    move v0, v0

    .line 1703343
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1703338
    iget v0, p0, LX/1vt;->c:I

    .line 1703339
    move v0, v0

    .line 1703340
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1703335
    iget v0, p0, LX/1vt;->b:I

    .line 1703336
    move v0, v0

    .line 1703337
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1703332
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1703333
    move-object v0, v0

    .line 1703334
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1703323
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1703324
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1703325
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1703326
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1703327
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1703328
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1703329
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1703330
    invoke-static {v3, v9, v2}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1703331
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1703320
    iget v0, p0, LX/1vt;->c:I

    .line 1703321
    move v0, v0

    .line 1703322
    return v0
.end method
