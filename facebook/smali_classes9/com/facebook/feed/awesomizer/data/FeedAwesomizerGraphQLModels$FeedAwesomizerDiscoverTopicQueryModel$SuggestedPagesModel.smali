.class public final Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0x3cfe2d65
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$Serializer;
.end annotation


# instance fields
.field private e:I

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:I
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1704431
    const-class v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1704434
    const-class v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1704432
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1704433
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1704428
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1704429
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1704430
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1704426
    invoke-virtual {p0, v0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1704427
    iget v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->e:I

    return v0
.end method

.method public final a(LX/186;)I
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1704417
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1704418
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->j()LX/0Px;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v0

    .line 1704419
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const v3, 0x73703c0f

    invoke-static {v2, v1, v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v1

    invoke-static {p1, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 1704420
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 1704421
    iget v2, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->e:I

    invoke-virtual {p1, v4, v2, v4}, LX/186;->a(III)V

    .line 1704422
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v0}, LX/186;->b(II)V

    .line 1704423
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1704424
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1704425
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1704435
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1704436
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->j()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1704437
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->j()LX/0Px;

    move-result-object v1

    invoke-static {v1, p1}, LX/1k0;->a(LX/0Px;LX/1jy;)LX/0Pz;

    move-result-object v1

    .line 1704438
    if-eqz v1, :cond_2

    .line 1704439
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    .line 1704440
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->f:Ljava/util/List;

    move-object v1, v0

    .line 1704441
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_0

    .line 1704442
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, 0x73703c0f

    invoke-static {v2, v0, v3}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v2, v0, LX/1vt;->a:LX/15i;

    iget v3, v0, LX/1vt;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1704443
    invoke-virtual {p0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->k()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v2, v3}, LX/1vu;->a(LX/15i;ILX/15i;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1704444
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    .line 1704445
    iput v3, v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->g:I

    move-object v1, v0

    .line 1704446
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1704447
    if-nez v1, :cond_1

    :goto_1
    return-object p0

    .line 1704448
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_1
    move-object p0, v1

    .line 1704449
    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1704413
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1704414
    invoke-virtual {p1, p2, v0, v0}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->e:I

    .line 1704415
    const/4 v0, 0x2

    const v1, 0x73703c0f

    invoke-static {p1, p2, v0, v1}, LX/1vr;->a(LX/15i;III)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    iput v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->g:I

    .line 1704416
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1704410
    new-instance v0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;

    invoke-direct {v0}, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;-><init>()V

    .line 1704411
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1704412
    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1704409
    const v0, 0x10f9bf67

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1704404
    const v0, -0x3f594a3b

    return v0
.end method

.method public final j()LX/0Px;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getNodes"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1704407
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->f:Ljava/util/List;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel$NodesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/util/List;ILjava/lang/Class;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->f:Ljava/util/List;

    .line 1704408
    iget-object v0, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->f:Ljava/util/List;

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final k()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPageInfo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1704405
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1704406
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/feed/awesomizer/data/FeedAwesomizerGraphQLModels$FeedAwesomizerDiscoverTopicQueryModel$SuggestedPagesModel;->g:I

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    return-object v0
.end method
