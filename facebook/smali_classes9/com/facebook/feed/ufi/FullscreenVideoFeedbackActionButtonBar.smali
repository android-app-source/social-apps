.class public Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;
.super Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private A:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

.field public B:Landroid/view/View;

.field private C:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

.field private D:LX/1EO;

.field private final E:LX/Amt;

.field private F:LX/Aov;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private G:LX/Amw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private H:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private I:LX/3mH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private a:LX/1nA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/intent/feed/IFeedIntentBuilder;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:LX/1AM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private e:LX/17Q;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/1EP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/1Kf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/0qn;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private j:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/1EQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:LX/189;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/feed/analytics/IsStorySharingAnalyticsEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:LX/1Wo;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private final r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private final s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field private final t:[Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

.field public u:Z

.field private v:Z

.field private w:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/feed/annotations/IsUFIShareActionEnabled;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field private y:LX/21T;

.field private z:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1711537
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1711538
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1711539
    invoke-direct {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1711540
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 1711541
    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->p:LX/0Ot;

    .line 1711542
    iput-boolean v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->u:Z

    .line 1711543
    iput-boolean v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->v:Z

    .line 1711544
    new-instance v0, LX/Amt;

    invoke-direct {v0, p0}, LX/Amt;-><init>(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->E:LX/Amt;

    .line 1711545
    const-class v0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    invoke-static {v0, p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1711546
    const v0, 0x7f030628

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1711547
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->o:LX/1Wo;

    .line 1711548
    sget-object v1, LX/21S;->INLINE:LX/21S;

    move-object v1, v1

    .line 1711549
    invoke-virtual {v0, v1}, LX/1Wo;->a(LX/21S;)LX/21T;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->y:LX/21T;

    .line 1711550
    const v0, 0x7f0d0c3f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1711551
    const v0, 0x7f0d0c42

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1711552
    const v0, 0x7f0d0c43

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1711553
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->B:Landroid/view/View;

    .line 1711554
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->n:LX/0Or;

    invoke-direct {p0, v0, v2}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(LX/0Or;Z)V

    .line 1711555
    invoke-direct {p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a()[Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->t:[Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    .line 1711556
    sget-object v0, LX/03r;->FullscreenVideoFeedbackActionButtonBar:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 1711557
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 1711558
    invoke-static {v1}, LX/Amw;->a(I)LX/1Wk;

    move-result-object v1

    .line 1711559
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 1711560
    invoke-direct {p0, v1}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->setupDownstates(LX/1Wk;)V

    .line 1711561
    sget-object v0, LX/1vY;->FEEDBACK_SECTION:LX/1vY;

    invoke-static {p0, v0}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1711562
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    sget-object v1, LX/1vY;->LIKE_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1711563
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    sget-object v1, LX/1vY;->COMMENT_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1711564
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    sget-object v1, LX/1vY;->SHARE_LINK:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 1711565
    return-void
.end method

.method private a(LX/0Or;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1711566
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    .line 1711567
    new-instance v1, LX/Ams;

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    invoke-direct {v1, v2}, LX/Ams;-><init>(LX/0bH;)V

    .line 1711568
    iput-object v1, v0, LX/215;->k:LX/Ams;

    .line 1711569
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1711570
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1711571
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/215;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSpring(LX/215;)V

    .line 1711572
    return-void
.end method

.method private a(LX/0Or;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 1711573
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    sget-object v1, LX/Amv;->LIKE:LX/Amv;

    invoke-static {v1}, LX/Amw;->a(LX/Amv;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1711574
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->G:LX/Amw;

    sget-object v2, LX/Amv;->LIKE:LX/Amv;

    invoke-virtual {v1, v2}, LX/Amw;->b(LX/Amv;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1711575
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    sget-object v1, LX/Amv;->COMMENT:LX/Amv;

    invoke-static {v1}, LX/Amw;->a(LX/Amv;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1711576
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->G:LX/Amw;

    sget-object v2, LX/Amv;->COMMENT:LX/Amv;

    invoke-virtual {v1, v2}, LX/Amw;->b(LX/Amv;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1711577
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    sget-object v1, LX/Amv;->SHARE:LX/Amv;

    invoke-static {v1}, LX/Amw;->a(LX/Amv;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setText(I)V

    .line 1711578
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->G:LX/Amw;

    sget-object v2, LX/Amv;->SHARE:LX/Amv;

    invoke-virtual {v1, v2}, LX/Amw;->b(LX/Amv;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1711579
    invoke-direct {p0, p1}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(LX/0Or;)V

    .line 1711580
    invoke-direct {p0, p2}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(Z)V

    .line 1711581
    return-void
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1711582
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1711583
    move-object v3, v0

    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1711584
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const v2, 0x7f0d0067

    invoke-virtual {v0, v2, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTag(ILjava/lang/Object;)V

    .line 1711585
    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1711586
    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1711587
    invoke-static {p1}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1711588
    new-instance v2, LX/1Zc;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v4

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {v2, v4, v0}, LX/1Zc;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1711589
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const v4, 0x7f0d0067

    invoke-virtual {v0, v4, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTag(ILjava/lang/Object;)V

    .line 1711590
    invoke-static {p1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    .line 1711591
    invoke-static {p1}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    .line 1711592
    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const v5, 0x7f0d006d

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTag(ILjava/lang/Object;)V

    .line 1711593
    iget-object v6, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a:LX/1nA;

    iget-object v7, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v8

    new-instance v9, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 1711594
    const-string v2, "newsfeed_ufi"

    move-object v2, v2

    .line 1711595
    iget-object v5, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->D:LX/1EO;

    iget-object v5, v5, LX/1EO;->analyticModule:Ljava/lang/String;

    invoke-direct {v9, v4, v2, v5}, Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;-><init>(LX/162;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->D:LX/1EO;

    iget-object v5, v5, LX/1EO;->analyticModule:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, LX/1EP;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0lF;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    const-string v5, "tap_footer_comment"

    move-object v0, v6

    move-object v1, v7

    move-object v2, v8

    move-object v3, v9

    .line 1711596
    if-eqz v1, :cond_3

    if-eqz v2, :cond_3

    .line 1711597
    const v6, 0x7f0d006a

    invoke-virtual {v1, v6, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1711598
    const v6, 0x7f0d006c

    invoke-virtual {v1, v6, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1711599
    const v6, 0x7f0d006f

    invoke-virtual {v1, v6, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1711600
    const v6, 0x7f0d0070

    invoke-virtual {v1, v6, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1711601
    iget-object v6, v0, LX/1nA;->t:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1711602
    :cond_0
    :goto_2
    return-void

    .line 1711603
    :cond_1
    const/16 v0, 0x8

    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 1711604
    goto :goto_1

    :cond_3
    goto :goto_2
.end method

.method private a(Lcom/facebook/feed/rows/core/props/FeedProps;ZZZ)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;ZZZ)V"
        }
    .end annotation

    .prologue
    .line 1711605
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->setVisibility(I)V

    .line 1711606
    invoke-direct {p0, p2}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c(Z)V

    .line 1711607
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1711608
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->b(Z)V

    .line 1711609
    invoke-direct {p0, p1, p3}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    .line 1711610
    invoke-direct {p0, p4}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->d(Z)V

    .line 1711611
    return-void
.end method

.method private static a(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;LX/1nA;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0bH;LX/1AM;LX/17Q;LX/1EP;LX/0Zb;LX/1Kf;LX/0qn;LX/03V;LX/1EQ;LX/189;Ljava/lang/Boolean;LX/0Or;LX/1Wo;LX/0Ot;Ljava/lang/Boolean;LX/Aov;LX/Amw;LX/0Or;LX/3mH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;",
            "LX/1nA;",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            "LX/0bH;",
            "LX/1AM;",
            "LX/17Q;",
            "LX/1EP;",
            "LX/0Zb;",
            "LX/1Kf;",
            "LX/0qn;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1EQ;",
            "LX/189;",
            "Ljava/lang/Boolean;",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "LX/1Wo;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActorCache;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/Aov;",
            "LX/Amw;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/3mH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1711612
    iput-object p1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a:LX/1nA;

    iput-object p2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iput-object p3, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    iput-object p4, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->d:LX/1AM;

    iput-object p5, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->e:LX/17Q;

    iput-object p6, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->f:LX/1EP;

    iput-object p7, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->g:LX/0Zb;

    iput-object p8, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->h:LX/1Kf;

    iput-object p9, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->i:LX/0qn;

    iput-object p10, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->j:LX/03V;

    iput-object p11, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->k:LX/1EQ;

    iput-object p12, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->l:LX/189;

    iput-object p13, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->m:Ljava/lang/Boolean;

    iput-object p14, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->n:LX/0Or;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->o:LX/1Wo;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->p:LX/0Ot;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->w:Ljava/lang/Boolean;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->F:LX/Aov;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->G:LX/Amw;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->H:LX/0Or;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->I:LX/3mH;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 24

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v23

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;

    invoke-static/range {v23 .. v23}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static/range {v23 .. v23}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v4

    check-cast v4, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-static/range {v23 .. v23}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v5

    check-cast v5, LX/0bH;

    invoke-static/range {v23 .. v23}, LX/1AM;->a(LX/0QB;)LX/1AM;

    move-result-object v6

    check-cast v6, LX/1AM;

    invoke-static/range {v23 .. v23}, LX/17Q;->a(LX/0QB;)LX/17Q;

    move-result-object v7

    check-cast v7, LX/17Q;

    invoke-static/range {v23 .. v23}, LX/1EP;->a(LX/0QB;)LX/1EP;

    move-result-object v8

    check-cast v8, LX/1EP;

    invoke-static/range {v23 .. v23}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v9

    check-cast v9, LX/0Zb;

    invoke-static/range {v23 .. v23}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v10

    check-cast v10, LX/1Kf;

    invoke-static/range {v23 .. v23}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v11

    check-cast v11, LX/0qn;

    invoke-static/range {v23 .. v23}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v12

    check-cast v12, LX/03V;

    invoke-static/range {v23 .. v23}, LX/1EQ;->a(LX/0QB;)LX/1EQ;

    move-result-object v13

    check-cast v13, LX/1EQ;

    invoke-static/range {v23 .. v23}, LX/189;->a(LX/0QB;)LX/189;

    move-result-object v14

    check-cast v14, LX/189;

    invoke-static/range {v23 .. v23}, LX/Aog;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v15

    check-cast v15, Ljava/lang/Boolean;

    const/16 v16, 0x13a4

    move-object/from16 v0, v23

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v16

    const-class v17, LX/1Wo;

    move-object/from16 v0, v23

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v17

    check-cast v17, LX/1Wo;

    const/16 v18, 0x123

    move-object/from16 v0, v23

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-static/range {v23 .. v23}, LX/Amj;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v19

    check-cast v19, Ljava/lang/Boolean;

    invoke-static/range {v23 .. v23}, LX/Aov;->a(LX/0QB;)LX/Aov;

    move-result-object v20

    check-cast v20, LX/Aov;

    invoke-static/range {v23 .. v23}, LX/Amw;->a(LX/0QB;)LX/Amw;

    move-result-object v21

    check-cast v21, LX/Amw;

    const/16 v22, 0x19e

    move-object/from16 v0, v23

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v22

    invoke-static/range {v23 .. v23}, LX/3mH;->a(LX/0QB;)LX/3mH;

    move-result-object v23

    check-cast v23, LX/3mH;

    invoke-static/range {v2 .. v23}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;LX/1nA;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/0bH;LX/1AM;LX/17Q;LX/1EP;LX/0Zb;LX/1Kf;LX/0qn;LX/03V;LX/1EQ;LX/189;Ljava/lang/Boolean;LX/0Or;LX/1Wo;LX/0Ot;Ljava/lang/Boolean;LX/Aov;LX/Amw;LX/0Or;LX/3mH;)V

    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1711613
    if-eqz p1, :cond_0

    .line 1711614
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSoundEffectsEnabled(Z)V

    .line 1711615
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSoundEffectsEnabled(Z)V

    .line 1711616
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setSoundEffectsEnabled(Z)V

    .line 1711617
    :cond_0
    return-void
.end method

.method private a()[Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;
    .locals 3

    .prologue
    .line 1711618
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    aput-object v2, v0, v1

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;Ljava/lang/String;)V
    .locals 8
    .param p0    # Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 1711619
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 1711620
    iget-object v0, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1711621
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1711622
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1711623
    iget-object v4, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->k:LX/1EQ;

    .line 1711624
    const-string v1, "newsfeed_ufi"

    move-object v5, v1

    .line 1711625
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v6

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->H:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 1711626
    iget-object v7, v1, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v1, v7

    .line 1711627
    invoke-static {v0}, LX/214;->a(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v1, v7}, LX/1EQ;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1711628
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->A:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v1, :cond_2

    if-eqz v0, :cond_2

    .line 1711629
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-direct {p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->getComposerSourceSurface()LX/21D;

    move-result-object v4

    const-string v5, "fullscreenVideoFeedbackActionButton"

    invoke-interface {v1, v3, v4, v5}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    .line 1711630
    const-string v4, "newsfeed_ufi"

    move-object v4, v4

    .line 1711631
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1711632
    const-string v4, "group_composer"

    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1711633
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->getReactionSurface()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 1711634
    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->A:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1711635
    :cond_2
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->A:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    if-nez v1, :cond_3

    .line 1711636
    :goto_0
    return-void

    .line 1711637
    :cond_3
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    .line 1711638
    iget-object v4, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v4}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v4

    .line 1711639
    invoke-static {v1, v4}, LX/17Q;->b(ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 1711640
    invoke-static {v1, p0}, LX/1vZ;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/view/View;)V

    .line 1711641
    iget-object v4, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->g:LX/0Zb;

    invoke-interface {v4, v1}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 1711642
    invoke-static {v3}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 1711643
    iget-object v4, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    new-instance v5, LX/1Zb;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    :goto_2
    invoke-direct {v5, v6, v1, v0}, LX/1Zb;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b7;)V

    .line 1711644
    invoke-virtual {p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1711645
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->h:LX/1Kf;

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->A:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    const/16 v3, 0x6dc

    invoke-interface {v1, p1, v2, v3, v0}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0

    :cond_4
    move-object v1, v2

    .line 1711646
    goto :goto_1

    :cond_5
    move-object v0, v2

    goto :goto_2
.end method

.method private b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1711647
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->z:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 1711648
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->z:Landroid/view/View$OnClickListener;

    .line 1711649
    :goto_0
    return-object v0

    .line 1711650
    :cond_0
    new-instance v0, LX/Aml;

    invoke-direct {v0, p0}, LX/Aml;-><init>(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->z:Landroid/view/View$OnClickListener;

    .line 1711651
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->z:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 1711418
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->G:LX/Amw;

    invoke-virtual {v1, p1}, LX/Amw;->a(Z)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setTextColor(I)V

    .line 1711419
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->G:LX/Amw;

    invoke-virtual {v1, p1}, LX/Amw;->b(Z)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1711420
    iget-boolean v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->u:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1711421
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    new-instance v1, LX/Amq;

    invoke-direct {v1, p0}, LX/Amq;-><init>(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1711422
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->u:Z

    .line 1711423
    :cond_0
    return-void
.end method

.method public static c(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V
    .locals 8

    .prologue
    .line 1711652
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->F:LX/Aov;

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->B:Landroid/view/View;

    .line 1711653
    const-string v3, "newsfeed_ufi"

    move-object v3, v3

    .line 1711654
    new-instance v4, LX/Amn;

    invoke-direct {v4, p0}, LX/Amn;-><init>(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V

    new-instance v5, LX/Amp;

    invoke-direct {v5, p0}, LX/Amp;-><init>(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V

    iget-object v6, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->D:LX/1EO;

    sget-object v7, LX/1EO;->NEWSFEED:LX/1EO;

    if-ne v6, v7, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-virtual/range {v0 .. v6}, LX/Aov;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;Ljava/lang/String;LX/Amm;LX/Amo;Z)LX/5OM;

    move-result-object v0

    .line 1711655
    if-nez v0, :cond_1

    .line 1711656
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a$redex0(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;Ljava/lang/String;)V

    .line 1711657
    :goto_1
    return-void

    .line 1711658
    :cond_0
    const/4 v6, 0x0

    goto :goto_0

    .line 1711659
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->d:LX/1AM;

    new-instance v2, LX/1ZH;

    invoke-direct {v2}, LX/1ZH;-><init>()V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 1711660
    invoke-virtual {v0}, LX/0ht;->d()V

    goto :goto_1
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 1711534
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1711535
    return-void

    .line 1711536
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static d(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;)V
    .locals 2

    .prologue
    .line 1711661
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->d:LX/1AM;

    new-instance v1, LX/1ZJ;

    invoke-direct {v1}, LX/1ZJ;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 1711662
    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1711410
    if-eqz p1, :cond_0

    .line 1711411
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1711412
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-direct {p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1711413
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->v:Z

    .line 1711414
    :goto_0
    return-void

    .line 1711415
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setVisibility(I)V

    .line 1711416
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1711417
    iput-boolean v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->v:Z

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 1711424
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->setVisibility(I)V

    .line 1711425
    return-void
.end method

.method public static e(Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;Z)V
    .locals 10

    .prologue
    .line 1711426
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20i;

    invoke-virtual {v0}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v7

    .line 1711427
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1711428
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1711429
    move-object v6, v0

    check-cast v6, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1711430
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->I:LX/3mH;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v3

    if-nez v7, :cond_1

    const-string v4, "actor is null"

    :goto_0
    sget-object v5, LX/AkV;->CLICK:LX/AkV;

    move-object v2, p0

    invoke-virtual/range {v0 .. v5}, LX/3mH;->a(Ljava/lang/String;Ljava/lang/Object;ZLjava/lang/String;LX/AkV;)V

    .line 1711431
    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1711432
    :cond_0
    :goto_1
    return-void

    .line 1711433
    :cond_1
    const-string v4, "actor is available"

    goto :goto_0

    .line 1711434
    :cond_2
    if-eqz v7, :cond_0

    .line 1711435
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->l:LX/189;

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v2, v7, v0}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 1711436
    iget-object v0, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1711437
    move-object v8, v0

    check-cast v8, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1711438
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, v8}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1711439
    invoke-static {v1}, LX/182;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    .line 1711440
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v5

    .line 1711441
    iget-object v9, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    new-instance v0, LX/1Zj;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v4, :cond_4

    invoke-interface {v4}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v4

    :goto_3
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFeedback;->s_()Z

    move-result v5

    iget-object v6, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->D:LX/1EO;

    iget-object v7, v6, LX/1EO;->analyticModule:Ljava/lang/String;

    move v6, p1

    invoke-direct/range {v0 .. v7}, LX/1Zj;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    invoke-virtual {v9, v0}, LX/0b4;->a(LX/0b7;)V

    .line 1711442
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->b(Z)V

    .line 1711443
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    new-instance v1, LX/8q9;

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v3

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-direct {v1, v2, v3, p1, v4}, LX/8q9;-><init>(Ljava/lang/String;ZZLcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    .line 1711444
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1711445
    :cond_4
    const/4 v4, 0x0

    goto :goto_3
.end method

.method private f()Z
    .locals 2

    .prologue
    .line 1711446
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->C:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->C:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getComposerSourceSurface()LX/21D;
    .locals 3

    .prologue
    .line 1711447
    sget-object v0, LX/Amr;->a:[I

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->D:LX/1EO;

    invoke-virtual {v1}, LX/1EO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1711448
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No ComposerSourceSurface for story render context "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->D:LX/1EO;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1711449
    :pswitch_0
    sget-object v0, LX/21D;->NEWSFEED:LX/21D;

    .line 1711450
    :goto_0
    return-object v0

    .line 1711451
    :pswitch_1
    sget-object v0, LX/21D;->TIMELINE:LX/21D;

    goto :goto_0

    .line 1711452
    :pswitch_2
    sget-object v0, LX/21D;->GROUP_FEED:LX/21D;

    goto :goto_0

    .line 1711453
    :pswitch_3
    sget-object v0, LX/21D;->PAGE_FEED:LX/21D;

    goto :goto_0

    .line 1711454
    :pswitch_4
    sget-object v0, LX/21D;->EVENT:LX/21D;

    goto :goto_0

    .line 1711455
    :pswitch_5
    sget-object v0, LX/21D;->PERMALINK:LX/21D;

    goto :goto_0

    .line 1711456
    :pswitch_6
    sget-object v0, LX/21D;->FULLSCREEN_VIDEO_PLAYER:LX/21D;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private getNectarModule()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1711457
    const-string v0, "newsfeed_ufi"

    return-object v0
.end method

.method private getReactionSurface()Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation

    .prologue
    .line 1711458
    sget-object v0, LX/Amr;->a:[I

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->D:LX/1EO;

    invoke-virtual {v1}, LX/1EO;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1711459
    :pswitch_0
    const-string v0, "ANDROID_COMPOSER"

    :goto_0
    return-object v0

    .line 1711460
    :pswitch_1
    const-string v0, "ANDROID_FEED_COMPOSER"

    goto :goto_0

    .line 1711461
    :pswitch_2
    const-string v0, "ANDROID_TIMELINE_COMPOSER"

    goto :goto_0

    .line 1711462
    :pswitch_3
    const-string v0, "ANDROID_GROUP_COMPOSER"

    goto :goto_0

    .line 1711463
    :pswitch_4
    const-string v0, "ANDROID_PAGE_COMPOSER"

    goto :goto_0

    .line 1711464
    :pswitch_5
    const-string v0, "ANDROID_VIDEO_COMPOSER"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private setupDownstates(LX/1Wk;)V
    .locals 1

    .prologue
    .line 1711465
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setDownstateType(LX/1Wk;)V

    .line 1711466
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setDownstateType(LX/1Wk;)V

    .line 1711467
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setDownstateType(LX/1Wk;)V

    .line 1711468
    return-void
.end method

.method private setupFeedbackBarPublishState(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 1711469
    sget-object v0, LX/Amr;->b:[I

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->i:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1711470
    :goto_0
    return-void

    .line 1711471
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->q:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1711472
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->r:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1711473
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->s:Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    invoke-virtual {v0, v2}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1711474
    iput-boolean v3, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->u:Z

    .line 1711475
    iput-boolean v3, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->v:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1EO;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/1EO;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1711476
    iput-object p2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->D:LX/1EO;

    .line 1711477
    iput-object p1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1711478
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1711479
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1711480
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->A:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 1711481
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v2

    .line 1711482
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v3

    .line 1711483
    invoke-static {p1}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->w:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    .line 1711484
    :goto_0
    iget-object v4, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->i:LX/0qn;

    invoke-virtual {v4, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v4

    iput-object v4, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->C:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    .line 1711485
    if-nez v2, :cond_1

    if-nez v3, :cond_1

    if-nez v1, :cond_1

    .line 1711486
    invoke-direct {p0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->e()V

    .line 1711487
    :goto_1
    return-void

    .line 1711488
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1711489
    :cond_1
    invoke-direct {p0, p1, v2, v3, v1}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZZZ)V

    .line 1711490
    invoke-direct {p0, v0}, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->setupFeedbackBarPublishState(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_1
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x7e20a3e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1711491
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onAttachedToWindow()V

    .line 1711492
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->E:LX/Amt;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 1711493
    const/16 v1, 0x2d

    const v2, -0x15ddea72

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6f90149a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1711494
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onDetachedFromWindow()V

    .line 1711495
    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->E:LX/Amt;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 1711496
    const/16 v1, 0x2d

    const v2, 0x52e07c01

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onFinishTemporaryDetach()V
    .locals 2

    .prologue
    .line 1711497
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onFinishTemporaryDetach()V

    .line 1711498
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->E:LX/Amt;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 1711499
    return-void
.end method

.method public final onMeasure(II)V
    .locals 10

    .prologue
    .line 1711500
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    .line 1711501
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1711502
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1711503
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1711504
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 1711505
    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->y:LX/21T;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->w()Z

    move-result v3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->y()Z

    move-result v4

    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->x:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/14w;->q(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v4, v0}, LX/21T;->a(ZZZ)LX/21Y;

    move-result-object v0

    .line 1711506
    iget-object v2, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->t:[Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;

    const/4 v3, 0x0

    .line 1711507
    iget-object v4, v0, LX/21Y;->a:LX/21Z;

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1711508
    array-length v5, v2

    .line 1711509
    iget-object v8, v4, LX/21Z;->h:[F

    move-object v8, v8

    .line 1711510
    array-length v8, v8

    if-gt v5, v8, :cond_2

    move v5, v6

    :goto_1
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 1711511
    array-length v5, v2

    .line 1711512
    iget-object v8, v4, LX/21Z;->i:[F

    move-object v8, v8

    .line 1711513
    array-length v8, v8

    if-gt v5, v8, :cond_3

    move v5, v6

    :goto_2
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 1711514
    array-length v5, v2

    .line 1711515
    iget-object v8, v4, LX/21Z;->j:[F

    move-object v8, v8

    .line 1711516
    array-length v8, v8

    if-gt v5, v8, :cond_4

    :goto_3
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 1711517
    invoke-virtual {v0, v1}, LX/21Y;->a(I)LX/21c;

    move-result-object v7

    .line 1711518
    invoke-virtual {v0, v7}, LX/21Y;->a(LX/21c;)[F

    move-result-object v8

    .line 1711519
    array-length v9, v2

    move v4, v3

    move v5, v3

    :goto_4
    if-ge v4, v9, :cond_0

    aget-object v3, v2, v4

    .line 1711520
    invoke-static {v7}, LX/21c;->hasIcons(LX/21c;)Z

    move-result v6

    invoke-virtual {v3, v6}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->a(Z)V

    .line 1711521
    invoke-virtual {v3}, Lcom/facebook/feed/widget/FeedbackCustomPressStateButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout$LayoutParams;

    add-int/lit8 v6, v5, 0x1

    aget v5, v8, v5

    iput v5, v3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 1711522
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v5, v6

    goto :goto_4

    .line 1711523
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onMeasure(II)V

    .line 1711524
    return-void

    .line 1711525
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v5, v7

    .line 1711526
    goto :goto_1

    :cond_3
    move v5, v7

    .line 1711527
    goto :goto_2

    :cond_4
    move v6, v7

    .line 1711528
    goto :goto_3
.end method

.method public final onStartTemporaryDetach()V
    .locals 2

    .prologue
    .line 1711529
    invoke-super {p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->onStartTemporaryDetach()V

    .line 1711530
    iget-object v0, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->c:LX/0bH;

    iget-object v1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->E:LX/Amt;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 1711531
    return-void
.end method

.method public setSharePopoverAnchor(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1711532
    iput-object p1, p0, Lcom/facebook/feed/ufi/FullscreenVideoFeedbackActionButtonBar;->B:Landroid/view/View;

    .line 1711533
    return-void
.end method
