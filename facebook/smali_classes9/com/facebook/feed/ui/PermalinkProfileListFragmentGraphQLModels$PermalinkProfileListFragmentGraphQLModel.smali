.class public final Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/16i;
.implements LX/16f;
.implements LX/36M;
.implements LX/2vI;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = -0x59fd9e75
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$Serializer;
.end annotation


# instance fields
.field private e:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private j:Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private k:I

.field private l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1712042
    const-class v0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1712029
    const-class v0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1712030
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1712031
    return-void
.end method

.method public constructor <init>(LX/15i;)V
    .locals 1

    .prologue
    .line 1712032
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1712033
    invoke-virtual {p1}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1712034
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 4

    .prologue
    .line 1712035
    iput-object p1, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1712036
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    .line 1712037
    iget-boolean v1, v0, LX/15i;->g:Z

    move v0, v1

    .line 1712038
    if-eqz v0, :cond_0

    .line 1712039
    iget-object v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v2, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v3, 0x1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v2, v3, v0}, LX/15i;->a(IILjava/lang/String;)V

    .line 1712040
    :cond_0
    return-void

    .line 1712041
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712043
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    const/4 v1, 0x3

    const-class v2, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    .line 1712044
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    return-object v0
.end method

.method private o()Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712088
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j:Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    const/4 v1, 0x5

    const-class v2, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j:Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    .line 1712089
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j:Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    return-object v0
.end method

.method private p()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712045
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    const/4 v1, 0x7

    const-class v2, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    invoke-super {p0, v0, v1, v2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/Object;ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    .line 1712046
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    return-object v0
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 1712047
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1712048
    invoke-virtual {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->c()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-static {p1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v0

    .line 1712049
    invoke-virtual {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {p1, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 1712050
    invoke-virtual {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1712051
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v3

    invoke-static {p1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 1712052
    invoke-virtual {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 1712053
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->o()Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v5

    invoke-static {p1, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 1712054
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->p()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v6

    invoke-static {p1, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 1712055
    invoke-virtual {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 1712056
    const/16 v8, 0x9

    invoke-virtual {p1, v8}, LX/186;->c(I)V

    .line 1712057
    invoke-virtual {p1, v9, v0}, LX/186;->b(II)V

    .line 1712058
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1712059
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1712060
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v3}, LX/186;->b(II)V

    .line 1712061
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 1712062
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 1712063
    const/4 v0, 0x6

    iget v1, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->k:I

    invoke-virtual {p1, v0, v1, v9}, LX/186;->a(III)V

    .line 1712064
    const/4 v0, 0x7

    invoke-virtual {p1, v0, v6}, LX/186;->b(II)V

    .line 1712065
    const/16 v0, 0x8

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 1712066
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1712067
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1712068
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1712069
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1712070
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    .line 1712071
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v2

    if-eq v2, v0, :cond_0

    .line 1712072
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;

    .line 1712073
    iput-object v0, v1, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->h:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    .line 1712074
    :cond_0
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->o()Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1712075
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->o()Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    .line 1712076
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->o()Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v2

    if-eq v2, v0, :cond_1

    .line 1712077
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;

    .line 1712078
    iput-object v0, v1, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j:Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    .line 1712079
    :cond_1
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->p()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1712080
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->p()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    .line 1712081
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->p()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v2

    if-eq v2, v0, :cond_2

    .line 1712082
    invoke-static {v1, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;

    .line 1712083
    iput-object v0, v1, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->l:Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    .line 1712084
    :cond_2
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1712085
    if-nez v1, :cond_3

    :goto_0
    return-object p0

    :cond_3
    move-object p0, v1

    goto :goto_0
.end method

.method public final a(LX/4VK;)LX/40U;
    .locals 1

    .prologue
    .line 1712086
    new-instance v0, LX/An5;

    invoke-direct {v0, p1}, LX/An5;-><init>(LX/4VK;)V

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712087
    invoke-virtual {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/15i;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 1712020
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1712021
    const/4 v0, 0x6

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, LX/15i;->a(III)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->k:I

    .line 1712022
    return-void
.end method

.method public final a(Ljava/lang/String;LX/18L;)V
    .locals 1

    .prologue
    .line 1712023
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1712024
    invoke-virtual {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    iput-object v0, p2, LX/18L;->a:Ljava/lang/Object;

    .line 1712025
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v0

    iput v0, p2, LX/18L;->b:I

    .line 1712026
    const/4 v0, 0x1

    iput v0, p2, LX/18L;->c:I

    .line 1712027
    :goto_0
    return-void

    .line 1712028
    :cond_0
    invoke-virtual {p2}, LX/18L;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;Z)V
    .locals 1

    .prologue
    .line 1711995
    const-string v0, "friendship_status"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1711996
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0, p2}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1711997
    :cond_0
    return-void
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1711998
    new-instance v0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;

    invoke-direct {v0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;-><init>()V

    .line 1711999
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1712000
    return-object v0
.end method

.method public final synthetic b()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712001
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->j()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$MutualFriendsModel;

    move-result-object v0

    return-object v0
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712002
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    if-nez v0, :cond_0

    .line 1712003
    iget-object v0, p0, Lcom/facebook/graphql/modelutil/BaseModel;->c:LX/15i;

    iget v1, p0, Lcom/facebook/graphql/modelutil/BaseModel;->d:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-virtual {v0, v1, v2, v3}, LX/15i;->d(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 1712004
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->e:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final synthetic cp_()LX/1Fb;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712005
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->o()Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712006
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v1, 0x1

    const-class v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-super {p0, v0, v1, v2, v3}, Lcom/facebook/graphql/modelutil/BaseModel;->b(Ljava/lang/Enum;ILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1712007
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1712008
    const v0, 0x5d86feac

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712009
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->g:Ljava/lang/String;

    .line 1712010
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1712011
    const v0, 0x3c2b9d5

    return v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 1712018
    const/4 v0, 0x0

    const/4 v1, 0x6

    invoke-virtual {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(II)V

    .line 1712019
    iget v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->k:I

    return v0
.end method

.method public final synthetic l()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712012
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->p()Lcom/facebook/ufiservices/ui/ProfileListFriendingControllerGraphQLModels$ProfileListFriendingControllerGraphQLModel$UnseenStoriesModel;

    move-result-object v0

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712013
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->m:Ljava/lang/String;

    const/16 v1, 0x8

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->m:Ljava/lang/String;

    .line 1712014
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final synthetic n()LX/8qg;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712015
    invoke-direct {p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->o()Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$ProfilePictureModel;

    move-result-object v0

    return-object v0
.end method

.method public final v_()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1712016
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->i:Ljava/lang/String;

    const/4 v1, 0x4

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->i:Ljava/lang/String;

    .line 1712017
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;->i:Ljava/lang/String;

    return-object v0
.end method
