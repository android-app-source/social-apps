.class public Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/3BH;

.field private final c:LX/1nu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1nu",
            "<",
            "LX/1Pm;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0W9;

.field public final e:LX/3Bi;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1829495
    const-class v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/3BH;LX/1nu;LX/0W9;LX/3Bi;LX/0Ot;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3BH;",
            "LX/1nu;",
            "LX/0W9;",
            "LX/3Bi;",
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1829496
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1829497
    iput-object p1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->b:LX/3BH;

    .line 1829498
    iput-object p2, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->c:LX/1nu;

    .line 1829499
    iput-object p3, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->d:LX/0W9;

    .line 1829500
    iput-object p4, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->e:LX/3Bi;

    .line 1829501
    iput-object p5, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->f:LX/0Ot;

    .line 1829502
    return-void
.end method

.method private static a(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;Z)Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1829503
    invoke-static {p2}, LX/3BH;->c(Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1829504
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLPlace;->O()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    .line 1829505
    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1829506
    :cond_0
    :goto_0
    return-object v0

    .line 1829507
    :cond_1
    if-eqz p3, :cond_0

    iget-object v1, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->b:LX/3BH;

    invoke-virtual {v1, p2, p1}, LX/3BH;->a(Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1829508
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "res:///"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v1, 0x7f0201d2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;
    .locals 9

    .prologue
    .line 1829509
    const-class v1, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;

    monitor-enter v1

    .line 1829510
    :try_start_0
    sget-object v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1829511
    sput-object v2, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1829512
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1829513
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1829514
    new-instance v3, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;

    invoke-static {v0}, LX/3BH;->b(LX/0QB;)LX/3BH;

    move-result-object v4

    check-cast v4, LX/3BH;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v6

    check-cast v6, LX/0W9;

    invoke-static {v0}, LX/3Bi;->a(LX/0QB;)LX/3Bi;

    move-result-object v7

    check-cast v7, LX/3Bi;

    const/16 v8, 0x6b9

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;-><init>(LX/3BH;LX/1nu;LX/0W9;LX/3Bi;LX/0Ot;)V

    .line 1829515
    move-object v0, v3

    .line 1829516
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1829517
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1829518
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1829519
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;ZLX/1Pm;Lcom/facebook/analytics/logger/HoneyClientEvent;)LX/1Dg;
    .locals 12
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/graphql/model/GraphQLPlace;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # LX/1Pm;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/analytics/logger/HoneyClientEvent;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPlace;",
            "Z",
            "LX/1Pm;",
            "Lcom/facebook/analytics/logger/HoneyClientEvent;",
            ")",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1829520
    invoke-virtual {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLStory;

    move/from16 v0, p4

    invoke-static {p0, v2, p3, v0}, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->a(Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLPlace;Z)Landroid/net/Uri;

    move-result-object v4

    .line 1829521
    move-object/from16 v0, p5

    invoke-static {v0, p2, p3}, LX/3Bi;->a(LX/1Pm;Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v5

    .line 1829522
    iget-object v2, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->d:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v2

    .line 1829523
    invoke-static {p3}, LX/3BH;->a(Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v2, 0x0

    move-object v3, v2

    .line 1829524
    :goto_0
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v6, 0x2

    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/16 v6, 0x8

    const v7, 0x7f0b0060

    invoke-interface {v2, v6, v7}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v2

    sget-object v6, LX/1vY;->STORY_LOCATION:LX/1vY;

    move-object/from16 v0, p6

    invoke-static {p3, p2, v6, v0}, LX/3Bi;->a(Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1vY;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/util/SparseArray;

    move-result-object v6

    invoke-interface {v2, v6}, LX/1Dh;->b(Landroid/util/SparseArray;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/Btm;->d(LX/1De;)LX/1dQ;

    move-result-object v6

    invoke-interface {v2, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v6

    if-nez v4, :cond_1

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/4 v6, 0x0

    invoke-interface {v2, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v2, v6}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPlace;->A()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    const v7, 0x7f0b0050

    invoke-virtual {v6, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, LX/1ne;->t(I)LX/1ne;

    move-result-object v6

    const v7, 0x7f0a0158

    invoke-virtual {v6, v7}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v7, 0x3

    const/4 v8, 0x2

    invoke-interface {v6, v7, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    const v7, 0x7f020ac0

    invoke-interface {v6, v7}, LX/1Di;->x(I)LX/1Di;

    move-result-object v6

    invoke-interface {v2, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p3}, LX/3BH;->b(Lcom/facebook/graphql/model/GraphQLPlace;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_2
    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    if-nez v3, :cond_3

    const/4 v2, 0x0

    :goto_3
    invoke-interface {v6, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v4

    const v6, 0x7f0a0160

    invoke-virtual {v4, v6}, LX/25Q;->i(I)LX/25Q;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v6, 0x1

    invoke-interface {v4, v6}, LX/1Di;->j(I)LX/1Di;

    move-result-object v4

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v4, v6}, LX/1Di;->b(F)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v4

    if-eqz v5, :cond_4

    const v2, 0x7f02178c

    :goto_4
    invoke-virtual {v4, v2}, LX/1o5;->h(I)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v4, 0x3

    invoke-interface {v2, v4}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const/4 v4, 0x4

    const v5, 0x7f0b0b52

    invoke-interface {v2, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 v4, 0x7

    const v5, 0x7f0b0060

    invoke-interface {v2, v4, v5}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPlace;->aa()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v2, v5, :cond_5

    const v2, 0x7f0810f8

    :goto_5
    invoke-interface {v4, v2}, LX/1Di;->A(I)LX/1Di;

    move-result-object v2

    invoke-static {p1}, LX/Btm;->e(LX/1De;)LX/1dQ;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    return-object v2

    .line 1829525
    :cond_0
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0f008f

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;->a()I

    move-result v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPlace;->D()Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;

    move-result-object v10

    invoke-virtual {v10}, Lcom/facebook/graphql/model/GraphQLPageVisitsConnection;->a()I

    move-result v10

    int-to-long v10, v10

    invoke-virtual {v2, v10, v11}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v9

    invoke-virtual {v3, v6, v7, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    goto/16 :goto_0

    .line 1829526
    :cond_1
    iget-object v2, p0, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->c:LX/1nu;

    invoke-virtual {v2, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v2

    sget-object v4, Lcom/facebook/feed/ui/location/StoryLocationPlaceInfoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v4}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v2

    sget-object v4, LX/1Up;->g:LX/1Up;

    invoke-virtual {v2, v4}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v2

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    sget-object v4, LX/1vY;->ACTOR_PHOTO:LX/1vY;

    move-object/from16 v0, p6

    invoke-static {p3, p2, v4, v0}, LX/3Bi;->a(Lcom/facebook/graphql/model/GraphQLPlace;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1vY;Lcom/facebook/analytics/logger/HoneyClientEvent;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Di;->a(Landroid/util/SparseArray;)LX/1Di;

    move-result-object v2

    invoke-static {p1}, LX/Btm;->d(LX/1De;)LX/1dQ;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b0b53

    invoke-interface {v2, v4}, LX/1Di;->i(I)LX/1Di;

    move-result-object v2

    const v4, 0x7f0b0b53

    invoke-interface {v2, v4}, LX/1Di;->q(I)LX/1Di;

    move-result-object v2

    const/4 v4, 0x5

    const/16 v7, 0x8

    invoke-interface {v2, v4, v7}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    goto/16 :goto_1

    :cond_2
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v7

    invoke-virtual {p3}, Lcom/facebook/graphql/model/GraphQLPlace;->o()LX/0Px;

    move-result-object v2

    const/4 v8, 0x0

    invoke-virtual {v2, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v7, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v7, 0x7f0b004e

    invoke-virtual {v2, v7}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v7, 0x1010212

    invoke-virtual {v2, v7}, LX/1ne;->o(I)LX/1ne;

    move-result-object v2

    const/4 v7, 0x0

    invoke-virtual {v2, v7}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    const/4 v7, 0x1

    invoke-virtual {v2, v7}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v7}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/4 v7, 0x3

    const/4 v8, 0x2

    invoke-interface {v2, v7, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    goto/16 :goto_2

    :cond_3
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    const v3, 0x7f0b004e

    invoke-virtual {v2, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v3, 0x1010212

    invoke-virtual {v2, v3}, LX/1ne;->o(I)LX/1ne;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v3}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    goto/16 :goto_3

    :cond_4
    const v2, 0x7f02178b

    goto/16 :goto_4

    :cond_5
    const v2, 0x7f0810f7

    goto/16 :goto_5
.end method
