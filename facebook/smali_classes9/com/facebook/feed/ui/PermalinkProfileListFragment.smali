.class public Lcom/facebook/feed/ui/PermalinkProfileListFragment;
.super Lcom/facebook/ufiservices/ui/ProfileListFragment;
.source ""

# interfaces
.implements LX/0fh;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/ufiservices/ui/ProfileListFragment",
        "<",
        "Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLInterfaces$PermalinkProfileListFragmentGraphQL;",
        ">;",
        "LX/0fh;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:LX/0zG;

.field private c:Lcom/facebook/ufiservices/flyout/ProfileListParams;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1711833
    const-class v0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;

    const-string v1, "permalink_profile_list"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1711832
    invoke-direct {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, Lcom/facebook/feed/ui/PermalinkProfileListFragment;

    invoke-static {p0}, LX/0zF;->a(LX/0QB;)LX/0zF;

    move-result-object p0

    check-cast p0, LX/0zG;

    iput-object p0, p1, Lcom/facebook/feed/ui/PermalinkProfileListFragment;->b:LX/0zG;

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1711809
    const-string v0, "permalink_profile_list"

    return-object v0
.end method

.method public final a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLActor;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLInterfaces$PermalinkProfileListFragmentGraphQL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1711828
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1711829
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLActor;

    .line 1711830
    invoke-static {v0}, LX/An3;->a(Lcom/facebook/graphql/model/GraphQLActor;)Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1711831
    :cond_0
    return-object v1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1711823
    invoke-super {p0, p1}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->a(Landroid/os/Bundle;)V

    .line 1711824
    const-class v0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;

    invoke-static {v0, p0}, Lcom/facebook/feed/ui/PermalinkProfileListFragment;->a(Ljava/lang/Class;LX/02k;)V

    .line 1711825
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1711826
    const-string v1, "profileListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ufiservices/flyout/ProfileListParams;

    iput-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;->c:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1711827
    return-void
.end method

.method public final m()Landroid/widget/BaseAdapter;
    .locals 2

    .prologue
    .line 1711820
    new-instance v0, LX/8rX;

    .line 1711821
    iget-object v1, p0, Lcom/facebook/ufiservices/ui/ProfileListFragment;->m:LX/55h;

    move-object v1, v1

    .line 1711822
    invoke-direct {v0, v1}, LX/8rX;-><init>(LX/55h;)V

    return-object v0
.end method

.method public final n()I
    .locals 1

    .prologue
    .line 1711819
    const v0, 0x7f0306b2

    return v0
.end method

.method public final o()Lcom/facebook/common/callercontext/CallerContext;
    .locals 1

    .prologue
    .line 1711818
    sget-object v0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-object v0
.end method

.method public final onStart()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0xf84f143

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1711813
    invoke-super {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->onStart()V

    .line 1711814
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;->b:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    iget-object v2, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;->c:Lcom/facebook/ufiservices/flyout/ProfileListParams;

    .line 1711815
    iget-object p0, v2, Lcom/facebook/ufiservices/flyout/ProfileListParams;->k:Ljava/lang/String;

    move-object v2, p0

    .line 1711816
    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1711817
    const/16 v0, 0x2b

    const v2, 0x6702c52d

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x4d975cde    # 3.1743072E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1711810
    iget-object v0, p0, Lcom/facebook/feed/ui/PermalinkProfileListFragment;->b:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    const-string v2, ""

    invoke-interface {v0, v2}, LX/0h5;->setTitle(Ljava/lang/String;)V

    .line 1711811
    invoke-super {p0}, Lcom/facebook/ufiservices/ui/ProfileListFragment;->onStop()V

    .line 1711812
    const/16 v0, 0x2b

    const v2, -0x27cf16

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
