.class public final Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1711993
    const-class v0, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;

    new-instance v1, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1711994
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1711992
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1711950
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1711951
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    const/4 v3, 0x1

    const/4 p0, 0x0

    .line 1711952
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1711953
    invoke-virtual {v1, v0, p0}, LX/15i;->g(II)I

    move-result v2

    .line 1711954
    if-eqz v2, :cond_0

    .line 1711955
    const-string v2, "__type__"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711956
    invoke-static {v1, v0, p0, p1}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 1711957
    :cond_0
    invoke-virtual {v1, v0, v3}, LX/15i;->g(II)I

    move-result v2

    .line 1711958
    if-eqz v2, :cond_1

    .line 1711959
    const-string v2, "friendship_status"

    invoke-virtual {p1, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711960
    invoke-virtual {v1, v0, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1711961
    :cond_1
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1711962
    if-eqz v2, :cond_2

    .line 1711963
    const-string v3, "id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711964
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1711965
    :cond_2
    const/4 v2, 0x3

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1711966
    if-eqz v2, :cond_3

    .line 1711967
    const-string v3, "mutual_friends"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711968
    invoke-static {v1, v2, p1}, LX/8rr;->a(LX/15i;ILX/0nX;)V

    .line 1711969
    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1711970
    if-eqz v2, :cond_4

    .line 1711971
    const-string v3, "name"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711972
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1711973
    :cond_4
    const/4 v2, 0x5

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1711974
    if-eqz v2, :cond_5

    .line 1711975
    const-string v3, "profile_picture"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711976
    invoke-static {v1, v2, p1}, LX/An7;->a(LX/15i;ILX/0nX;)V

    .line 1711977
    :cond_5
    const/4 v2, 0x6

    invoke-virtual {v1, v0, v2, p0}, LX/15i;->a(III)I

    move-result v2

    .line 1711978
    if-eqz v2, :cond_6

    .line 1711979
    const-string v3, "unread_count"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711980
    invoke-virtual {p1, v2}, LX/0nX;->b(I)V

    .line 1711981
    :cond_6
    const/4 v2, 0x7

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1711982
    if-eqz v2, :cond_7

    .line 1711983
    const-string v3, "unseen_stories"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711984
    invoke-static {v1, v2, p1}, LX/8rs;->a(LX/15i;ILX/0nX;)V

    .line 1711985
    :cond_7
    const/16 v2, 0x8

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1711986
    if-eqz v2, :cond_8

    .line 1711987
    const-string v3, "url"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1711988
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1711989
    :cond_8
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1711990
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1711991
    check-cast p1, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel$Serializer;->a(Lcom/facebook/feed/ui/PermalinkProfileListFragmentGraphQLModels$PermalinkProfileListFragmentGraphQLModel;LX/0nX;LX/0my;)V

    return-void
.end method
