.class public Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;
.super LX/1Yh;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field private final d:LX/Ajr;

.field private final e:LX/1OP;

.field public final f:Ljava/util/WeakHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/WeakHashMap",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "Ljava/util/List",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;>;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Ljava/util/concurrent/Executor;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0g8;LX/1OP;LX/22q;LX/Ajk;LX/0Sy;Ljava/util/concurrent/Executor;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 10
    .param p6    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param

    .prologue
    .line 1708342
    new-instance v3, LX/Ajo;

    invoke-direct {v3}, LX/Ajo;-><init>()V

    sget-object v4, LX/1Yi;->CLOSEST_FIRST:LX/1Yi;

    sget-object v5, LX/1Yj;->ALL_OFFSCREEN:LX/1Yj;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v6, p5

    invoke-direct/range {v1 .. v9}, LX/1Yh;-><init>(LX/0g8;LX/1Yp;LX/1Yi;LX/1Yj;LX/0Sy;ZLX/1AP;I)V

    .line 1708343
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->h:Ljava/util/concurrent/Executor;

    .line 1708344
    if-eqz p7, :cond_0

    .line 1708345
    :goto_0
    move-object/from16 v0, p7

    invoke-virtual {p3, v0, p4}, LX/22q;->a(Lcom/facebook/common/callercontext/CallerContext;LX/Ajk;)LX/Ajr;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->d:LX/Ajr;

    .line 1708346
    iput-object p2, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->e:LX/1OP;

    .line 1708347
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->f:Ljava/util/WeakHashMap;

    .line 1708348
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->g:Ljava/util/Set;

    .line 1708349
    return-void

    .line 1708350
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "native_newsfeed"

    invoke-static {v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object p7

    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    .line 1708327
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->e:LX/1OP;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1708328
    :cond_0
    :goto_0
    return-void

    .line 1708329
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->e:LX/1OP;

    invoke-interface {v0, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 1708330
    if-eqz v1, :cond_0

    .line 1708331
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->d:LX/Ajr;

    .line 1708332
    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    sget-object v3, LX/5g1;->OFF:LX/5g1;

    invoke-virtual {v0, v2, v3}, LX/Ajr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/5g1;)Ljava/util/List;

    move-result-object v2

    move-object v2, v2

    .line 1708333
    if-eqz v2, :cond_0

    .line 1708334
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1708335
    new-instance v4, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader$1;

    invoke-direct {v4, p0, v1, v0}, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader$1;-><init>(Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;Lcom/facebook/graphql/model/FeedUnit;Lcom/google/common/util/concurrent/ListenableFuture;)V

    iget-object v5, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->h:Ljava/util/concurrent/Executor;

    invoke-interface {v0, v4, v5}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_1

    .line 1708336
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1708337
    if-eqz v0, :cond_3

    .line 1708338
    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1708339
    :goto_2
    goto :goto_0

    .line 1708340
    :cond_3
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1, v2}, Ljava/util/WeakHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1708341
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->g:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    .line 1708313
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->e:LX/1OP;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 1708314
    const/4 v0, 0x0

    .line 1708315
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->g:Ljava/util/Set;

    iget-object v1, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->e:LX/1OP;

    invoke-interface {v1, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 4

    .prologue
    .line 1708316
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->e:LX/1OP;

    invoke-interface {v0}, LX/1OP;->ij_()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 1708317
    :cond_0
    return-void

    .line 1708318
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->e:LX/1OP;

    invoke-interface {v0, p1}, LX/1OP;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/1fr;->a(Ljava/lang/Object;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 1708319
    if-eqz v1, :cond_0

    .line 1708320
    iget-object v0, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->f:Ljava/util/WeakHashMap;

    invoke-virtual {v0, v1}, Ljava/util/WeakHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 1708321
    if-eqz v0, :cond_0

    .line 1708322
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1708323
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1708324
    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1708325
    iget-object v3, p0, Lcom/facebook/feed/imageloader/FeedImagePrefetchViewPreloader;->g:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1708326
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    goto :goto_0
.end method
