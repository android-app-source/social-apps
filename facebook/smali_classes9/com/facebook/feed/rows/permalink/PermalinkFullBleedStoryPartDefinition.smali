.class public Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

.field private final b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822225
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1822226
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;->a:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

    .line 1822227
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    .line 1822228
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;
    .locals 5

    .prologue
    .line 1822208
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;

    monitor-enter v1

    .line 1822209
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822210
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822211
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822212
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822213
    new-instance p0, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;-><init>(Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;)V

    .line 1822214
    move-object v0, p0

    .line 1822215
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822216
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822217
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822218
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1822221
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822222
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;->a:Lcom/facebook/feedplugins/fullbleedmedia/FullBleedStorySelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822223
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFullBleedStoryPartDefinition;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822224
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822219
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822220
    invoke-static {p1}, LX/1VF;->g(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
