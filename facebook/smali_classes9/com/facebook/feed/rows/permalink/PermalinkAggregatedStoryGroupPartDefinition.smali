.class public Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

.field private final b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

.field private final c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

.field private final d:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

.field private final e:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

.field private final f:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

.field private final g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final h:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;Lcom/facebook/permalink/rows/SeenByPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822046
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1822047
    iput-object p10, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    .line 1822048
    iput-object p9, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    .line 1822049
    iput-object p8, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    .line 1822050
    iput-object p7, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->d:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    .line 1822051
    iput-object p6, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->e:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    .line 1822052
    iput-object p5, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->f:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    .line 1822053
    iput-object p4, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 1822054
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->h:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    .line 1822055
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    .line 1822056
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->j:Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderSelectorPartDefinition;

    .line 1822057
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;
    .locals 14

    .prologue
    .line 1822058
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;

    monitor-enter v1

    .line 1822059
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822060
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822061
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822062
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822063
    new-instance v3, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    move-result-object v10

    check-cast v10, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    invoke-static {v0}, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/SeenByPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/SeenByPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    invoke-direct/range {v3 .. v13}, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;Lcom/facebook/permalink/rows/SeenByPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;)V

    .line 1822064
    move-object v0, v3

    .line 1822065
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822066
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822067
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822068
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1822069
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822070
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->j:Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822071
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822072
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->h:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822073
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->g:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822074
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->f:Lcom/facebook/feed/rows/sections/FeedAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822075
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->e:Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822076
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->d:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822077
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822078
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822079
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAggregatedStoryGroupPartDefinition;->a:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822080
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1822081
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    .line 1822082
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822083
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1822084
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1822085
    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, LX/1WP;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/1WQ;

    move-result-object v0

    invoke-static {v0}, LX/14w;->a(LX/1WQ;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method
