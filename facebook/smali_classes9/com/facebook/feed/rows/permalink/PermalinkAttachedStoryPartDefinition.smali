.class public Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final d:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final f:Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

.field private final g:Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;

.field private final h:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

.field private final i:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

.field private final j:Lcom/facebook/permalink/rows/SeenByPartDefinition;

.field private final k:LX/1x9;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;LX/14w;Lcom/facebook/permalink/rows/SeenByPartDefinition;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822099
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1822100
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 1822101
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    .line 1822102
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 1822103
    iput-object p4, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->d:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    .line 1822104
    iput-object p5, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 1822105
    iput-object p6, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->f:Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    .line 1822106
    iput-object p7, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->g:Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;

    .line 1822107
    iput-object p8, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->h:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    .line 1822108
    iput-object p9, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->i:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    .line 1822109
    iput-object p11, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->j:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    .line 1822110
    new-instance v0, LX/1x9;

    invoke-direct {v0, p10}, LX/1x9;-><init>(LX/14w;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->k:LX/1x9;

    .line 1822111
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;
    .locals 15

    .prologue
    .line 1822088
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    monitor-enter v1

    .line 1822089
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822090
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822091
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822092
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822093
    new-instance v3, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    move-result-object v11

    check-cast v11, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    invoke-static {v0}, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v13

    check-cast v13, LX/14w;

    invoke-static {v0}, Lcom/facebook/permalink/rows/SeenByPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/SeenByPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;LX/14w;Lcom/facebook/permalink/rows/SeenByPartDefinition;)V

    .line 1822094
    move-object v0, v3

    .line 1822095
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822096
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822097
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822098
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1822112
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822113
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822114
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1822115
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 1822116
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1822117
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822118
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822119
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822120
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->d:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822121
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822122
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->f:Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822123
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->g:Lcom/facebook/feed/rows/links/ActionLinkFooterPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822124
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->h:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822125
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->i:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822126
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->j:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822127
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1822087
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->k:LX/1x9;

    invoke-virtual {v0, p1}, LX/1x9;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822086
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, p1}, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
