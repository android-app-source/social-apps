.class public Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static o:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;

.field private final b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

.field private final c:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

.field private final d:Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

.field private final e:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

.field private final f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final g:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

.field private final h:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

.field private final i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

.field private final j:Lcom/facebook/feed/rows/permalink/PermalinkTextSelectorPartDefinition;

.field private final k:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

.field private final m:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

.field private final n:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkTextSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;Lcom/facebook/permalink/rows/SeenByPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822128
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1822129
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    .line 1822130
    iput-object p13, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->a:Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;

    .line 1822131
    iput-object p12, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    .line 1822132
    iput-object p11, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    .line 1822133
    iput-object p10, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->d:Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    .line 1822134
    iput-object p9, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->e:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    .line 1822135
    iput-object p7, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 1822136
    iput-object p8, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->h:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    .line 1822137
    iput-object p6, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    .line 1822138
    iput-object p5, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->j:Lcom/facebook/feed/rows/permalink/PermalinkTextSelectorPartDefinition;

    .line 1822139
    iput-object p4, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    .line 1822140
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->l:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 1822141
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->m:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 1822142
    iput-object p14, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->n:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    .line 1822143
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;
    .locals 3

    .prologue
    .line 1822144
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;

    monitor-enter v1

    .line 1822145
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->o:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822146
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->o:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822147
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822148
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822149
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822150
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822151
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;
    .locals 15

    .prologue
    .line 1822152
    new-instance v0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v2

    check-cast v2, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/permalink/PermalinkTextSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkTextSelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/permalink/PermalinkTextSelectorPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {p0}, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    invoke-static {p0}, Lcom/facebook/permalink/rows/SeenByPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/SeenByPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-static {p0}, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;

    move-result-object v13

    check-cast v13, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;

    invoke-static {p0}, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-direct/range {v0 .. v14}, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkTextSelectorPartDefinition;Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;Lcom/facebook/permalink/rows/SeenByPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;)V

    .line 1822153
    return-object v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1822154
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822155
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->m:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822156
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->l:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822157
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->n:Lcom/facebook/feedplugins/taptoadd/TapToAddPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822158
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->k:Lcom/facebook/feedplugins/graphqlstory/stickers/StickerRootPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822159
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->h:Lcom/facebook/feedplugins/attachments/MinutiaeAttachmentGroupPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822160
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->j:Lcom/facebook/feed/rows/permalink/PermalinkTextSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822161
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->i:Lcom/facebook/feedplugins/graphqlstory/translation/SeeTranslationComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822162
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->f:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822163
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->g:Lcom/facebook/feed/rows/sections/AttachmentCallToActionSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822164
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->e:Lcom/facebook/feed/rows/permalink/PermalinkProfilePhotoPromptPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822165
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->d:Lcom/facebook/feed/rows/permalink/VideoViewCountPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822166
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->c:Lcom/facebook/feed/rows/sections/SubStoriesHScrollPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822167
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->a:Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822168
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkBasicGroupPartDefinition;->b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822169
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822170
    const/4 v0, 0x1

    return v0
.end method
