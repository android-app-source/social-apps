.class public Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

.field public final b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field public final e:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

.field public final f:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822409
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1822410
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 1822411
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->c:Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    .line 1822412
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->d:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 1822413
    iput-object p4, p0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->e:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    .line 1822414
    iput-object p5, p0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->f:Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    .line 1822415
    iput-object p6, p0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->a:Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    .line 1822416
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;
    .locals 10

    .prologue
    .line 1822420
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    monitor-enter v1

    .line 1822421
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822422
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822423
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822424
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822425
    new-instance v3, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/BlingBarSelectorPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;)V

    .line 1822426
    move-object v0, v3

    .line 1822427
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822428
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822429
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822430
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1822431
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822432
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;->a:Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;

    new-instance v1, LX/36I;

    .line 1822433
    new-instance v2, LX/Box;

    invoke-direct {v2, p0}, LX/Box;-><init>(Lcom/facebook/feed/rows/permalink/PermalinkSubStoriesPartDefinition;)V

    move-object v2, v2

    .line 1822434
    invoke-direct {v1, p2, v2}, LX/36I;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/36K;)V

    invoke-virtual {p1, v0, v1}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822435
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822417
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822418
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822419
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/SubStoriesGroupPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method
