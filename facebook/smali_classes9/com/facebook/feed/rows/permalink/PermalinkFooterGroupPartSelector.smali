.class public Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

.field private final c:Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;

.field private final d:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

.field private final e:LX/0pf;

.field private final f:Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;


# direct methods
.method public constructor <init>(LX/0Ot;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;LX/0pf;Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;",
            ">;",
            "Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;",
            "Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;",
            "Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;",
            "LX/0pf;",
            "Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822171
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1822172
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->a:LX/0Ot;

    .line 1822173
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    .line 1822174
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->c:Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;

    .line 1822175
    iput-object p4, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->d:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    .line 1822176
    iput-object p5, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->e:LX/0pf;

    .line 1822177
    iput-object p6, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->f:Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;

    .line 1822178
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;
    .locals 10

    .prologue
    .line 1822179
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;

    monitor-enter v1

    .line 1822180
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822181
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822182
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822183
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822184
    new-instance v3, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;

    const/16 v4, 0x2df8

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {v0}, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-static {v0}, LX/0pf;->a(LX/0QB;)LX/0pf;

    move-result-object v8

    check-cast v8, LX/0pf;

    invoke-static {v0}, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;-><init>(LX/0Ot;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;LX/0pf;Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;)V

    .line 1822185
    move-object v0, v3

    .line 1822186
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822187
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822188
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822189
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1822190
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pf;

    .line 1822191
    invoke-interface {p3}, LX/1Pl;->r()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 1822192
    :goto_0
    iget-object v2, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->e:LX/0pf;

    .line 1822193
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822194
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v2, v0}, LX/0pf;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/1g0;

    move-result-object v0

    .line 1822195
    iget-boolean v2, v0, LX/1g0;->o:Z

    move v0, v2

    .line 1822196
    if-eqz v0, :cond_1

    .line 1822197
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822198
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->c:Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822199
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->a:LX/0Ot;

    invoke-virtual {p1, v1, v0, p2}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 1822200
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->d:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822201
    :goto_1
    const/4 v0, 0x0

    return-object v0

    .line 1822202
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 1822203
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->c:Lcom/facebook/feed/rows/permalink/ShareCountPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822204
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->a:LX/0Ot;

    invoke-virtual {p1, v1, v0, p2}, LX/1RF;->a(ZLX/0Ot;Ljava/lang/Object;)Z

    .line 1822205
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->d:Lcom/facebook/feed/rows/sections/StoryPostFooterSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822206
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->f:Lcom/facebook/permalink/rows/ReactorsDescriptionPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkFooterGroupPartSelector;->b:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822207
    const/4 v0, 0x1

    return v0
.end method
