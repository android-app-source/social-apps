.class public Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

.field private final b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

.field private final c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

.field private final d:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

.field private final e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

.field private final f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

.field private final g:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

.field private final h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;Lcom/facebook/permalink/rows/SeenByPartDefinition;Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822338
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1822339
    iput-object p8, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->a:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    .line 1822340
    iput-object p7, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    .line 1822341
    iput-object p6, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    .line 1822342
    iput-object p5, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->d:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    .line 1822343
    iput-object p4, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    .line 1822344
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    .line 1822345
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->g:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    .line 1822346
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    .line 1822347
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;
    .locals 12

    .prologue
    .line 1822362
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;

    monitor-enter v1

    .line 1822363
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822364
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822365
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822366
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822367
    new-instance v3, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/SeenByPartDefinition;->a(LX/0QB;)Lcom/facebook/permalink/rows/SeenByPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-static {v0}, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;->a(LX/0QB;)Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    move-result-object v11

    check-cast v11, Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;Lcom/facebook/permalink/rows/SeenByPartDefinition;Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;)V

    .line 1822368
    move-object v0, v3

    .line 1822369
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822370
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822371
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822372
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1822352
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822353
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->h:Lcom/facebook/feed/rows/sections/header/ExplanationSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822354
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->f:Lcom/facebook/feed/rows/sections/header/components/FeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822355
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->g:Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822356
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->e:Lcom/facebook/feedplugins/attachments/AttachmentsPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822357
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->d:Lcom/facebook/feed/rows/permalink/PermalinkAttachedStoryPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822358
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->a:Lcom/facebook/permalink/rows/PermalinkTopLevelFooterPartSelector;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822359
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->c:Lcom/facebook/permalink/rows/LikesDescriptionPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822360
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkSharedStoryPartDefinition;->b:Lcom/facebook/permalink/rows/SeenByPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1822361
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1822348
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822349
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822350
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1822351
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
