.class public Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition",
            "<TE;",
            "LX/2ee;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition",
            "<",
            "LX/2ee;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822467
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1822468
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    .line 1822469
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;->b:Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    .line 1822470
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;
    .locals 5

    .prologue
    .line 1822471
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;

    monitor-enter v1

    .line 1822472
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822473
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822474
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822475
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822476
    new-instance p0, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;)V

    .line 1822477
    move-object v0, p0

    .line 1822478
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822479
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822480
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822481
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1822482
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1822483
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822484
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822485
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextHeaderPartDefinition;->b:Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822486
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822487
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822488
    invoke-static {p1}, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    return v0
.end method
