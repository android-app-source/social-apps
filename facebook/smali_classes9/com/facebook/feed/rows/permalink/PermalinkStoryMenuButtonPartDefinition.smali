.class public Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/24a;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Boolean;",
        "LX/1Pk;",
        "TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final b:LX/1VF;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822373
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1822374
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 1822375
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;->b:LX/1VF;

    .line 1822376
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;
    .locals 5

    .prologue
    .line 1822377
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    monitor-enter v1

    .line 1822378
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822379
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822380
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822381
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822382
    new-instance p0, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v4

    check-cast v4, LX/1VF;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1VF;)V

    .line 1822383
    move-object v0, p0

    .line 1822384
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822385
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822386
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822387
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1822388
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pk;

    const/4 v1, 0x1

    .line 1822389
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822390
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1822391
    invoke-static {p2}, LX/182;->o(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1822392
    :goto_0
    iget-object v2, p0, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;->b:LX/1VF;

    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v3

    invoke-virtual {v2, p2, v0, v3, v1}, LX/1VF;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;Z)Z

    move-result v0

    .line 1822393
    if-eqz v0, :cond_1

    sget-object v0, LX/1dl;->CLICKABLE:LX/1dl;

    .line 1822394
    :goto_1
    const v2, 0x7f0d0bde

    iget-object v3, p0, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v4, LX/24b;

    invoke-direct {v4, p2, v0}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v2, v3, v4}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1822395
    sget-object v2, LX/1dl;->HIDDEN:LX/1dl;

    if-eq v0, v2, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p2

    .line 1822396
    goto :goto_0

    .line 1822397
    :cond_1
    sget-object v0, LX/1dl;->HIDDEN:LX/1dl;

    goto :goto_1

    .line 1822398
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x8f26e57

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1822399
    check-cast p2, Ljava/lang/Boolean;

    .line 1822400
    check-cast p4, LX/24a;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {p4, v1}, LX/24a;->setMenuButtonActive(Z)V

    .line 1822401
    const/16 v1, 0x1f

    const v2, -0x48248505

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
