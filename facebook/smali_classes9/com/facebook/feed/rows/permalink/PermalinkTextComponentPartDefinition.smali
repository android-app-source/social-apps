.class public Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1WE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1WE",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/1V0;

.field private final f:LX/1VF;

.field private final g:LX/1VI;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1WE;LX/1V0;LX/1VF;LX/1VI;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822442
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1822443
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->d:LX/1WE;

    .line 1822444
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->e:LX/1V0;

    .line 1822445
    iput-object p4, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->f:LX/1VF;

    .line 1822446
    iput-object p5, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->g:LX/1VI;

    .line 1822447
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1822436
    iget-object v0, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->d:LX/1WE;

    invoke-virtual {v0, p1}, LX/1WE;->c(LX/1De;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1XG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1XG;->a(LX/1Pn;)LX/1XG;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/1XG;->a(Z)LX/1XG;

    move-result-object v0

    .line 1822437
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->g:LX/1VI;

    invoke-virtual {v1}, LX/1VI;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1822438
    const v1, 0x7f0a00aa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1XG;->a(Ljava/lang/Integer;)LX/1XG;

    .line 1822439
    :cond_0
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1822440
    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 1822441
    iget-object v2, p0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v1, v0}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;
    .locals 9

    .prologue
    .line 1822455
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    monitor-enter v1

    .line 1822456
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822457
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822458
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822459
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822460
    new-instance v3, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1WE;->a(LX/0QB;)LX/1WE;

    move-result-object v5

    check-cast v5, LX/1WE;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v6

    check-cast v6, LX/1V0;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v7

    check-cast v7, LX/1VF;

    invoke-static {v0}, LX/1VI;->a(LX/0QB;)LX/1VI;

    move-result-object v8

    check-cast v8, LX/1VI;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;-><init>(Landroid/content/Context;LX/1WE;LX/1V0;LX/1VF;LX/1VI;)V

    .line 1822461
    move-object v0, v3

    .line 1822462
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822463
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822464
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822465
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1822454
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1822466
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/permalink/PermalinkTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822451
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822452
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822453
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VF;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1822449
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822450
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 1822448
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
