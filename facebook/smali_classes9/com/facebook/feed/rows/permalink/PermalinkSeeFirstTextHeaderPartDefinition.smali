.class public Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3VB;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition",
            "<TE;",
            "LX/3VB;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition",
            "<",
            "LX/3VB;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/1JC;

.field private final d:Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition",
            "<",
            "LX/3VB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;LX/1JC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822309
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1822310
    iput-object p2, p0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    .line 1822311
    iput-object p3, p0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;

    .line 1822312
    iput-object p4, p0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->c:LX/1JC;

    .line 1822313
    iput-object p1, p0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->d:Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    .line 1822314
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;
    .locals 7

    .prologue
    .line 1822315
    const-class v1, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;

    monitor-enter v1

    .line 1822316
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822317
    sput-object v2, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822318
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822319
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822320
    new-instance p0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;

    invoke-static {v0}, LX/1JC;->a(LX/0QB;)LX/1JC;

    move-result-object v6

    check-cast v6, LX/1JC;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;-><init>(Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;LX/1JC;)V

    .line 1822321
    move-object v0, p0

    .line 1822322
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822323
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822324
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822325
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1822326
    sget-object v0, LX/3VB;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1822327
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822328
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822329
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1822330
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822331
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->d:Lcom/facebook/feed/rows/permalink/PermalinkStoryMenuButtonPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822332
    iget-object v1, p0, Lcom/facebook/feed/rows/permalink/PermalinkSeeFirstTextHeaderPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/header/SeeFirstTooltipPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822333
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822334
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1822335
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1822336
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1822337
    invoke-static {v0}, LX/1JC;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feed/rows/sections/header/TextHeaderPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
