.class public Lcom/facebook/feed/rows/photosfeed/MediaMetadataBlingBarPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Bp2;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3VF;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

.field private final c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822657
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1822658
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataBlingBarPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1822659
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    .line 1822660
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataBlingBarPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1822661
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1822662
    check-cast p2, LX/5kD;

    check-cast p3, LX/Bp2;

    .line 1822663
    invoke-interface {p2}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1822664
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataBlingBarPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v2, LX/1X6;

    sget-object v3, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->a:LX/1Ua;

    invoke-direct {v2, v3}, LX/1X6;-><init>(LX/1Ua;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822665
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataBlingBarPartDefinition;->b:Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;

    new-instance v2, LX/Anj;

    new-instance v3, LX/4Vj;

    invoke-interface {p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/4Vj;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1822666
    invoke-static {v0}, LX/Ani;->b(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v5

    if-gtz v5, :cond_0

    invoke-static {v0}, LX/Ani;->a(Lcom/facebook/graphql/model/GraphQLFeedback;)I

    move-result v5

    if-lez v5, :cond_1

    :cond_0
    const/4 v5, 0x1

    :goto_0
    move v5, v5

    .line 1822667
    invoke-direct {v2, v3, v0, v4, v5}, LX/Anj;-><init>(LX/0jW;Lcom/facebook/graphql/model/GraphQLFeedback;Ljava/lang/String;Z)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822668
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataBlingBarPartDefinition;->c:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    new-instance v2, LX/BpB;

    invoke-direct {v2, p0, p3, v0}, LX/BpB;-><init>(Lcom/facebook/feed/rows/photosfeed/MediaMetadataBlingBarPartDefinition;LX/Bp2;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822669
    const/4 v0, 0x0

    return-object v0

    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method
