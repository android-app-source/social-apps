.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummarySelectorPartDefinition;

.field private final d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummarySelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824167
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1824168
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition;

    .line 1824169
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;

    .line 1824170
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummarySelectorPartDefinition;

    .line 1824171
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;

    .line 1824172
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;
    .locals 7

    .prologue
    .line 1824173
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;

    monitor-enter v1

    .line 1824174
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824175
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824176
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824177
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824178
    new-instance p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummarySelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummarySelectorPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummarySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummarySelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;)V

    .line 1824179
    move-object v0, p0

    .line 1824180
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824181
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824182
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824183
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1824184
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1824185
    invoke-static {p2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1824186
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryHeaderComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824187
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824188
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryUFIFeedbackSummarySelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824189
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryGroupPartDefinition;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824190
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1824191
    const/4 v0, 0x1

    return v0
.end method
