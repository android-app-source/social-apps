.class public final Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;III)V
    .locals 0

    .prologue
    .line 1823574
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iput p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->a:I

    iput p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->b:I

    iput p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1823575
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v0}, LX/0g8;->B()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1823576
    iget v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->c:I

    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 1823577
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->a:I

    iget v3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->b:I

    iget v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->c:I

    add-int/lit8 v4, v4, 0x1

    invoke-static {v1, v2, v3, v4}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;III)Ljava/lang/Runnable;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(Ljava/lang/Runnable;)V

    .line 1823578
    :goto_0
    return-void

    .line 1823579
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->a:I

    invoke-static {v1, v2}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->f$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)I

    move-result v1

    iget v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->b:I

    invoke-interface {v0, v1, v2}, LX/0g8;->d(II)V

    .line 1823580
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Z)V

    goto :goto_0

    .line 1823581
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->s:LX/03V;

    const-string v2, "PhotosFeedFragment_inconsistentAdapterViewCountThreshold"

    const-string v3, "GraphQLStory Id is %s"

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    iget-object v0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823582
    iget-object v4, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v4

    .line 1823583
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1823584
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Z)V

    goto :goto_0

    .line 1823585
    :cond_2
    const-string v0, "unknown"

    goto :goto_1
.end method
