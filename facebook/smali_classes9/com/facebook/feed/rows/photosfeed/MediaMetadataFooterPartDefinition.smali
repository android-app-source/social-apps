.class public Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Bp2;",
        ":",
        "LX/Bp4;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/5kD;",
        "LX/BpE;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

.field public final c:Landroid/content/Context;

.field private final d:LX/03V;

.field public final e:LX/1Kf;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;Landroid/content/Context;LX/03V;LX/1Kf;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;",
            "Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Kf;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822687
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1822688
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->a:LX/0Or;

    .line 1822689
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    .line 1822690
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->c:Landroid/content/Context;

    .line 1822691
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->d:LX/03V;

    .line 1822692
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->e:LX/1Kf;

    .line 1822693
    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->f:LX/0Ot;

    .line 1822694
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;
    .locals 10

    .prologue
    .line 1822695
    const-class v1, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;

    monitor-enter v1

    .line 1822696
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822697
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822698
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822699
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822700
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;

    const/16 v4, 0x13a4

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v7

    check-cast v7, LX/03V;

    invoke-static {v0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v8

    check-cast v8, LX/1Kf;

    const/16 v9, 0xbc6

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;-><init>(LX/0Or;Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;Landroid/content/Context;LX/03V;LX/1Kf;LX/0Ot;)V

    .line 1822701
    move-object v0, v3

    .line 1822702
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822703
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822704
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822705
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/5kD;)Z
    .locals 1

    .prologue
    .line 1822706
    invoke-interface {p0}, LX/5kD;->s()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/5kD;->w()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1822707
    check-cast p2, LX/5kD;

    check-cast p3, LX/1Po;

    const/4 v1, 0x0

    .line 1822708
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    new-instance v2, LX/BpC;

    invoke-direct {v2, p0, p2, p3}, LX/BpC;-><init>(Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;LX/5kD;LX/1Po;)V

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822709
    check-cast p3, LX/1Pr;

    new-instance v0, LX/213;

    invoke-interface {p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, LX/213;-><init>(Ljava/lang/String;)V

    new-instance v2, LX/4Vj;

    invoke-interface {p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, LX/4Vj;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/EnumMap;

    .line 1822710
    invoke-interface {p2}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v4

    .line 1822711
    if-eqz v4, :cond_0

    .line 1822712
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->aC_()Z

    move-result v3

    .line 1822713
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->c()Z

    move-result v2

    .line 1822714
    invoke-virtual {v4}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->k()Z

    move-result v1

    .line 1822715
    :goto_0
    invoke-static {p2}, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->a(LX/5kD;)Z

    move-result v4

    iget-object v5, p0, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->a:LX/0Or;

    invoke-static {v3, v2, v4, v0, v5}, LX/212;->a(ZZZLjava/util/EnumMap;LX/0Or;)V

    .line 1822716
    new-instance v2, LX/BpE;

    invoke-direct {v2, v0, v1}, LX/BpE;-><init>(Ljava/util/EnumMap;Z)V

    return-object v2

    :cond_0
    move v2, v1

    move v3, v1

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x62dc40b3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1822717
    check-cast p2, LX/BpE;

    .line 1822718
    move-object v1, p4

    check-cast v1, LX/1wK;

    iget-object v2, p2, LX/BpE;->a:Ljava/util/EnumMap;

    invoke-virtual {v2}, Ljava/util/EnumMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1wK;->setButtons(Ljava/util/Set;)V

    move-object v1, p4

    .line 1822719
    check-cast v1, LX/1wK;

    iget-object v2, p2, LX/BpE;->a:Ljava/util/EnumMap;

    invoke-interface {v1, v2}, LX/1wK;->setSprings(Ljava/util/EnumMap;)V

    .line 1822720
    check-cast p4, LX/1wK;

    iget-boolean v1, p2, LX/BpE;->b:Z

    invoke-interface {p4, v1}, LX/1wK;->setIsLiked(Z)V

    .line 1822721
    const/16 v1, 0x1f

    const v2, -0x3c1e34a4

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 1822722
    check-cast p4, LX/1wK;

    invoke-interface {p4}, LX/1wK;->a()V

    .line 1822723
    return-void
.end method
