.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/Bp2;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

.field private final b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822769
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1822770
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    .line 1822771
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;

    .line 1822772
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;
    .locals 5

    .prologue
    .line 1822777
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;

    monitor-enter v1

    .line 1822778
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822779
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822780
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822781
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822782
    new-instance p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;)V

    .line 1822783
    move-object v0, p0

    .line 1822784
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822785
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822786
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822787
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1822774
    check-cast p2, LX/5kD;

    .line 1822775
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1822776
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822773
    const/4 v0, 0x1

    return v0
.end method
