.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        "LX/Bpg;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition",
            "<",
            "LX/Bpg;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummarySelectorPartDefinition;

.field private final c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;

.field private final d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition",
            "<",
            "LX/Bpg;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition",
            "<",
            "LX/Bpg;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition",
            "<",
            "LX/Bpg;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition",
            "<",
            "LX/Bpg;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition",
            "<",
            "LX/Bpg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummarySelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824038
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1824039
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;

    .line 1824040
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummarySelectorPartDefinition;

    .line 1824041
    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;

    .line 1824042
    iput-object p7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->e:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    .line 1824043
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;

    .line 1824044
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->f:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;

    .line 1824045
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->g:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;

    .line 1824046
    iput-object p8, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->h:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;

    .line 1824047
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;
    .locals 12

    .prologue
    .line 1824048
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;

    monitor-enter v1

    .line 1824049
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824050
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824051
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824052
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824053
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummarySelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummarySelectorPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummarySelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummarySelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;)V

    .line 1824054
    move-object v0, v3

    .line 1824055
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824056
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824057
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824058
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1824059
    check-cast p2, LX/5kD;

    check-cast p3, LX/Bpg;

    .line 1824060
    invoke-virtual {p3}, LX/Bpg;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->PHOTOS_FEED:LX/1Qt;

    if-ne v0, v1, :cond_1

    .line 1824061
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->d:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->h:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentSphericalImagePartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->f:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1824062
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->g:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentVideoPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824063
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824064
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentUFIFeedbackSummarySelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824065
    invoke-virtual {p3}, LX/Bpg;->c()LX/1PT;

    move-result-object v0

    invoke-interface {v0}, LX/1PT;->a()LX/1Qt;

    move-result-object v0

    sget-object v1, LX/1Qt;->PHOTOS_FEED:LX/1Qt;

    if-ne v0, v1, :cond_0

    .line 1824066
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->e:Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentMessagePartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824067
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824068
    const/4 v0, 0x0

    return-object v0

    .line 1824069
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedMediaMetadataRootPartDefinition;->f:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageSelectorPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1824070
    const/4 v0, 0x1

    return v0
.end method
