.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/14w;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;LX/14w;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824125
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1824126
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->a:LX/14w;

    .line 1824127
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 1824128
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;

    .line 1824129
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;
    .locals 6

    .prologue
    .line 1824131
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;

    monitor-enter v1

    .line 1824132
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824133
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824134
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824135
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824136
    new-instance p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v4

    check-cast v4, LX/14w;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;LX/14w;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;)V

    .line 1824137
    move-object v0, p0

    .line 1824138
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824139
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824140
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824141
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLStory;LX/14w;)Z
    .locals 1

    .prologue
    .line 1824130
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0}, LX/14w;->f(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1824142
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1824118
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824119
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/FooterPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824120
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v1, LX/20d;

    sget-object v2, LX/1Wi;->PHOTOS_FEED:LX/1Wi;

    invoke-direct {v1, p2, v2}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824121
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1824122
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824123
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1824124
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->a:LX/14w;

    invoke-static {v0, v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;LX/14w;)Z

    move-result v0

    return v0
.end method
