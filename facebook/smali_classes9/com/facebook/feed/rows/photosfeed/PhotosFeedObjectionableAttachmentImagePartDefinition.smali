.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/3VN;",
        ">;"
    }
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

.field private final b:Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;

.field private final c:LX/1WM;

.field private final d:LX/1ev;

.field private final e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final f:LX/1f2;


# direct methods
.method public constructor <init>(Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;LX/1WM;LX/1ev;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1f2;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824071
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1824072
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->a:Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    .line 1824073
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->b:Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;

    .line 1824074
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->c:LX/1WM;

    .line 1824075
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->d:LX/1ev;

    .line 1824076
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1824077
    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->f:LX/1f2;

    .line 1824078
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;
    .locals 10

    .prologue
    .line 1824098
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;

    monitor-enter v1

    .line 1824099
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824100
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824101
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824102
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824103
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;

    invoke-static {v0}, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;->a(LX/0QB;)Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    invoke-static {v0}, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;->a(LX/0QB;)Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;

    invoke-static {v0}, LX/1WM;->a(LX/0QB;)LX/1WM;

    move-result-object v6

    check-cast v6, LX/1WM;

    invoke-static {v0}, LX/1ev;->a(LX/0QB;)LX/1ev;

    move-result-object v7

    check-cast v7, LX/1ev;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1f2;->b(LX/0QB;)LX/1f2;

    move-result-object v9

    check-cast v9, LX/1f2;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;-><init>(Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;LX/1WM;LX/1ev;Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1f2;)V

    .line 1824104
    move-object v0, v3

    .line 1824105
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824106
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824107
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824108
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1824097
    sget-object v0, LX/3VN;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1824083
    check-cast p2, LX/5kD;

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 1824084
    invoke-static {p2}, LX/5k9;->a(LX/5kD;)Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v5

    .line 1824085
    new-instance v0, LX/1X6;

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/1UY;->b(I)LX/1UY;

    move-result-object v1

    invoke-virtual {v1}, LX/1UY;->i()LX/1Ua;

    move-result-object v1

    sget-object v2, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v7, v1, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 1824086
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->e:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-interface {p1, v1, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824087
    invoke-interface {p2}, LX/5kD;->C()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$MediaMetadataCreationStoryModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1824088
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->d:LX/1ev;

    invoke-virtual {v0, v5}, LX/1ev;->c(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1f6;

    move-result-object v0

    .line 1824089
    iget v1, v0, LX/1f6;->g:I

    move v3, v1

    .line 1824090
    iget v1, v0, LX/1f6;->h:I

    move v0, v1

    .line 1824091
    new-instance v1, LX/24k;

    invoke-direct {v1, v3, v0}, LX/24k;-><init>(II)V

    .line 1824092
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->a:Lcom/facebook/photos/warning/rows/ObjectionableContentWarningPartDefinition;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6, v4, v1}, LX/Ccm;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;ZLX/24k;)LX/Ccm;

    move-result-object v3

    invoke-interface {p1, v0, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824093
    iget-object v3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->f:LX/1f2;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->V()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    :goto_0
    iget-object v6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->c:LX/1WM;

    invoke-virtual {v6}, LX/1WM;->a()LX/33B;

    move-result-object v6

    invoke-virtual {v3, v5, v0, v7, v6}, LX/1f2;->a(Lcom/facebook/graphql/model/GraphQLMedia;Lcom/facebook/graphql/model/GraphQLImage;LX/1Ad;LX/33B;)LX/1bf;

    move-result-object v3

    .line 1824094
    iget-object v6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->b:Lcom/facebook/photos/warning/rows/ObjectionableContentImagePartDefinition;

    new-instance v0, LX/CcZ;

    invoke-static {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/CcZ;-><init>(LX/24k;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1bf;ZLjava/lang/String;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824095
    return-object v7

    .line 1824096
    :cond_0
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLMedia;->U()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1824079
    check-cast p1, LX/5kD;

    .line 1824080
    invoke-interface {p1}, LX/5kD;->S()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedObjectionableAttachmentImagePartDefinition;->c:LX/1WM;

    .line 1824081
    invoke-static {v0}, LX/1WM;->b(LX/1WM;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-static {p1}, LX/1WM;->b(LX/5kD;)Z

    move-result p0

    if-eqz p0, :cond_1

    invoke-interface {p1}, LX/5kD;->d()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/1WM;->a(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_1

    const/4 p0, 0x1

    :goto_0
    move v0, p0

    .line 1824082
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method
