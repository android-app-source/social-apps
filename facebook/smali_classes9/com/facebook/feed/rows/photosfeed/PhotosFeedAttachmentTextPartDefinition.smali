.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "Landroid/text/Layout;",
        "LX/1Ps;",
        "LX/BtI;",
        ">;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:LX/1nq;

.field private final c:LX/03V;

.field private final d:LX/1zB;

.field private final e:LX/BpX;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1W9;LX/03V;Landroid/content/Context;LX/1zB;LX/BpX;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823191
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1823192
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->c:LX/03V;

    .line 1823193
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->d:LX/1zB;

    .line 1823194
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1823195
    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->e:LX/BpX;

    .line 1823196
    invoke-static {p4, p2}, LX/1zB;->a(Landroid/content/Context;LX/1W9;)LX/1nq;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->b:LX/1nq;

    .line 1823197
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;
    .locals 10

    .prologue
    .line 1823198
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;

    monitor-enter v1

    .line 1823199
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1823200
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823201
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1823202
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1823203
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    invoke-static {v0}, LX/1W9;->a(LX/0QB;)LX/1W9;

    move-result-object v5

    check-cast v5, LX/1W9;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v6

    check-cast v6, LX/03V;

    const-class v7, Landroid/content/Context;

    invoke-interface {v0, v7}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/Context;

    invoke-static {v0}, LX/1zB;->a(LX/0QB;)LX/1zB;

    move-result-object v8

    check-cast v8, LX/1zB;

    invoke-static {v0}, LX/BpX;->a(LX/0QB;)LX/BpX;

    move-result-object v9

    check-cast v9, LX/BpX;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;-><init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;LX/1W9;LX/03V;Landroid/content/Context;LX/1zB;LX/BpX;)V

    .line 1823204
    move-object v0, v3

    .line 1823205
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1823206
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1823207
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1823208
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/5kD;)Z
    .locals 1

    .prologue
    .line 1823209
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/5kD;->X()LX/175;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1823210
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextLayoutBasePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1823211
    check-cast p2, LX/5kD;

    const/4 v3, 0x0

    .line 1823212
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    sget-object v2, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v1, v3, v2}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1823213
    invoke-interface {p2}, LX/5kD;->X()LX/175;

    move-result-object v0

    invoke-static {v0}, LX/9JZ;->a(LX/175;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 1823214
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->e:LX/BpX;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->b:LX/1nq;

    invoke-virtual {v2}, LX/1nq;->b()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v1, v0, v2}, LX/BpX;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;I)Landroid/text/Spannable;

    move-result-object v1

    .line 1823215
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->d:LX/1zB;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->b:LX/1nq;

    invoke-virtual {v0, v2, v3}, LX/1zB;->a(LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1823216
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->b:LX/1nq;

    invoke-virtual {v0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1823217
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->b:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    .line 1823218
    return-object v0

    .line 1823219
    :catch_0
    move-exception v0

    .line 1823220
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->c:LX/03V;

    const-string v3, "PhotosFeedAttachmentTextPartDefinition"

    const-string v4, "JellyBean setText bug with MediaMetadata ID: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v3

    .line 1823221
    iput-object v0, v3, LX/0VK;->c:Ljava/lang/Throwable;

    .line 1823222
    move-object v0, v3

    .line 1823223
    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/03V;->a(LX/0VG;)V

    .line 1823224
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->b:LX/1nq;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x38fc278b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1823225
    check-cast p2, Landroid/text/Layout;

    check-cast p4, LX/BtI;

    .line 1823226
    invoke-virtual {p4, p2}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setTextLayout(Landroid/text/Layout;)V

    .line 1823227
    const/16 v1, 0x1f

    const v2, -0x7c231167

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1823228
    check-cast p1, LX/5kD;

    invoke-static {p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;->a(LX/5kD;)Z

    move-result v0

    return v0
.end method
