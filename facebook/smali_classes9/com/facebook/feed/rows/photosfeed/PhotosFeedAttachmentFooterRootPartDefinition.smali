.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/Bp2;",
        ":",
        "LX/Bp4;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/1wK;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822738
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1822739
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    .line 1822740
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;

    .line 1822741
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;
    .locals 5

    .prologue
    .line 1822742
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    monitor-enter v1

    .line 1822743
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822744
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822745
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822746
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822747
    new-instance p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;)V

    .line 1822748
    move-object v0, p0

    .line 1822749
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822750
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822751
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822752
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/5kD;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1822753
    invoke-static {p0}, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->a(LX/5kD;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1822754
    :cond_0
    :goto_0
    return v0

    .line 1822755
    :cond_1
    invoke-interface {p0}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v2

    .line 1822756
    if-nez v2, :cond_2

    move v0, v1

    .line 1822757
    goto :goto_0

    .line 1822758
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->c()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->aC_()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1822759
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1822760
    check-cast p2, LX/5kD;

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 1822761
    invoke-interface {p2}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    .line 1822762
    if-eqz v0, :cond_0

    .line 1822763
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->aC_()Z

    move-result v1

    .line 1822764
    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->c()Z

    move-result v2

    .line 1822765
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822766
    iget-object v6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/DefaultFooterBackgroundPartDefinition;

    new-instance v0, LX/21P;

    invoke-static {p2}, Lcom/facebook/feed/rows/photosfeed/MediaMetadataFooterPartDefinition;->a(LX/5kD;)Z

    move-result v3

    sget-object v4, LX/1Wi;->TOP:LX/1Wi;

    invoke-direct/range {v0 .. v5}, LX/21P;-><init>(ZZZLX/1Wi;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v6, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1822767
    return-object v5

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1822768
    check-cast p1, LX/5kD;

    invoke-static {p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->a(LX/5kD;)Z

    move-result v0

    return v0
.end method
