.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Bp2;",
        ":",
        "LX/Bp4;",
        ":",
        "LX/Bp6;",
        "V:",
        "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition",
            "<TE;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823121
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1823122
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    .line 1823123
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    .line 1823124
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;
    .locals 5

    .prologue
    .line 1823125
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;

    monitor-enter v1

    .line 1823126
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1823127
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1823129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1823130
    new-instance p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;)V

    .line 1823131
    move-object v0, p0

    .line 1823132
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1823133
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1823134
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1823135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1823136
    sget-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->h:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1823137
    check-cast p2, LX/5kD;

    .line 1823138
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1823139
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentRootReactionsFooterPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1823140
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1823141
    check-cast p1, LX/5kD;

    .line 1823142
    if-eqz p1, :cond_0

    invoke-interface {p1}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;->aD_()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentFooterRootPartDefinition;->a(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
