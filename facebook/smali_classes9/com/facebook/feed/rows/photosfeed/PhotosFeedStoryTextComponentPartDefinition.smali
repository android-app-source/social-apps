.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final d:LX/1WE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1WE",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:LX/Bpc;

.field private final f:LX/1VH;

.field private final g:LX/1VF;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1WE;LX/Bpc;LX/1VH;LX/1VF;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824288
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1824289
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->d:LX/1WE;

    .line 1824290
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->e:LX/Bpc;

    .line 1824291
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->f:LX/1VH;

    .line 1824292
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->g:LX/1VF;

    .line 1824293
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1824265
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->d:LX/1WE;

    invoke-virtual {v0, p1}, LX/1WE;->c(LX/1De;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1XG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/1XG;->a(LX/1Pn;)LX/1XG;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1824266
    new-instance v1, LX/Bpe;

    sget-object v2, LX/1Ua;->d:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/Bpe;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    .line 1824267
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->e:LX/Bpc;

    invoke-virtual {v2, p1}, LX/Bpc;->c(LX/1De;)LX/Bpa;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/Bpa;->a(LX/1X1;)LX/Bpa;

    move-result-object v0

    check-cast p3, LX/1Ps;

    invoke-virtual {v0, p3}, LX/Bpa;->a(LX/1Ps;)LX/Bpa;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/Bpa;->a(LX/Bpe;)LX/Bpa;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1824268
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->f:LX/1VH;

    invoke-virtual {v1, p1}, LX/1VH;->c(LX/1De;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XD;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XD;

    move-result-object v1

    sget-object v2, LX/1EO;->PHOTOS_FEED:LX/1EO;

    invoke-virtual {v1, v2}, LX/1XD;->a(LX/1EO;)LX/1XD;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1XD;->a(LX/1X1;)LX/1XD;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;
    .locals 9

    .prologue
    .line 1824277
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;

    monitor-enter v1

    .line 1824278
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824279
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824280
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824281
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824282
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1WE;->a(LX/0QB;)LX/1WE;

    move-result-object v5

    check-cast v5, LX/1WE;

    invoke-static {v0}, LX/Bpc;->a(LX/0QB;)LX/Bpc;

    move-result-object v6

    check-cast v6, LX/Bpc;

    invoke-static {v0}, LX/1VH;->a(LX/0QB;)LX/1VH;

    move-result-object v7

    check-cast v7, LX/1VH;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v8

    check-cast v8, LX/1VF;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;-><init>(Landroid/content/Context;LX/1WE;LX/Bpc;LX/1VH;LX/1VF;)V

    .line 1824283
    move-object v0, v3

    .line 1824284
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824285
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824286
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824287
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1824276
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1824275
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryTextComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1824272
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824273
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1824274
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/1VF;->b(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1824270
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824271
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 1824269
    sget-object v0, Lcom/facebook/feed/rows/sections/text/ContentTextComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
