.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedBasicRootPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "LX/Bpg;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1T5;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1T5",
            "<",
            "LX/Bpg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 3
    .param p1    # LX/0Ot;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<+",
            "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
            "<",
            "LX/5kD;",
            "Ljava/lang/Void;",
            "LX/Bpg;",
            ">;>;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/unknown/UnknownFeedUnitPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/sections/common/EmptyPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823445
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1823446
    invoke-static {}, LX/1T5;->a()LX/1T5;

    move-result-object v0

    const-class v1, LX/5kD;

    invoke-virtual {v0, v1, p1}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;

    invoke-virtual {v0, v1, p2}, LX/1T5;->b(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Lcom/facebook/api/feed/data/EndOfFeedSentinel$EndOfFeedSentinelFeedUnit;

    .line 1823447
    move-object v2, p4

    .line 1823448
    invoke-virtual {v0, v1, v2}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    const-class v1, Ljava/lang/Object;

    invoke-virtual {v0, v1, p3}, LX/1T5;->a(Ljava/lang/Class;LX/0Ot;)LX/1T5;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedBasicRootPartDefinition;->a:LX/1T5;

    .line 1823449
    return-void
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1823442
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedBasicRootPartDefinition;->a:LX/1T5;

    invoke-virtual {v0, p1, p2}, LX/1T5;->a(LX/1RF;Ljava/lang/Object;)Z

    .line 1823443
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1823444
    const/4 v0, 0x1

    return v0
.end method
