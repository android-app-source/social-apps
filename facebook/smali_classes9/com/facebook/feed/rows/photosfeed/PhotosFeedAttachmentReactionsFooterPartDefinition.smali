.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/Bp2;",
        ":",
        "LX/Bp4;",
        ":",
        "LX/Bp6;",
        "V:",
        "Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "LX/5kD;",
        "LX/BpS;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

.field private static r:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

.field private final c:LX/0wW;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0iA;

.field private final f:LX/20m;

.field private final g:LX/20w;

.field private final h:LX/20K;

.field private final i:LX/1zf;

.field public final j:LX/20l;

.field private final k:LX/0tH;

.field public final l:Landroid/content/Context;

.field private final m:LX/03V;

.field public final n:LX/1Kf;

.field public final o:LX/20r;

.field public final p:LX/20s;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1823118
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->UFI_CLICKED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;LX/0wW;LX/0Ot;LX/0iA;LX/20m;LX/20w;LX/20K;LX/1zf;LX/20l;LX/0tH;Landroid/content/Context;LX/03V;LX/1Kf;LX/20r;LX/20s;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;",
            "LX/0wW;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/sounds/FBSoundUtil;",
            ">;",
            "LX/0iA;",
            "LX/20m;",
            "LX/20w;",
            "LX/20K;",
            "LX/1zf;",
            "LX/20l;",
            "LX/0tH;",
            "Landroid/content/Context;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1Kf;",
            "LX/20r;",
            "LX/20s;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823100
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1823101
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    .line 1823102
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->c:LX/0wW;

    .line 1823103
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->d:LX/0Ot;

    .line 1823104
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->e:LX/0iA;

    .line 1823105
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->f:LX/20m;

    .line 1823106
    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->g:LX/20w;

    .line 1823107
    iput-object p7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->h:LX/20K;

    .line 1823108
    iput-object p8, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->i:LX/1zf;

    .line 1823109
    iput-object p9, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->j:LX/20l;

    .line 1823110
    iput-object p10, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->k:LX/0tH;

    .line 1823111
    iput-object p11, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->l:Landroid/content/Context;

    .line 1823112
    iput-object p12, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->m:LX/03V;

    .line 1823113
    iput-object p13, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->n:LX/1Kf;

    .line 1823114
    iput-object p14, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->o:LX/20r;

    .line 1823115
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->p:LX/20s;

    .line 1823116
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->q:LX/0Ot;

    .line 1823117
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;
    .locals 3

    .prologue
    .line 1823092
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    monitor-enter v1

    .line 1823093
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1823094
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823095
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1823096
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->b(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1823097
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1823098
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1823099
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/BpS;Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BpS;",
            "TV;)V"
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 1823087
    iget-object v0, p1, LX/BpS;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    if-ne v0, v9, :cond_0

    move v0, v9

    :goto_0
    invoke-virtual {p2, v0}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->setIsLiked(Z)V

    .line 1823088
    iget-object v1, p1, LX/BpS;->c:Lcom/facebook/graphql/model/GraphQLFeedback;

    iget-object v2, p1, LX/BpS;->a:LX/21M;

    iget-object v3, p1, LX/BpS;->b:LX/0wd;

    iget-object v4, p1, LX/BpS;->d:LX/20z;

    iget-object v5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->i:LX/1zf;

    sget-object v6, LX/20I;->LIGHT:LX/20I;

    iget-object v7, p1, LX/BpS;->e:LX/0Px;

    const/4 v8, 0x0

    move-object v0, p2

    invoke-static/range {v0 .. v8}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;Lcom/facebook/graphql/model/GraphQLFeedback;LX/21M;LX/0wd;LX/20z;LX/1zf;LX/20I;LX/0Px;LX/21H;)V

    .line 1823089
    sget-object v0, LX/20H;->DEFAULT:LX/20H;

    invoke-virtual {p2, v0, v9}, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->a(LX/20H;Z)V

    .line 1823090
    return-void

    .line 1823091
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;Landroid/view/View;Lcom/facebook/graphql/model/GraphQLFeedback;LX/1Po;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            "TE;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 1823080
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLFeedback;->W()I

    move-result v0

    .line 1823081
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->i:LX/1zf;

    invoke-virtual {v0}, LX/1zf;->e()LX/1zt;

    move-result-object v9

    .line 1823082
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->d:LX/0Ot;

    invoke-static {v0, v9}, LX/21N;->a(LX/0Ot;LX/1zt;)V

    .line 1823083
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->e:LX/0iA;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->j:LX/20l;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->k:LX/0tH;

    sget-object v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->f:LX/20m;

    const-string v5, "photos_feed"

    move-object v7, p2

    move-object v8, p1

    invoke-static/range {v0 .. v9}, LX/21N;->a(LX/0iA;LX/20l;LX/0tH;Lcom/facebook/interstitial/manager/InterstitialTrigger;LX/20m;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLFeedback;Landroid/view/View;LX/1zt;)Z

    .line 1823084
    check-cast p3, LX/Bp6;

    invoke-interface {p3, p2, v9, v6}, LX/Bp6;->a(Lcom/facebook/graphql/model/GraphQLFeedback;LX/1zt;LX/0Ve;)V

    .line 1823085
    return-void

    .line 1823086
    :cond_0
    sget-object v9, LX/1zt;->c:LX/1zt;

    goto :goto_0
.end method

.method private static b(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;
    .locals 19

    .prologue
    .line 1823119
    new-instance v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    invoke-static/range {p0 .. p0}, LX/0wW;->a(LX/0QB;)LX/0wW;

    move-result-object v4

    check-cast v4, LX/0wW;

    const/16 v5, 0x3567

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static/range {p0 .. p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v6

    check-cast v6, LX/0iA;

    invoke-static/range {p0 .. p0}, LX/20m;->a(LX/0QB;)LX/20m;

    move-result-object v7

    check-cast v7, LX/20m;

    const-class v8, LX/20w;

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/20w;

    invoke-static/range {p0 .. p0}, LX/20K;->a(LX/0QB;)LX/20K;

    move-result-object v9

    check-cast v9, LX/20K;

    invoke-static/range {p0 .. p0}, LX/1zf;->a(LX/0QB;)LX/1zf;

    move-result-object v10

    check-cast v10, LX/1zf;

    invoke-static/range {p0 .. p0}, LX/20l;->a(LX/0QB;)LX/20l;

    move-result-object v11

    check-cast v11, LX/20l;

    invoke-static/range {p0 .. p0}, LX/0tH;->a(LX/0QB;)LX/0tH;

    move-result-object v12

    check-cast v12, LX/0tH;

    const-class v13, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v13}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-static/range {p0 .. p0}, LX/1Ke;->a(LX/0QB;)LX/1Ke;

    move-result-object v15

    check-cast v15, LX/1Kf;

    invoke-static/range {p0 .. p0}, LX/20r;->a(LX/0QB;)LX/20r;

    move-result-object v16

    check-cast v16, LX/20r;

    invoke-static/range {p0 .. p0}, LX/20s;->a(LX/0QB;)LX/20s;

    move-result-object v17

    check-cast v17, LX/20s;

    const/16 v18, 0xbc6

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v18

    invoke-direct/range {v2 .. v18}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;LX/0wW;LX/0Ot;LX/0iA;LX/20m;LX/20w;LX/20K;LX/1zf;LX/20l;LX/0tH;Landroid/content/Context;LX/03V;LX/1Kf;LX/20r;LX/20s;LX/0Ot;)V

    .line 1823120
    return-object v2
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1823066
    sget-object v0, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;->h:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1823067
    check-cast p2, LX/5kD;

    check-cast p3, LX/1Po;

    .line 1823068
    invoke-interface {p2}, LX/5kD;->G()Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;

    move-result-object v0

    invoke-static {v0}, LX/5k9;->a(Lcom/facebook/photos/data/protocol/PhotosMetadataGraphQLModels$SimpleMediaFeedbackModel;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    .line 1823069
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->b:Lcom/facebook/feedplugins/base/footer/rows/FooterButtonClickListenerPartDefinition;

    new-instance v1, LX/BpP;

    invoke-direct {v1, p0, v3, p2, p3}, LX/BpP;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;Lcom/facebook/graphql/model/GraphQLFeedback;LX/5kD;LX/1Po;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1823070
    new-instance v0, LX/BpQ;

    invoke-direct {v0, p0, p3, v3}, LX/BpQ;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;LX/1Po;Lcom/facebook/graphql/model/GraphQLFeedback;)V

    move-object v1, v0

    .line 1823071
    check-cast p3, LX/1Pr;

    new-instance v0, LX/21E;

    invoke-interface {p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->c:LX/0wW;

    invoke-direct {v0, v2, v4}, LX/21E;-><init>(Ljava/lang/String;LX/0wW;)V

    new-instance v2, LX/4Vj;

    invoke-interface {p2}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, LX/4Vj;-><init>(Ljava/lang/String;)V

    invoke-interface {p3, v0, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0wd;

    .line 1823072
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->g:LX/20w;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v5

    const-string v6, "photos_feed"

    invoke-virtual {v0, v4, v5, v6}, LX/20w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/20z;

    move-result-object v4

    .line 1823073
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->i:LX/1zf;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLFeedback;->M()LX/0Px;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/1zf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v5

    .line 1823074
    new-instance v0, LX/BpS;

    invoke-direct/range {v0 .. v5}, LX/BpS;-><init>(LX/21M;LX/0wd;Lcom/facebook/graphql/model/GraphQLFeedback;LX/20z;LX/0Px;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x134345ba

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1823075
    check-cast p2, LX/BpS;

    check-cast p4, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    invoke-direct {p0, p2, p4}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->a(LX/BpS;Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;)V

    const/16 v1, 0x1f

    const v2, 0x246d616d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1823076
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1823077
    check-cast p4, Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;

    .line 1823078
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentReactionsFooterPartDefinition;->h:LX/20K;

    invoke-static {p4, v0}, LX/21N;->a(Lcom/facebook/feedplugins/feedbackreactions/ui/ReactionsFooterView;LX/20K;)V

    .line 1823079
    return-void
.end method
