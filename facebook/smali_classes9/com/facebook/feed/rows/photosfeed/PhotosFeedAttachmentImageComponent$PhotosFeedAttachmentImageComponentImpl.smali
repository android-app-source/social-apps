.class public final Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/BpJ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/5kD;

.field public b:LX/Bpg;

.field public c:Lcom/facebook/common/callercontext/CallerContext;

.field public d:Ljava/lang/Integer;

.field public e:LX/1bf;

.field public final synthetic f:LX/BpJ;


# direct methods
.method public constructor <init>(LX/BpJ;)V
    .locals 1

    .prologue
    .line 1822833
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->f:LX/BpJ;

    .line 1822834
    move-object v0, p1

    .line 1822835
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 1822836
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1822815
    const-string v0, "PhotosFeedAttachmentImageComponent"

    return-object v0
.end method

.method public final a(LX/1X1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1",
            "<",
            "LX/BpJ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1822837
    check-cast p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    .line 1822838
    iget-object v0, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->d:Ljava/lang/Integer;

    .line 1822839
    iget-object v0, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->e:LX/1bf;

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->e:LX/1bf;

    .line 1822840
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1822816
    if-ne p0, p1, :cond_1

    .line 1822817
    :cond_0
    :goto_0
    return v0

    .line 1822818
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 1822819
    goto :goto_0

    .line 1822820
    :cond_3
    check-cast p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    .line 1822821
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 1822822
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 1822823
    if-eq v2, v3, :cond_0

    .line 1822824
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->a:LX/5kD;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->a:LX/5kD;

    iget-object v3, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->a:LX/5kD;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 1822825
    goto :goto_0

    .line 1822826
    :cond_5
    iget-object v2, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->a:LX/5kD;

    if-nez v2, :cond_4

    .line 1822827
    :cond_6
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->b:LX/Bpg;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->b:LX/Bpg;

    iget-object v3, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->b:LX/Bpg;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 1822828
    goto :goto_0

    .line 1822829
    :cond_8
    iget-object v2, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->b:LX/Bpg;

    if-nez v2, :cond_7

    .line 1822830
    :cond_9
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v3}, Lcom/facebook/common/callercontext/CallerContext;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 1822831
    goto :goto_0

    .line 1822832
    :cond_a
    iget-object v2, p1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final g()LX/1X1;
    .locals 2

    .prologue
    .line 1822810
    const/4 v1, 0x0

    .line 1822811
    invoke-super {p0}, LX/1X1;->g()LX/1X1;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    .line 1822812
    iput-object v1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->d:Ljava/lang/Integer;

    .line 1822813
    iput-object v1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->e:LX/1bf;

    .line 1822814
    return-object v0
.end method
