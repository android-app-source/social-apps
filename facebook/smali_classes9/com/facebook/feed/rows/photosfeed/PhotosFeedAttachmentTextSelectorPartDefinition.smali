.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pm;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/5kD;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextComponentPartDefinition;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1823229
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1823230
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;->a:LX/0Ot;

    .line 1823231
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;->b:LX/0Ot;

    .line 1823232
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;
    .locals 5

    .prologue
    .line 1823234
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;

    monitor-enter v1

    .line 1823235
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1823236
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1823237
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1823238
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1823239
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;

    const/16 v4, 0x1ce9

    invoke-static {v0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x6d4

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, v4, p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;-><init>(LX/0Ot;LX/0Ot;)V

    .line 1823240
    move-object v0, v3

    .line 1823241
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1823242
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1823243
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1823244
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1823245
    check-cast p2, LX/5kD;

    .line 1823246
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;->b:LX/0Ot;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;LX/0Ot;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentTextSelectorPartDefinition;->a:LX/0Ot;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(LX/0Ot;Ljava/lang/Object;)LX/1RG;

    .line 1823247
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1823233
    const/4 v0, 0x1

    return v0
.end method
