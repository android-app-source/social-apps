.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;
.super Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;
.source ""

# interfaces
.implements LX/0fh;


# static fields
.field public static final E:Lcom/facebook/common/callercontext/CallerContext;

.field private static final F:LX/Bq0;


# instance fields
.field public A:LX/23N;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/Bq9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/1B1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0bH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/0g8;

.field private H:LX/62C;

.field public I:LX/1Qq;

.field public J:LX/1Qq;

.field public K:LX/195;

.field private L:LX/1Cq;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cq",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public M:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private N:LX/9Ea;

.field private O:LX/Bpl;

.field private P:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

.field public Q:LX/9eC;

.field private R:LX/1Jg;

.field private S:LX/2jf;

.field private T:J

.field public U:Z

.field public V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public W:LX/BpG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public X:LX/BqD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Y:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "LX/5kD;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public Z:LX/BqC;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/BqC",
            "<",
            "LX/5kD;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:LX/1Db;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aa:LX/Bpy;

.field public b:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/3H7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/189;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/1DS;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Bp9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Bph;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/Bpm;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/99w;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/9Eb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Bp1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9iC;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/1ev;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Or;
    .annotation runtime Lcom/facebook/feed/rows/photosfeed/CustomSnowFlakeBackgroundColor;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/1DL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/1QH;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/3E1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public u:LX/0SG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public v:LX/23g;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public w:LX/23T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public x:LX/BqF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public y:LX/Bq4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/1rq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1823890
    const-class v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    const-string v1, "photos_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->E:Lcom/facebook/common/callercontext/CallerContext;

    .line 1823891
    new-instance v0, LX/Bq0;

    invoke-direct {v0}, LX/Bq0;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->F:LX/Bq0;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1823892
    invoke-direct {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;-><init>()V

    .line 1823893
    new-instance v0, LX/1Cq;

    invoke-direct {v0}, LX/1Cq;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->L:LX/1Cq;

    .line 1823894
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)LX/5kD;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1823895
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v0}, LX/BqF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    .line 1823896
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 1823897
    invoke-interface {v0}, LX/2kM;->c()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 1823898
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    .line 1823899
    iget-object v1, v0, LX/2kW;->o:LX/2kM;

    move-object v0, v1

    .line 1823900
    invoke-interface {v0, p1}, LX/2kM;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1823901
    :goto_0
    return-object v0

    .line 1823902
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 1823903
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    .line 1823904
    iget-object v1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->f:LX/BpG;

    invoke-virtual {v1, p1}, LX/BpG;->b(I)LX/5kD;

    move-result-object v1

    move-object v0, v1

    .line 1823905
    goto :goto_0

    .line 1823906
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;LX/1Db;LX/193;LX/3H7;LX/1Ck;LX/189;LX/1DS;LX/Bp9;LX/Bph;LX/Bpm;LX/99w;LX/9Eb;LX/Bp1;LX/0Ot;LX/1ev;LX/0Or;Landroid/content/res/Resources;LX/1DL;LX/1QH;LX/03V;LX/3E1;LX/0SG;LX/23g;LX/23T;LX/BqF;LX/Bq4;LX/1rq;LX/23N;LX/Bq9;LX/1B1;LX/0bH;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;",
            "LX/1Db;",
            "LX/193;",
            "LX/3H7;",
            "LX/1Ck;",
            "LX/189;",
            "LX/1DS;",
            "Lcom/facebook/feed/rows/photosfeed/PhotosFeedPartDefinitionResolver;",
            "LX/Bph;",
            "LX/Bpm;",
            "LX/99w;",
            "LX/9Eb;",
            "LX/Bp1;",
            "LX/0Ot",
            "<",
            "LX/9iC;",
            ">;",
            "LX/1ev;",
            "LX/0Or",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/content/res/Resources;",
            "LX/1DL;",
            "LX/1QH;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/3E1;",
            "LX/0SG;",
            "LX/23g;",
            "LX/23T;",
            "LX/BqF;",
            "LX/Bq4;",
            "LX/1rq;",
            "LX/23N;",
            "LX/Bq9;",
            "LX/1B1;",
            "LX/0bH;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1823907
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a:LX/1Db;

    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->b:LX/193;

    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->c:LX/3H7;

    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->d:LX/1Ck;

    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->e:LX/189;

    iput-object p6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->f:LX/1DS;

    iput-object p7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->g:LX/Bp9;

    iput-object p8, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->h:LX/Bph;

    iput-object p9, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->i:LX/Bpm;

    iput-object p10, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->j:LX/99w;

    iput-object p11, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->k:LX/9Eb;

    iput-object p12, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->l:LX/Bp1;

    iput-object p13, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->m:LX/0Ot;

    iput-object p14, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->n:LX/1ev;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->o:LX/0Or;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->p:Landroid/content/res/Resources;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->q:LX/1DL;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->r:LX/1QH;

    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->s:LX/03V;

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->t:LX/3E1;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->u:LX/0SG;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->v:LX/23g;

    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->w:LX/23T;

    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->y:LX/Bq4;

    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->z:LX/1rq;

    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->A:LX/23N;

    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->B:LX/Bq9;

    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->C:LX/1B1;

    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->D:LX/0bH;

    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 33

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v32

    move-object/from16 v2, p0

    check-cast v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    invoke-static/range {v32 .. v32}, LX/1Db;->a(LX/0QB;)LX/1Db;

    move-result-object v3

    check-cast v3, LX/1Db;

    const-class v4, LX/193;

    move-object/from16 v0, v32

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/193;

    invoke-static/range {v32 .. v32}, LX/3H7;->a(LX/0QB;)LX/3H7;

    move-result-object v5

    check-cast v5, LX/3H7;

    invoke-static/range {v32 .. v32}, LX/1Ck;->a(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static/range {v32 .. v32}, LX/189;->a(LX/0QB;)LX/189;

    move-result-object v7

    check-cast v7, LX/189;

    invoke-static/range {v32 .. v32}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v8

    check-cast v8, LX/1DS;

    invoke-static/range {v32 .. v32}, LX/Bp9;->a(LX/0QB;)LX/Bp9;

    move-result-object v9

    check-cast v9, LX/Bp9;

    const-class v10, LX/Bph;

    move-object/from16 v0, v32

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/Bph;

    const-class v11, LX/Bpm;

    move-object/from16 v0, v32

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Bpm;

    invoke-static/range {v32 .. v32}, LX/99w;->a(LX/0QB;)LX/99w;

    move-result-object v12

    check-cast v12, LX/99w;

    const-class v13, LX/9Eb;

    move-object/from16 v0, v32

    invoke-interface {v0, v13}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v13

    check-cast v13, LX/9Eb;

    const-class v14, LX/Bp1;

    move-object/from16 v0, v32

    invoke-interface {v0, v14}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v14

    check-cast v14, LX/Bp1;

    const/16 v15, 0x2e85

    move-object/from16 v0, v32

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    invoke-static/range {v32 .. v32}, LX/1ev;->a(LX/0QB;)LX/1ev;

    move-result-object v16

    check-cast v16, LX/1ev;

    const/16 v17, 0x15cf

    move-object/from16 v0, v32

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v17

    invoke-static/range {v32 .. v32}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v18

    check-cast v18, Landroid/content/res/Resources;

    invoke-static/range {v32 .. v32}, LX/1DL;->a(LX/0QB;)LX/1DL;

    move-result-object v19

    check-cast v19, LX/1DL;

    const-class v20, LX/1QH;

    move-object/from16 v0, v32

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v20

    check-cast v20, LX/1QH;

    invoke-static/range {v32 .. v32}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v21

    check-cast v21, LX/03V;

    invoke-static/range {v32 .. v32}, LX/3E1;->a(LX/0QB;)LX/3E1;

    move-result-object v22

    check-cast v22, LX/3E1;

    invoke-static/range {v32 .. v32}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v23

    check-cast v23, LX/0SG;

    invoke-static/range {v32 .. v32}, LX/23g;->a(LX/0QB;)LX/23g;

    move-result-object v24

    check-cast v24, LX/23g;

    invoke-static/range {v32 .. v32}, LX/23T;->a(LX/0QB;)LX/23T;

    move-result-object v25

    check-cast v25, LX/23T;

    invoke-static/range {v32 .. v32}, LX/BqF;->a(LX/0QB;)LX/BqF;

    move-result-object v26

    check-cast v26, LX/BqF;

    const-class v27, LX/Bq4;

    move-object/from16 v0, v32

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v27

    check-cast v27, LX/Bq4;

    const-class v28, LX/1rq;

    move-object/from16 v0, v32

    move-object/from16 v1, v28

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v28

    check-cast v28, LX/1rq;

    invoke-static/range {v32 .. v32}, LX/23N;->a(LX/0QB;)LX/23N;

    move-result-object v29

    check-cast v29, LX/23N;

    invoke-static/range {v32 .. v32}, LX/Bq9;->a(LX/0QB;)LX/Bq9;

    move-result-object v30

    check-cast v30, LX/Bq9;

    invoke-static/range {v32 .. v32}, LX/1B1;->a(LX/0QB;)LX/1B1;

    move-result-object v31

    check-cast v31, LX/1B1;

    invoke-static/range {v32 .. v32}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v32

    check-cast v32, LX/0bH;

    invoke-static/range {v2 .. v32}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;LX/1Db;LX/193;LX/3H7;LX/1Ck;LX/189;LX/1DS;LX/Bp9;LX/Bph;LX/Bpm;LX/99w;LX/9Eb;LX/Bp1;LX/0Ot;LX/1ev;LX/0Or;Landroid/content/res/Resources;LX/1DL;LX/1QH;LX/03V;LX/3E1;LX/0SG;LX/23g;LX/23T;LX/BqF;LX/Bq4;LX/1rq;LX/23N;LX/Bq9;LX/1B1;LX/0bH;)V

    return-void
.end method

.method public static a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Lcom/facebook/graphql/model/GraphQLMedia;)I
    .locals 2

    .prologue
    .line 1823908
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->n:LX/1ev;

    invoke-virtual {v0, p1}, LX/1ev;->c(Lcom/facebook/graphql/model/GraphQLMedia;)LX/1f6;

    move-result-object v0

    .line 1823909
    iget v1, v0, LX/1f6;->h:I

    move v0, v1

    .line 1823910
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v1}, LX/0g8;->d()I

    move-result v1

    sub-int v0, v1, v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public static a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;III)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 1823911
    new-instance v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$12;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;III)V

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 7

    .prologue
    .line 1823912
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823913
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->O:LX/Bpl;

    invoke-virtual {v0, p1}, LX/Bpl;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 1823914
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->j:LX/99w;

    .line 1823915
    iget-boolean v1, v0, LX/99w;->c:Z

    move v0, v1

    .line 1823916
    if-nez v0, :cond_0

    .line 1823917
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->j:LX/99w;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->O:LX/Bpl;

    const/4 v5, 0x0

    .line 1823918
    const/4 v2, 0x3

    new-array v2, v2, [LX/0b2;

    new-instance v3, LX/Bpi;

    invoke-direct {v3, v1}, LX/Bpi;-><init>(LX/Bpl;)V

    aput-object v3, v2, v5

    const/4 v3, 0x1

    new-instance v4, LX/Bpk;

    invoke-direct {v4, v1}, LX/Bpk;-><init>(LX/Bpl;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, LX/Bpj;

    invoke-direct {v4, v1}, LX/Bpj;-><init>(LX/Bpl;)V

    aput-object v4, v2, v3

    .line 1823919
    move-object v1, v2

    .line 1823920
    invoke-virtual {v0, v1}, LX/99w;->a([LX/0b2;)V

    .line 1823921
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->L:LX/1Cq;

    .line 1823922
    iput-object p1, v0, LX/1Cq;->a:Ljava/lang/Object;

    .line 1823923
    invoke-static {p1}, LX/16z;->f(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 1823924
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->N:LX/9Ea;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, LX/9Ea;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1823925
    invoke-static {p1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 1823926
    if-nez v0, :cond_4

    .line 1823927
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->B:LX/Bq9;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823928
    iput-object v1, v0, LX/Bq9;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823929
    invoke-virtual {p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->l()V

    .line 1823930
    return-void

    .line 1823931
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v0, p1}, Lcom/facebook/feed/rows/core/props/FeedProps;->b(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    goto :goto_0

    .line 1823932
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 1823933
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1823934
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_6

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1823935
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    if-nez v6, :cond_5

    .line 1823936
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->s:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v6

    const-string p1, "attachment with null media received, skipping attachment"

    invoke-virtual {v1, v6, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1823937
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 1823938
    :cond_5
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v6

    invoke-static {v6}, LX/4XB;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/4XB;

    move-result-object v6

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 1823939
    iput-object v1, v6, LX/4XB;->aK:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1823940
    move-object v1, v6

    .line 1823941
    invoke-virtual {v1}, LX/4XB;->a()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v1

    invoke-static {v1}, LX/5k9;->a(Lcom/facebook/graphql/model/GraphQLMedia;)LX/5kD;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_4

    .line 1823942
    :cond_6
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v1}, LX/BqF;->a()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-eqz v1, :cond_1

    .line 1823943
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->a(LX/0Px;)V

    goto :goto_2
.end method

.method public static a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Z)V
    .locals 2

    .prologue
    .line 1823944
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v0}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$13;

    invoke-direct {v1, p0, p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$13;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Z)V

    invoke-static {v0, v1}, LX/8He;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1823945
    return-void
.end method

.method public static b(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1823946
    invoke-static {p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->q(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)LX/BpF;

    move-result-object v1

    invoke-interface {v1}, LX/BpF;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5kD;

    .line 1823947
    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-interface {v0}, LX/5kD;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1823948
    :goto_1
    return v0

    .line 1823949
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 1823950
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1823951
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 1823952
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v0, :cond_0

    .line 1823953
    const-string v0, "story_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1823954
    const-string v1, "story_cache_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1823955
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->c:LX/3H7;

    sget-object v4, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    sget-object v6, LX/21y;->DEFAULT_ORDER:LX/21y;

    const/4 v7, 0x0

    move-object v3, v0

    move-object v5, v1

    invoke-virtual/range {v2 .. v7}, LX/3H7;->a(Ljava/lang/String;LX/5Go;Ljava/lang/String;LX/21y;Z)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 1823956
    if-nez v2, :cond_1

    .line 1823957
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->d:LX/1Ck;

    sget-object v3, LX/Bq1;->FETCH_STORY:LX/Bq1;

    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->c:LX/3H7;

    sget-object v5, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    sget-object v6, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    invoke-virtual {v4, v0, v5, v6}, LX/3H7;->a(Ljava/lang/String;LX/5Go;LX/0rS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/Bpv;

    invoke-direct {v5, p0}, LX/Bpv;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    invoke-virtual {v2, v3, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1823958
    :goto_0
    return-void

    .line 1823959
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823960
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1823961
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p0, v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 1823962
    :cond_1
    invoke-static {p0, v2}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0
.end method

.method public static d(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)Landroid/view/View;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1823963
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-static {p0, p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->f$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)I

    move-result v2

    invoke-interface {v0, v2}, LX/0g8;->c(I)Landroid/view/View;

    move-result-object v0

    .line 1823964
    instance-of v2, v0, LX/1a5;

    if-eqz v2, :cond_0

    .line 1823965
    check-cast v0, LX/1a5;

    invoke-virtual {v0}, LX/1a5;->getWrappedView()Landroid/view/View;

    move-result-object v0

    .line 1823966
    :cond_0
    move-object v0, v0

    .line 1823967
    instance-of v2, v0, LX/24e;

    if-eqz v2, :cond_2

    .line 1823968
    check-cast v0, LX/24e;

    invoke-interface {v0}, LX/24e;->getUnderlyingDraweeView()Lcom/facebook/drawee/view/GenericDraweeView;

    move-result-object v1

    .line 1823969
    :cond_1
    :goto_0
    return-object v1

    .line 1823970
    :cond_2
    instance-of v2, v0, LX/3JH;

    if-eqz v2, :cond_3

    .line 1823971
    check-cast v0, LX/3JH;

    .line 1823972
    iget-object v1, v0, LX/3JH;->a:Lcom/facebook/video/player/InlineVideoPlayer2;

    move-object v0, v1

    .line 1823973
    invoke-virtual {v0}, Lcom/facebook/video/player/InlineVideoPlayer2;->getCoverImage()Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-result-object v1

    goto :goto_0

    .line 1823974
    :cond_3
    instance-of v2, v0, LX/7gM;

    if-eqz v2, :cond_4

    .line 1823975
    check-cast v0, LX/7gM;

    .line 1823976
    iget-object v1, v0, LX/7gM;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    move-object v1, v1

    .line 1823977
    goto :goto_0

    .line 1823978
    :cond_4
    instance-of v2, v0, LX/1cn;

    if-eqz v2, :cond_5

    .line 1823979
    const v1, 0x7f0d24e0

    invoke-static {v0, v1}, LX/5JR;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 1823980
    :cond_5
    if-eqz v0, :cond_1

    .line 1823981
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->s:LX/03V;

    const-string v3, "PhotosFeedFragment_incorrectPhotoAttachmentView"

    const-string v4, "Looking for startingPhotoIndex of %s, headerAdapter has size of %s, feedAdapter has size of %s, feedAdapter.translateFeedEdgeIndexToFirstRow(photoIndex) is %s,View is %s, GraphQLStory Id is %s"

    const/4 v5, 0x6

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->I:LX/1Qq;

    invoke-interface {v7}, LX/1Qq;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->J:LX/1Qq;

    invoke-interface {v7}, LX/1Qq;->getCount()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    iget-object v7, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->J:LX/1Qq;

    invoke-interface {v7, p1}, LX/1Qr;->m_(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    aput-object v0, v5, v6

    const/4 v6, 0x5

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1823982
    iget-object v7, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v7

    .line 1823983
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v5, v6

    invoke-static {v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_6
    move-object v0, v1

    goto :goto_1
.end method

.method public static f$redex0(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)I
    .locals 2

    .prologue
    .line 1823984
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->I:LX/1Qq;

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->J:LX/1Qq;

    invoke-interface {v1, p1}, LX/1Qr;->m_(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public static q(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)LX/BpF;
    .locals 1

    .prologue
    .line 1823985
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v0}, LX/BqF;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1823986
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->X:LX/BqD;

    .line 1823987
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->W:LX/BpG;

    goto :goto_0
.end method

.method public static v(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V
    .locals 5

    .prologue
    .line 1823988
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-nez v0, :cond_0

    .line 1823989
    :goto_0
    return-void

    .line 1823990
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1823991
    const-string v1, "media_fetcher_rule"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    .line 1823992
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v1, v1

    .line 1823993
    const-string v2, "starting_media_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    .line 1823994
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    const/16 p0, 0x14

    .line 1823995
    iget-object v3, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    if-eqz v3, :cond_2

    .line 1823996
    :cond_1
    :goto_1
    goto :goto_0

    .line 1823997
    :cond_2
    iget-object v3, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->g:LX/23T;

    sget-object v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v0, v4}, LX/23T;->a(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9g2;

    move-result-object v3

    iput-object v3, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    .line 1823998
    iget-object v3, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    iget-object v4, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->e:LX/Bq3;

    invoke-virtual {v3, v4}, LX/9g2;->a(LX/9g5;)V

    .line 1823999
    iget-object v3, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    invoke-virtual {v3}, LX/9g2;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1824000
    iget-object v3, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    invoke-virtual {v3, p0, v1}, LX/9g2;->a(ILX/0am;)V

    .line 1824001
    iput p0, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->d:I

    goto :goto_1
.end method

.method public static x(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 1824002
    new-instance v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$10;

    invoke-direct {v0, p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$10;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)LX/9hP;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1823885
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1823886
    const-string v1, "starting_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1823887
    invoke-static {p0, v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->d(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)Landroid/view/View;

    move-result-object v0

    .line 1823888
    invoke-static {v0}, LX/BaD;->a(Landroid/view/View;)LX/9hP;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1823889
    const-string v0, "photos_feed"

    return-object v0
.end method

.method public final a(LX/1L1;ZI)V
    .locals 0

    .prologue
    .line 1823702
    iput-boolean p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->U:Z

    .line 1823703
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 12
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x1

    const/4 v11, 0x0

    const/4 v2, 0x0

    .line 1823704
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->a(Landroid/os/Bundle;)V

    .line 1823705
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1823706
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->b:LX/193;

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const-string v3, "photos_feed_scroll_perf"

    invoke-virtual {v0, v1, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->K:LX/195;

    .line 1823707
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v0}, LX/BqF;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1823708
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v0}, LX/BqF;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1823709
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->k:LX/9Eb;

    new-instance v1, LX/Bpq;

    invoke-direct {v1, p0}, LX/Bpq;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, LX/9Eb;->a(LX/0QK;LX/9CC;LX/9D1;LX/9Do;LX/9Du;LX/9EL;)LX/9Ea;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->N:LX/9Ea;

    .line 1823710
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->i:LX/Bpm;

    new-instance v1, LX/Bpr;

    invoke-direct {v1, p0}, LX/Bpr;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    .line 1823711
    new-instance v4, LX/Bpl;

    const-class v2, LX/3iG;

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v2

    check-cast v2, LX/3iG;

    invoke-static {v0}, LX/20h;->a(LX/0QB;)LX/20h;

    move-result-object v3

    check-cast v3, LX/20h;

    invoke-direct {v4, v1, v2, v3}, LX/Bpl;-><init>(LX/0QK;LX/3iG;LX/20h;)V

    .line 1823712
    move-object v0, v4

    .line 1823713
    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->O:LX/Bpl;

    .line 1823714
    new-instance v4, LX/Bps;

    invoke-direct {v4, p0}, LX/Bps;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    .line 1823715
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->q:LX/1DL;

    invoke-virtual {v0}, LX/1DL;->a()LX/1Jg;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->R:LX/1Jg;

    .line 1823716
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v10, v0

    .line 1823717
    new-instance v9, LX/Bpt;

    invoke-direct {v9, p0}, LX/Bpt;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    .line 1823718
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->l:LX/Bp1;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->q(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)LX/BpF;

    move-result-object v2

    new-instance v3, LX/Bpz;

    invoke-direct {v3, p0}, LX/Bpz;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    const-string v5, "gallery_fetcher_rule"

    invoke-virtual {v10, v5}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v5

    check-cast v5, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    const-string v6, "target_type"

    invoke-virtual {v10, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string v7, "target_id"

    invoke-virtual {v10, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v0 .. v9}, LX/Bp1;->a(Landroid/content/Context;LX/BpF;LX/9hM;LX/9hN;Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;ILjava/lang/String;ZLjava/util/concurrent/Callable;)LX/Bp0;

    move-result-object v3

    .line 1823719
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->h:LX/Bph;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1823720
    sget-object v2, LX/Bq5;->a:LX/Bq5;

    move-object v2, v2

    .line 1823721
    new-instance v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$5;

    invoke-direct {v4, p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment$5;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    iget-object v5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->r:LX/1QH;

    iget-object v6, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->R:LX/1Jg;

    invoke-virtual {v5, v6}, LX/1QH;->a(LX/1Jg;)LX/1QW;

    move-result-object v6

    move-object v5, v9

    invoke-virtual/range {v0 .. v6}, LX/Bph;->a(Landroid/content/Context;LX/1PT;LX/Bp0;Ljava/lang/Runnable;Ljava/util/concurrent/Callable;LX/1QW;)LX/Bpg;

    move-result-object v0

    .line 1823722
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->f:LX/1DS;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->g:LX/Bp9;

    .line 1823723
    iget-object v3, v2, LX/Bp9;->a:LX/0Ot;

    move-object v2, v3

    .line 1823724
    iget-object v3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->L:LX/1Cq;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 1823725
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 1823726
    move-object v1, v1

    .line 1823727
    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->I:LX/1Qq;

    .line 1823728
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->f:LX/1DS;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->g:LX/Bp9;

    .line 1823729
    new-instance v3, LX/Bp8;

    invoke-direct {v3, v2}, LX/Bp8;-><init>(LX/Bp9;)V

    move-object v2, v3

    .line 1823730
    invoke-static {p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->q(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)LX/BpF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 1823731
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 1823732
    move-object v1, v1

    .line 1823733
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v2}, LX/BqF;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1823734
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v2}, LX/BqF;->a()Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    if-eqz v2, :cond_5

    .line 1823735
    new-instance v2, LX/BqB;

    iget-object v3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    invoke-direct {v2, v3}, LX/BqB;-><init>(LX/2kW;)V

    .line 1823736
    :goto_1
    move-object v2, v2

    .line 1823737
    invoke-virtual {v1, v2}, LX/1Ql;->a(LX/99g;)LX/1Ql;

    .line 1823738
    :cond_0
    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v1

    move-object v0, v1

    .line 1823739
    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->J:LX/1Qq;

    .line 1823740
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->R:LX/1Jg;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->J:LX/1Qq;

    invoke-interface {v1}, LX/1Qr;->e()LX/1R4;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Jg;->a(LX/1R4;)V

    .line 1823741
    new-instance v0, LX/99d;

    const/4 v1, 0x2

    new-array v1, v1, [LX/1Qq;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->I:LX/1Qq;

    aput-object v2, v1, v11

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->J:LX/1Qq;

    aput-object v2, v1, v8

    invoke-direct {v0, v8, v1}, LX/99d;-><init>(Z[LX/1Qq;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->H:LX/62C;

    .line 1823742
    new-instance v0, LX/Bpu;

    invoke-direct {v0, p0}, LX/Bpu;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    move-object v0, v0

    .line 1823743
    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->S:LX/2jf;

    .line 1823744
    if-eqz p1, :cond_1

    .line 1823745
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iC;

    const-string v1, "photos_feed_extra_media"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1, v8}, LX/9iC;->a(Ljava/util/ArrayList;Z)LX/0Px;

    move-result-object v0

    .line 1823746
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v1}, LX/BqF;->a()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-eqz v1, :cond_1

    .line 1823747
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->a(LX/0Px;)V

    .line 1823748
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v0}, LX/BqF;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    if-eqz v0, :cond_7

    .line 1823749
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    const/16 v1, 0x14

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/2kW;->a(ILjava/lang/Object;)V

    .line 1823750
    :goto_2
    invoke-direct {p0, v10}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->b(Landroid/os/Bundle;)V

    .line 1823751
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->A:LX/23N;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->B:LX/Bq9;

    invoke-virtual {v0, v1}, LX/1Kt;->a(LX/1Ce;)V

    .line 1823752
    new-instance v0, LX/Bpy;

    invoke-direct {v0, p0}, LX/Bpy;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->aa:LX/Bpy;

    .line 1823753
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->C:LX/1B1;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->aa:LX/Bpy;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b2;)Z

    .line 1823754
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->C:LX/1B1;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->D:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->a(LX/0b4;)V

    .line 1823755
    return-void

    .line 1823756
    :cond_2
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v0}, LX/BqF;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1823757
    :goto_3
    goto/16 :goto_0

    .line 1823758
    :cond_3
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1823759
    const-string v1, "media_fetcher_rule"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;

    .line 1823760
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->w:LX/23T;

    sget-object v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->E:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v3}, LX/23T;->b(Lcom/facebook/photos/mediafetcher/interfaces/MediaFetcherConstructionRule;Lcom/facebook/common/callercontext/CallerContext;)LX/9gr;

    move-result-object v0

    .line 1823761
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "photosfeed/"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1823762
    iget-object v3, v0, LX/9g8;->a:Lcom/facebook/photos/mediafetcher/interfaces/QueryParam;

    move-object v3, v3

    .line 1823763
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1823764
    iget-object v3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->z:LX/1rq;

    invoke-virtual {v3, v1, v0}, LX/1rq;->a(Ljava/lang/String;LX/1rs;)LX/2jj;

    move-result-object v0

    invoke-virtual {v0}, LX/2jj;->a()LX/2kW;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    .line 1823765
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    invoke-virtual {v0}, LX/2kW;->a()V

    .line 1823766
    new-instance v0, LX/BqD;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    invoke-direct {v0, v1}, LX/BqD;-><init>(LX/2kW;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->X:LX/BqD;

    .line 1823767
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->X:LX/BqD;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    goto/16 :goto_0

    .line 1823768
    :cond_4
    new-instance v0, LX/BpG;

    invoke-direct {v0}, LX/BpG;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->W:LX/BpG;

    .line 1823769
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->y:LX/Bq4;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->W:LX/BpG;

    .line 1823770
    new-instance v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    invoke-static {v0}, LX/23T;->b(LX/0QB;)LX/23T;

    move-result-object v3

    check-cast v3, LX/23T;

    invoke-direct {v4, v1, v3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;-><init>(LX/BpG;LX/23T;)V

    .line 1823771
    move-object v0, v4

    .line 1823772
    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    .line 1823773
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    .line 1823774
    iput-object p0, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    .line 1823775
    goto :goto_3

    .line 1823776
    :cond_5
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-eqz v2, :cond_6

    .line 1823777
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    .line 1823778
    new-instance v3, LX/Bq2;

    invoke-direct {v3, v2, p0}, LX/Bq2;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    move-object v2, v3

    .line 1823779
    goto/16 :goto_1

    .line 1823780
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1823781
    :cond_7
    invoke-static {p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->v(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    goto/16 :goto_2
.end method

.method public final a(LX/9eC;)Z
    .locals 1

    .prologue
    .line 1823782
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Q:LX/9eC;

    .line 1823783
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1823784
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1823785
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->c()V

    .line 1823786
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-eqz v0, :cond_0

    .line 1823787
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c()V

    .line 1823788
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    .line 1823789
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;

    .line 1823790
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    if-eqz v0, :cond_1

    .line 1823791
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->C:LX/1B1;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->D:LX/0bH;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 1823792
    :cond_1
    return-void
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1823793
    sget-object v0, Lcom/facebook/photos/dialog/PhotoAnimationDialogFragment;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final k()LX/9eT;
    .locals 4

    .prologue
    .line 1823794
    new-instance v0, LX/9eT;

    sget-object v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->F:LX/Bq0;

    const-string v2, "OpenPhotosFeed"

    const v3, 0x140012

    invoke-direct {v0, v1, v2, v3}, LX/9eT;-><init>(LX/0Pq;Ljava/lang/String;I)V

    return-object v0
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 1823795
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->H:LX/62C;

    if-eqz v0, :cond_0

    .line 1823796
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->H:LX/62C;

    const v1, 0x49940ca3

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1823797
    :cond_0
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1823798
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1823799
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-eqz v0, :cond_0

    .line 1823800
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    invoke-virtual {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c()V

    .line 1823801
    invoke-static {p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->v(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    .line 1823802
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->I:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 1823803
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->J:LX/1Qq;

    invoke-interface {v0, p1}, LX/1Qr;->a(Landroid/content/res/Configuration;)V

    .line 1823804
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x55c5c083

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1823805
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->v:LX/23g;

    invoke-virtual {v0}, LX/23g;->e()V

    .line 1823806
    const v0, 0x7f0311a8

    const/4 v2, 0x0

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 1823807
    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->p:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {v2, v3}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 1823808
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->v:LX/23g;

    invoke-virtual {v0}, LX/23g;->f()V

    .line 1823809
    const/16 v0, 0x2b

    const v3, -0x7fc21c19

    invoke-static {v5, v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v2
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x16a87267

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1823810
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onDestroy()V

    .line 1823811
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->O:LX/Bpl;

    .line 1823812
    iget-object v2, v1, LX/Bpl;->b:LX/3iK;

    invoke-virtual {v2}, LX/3iK;->a()V

    .line 1823813
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->N:LX/9Ea;

    invoke-virtual {v1}, LX/9Ea;->a()V

    .line 1823814
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->d:LX/1Ck;

    sget-object v2, LX/Bq1;->FETCH_STORY:LX/Bq1;

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 1823815
    invoke-virtual {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->c()V

    .line 1823816
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    if-eqz v1, :cond_1

    .line 1823817
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->X:LX/BqD;

    if-eqz v1, :cond_0

    .line 1823818
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->X:LX/BqD;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 1823819
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->X:LX/BqD;

    invoke-virtual {v1}, LX/Be9;->a()V

    .line 1823820
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    invoke-virtual {v1}, LX/2kW;->c()V

    .line 1823821
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x4e991af7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroyView()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x3dd94d7a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1823822
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onDestroyView()V

    .line 1823823
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->H:LX/62C;

    if-eqz v1, :cond_0

    .line 1823824
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->H:LX/62C;

    invoke-virtual {v1}, LX/62C;->dispose()V

    .line 1823825
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v1}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->P:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-static {v1, v2}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1823826
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Z:LX/BqC;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    if-eqz v1, :cond_1

    .line 1823827
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Y:LX/2kW;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->Z:LX/BqC;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 1823828
    :cond_1
    const/16 v1, 0x2b

    const v2, -0x69e26ec5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onPause()V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x74b97091

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1823829
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onPause()V

    .line 1823830
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->j:LX/99w;

    .line 1823831
    iget-object v2, v0, LX/99w;->a:LX/1B1;

    iget-object v3, v0, LX/99w;->b:LX/0bH;

    invoke-virtual {v2, v3}, LX/1B1;->b(LX/0b4;)V

    .line 1823832
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->R:LX/1Jg;

    invoke-interface {v0}, LX/1Jg;->b()V

    .line 1823833
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->R:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v0, v2}, LX/0g8;->c(LX/0fx;)V

    .line 1823834
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->S:LX/2jf;

    invoke-interface {v0, v2}, LX/0g8;->c(LX/0fx;)V

    .line 1823835
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->u:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->T:J

    sub-long/2addr v2, v4

    .line 1823836
    iget-object v4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->t:LX/3E1;

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->M:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v2, v3, v5}, LX/3E1;->a(LX/0lF;JLjava/lang/String;)V

    .line 1823837
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->A:LX/23N;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-virtual {v0, v2}, LX/1Kt;->c(LX/0g8;)V

    .line 1823838
    const v0, 0x698905e

    invoke-static {v0, v1}, LX/02F;->f(II)V

    return-void

    .line 1823839
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, -0x77078dbc

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1823840
    invoke-super {p0}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onResume()V

    .line 1823841
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->j:LX/99w;

    .line 1823842
    invoke-static {v1}, LX/99w;->d(LX/99w;)V

    .line 1823843
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->R:LX/1Jg;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v1, v2}, LX/1Jg;->a(LX/0g8;)V

    .line 1823844
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->R:LX/1Jg;

    invoke-interface {v2}, LX/1Jg;->a()LX/0fx;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 1823845
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->S:LX/2jf;

    invoke-interface {v1, v2}, LX/0g8;->b(LX/0fx;)V

    .line 1823846
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->t:LX/3E1;

    invoke-virtual {v1}, LX/3E1;->a()V

    .line 1823847
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->u:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->T:J

    .line 1823848
    const/16 v1, 0x2b

    const v2, -0x2c9d7db

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1823849
    invoke-super {p0, p1}, Lcom/facebook/photos/dialog/PhotoAnimationContentFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1823850
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    if-eqz v0, :cond_0

    .line 1823851
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9iC;

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->V:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    invoke-virtual {v2}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->b()LX/0Px;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, LX/9iC;->a(LX/0Px;Z)Ljava/util/ArrayList;

    move-result-object v0

    .line 1823852
    const-string v2, "photos_feed_extra_media"

    invoke-virtual {p1, v2, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1823853
    :cond_0
    const-string v0, "photos_feed_list_visible_index"

    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v2}, LX/0g8;->q()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1823854
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v0, v1}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v0

    .line 1823855
    if-nez v0, :cond_1

    move v0, v1

    .line 1823856
    :goto_0
    const-string v1, "photos_feed_list_offset"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1823857
    return-void

    .line 1823858
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v1}, LX/0g8;->g()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public final onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1823859
    const v0, 0x7f0d2973

    invoke-virtual {p0, v0}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1823860
    new-instance v1, LX/1Oz;

    invoke-direct {v1, v0}, LX/1Oz;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1823861
    new-instance v1, LX/0g6;

    invoke-direct {v1, v0}, LX/0g6;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    move-object v0, v1

    .line 1823862
    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    .line 1823863
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->H:LX/62C;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 1823864
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->a:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 1823865
    if-eqz p2, :cond_0

    .line 1823866
    const-string v0, "photos_feed_list_visible_index"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1823867
    const-string v1, "photos_feed_list_offset"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1823868
    iget-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {p1}, LX/BqF;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    .line 1823869
    new-instance p1, LX/Bpo;

    invoke-direct {p1, p0}, LX/Bpo;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;)V

    move-object p1, p1

    .line 1823870
    :goto_0
    move-object v0, p1

    .line 1823871
    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->P:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 1823872
    :goto_1
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->G:LX/0g8;

    invoke-interface {v0}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->P:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 1823873
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->v:LX/23g;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/23g;->a(Z)V

    .line 1823874
    return-void

    .line 1823875
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1823876
    const-string v1, "starting_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1823877
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->x:LX/BqF;

    invoke-virtual {v1}, LX/BqF;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1823878
    new-instance v1, LX/Bpw;

    invoke-direct {v1, p0, v0}, LX/Bpw;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)V

    move-object v0, v1

    .line 1823879
    :goto_2
    move-object v0, v0

    .line 1823880
    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;->P:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    goto :goto_1

    .line 1823881
    :cond_1
    new-instance p1, LX/Bpp;

    invoke-direct {p1, p0, v0, v1}, LX/Bpp;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;II)V

    move-object p1, p1

    .line 1823882
    goto :goto_0

    .line 1823883
    :cond_2
    new-instance v1, LX/Bpn;

    invoke-direct {v1, p0, v0}, LX/Bpn;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;I)V

    move-object v0, v1

    .line 1823884
    goto :goto_2
.end method
