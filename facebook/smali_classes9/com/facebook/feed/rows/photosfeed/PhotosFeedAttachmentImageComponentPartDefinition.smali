.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "LX/Bpg;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "LX/5kD;",
        "TE;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;

.field private static final e:Lcom/facebook/common/callercontext/CallerContext;

.field private static j:LX/0Xm;


# instance fields
.field private final f:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;

.field private final g:LX/BqF;

.field private final h:LX/BpJ;

.field private final i:LX/1V0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1822924
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->d:LX/1Cz;

    .line 1822925
    const-class v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;

    const-string v1, "photos_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;LX/BqF;LX/BpJ;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1822918
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1822919
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->f:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;

    .line 1822920
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->g:LX/BqF;

    .line 1822921
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->h:LX/BpJ;

    .line 1822922
    iput-object p5, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->i:LX/1V0;

    .line 1822923
    return-void
.end method

.method private a(LX/1De;LX/5kD;LX/Bpg;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/5kD;",
            "TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1822896
    new-instance v0, LX/1X6;

    const/4 v1, 0x0

    invoke-static {}, LX/1UY;->f()LX/1UY;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, LX/1UY;->b(I)LX/1UY;

    move-result-object v2

    invoke-virtual {v2}, LX/1UY;->i()LX/1Ua;

    move-result-object v2

    sget-object v3, LX/1X9;->MIDDLE:LX/1X9;

    invoke-direct {v0, v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;LX/1X9;)V

    .line 1822897
    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->h:LX/BpJ;

    const/4 v2, 0x0

    .line 1822898
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    invoke-direct {v3, v1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;-><init>(LX/BpJ;)V

    .line 1822899
    sget-object v4, LX/BpJ;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BpI;

    .line 1822900
    if-nez v4, :cond_0

    .line 1822901
    new-instance v4, LX/BpI;

    invoke-direct {v4}, LX/BpI;-><init>()V

    .line 1822902
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/BpI;->a$redex0(LX/BpI;LX/1De;IILcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;)V

    .line 1822903
    move-object v3, v4

    .line 1822904
    move-object v2, v3

    .line 1822905
    move-object v1, v2

    .line 1822906
    iget-object v2, v1, LX/BpI;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    iput-object p2, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->a:LX/5kD;

    .line 1822907
    iget-object v2, v1, LX/BpI;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1822908
    move-object v1, v1

    .line 1822909
    iget-object v2, v1, LX/BpI;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    iput-object p3, v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->b:LX/Bpg;

    .line 1822910
    iget-object v2, v1, LX/BpI;->d:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1822911
    move-object v1, v1

    .line 1822912
    sget-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 1822913
    iget-object v3, v1, LX/BpI;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;

    iput-object v2, v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponent$PhotosFeedAttachmentImageComponentImpl;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1822914
    iget-object v3, v1, LX/BpI;->d:Ljava/util/BitSet;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 1822915
    move-object v1, v1

    .line 1822916
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1822917
    iget-object v2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->i:LX/1V0;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;
    .locals 9

    .prologue
    .line 1822885
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;

    monitor-enter v1

    .line 1822886
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1822887
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1822888
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1822889
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1822890
    new-instance v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;

    invoke-static {v0}, LX/BqF;->a(LX/0QB;)LX/BqF;

    move-result-object v6

    check-cast v6, LX/BqF;

    invoke-static {v0}, LX/BpJ;->a(LX/0QB;)LX/BpJ;

    move-result-object v7

    check-cast v7, LX/BpJ;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v8

    check-cast v8, LX/1V0;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;-><init>(Landroid/content/Context;Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;LX/BqF;LX/BpJ;LX/1V0;)V

    .line 1822891
    move-object v0, v3

    .line 1822892
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1822893
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1822894
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1822895
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1822876
    check-cast p2, LX/5kD;

    check-cast p3, LX/Bpg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->a(LX/1De;LX/5kD;LX/Bpg;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1822884
    check-cast p2, LX/5kD;

    check-cast p3, LX/Bpg;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->a(LX/1De;LX/5kD;LX/Bpg;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1822878
    check-cast p1, LX/5kD;

    .line 1822879
    invoke-static {p1}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImagePartDefinition;->a(LX/5kD;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->g:LX/BqF;

    .line 1822880
    iget-object v1, v0, LX/BqF;->i:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1822881
    iget-object v1, v0, LX/BqF;->a:LX/0ad;

    sget-short p0, LX/BqE;->b:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/BqF;->i:Ljava/lang/Boolean;

    .line 1822882
    :cond_0
    iget-object v1, v0, LX/BqF;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 1822883
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/1Cz;
    .locals 1

    .prologue
    .line 1822877
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedAttachmentImageComponentPartDefinition;->d:LX/1Cz;

    return-object v0
.end method
