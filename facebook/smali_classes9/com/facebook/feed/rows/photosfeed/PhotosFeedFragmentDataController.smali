.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/9g2;

.field public d:I

.field public final e:LX/Bq3;

.field public final f:LX/BpG;

.field public final g:LX/23T;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1824032
    const-class v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;

    const-string v1, "photos_feed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/BpG;LX/23T;)V
    .locals 2
    .param p1    # LX/BpG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824019
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1824020
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->g:LX/23T;

    .line 1824021
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->f:LX/BpG;

    .line 1824022
    new-instance v0, LX/Bq3;

    invoke-direct {v0, p0}, LX/Bq3;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->e:LX/Bq3;

    .line 1824023
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1824030
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->e:LX/Bq3;

    invoke-virtual {v0, p1}, LX/Bq3;->a(LX/0Px;)V

    .line 1824031
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/5kD;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1824029
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->f:LX/BpG;

    invoke-virtual {v0}, LX/BpG;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1824024
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    if-eqz v0, :cond_0

    .line 1824025
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->e:LX/Bq3;

    invoke-virtual {v0, v1}, LX/9g2;->b(LX/9g5;)V

    .line 1824026
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    invoke-virtual {v0}, LX/9g2;->c()V

    .line 1824027
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedFragmentDataController;->c:LX/9g2;

    .line 1824028
    :cond_0
    return-void
.end method
