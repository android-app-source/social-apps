.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

.field private final b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedBackgroundPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824235
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1824236
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    .line 1824237
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedBackgroundPartDefinition;

    .line 1824238
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;
    .locals 5

    .prologue
    .line 1824220
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;

    monitor-enter v1

    .line 1824221
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824222
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824223
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824224
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824225
    new-instance p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;-><init>(Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedBackgroundPartDefinition;)V

    .line 1824226
    move-object v0, p0

    .line 1824227
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824228
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824229
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824230
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1824239
    sget-object v0, Lcom/facebook/feedback/reactions/ui/PillsBlingBarView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1824231
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824232
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedBackgroundPartDefinition;

    new-instance v1, LX/Bpe;

    sget-object v2, LX/1Ua;->p:LX/1Ua;

    invoke-direct {v1, p2, v2}, LX/Bpe;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824233
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryPillsBlingBarPartDefinition;->a:Lcom/facebook/feedplugins/pillsblingbar/ui/PillsBlingBarWithoutBackgroundPartDefinition;

    invoke-interface {p1, v0, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824234
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1824219
    const/4 v0, 0x1

    return v0
.end method
