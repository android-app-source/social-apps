.class public Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;

.field private final b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

.field private final c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;

.field private final d:LX/14w;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;LX/14w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824143
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1824144
    iput-object p1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;

    .line 1824145
    iput-object p2, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

    .line 1824146
    iput-object p3, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;

    .line 1824147
    iput-object p4, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->d:LX/14w;

    .line 1824148
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;
    .locals 7

    .prologue
    .line 1824149
    const-class v1, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;

    monitor-enter v1

    .line 1824150
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824151
    sput-object v2, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824152
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824153
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824154
    new-instance p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;

    invoke-static {v0}, LX/14w;->a(LX/0QB;)LX/14w;

    move-result-object v6

    check-cast v6, LX/14w;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;-><init>(Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;LX/14w;)V

    .line 1824155
    move-object v0, p0

    .line 1824156
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824157
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824158
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1824160
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824161
    iget-object v0, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->c:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->b:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryReactionsFooterPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterSelectorPartDefinition;->a:Lcom/facebook/feed/rows/photosfeed/PhotosFeedStoryFooterPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1824162
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1824163
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824164
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1824165
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1824166
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, LX/14w;->f(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
