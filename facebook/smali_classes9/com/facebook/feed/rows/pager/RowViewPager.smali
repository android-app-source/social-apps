.class public Lcom/facebook/feed/rows/pager/RowViewPager;
.super Lcom/facebook/widget/ListViewFriendlyViewPager;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static a:I


# instance fields
.field public b:LX/1DR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1710745
    const/4 v0, 0x2

    sput v0, Lcom/facebook/feed/rows/pager/RowViewPager;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1710742
    invoke-direct {p0, p1}, Lcom/facebook/widget/ListViewFriendlyViewPager;-><init>(Landroid/content/Context;)V

    .line 1710743
    invoke-direct {p0}, Lcom/facebook/feed/rows/pager/RowViewPager;->h()V

    .line 1710744
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1710739
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/ListViewFriendlyViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1710740
    invoke-direct {p0}, Lcom/facebook/feed/rows/pager/RowViewPager;->h()V

    .line 1710741
    return-void
.end method

.method private a(LX/0gG;)V
    .locals 5
    .param p1    # LX/0gG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    .line 1710728
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0gG;->b()I

    move-result v0

    sget v1, Lcom/facebook/feed/rows/pager/RowViewPager;->a:I

    if-ge v0, v1, :cond_1

    .line 1710729
    :cond_0
    :goto_0
    return-void

    .line 1710730
    :cond_1
    iget-object v0, p0, Lcom/facebook/feed/rows/pager/RowViewPager;->b:LX/1DR;

    invoke-virtual {v0}, LX/1DR;->a()I

    move-result v0

    .line 1710731
    iget v1, p0, Landroid/support/v4/view/ViewPager;->q:I

    move v1, v1

    .line 1710732
    int-to-float v1, v1

    int-to-float v2, v0

    div-float/2addr v1, v2

    .line 1710733
    invoke-virtual {p0}, Lcom/facebook/feed/rows/pager/RowViewPager;->getPaddingLeft()I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    .line 1710734
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, LX/0gG;->d(I)F

    move-result v2

    add-float/2addr v2, v1

    .line 1710735
    invoke-virtual {p1, v4}, LX/0gG;->d(I)F

    move-result v3

    add-float/2addr v1, v3

    .line 1710736
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v0, v3, v0

    sub-float/2addr v0, v2

    .line 1710737
    div-float/2addr v0, v1

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    .line 1710738
    iget v1, p0, Lcom/facebook/feed/rows/pager/RowViewPager;->c:I

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/feed/rows/pager/RowViewPager;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/rows/pager/RowViewPager;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v0

    check-cast v0, LX/1DR;

    iput-object v0, p0, Lcom/facebook/feed/rows/pager/RowViewPager;->b:LX/1DR;

    return-void
.end method

.method private h()V
    .locals 1

    .prologue
    .line 1710726
    const-class v0, Lcom/facebook/feed/rows/pager/RowViewPager;

    invoke-static {v0, p0}, Lcom/facebook/feed/rows/pager/RowViewPager;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1710727
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 1710724
    invoke-virtual {p0}, Landroid/support/v4/view/ViewPager;->getAdapter()LX/0gG;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/facebook/feed/rows/pager/RowViewPager;->a(LX/0gG;)V

    .line 1710725
    return-void
.end method


# virtual methods
.method public getHiddenPages()I
    .locals 1

    .prologue
    .line 1710723
    iget v0, p0, Lcom/facebook/feed/rows/pager/RowViewPager;->c:I

    return v0
.end method

.method public final onMeasure(II)V
    .locals 0

    .prologue
    .line 1710720
    invoke-direct {p0}, Lcom/facebook/feed/rows/pager/RowViewPager;->i()V

    .line 1710721
    invoke-super {p0, p1, p2}, Lcom/facebook/widget/ListViewFriendlyViewPager;->onMeasure(II)V

    .line 1710722
    return-void
.end method

.method public setAdapter(LX/0gG;)V
    .locals 0
    .param p1    # LX/0gG;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1710717
    invoke-direct {p0, p1}, Lcom/facebook/feed/rows/pager/RowViewPager;->a(LX/0gG;)V

    .line 1710718
    invoke-super {p0, p1}, Lcom/facebook/widget/ListViewFriendlyViewPager;->setAdapter(LX/0gG;)V

    .line 1710719
    return-void
.end method

.method public setHiddenPages(I)V
    .locals 0

    .prologue
    .line 1710714
    iput p1, p0, Lcom/facebook/feed/rows/pager/RowViewPager;->c:I

    .line 1710715
    invoke-direct {p0}, Lcom/facebook/feed/rows/pager/RowViewPager;->i()V

    .line 1710716
    return-void
.end method
