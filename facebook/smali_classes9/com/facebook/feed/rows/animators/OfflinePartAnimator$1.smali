.class public final Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/C96;

.field public final synthetic b:LX/Boq;


# direct methods
.method public constructor <init>(LX/Boq;LX/C96;)V
    .locals 0

    .prologue
    .line 1821976
    iput-object p1, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->b:LX/Boq;

    iput-object p2, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->a:LX/C96;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 15

    .prologue
    .line 1821977
    iget-object v0, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->b:LX/Boq;

    iget-object v0, v0, LX/Boq;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1821978
    :goto_0
    return-void

    .line 1821979
    :cond_0
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 1821980
    iget-object v1, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->b:LX/Boq;

    iget-object v1, v1, LX/Boq;->b:Ljava/util/Set;

    const v2, 0x3e99999a    # 0.3f

    const/high16 v3, 0x3f800000    # 1.0f

    const-wide/16 v4, 0x1f4

    const/high16 v9, 0x3f800000    # 1.0f

    const-wide/16 v12, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1821981
    float-to-double v10, v2

    cmpl-double v6, v10, v12

    if-ltz v6, :cond_1

    cmpg-float v6, v2, v9

    if-gtz v6, :cond_1

    move v6, v7

    :goto_1
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 1821982
    float-to-double v10, v3

    cmpl-double v6, v10, v12

    if-ltz v6, :cond_2

    cmpg-float v6, v3, v9

    if-gtz v6, :cond_2

    move v6, v7

    :goto_2
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 1821983
    long-to-double v10, v4

    cmpl-double v6, v10, v12

    if-ltz v6, :cond_3

    move v6, v7

    :goto_3
    invoke-static {v6}, LX/0PB;->checkArgument(Z)V

    .line 1821984
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v9

    .line 1821985
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_4
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    .line 1821986
    const-string v11, "alpha"

    const/4 v12, 0x2

    new-array v12, v12, [F

    aput v2, v12, v8

    aput v3, v12, v7

    invoke-static {v6, v11, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v6

    .line 1821987
    invoke-virtual {v6, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1821988
    invoke-interface {v9, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_1
    move v6, v8

    .line 1821989
    goto :goto_1

    :cond_2
    move v6, v8

    .line 1821990
    goto :goto_2

    :cond_3
    move v6, v8

    .line 1821991
    goto :goto_3

    .line 1821992
    :cond_4
    move-object v1, v9

    .line 1821993
    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1821994
    iget-object v1, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->b:LX/Boq;

    iget-object v1, v1, LX/Boq;->d:LX/Bsz;

    if-eqz v1, :cond_5

    .line 1821995
    iget-object v1, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->b:LX/Boq;

    iget-object v2, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->b:LX/Boq;

    iget-object v2, v2, LX/Boq;->d:LX/Bsz;

    const/4 v8, 0x0

    .line 1821996
    invoke-virtual {v2}, LX/Bsz;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1821997
    const/high16 v7, 0x40000000    # 2.0f

    invoke-static {v6, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v8, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v2, v6, v7}, LX/Bsz;->measure(II)V

    .line 1821998
    iget-object v6, v1, LX/Boq;->a:LX/21s;

    invoke-virtual {v2}, LX/Bsz;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {v6, v2, v8, v7}, LX/21s;->a(Landroid/view/View;II)Landroid/animation/Animator;

    move-result-object v6

    .line 1821999
    const-wide/16 v8, 0x12c

    invoke-virtual {v6, v8, v9}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 1822000
    new-instance v7, LX/Bop;

    invoke-direct {v7, v1, v2}, LX/Bop;-><init>(LX/Boq;LX/Bsz;)V

    invoke-virtual {v6, v7}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1822001
    move-object v1, v6

    .line 1822002
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1822003
    :cond_5
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 1822004
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 1822005
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 1822006
    iget-object v0, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->b:LX/Boq;

    iget-object v0, v0, LX/Boq;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1822007
    iget-object v0, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->b:LX/Boq;

    const/4 v1, 0x0

    .line 1822008
    iput-object v1, v0, LX/Boq;->d:LX/Bsz;

    .line 1822009
    iget-object v0, p0, Lcom/facebook/feed/rows/animators/OfflinePartAnimator$1;->a:LX/C96;

    .line 1822010
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/C96;->a:Z

    .line 1822011
    goto/16 :goto_0
.end method
