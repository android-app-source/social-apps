.class public Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/layout/ImageBlockLayout;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field private final b:F

.field private final c:Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition",
            "<TE;",
            "Lcom/facebook/attachments/angora/actionbutton/GenericActionButtonView;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

.field private final h:LX/1Ad;

.field private final i:LX/1qa;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1825766
    const-class v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    const-string v1, "native_newsfeed"

    const-string v2, "profile_image"

    invoke-static {v0, v1, v2}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Float;Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition;Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;LX/1Ad;LX/1qa;)V
    .locals 1
    .param p1    # Ljava/lang/Float;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825776
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1825777
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->b:F

    .line 1825778
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition;

    .line 1825779
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;

    .line 1825780
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    .line 1825781
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 1825782
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->i:LX/1qa;

    .line 1825783
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->h:LX/1Ad;

    .line 1825784
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

    .line 1825785
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1825767
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825768
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1825769
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1825770
    const v1, 0x7f0d2e1c

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->c:Lcom/facebook/feed/rows/sections/SubStoriesSubtitlePartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1825771
    const v1, 0x7f0d2e1b

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->d:Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1825772
    const v1, 0x7f0d2e1e

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/calltoaction/ActionButtonPartDefinition;

    invoke-interface {p1, v1, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1825773
    const v1, 0x7f0d2e1a

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->f:Lcom/facebook/feedplugins/attachments/PhotoPartDefinition;

    new-instance v3, LX/Bye;

    iget-object v4, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->h:LX/1Ad;

    sget-object v5, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->i:LX/1qa;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v6, LX/26P;->Share:LX/26P;

    invoke-virtual {v5, v0, v6}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    iget v4, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->b:F

    invoke-direct {v3, v0, v4}, LX/Bye;-><init>(LX/1aZ;F)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1825774
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v1, LX/2ya;

    invoke-direct {v1, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1825775
    const/4 v0, 0x0

    return-object v0
.end method
