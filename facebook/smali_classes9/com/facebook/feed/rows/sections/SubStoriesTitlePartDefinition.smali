.class public Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1PW;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        "Ljava/lang/CharSequence;",
        "TE;",
        "Landroid/widget/TextView;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825880
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1825881
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;
    .locals 3

    .prologue
    .line 1825882
    const-class v1, Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;

    monitor-enter v1

    .line 1825883
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825884
    sput-object v2, Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825885
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825886
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 1825887
    new-instance v0, Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;-><init>()V

    .line 1825888
    move-object v0, v0

    .line 1825889
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825890
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/SubStoriesTitlePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825891
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825892
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1825893
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1825894
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1825895
    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->A()Ljava/lang/String;

    move-result-object v0

    .line 1825896
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x2e684e81

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1825897
    check-cast p2, Ljava/lang/CharSequence;

    check-cast p4, Landroid/widget/TextView;

    .line 1825898
    invoke-virtual {p4, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1825899
    const/16 v1, 0x1f

    const v2, 0x676a153

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
