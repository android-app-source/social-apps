.class public Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pg;",
        ":",
        "LX/1Pe;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;>;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;

.field private static f:LX/0Xm;


# instance fields
.field private final b:Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;

.field private final c:Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

.field private final d:Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1824972
    new-instance v0, LX/Bqh;

    invoke-direct {v0}, LX/Bqh;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824946
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1824947
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->b:Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;

    .line 1824948
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->c:Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

    .line 1824949
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->d:Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;

    .line 1824950
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->e:Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;

    .line 1824951
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;
    .locals 7

    .prologue
    .line 1824961
    const-class v1, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;

    monitor-enter v1

    .line 1824962
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824963
    sput-object v2, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824964
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824965
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824966
    new-instance p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;)V

    .line 1824967
    move-object v0, p0

    .line 1824968
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824969
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824970
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824971
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1824953
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    .line 1824954
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1824955
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1824956
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->c:Lcom/facebook/feedplugins/attachments/EventAttachmentPartDefinition;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824957
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->d:Lcom/facebook/feedplugins/attachments/EventSubstoryDividerDefinition;

    invoke-virtual {p1, v1, v3}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824958
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->b:Lcom/facebook/feedplugins/attachments/EventSubstoryBlingBarPartDefinition;

    new-instance v2, LX/Anm;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarAnimationsPartDefinition;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    invoke-direct {v2, p2, v0}, LX/Anm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-virtual {p1, v1, v2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824959
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/EventSubStoriesPagePartDefinition;->e:Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1824960
    return-object v3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1824952
    const/4 v0, 0x1

    return v0
.end method
