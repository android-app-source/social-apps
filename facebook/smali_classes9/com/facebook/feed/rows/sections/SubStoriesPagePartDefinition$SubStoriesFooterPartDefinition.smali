.class public final Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;)V
    .locals 0

    .prologue
    .line 1825804
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;->a:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1825805
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825806
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;->a:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v1, LX/20d;

    sget-object v2, LX/1Wi;->PAGE:LX/1Wi;

    invoke-direct {v1, p2, v2}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1825807
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;->a:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    iget-object v0, v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->b:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    new-instance v1, LX/3Dt;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, LX/3Dt;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1825808
    const/4 v0, 0x0

    return-object v0
.end method
