.class public Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        "LX/C96;",
        "LX/1Pr;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/Boq;

.field public final b:LX/0qn;


# direct methods
.method public constructor <init>(LX/Boq;LX/0qn;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828458
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1828459
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;->a:LX/Boq;

    .line 1828460
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;->b:LX/0qn;

    .line 1828461
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;
    .locals 5

    .prologue
    .line 1828462
    const-class v1, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;

    monitor-enter v1

    .line 1828463
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828464
    sput-object v2, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828465
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828466
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828467
    new-instance p0, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;

    invoke-static {v0}, LX/Boq;->a(LX/0QB;)LX/Boq;

    move-result-object v3

    check-cast v3, LX/Boq;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v4

    check-cast v4, LX/0qn;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;-><init>(LX/Boq;LX/0qn;)V

    .line 1828468
    move-object v0, p0

    .line 1828469
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828470
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828471
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828472
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1828473
    check-cast p2, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p3, LX/1Pr;

    .line 1828474
    new-instance v0, LX/C95;

    invoke-direct {v0, p2}, LX/C95;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v0, p2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C96;

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x1b37ce4b

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1828475
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    check-cast p2, LX/C96;

    .line 1828476
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;->b:LX/0qn;

    invoke-virtual {v1, p1}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v1, v2, :cond_0

    .line 1828477
    iget-boolean v1, p2, LX/C96;->a:Z

    move v1, v1

    .line 1828478
    if-eqz v1, :cond_1

    .line 1828479
    :cond_0
    :goto_0
    const/16 v1, 0x1f

    const v2, -0x46bf3ce7

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1828480
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;->a:LX/Boq;

    invoke-virtual {v1, p4}, LX/Boq;->a(Landroid/view/View;)V

    .line 1828481
    iget-boolean v1, p2, LX/C96;->b:Z

    move v1, v1

    .line 1828482
    if-nez v1, :cond_0

    .line 1828483
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflinePartAnimationPartDefinition;->a:LX/Boq;

    invoke-virtual {v1, p2}, LX/Boq;->a(LX/C96;)V

    .line 1828484
    invoke-virtual {p2}, LX/C96;->d()V

    goto :goto_0
.end method
