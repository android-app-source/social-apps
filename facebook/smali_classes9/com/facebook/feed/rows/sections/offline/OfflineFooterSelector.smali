.class public Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828451
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1828452
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;->a:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    .line 1828453
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;->b:Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;

    .line 1828454
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;
    .locals 5

    .prologue
    .line 1828440
    const-class v1, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    monitor-enter v1

    .line 1828441
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828442
    sput-object v2, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828443
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828444
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828445
    new-instance p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;-><init>(Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;)V

    .line 1828446
    move-object v0, p0

    .line 1828447
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828448
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828449
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828450
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1828455
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1828456
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;->b:Lcom/facebook/feed/rows/sections/offline/OfflineFooterPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineFooterSelector;->a:Lcom/facebook/feedplugins/graphqlstory/footer/StoryEmptyFooterPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1828457
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1828439
    const/4 v0, 0x1

    return v0
.end method
