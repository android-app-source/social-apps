.class public Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static l:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;

.field private final e:Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;

.field private final f:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

.field private final i:Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

.field private final j:Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

.field private final k:Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition",
            "<",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828485
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1828486
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->c:Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;

    .line 1828487
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->e:Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;

    .line 1828488
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->a:Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;

    .line 1828489
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->b:Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    .line 1828490
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->d:Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;

    .line 1828491
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->f:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    .line 1828492
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->g:Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    .line 1828493
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->h:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    .line 1828494
    iput-object p9, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->i:Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    .line 1828495
    iput-object p10, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->j:Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    .line 1828496
    iput-object p11, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->k:Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;

    .line 1828497
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;
    .locals 15

    .prologue
    .line 1828498
    const-class v1, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    monitor-enter v1

    .line 1828499
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->l:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828500
    sput-object v2, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->l:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828501
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828502
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828503
    new-instance v3, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    move-result-object v11

    check-cast v11, Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    move-result-object v12

    check-cast v12, Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    move-result-object v13

    check-cast v13, Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;

    move-result-object v14

    check-cast v14, Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;

    invoke-direct/range {v3 .. v14}, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;-><init>(Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;)V

    .line 1828504
    move-object v0, v3

    .line 1828505
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828506
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828507
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828508
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1828509
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1828510
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->c:Lcom/facebook/feedplugins/offline/rows/MediaUploadTimedOutComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->k:Lcom/facebook/feedplugins/offline/rows/MediaUploadRetryComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->a:Lcom/facebook/feedplugins/offline/rows/MediaUploadProgressPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->b:Lcom/facebook/feedplugins/offline/rows/MediaUploadProcessingComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->g:Lcom/facebook/feedplugins/offline/rows/OfflineProgressV2PartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->f:Lcom/facebook/feedplugins/offline/rows/OfflineProgressPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->d:Lcom/facebook/feedplugins/offline/rows/OfflineRetryComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->h:Lcom/facebook/feedplugins/offline/rows/OfflineRetryPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->j:Lcom/facebook/feedplugins/offline/rows/OfflineFailedV2PartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->i:Lcom/facebook/feedplugins/offline/rows/OfflineFailedPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineProgressIndicatorSelector;->e:Lcom/facebook/feedplugins/offline/rows/OfflineSuccessComponentPartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1828511
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1828512
    const/4 v0, 0x1

    return v0
.end method
