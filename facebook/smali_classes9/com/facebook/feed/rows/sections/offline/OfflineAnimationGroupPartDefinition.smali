.class public Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "LX/Bsx",
        "<TE;>;",
        "LX/C96;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/Boq;

.field private final b:LX/0qn;

.field private final c:LX/0ad;

.field private final d:LX/0fO;


# direct methods
.method public constructor <init>(LX/Boq;LX/0qn;LX/0ad;LX/0fO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828391
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1828392
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->a:LX/Boq;

    .line 1828393
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->b:LX/0qn;

    .line 1828394
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->c:LX/0ad;

    .line 1828395
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->d:LX/0fO;

    .line 1828396
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;
    .locals 7

    .prologue
    .line 1828397
    const-class v1, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    monitor-enter v1

    .line 1828398
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828399
    sput-object v2, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828400
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828401
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828402
    new-instance p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;

    invoke-static {v0}, LX/Boq;->a(LX/0QB;)LX/Boq;

    move-result-object v3

    check-cast v3, LX/Boq;

    invoke-static {v0}, LX/0qn;->a(LX/0QB;)LX/0qn;

    move-result-object v4

    check-cast v4, LX/0qn;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/0fO;->b(LX/0QB;)LX/0fO;

    move-result-object v6

    check-cast v6, LX/0fO;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;-><init>(LX/Boq;LX/0qn;LX/0ad;LX/0fO;)V

    .line 1828403
    move-object v0, p0

    .line 1828404
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828405
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828406
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828407
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1828408
    check-cast p2, LX/Bsx;

    check-cast p3, LX/1Pr;

    .line 1828409
    iget-object v0, p2, LX/Bsx;->a:LX/Bsv;

    iget-object v1, p2, LX/Bsx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-interface {v0, p1, v1, p3}, LX/Bsv;->a(LX/1RF;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pr;)V

    .line 1828410
    iget-object v0, p2, LX/Bsx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1828411
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 1828412
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1828413
    new-instance v1, LX/C95;

    invoke-direct {v1, v0}, LX/C95;-><init>(Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/C96;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 5

    .prologue
    .line 1828414
    check-cast p1, LX/Bsx;

    check-cast p2, LX/C96;

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1828415
    iget-object v0, p1, LX/Bsx;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1828416
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 1828417
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1828418
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->b:LX/0qn;

    invoke-virtual {v3, v0}, LX/0qn;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    move-result-object v4

    .line 1828419
    invoke-static {v0}, LX/17E;->m(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, LX/17E;->q(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v4, v0, :cond_3

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->c:LX/0ad;

    invoke-static {v0}, LX/8Nv;->a(LX/0ad;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v3, v2

    .line 1828420
    :goto_0
    if-nez v3, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->POSTING:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v4, v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-eq v4, v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->TRANSCODING_FAILED:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v4, v0, :cond_4

    :cond_1
    move v0, v2

    .line 1828421
    :goto_1
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v1

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->d:LX/0fO;

    invoke-virtual {v0}, LX/0fO;->b()Z

    move-result v0

    if-nez v0, :cond_5

    const v0, 0x3e99999a    # 0.3f

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 1828422
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    if-ne v4, v0, :cond_2

    .line 1828423
    iget-boolean v0, p2, LX/C96;->a:Z

    move v0, v0

    .line 1828424
    if-nez v0, :cond_2

    if-eqz v3, :cond_6

    .line 1828425
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v3, v1

    .line 1828426
    goto :goto_0

    :cond_4
    move v0, v1

    .line 1828427
    goto :goto_1

    .line 1828428
    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2

    .line 1828429
    :cond_6
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->a:LX/Boq;

    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Boq;->a(Landroid/view/View;)V

    .line 1828430
    iget-boolean v0, p2, LX/C96;->b:Z

    move v0, v0

    .line 1828431
    if-nez v0, :cond_2

    .line 1828432
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->a:LX/Boq;

    invoke-virtual {v0, p2}, LX/Boq;->a(LX/C96;)V

    .line 1828433
    invoke-virtual {p2}, LX/C96;->d()V

    goto :goto_3
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1828434
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;LX/1RE;)V
    .locals 2

    .prologue
    .line 1828435
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/offline/OfflineAnimationGroupPartDefinition;->a:LX/Boq;

    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v1

    .line 1828436
    iget-object p0, v0, LX/Boq;->b:Ljava/util/Set;

    invoke-interface {p0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1828437
    invoke-virtual {p4}, LX/1RE;->a()Landroid/view/View;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 1828438
    return-void
.end method
