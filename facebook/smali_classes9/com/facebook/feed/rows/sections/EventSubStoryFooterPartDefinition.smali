.class public Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

.field private final b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1824973
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1824974
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    .line 1824975
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 1824976
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;
    .locals 5

    .prologue
    .line 1824977
    const-class v1, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;

    monitor-enter v1

    .line 1824978
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1824979
    sput-object v2, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1824980
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1824981
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1824982
    new-instance p0, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;-><init>(Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;)V

    .line 1824983
    move-object v0, p0

    .line 1824984
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1824985
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1824986
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1824987
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1824988
    sget-object v0, Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1824989
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1824990
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;->b:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    new-instance v1, LX/20d;

    sget-object v2, LX/1Wi;->PAGE:LX/1Wi;

    invoke-direct {v1, p2, v2}, LX/20d;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Wi;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824991
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/EventSubStoryFooterPartDefinition;->a:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    new-instance v1, LX/3Dt;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, LX/3Dt;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1824992
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1824993
    const/4 v0, 0x1

    return v0
.end method
