.class public Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Integer;",
        "TE;",
        "LX/Byo;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "LX/Byo;",
            ">;"
        }
    .end annotation
.end field

.field private static i:LX/0Xm;


# instance fields
.field public final b:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

.field public final c:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition",
            "<",
            "Lcom/facebook/feedplugins/base/footer/ui/DefaultFooterView;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

.field private final e:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesSponsoredPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesSponsoredPartDefinition",
            "<",
            "LX/1PW;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition",
            "<TE;>.SubStoriesFooterPartDefinition;"
        }
    .end annotation
.end field

.field private final g:LX/Br3;

.field private final h:LX/99j;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1825818
    new-instance v0, LX/Br5;

    invoke-direct {v0}, LX/Br5;-><init>()V

    sput-object v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->a:LX/1Cz;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;LX/Br3;LX/99j;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825819
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 1825820
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->d:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    .line 1825821
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->b:Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    .line 1825822
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->c:Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    .line 1825823
    new-instance v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesSponsoredPartDefinition;

    invoke-direct {v0}, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesSponsoredPartDefinition;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->e:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesSponsoredPartDefinition;

    .line 1825824
    new-instance v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;

    invoke-direct {v0, p0}, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;)V

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->f:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;

    .line 1825825
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->g:LX/Br3;

    .line 1825826
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->h:LX/99j;

    .line 1825827
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;
    .locals 9

    .prologue
    .line 1825828
    const-class v1, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    monitor-enter v1

    .line 1825829
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825830
    sput-object v2, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825831
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825832
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825833
    new-instance v3, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    const-class v7, LX/Br3;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/Br3;

    invoke-static {v0}, LX/99j;->a(LX/0QB;)LX/99j;

    move-result-object v8

    check-cast v8, LX/99j;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;-><init>(Lcom/facebook/feedplugins/graphqlstory/footer/FooterBackgroundPartDefinition;Lcom/facebook/feedplugins/base/footer/ui/BasicFooterPartDefinition;Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;LX/Br3;LX/99j;)V

    .line 1825834
    move-object v0, v3

    .line 1825835
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825836
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825837
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825838
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/Byo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1825839
    sget-object v0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1825840
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825841
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1825842
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825843
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->e:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesSponsoredPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1825844
    const v1, 0x7f0d2e20

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->d:Lcom/facebook/feedplugins/base/blingbar/BlingBarPartDefinition;

    new-instance v3, LX/Anm;

    const/4 v4, 0x1

    invoke-direct {v3, p2, v4}, LX/Anm;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Z)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1825845
    const v1, 0x7f0d2e21

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->f:Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition$SubStoriesFooterPartDefinition;

    invoke-interface {p1, v1, v2, p2}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1825846
    const v1, 0x7f0d2e19

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->g:LX/Br3;

    const v3, 0x3ff745d1

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Br3;->a(Ljava/lang/Float;)Lcom/facebook/feed/rows/sections/SubStoriesAttachmentPartDefinition;

    move-result-object v2

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-interface {p1, v1, v2, v0}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1825847
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/SubStoriesPagePartDefinition;->h:LX/99j;

    .line 1825848
    iget-object v1, v0, LX/99j;->a:Landroid/content/Context;

    invoke-virtual {v0}, LX/99j;->b()F

    move-result v2

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    move v0, v1

    .line 1825849
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x6a114f18

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1825850
    check-cast p2, Ljava/lang/Integer;

    check-cast p4, LX/Byo;

    .line 1825851
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p4, v1}, LX/Byo;->setWidth(I)V

    .line 1825852
    const/16 v1, 0x1f

    const v2, -0x21291eb9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
