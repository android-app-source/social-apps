.class public Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static h:LX/0Xm;


# instance fields
.field public final b:LX/1WE;

.field private final c:LX/BtF;

.field private final d:LX/BtH;

.field private final e:LX/1Uo;

.field private final f:LX/1Ad;

.field private final g:LX/0tO;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1828787
    const-class v0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;

    const-class v1, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->c(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1WE;LX/BtF;LX/BtH;LX/1Uo;LX/1Ad;LX/0tO;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828779
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828780
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->b:LX/1WE;

    .line 1828781
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->c:LX/BtF;

    .line 1828782
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->d:LX/BtH;

    .line 1828783
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->e:LX/1Uo;

    .line 1828784
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->f:LX/1Ad;

    .line 1828785
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->g:LX/0tO;

    .line 1828786
    return-void
.end method

.method private static a(Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;LX/BtG;Z)LX/1Dg;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/BtG;",
            "Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1828744
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x7

    iget v2, p4, LX/BtG;->j:I

    invoke-interface {v0, v1, v2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    const/4 v1, 0x6

    iget v2, p4, LX/BtG;->k:I

    invoke-interface {v0, v1, v2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->b:LX/1WE;

    invoke-virtual {v1, p1}, LX/1WE;->c(LX/1De;)LX/1XG;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/1XG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XG;

    move-result-object v1

    invoke-virtual {v1, p3}, LX/1XG;->a(LX/1Pn;)LX/1XG;

    move-result-object v1

    iget v2, p4, LX/BtG;->d:I

    invoke-virtual {v1, v2}, LX/1XG;->h(I)LX/1XG;

    move-result-object v1

    iget v2, p4, LX/BtG;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1XG;->b(Ljava/lang/Integer;)LX/1XG;

    move-result-object v1

    iget-object v2, p4, LX/BtG;->f:Landroid/text/Layout$Alignment;

    invoke-virtual {v1, v2}, LX/1XG;->a(Landroid/text/Layout$Alignment;)LX/1XG;

    move-result-object v1

    iget-object v2, p4, LX/BtG;->e:Ljava/lang/String;

    iget v3, p4, LX/BtG;->c:I

    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1XG;->a(Landroid/graphics/Typeface;)LX/1XG;

    move-result-object v1

    const v2, 0x7f0a05da

    invoke-virtual {v1, v2}, LX/1XG;->j(I)LX/1XG;

    move-result-object v1

    iget v2, p4, LX/BtG;->i:F

    .line 1828745
    iget-object v3, v1, LX/1XG;->a:LX/1XE;

    iput v2, v3, LX/1XE;->m:F

    .line 1828746
    move-object v1, v1

    .line 1828747
    invoke-virtual {v1, p5}, LX/1XG;->a(Z)LX/1XG;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget v1, p4, LX/BtG;->h:I

    invoke-interface {v0, v1}, LX/1Dh;->N(I)LX/1Dh;

    move-result-object v1

    .line 1828748
    iget-object v3, p4, LX/BtG;->g:Ljava/lang/String;

    iget v4, p4, LX/BtG;->b:I

    iget-object v5, p4, LX/BtG;->l:Ljava/lang/String;

    iget-object v6, p4, LX/BtG;->m:Ljava/lang/String;

    iget-object v7, p4, LX/BtG;->n:Ljava/lang/String;

    move-object v0, p0

    move-object v2, p3

    invoke-static/range {v0 .. v7}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->a(Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;LX/1Di;LX/1Pn;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1828749
    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;
    .locals 10

    .prologue
    .line 1828788
    const-class v1, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;

    monitor-enter v1

    .line 1828789
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828790
    sput-object v2, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828791
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828792
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828793
    new-instance v3, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;

    invoke-static {v0}, LX/1WE;->a(LX/0QB;)LX/1WE;

    move-result-object v4

    check-cast v4, LX/1WE;

    invoke-static {v0}, LX/BtF;->a(LX/0QB;)LX/BtF;

    move-result-object v5

    check-cast v5, LX/BtF;

    invoke-static {v0}, LX/BtH;->a(LX/0QB;)LX/BtH;

    move-result-object v6

    check-cast v6, LX/BtH;

    invoke-static {v0}, LX/1qZ;->b(LX/0QB;)LX/1Uo;

    move-result-object v7

    check-cast v7, LX/1Uo;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v8

    check-cast v8, LX/1Ad;

    invoke-static {v0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v9

    check-cast v9, LX/0tO;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;-><init>(LX/1WE;LX/BtF;LX/BtH;LX/1Uo;LX/1Ad;LX/0tO;)V

    .line 1828794
    move-object v0, v3

    .line 1828795
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828796
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828797
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828798
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;LX/1Di;LX/1Pn;Ljava/lang/String;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Di;",
            "TE;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 1828777
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v6, v5

    move-object v7, v5

    invoke-static/range {v0 .. v7}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->a(Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;LX/1Di;LX/1Pn;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1828778
    return-void
.end method

.method private static a(Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;LX/1Di;LX/1Pn;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Di;",
            "TE;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1828763
    invoke-static {p5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p7}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1828764
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p5}, LX/87X;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    .line 1828765
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1Di;->a(LX/1dc;)LX/1Di;

    .line 1828766
    :goto_0
    return-void

    .line 1828767
    :cond_0
    invoke-static {p3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1828768
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->f:LX/1Ad;

    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v1

    invoke-virtual {v1}, LX/1bX;->n()LX/1bf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    sget-object v1, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    .line 1828769
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->e:LX/1Uo;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, p4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 1828770
    iput-object v1, v0, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 1828771
    move-object v0, v0

    .line 1828772
    invoke-virtual {v0}, LX/1Uo;->u()LX/1af;

    move-result-object v0

    invoke-interface {p2}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, LX/1aX;->a(LX/1aY;Landroid/content/Context;)LX/1aX;

    move-result-object v0

    .line 1828773
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->f:LX/1Ad;

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1aX;->a(LX/1aZ;)V

    .line 1828774
    invoke-static {}, LX/1n3;->b()LX/1n5;

    move-result-object v1

    invoke-virtual {v0}, LX/1aX;->j()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/1n5;->a(Landroid/graphics/drawable/Drawable;)LX/1n5;

    move-result-object v0

    invoke-virtual {v0}, LX/1n6;->b()LX/1dc;

    move-result-object v0

    .line 1828775
    invoke-interface {p1, v0}, LX/1Di;->a(LX/1dc;)LX/1Di;

    goto :goto_0

    .line 1828776
    :cond_1
    invoke-interface {p1, p4}, LX/1Di;->y(I)LX/1Di;

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;Z)LX/1Dg;
    .locals 6
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 1828750
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->g:LX/0tO;

    invoke-virtual {v0}, LX/0tO;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828751
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->d:LX/BtH;

    .line 1828752
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1828753
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/BtH;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BtG;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    .line 1828754
    invoke-static/range {v0 .. v5}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->a(Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;LX/BtG;Z)LX/1Dg;

    move-result-object v0

    .line 1828755
    :goto_0
    return-object v0

    .line 1828756
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->c:LX/BtF;

    .line 1828757
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1828758
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/BtF;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/BtE;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v5, p4

    .line 1828759
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p0

    const/4 p1, 0x7

    iget p2, v4, LX/BtE;->h:I

    invoke-interface {p0, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p0

    const/4 p1, 0x6

    iget p2, v4, LX/BtE;->i:I

    invoke-interface {p0, p1, p2}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p0

    iget-object p1, v0, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->b:LX/1WE;

    invoke-virtual {p1, v1}, LX/1WE;->c(LX/1De;)LX/1XG;

    move-result-object p1

    invoke-virtual {p1, v2}, LX/1XG;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1XG;

    move-result-object p1

    invoke-virtual {p1, v3}, LX/1XG;->a(LX/1Pn;)LX/1XG;

    move-result-object p1

    iget p2, v4, LX/BtE;->d:I

    invoke-virtual {p1, p2}, LX/1XG;->h(I)LX/1XG;

    move-result-object p1

    iget p2, v4, LX/BtE;->a:I

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/1XG;->b(Ljava/lang/Integer;)LX/1XG;

    move-result-object p1

    iget-object p2, v4, LX/BtE;->f:Landroid/text/Layout$Alignment;

    invoke-virtual {p1, p2}, LX/1XG;->a(Landroid/text/Layout$Alignment;)LX/1XG;

    move-result-object p1

    iget-object p2, v4, LX/BtE;->e:Ljava/lang/String;

    iget p3, v4, LX/BtE;->c:I

    invoke-static {p2, p3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/1XG;->a(Landroid/graphics/Typeface;)LX/1XG;

    move-result-object p1

    const p2, 0x7f0a05da

    invoke-virtual {p1, p2}, LX/1XG;->j(I)LX/1XG;

    move-result-object p1

    invoke-virtual {p1, v5}, LX/1XG;->a(Z)LX/1XG;

    move-result-object p1

    invoke-virtual {p1}, LX/1X5;->c()LX/1Di;

    move-result-object p1

    const/high16 p2, 0x3f800000    # 1.0f

    invoke-interface {p1, p2}, LX/1Di;->a(F)LX/1Di;

    move-result-object p1

    invoke-interface {p0, p1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p0

    .line 1828760
    iget-object p1, v4, LX/BtE;->g:Ljava/lang/String;

    iget p2, v4, LX/BtE;->b:I

    invoke-static {v0, p0, v3, p1, p2}, Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;->a(Lcom/facebook/feed/rows/sections/text/StoryRichTextComponentSpec;LX/1Di;LX/1Pn;Ljava/lang/String;I)V

    .line 1828761
    invoke-interface {p0}, LX/1Di;->k()LX/1Dg;

    move-result-object p0

    move-object v0, p0

    .line 1828762
    goto :goto_0
.end method
