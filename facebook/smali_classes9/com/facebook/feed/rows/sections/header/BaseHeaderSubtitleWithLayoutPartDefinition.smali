.class public Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Brw;",
        "LX/Brx;",
        "TE;",
        "Lcom/facebook/fbui/widget/text/TextLayoutView;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/Bs7;

.field private final b:LX/BsG;

.field public final c:LX/1DR;

.field public final d:Landroid/text/TextPaint;

.field private final e:LX/1W9;

.field public final f:LX/03V;

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BsG;LX/Bs7;LX/1W9;LX/1DR;LX/03V;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826814
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1826815
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->a:LX/Bs7;

    .line 1826816
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->b:LX/BsG;

    .line 1826817
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->e:LX/1W9;

    .line 1826818
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->c:LX/1DR;

    .line 1826819
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->f:LX/03V;

    .line 1826820
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->g:Ljava/util/Set;

    .line 1826821
    new-instance v0, Landroid/text/TextPaint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/text/TextPaint;-><init>(I)V

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->d:Landroid/text/TextPaint;

    .line 1826822
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->d:Landroid/text/TextPaint;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0904

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1826823
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;
    .locals 10

    .prologue
    .line 1826841
    const-class v1, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    monitor-enter v1

    .line 1826842
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826843
    sput-object v2, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826844
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826845
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826846
    new-instance v3, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const-class v5, LX/BsG;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/BsG;

    invoke-static {v0}, LX/Bs7;->a(LX/0QB;)LX/Bs7;

    move-result-object v6

    check-cast v6, LX/Bs7;

    invoke-static {v0}, LX/1W9;->a(LX/0QB;)LX/1W9;

    move-result-object v7

    check-cast v7, LX/1W9;

    invoke-static {v0}, LX/1DR;->a(LX/0QB;)LX/1DR;

    move-result-object v8

    check-cast v8, LX/1DR;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;-><init>(Landroid/content/Context;LX/BsG;LX/Bs7;LX/1W9;LX/1DR;LX/03V;)V

    .line 1826847
    move-object v0, v3

    .line 1826848
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826849
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826850
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826851
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;LX/1z4;LX/BsF;I)Landroid/text/Layout;
    .locals 2

    .prologue
    .line 1826852
    iget-object v0, p2, LX/BsF;->a:LX/1nq;

    move-object v0, v0

    .line 1826853
    iget-object v1, p1, LX/1z4;->b:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    .line 1826854
    invoke-virtual {v0, p3}, LX/1nq;->a(I)LX/1nq;

    .line 1826855
    const/4 v1, 0x1

    .line 1826856
    iput-boolean v1, v0, LX/1nq;->f:Z

    .line 1826857
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->e:LX/1W9;

    .line 1826858
    iput-object v1, v0, LX/1nq;->d:LX/1WA;

    .line 1826859
    invoke-virtual {v0}, LX/1nq;->c()Landroid/text/Layout;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1826829
    check-cast p2, LX/Brw;

    check-cast p3, LX/1Pk;

    .line 1826830
    iget-object v0, p2, LX/Brw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v1, p2, LX/Brw;->b:LX/1xb;

    iget v2, p2, LX/Brw;->c:I

    .line 1826831
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->a:LX/Bs7;

    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->c:LX/1DR;

    invoke-virtual {v5}, LX/1DR;->a()I

    move-result v5

    invoke-virtual {v3, v0, v4, v5, v2}, LX/Bs7;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;II)I

    move-result v3

    .line 1826832
    iget-object v4, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->d:Landroid/text/TextPaint;

    invoke-interface {v1, v0, v4, v3}, LX/1xb;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/text/TextPaint;I)LX/1z4;

    move-result-object v3

    move-object v1, v3

    .line 1826833
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->b:LX/BsG;

    check-cast p3, LX/1Pn;

    invoke-interface {p3}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 1826834
    new-instance v5, LX/BsF;

    invoke-static {v0}, LX/0hL;->a(LX/0QB;)LX/0hL;

    move-result-object v3

    check-cast v3, LX/0hL;

    invoke-static {v0}, LX/1W9;->a(LX/0QB;)LX/1W9;

    move-result-object v4

    check-cast v4, LX/1W9;

    invoke-direct {v5, v2, v3, v4}, LX/BsF;-><init>(Landroid/content/Context;LX/0hL;LX/1W9;)V

    .line 1826835
    move-object v2, v5

    .line 1826836
    new-instance v3, LX/Brx;

    iget v0, v1, LX/1z4;->a:I

    invoke-static {p0, v1, v2, v0}, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->a$redex0(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;LX/1z4;LX/BsF;I)Landroid/text/Layout;

    move-result-object v4

    iget-object v0, p2, LX/Brw;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826837
    iget-object v5, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v5

    .line 1826838
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1826839
    new-instance v5, LX/Brv;

    invoke-direct {v5, p0, v0}, LX/Brv;-><init>(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;Lcom/facebook/graphql/model/GraphQLStory;)V

    move-object v0, v5

    .line 1826840
    new-instance v5, LX/Bru;

    invoke-direct {v5, p0, v1, v2}, LX/Bru;-><init>(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;LX/1z4;LX/BsF;)V

    invoke-direct {v3, v4, v0, v5}, LX/Brx;-><init>(Landroid/text/Layout;LX/34L;LX/5Of;)V

    return-object v3
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x7b9382c4

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1826824
    check-cast p2, LX/Brx;

    check-cast p4, Lcom/facebook/fbui/widget/text/TextLayoutView;

    .line 1826825
    iget-object v1, p2, LX/Brx;->c:Landroid/text/Layout;

    if-nez v1, :cond_0

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {p4, v1}, Lcom/facebook/fbui/widget/text/TextLayoutView;->setVisibility(I)V

    .line 1826826
    iget-object v1, p2, LX/Brx;->c:Landroid/text/Layout;

    iget-object v2, p2, LX/Brx;->b:LX/5Of;

    iget-object p0, p2, LX/Brx;->a:LX/34L;

    invoke-virtual {p4, v1, v2, p0}, Lcom/facebook/fbui/widget/text/TextLayoutView;->a(Landroid/text/Layout;LX/5Of;LX/34L;)V

    .line 1826827
    const/16 v1, 0x1f

    const v2, 0x680c786

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1826828
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
