.class public Lcom/facebook/feed/rows/sections/header/TitleWithMenuButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Ps;",
        ":",
        "LX/1Pk;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType",
        "<",
        "LX/BsL;",
        "Ljava/lang/Void;",
        "TE;",
        "LX/2ee;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827403
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinitionWithViewType;-><init>()V

    .line 1827404
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/TitleWithMenuButtonPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    .line 1827405
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/TitleWithMenuButtonPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 1827406
    return-void
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1Cz",
            "<",
            "LX/2ee;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1827407
    sget-object v0, LX/2ee;->b:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1827398
    check-cast p2, LX/BsL;

    .line 1827399
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/TitleWithMenuButtonPartDefinition;->a:Lcom/facebook/feed/rows/styling/BackgroundPartDefinition;

    new-instance v1, LX/1X6;

    iget-object v2, p2, LX/BsL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    sget-object v3, LX/1Ua;->i:LX/1Ua;

    invoke-direct {v1, v2, v3}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1827400
    const v1, 0x7f0d0bde

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/TitleWithMenuButtonPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v3, LX/24b;

    iget-object v4, p2, LX/BsL;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-boolean v0, p2, LX/BsL;->d:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/1dl;->CLICKABLE:LX/1dl;

    :goto_0
    invoke-direct {v3, v4, v0}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v1, v2, v3}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1827401
    const/4 v0, 0x0

    return-object v0

    .line 1827402
    :cond_0
    sget-object v0, LX/1dl;->HIDDEN:LX/1dl;

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x166949ad

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1827393
    check-cast p1, LX/BsL;

    check-cast p4, LX/2ee;

    .line 1827394
    iget-object v1, p1, LX/BsL;->b:Ljava/lang/String;

    iget-object v2, p1, LX/BsL;->c:LX/2ej;

    invoke-virtual {p4, v1, v2}, LX/2ee;->a(Ljava/lang/CharSequence;LX/2ej;)V

    .line 1827395
    sget-object v1, LX/2ei;->SUGGESTED_CONTENT:LX/2ei;

    invoke-virtual {p4, v1}, LX/2ee;->setStyle(LX/2ei;)V

    .line 1827396
    iget-boolean v1, p1, LX/BsL;->d:Z

    invoke-virtual {p4, v1}, LX/2ee;->setMenuButtonActive(Z)V

    .line 1827397
    const/16 v1, 0x1f

    const v2, -0x48a41f18

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
