.class public Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "LX/Bs6;",
        "LX/1PW;",
        "Lcom/facebook/widget/text/TextViewWithFallback;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1xe;

.field private final b:LX/1xg;

.field private final c:LX/1xc;

.field public final d:LX/1e4;

.field public final e:LX/0wM;


# direct methods
.method public constructor <init>(LX/1xe;LX/1xg;LX/1xc;LX/1e4;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827026
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1827027
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->a:LX/1xe;

    .line 1827028
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->b:LX/1xg;

    .line 1827029
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->c:LX/1xc;

    .line 1827030
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->d:LX/1e4;

    .line 1827031
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->e:LX/0wM;

    .line 1827032
    return-void
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1827033
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1827034
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1827035
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827036
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->a:LX/1xe;

    invoke-virtual {v1, p2}, LX/1xe;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Ljava/lang/String;

    move-result-object v2

    .line 1827037
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->b:LX/1xg;

    invoke-virtual {v1, p2, v2}, LX/1xg;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1827038
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->b:LX/1xg;

    invoke-virtual {v3, p2, v2}, LX/1xg;->b(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1827039
    invoke-static {v0}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->c:LX/1xc;

    invoke-virtual {v3, v0, v1}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 1827040
    :cond_0
    invoke-static {v0}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->c:LX/1xc;

    invoke-virtual {v3, v0, v2}, LX/1xc;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 1827041
    :cond_1
    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    if-eqz v3, :cond_2

    if-eqz v1, :cond_2

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-gtz v3, :cond_3

    .line 1827042
    :cond_2
    const/4 v3, 0x0

    .line 1827043
    :goto_0
    move v0, v3

    .line 1827044
    new-instance v3, LX/Bs6;

    invoke-direct {v3, v2, v1, v0}, LX/Bs6;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    return-object v3

    .line 1827045
    :cond_3
    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v3

    .line 1827046
    iget-object p1, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->d:LX/1e4;

    invoke-virtual {p1, v3}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v3

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0xf08b163

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1827047
    check-cast p2, LX/Bs6;

    check-cast p4, Lcom/facebook/widget/text/TextViewWithFallback;

    const/4 p3, 0x0

    .line 1827048
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/HeaderSubtitlePartDefinition;->e:LX/0wM;

    iget v2, p2, LX/Bs6;->c:I

    const p1, -0x6e685d

    invoke-virtual {v1, v2, p1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p4, p3, p3, v1, p3}, Lcom/facebook/widget/text/TextViewWithFallback;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1827049
    iget-object v1, p2, LX/Bs6;->b:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/TextViewWithFallback;->setVisibility(I)V

    .line 1827050
    iget-object v1, p2, LX/Bs6;->b:Ljava/lang/CharSequence;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/text/TextViewWithFallback;->setText(Ljava/lang/CharSequence;)V

    .line 1827051
    iget-object v1, p2, LX/Bs6;->a:Ljava/lang/CharSequence;

    .line 1827052
    iput-object v1, p4, Lcom/facebook/widget/text/TextViewWithFallback;->a:Ljava/lang/CharSequence;

    .line 1827053
    const/16 v1, 0x1f

    const v2, 0x4ef7ae38

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1827054
    :cond_0
    const/16 v1, 0x8

    goto :goto_0
.end method
