.class public Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/2ei;",
        "LX/Bs8;",
        "LX/1PW;",
        "Lcom/facebook/feed/rows/views/ContentTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827117
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1827118
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;->a:Landroid/content/res/Resources;

    .line 1827119
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;
    .locals 4

    .prologue
    .line 1827120
    const-class v1, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;

    monitor-enter v1

    .line 1827121
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827122
    sput-object v2, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827123
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827124
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827125
    new-instance p0, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;-><init>(Landroid/content/res/Resources;)V

    .line 1827126
    move-object v0, p0

    .line 1827127
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827128
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827129
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827130
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1827115
    check-cast p2, LX/2ei;

    .line 1827116
    new-instance v0, LX/Bs8;

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;->a:Landroid/content/res/Resources;

    invoke-virtual {p2, v1}, LX/2ei;->getColor(Landroid/content/res/Resources;)I

    move-result v1

    invoke-virtual {p2}, LX/2ei;->getFontStyle()I

    move-result v2

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;->a:Landroid/content/res/Resources;

    invoke-virtual {p2, v3}, LX/2ei;->getFontSize(Landroid/content/res/Resources;)I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, LX/Bs8;-><init>(III)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x254930a8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1827110
    check-cast p2, LX/Bs8;

    check-cast p4, Lcom/facebook/feed/rows/views/ContentTextView;

    .line 1827111
    iget v1, p2, LX/Bs8;->a:I

    invoke-virtual {p4, v1}, Lcom/facebook/feed/rows/views/ContentTextView;->setTextColor(I)V

    .line 1827112
    const/4 v1, 0x0

    iget v2, p2, LX/Bs8;->b:I

    invoke-virtual {p4, v1, v2}, Lcom/facebook/feed/rows/views/ContentTextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 1827113
    iget v1, p2, LX/Bs8;->c:I

    int-to-float v1, v1

    invoke-virtual {p4, v1}, Lcom/facebook/feed/rows/views/ContentTextView;->setTextSize(F)V

    .line 1827114
    const/16 v1, 0x1f

    const v2, 0x45877c22

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
