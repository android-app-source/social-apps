.class public Lcom/facebook/feed/rows/sections/header/FollowUserButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pb;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Bs1;",
        "Ljava/lang/Boolean;",
        "TE;",
        "Lcom/facebook/widget/springbutton/SpringScaleButton;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826914
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1826915
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/FollowUserButtonPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;

    .line 1826916
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Ljava/lang/String;Ljava/lang/String;LX/1Pb;)Landroid/view/View$OnClickListener;
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FollowLocations;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/FollowLocations;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pb;",
            ">(",
            "Ljava/lang/String;",
            "Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "TE;)",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 1826917
    new-instance v0, LX/Bs0;

    move-object v1, p1

    move-object v2, p4

    move-object v3, p0

    move-object v4, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/Bs0;-><init>(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;LX/1Pb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1826918
    return-object v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1826919
    check-cast p2, LX/Bs1;

    check-cast p3, LX/1Pb;

    .line 1826920
    iget-object v0, p2, LX/Bs1;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 1826921
    :goto_0
    iget-object v1, p2, LX/Bs1;->c:Ljava/lang/String;

    iget-object v2, p2, LX/Bs1;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    iget-object v3, p2, LX/Bs1;->d:Ljava/lang/String;

    iget-object v4, p2, LX/Bs1;->e:Ljava/lang/String;

    invoke-static {v1, v2, v3, v4, p3}, Lcom/facebook/feed/rows/sections/header/FollowUserButtonPartDefinition;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Ljava/lang/String;Ljava/lang/String;LX/1Pb;)Landroid/view/View$OnClickListener;

    move-result-object v1

    .line 1826922
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/FollowUserButtonPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;

    new-instance v3, LX/BsD;

    iget-object v4, p2, LX/Bs1;->a:LX/0jW;

    iget-object v5, p2, LX/Bs1;->c:Ljava/lang/String;

    invoke-direct {v3, v4, v5, v1, v0}, LX/BsD;-><init>(LX/0jW;Ljava/lang/String;Landroid/view/View$OnClickListener;Z)V

    invoke-interface {p1, v2, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826923
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1826924
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x4d041ded    # 1.38534608E8f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1826925
    check-cast p1, LX/Bs1;

    check-cast p2, Ljava/lang/Boolean;

    check-cast p4, Lcom/facebook/widget/springbutton/SpringScaleButton;

    .line 1826926
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, LX/Bs1;->g:I

    :goto_0
    invoke-virtual {p4, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setImageResource(I)V

    .line 1826927
    const/16 v1, 0x1f

    const v2, 0x4911a8c9

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1826928
    :cond_0
    iget v1, p1, LX/Bs1;->f:I

    goto :goto_0
.end method
