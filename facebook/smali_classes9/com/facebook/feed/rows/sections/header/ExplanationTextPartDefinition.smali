.class public Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Brz;",
        "Ljava/lang/Boolean;",
        "TE;",
        "Lcom/facebook/feed/rows/views/ContentTextView;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/3gP;

.field private final b:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final c:Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;


# direct methods
.method public constructor <init>(LX/3gP;Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826886
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1826887
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->a:LX/3gP;

    .line 1826888
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->b:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    .line 1826889
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;

    .line 1826890
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;
    .locals 6

    .prologue
    .line 1826891
    const-class v1, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;

    monitor-enter v1

    .line 1826892
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826893
    sput-object v2, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826894
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826895
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826896
    new-instance p0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;

    invoke-static {v0}, LX/3gP;->a(LX/0QB;)LX/3gP;

    move-result-object v3

    check-cast v3, LX/3gP;

    invoke-static {v0}, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;-><init>(LX/3gP;Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;)V

    .line 1826897
    move-object v0, p0

    .line 1826898
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826899
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826900
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826901
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1826902
    check-cast p2, LX/Brz;

    .line 1826903
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->b:Lcom/facebook/feedplugins/spannable/SpannableInTextViewPartDefinition;

    new-instance v1, LX/3gR;

    iget-object v2, p2, LX/Brz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->a:LX/3gP;

    invoke-direct {v1, v2, v3}, LX/3gR;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/3gP;)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826904
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/ExplanationTextPartDefinition;->c:Lcom/facebook/feed/rows/sections/header/HeaderTextStylePartDefinition;

    iget-object v1, p2, LX/Brz;->b:LX/2ei;

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826905
    iget-object v0, p2, LX/Brz;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x7e1ba079

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1826906
    check-cast p2, Ljava/lang/Boolean;

    check-cast p4, Lcom/facebook/feed/rows/views/ContentTextView;

    .line 1826907
    const v1, 0x7f0d0081

    invoke-virtual {p4, v1, p2}, Lcom/facebook/feed/rows/views/ContentTextView;->setTag(ILjava/lang/Object;)V

    .line 1826908
    const/16 v1, 0x1f

    const v2, -0x642350ea

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
