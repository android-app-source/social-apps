.class public Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pn;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static f:LX/0Xm;


# instance fields
.field public final b:LX/1nA;

.field public final c:LX/1nu;

.field public final d:LX/0ad;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1828036
    const-class v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1nA;LX/1nu;LX/0ad;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1nA;",
            "LX/1nu;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828037
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828038
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->b:LX/1nA;

    .line 1828039
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->c:LX/1nu;

    .line 1828040
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->d:LX/0ad;

    .line 1828041
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->e:LX/0Or;

    .line 1828042
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;
    .locals 7

    .prologue
    .line 1828043
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;

    monitor-enter v1

    .line 1828044
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828045
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828046
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828047
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828048
    new-instance v6, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;

    invoke-static {v0}, LX/1nA;->a(LX/0QB;)LX/1nA;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    const/16 p0, 0x122d

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;-><init>(LX/1nA;LX/1nu;LX/0ad;LX/0Or;)V

    .line 1828049
    move-object v0, v6

    .line 1828050
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828051
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/GroupStoryTitleComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828052
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828053
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
