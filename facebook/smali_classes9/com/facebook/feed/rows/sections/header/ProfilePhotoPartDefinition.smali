.class public Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pp;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/BsB;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/drawee/fbpipeline/FbDraweeView;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static g:LX/0Xm;


# instance fields
.field private final b:LX/1xa;

.field private final c:LX/1BM;

.field private final d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/multirow/parts/FbDraweePartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final f:LX/0pJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1827213
    const-class v0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    const-string v1, "native_newsfeed"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1xa;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1BM;LX/0pJ;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827214
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1827215
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->b:LX/1xa;

    .line 1827216
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->c:LX/1BM;

    .line 1827217
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    .line 1827218
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1827219
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->f:LX/0pJ;

    .line 1827220
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;
    .locals 9

    .prologue
    .line 1827221
    const-class v1, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    monitor-enter v1

    .line 1827222
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827223
    sput-object v2, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827224
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827225
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827226
    new-instance v3, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;

    invoke-static {v0}, LX/1xa;->a(LX/0QB;)LX/1xa;

    move-result-object v4

    check-cast v4, LX/1xa;

    invoke-static {v0}, Lcom/facebook/multirow/parts/FbDraweePartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    move-result-object v5

    check-cast v5, Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v6

    check-cast v6, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/1BM;->a(LX/0QB;)LX/1BM;

    move-result-object v7

    check-cast v7, LX/1BM;

    invoke-static {v0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v8

    check-cast v8, LX/0pJ;

    invoke-direct/range {v3 .. v8}, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;-><init>(LX/1xa;Lcom/facebook/multirow/parts/FbDraweePartDefinition;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/1BM;LX/0pJ;)V

    .line 1827227
    move-object v0, v3

    .line 1827228
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827229
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827230
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827231
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1827232
    check-cast p2, LX/BsB;

    const/4 v1, 0x0

    .line 1827233
    iget-object v0, p2, LX/BsB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/BsB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1827234
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 1827235
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    move-object v3, v0

    .line 1827236
    :goto_0
    if-eqz v3, :cond_2

    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v0

    .line 1827237
    :goto_1
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v0}, LX/1xl;->c(Lcom/facebook/graphql/model/GraphQLActor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v2, v0

    .line 1827238
    :goto_2
    if-eqz v3, :cond_4

    iget-object v0, p2, LX/BsB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v0}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v0

    .line 1827239
    :goto_3
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->f:LX/0pJ;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0pJ;->c(Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1827240
    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    .line 1827241
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->c:LX/1BM;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/1BM;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827242
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->e:Lcom/facebook/multirow/parts/FbDraweePartDefinition;

    new-instance v3, LX/2f8;

    invoke-direct {v3}, LX/2f8;-><init>()V

    invoke-virtual {v3, v2}, LX/2f8;->a(Landroid/net/Uri;)LX/2f8;

    move-result-object v2

    sget-object v3, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 1827243
    iput-object v3, v2, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 1827244
    move-object v2, v2

    .line 1827245
    iget v3, p2, LX/BsB;->b:I

    iget v4, p2, LX/BsB;->c:I

    invoke-virtual {v2, v3, v4}, LX/2f8;->a(II)LX/2f8;

    move-result-object v2

    const/4 v3, 0x1

    .line 1827246
    iput-boolean v3, v2, LX/2f8;->g:Z

    .line 1827247
    move-object v2, v2

    .line 1827248
    invoke-virtual {v2}, LX/2f8;->a()LX/2f9;

    move-result-object v2

    invoke-interface {p1, v0, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1827249
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->b:LX/1xa;

    iget-object v2, p2, LX/BsB;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1827250
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 1827251
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827252
    invoke-static {v3}, LX/185;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v4

    .line 1827253
    if-nez v4, :cond_5

    .line 1827254
    iget-object v4, v0, LX/1xa;->c:LX/03V;

    sget-object v5, LX/1xa;->a:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    const-string p3, "Story without an actor "

    invoke-direct {p2, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLStory;->E_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827255
    const/4 v3, 0x0

    .line 1827256
    :goto_4
    move-object v0, v3

    .line 1827257
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/ProfilePhotoPartDefinition;->d:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1827258
    return-object v1

    :cond_1
    move-object v3, v1

    .line 1827259
    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 1827260
    goto/16 :goto_1

    :cond_3
    move-object v2, v1

    .line 1827261
    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 1827262
    goto :goto_3

    :cond_5
    new-instance v4, LX/Ao8;

    invoke-direct {v4, v0, v3, v2}, LX/Ao8;-><init>(LX/1xa;Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v3, v4

    goto :goto_4
.end method
