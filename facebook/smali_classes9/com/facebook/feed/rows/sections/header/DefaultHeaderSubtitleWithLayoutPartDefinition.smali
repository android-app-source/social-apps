.class public Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ":",
        "LX/1Pn;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Bry;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/fbui/widget/text/TextLayoutView;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final b:LX/1xa;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;LX/1xa;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826864
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1826865
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    .line 1826866
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;->b:LX/1xa;

    .line 1826867
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;
    .locals 5

    .prologue
    .line 1826868
    const-class v1, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    monitor-enter v1

    .line 1826869
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826870
    sput-object v2, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826871
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826872
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826873
    new-instance p0, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    invoke-static {v0}, LX/1xa;->a(LX/0QB;)LX/1xa;

    move-result-object v4

    check-cast v4, LX/1xa;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;LX/1xa;)V

    .line 1826874
    move-object v0, p0

    .line 1826875
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826876
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826877
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826878
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 1826879
    check-cast p2, LX/Bry;

    .line 1826880
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;->a:Lcom/facebook/feed/rows/sections/header/BaseHeaderSubtitleWithLayoutPartDefinition;

    new-instance v1, LX/Brw;

    iget-object v2, p2, LX/Bry;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/DefaultHeaderSubtitleWithLayoutPartDefinition;->b:LX/1xa;

    iget v4, p2, LX/Bry;->b:I

    invoke-direct {v1, v2, v3, v4}, LX/Brw;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1xb;I)V

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826881
    const/4 v0, 0x0

    return-object v0
.end method
