.class public Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pi;",
        ":",
        "LX/1Pk;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/24a;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Boolean;",
        "TE;TV;>;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

.field private final c:LX/1VE;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1VE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827305
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1827306
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->a:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    .line 1827307
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    .line 1827308
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->c:LX/1VE;

    .line 1827309
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;
    .locals 6

    .prologue
    .line 1827310
    const-class v1, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;

    monitor-enter v1

    .line 1827311
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827312
    sput-object v2, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827313
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827314
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827315
    new-instance p0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v5

    check-cast v5, LX/1VE;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;LX/1VE;)V

    .line 1827316
    move-object v0, p0

    .line 1827317
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827318
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827319
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827320
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 1827321
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pi;

    .line 1827322
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1827323
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827324
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->c:LX/1VE;

    check-cast p3, LX/1Pk;

    invoke-interface {p3}, LX/1Pk;->e()LX/1SX;

    move-result-object v2

    invoke-virtual {v1, p2, v2}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;)LX/1dl;

    move-result-object v2

    .line 1827325
    sget-object v1, LX/1dl;->HIDDEN:LX/1dl;

    if-eq v2, v1, :cond_1

    const/4 v1, 0x1

    .line 1827326
    :goto_0
    const v3, 0x7f0d0bde

    iget-object v4, p0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/MenuButtonPartDefinition;

    new-instance v5, LX/24b;

    invoke-direct {v5, p2, v2}, LX/24b;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1dl;)V

    invoke-interface {p1, v3, v4, v5}, LX/1aD;->a(ILX/1Nt;Ljava/lang/Object;)V

    .line 1827327
    if-eqz v1, :cond_0

    .line 1827328
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/header/StoryMenuButtonPartDefinition;->a:Lcom/facebook/feed/rows/sections/tooltip/AnchoredTooltipPartDefinition;

    invoke-interface {p1, v2, v0}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1827329
    :cond_0
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    .line 1827330
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, 0x55ccc110

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1827331
    check-cast p2, Ljava/lang/Boolean;

    .line 1827332
    check-cast p4, LX/24a;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {p4, v1}, LX/24a;->setMenuButtonActive(Z)V

    .line 1827333
    const/16 v1, 0x1f

    const v2, -0x46a75242

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1827334
    check-cast p4, LX/24a;

    const/4 v0, 0x0

    invoke-interface {p4, v0}, LX/24a;->setMenuButtonActive(Z)V

    .line 1827335
    return-void
.end method
