.class public Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/BsD;",
        "LX/BsE;",
        "LX/1Pr;",
        "Lcom/facebook/widget/springbutton/SpringScaleButton;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/215;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/215;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827302
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1827303
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;->a:LX/0Or;

    .line 1827304
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;
    .locals 4

    .prologue
    .line 1827275
    const-class v1, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;

    monitor-enter v1

    .line 1827276
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827277
    sput-object v2, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827278
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827279
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827280
    new-instance v3, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;

    const/16 p0, 0x13a4

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;-><init>(LX/0Or;)V

    .line 1827281
    move-object v0, v3

    .line 1827282
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827283
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827284
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1827286
    check-cast p2, LX/BsD;

    check-cast p3, LX/1Pr;

    .line 1827287
    new-instance v0, LX/BsC;

    iget-object v1, p2, LX/BsD;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, LX/BsC;-><init>(Ljava/lang/String;)V

    iget-object v1, p2, LX/BsD;->a:LX/0jW;

    invoke-interface {p3, v0, v1}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BsE;

    .line 1827288
    iget-object v1, v0, LX/BsE;->a:LX/215;

    if-nez v1, :cond_0

    .line 1827289
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/215;

    iput-object v1, v0, LX/BsE;->a:LX/215;

    .line 1827290
    :cond_0
    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x966459e

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1827291
    check-cast p1, LX/BsD;

    check-cast p2, LX/BsE;

    check-cast p4, Lcom/facebook/widget/springbutton/SpringScaleButton;

    .line 1827292
    const/4 v1, 0x0

    invoke-virtual {p4, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    .line 1827293
    iget-object v1, p1, LX/BsD;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1827294
    iget-boolean v1, p1, LX/BsD;->d:Z

    invoke-virtual {p4, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setSelected(Z)V

    .line 1827295
    iget-object v1, p2, LX/BsE;->a:LX/215;

    invoke-virtual {p4, v1}, Lcom/facebook/widget/springbutton/SpringScaleButton;->a(LX/215;)V

    .line 1827296
    const/16 v1, 0x1f

    const v2, -0x5a60eb4d

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1827297
    check-cast p4, Lcom/facebook/widget/springbutton/SpringScaleButton;

    .line 1827298
    const/4 v0, 0x0

    invoke-virtual {p4, v0}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1827299
    invoke-virtual {p4}, Lcom/facebook/widget/springbutton/SpringScaleButton;->a()V

    .line 1827300
    const/16 v0, 0x8

    invoke-virtual {p4, v0}, Lcom/facebook/widget/springbutton/SpringScaleButton;->setVisibility(I)V

    .line 1827301
    return-void
.end method
