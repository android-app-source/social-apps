.class public Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/MountSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Po;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final b:LX/0Zb;

.field public final c:LX/0Zm;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1827458
    const-class v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/0Zm;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827470
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827471
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->d:Z

    .line 1827472
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->b:LX/0Zb;

    .line 1827473
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->c:LX/0Zm;

    .line 1827474
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;
    .locals 5

    .prologue
    .line 1827459
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;

    monitor-enter v1

    .line 1827460
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827461
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827462
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827463
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827464
    new-instance p0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0Zl;->a(LX/0QB;)LX/0Zl;

    move-result-object v4

    check-cast v4, LX/0Zm;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;-><init>(LX/0Zb;LX/0Zm;)V

    .line 1827465
    move-object v0, p0

    .line 1827466
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827467
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ActorProfileVideoComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827468
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827469
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
