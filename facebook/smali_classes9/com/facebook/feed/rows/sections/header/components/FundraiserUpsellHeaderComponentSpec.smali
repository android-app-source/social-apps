.class public Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field public static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/17W;

.field public final d:LX/0bH;

.field public final e:LX/189;

.field public final f:LX/2g9;

.field public final g:LX/BsZ;

.field public final h:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1827971
    const-class v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/17W;LX/0bH;LX/189;LX/2g9;LX/BsZ;LX/1vg;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/17W;",
            "LX/0bH;",
            "LX/189;",
            "LX/2g9;",
            "LX/BsZ;",
            "LX/1vg;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1827973
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->b:LX/0Or;

    .line 1827974
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->c:LX/17W;

    .line 1827975
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->d:LX/0bH;

    .line 1827976
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->e:LX/189;

    .line 1827977
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->f:LX/2g9;

    .line 1827978
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->g:LX/BsZ;

    .line 1827979
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->h:LX/1vg;

    .line 1827980
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;
    .locals 14

    .prologue
    .line 1827981
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;

    monitor-enter v1

    .line 1827982
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827983
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827984
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827985
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827986
    new-instance v3, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;

    const/16 v4, 0x509

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v6

    check-cast v6, LX/0bH;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v7

    check-cast v7, LX/189;

    invoke-static {v0}, LX/2g9;->a(LX/0QB;)LX/2g9;

    move-result-object v8

    check-cast v8, LX/2g9;

    .line 1827987
    new-instance v13, LX/BsZ;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v9

    check-cast v9, LX/0bH;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v10

    check-cast v10, LX/189;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v12

    check-cast v12, LX/1Ck;

    invoke-direct {v13, v9, v10, v11, v12}, LX/BsZ;-><init>(LX/0bH;LX/189;LX/0tX;LX/1Ck;)V

    .line 1827988
    move-object v9, v13

    .line 1827989
    check-cast v9, LX/BsZ;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v10

    check-cast v10, LX/1vg;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;-><init>(LX/0Or;LX/17W;LX/0bH;LX/189;LX/2g9;LX/BsZ;LX/1vg;)V

    .line 1827990
    move-object v0, v3

    .line 1827991
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827992
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/FundraiserUpsellHeaderComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827993
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827994
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
