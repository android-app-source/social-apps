.class public Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pd;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Ljava/lang/Void;",
        "TE;",
        "Lcom/facebook/widget/springbutton/SpringScaleButton;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/1yo;

.field private final b:Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;


# direct methods
.method public constructor <init>(LX/1yo;Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827204
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1827205
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;->a:LX/1yo;

    .line 1827206
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;

    .line 1827207
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;
    .locals 5

    .prologue
    .line 1827193
    const-class v1, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;

    monitor-enter v1

    .line 1827194
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827195
    sput-object v2, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827196
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827197
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827198
    new-instance p0, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;

    invoke-static {v0}, LX/1yo;->b(LX/0QB;)LX/1yo;

    move-result-object v3

    check-cast v3, LX/1yo;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;-><init>(LX/1yo;Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;)V

    .line 1827199
    move-object v0, p0

    .line 1827200
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827201
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827202
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827203
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1827184
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pd;

    const/4 v7, 0x0

    .line 1827185
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1827186
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827187
    invoke-static {p2}, LX/1yo;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v1

    .line 1827188
    if-nez v1, :cond_0

    .line 1827189
    :goto_0
    return-object v7

    .line 1827190
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryActionLink;->as()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    .line 1827191
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v2

    .line 1827192
    iget-object v3, p0, Lcom/facebook/feed/rows/sections/header/LikePageButtonPartDefinition;->b:Lcom/facebook/feed/rows/sections/header/SpringScaleButtonPartDefinition;

    new-instance v4, LX/BsD;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v5

    new-instance v6, LX/BsA;

    invoke-direct {v6, p2, v1, p3}, LX/BsA;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLPage;LX/1Pd;)V

    invoke-direct {v4, v0, v5, v6, v2}, LX/BsD;-><init>(LX/0jW;Ljava/lang/String;Landroid/view/View$OnClickListener;Z)V

    invoke-interface {p1, v3, v4}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    goto :goto_0
.end method
