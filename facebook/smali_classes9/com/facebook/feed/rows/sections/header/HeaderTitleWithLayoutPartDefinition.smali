.class public Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "LX/Bs9;",
        "Ljava/lang/Boolean;",
        "TE;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static i:LX/0Xm;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xc;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BsK;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bs7;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1DR;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/spannable/SpannablePartDefinition",
            "<TE;>;>;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1xv;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0ad;

.field private final h:LX/1VE;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/1VE;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/1xc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BsK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Bs7;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1DR;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/spannable/SpannablePartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1xv;",
            ">;",
            "LX/0ad;",
            "LX/1VE;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1827136
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1827137
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->a:LX/0Ot;

    .line 1827138
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->b:LX/0Ot;

    .line 1827139
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->c:LX/0Ot;

    .line 1827140
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->d:LX/0Ot;

    .line 1827141
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->e:LX/0Ot;

    .line 1827142
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->f:LX/0Ot;

    .line 1827143
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->g:LX/0ad;

    .line 1827144
    iput-object p8, p0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->h:LX/1VE;

    .line 1827145
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;
    .locals 12

    .prologue
    .line 1827146
    const-class v1, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    monitor-enter v1

    .line 1827147
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1827148
    sput-object v2, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1827149
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1827150
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1827151
    new-instance v3, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;

    const/16 v4, 0x959

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1d2e

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x1d27

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x78b

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x218e

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x903

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v10

    check-cast v10, LX/0ad;

    invoke-static {v0}, LX/1VE;->a(LX/0QB;)LX/1VE;

    move-result-object v11

    check-cast v11, LX/1VE;

    invoke-direct/range {v3 .. v11}, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0ad;LX/1VE;)V

    .line 1827152
    move-object v0, v3

    .line 1827153
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1827154
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1827155
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1827156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1aD;LX/Bs9;LX/1Pr;)Ljava/lang/Boolean;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1aD",
            "<TE;>;",
            "LX/Bs9;",
            "TE;)",
            "Ljava/lang/Boolean;"
        }
    .end annotation

    .prologue
    .line 1827157
    move-object/from16 v1, p3

    check-cast v1, LX/1Pn;

    invoke-interface {v1}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f01029b

    const v3, 0x7f0a010c

    invoke-static {v1, v2, v3}, LX/0WH;->b(Landroid/content/Context;II)I

    move-result v10

    .line 1827158
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    move-object v15, v1

    check-cast v15, LX/1Nt;

    new-instance v1, LX/1xy;

    move-object/from16 v0, p2

    iget-boolean v2, v0, LX/Bs9;->b:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/BsK;

    invoke-virtual {v2}, LX/BsK;->a()LX/1nq;

    move-result-object v2

    :goto_0
    move-object/from16 v0, p2

    iget-object v3, v0, LX/Bs9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v4, p3

    check-cast v4, LX/1Pk;

    invoke-interface {v4}, LX/1Pk;->e()LX/1SX;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1xc;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->c:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Bs7;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->d:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/1DR;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->f:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1xv;

    move-object/from16 v0, p2

    iget v9, v0, LX/Bs9;->c:I

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->h:LX/1VE;

    move-object/from16 v0, p2

    iget-object v13, v0, LX/Bs9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object/from16 v11, p3

    check-cast v11, LX/1Po;

    invoke-interface {v11}, LX/1Po;->c()LX/1PT;

    move-result-object v11

    invoke-virtual {v12, v13, v11}, LX/1VE;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1PT;)Z

    move-result v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    check-cast p3, LX/1Po;

    invoke-interface/range {p3 .. p3}, LX/1Po;->c()LX/1PT;

    move-result-object v14

    invoke-direct/range {v1 .. v14}, LX/1xy;-><init>(LX/1nq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1SX;LX/1xc;LX/Bs7;LX/1DR;LX/1xv;IIZZZLX/1PT;)V

    move-object/from16 v0, p1

    invoke-interface {v0, v15, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1827159
    move-object/from16 v0, p2

    iget-object v1, v0, LX/Bs9;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {v1}, LX/182;->s(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    return-object v1

    .line 1827160
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1827161
    check-cast p2, LX/Bs9;

    check-cast p3, LX/1Pr;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/header/HeaderTitleWithLayoutPartDefinition;->a(LX/1aD;LX/Bs9;LX/1Pr;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x63e27383

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1827162
    check-cast p2, Ljava/lang/Boolean;

    .line 1827163
    const v1, 0x7f0d0081

    invoke-virtual {p4, v1, p2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1827164
    const/16 v1, 0x1f

    const v2, 0x286202e5

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1827165
    const v0, 0x7f0d0081

    const/4 v1, 0x0

    invoke-virtual {p4, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1827166
    return-void
.end method
