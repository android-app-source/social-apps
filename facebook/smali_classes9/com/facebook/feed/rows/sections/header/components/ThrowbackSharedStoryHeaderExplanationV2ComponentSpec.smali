.class public Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field private final b:LX/1nu;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1828300
    const-class v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1vg;LX/0Ot;LX/1nu;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1vg;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/1nu;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828301
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828302
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->b:LX/1nu;

    .line 1828303
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->d:LX/1vg;

    .line 1828304
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->c:LX/0Ot;

    .line 1828305
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;
    .locals 6

    .prologue
    .line 1828306
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;

    monitor-enter v1

    .line 1828307
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828308
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828309
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828310
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828311
    new-instance v5, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    const/16 v4, 0x2eb

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v4

    check-cast v4, LX/1nu;

    invoke-direct {v5, v3, p0, v4}, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;-><init>(LX/1vg;LX/0Ot;LX/1nu;)V

    .line 1828312
    move-object v0, v5

    .line 1828313
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828314
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828315
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828316
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/1Dg;
    .locals 13
    .param p2    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    const/4 v12, 0x6

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    .line 1828317
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1828318
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1828319
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->o()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v2

    .line 1828320
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->n()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v3

    .line 1828321
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1828322
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->aL()Lcom/facebook/graphql/model/GraphQLStoryHeader;

    move-result-object v0

    const/4 v4, 0x0

    .line 1828323
    if-nez v0, :cond_0

    move-object v1, v4

    .line 1828324
    :goto_0
    move-object v4, v1

    .line 1828325
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1828326
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLImage;

    .line 1828327
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLImage;

    .line 1828328
    iget-object v5, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->d:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v5

    const v6, 0x7f0a00a3

    invoke-virtual {v5, v6}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    const v6, 0x7f0207ed

    invoke-virtual {v5, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    .line 1828329
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->k()Ljava/lang/String;

    move-result-object v4

    const/16 v6, 0x10

    invoke-static {v4, v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v6

    long-to-int v4, v6

    .line 1828330
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v7

    const v8, 0x7f0a00e9

    invoke-virtual {v7, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-interface {v7, v9}, LX/1Di;->r(I)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v7

    invoke-interface {v7, v4}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v4

    const/16 v7, 0x32

    invoke-interface {v4, v7}, LX/1Dh;->K(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v11, v12}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/16 v7, 0x14

    invoke-interface {v4, v10, v7}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    const/4 v7, 0x3

    invoke-interface {v4, v7}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-interface {v4, v7}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object v4

    iget-object v7, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->b:LX/1nu;

    invoke-virtual {v7, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v7

    sget-object v8, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v7, v8}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v8

    invoke-interface {v7, v8}, LX/1Di;->o(I)LX/1Di;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v0

    invoke-interface {v7, v0}, LX/1Di;->g(I)LX/1Di;

    move-result-object v0

    invoke-interface {v4, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    iget-object v4, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->b:LX/1nu;

    invoke-virtual {v4, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v4

    sget-object v7, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationV2ComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v7}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v4, v7}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v7

    invoke-interface {v4, v7}, LX/1Di;->o(I)LX/1Di;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    invoke-interface {v4, v1}, LX/1Di;->g(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v6, v0}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v1

    const v4, 0x7f0a00e9

    invoke-virtual {v1, v4}, LX/25Q;->i(I)LX/25Q;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1, v9}, LX/1Di;->r(I)LX/1Di;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v11}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v10}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v9}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v1

    const/16 v4, 0x64

    invoke-interface {v1, v12, v4}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v9}, LX/1Dh;->C(I)LX/1Dh;

    move-result-object v1

    const/16 v4, 0x8

    invoke-interface {v1, v4, v11}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    const v4, 0x7f0b004f

    invoke-virtual {v2, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/1ne;->t(I)LX/1ne;

    move-result-object v2

    const v4, 0x7f0a00a7

    invoke-virtual {v2, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v2, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v2

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v2, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v10}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v10}, LX/1Dh;->T(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v9}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    const/16 v4, 0xa

    invoke-interface {v2, v12, v4}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b004c

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v4, 0x7f0a00a7

    invoke-virtual {v3, v4}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v3, v4}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v3

    sget-object v4, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v3, v4}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    invoke-interface {v3, v9, v10}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    .line 1828331
    const v3, -0x15e5a53a

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 1828332
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v0, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0

    .line 1828333
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryHeader;->p()LX/0Px;

    move-result-object v6

    .line 1828334
    const/4 v1, 0x0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v1

    :goto_1
    if-ge v5, v7, :cond_2

    .line 1828335
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 1828336
    if-eqz v1, :cond_1

    const-string v8, "GoodwillThrowbackSharedStoryHeaderStyleInfo"

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1828337
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryHeaderStyleInfo;

    goto/16 :goto_0

    .line 1828338
    :cond_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    :cond_2
    move-object v1, v4

    .line 1828339
    goto/16 :goto_0
.end method
