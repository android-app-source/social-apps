.class public Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final d:Lcom/facebook/common/callercontext/CallerContext;

.field private static e:LX/0Xm;


# instance fields
.field public final a:LX/1nu;

.field public final b:LX/17W;

.field public final c:LX/1vg;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1828250
    const-class v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;

    const-string v1, "goodwill_throwback"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->d:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(LX/1vg;LX/17W;LX/1nu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1828251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1828252
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->a:LX/1nu;

    .line 1828253
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->c:LX/1vg;

    .line 1828254
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->b:LX/17W;

    .line 1828255
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;
    .locals 6

    .prologue
    .line 1828256
    const-class v1, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;

    monitor-enter v1

    .line 1828257
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1828258
    sput-object v2, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1828259
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1828260
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1828261
    new-instance p0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;

    invoke-static {v0}, LX/1vg;->a(LX/0QB;)LX/1vg;

    move-result-object v3

    check-cast v3, LX/1vg;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, LX/1nu;->a(LX/0QB;)LX/1nu;

    move-result-object v5

    check-cast v5, LX/1nu;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;-><init>(LX/1vg;LX/17W;LX/1nu;)V

    .line 1828262
    move-object v0, p0

    .line 1828263
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1828264
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/header/components/ThrowbackSharedStoryHeaderExplanationComponentSpec;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1828265
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1828266
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
