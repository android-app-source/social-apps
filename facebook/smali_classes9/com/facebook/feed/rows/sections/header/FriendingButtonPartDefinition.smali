.class public Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pc;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        "TE;",
        "Lcom/facebook/friends/ui/FriendingButton;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Zb;

.field private final b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final c:LX/2do;

.field public final d:LX/17Q;

.field private final e:LX/0ad;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/2do;LX/17Q;LX/0ad;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "Lcom/facebook/multirow/parts/ClickListenerPartDefinition;",
            "LX/2do;",
            "LX/17Q;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/1My;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826976
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1826977
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->a:LX/0Zb;

    .line 1826978
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1826979
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->c:LX/2do;

    .line 1826980
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->d:LX/17Q;

    .line 1826981
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->e:LX/0ad;

    .line 1826982
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->f:LX/0Ot;

    .line 1826983
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;LX/17Q;LX/0Zb;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "LX/1Pc;",
            ":",
            "LX/1Pq;",
            ":",
            "LX/1Pr;",
            ">(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;",
            "LX/17Q;",
            "LX/0Zb;",
            ")V"
        }
    .end annotation

    .prologue
    .line 1826984
    iget-object v0, p0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1826985
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1826986
    invoke-static {v0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    .line 1826987
    new-instance v6, LX/33P;

    invoke-direct {v6, v2}, LX/33P;-><init>(Lcom/facebook/graphql/model/GraphQLProfile;)V

    move-object v1, p1

    .line 1826988
    check-cast v1, LX/1Pr;

    invoke-interface {v1, v6, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 1826989
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->D()Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/2h7;->FEED_FRIENDABLE_HEADER:LX/2h7;

    new-instance v5, LX/Bs2;

    invoke-direct {v5, p1, v6, v4, p0}, LX/Bs2;-><init>(LX/1Pc;LX/33P;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    move-object v0, p1

    invoke-interface/range {v0 .. v5}, LX/1Pc;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v1

    .line 1826990
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v4}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826991
    invoke-static {p0}, LX/181;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/162;

    move-result-object v0

    .line 1826992
    invoke-static {v0}, LX/17Q;->F(LX/0lF;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1826993
    const/4 v2, 0x0

    .line 1826994
    :goto_0
    move-object v0, v2

    .line 1826995
    invoke-interface {p3, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    :cond_0
    move-object v0, p1

    .line 1826996
    check-cast v0, LX/1Pr;

    iget-object v1, v1, LX/5Oh;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-interface {v0, v6, v1}, LX/1Pr;->a(LX/1KL;Ljava/lang/Object;)Z

    .line 1826997
    check-cast p1, LX/1Pq;

    const/4 v0, 0x1

    new-array v0, v0, [Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    invoke-interface {p1, v0}, LX/1Pq;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 1826998
    return-void

    .line 1826999
    :cond_1
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "feed_friendable_header_add"

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "tracking"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "native_newsfeed"

    .line 1827000
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 1827001
    move-object v2, v2

    .line 1827002
    goto :goto_0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 1827003
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pc;

    .line 1827004
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1827005
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1827006
    invoke-static {v0}, LX/25D;->a(LX/16q;)Lcom/facebook/graphql/model/GraphQLProfile;

    move-result-object v2

    .line 1827007
    if-nez v2, :cond_0

    .line 1827008
    const/4 v0, 0x0

    .line 1827009
    :goto_0
    return-object v0

    .line 1827010
    :cond_0
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->b:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1827011
    new-instance v3, LX/Bs3;

    invoke-direct {v3, p0, p2, p3}, LX/Bs3;-><init>(Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pc;)V

    move-object v3, v3

    .line 1827012
    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1827013
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->e:LX/0ad;

    sget-short v3, LX/2ez;->l:S

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1827014
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v4, 0x0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/1My;->a(LX/0jT;LX/0ta;JLjava/util/Set;)Lcom/facebook/graphql/executor/GraphQLResult;

    move-result-object v4

    .line 1827015
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1My;

    new-instance v5, LX/Bs5;

    move-object v3, p3

    check-cast v3, LX/1Pq;

    invoke-direct {v5, p2, v3}, LX/Bs5;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pq;)V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLProfile;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v5, v3, v4}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 1827016
    :goto_1
    check-cast p3, LX/1Pr;

    new-instance v1, LX/33P;

    invoke-direct {v1, v2}, LX/33P;-><init>(Lcom/facebook/graphql/model/GraphQLProfile;)V

    invoke-interface {p3, v1, v0}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 1827017
    :cond_1
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/header/FriendingButtonPartDefinition;->c:LX/2do;

    new-instance v3, LX/Bs4;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    new-instance v5, Ljava/lang/ref/WeakReference;

    invoke-direct {v5, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-direct {v3, v4, v5}, LX/Bs4;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/ref/WeakReference;)V

    invoke-virtual {v1, v3}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x3596a3ee    # -3823364.5f

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1827018
    check-cast p2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    check-cast p4, Lcom/facebook/friends/ui/FriendingButton;

    .line 1827019
    invoke-virtual {p4, p2}, Lcom/facebook/friends/ui/FriendingButton;->a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 1827020
    const/16 v1, 0x1f

    const v2, 0x235468b1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
