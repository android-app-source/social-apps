.class public Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/5Or;

.field private final e:LX/Brl;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Brl;LX/5Or;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826607
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1826608
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->e:LX/Brl;

    .line 1826609
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->d:LX/5Or;

    .line 1826610
    return-void
.end method

.method private a(LX/1De;)LX/1X1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1826597
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->e:LX/Brl;

    const/4 v1, 0x0

    .line 1826598
    new-instance v2, LX/Brk;

    invoke-direct {v2, v0}, LX/Brk;-><init>(LX/Brl;)V

    .line 1826599
    sget-object p0, LX/Brl;->a:LX/0Zi;

    invoke-virtual {p0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Brj;

    .line 1826600
    if-nez p0, :cond_0

    .line 1826601
    new-instance p0, LX/Brj;

    invoke-direct {p0}, LX/Brj;-><init>()V

    .line 1826602
    :cond_0
    invoke-static {p0, p1, v1, v1, v2}, LX/Brj;->a$redex0(LX/Brj;LX/1De;IILX/Brk;)V

    .line 1826603
    move-object v2, p0

    .line 1826604
    move-object v1, v2

    .line 1826605
    move-object v0, v1

    .line 1826606
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;
    .locals 6

    .prologue
    .line 1826586
    const-class v1, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;

    monitor-enter v1

    .line 1826587
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826588
    sput-object v2, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826589
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826590
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826591
    new-instance p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/Brl;->a(LX/0QB;)LX/Brl;

    move-result-object v4

    check-cast v4, LX/Brl;

    invoke-static {v0}, LX/5Or;->a(LX/0QB;)LX/5Or;

    move-result-object v5

    check-cast v5, LX/5Or;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;-><init>(Landroid/content/Context;LX/Brl;LX/5Or;)V

    .line 1826592
    move-object v0, p0

    .line 1826593
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826594
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826595
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826596
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1826585
    invoke-direct {p0, p1}, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1826578
    invoke-direct {p0, p1}, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->a(LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 1826580
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->d:LX/5Or;

    .line 1826581
    iget-object v1, v0, LX/5Or;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1826582
    iget-object v1, v0, LX/5Or;->a:LX/0ad;

    sget-short p0, LX/1Dd;->D:S

    const/4 p1, 0x0

    invoke-interface {v1, p0, p1}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/5Or;->b:Ljava/lang/Boolean;

    .line 1826583
    :cond_0
    iget-object v1, v0, LX/5Or;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 1826584
    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1826579
    const/4 v0, 0x0

    return-object v0
.end method
