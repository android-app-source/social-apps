.class public Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/api/feed/data/LoadingMoreSentinel$LoadingMoreSentinelFeedUnit;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;

.field private final b:Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826625
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1826626
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;->a:Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;

    .line 1826627
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;->b:Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;

    .line 1826628
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;
    .locals 5

    .prologue
    .line 1826629
    const-class v1, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;

    monitor-enter v1

    .line 1826630
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826631
    sput-object v2, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826632
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826633
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826634
    new-instance p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;

    invoke-direct {p0, v3, v4}, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;-><init>(Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;)V

    .line 1826635
    move-object v0, p0

    .line 1826636
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826637
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826638
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826639
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1826640
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826641
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;->b:Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreComponentPartDefinition;

    invoke-static {p1, v0, p2}, LX/1RG;->a(LX/1RF;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMoreSelectorPartDefinition;->a:Lcom/facebook/feed/rows/sections/common/loadingmore/LoadingMorePartDefinition;

    invoke-virtual {v0, v1, p2}, LX/1RG;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;)LX/1RG;

    .line 1826642
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1826643
    const/4 v0, 0x1

    return v0
.end method
