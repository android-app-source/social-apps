.class public Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field public static final d:LX/1Cz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Cz",
            "<",
            "Lcom/facebook/components/feed/FeedComponentView;",
            ">;"
        }
    .end annotation
.end field

.field private static j:LX/0Xm;


# instance fields
.field private final e:LX/1V4;

.field private final f:LX/BqL;

.field private final g:LX/1V0;

.field private final h:LX/1VF;

.field private final i:LX/Bqk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1825200
    invoke-static {}, Lcom/facebook/components/feed/ComponentPartDefinition;->e()LX/1Cz;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->d:LX/1Cz;

    return-void
.end method

.method public constructor <init>(LX/1V4;LX/BqL;Landroid/content/Context;LX/1V0;LX/1VF;LX/Bqk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1825193
    invoke-direct {p0, p3}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1825194
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->e:LX/1V4;

    .line 1825195
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->f:LX/BqL;

    .line 1825196
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->g:LX/1V0;

    .line 1825197
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->h:LX/1VF;

    .line 1825198
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->i:LX/Bqk;

    .line 1825199
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1825160
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1825161
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825162
    invoke-static {v0}, LX/BqL;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1825163
    invoke-static {v0}, LX/BqL;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v2

    .line 1825164
    invoke-static {v0}, LX/BqL;->d(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v3

    .line 1825165
    invoke-static {v0}, LX/BqL;->h(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLAYMTTip;

    move-result-object v4

    .line 1825166
    if-nez v4, :cond_1

    const/4 v4, 0x0

    :goto_0
    move-object v4, v4

    .line 1825167
    iget-object v5, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->i:LX/Bqk;

    const/4 v6, 0x0

    .line 1825168
    new-instance v7, LX/Bqj;

    invoke-direct {v7, v5}, LX/Bqj;-><init>(LX/Bqk;)V

    .line 1825169
    iget-object v8, v5, LX/Bqk;->b:LX/0Zi;

    invoke-virtual {v8}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/Bqi;

    .line 1825170
    if-nez v8, :cond_0

    .line 1825171
    new-instance v8, LX/Bqi;

    invoke-direct {v8, v5}, LX/Bqi;-><init>(LX/Bqk;)V

    .line 1825172
    :cond_0
    invoke-static {v8, p1, v6, v6, v7}, LX/Bqi;->a$redex0(LX/Bqi;LX/1De;IILX/Bqj;)V

    .line 1825173
    move-object v7, v8

    .line 1825174
    move-object v6, v7

    .line 1825175
    move-object v5, v6

    .line 1825176
    iget-object v6, v5, LX/Bqi;->a:LX/Bqj;

    iput-object v1, v6, LX/Bqj;->a:Landroid/net/Uri;

    .line 1825177
    iget-object v6, v5, LX/Bqi;->e:Ljava/util/BitSet;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Ljava/util/BitSet;->set(I)V

    .line 1825178
    move-object v1, v5

    .line 1825179
    iget-object v5, v1, LX/Bqi;->a:LX/Bqj;

    iput-object v2, v5, LX/Bqj;->b:Ljava/lang/String;

    .line 1825180
    iget-object v5, v1, LX/Bqi;->e:Ljava/util/BitSet;

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Ljava/util/BitSet;->set(I)V

    .line 1825181
    move-object v1, v1

    .line 1825182
    iget-object v2, v1, LX/Bqi;->a:LX/Bqj;

    iput-object v3, v2, LX/Bqj;->c:Ljava/lang/String;

    .line 1825183
    iget-object v2, v1, LX/Bqi;->e:Ljava/util/BitSet;

    const/4 v5, 0x2

    invoke-virtual {v2, v5}, Ljava/util/BitSet;->set(I)V

    .line 1825184
    move-object v1, v1

    .line 1825185
    iget-object v2, v1, LX/Bqi;->a:LX/Bqj;

    iput-object v4, v2, LX/Bqj;->e:Ljava/lang/String;

    .line 1825186
    iget-object v2, v1, LX/Bqi;->e:Ljava/util/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1825187
    move-object v1, v1

    .line 1825188
    iget-object v2, v1, LX/Bqi;->a:LX/Bqj;

    iput-object v0, v2, LX/Bqj;->d:Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825189
    iget-object v2, v1, LX/Bqi;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1825190
    move-object v0, v1

    .line 1825191
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 1825192
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->g:LX/1V0;

    check-cast p3, LX/1Ps;

    new-instance v2, LX/1X6;

    sget-object v3, LX/1Ua;->a:LX/1Ua;

    const v4, 0x7f0a00ea

    const v5, 0x7f0a00ea

    invoke-direct {v2, p2, v3, v4, v5}, LX/1X6;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Ua;II)V

    invoke-virtual {v1, p1, p3, v2, v0}, LX/1V0;->a(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLAYMTTip;->k()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;
    .locals 10

    .prologue
    .line 1825149
    const-class v1, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;

    monitor-enter v1

    .line 1825150
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1825151
    sput-object v2, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1825152
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1825153
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1825154
    new-instance v3, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;

    invoke-static {v0}, LX/1V4;->a(LX/0QB;)LX/1V4;

    move-result-object v4

    check-cast v4, LX/1V4;

    invoke-static {v0}, LX/BqL;->a(LX/0QB;)LX/BqL;

    move-result-object v5

    check-cast v5, LX/BqL;

    const-class v6, Landroid/content/Context;

    invoke-interface {v0, v6}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/Context;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v7

    check-cast v7, LX/1V0;

    invoke-static {v0}, LX/1VF;->a(LX/0QB;)LX/1VF;

    move-result-object v8

    check-cast v8, LX/1VF;

    invoke-static {v0}, LX/Bqk;->a(LX/0QB;)LX/Bqk;

    move-result-object v9

    check-cast v9, LX/Bqk;

    invoke-direct/range {v3 .. v9}, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;-><init>(LX/1V4;LX/BqL;Landroid/content/Context;LX/1V0;LX/1VF;LX/Bqk;)V

    .line 1825155
    move-object v0, v3

    .line 1825156
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1825157
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1825158
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1825159
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1825201
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1825148
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 1825140
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825141
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1825142
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1825143
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->h:LX/1VF;

    invoke-virtual {v1, v0}, LX/1VF;->c(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, LX/BqL;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->f:LX/BqL;

    const-string v2, "post_footer"

    invoke-virtual {v1, v0, v2}, LX/BqL;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->f:LX/BqL;

    const-string v2, "post_footer"

    invoke-virtual {v1, v0, v2}, LX/BqL;->b(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->f:LX/BqL;

    const-string v2, "post_footer"

    invoke-virtual {v1, v0, v2}, LX/BqL;->c(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/rows/sections/StoryAYMTPagePostFooterNativeChannelComponentPartDefinition;->e:LX/1V4;

    .line 1825144
    iget-object v1, v0, LX/1V4;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1825145
    iget-object v1, v0, LX/1V4;->a:LX/0ad;

    sget-short v2, LX/1Dd;->G:S

    const/4 p0, 0x0

    invoke-interface {v1, v2, p0}, LX/0ad;->a(SZ)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, LX/1V4;->e:Ljava/lang/Boolean;

    .line 1825146
    :cond_0
    iget-object v1, v0, LX/1V4;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v0, v1

    .line 1825147
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1825138
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1825139
    invoke-static {p1}, LX/1V3;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/0jW;

    move-result-object v0

    return-object v0
.end method
