.class public Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;
.super Lcom/facebook/widget/text/BetterTextView;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1826472
    invoke-direct {p0, p1}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;)V

    .line 1826473
    invoke-direct {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a()V

    .line 1826474
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1826490
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1826491
    invoke-direct {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a()V

    .line 1826492
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1826493
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/text/BetterTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1826494
    invoke-direct {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a()V

    .line 1826495
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1826488
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    .line 1826489
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1826475
    invoke-virtual {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->getWidth()I

    move-result v6

    .line 1826476
    invoke-virtual {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->getHeight()I

    move-result v7

    .line 1826477
    invoke-virtual {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/lit8 v8, v0, 0x1

    .line 1826478
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1826479
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1826480
    int-to-float v3, v6

    int-to-float v4, v7

    iget-object v5, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1826481
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 1826482
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1826483
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1826484
    int-to-float v2, v8

    int-to-float v3, v6

    int-to-float v4, v8

    iget-object v5, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1826485
    int-to-float v3, v6

    int-to-float v4, v7

    iget-object v5, p0, Lcom/facebook/feed/rows/sections/attachments/ui/ScoreCardTextView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1826486
    invoke-super {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1826487
    return-void
.end method
