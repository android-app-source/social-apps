.class public Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;
.super Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1Pf;",
        ">;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private a:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

.field private b:Lcom/facebook/feed/rows/sections/attachments/FileUploadAttachmentPartDefinition;

.field public c:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/sections/attachments/FileUploadAttachmentPartDefinition;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826163
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseMultiRowGroupPartDefinition;-><init>()V

    .line 1826164
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    .line 1826165
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/sections/attachments/FileUploadAttachmentPartDefinition;

    .line 1826166
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->c:LX/0Uh;

    .line 1826167
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 1826184
    const-class v1, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;

    monitor-enter v1

    .line 1826185
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826186
    sput-object v2, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826187
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826188
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826189
    new-instance p0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-static {v0}, Lcom/facebook/feed/rows/sections/attachments/FileUploadAttachmentPartDefinition;->a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/FileUploadAttachmentPartDefinition;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/rows/sections/attachments/FileUploadAttachmentPartDefinition;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;-><init>(Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;Lcom/facebook/feed/rows/sections/attachments/FileUploadAttachmentPartDefinition;LX/0Uh;)V

    .line 1826190
    move-object v0, p0

    .line 1826191
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826192
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826193
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1RF;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 1826169
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826170
    const/4 p3, 0x0

    .line 1826171
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->c:LX/0Uh;

    const/16 v1, 0x84

    invoke-virtual {v0, v1, p3}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1826172
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1826173
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1826174
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v0

    .line 1826175
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 1826176
    invoke-virtual {v0, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1826177
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v1

    sget-object p3, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->ALBUM:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v1, p3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1826178
    :goto_0
    move-object v0, v0

    .line 1826179
    if-eqz v0, :cond_0

    .line 1826180
    invoke-virtual {p2, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 1826181
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->a:Lcom/facebook/feedplugins/attachments/collage/CollageAttachmentPartDefinition;

    invoke-virtual {p1, v1, v0}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1826182
    :cond_0
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/RichPreviewFileUploadAttachmentPartDefinition;->b:Lcom/facebook/feed/rows/sections/attachments/FileUploadAttachmentPartDefinition;

    invoke-virtual {p1, v0, p2}, LX/1RF;->a(Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;Ljava/lang/Object;)Z

    .line 1826183
    const/4 v0, 0x0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1826168
    const/4 v0, 0x1

    return v0
.end method
