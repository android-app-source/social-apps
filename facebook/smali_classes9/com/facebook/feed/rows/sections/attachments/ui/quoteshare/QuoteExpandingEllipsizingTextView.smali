.class public Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;
.super Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;
.source ""


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1710746
    invoke-direct {p0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;-><init>(Landroid/content/Context;)V

    .line 1710747
    invoke-direct {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->a()V

    .line 1710748
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1710749
    invoke-direct {p0, p1, p2}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1710750
    invoke-direct {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->a()V

    .line 1710751
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 1710752
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1710753
    invoke-direct {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->a()V

    .line 1710754
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1710755
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->a:Landroid/graphics/Paint;

    .line 1710756
    invoke-virtual {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a009c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->b:I

    .line 1710757
    invoke-virtual {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b10a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->c:I

    .line 1710758
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->setMaxLines(I)V

    .line 1710759
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1710760
    invoke-super {p0, p1}, Lcom/facebook/resources/ui/ExpandingEllipsizingTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 1710761
    invoke-virtual {p0}, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->getHeight()I

    move-result v0

    .line 1710762
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->a:Landroid/graphics/Paint;

    iget v3, p0, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->b:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1710763
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->a:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1710764
    iget v2, p0, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->c:I

    add-int/lit8 v2, v2, 0x0

    int-to-float v3, v2

    int-to-float v4, v0

    iget-object v5, p0, Lcom/facebook/feed/rows/sections/attachments/ui/quoteshare/QuoteExpandingEllipsizingTextView;->a:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 1710765
    return-void
.end method
