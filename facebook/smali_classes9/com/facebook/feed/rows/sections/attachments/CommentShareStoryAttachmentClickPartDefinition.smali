.class public Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;
.super Lcom/facebook/multirow/api/BaseSinglePartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/multirow/api/BaseSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "Ljava/lang/Void;",
        "LX/1PW;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

.field private final b:LX/0hy;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0Zb;


# direct methods
.method public constructor <init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826062
    invoke-direct {p0}, Lcom/facebook/multirow/api/BaseSinglePartDefinition;-><init>()V

    .line 1826063
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    .line 1826064
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->b:LX/0hy;

    .line 1826065
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1826066
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->d:LX/0Zb;

    .line 1826067
    return-void
.end method

.method public static a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/0Zb;)Landroid/view/View$OnClickListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;",
            "LX/0hy;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Zb;",
            ")",
            "Landroid/view/View$OnClickListener;"
        }
    .end annotation

    .prologue
    .line 1826068
    new-instance v0, LX/BrC;

    invoke-direct {v0, p0, p1, p3, p2}, LX/BrC;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0hy;LX/0Zb;Lcom/facebook/content/SecureContextHelper;)V

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;
    .locals 7

    .prologue
    .line 1826069
    const-class v1, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;

    monitor-enter v1

    .line 1826070
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826071
    sput-object v2, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826072
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826073
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826074
    new-instance p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;

    invoke-static {v0}, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;->a(LX/0QB;)Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    move-result-object v3

    check-cast v3, Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v4

    check-cast v4, LX/0hy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v5

    check-cast v5, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v6

    check-cast v6, LX/0Zb;

    invoke-direct {p0, v3, v4, v5, v6}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;-><init>(Lcom/facebook/multirow/parts/ClickListenerPartDefinition;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/0Zb;)V

    .line 1826075
    move-object v0, p0

    .line 1826076
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826077
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826078
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826079
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 1826080
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826081
    iget-object v0, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->a:Lcom/facebook/multirow/parts/ClickListenerPartDefinition;

    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->b:LX/0hy;

    iget-object v2, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->d:LX/0Zb;

    invoke-static {p2, v1, v2, v3}, Lcom/facebook/feed/rows/sections/attachments/CommentShareStoryAttachmentClickPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/0Zb;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-interface {p1, v0, v1}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826082
    const/4 v0, 0x0

    return-object v0
.end method
