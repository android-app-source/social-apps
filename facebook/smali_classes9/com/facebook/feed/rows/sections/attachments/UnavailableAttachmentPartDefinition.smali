.class public Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;
.super Lcom/facebook/components/feed/ComponentPartDefinition;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "Landroid/view/View;",
        ":",
        "LX/2yX;",
        ":",
        "LX/2yW;",
        "E::",
        "LX/1Pn;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ">",
        "Lcom/facebook/components/feed/ComponentPartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;TE;>;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final d:LX/BrH;

.field private final e:LX/1V0;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BrH;LX/1V0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826301
    invoke-direct {p0, p1}, Lcom/facebook/components/feed/ComponentPartDefinition;-><init>(Landroid/content/Context;)V

    .line 1826302
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->d:LX/BrH;

    .line 1826303
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->e:LX/1V0;

    .line 1826304
    return-void
.end method

.method private a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
            ">;TE;)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 1826286
    const v0, 0x7f020a43

    invoke-static {p2, v0}, Lcom/facebook/feedplugins/attachments/linkshare/BaseShareAttachmentPartDefinition;->a(Lcom/facebook/feed/rows/core/props/FeedProps;I)LX/1X6;

    move-result-object v0

    move-object v0, v0

    .line 1826287
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->d:LX/BrH;

    const/4 v2, 0x0

    .line 1826288
    new-instance v3, LX/BrG;

    invoke-direct {v3, v1}, LX/BrG;-><init>(LX/BrH;)V

    .line 1826289
    sget-object v4, LX/BrH;->a:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/BrF;

    .line 1826290
    if-nez v4, :cond_0

    .line 1826291
    new-instance v4, LX/BrF;

    invoke-direct {v4}, LX/BrF;-><init>()V

    .line 1826292
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/BrF;->a$redex0(LX/BrF;LX/1De;IILX/BrG;)V

    .line 1826293
    move-object v3, v4

    .line 1826294
    move-object v2, v3

    .line 1826295
    move-object v1, v2

    .line 1826296
    iget-object v2, v1, LX/BrF;->a:LX/BrG;

    iput-object p2, v2, LX/BrG;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826297
    iget-object v2, v1, LX/BrF;->d:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 1826298
    move-object v1, v1

    .line 1826299
    invoke-virtual {v1}, LX/1X5;->d()LX/1X1;

    move-result-object v1

    .line 1826300
    iget-object v2, p0, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->e:LX/1V0;

    check-cast p3, LX/1Ps;

    invoke-virtual {v2, p1, p3, v0, v1}, LX/1V0;->b(LX/1De;LX/1Ps;LX/1X6;LX/1X1;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;
    .locals 6

    .prologue
    .line 1826275
    const-class v1, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;

    monitor-enter v1

    .line 1826276
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826277
    sput-object v2, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826278
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826279
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826280
    new-instance p0, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/BrH;->a(LX/0QB;)LX/BrH;

    move-result-object v4

    check-cast v4, LX/BrH;

    invoke-static {v0}, LX/1V0;->a(LX/0QB;)LX/1V0;

    move-result-object v5

    check-cast v5, LX/1V0;

    invoke-direct {p0, v3, v4, v5}, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;-><init>(Landroid/content/Context;LX/BrH;LX/1V0;)V

    .line 1826281
    move-object v0, p0

    .line 1826282
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826283
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826284
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826285
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1PW;)LX/1X1;
    .locals 1

    .prologue
    .line 1826274
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    check-cast p3, LX/1Pn;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(LX/1De;Ljava/lang/Object;LX/1Pn;)LX/1X1;
    .locals 1

    .prologue
    .line 1826273
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/feed/rows/sections/attachments/UnavailableAttachmentPartDefinition;->a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1826270
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;)LX/0jW;
    .locals 1

    .prologue
    .line 1826271
    check-cast p1, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 1826272
    invoke-static {p1}, LX/1WF;->f(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    return-object v0
.end method
