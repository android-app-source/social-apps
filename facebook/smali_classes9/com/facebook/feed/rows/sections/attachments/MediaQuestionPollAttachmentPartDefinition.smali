.class public Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;
.super Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pq;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pn;",
        "V:",
        "Landroid/view/View;",
        ":",
        "LX/2yX;",
        ":",
        "LX/35q;",
        ":",
        "LX/2yW;",
        ">",
        "Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition",
        "<",
        "Lcom/facebook/feed/rows/core/props/FeedProps",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;",
        "LX/BrE;",
        "TE;TV;>;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# static fields
.field private static final a:Lcom/facebook/common/callercontext/CallerContext;

.field private static i:LX/0Xm;


# instance fields
.field public final b:Landroid/content/res/Resources;

.field public final c:LX/1Uj;

.field private final d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition",
            "<TV;>;"
        }
    .end annotation
.end field

.field private final f:LX/1Ad;

.field private final g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1826115
    const-class v0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;

    const-string v1, "newsfeed_angora_attachment_view"

    invoke-static {v0, v1}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/1Ad;LX/1Uj;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1826116
    invoke-direct {p0}, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;-><init>()V

    .line 1826117
    iput-object p1, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    .line 1826118
    iput-object p3, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->c:LX/1Uj;

    .line 1826119
    iput-object p4, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    .line 1826120
    iput-object p5, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    .line 1826121
    iput-object p2, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->f:LX/1Ad;

    .line 1826122
    iput-object p6, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    .line 1826123
    iput-object p7, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    .line 1826124
    return-void
.end method

.method public static a(LX/0QB;)Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;
    .locals 11

    .prologue
    .line 1826125
    const-class v1, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;

    monitor-enter v1

    .line 1826126
    :try_start_0
    sget-object v0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->i:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 1826127
    sput-object v2, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->i:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1826128
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1826129
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 1826130
    new-instance v3, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/1Ad;->b(LX/0QB;)LX/1Ad;

    move-result-object v5

    check-cast v5, LX/1Ad;

    invoke-static {v0}, LX/1Uj;->a(LX/0QB;)LX/1Uj;

    move-result-object v6

    check-cast v6, LX/1Uj;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    move-result-object v7

    check-cast v7, Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    move-result-object v8

    check-cast v8, Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    move-result-object v9

    check-cast v9, Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    invoke-static {v0}, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;->a(LX/0QB;)Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    move-result-object v10

    check-cast v10, Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-direct/range {v3 .. v10}, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;-><init>(Landroid/content/res/Resources;LX/1Ad;LX/1Uj;Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;)V

    .line 1826131
    move-object v0, v3

    .line 1826132
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 1826133
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1826134
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 1826135
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/1Cz;
    .locals 1

    .prologue
    .line 1826136
    sget-object v0, Lcom/facebook/attachments/angora/AngoraAttachmentView;->a:LX/1Cz;

    return-object v0
.end method

.method public final a(LX/1aD;Ljava/lang/Object;LX/1PW;)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 1826137
    check-cast p2, Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 1826138
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 1826139
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 1826140
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->e:Lcom/facebook/feedplugins/attachments/linkshare/base/AttachmentLabelPartDefinition;

    new-instance v2, LX/2yZ;

    .line 1826141
    iget-object v4, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->b:Landroid/content/res/Resources;

    const v5, 0x7f081022

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 1826142
    invoke-static {v4}, Landroid/text/SpannableStringBuilder;->valueOf(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    .line 1826143
    iget-object v7, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->c:LX/1Uj;

    invoke-virtual {v7}, LX/1Uj;->a()Landroid/text/style/MetricAffectingSpan;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v4}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    const/16 p3, 0x11

    invoke-virtual {v5, v7, v8, v4, p3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 1826144
    move-object v4, v5

    .line 1826145
    invoke-direct {v2, v4, v3}, LX/2yZ;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826146
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->h:Lcom/facebook/feedplugins/attachments/linkshare/AngoraAttachmentBackgroundPartDefinition;

    invoke-interface {p1, v1, p2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826147
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->g:Lcom/facebook/feedplugins/attachments/linkshare/AttachmentLinkPartDefinition;

    new-instance v2, LX/2ya;

    invoke-direct {v2, p2}, LX/2ya;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;)V

    invoke-interface {p1, v1, v2}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826148
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fr()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fr()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fr()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fr()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fr()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    .line 1826149
    :goto_0
    if-eqz v1, :cond_1

    .line 1826150
    new-instance v2, LX/BrE;

    iget-object v4, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->f:LX/1Ad;

    sget-object v5, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, v5}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fr()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->z()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fr()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhoto;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    invoke-direct {v2, v4, v0}, LX/BrE;-><init>(LX/1aZ;F)V

    move-object v0, v2

    .line 1826151
    :goto_1
    iget-object v1, p0, Lcom/facebook/feed/rows/sections/attachments/MediaQuestionPollAttachmentPartDefinition;->d:Lcom/facebook/feedplugins/attachments/linkshare/base/AngoraClearPartDefinition;

    invoke-interface {p1, v1, v3}, LX/1aD;->a(LX/1Nt;Ljava/lang/Object;)V

    .line 1826152
    return-object v0

    :cond_0
    move-object v1, v3

    .line 1826153
    goto :goto_0

    :cond_1
    move-object v0, v3

    goto :goto_1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/16 v0, 0x1e

    const v1, -0x513dd089

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1826154
    check-cast p2, LX/BrE;

    .line 1826155
    if-eqz p2, :cond_0

    iget-object v1, p2, LX/BrE;->a:LX/1aZ;

    if-eqz v1, :cond_0

    iget v1, p2, LX/BrE;->b:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    move-object v1, p4

    .line 1826156
    check-cast v1, LX/35q;

    iget-object v2, p2, LX/BrE;->a:LX/1aZ;

    invoke-interface {v1, v2}, LX/35q;->setLargeImageController(LX/1aZ;)V

    .line 1826157
    check-cast p4, LX/35q;

    iget v1, p2, LX/BrE;->b:F

    invoke-interface {p4, v1}, LX/35q;->setLargeImageAspectRatio(F)V

    .line 1826158
    :cond_0
    const/16 v1, 0x1f

    const v2, -0x7fe77d40

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 1826159
    const/4 v0, 0x1

    return v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;LX/1PW;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 1826160
    move-object v0, p4

    check-cast v0, LX/35q;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/35q;->setLargeImageController(LX/1aZ;)V

    .line 1826161
    check-cast p4, LX/2yW;

    invoke-interface {p4}, LX/2yW;->a()V

    .line 1826162
    return-void
.end method
