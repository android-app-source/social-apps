.class public final Lcom/facebook/feed/data/FeedDataLoader$5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/api/feed/FetchFeedParams;

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/feed/data/FeedDataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/FeedDataLoader;Lcom/facebook/api/feed/FetchFeedParams;Z)V
    .locals 0

    .prologue
    .line 1707801
    iput-object p1, p0, Lcom/facebook/feed/data/FeedDataLoader$5;->c:Lcom/facebook/feed/data/FeedDataLoader;

    iput-object p2, p0, Lcom/facebook/feed/data/FeedDataLoader$5;->a:Lcom/facebook/api/feed/FetchFeedParams;

    iput-boolean p3, p0, Lcom/facebook/feed/data/FeedDataLoader$5;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    .line 1707802
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v0

    .line 1707803
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoader$5;->c:Lcom/facebook/feed/data/FeedDataLoader;

    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoader$5;->a:Lcom/facebook/api/feed/FetchFeedParams;

    iget-boolean v3, p0, Lcom/facebook/feed/data/FeedDataLoader$5;->b:Z

    .line 1707804
    new-instance v4, LX/0rT;

    invoke-direct {v4}, LX/0rT;-><init>()V

    invoke-virtual {v4, v2}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v4

    sget-object v5, LX/0gf;->SKIP_TAIL_GAP:LX/0gf;

    invoke-virtual {v4, v5}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v4

    sget-object v5, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 1707805
    iput-object v5, v4, LX/0rT;->a:LX/0rS;

    .line 1707806
    move-object v4, v4

    .line 1707807
    const/4 v5, 0x1

    .line 1707808
    iput-boolean v5, v4, LX/0rT;->q:Z

    .line 1707809
    move-object v4, v4

    .line 1707810
    invoke-virtual {v4}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v4

    move-object v5, v4

    .line 1707811
    sget-object v6, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 1707812
    invoke-static {v1, v6, v3, v5, v0}, Lcom/facebook/feed/data/FeedDataLoader;->a(Lcom/facebook/feed/data/FeedDataLoader;LX/0rS;ZLcom/facebook/api/feed/FetchFeedParams;Lcom/google/common/util/concurrent/SettableFuture;)LX/22z;

    move-result-object v4

    iput-object v4, v1, Lcom/facebook/feed/data/FeedDataLoader;->t:LX/22z;

    .line 1707813
    iget-object v12, v1, Lcom/facebook/feed/data/FeedDataLoader;->X:LX/22k;

    new-instance v4, LX/230;

    iget-object v7, v1, LX/0gD;->o:LX/0rB;

    invoke-virtual {v7}, LX/0rB;->b()LX/0rn;

    move-result-object v7

    iget-object v8, v1, Lcom/facebook/feed/data/FeedDataLoader;->t:LX/22z;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, v1, LX/0gD;->n:Lcom/facebook/api/feedtype/FeedType;

    .line 1707814
    iget-object v11, v10, Lcom/facebook/api/feedtype/FeedType;->f:Lcom/facebook/api/feedtype/FeedType$Name;

    move-object v10, v11

    .line 1707815
    iget-object v11, v10, Lcom/facebook/api/feedtype/FeedType$Name;->B:Ljava/lang/String;

    move-object v10, v11

    .line 1707816
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "_after"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    iget-object v10, v1, Lcom/facebook/feed/data/FeedDataLoader;->af:LX/0SG;

    invoke-interface {v10}, LX/0SG;->a()J

    move-result-wide v10

    invoke-direct/range {v4 .. v11}, LX/230;-><init>(Lcom/facebook/api/feed/FetchFeedParams;LX/0rS;LX/0rn;LX/22z;Ljava/lang/String;J)V

    invoke-virtual {v12, v4}, LX/22k;->a(LX/230;)V

    .line 1707817
    return-void
.end method
