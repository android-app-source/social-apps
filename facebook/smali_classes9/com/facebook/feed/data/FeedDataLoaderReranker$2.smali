.class public final Lcom/facebook/feed/data/FeedDataLoaderReranker$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0rl;

.field public final synthetic b:Lcom/facebook/api/feed/FetchFeedParams;

.field public final synthetic c:LX/0pl;


# direct methods
.method public constructor <init>(LX/0pl;LX/0rl;Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 0

    .prologue
    .line 1707862
    iput-object p1, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->c:LX/0pl;

    iput-object p2, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->a:LX/0rl;

    iput-object p3, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->b:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 1707852
    new-instance v1, LX/69Q;

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->c:LX/0pl;

    iget-object v0, v0, LX/0pl;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fM;

    new-instance v2, LX/69X;

    new-instance v3, LX/AjM;

    invoke-direct {v3, p0}, LX/AjM;-><init>(Lcom/facebook/feed/data/FeedDataLoaderReranker$2;)V

    iget-object v4, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->b:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-direct {v2, v3, v4}, LX/69X;-><init>(LX/AjM;Lcom/facebook/api/feed/FetchFeedParams;)V

    invoke-direct {v1, v0, v2}, LX/69Q;-><init>(LX/1fM;LX/1fd;)V

    .line 1707853
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->c:LX/0pl;

    iget-object v0, v0, LX/0pl;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1fJ;

    invoke-virtual {v0, v1}, LX/1fJ;->b(LX/1fL;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1707854
    :try_start_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->c:LX/0pl;

    iget-object v0, v0, LX/0pl;->l:LX/0pn;

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->b:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-virtual {v0, v1}, LX/0pn;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 1707855
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->c:LX/0pl;

    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->a:LX/0rl;

    invoke-static {v1, v0, v2}, LX/0pl;->a$redex0(LX/0pl;Lcom/facebook/api/feed/FetchFeedResult;LX/0rl;)V
    :try_end_0
    .catch LX/69I; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 1707856
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->c:LX/0pl;

    iget-object v0, v0, LX/0pl;->h:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 1707857
    return-void

    .line 1707858
    :catch_0
    move-exception v0

    .line 1707859
    sget-object v1, LX/0pl;->a:Ljava/lang/Class;

    const-string v2, "fetchHomeStoriesFromDb failed"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1707860
    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->a:LX/0rl;

    invoke-interface {v1, v0}, LX/0rl;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    .line 1707861
    :catch_1
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->c:LX/0pl;

    iget-object v1, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->b:Lcom/facebook/api/feed/FetchFeedParams;

    invoke-static {v1}, Lcom/facebook/api/feed/FetchFeedResult;->a(Lcom/facebook/api/feed/FetchFeedParams;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/feed/data/FeedDataLoaderReranker$2;->a:LX/0rl;

    invoke-static {v0, v1, v2}, LX/0pl;->a$redex0(LX/0pl;Lcom/facebook/api/feed/FetchFeedResult;LX/0rl;)V

    goto :goto_0
.end method
