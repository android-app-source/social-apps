.class public final Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V
    .locals 0

    .prologue
    .line 1708064
    iput-object p1, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$2;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1708065
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$2;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    const/4 v4, 0x0

    .line 1708066
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->H:LX/0pV;

    sget-object v2, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->C:Ljava/lang/String;

    const-string v3, "Tearing down Handlers"

    invoke-virtual {v1, v2, v3}, LX/0pV;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1708067
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    if-eqz v1, :cond_0

    .line 1708068
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    invoke-virtual {v1}, LX/0jZ;->a()V

    .line 1708069
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 1708070
    iput-object v4, v1, LX/1gi;->x:LX/1gm;

    .line 1708071
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 1708072
    iput-object v4, v1, LX/1gi;->f:LX/1gh;

    .line 1708073
    iput-object v4, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->W:LX/1gi;

    .line 1708074
    :cond_0
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    if-eqz v1, :cond_6

    .line 1708075
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    const/4 v6, 0x0

    .line 1708076
    iget-object v2, v1, LX/1gp;->g:LX/1gu;

    if-eqz v2, :cond_1

    .line 1708077
    iget-object v2, v1, LX/1gp;->g:LX/1gu;

    invoke-virtual {v2}, LX/0jZ;->a()V

    .line 1708078
    :cond_1
    iget-object v2, v1, LX/1gp;->h:LX/1gv;

    if-eqz v2, :cond_2

    .line 1708079
    iget-object v2, v1, LX/1gp;->h:LX/1gv;

    invoke-virtual {v2}, LX/0jZ;->a()V

    .line 1708080
    :cond_2
    const/4 v2, 0x0

    .line 1708081
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v3, v5, :cond_3

    .line 1708082
    const/4 v2, 0x1

    .line 1708083
    :cond_3
    iget-object v3, v1, LX/1gp;->e:Landroid/os/HandlerThread;

    if-eqz v3, :cond_4

    .line 1708084
    if-eqz v2, :cond_c

    .line 1708085
    iget-object v3, v1, LX/1gp;->e:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 1708086
    :goto_0
    iput-object v6, v1, LX/1gp;->e:Landroid/os/HandlerThread;

    .line 1708087
    :cond_4
    iget-object v3, v1, LX/1gp;->f:Landroid/os/HandlerThread;

    if-eqz v3, :cond_5

    .line 1708088
    if-eqz v2, :cond_d

    .line 1708089
    iget-object v2, v1, LX/1gp;->f:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 1708090
    :goto_1
    iput-object v6, v1, LX/1gp;->f:Landroid/os/HandlerThread;

    .line 1708091
    :cond_5
    iput-object v4, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->X:LX/1gp;

    .line 1708092
    :cond_6
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    if-eqz v1, :cond_7

    .line 1708093
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    invoke-virtual {v1}, LX/0jZ;->a()V

    .line 1708094
    iput-object v4, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->r:LX/0jY;

    .line 1708095
    :cond_7
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_a

    const/4 v1, 0x1

    .line 1708096
    :goto_2
    iget-object v2, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->q:Landroid/os/HandlerThread;

    if-eqz v2, :cond_8

    .line 1708097
    if-eqz v1, :cond_b

    .line 1708098
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->q:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quitSafely()Z

    .line 1708099
    :goto_3
    iput-object v4, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->q:Landroid/os/HandlerThread;

    .line 1708100
    :cond_8
    sget-object v1, LX/1KX;->a:LX/1KX;

    move-object v1, v1

    .line 1708101
    invoke-virtual {v1, v4}, LX/1KX;->a(Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;)V

    .line 1708102
    iget-object v1, v0, LX/0gD;->b:LX/0fz;

    invoke-virtual {v1}, LX/0fz;->m()V

    .line 1708103
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    if-eqz v1, :cond_9

    .line 1708104
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->ab:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1708105
    :cond_9
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader$2;->a:Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->z:LX/0YG;

    .line 1708106
    return-void

    .line 1708107
    :cond_a
    const/4 v1, 0x0

    goto :goto_2

    .line 1708108
    :cond_b
    iget-object v1, v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->q:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    goto :goto_3

    .line 1708109
    :cond_c
    iget-object v3, v1, LX/1gp;->e:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->quit()Z

    goto :goto_0

    .line 1708110
    :cond_d
    iget-object v2, v1, LX/1gp;->f:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->quit()Z

    goto :goto_1
.end method
