.class public final Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/graphql/model/GraphQLComment;

.field public final synthetic d:LX/Bnz;


# direct methods
.method public constructor <init>(LX/Bnz;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLComment;)V
    .locals 0

    .prologue
    .line 1821362
    iput-object p1, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->d:LX/Bnz;

    iput-object p2, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->c:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    .line 1821363
    iget-object v0, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->d:LX/Bnz;

    iget-object v0, v0, LX/Bnz;->e:LX/0pG;

    invoke-virtual {v0}, LX/0pG;->a()LX/0gC;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;

    .line 1821364
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(Ljava/lang/String;)Lcom/facebook/feed/model/ClientFeedUnitEdge;

    move-result-object v1

    .line 1821365
    if-nez v1, :cond_3

    .line 1821366
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->d:LX/Bnz;

    iget-object v1, v1, LX/Bnz;->g:LX/0pn;

    iget-object v2, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->d:LX/Bnz;

    iget-object v2, v2, LX/Bnz;->f:LX/0Ym;

    invoke-virtual {v2}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v2

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v8, 0x0

    .line 1821367
    const/4 v5, 0x2

    new-array v5, v5, [LX/0ux;

    sget-object v6, LX/0pp;->a:LX/0U1;

    .line 1821368
    iget-object v7, v6, LX/0U1;->d:Ljava/lang/String;

    move-object v6, v7

    .line 1821369
    invoke-virtual {v2}, Lcom/facebook/api/feedtype/FeedType;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    sget-object v7, LX/0pp;->d:LX/0U1;

    .line 1821370
    iget-object v2, v7, LX/0U1;->d:Ljava/lang/String;

    move-object v7, v2

    .line 1821371
    invoke-static {v7, v3}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v5}, LX/0uu;->a([LX/0ux;)LX/0uw;

    move-result-object v5

    .line 1821372
    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v6, v5, v4}, LX/0pn;->a(LX/0pn;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)LX/0Px;

    move-result-object v5

    .line 1821373
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1821374
    invoke-virtual {v5, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 1821375
    :cond_0
    move-object v1, v4

    .line 1821376
    move-object v2, v1

    .line 1821377
    :goto_0
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    instance-of v1, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v1, :cond_2

    .line 1821378
    :cond_1
    :goto_1
    return-void

    .line 1821379
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 1821380
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->b:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1821381
    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->c:Lcom/facebook/graphql/model/GraphQLComment;

    .line 1821382
    invoke-static {v1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v4

    new-instance v5, LX/4WK;

    invoke-direct {v5}, LX/4WK;-><init>()V

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v6

    .line 1821383
    iput-object v6, v5, LX/4WK;->i:LX/0Px;

    .line 1821384
    move-object v5, v5

    .line 1821385
    new-instance v6, Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    invoke-direct {v6, v5}, Lcom/facebook/graphql/model/GraphQLFeedbackContext;-><init>(LX/4WK;)V

    .line 1821386
    move-object v5, v6

    .line 1821387
    iput-object v5, v4, LX/23u;->E:Lcom/facebook/graphql/model/GraphQLFeedbackContext;

    .line 1821388
    move-object v4, v4

    .line 1821389
    invoke-virtual {v4}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    move-object v1, v4

    .line 1821390
    invoke-virtual {v2, v1}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 1821391
    iget-object v1, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->d:LX/Bnz;

    iget-object v1, v1, LX/Bnz;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Bo0;

    iget-object v3, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->c:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLComment;->z()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/facebook/feed/data/freshfeed/livecomments/LiveCommentsHandler$1;->c:Lcom/facebook/graphql/model/GraphQLComment;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLComment;->x()J

    move-result-wide v6

    invoke-virtual {v1, v3, v4, v6, v7}, LX/Bo0;->a(Ljava/lang/String;Ljava/lang/String;J)V

    .line 1821392
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/feed/data/freshfeed/FreshFeedDataLoader;->a(LX/0Px;)V

    goto :goto_1

    :cond_3
    move-object v2, v1

    goto :goto_0
.end method
