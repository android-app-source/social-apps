.class public final Lcom/facebook/feed/data/FeedDataLoader$7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/feed/data/FeedDataLoader;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/data/FeedDataLoader;LX/0Px;)V
    .locals 0

    .prologue
    .line 1707818
    iput-object p1, p0, Lcom/facebook/feed/data/FeedDataLoader$7;->b:Lcom/facebook/feed/data/FeedDataLoader;

    iput-object p2, p0, Lcom/facebook/feed/data/FeedDataLoader$7;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 1707819
    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader$7;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, Lcom/facebook/feed/data/FeedDataLoader$7;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 1707820
    iget-object v3, p0, Lcom/facebook/feed/data/FeedDataLoader$7;->b:Lcom/facebook/feed/data/FeedDataLoader;

    .line 1707821
    iget-object v4, v3, Lcom/facebook/feed/data/FeedDataLoader;->as:LX/Ajr;

    if-nez v4, :cond_0

    .line 1707822
    iget-object v5, v3, Lcom/facebook/feed/data/FeedDataLoader;->ao:LX/22q;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v6, "prefetch_newsfeed_image_in_fg"

    invoke-static {v4, v6}, Lcom/facebook/common/callercontext/CallerContext;->b(Ljava/lang/Class;Ljava/lang/String;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v6

    iget-object v4, v3, Lcom/facebook/feed/data/FeedDataLoader;->ap:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Ajk;

    invoke-virtual {v5, v6, v4}, LX/22q;->a(Lcom/facebook/common/callercontext/CallerContext;LX/Ajk;)LX/Ajr;

    move-result-object v4

    iput-object v4, v3, Lcom/facebook/feed/data/FeedDataLoader;->as:LX/Ajr;

    .line 1707823
    :cond_0
    iget-object v4, v3, Lcom/facebook/feed/data/FeedDataLoader;->as:LX/Ajr;

    move-object v3, v4

    .line 1707824
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    sget-object v4, LX/5g1;->OFF:LX/5g1;

    invoke-virtual {v3, v0, v4}, LX/Ajr;->a(Lcom/facebook/feed/rows/core/props/FeedProps;LX/5g1;)Ljava/util/List;

    .line 1707825
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1707826
    :cond_1
    return-void
.end method
