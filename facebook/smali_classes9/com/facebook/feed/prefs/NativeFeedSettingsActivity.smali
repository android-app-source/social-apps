.class public Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;
.super Lcom/facebook/base/activity/FbPreferenceActivity;
.source ""


# instance fields
.field public a:LX/0bH;

.field public b:Lcom/facebook/debug/feed/DebugFeedConfig;

.field public c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1710671
    invoke-direct {p0}, Lcom/facebook/base/activity/FbPreferenceActivity;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/content/SecureContextHelper;LX/0bH;Lcom/facebook/debug/feed/DebugFeedConfig;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 1710558
    iput-object p1, p0, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->c:Lcom/facebook/content/SecureContextHelper;

    .line 1710559
    iput-object p2, p0, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->a:LX/0bH;

    .line 1710560
    iput-object p3, p0, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->b:Lcom/facebook/debug/feed/DebugFeedConfig;

    .line 1710561
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;

    invoke-static {v2}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v2}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v1

    check-cast v1, LX/0bH;

    invoke-static {v2}, Lcom/facebook/debug/feed/DebugFeedConfig;->a(LX/0QB;)Lcom/facebook/debug/feed/DebugFeedConfig;

    move-result-object v2

    check-cast v2, Lcom/facebook/debug/feed/DebugFeedConfig;

    invoke-direct {p0, v0, v1, v2}, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->a(Lcom/facebook/content/SecureContextHelper;LX/0bH;Lcom/facebook/debug/feed/DebugFeedConfig;)V

    return-void
.end method


# virtual methods
.method public final c(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1710562
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbPreferenceActivity;->c(Landroid/os/Bundle;)V

    .line 1710563
    const-string v0, "Native Feed - internal"

    invoke-virtual {p0, v0}, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710564
    invoke-static {p0, p0}, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1710565
    invoke-virtual {p0}, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->getPreferenceManager()Landroid/preference/PreferenceManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/preference/PreferenceManager;->createPreferenceScreen(Landroid/content/Context;)Landroid/preference/PreferenceScreen;

    move-result-object v0

    .line 1710566
    invoke-virtual {p0, v0}, Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;->setPreferenceScreen(Landroid/preference/PreferenceScreen;)V

    .line 1710567
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710568
    sget-object v2, LX/0pP;->b:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710569
    const-string v2, "Override comment flyout with permalink view"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710570
    const-string v2, "Launch permalink view instead of opening comment flyout"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710571
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710572
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710573
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710574
    sget-object v2, LX/0pP;->k:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710575
    const-string v2, "Report Spam from Feed"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710576
    const-string v2, "Display the spam reporting option in the story menu"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710577
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710578
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710579
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710580
    sget-object v2, LX/1Ro;->b:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710581
    const-string v2, "Show PartDefinition Names"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710582
    const-string v2, "Enable/Disable showing PartDefinition names and render measurement"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710583
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710584
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710585
    new-instance v2, LX/AmC;

    invoke-direct {v2, p0}, LX/AmC;-><init>(Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;)V

    invoke-virtual {v1, v2}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1710586
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1710587
    sget-object v2, LX/0eJ;->m:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setKey(Ljava/lang/String;)V

    .line 1710588
    const-string v2, "Components Conversion Overlay"

    invoke-virtual {v1, v2}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710589
    const-string v2, "Requires app restart to take effect"

    invoke-virtual {v1, v2}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710590
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710591
    new-instance v1, LX/4or;

    invoke-direct {v1, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1710592
    const-string v2, "Clear stories from cache to see the scissors gap"

    invoke-virtual {v1, v2}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710593
    const-string v2, "Clear stories from cache"

    invoke-virtual {v1, v2}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710594
    const-string v2, "Clear top stories"

    invoke-virtual {v1, v2}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 1710595
    sget-object v2, LX/0pP;->g:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4or;->a(LX/0Tn;)V

    .line 1710596
    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "at least 10"

    aput-object v3, v2, v4

    const-string v3, "at least 15"

    aput-object v3, v2, v5

    const-string v3, "at least 20"

    aput-object v3, v2, v6

    const-string v3, "everything"

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 1710597
    new-array v2, v8, [Ljava/lang/String;

    const-string v3, "10"

    aput-object v3, v2, v4

    const-string v3, "15"

    aput-object v3, v2, v5

    const-string v3, "20"

    aput-object v3, v2, v6

    const-string v3, "all"

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1710598
    const-string v2, "15"

    invoke-virtual {v1, v2}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710599
    new-instance v2, LX/AmD;

    invoke-direct {v2, p0}, LX/AmD;-><init>(Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;)V

    invoke-virtual {v1, v2}, LX/4or;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1710600
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710601
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710602
    sget-object v2, LX/0pP;->o:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710603
    const-string v2, "Enable Story Privacy Editing"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710604
    const-string v2, "Allow users to change the audience of their own stories from the story action menu"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710605
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710606
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710607
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710608
    sget-object v2, LX/0pP;->p:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710609
    const-string v2, "Enable demo ad invalidation"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710610
    const-string v2, "Allow demo ads to participate in ad invalidation checks"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710611
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710612
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710613
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710614
    sget-object v2, LX/0pP;->t:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710615
    const-string v2, "Enable debug feed"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710616
    const-string v2, "Enables fetching newsfeed story for debug feed enpoint"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710617
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710618
    new-instance v2, LX/AmE;

    invoke-direct {v2, p0}, LX/AmE;-><init>(Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;)V

    invoke-virtual {v1, v2}, LX/4oi;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 1710619
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710620
    new-instance v1, LX/AmA;

    invoke-direct {v1, p0}, LX/AmA;-><init>(Landroid/content/Context;)V

    .line 1710621
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710622
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710623
    sget-object v2, LX/0pP;->u:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710624
    const-string v2, "Visual Feedback for impression logging"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710625
    const-string v2, "If enabled, a toast is shown for every impression log event"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710626
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710627
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710628
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710629
    sget-object v2, LX/0pP;->h:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710630
    const-string v2, "Always do fresh fetch on cold start"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710631
    const-string v2, "Always go to the network for new stories on cold start"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710632
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710633
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710634
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710635
    sget-object v2, LX/0pP;->i:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710636
    const-string v2, "Visual Feedback for topics prediction"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710637
    const-string v2, "If enabled, a toast is shown when for every topics prediction event in the composer"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710638
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710639
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710640
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710641
    sget-object v2, LX/0pP;->j:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710642
    const-string v2, "Visual Feedback for the VPVD logging"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710643
    const-string v2, "If enabled, a toast is shown for every viewport visualization duration event. (Restart)"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710644
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710645
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710646
    new-instance v1, LX/4oi;

    invoke-direct {v1, p0}, LX/4oi;-><init>(Landroid/content/Context;)V

    .line 1710647
    sget-object v2, LX/0pP;->z:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4oi;->a(LX/0Tn;)V

    .line 1710648
    const-string v2, "Enable Debug Inline Survey"

    invoke-virtual {v1, v2}, LX/4oi;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710649
    const-string v2, "If enabled, inline survey will be attached to all stories"

    invoke-virtual {v1, v2}, LX/4oi;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710650
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4oi;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710651
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710652
    new-instance v1, LX/4ok;

    invoke-direct {v1, p0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 1710653
    sget-object v2, LX/0eJ;->o:LX/0Tn;

    invoke-virtual {v2}, LX/0To;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4ok;->setKey(Ljava/lang/String;)V

    .line 1710654
    const-string v2, "Client Value Model Overlay"

    invoke-virtual {v1, v2}, LX/4ok;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710655
    const-string v2, "Display client value model of feed story"

    invoke-virtual {v1, v2}, LX/4ok;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710656
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710657
    new-instance v1, LX/4or;

    invoke-direct {v1, p0}, LX/4or;-><init>(Landroid/content/Context;)V

    .line 1710658
    const-string v2, "Override Fresh Feed XConfig"

    invoke-virtual {v1, v2}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 1710659
    const-string v2, "Override Fresh Feed"

    invoke-virtual {v1, v2}, LX/4or;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710660
    const-string v2, "Override Fresh Feed"

    invoke-virtual {v1, v2}, LX/4or;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 1710661
    sget-object v2, LX/0pP;->A:LX/0Tn;

    invoke-virtual {v1, v2}, LX/4or;->a(LX/0Tn;)V

    .line 1710662
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "Default"

    aput-object v3, v2, v4

    const-string v3, "Off"

    aput-object v3, v2, v5

    const-string v3, "On"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, LX/4or;->setEntries([Ljava/lang/CharSequence;)V

    .line 1710663
    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "default"

    aput-object v3, v2, v4

    const-string v3, "off"

    aput-object v3, v2, v5

    const-string v3, "on"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, LX/4or;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 1710664
    const-string v2, "default"

    invoke-virtual {v1, v2}, LX/4or;->setDefaultValue(Ljava/lang/Object;)V

    .line 1710665
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710666
    new-instance v1, Landroid/preference/Preference;

    invoke-direct {v1, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 1710667
    const-string v2, "Display News Feed Event Logs"

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 1710668
    new-instance v2, LX/AmF;

    invoke-direct {v2, p0}, LX/AmF;-><init>(Lcom/facebook/feed/prefs/NativeFeedSettingsActivity;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 1710669
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 1710670
    return-void
.end method
