.class public Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;
.super Lcom/facebook/base/activity/FbFragmentActivity;
.source ""


# instance fields
.field public p:LX/0pV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1710693
    invoke-direct {p0}, Lcom/facebook/base/activity/FbFragmentActivity;-><init>()V

    .line 1710694
    return-void
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;

    invoke-static {v0}, LX/0pV;->a(LX/0QB;)LX/0pV;

    move-result-object v0

    check-cast v0, LX/0pV;

    iput-object v0, p0, Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;->p:LX/0pV;

    return-void
.end method

.method public static b(Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;Ljava/lang/String;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 1710695
    iget-object v0, p0, Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;->p:LX/0pV;

    invoke-virtual {v0}, LX/0pV;->a()Ljava/util/List;

    move-result-object v0

    .line 1710696
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1710697
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 1710698
    iget-object v1, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v1, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p1, v4}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1710699
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1710700
    :cond_1
    return-object v2
.end method


# virtual methods
.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1710701
    invoke-super {p0, p1}, Lcom/facebook/base/activity/FbFragmentActivity;->b(Landroid/os/Bundle;)V

    .line 1710702
    invoke-static {p0, p0}, Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 1710703
    const v0, 0x7f030c08

    invoke-virtual {p0, v0}, Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;->setContentView(I)V

    .line 1710704
    const v0, 0x7f0d0846

    invoke-virtual {p0, v0}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 1710705
    new-instance v1, LX/1P0;

    invoke-direct {v1, p0}, LX/1P0;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 1710706
    new-instance v1, LX/AmI;

    iget-object v2, p0, Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;->p:LX/0pV;

    invoke-virtual {v2}, LX/0pV;->a()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, LX/AmI;-><init>(Ljava/util/List;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 1710707
    const v1, 0x7f0d1dc8

    invoke-virtual {p0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->a(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 1710708
    new-instance v2, LX/AmG;

    invoke-direct {v2, p0, v0}, LX/AmG;-><init>(Lcom/facebook/feed/prefs/NewsfeedEventLogActivity;Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1710709
    return-void
.end method
