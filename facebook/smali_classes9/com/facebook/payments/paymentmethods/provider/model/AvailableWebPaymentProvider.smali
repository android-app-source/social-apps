.class public Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/BEp;

.field private final b:Landroid/net/Uri;

.field public final c:Landroid/net/Uri;

.field private final d:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1765077
    new-instance v0, LX/BEo;

    invoke-direct {v0}, LX/BEo;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/BEp;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1765071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765072
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->a:LX/BEp;

    .line 1765073
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->b:Landroid/net/Uri;

    .line 1765074
    iput-object p3, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->c:Landroid/net/Uri;

    .line 1765075
    iput-object p4, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->d:Landroid/net/Uri;

    .line 1765076
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1765065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765066
    const-class v0, LX/BEp;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BEp;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->a:LX/BEp;

    .line 1765067
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->b:Landroid/net/Uri;

    .line 1765068
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->c:Landroid/net/Uri;

    .line 1765069
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->d:Landroid/net/Uri;

    .line 1765070
    return-void
.end method


# virtual methods
.method public final a()LX/BEp;
    .locals 1

    .prologue
    .line 1765064
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->a:LX/BEp;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1765063
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1765058
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->a:LX/BEp;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1765059
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1765060
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1765061
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;->d:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1765062
    return-void
.end method
