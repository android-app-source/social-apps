.class public Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1765126
    new-instance v0, LX/BEr;

    invoke-direct {v0}, LX/BEr;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1765121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765122
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->a:Ljava/lang/String;

    .line 1765123
    const-class v0, Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->b:LX/0Px;

    .line 1765124
    const-class v0, Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->c:LX/0Px;

    .line 1765125
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/0Px;LX/0Px;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1765116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765117
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->a:Ljava/lang/String;

    .line 1765118
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->b:LX/0Px;

    .line 1765119
    iput-object p3, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->c:LX/0Px;

    .line 1765120
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1765111
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1765112
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765113
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1765114
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;->c:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1765115
    return-void
.end method
