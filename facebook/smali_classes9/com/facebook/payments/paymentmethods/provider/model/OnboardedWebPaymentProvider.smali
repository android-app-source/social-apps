.class public Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:LX/BEp;

.field private final b:Landroid/net/Uri;

.field public final c:Landroid/net/Uri;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1765091
    new-instance v0, LX/BEq;

    invoke-direct {v0}, LX/BEq;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/BEp;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 1765102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765103
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->a:LX/BEp;

    .line 1765104
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->b:Landroid/net/Uri;

    .line 1765105
    iput-object p3, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->c:Landroid/net/Uri;

    .line 1765106
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1765097
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765098
    const-class v0, LX/BEp;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/BEp;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->a:LX/BEp;

    .line 1765099
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->b:Landroid/net/Uri;

    .line 1765100
    const-class v0, Landroid/net/Uri;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->c:Landroid/net/Uri;

    .line 1765101
    return-void
.end method


# virtual methods
.method public final a()LX/BEp;
    .locals 1

    .prologue
    .line 1765107
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->a:LX/BEp;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1765096
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1765092
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->a:LX/BEp;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1765093
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1765094
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/model/OnboardedWebPaymentProvider;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1765095
    return-void
.end method
