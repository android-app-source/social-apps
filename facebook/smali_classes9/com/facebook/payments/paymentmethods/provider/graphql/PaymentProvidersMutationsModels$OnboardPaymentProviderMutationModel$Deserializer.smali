.class public final Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersMutationsModels$OnboardPaymentProviderMutationModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1764908
    const-class v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersMutationsModels$OnboardPaymentProviderMutationModel;

    new-instance v1, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersMutationsModels$OnboardPaymentProviderMutationModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersMutationsModels$OnboardPaymentProviderMutationModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1764909
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1764910
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 1764911
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1764912
    const/4 v2, 0x0

    .line 1764913
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_4

    .line 1764914
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764915
    :goto_0
    move v1, v2

    .line 1764916
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1764917
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1764918
    new-instance v1, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersMutationsModels$OnboardPaymentProviderMutationModel;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersMutationsModels$OnboardPaymentProviderMutationModel;-><init>()V

    .line 1764919
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1764920
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1764921
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1764922
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1764923
    :cond_0
    return-object v1

    .line 1764924
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764925
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 1764926
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 1764927
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1764928
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_2

    if-eqz v3, :cond_2

    .line 1764929
    const-string v4, "payment_provider"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1764930
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1764931
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v5, :cond_b

    .line 1764932
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764933
    :goto_2
    move v1, v3

    .line 1764934
    goto :goto_1

    .line 1764935
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 1764936
    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1764937
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_1

    .line 1764938
    :cond_5
    const-string p0, "onboarded"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 1764939
    invoke-virtual {p1}, LX/15w;->H()Z

    move-result v1

    move v5, v1

    move v1, v4

    .line 1764940
    :cond_6
    :goto_3
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, p0, :cond_9

    .line 1764941
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 1764942
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1764943
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_6

    if-eqz v7, :cond_6

    .line 1764944
    const-string p0, "__type__"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_7

    const-string p0, "__typename"

    invoke-virtual {v7, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 1764945
    :cond_7
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {v0, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_3

    .line 1764946
    :cond_8
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    goto :goto_3

    .line 1764947
    :cond_9
    const/4 v7, 0x2

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 1764948
    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 1764949
    if-eqz v1, :cond_a

    .line 1764950
    invoke-virtual {v0, v4, v5}, LX/186;->a(IZ)V

    .line 1764951
    :cond_a
    invoke-virtual {v0}, LX/186;->d()I

    move-result v3

    goto :goto_2

    :cond_b
    move v1, v3

    move v5, v3

    move v6, v3

    goto :goto_3
.end method
