.class public final Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;
.super Lcom/facebook/graphql/modelutil/BaseModel;
.source ""

# interfaces
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation runtime Lcom/facebook/flatbuffers/FragmentModelWithoutBridge;
.end annotation

.annotation runtime Lcom/facebook/flatbuffers/ModelWithFlatBufferFormatHash;
    a = 0xda13f6c
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Deserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Serializer;
.end annotation


# instance fields
.field private e:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:LX/3Sb;
    .annotation build Lcom/facebook/dracula/api/FlatDependentField;
        value = "mutableFlatBuffer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1764832
    const-class v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Deserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1764816
    const-class v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Serializer;

    return-object v0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1764843
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/facebook/graphql/modelutil/BaseModel;-><init>(I)V

    .line 1764844
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 4

    .prologue
    .line 1764833
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1764834
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->b()LX/2uF;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v0

    .line 1764835
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->c()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/2uF;LX/186;)I

    move-result v1

    .line 1764836
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1764837
    const/4 v3, 0x3

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 1764838
    const/4 v3, 0x0

    invoke-virtual {p1, v3, v0}, LX/186;->b(II)V

    .line 1764839
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->b(II)V

    .line 1764840
    const/4 v0, 0x2

    invoke-virtual {p1, v0, v2}, LX/186;->b(II)V

    .line 1764841
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1764842
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1764817
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 1764818
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->b()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1764819
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->b()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1764820
    if-eqz v1, :cond_0

    .line 1764821
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;

    .line 1764822
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->e:LX/3Sb;

    .line 1764823
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->c()LX/2uF;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1764824
    invoke-virtual {p0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->c()LX/2uF;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/2uF;LX/1jy;)LX/3Si;

    move-result-object v1

    .line 1764825
    if-eqz v1, :cond_1

    .line 1764826
    invoke-static {v0, p0}, LX/1k0;->a(LX/0jT;LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;

    .line 1764827
    invoke-virtual {v1}, LX/3Si;->a()LX/2uF;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->f:LX/3Sb;

    .line 1764828
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->i()V

    .line 1764829
    if-nez v0, :cond_2

    :goto_0
    return-object p0

    :cond_2
    move-object p0, v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1764830
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->g:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-super {p0, v0, v1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->g:Ljava/lang/String;

    .line 1764831
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final b()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAvailableProviders"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1764807
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->e:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x0

    const v4, 0x3f13eab5    # 0.5778001f

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->e:LX/3Sb;

    .line 1764808
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->e:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 1

    .prologue
    .line 1764809
    new-instance v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;

    invoke-direct {v0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;-><init>()V

    .line 1764810
    invoke-virtual {v0, p1, p2}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;I)V

    .line 1764811
    return-object v0
.end method

.method public final c()LX/2uF;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOnboardedProviders"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 1764812
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->f:LX/3Sb;

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {p0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v2

    const/4 v3, 0x1

    const v4, -0x2157c80e

    invoke-static {v0, v1, v2, v3, v4}, LX/3SZ;->a(LX/3Sb;LX/15i;III)LX/2uF;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->f:LX/3Sb;

    .line 1764813
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;->f:LX/3Sb;

    check-cast v0, LX/2uF;

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1764814
    const v0, -0x5836871

    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1764815
    const v0, 0x649dd1f5

    return v0
.end method
