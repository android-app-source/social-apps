.class public final Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Deserializer;
.super Lcom/facebook/common/json/FbJsonDeserializer;
.source ""


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1764665
    const-class v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;

    new-instance v1, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Deserializer;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Deserializer;-><init>()V

    invoke-static {v0, v1}, LX/11G;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonDeserializer;)V

    .line 1764666
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1764777
    invoke-direct {p0}, Lcom/facebook/common/json/FbJsonDeserializer;-><init>()V

    return-void
.end method


# virtual methods
.method public final deserialize(LX/15w;LX/0n3;)Ljava/lang/Object;
    .locals 12

    .prologue
    .line 1764667
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 1764668
    const/4 v2, 0x0

    .line 1764669
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v1

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v1, v3, :cond_8

    .line 1764670
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764671
    :goto_0
    move v1, v2

    .line 1764672
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 1764673
    invoke-static {v0}, LX/1pR;->a(LX/186;)LX/15i;

    move-result-object v0

    move-object v2, v0

    .line 1764674
    new-instance v1, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;-><init>()V

    .line 1764675
    invoke-virtual {v2}, LX/15i;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {v0}, LX/0ah;->a(Ljava/nio/ByteBuffer;)I

    move-result v3

    move-object v0, v1

    .line 1764676
    check-cast v0, Lcom/facebook/graphql/modelutil/BaseModel;

    invoke-virtual {v0, v2, v3, p1}, Lcom/facebook/graphql/modelutil/BaseModel;->a(LX/15i;ILjava/lang/Object;)V

    .line 1764677
    instance-of v0, v1, LX/0Pm;

    if-eqz v0, :cond_0

    .line 1764678
    check-cast v1, LX/0Pm;

    invoke-interface {v1}, LX/0Pm;->a()Ljava/lang/Object;

    move-result-object v1

    .line 1764679
    :cond_0
    return-object v1

    .line 1764680
    :cond_1
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764681
    :cond_2
    :goto_1
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_7

    .line 1764682
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 1764683
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1764684
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_2

    if-eqz v5, :cond_2

    .line 1764685
    const-string v6, "available_providers"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1764686
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1764687
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_3

    .line 1764688
    :goto_2
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_3

    .line 1764689
    const/4 v6, 0x0

    .line 1764690
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_10

    .line 1764691
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764692
    :goto_3
    move v5, v6

    .line 1764693
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1764694
    :cond_3
    invoke-static {v4, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v4

    move v4, v4

    .line 1764695
    goto :goto_1

    .line 1764696
    :cond_4
    const-string v6, "onboarded_providers"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 1764697
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1764698
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_5

    .line 1764699
    :goto_4
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_5

    .line 1764700
    const/4 v6, 0x0

    .line 1764701
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_1b

    .line 1764702
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764703
    :goto_5
    move v5, v6

    .line 1764704
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1764705
    :cond_5
    invoke-static {v3, v0}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 1764706
    goto/16 :goto_1

    .line 1764707
    :cond_6
    const-string v6, "payment_account_id"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1764708
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    goto/16 :goto_1

    .line 1764709
    :cond_7
    const/4 v5, 0x3

    invoke-virtual {v0, v5}, LX/186;->c(I)V

    .line 1764710
    invoke-virtual {v0, v2, v4}, LX/186;->b(II)V

    .line 1764711
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v3}, LX/186;->b(II)V

    .line 1764712
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 1764713
    invoke-virtual {v0}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_8
    move v1, v2

    move v3, v2

    move v4, v2

    goto/16 :goto_1

    .line 1764714
    :cond_9
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764715
    :cond_a
    :goto_6
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_f

    .line 1764716
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1764717
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1764718
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, p0, :cond_a

    if-eqz v10, :cond_a

    .line 1764719
    const-string v11, "__type__"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_b

    const-string v11, "__typename"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1764720
    :cond_b
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v9

    invoke-virtual {v0, v9}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v9

    goto :goto_6

    .line 1764721
    :cond_c
    const-string v11, "image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_d

    .line 1764722
    const/4 v10, 0x0

    .line 1764723
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v11, :cond_14

    .line 1764724
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764725
    :goto_7
    move v8, v10

    .line 1764726
    goto :goto_6

    .line 1764727
    :cond_d
    const-string v11, "onboarding_dismiss_url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 1764728
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_6

    .line 1764729
    :cond_e
    const-string v11, "onboarding_url"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 1764730
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_6

    .line 1764731
    :cond_f
    const/4 v10, 0x4

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1764732
    invoke-virtual {v0, v6, v9}, LX/186;->b(II)V

    .line 1764733
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v8}, LX/186;->b(II)V

    .line 1764734
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 1764735
    const/4 v6, 0x3

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1764736
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_3

    :cond_10
    move v5, v6

    move v7, v6

    move v8, v6

    move v9, v6

    goto/16 :goto_6

    .line 1764737
    :cond_11
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764738
    :cond_12
    :goto_8
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, p0, :cond_13

    .line 1764739
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 1764740
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1764741
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object p0

    sget-object p2, LX/15z;->VALUE_NULL:LX/15z;

    if-eq p0, p2, :cond_12

    if-eqz v11, :cond_12

    .line 1764742
    const-string p0, "uri"

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 1764743
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_8

    .line 1764744
    :cond_13
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, LX/186;->c(I)V

    .line 1764745
    invoke-virtual {v0, v10, v8}, LX/186;->b(II)V

    .line 1764746
    invoke-virtual {v0}, LX/186;->d()I

    move-result v10

    goto :goto_7

    :cond_14
    move v8, v10

    goto :goto_8

    .line 1764747
    :cond_15
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764748
    :cond_16
    :goto_9
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_1a

    .line 1764749
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 1764750
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1764751
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_16

    if-eqz v9, :cond_16

    .line 1764752
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_17

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_18

    .line 1764753
    :cond_17
    invoke-static {p1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {v0, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_9

    .line 1764754
    :cond_18
    const-string v10, "image"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_19

    .line 1764755
    const/4 v9, 0x0

    .line 1764756
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v10, :cond_1f

    .line 1764757
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764758
    :goto_a
    move v7, v9

    .line 1764759
    goto :goto_9

    .line 1764760
    :cond_19
    const-string v10, "management_url"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_15

    .line 1764761
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_9

    .line 1764762
    :cond_1a
    const/4 v9, 0x3

    invoke-virtual {v0, v9}, LX/186;->c(I)V

    .line 1764763
    invoke-virtual {v0, v6, v8}, LX/186;->b(II)V

    .line 1764764
    const/4 v6, 0x1

    invoke-virtual {v0, v6, v7}, LX/186;->b(II)V

    .line 1764765
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v5}, LX/186;->b(II)V

    .line 1764766
    invoke-virtual {v0}, LX/186;->d()I

    move-result v6

    goto/16 :goto_5

    :cond_1b
    move v5, v6

    move v7, v6

    move v8, v6

    goto :goto_9

    .line 1764767
    :cond_1c
    invoke-virtual {p1}, LX/15w;->f()LX/15w;

    .line 1764768
    :cond_1d
    :goto_b
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_1e

    .line 1764769
    invoke-virtual {p1}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 1764770
    invoke-virtual {p1}, LX/15w;->c()LX/15z;

    .line 1764771
    invoke-virtual {p1}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object p0, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, p0, :cond_1d

    if-eqz v10, :cond_1d

    .line 1764772
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1c

    .line 1764773
    invoke-virtual {p1}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_b

    .line 1764774
    :cond_1e
    const/4 v10, 0x1

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 1764775
    invoke-virtual {v0, v9, v7}, LX/186;->b(II)V

    .line 1764776
    invoke-virtual {v0}, LX/186;->d()I

    move-result v9

    goto :goto_a

    :cond_1f
    move v7, v9

    goto :goto_b
.end method
