.class public final Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;
.super LX/1vt;
.source ""

# interfaces
.implements Lcom/facebook/flatbuffers/MutableFlattenable;
.implements Lcom/facebook/graphql/modelutil/FragmentModel;
.implements LX/0jT;


# annotations
.annotation build Lcom/facebook/dracula/api/FlatImplementation;
.end annotation

.annotation build Lcom/facebook/dracula/api/FlatWrapper;
    implementation = Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1764610
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, LX/1vt;-><init>(LX/15i;II)V

    .line 1764611
    return-void
.end method

.method private constructor <init>(LX/15i;II)V
    .locals 0

    .prologue
    .line 1764661
    invoke-direct {p0, p1, p2, p3}, LX/1vt;-><init>(LX/15i;II)V

    .line 1764662
    return-void
.end method

.method public static a(LX/15i;IILX/186;)I
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 1764628
    if-nez p1, :cond_0

    .line 1764629
    :goto_0
    return v0

    .line 1764630
    :cond_0
    sparse-switch p2, :sswitch_data_0

    .line 1764631
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1764632
    :sswitch_0
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v0

    .line 1764633
    const v1, -0x286be87c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1764634
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1764635
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1764636
    invoke-virtual {p0, p1, v6}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1764637
    invoke-virtual {p3, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 1764638
    const/4 v3, 0x4

    invoke-virtual {p3, v3}, LX/186;->c(I)V

    .line 1764639
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1764640
    invoke-virtual {p3, v5, v1}, LX/186;->b(II)V

    .line 1764641
    invoke-virtual {p3, v6, v2}, LX/186;->b(II)V

    .line 1764642
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1764643
    :sswitch_1
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1764644
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1764645
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1764646
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1764647
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1764648
    :sswitch_2
    invoke-virtual {p0, p1, v4}, LX/15i;->p(II)I

    move-result v0

    .line 1764649
    const v1, 0x4f20e94d    # 2.69964416E9f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    .line 1764650
    invoke-virtual {p0, p1, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1764651
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1764652
    invoke-virtual {p3, v6}, LX/186;->c(I)V

    .line 1764653
    invoke-virtual {p3, v4, v0}, LX/186;->b(II)V

    .line 1764654
    invoke-virtual {p3, v5, v1}, LX/186;->b(II)V

    .line 1764655
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    .line 1764656
    :sswitch_3
    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 1764657
    invoke-virtual {p3, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 1764658
    invoke-virtual {p3, v4}, LX/186;->c(I)V

    .line 1764659
    invoke-virtual {p3, v0, v1}, LX/186;->b(II)V

    .line 1764660
    invoke-virtual {p3}, LX/186;->d()I

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x286be87c -> :sswitch_1
        -0x2157c80e -> :sswitch_2
        0x3f13eab5 -> :sswitch_0
        0x4f20e94d -> :sswitch_3
    .end sparse-switch
.end method

.method public static a(LX/2uF;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1764619
    if-nez p0, :cond_0

    move v0, v1

    .line 1764620
    :goto_0
    return v0

    .line 1764621
    :cond_0
    invoke-virtual {p0}, LX/39O;->c()I

    move-result v2

    .line 1764622
    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 1764623
    :goto_1
    if-ge v1, v2, :cond_2

    .line 1764624
    invoke-virtual {p0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v5, v3, LX/1vs;->b:I

    iget v3, v3, LX/1vs;->c:I

    invoke-static {v4, v5, v3}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;

    move-result-object v3

    invoke-virtual {p1, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    aput v3, v0, v1

    .line 1764625
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1764626
    :cond_1
    new-array v0, v2, [I

    goto :goto_1

    .line 1764627
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, LX/186;->a([IZ)I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/2uF;LX/1jy;)LX/3Si;
    .locals 10

    .prologue
    .line 1764612
    const/4 v7, 0x0

    .line 1764613
    const/4 v1, 0x0

    .line 1764614
    invoke-virtual {p0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    iget v0, v0, LX/1vs;->c:I

    .line 1764615
    invoke-static {v2, v3, v0}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;

    move-result-object v0

    invoke-interface {p1, v0}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, LX/1vt;

    iget-object v4, v0, LX/1vt;->a:LX/15i;

    iget v5, v0, LX/1vt;->b:I

    iget v6, v0, LX/1vt;->c:I

    sget-object v8, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v8

    :try_start_0
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1764616
    add-int/lit8 v8, v1, 0x1

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/1k0;->a(LX/2uF;ILX/15i;ILX/15i;IILX/3Si;)LX/3Si;

    move-result-object v7

    move v1, v8

    goto :goto_0

    .line 1764617
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 1764618
    :cond_0
    return-object v7
.end method

.method public static a(LX/15i;II)Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;
    .locals 1

    .prologue
    .line 1764564
    new-instance v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;

    invoke-direct {v0, p0, p1, p2}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method private static a(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1764603
    sparse-switch p2, :sswitch_data_0

    .line 1764604
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1764605
    :sswitch_0
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1764606
    const v1, -0x286be87c

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    .line 1764607
    :goto_0
    :sswitch_1
    return-void

    .line 1764608
    :sswitch_2
    invoke-virtual {p0, p1, v0}, LX/15i;->p(II)I

    move-result v0

    .line 1764609
    const v1, 0x4f20e94d    # 2.69964416E9f

    invoke-static {p0, v0, v1, p3}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->b(LX/15i;IILX/1jy;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x286be87c -> :sswitch_1
        -0x2157c80e -> :sswitch_2
        0x3f13eab5 -> :sswitch_0
        0x4f20e94d -> :sswitch_1
    .end sparse-switch
.end method

.method private static b(LX/15i;IILX/1jy;)V
    .locals 2

    .prologue
    .line 1764597
    if-eqz p1, :cond_0

    .line 1764598
    invoke-static {p0, p1, p2}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;

    move-result-object v1

    .line 1764599
    invoke-interface {p3, v1}, LX/1jy;->b(LX/0jT;)LX/0jT;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;

    .line 1764600
    if-eq v0, v1, :cond_0

    .line 1764601
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1764602
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/186;)I
    .locals 3

    .prologue
    .line 1764596
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v0

    return v0
.end method

.method public final a(LX/1jy;)LX/0jT;
    .locals 3

    .prologue
    .line 1764663
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    invoke-static {v0, v1, v2, p1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;IILX/1jy;)V

    .line 1764664
    return-object p0
.end method

.method public final a(LX/15i;I)V
    .locals 1

    .prologue
    .line 1764591
    iget v0, p0, LX/1vt;->c:I

    if-nez v0, :cond_0

    .line 1764592
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    .line 1764593
    :cond_0
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a:LX/15i;

    .line 1764594
    iput p2, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->b:I

    .line 1764595
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;I)V
    .locals 1

    .prologue
    .line 1764590
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final b(LX/15i;I)Lcom/facebook/graphql/modelutil/FragmentModel;
    .locals 2

    .prologue
    .line 1764589
    new-instance v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;

    iget v1, p0, LX/1vt;->c:I

    invoke-direct {v0, p1, p2, v1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;-><init>(LX/15i;II)V

    return-object v0
.end method

.method public final d_()I
    .locals 1

    .prologue
    .line 1764586
    iget v0, p0, LX/1vt;->c:I

    .line 1764587
    move v0, v0

    .line 1764588
    return v0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 1764583
    iget v0, p0, LX/1vt;->c:I

    .line 1764584
    move v0, v0

    .line 1764585
    return v0
.end method

.method public final o_()I
    .locals 1

    .prologue
    .line 1764580
    iget v0, p0, LX/1vt;->b:I

    .line 1764581
    move v0, v0

    .line 1764582
    return v0
.end method

.method public final q_()LX/15i;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1764577
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    .line 1764578
    move-object v0, v0

    .line 1764579
    return-object v0
.end method

.method public final t_()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 1764568
    iget-object v0, p0, LX/1vt;->a:LX/15i;

    iget v1, p0, LX/1vt;->b:I

    iget v2, p0, LX/1vt;->c:I

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 1764569
    new-instance v3, LX/186;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 1764570
    invoke-static {v0, v1, v2, v3}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;IILX/186;)I

    move-result v9

    .line 1764571
    invoke-virtual {v3, v9}, LX/186;->d(I)V

    .line 1764572
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 1764573
    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1764574
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 1764575
    invoke-static {v3, v9, v2}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$DraculaImplementation;

    move-result-object v3

    move-object v0, v3

    .line 1764576
    return-object v0
.end method

.method public final u_()I
    .locals 1

    .prologue
    .line 1764565
    iget v0, p0, LX/1vt;->c:I

    .line 1764566
    move v0, v0

    .line 1764567
    return v0
.end method
