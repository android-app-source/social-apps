.class public Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/6xg;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1764493
    new-instance v0, LX/BEY;

    invoke-direct {v0}, LX/BEY;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 1764494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764495
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->a:Ljava/lang/String;

    .line 1764496
    const-class v0, LX/6xg;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->b:LX/6xg;

    .line 1764497
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/6xg;)V
    .locals 0

    .prologue
    .line 1764498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1764499
    iput-object p1, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->a:Ljava/lang/String;

    .line 1764500
    iput-object p2, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->b:LX/6xg;

    .line 1764501
    return-void
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1764502
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1764503
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1764504
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->b:LX/6xg;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1764505
    return-void
.end method
