.class public final Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Serializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1764778
    const-class v0, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;

    new-instance v1, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Serializer;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Serializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1764779
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1764780
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1764781
    invoke-static {p0}, LX/2bt;->a(Lcom/facebook/graphql/modelutil/BaseModel;)LX/2bu;

    move-result-object v0

    .line 1764782
    iget-object v1, v0, LX/2bu;->a:LX/15i;

    iget v0, v0, LX/2bu;->b:I

    .line 1764783
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1764784
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1764785
    if-eqz v2, :cond_1

    .line 1764786
    const-string v3, "available_providers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1764787
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1764788
    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_0

    .line 1764789
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/BEl;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1764790
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1764791
    :cond_0
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1764792
    :cond_1
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    .line 1764793
    if-eqz v2, :cond_3

    .line 1764794
    const-string v3, "onboarded_providers"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1764795
    invoke-virtual {p1}, LX/0nX;->d()V

    .line 1764796
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v1, v2}, LX/15i;->c(I)I

    move-result p0

    if-ge v3, p0, :cond_2

    .line 1764797
    invoke-virtual {v1, v2, v3}, LX/15i;->q(II)I

    move-result p0

    invoke-static {v1, p0, p1, p2}, LX/BEm;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 1764798
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1764799
    :cond_2
    invoke-virtual {p1}, LX/0nX;->e()V

    .line 1764800
    :cond_3
    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v2

    .line 1764801
    if-eqz v2, :cond_4

    .line 1764802
    const-string v3, "payment_account_id"

    invoke-virtual {p1, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 1764803
    invoke-virtual {p1, v2}, LX/0nX;->b(Ljava/lang/String;)V

    .line 1764804
    :cond_4
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1764805
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1764806
    check-cast p1, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;

    invoke-static {p1, p2, p3}, Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel$Serializer;->a(Lcom/facebook/payments/paymentmethods/provider/graphql/PaymentProvidersModels$FetchPaymentProvidersQueryModel;LX/0nX;LX/0my;)V

    return-void
.end method
