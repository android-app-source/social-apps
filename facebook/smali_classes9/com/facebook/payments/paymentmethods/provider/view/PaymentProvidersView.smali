.class public Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/BF1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:Lcom/facebook/payments/picker/HeaderItemView;

.field private c:Landroid/view/ViewGroup;

.field private d:LX/70k;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1765283
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1765284
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->d()V

    .line 1765285
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 1765280
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1765281
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->d()V

    .line 1765282
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1765277
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1765278
    invoke-direct {p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->d()V

    .line 1765279
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-static {v0}, LX/BF1;->a(LX/0QB;)LX/BF1;

    move-result-object v0

    check-cast v0, LX/BF1;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a:LX/BF1;

    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 1765255
    const-class v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;

    invoke-static {v0, p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1765256
    const v0, 0x7f030f0e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1765257
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->setOrientation(I)V

    .line 1765258
    const v0, 0x7f0d04f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/picker/HeaderItemView;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->b:Lcom/facebook/payments/picker/HeaderItemView;

    .line 1765259
    const v0, 0x7f0d0553

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->c:Landroid/view/ViewGroup;

    .line 1765260
    new-instance v1, LX/70k;

    const v0, 0x7f0d0a95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->c:Landroid/view/ViewGroup;

    invoke-direct {v1, v0, v2}, LX/70k;-><init>(Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;Landroid/view/View;)V

    iput-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->d:LX/70k;

    .line 1765261
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a:LX/BF1;

    invoke-virtual {v0, p0}, LX/BF1;->a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;)V

    .line 1765262
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1765275
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1765276
    return-void
.end method

.method public final a(LX/1DI;)V
    .locals 1

    .prologue
    .line 1765273
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->d:LX/70k;

    invoke-virtual {v0, p1}, LX/70k;->a(LX/1DI;)V

    .line 1765274
    return-void
.end method

.method public final a(LX/716;)V
    .locals 1

    .prologue
    .line 1765271
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->b:Lcom/facebook/payments/picker/HeaderItemView;

    invoke-virtual {v0, p1}, Lcom/facebook/payments/picker/HeaderItemView;->a(LX/6vm;)V

    .line 1765272
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 1765269
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1765270
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1765267
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->d:LX/70k;

    invoke-virtual {v0}, LX/70k;->a()V

    .line 1765268
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1765265
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->d:LX/70k;

    invoke-virtual {v0}, LX/70k;->b()V

    .line 1765266
    return-void
.end method

.method public setComponentController(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V
    .locals 1

    .prologue
    .line 1765263
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersView;->a:LX/BF1;

    invoke-virtual {v0, p1}, LX/BF1;->a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V

    .line 1765264
    return-void
.end method
