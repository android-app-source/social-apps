.class public Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;
.implements LX/BF6;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/6xg;

.field public final b:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1765409
    new-instance v0, LX/BF4;

    invoke-direct {v0}, LX/BF4;-><init>()V

    sput-object v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(LX/BF5;)V
    .locals 1

    .prologue
    .line 1765405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765406
    iget-object v0, p1, LX/BF5;->b:LX/6xg;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6xg;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->a:LX/6xg;

    .line 1765407
    iget-object v0, p1, LX/BF5;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->b:Ljava/lang/String;

    .line 1765408
    return-void
.end method

.method public synthetic constructor <init>(LX/BF5;B)V
    .locals 0

    .prologue
    .line 1765404
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;-><init>(LX/BF5;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1765400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1765401
    invoke-static {}, LX/6xg;->values()[LX/6xg;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->a:LX/6xg;

    .line 1765402
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->b:Ljava/lang/String;

    .line 1765403
    return-void
.end method

.method public synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 1765410
    invoke-direct {p0, p1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static newBuilder()LX/BF5;
    .locals 2

    .prologue
    .line 1765399
    new-instance v0, LX/BF5;

    invoke-direct {v0}, LX/BF5;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 1765398
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1765389
    if-ne p0, p1, :cond_1

    .line 1765390
    :cond_0
    :goto_0
    return v0

    .line 1765391
    :cond_1
    instance-of v2, p1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    if-nez v2, :cond_2

    move v0, v1

    .line 1765392
    goto :goto_0

    .line 1765393
    :cond_2
    check-cast p1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    .line 1765394
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->a:LX/6xg;

    iget-object v3, p1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->a:LX/6xg;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 1765395
    goto :goto_0

    .line 1765396
    :cond_3
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1765397
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 1765388
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->a:LX/6xg;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1765385
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->a:LX/6xg;

    invoke-virtual {v0}, LX/6xg;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1765386
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1765387
    return-void
.end method
