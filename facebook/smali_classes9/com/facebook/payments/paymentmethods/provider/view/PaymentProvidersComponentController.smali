.class public Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/BEd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/AaV;",
            ">;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1765253
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1765254
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    return-void
.end method

.method public static a(Landroid/net/Uri;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 1765252
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "force_external_browser"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;)Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;
    .locals 2

    .prologue
    .line 1765247
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1765248
    const-string v1, "extra_payment_providers_view_params"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1765249
    new-instance v1, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-direct {v1}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;-><init>()V

    .line 1765250
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1765251
    return-object v1
.end method

.method public static a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;Z)V
    .locals 1

    .prologue
    .line 1765244
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    .line 1765245
    :cond_0
    invoke-static {p0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V

    .line 1765246
    :cond_1
    return-void
.end method

.method public static e(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V
    .locals 5

    .prologue
    .line 1765229
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AaV;

    .line 1765230
    invoke-interface {v0}, LX/AaV;->a()V

    goto :goto_0

    .line 1765231
    :cond_0
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->c:LX/BEd;

    new-instance v1, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->g:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    .line 1765232
    iget-object v3, v2, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->b:Ljava/lang/String;

    move-object v2, v3

    .line 1765233
    iget-object v3, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->g:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    .line 1765234
    iget-object v4, v3, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->a:LX/6xg;

    move-object v3, v4

    .line 1765235
    invoke-direct {v1, v2, v3}, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;-><init>(Ljava/lang/String;LX/6xg;)V

    .line 1765236
    new-instance v2, LX/BEZ;

    invoke-direct {v2}, LX/BEZ;-><init>()V

    move-object v2, v2

    .line 1765237
    const-string v3, "payment_receiver_id"

    iget-object v4, v1, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "payment_item_type"

    iget-object v4, v1, Lcom/facebook/payments/paymentmethods/provider/graphql/GetPaymentProvidersInfoParams;->b:LX/6xg;

    invoke-virtual {v4}, LX/6xg;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/BEZ;

    .line 1765238
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 1765239
    iget-object v3, v0, LX/BEd;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    iput-object v2, v0, LX/BEd;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 1765240
    iget-object v2, v0, LX/BEd;->c:Lcom/google/common/util/concurrent/ListenableFuture;

    new-instance v3, LX/BEb;

    invoke-direct {v3, v0}, LX/BEb;-><init>(LX/BEd;)V

    iget-object v4, v0, LX/BEd;->a:Ljava/util/concurrent/Executor;

    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v0, v2

    .line 1765241
    new-instance v1, LX/BEx;

    invoke-direct {v1, p0}, LX/BEx;-><init>(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V

    .line 1765242
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a:LX/1Ck;

    const-string v3, "get_payment_providers_key"

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1765243
    return-void
.end method


# virtual methods
.method public final a(LX/AaV;)V
    .locals 1

    .prologue
    .line 1765226
    if-eqz p1, :cond_0

    .line 1765227
    iget-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1765228
    :cond_0
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1765223
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1765224
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v4

    check-cast p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;

    invoke-static {v4}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    invoke-static {v4}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    new-instance v1, LX/BEd;

    invoke-static {v4}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object p1

    check-cast p1, Ljava/util/concurrent/Executor;

    invoke-static {v4}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-direct {v1, p1, v0}, LX/BEd;-><init>(Ljava/util/concurrent/Executor;LX/0tX;)V

    move-object v4, v1

    check-cast v4, LX/BEd;

    iput-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a:LX/1Ck;

    iput-object v3, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->b:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->c:LX/BEd;

    .line 1765225
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x28353677

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1765184
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 1765185
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1765186
    const-string v1, "extra_payment_providers_view_params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->g:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    .line 1765187
    const/4 v0, 0x0

    .line 1765188
    if-eqz p1, :cond_0

    .line 1765189
    const/4 v1, 0x1

    .line 1765190
    const-string v0, "extra_payment_providers_info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    .line 1765191
    const-string v0, "extra_clicked_payment_provider"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    iput-object v0, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    move v0, v1

    .line 1765192
    :cond_0
    invoke-static {p0, v0}, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;Z)V

    .line 1765193
    const/16 v0, 0x2b

    const v1, -0x51a9b672

    invoke-static {v3, v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x1efc1c83

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1765219
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroy()V

    .line 1765220
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1765221
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 1765222
    const/16 v1, 0x2b

    const v2, 0x7c98b59

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1fd44f93

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1765198
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1765199
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    instance-of v1, v1, Lcom/facebook/payments/paymentmethods/provider/model/AvailableWebPaymentProvider;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    invoke-interface {v1}, Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;->a()LX/BEp;

    move-result-object v1

    invoke-virtual {v1}, LX/BEp;->informServerToPoll()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1765200
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/AaV;

    .line 1765201
    invoke-interface {v1}, LX/AaV;->a()V

    goto :goto_0

    .line 1765202
    :cond_0
    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1765203
    new-instance v1, LX/4HW;

    invoke-direct {v1}, LX/4HW;-><init>()V

    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->g:Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;

    .line 1765204
    iget-object v4, v2, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersViewParams;->b:Ljava/lang/String;

    move-object v2, v4

    .line 1765205
    const-string v4, "receiver_id"

    invoke-virtual {v1, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1765206
    move-object v1, v1

    .line 1765207
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    invoke-interface {v2}, Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;->a()LX/BEp;

    move-result-object v2

    invoke-virtual {v2}, LX/BEp;->name()Ljava/lang/String;

    move-result-object v2

    .line 1765208
    const-string v4, "nmor_provider_type"

    invoke-virtual {v1, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1765209
    move-object v1, v1

    .line 1765210
    iget-object v2, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->c:LX/BEd;

    .line 1765211
    new-instance v4, LX/BEh;

    invoke-direct {v4}, LX/BEh;-><init>()V

    move-object v4, v4

    .line 1765212
    const-string v5, "input"

    invoke-virtual {v4, v5, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 1765213
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v4

    .line 1765214
    iget-object v5, v2, LX/BEd;->b:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 1765215
    new-instance v5, LX/BEc;

    invoke-direct {v5, v2}, LX/BEc;-><init>(LX/BEd;)V

    iget-object v6, v2, LX/BEd;->a:Ljava/util/concurrent/Executor;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v1, v4

    .line 1765216
    new-instance v2, LX/BEw;

    invoke-direct {v2, p0}, LX/BEw;-><init>(Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;)V

    .line 1765217
    iget-object v4, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->a:LX/1Ck;

    const-string v5, "inform_server_to_poll_key"

    invoke-virtual {v4, v5, v1, v2}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1765218
    :cond_1
    const/16 v1, 0x2b

    const v2, 0x338ba919

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1765194
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 1765195
    const-string v0, "extra_payment_providers_info"

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->e:Lcom/facebook/payments/paymentmethods/provider/model/PaymentProvidersInfo;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1765196
    const-string v0, "extra_clicked_payment_provider"

    iget-object v1, p0, Lcom/facebook/payments/paymentmethods/provider/view/PaymentProvidersComponentController;->f:Lcom/facebook/payments/paymentmethods/provider/model/NmorPaymentProvider;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1765197
    return-void
.end method
