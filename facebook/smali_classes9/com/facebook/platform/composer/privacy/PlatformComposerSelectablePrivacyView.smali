.class public Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/8Rp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/BKx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Landroid/widget/TextView;

.field public h:Z

.field private i:Z

.field private j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

.field private k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field

.field private l:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1774734
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1774735
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->l:Z

    .line 1774736
    invoke-direct {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->a()V

    .line 1774737
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1774730
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1774731
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->l:Z

    .line 1774732
    invoke-direct {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->a()V

    .line 1774733
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 1774721
    const-class v0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

    invoke-static {v0, p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1774722
    const v0, 0x7f030fc8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1774723
    new-instance v0, LX/8QQ;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/8QQ;-><init>(Landroid/content/Context;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->c:LX/0Px;

    .line 1774724
    const v0, 0x7f0d0acf

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->d:Landroid/view/View;

    .line 1774725
    const v0, 0x7f0d0acd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->f:Landroid/view/View;

    .line 1774726
    const v0, 0x7f0d0ace

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->g:Landroid/widget/TextView;

    .line 1774727
    const v0, 0x7f0d0ad0

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->e:Landroid/view/View;

    .line 1774728
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->setAsLoading(Z)V

    .line 1774729
    return-void
.end method

.method private static a(Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;LX/8Rp;LX/BKx;)V
    .locals 0

    .prologue
    .line 1774720
    iput-object p1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->a:LX/8Rp;

    iput-object p2, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->b:LX/BKx;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;

    invoke-static {v1}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v0

    check-cast v0, LX/8Rp;

    invoke-static {v1}, LX/BKx;->a(LX/0QB;)LX/BKx;

    move-result-object v1

    check-cast v1, LX/BKx;

    invoke-static {p0, v0, v1}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->a(Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;LX/8Rp;LX/BKx;)V

    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 1774717
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->b:LX/BKx;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->k:LX/0Px;

    iget-object v3, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->g:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    iget-object v4, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->f:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, LX/BKx;->a(Landroid/content/Context;LX/0Px;FIZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1774718
    iget-object v1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1774719
    return-void
.end method

.method private setAsLoading(Z)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 1774704
    if-eqz p1, :cond_0

    .line 1774705
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->c:LX/0Px;

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->k:LX/0Px;

    .line 1774706
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1774707
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1774708
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->g:Landroid/widget/TextView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1774709
    :goto_0
    iput-boolean p1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->h:Z

    .line 1774710
    iput-boolean v4, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->l:Z

    .line 1774711
    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->invalidate()V

    .line 1774712
    return-void

    .line 1774713
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1774714
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1774715
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->g:Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 1774716
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->a:LX/8Rp;

    iget-object v1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v3, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->i:Z

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8Rp;->a(Lcom/facebook/privacy/model/SelectablePrivacyData;Landroid/content/res/Resources;ZZ)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->k:LX/0Px;

    goto :goto_0
.end method


# virtual methods
.method public final a(ZLcom/facebook/composer/privacy/model/ComposerPrivacyData;)V
    .locals 2

    .prologue
    .line 1774697
    iget-boolean v0, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_3

    iget-object v0, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Trying to display an invalid ComposerPrivacyData in SelectablePrivacyView"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1774698
    iget-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->i:Z

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v1, p2, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0, v1}, Lcom/facebook/privacy/model/SelectablePrivacyData;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1774699
    :cond_2
    :goto_1
    return-void

    .line 1774700
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 1774701
    :cond_4
    iput-boolean p1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->i:Z

    .line 1774702
    iput-object p2, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    .line 1774703
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->j:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-boolean v0, v0, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->d:Z

    invoke-direct {p0, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->setAsLoading(Z)V

    goto :goto_1
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1774693
    iget-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->l:Z

    if-eqz v0, :cond_0

    .line 1774694
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->l:Z

    .line 1774695
    invoke-direct {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->b()V

    .line 1774696
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x15ef2fb5

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1774689
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->onSizeChanged(IIII)V

    .line 1774690
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->l:Z

    .line 1774691
    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->invalidate()V

    .line 1774692
    const/16 v1, 0x2d

    const v2, -0x3d396142

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 1774685
    new-instance v0, LX/BKv;

    invoke-direct {v0, p0, p1}, LX/BKv;-><init>(Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;Landroid/view/View$OnClickListener;)V

    .line 1774686
    invoke-super {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1774687
    iget-object v1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerSelectablePrivacyView;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1774688
    return-void
.end method
