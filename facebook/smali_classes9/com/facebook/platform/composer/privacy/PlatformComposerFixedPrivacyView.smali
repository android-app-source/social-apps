.class public Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/BKx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/8Sa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private d:Landroid/view/View;

.field public e:Landroid/widget/TextView;

.field public f:LX/0hs;

.field private g:Z

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 1774573
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 1774574
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->g:Z

    .line 1774575
    invoke-direct {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a()V

    .line 1774576
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 1774622
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1774623
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->g:Z

    .line 1774624
    invoke-direct {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a()V

    .line 1774625
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 1774618
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 1774619
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->g:Z

    .line 1774620
    invoke-direct {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a()V

    .line 1774621
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 1774617
    new-instance v0, LX/BKr;

    invoke-direct {v0, p0, p1}, LX/BKr;-><init>(Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;Ljava/lang/String;)V

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 1774611
    const-class v0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;

    invoke-static {v0, p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 1774612
    const v0, 0x7f030fbd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 1774613
    const v0, 0x7f0d0a7b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->d:Landroid/view/View;

    .line 1774614
    const v0, 0x7f0d0a7c

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    .line 1774615
    new-instance v0, LX/BKq;

    invoke-direct {v0, p0}, LX/BKq;-><init>(Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;)V

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 1774616
    return-void
.end method

.method private static a(Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;LX/BKx;LX/0wM;LX/8Sa;)V
    .locals 0

    .prologue
    .line 1774610
    iput-object p1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a:LX/BKx;

    iput-object p2, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->b:LX/0wM;

    iput-object p3, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->c:LX/8Sa;

    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 3

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v2

    check-cast p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;

    invoke-static {v2}, LX/BKx;->a(LX/0QB;)LX/BKx;

    move-result-object v0

    check-cast v0, LX/BKx;

    invoke-static {v2}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v1

    check-cast v1, LX/0wM;

    invoke-static {v2}, LX/8Sa;->a(LX/0QB;)LX/8Sa;

    move-result-object v2

    check-cast v2, LX/8Sa;

    invoke-static {p0, v0, v1, v2}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a(Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;LX/BKx;LX/0wM;LX/8Sa;)V

    return-void
.end method

.method private b()V
    .locals 6

    .prologue
    .line 1774607
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a:LX/BKx;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->h:LX/0Px;

    iget-object v3, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTextSize()F

    move-result v3

    iget-object v4, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    const/4 v5, 0x1

    invoke-virtual/range {v0 .. v5}, LX/BKx;->a(Landroid/content/Context;LX/0Px;FIZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    .line 1774608
    iget-object v1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1774609
    return-void
.end method

.method private c(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 1774606
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->b:LX/0wM;

    const v1, -0x6e685d

    invoke-virtual {v0, p1, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;Lcom/facebook/graphql/model/GraphQLAlbum;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 1774585
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 1774586
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->d:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1774587
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1955

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1774588
    iget-object v3, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v4, LX/8SZ;->PILL:LX/8SZ;

    invoke-static {v3, v4}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v0

    .line 1774589
    iget-object v3, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->b:Ljava/lang/String;

    .line 1774590
    if-eqz p2, :cond_4

    .line 1774591
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1774592
    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v4, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->a:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    sget-object v5, LX/8SZ;->TOKEN:LX/8SZ;

    invoke-static {v4, v5}, LX/8Sa;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;LX/8SZ;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1774593
    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a010e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v3, v4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1774594
    iget-object v3, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-static {v3, v0, v6, v6, v6}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 1774595
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1774596
    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01b1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setCompoundDrawablePadding(I)V

    .line 1774597
    iget-object v3, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f08246f

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->D()Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->o()Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v6, v2

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLAlbum;->E()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v6, v1

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1774598
    :cond_0
    :goto_3
    iget-object v0, p1, Lcom/facebook/composer/privacy/model/ComposerFixedPrivacyData;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->a(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1774599
    return-void

    :cond_1
    move v0, v2

    .line 1774600
    goto/16 :goto_0

    .line 1774601
    :cond_2
    const-string v0, ""

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 1774602
    :cond_4
    if-eqz v3, :cond_5

    .line 1774603
    iget-object v1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1774604
    :cond_5
    if-eqz v0, :cond_0

    .line 1774605
    iget-object v1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->e:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->c(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v1, v0, v6, v6, v6}, LX/4lM;->a(Landroid/widget/TextView;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_3
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 1774581
    iget-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->h:LX/0Px;

    if-eqz v0, :cond_0

    .line 1774582
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->g:Z

    .line 1774583
    invoke-direct {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->b()V

    .line 1774584
    :cond_0
    return-void
.end method

.method public final onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x152301f3

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1774577
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/widget/CustomLinearLayout;->onSizeChanged(IIII)V

    .line 1774578
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->g:Z

    .line 1774579
    invoke-virtual {p0}, Lcom/facebook/platform/composer/privacy/PlatformComposerFixedPrivacyView;->invalidate()V

    .line 1774580
    const/16 v1, 0x2d

    const v2, -0x6f3c49d3

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
