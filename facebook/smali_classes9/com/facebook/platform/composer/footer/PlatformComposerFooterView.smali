.class public Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field private a:Lcom/facebook/fbui/glyph/GlyphView;

.field private b:Lcom/facebook/fbui/glyph/GlyphView;

.field private c:Lcom/facebook/fbui/glyph/GlyphView;

.field private d:Lcom/facebook/fbui/glyph/GlyphView;

.field private e:Lcom/facebook/fbui/glyph/GlyphView;

.field private f:Lcom/facebook/fbui/glyph/GlyphView;

.field private g:Landroid/view/View;

.field private h:Landroid/view/View;

.field private i:I

.field private j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/fbui/glyph/GlyphView;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 1774053
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 1774054
    invoke-direct {p0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->b()V

    .line 1774055
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1774050
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 1774051
    invoke-direct {p0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->b()V

    .line 1774052
    return-void
.end method

.method private b(LX/BKc;)Lcom/facebook/fbui/glyph/GlyphView;
    .locals 2

    .prologue
    .line 1774048
    invoke-virtual {p0, p1}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;)Landroid/view/View;

    move-result-object v0

    .line 1774049
    instance-of v1, v0, Lcom/facebook/fbui/glyph/GlyphView;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1774046
    const v0, 0x7fffffff

    iput v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->i:I

    .line 1774047
    return-void
.end method


# virtual methods
.method public final a(LX/BKc;)Landroid/view/View;
    .locals 2

    .prologue
    .line 1774037
    sget-object v0, LX/BKb;->a:[I

    invoke-virtual {p1}, LX/BKc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 1774038
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1774039
    :pswitch_0
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    goto :goto_0

    .line 1774040
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    goto :goto_0

    .line 1774041
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    goto :goto_0

    .line 1774042
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    goto :goto_0

    .line 1774043
    :pswitch_4
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    goto :goto_0

    .line 1774044
    :pswitch_5
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->g:Landroid/view/View;

    goto :goto_0

    .line 1774045
    :pswitch_6
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->h:Landroid/view/View;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 1773981
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1773982
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1773983
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1773984
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->j:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1773985
    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    .line 1773986
    add-int/lit8 v0, v1, 0x1

    .line 1773987
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1773988
    :cond_0
    iget v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->i:I

    if-gt v1, v0, :cond_1

    .line 1773989
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1773990
    :goto_2
    return-void

    .line 1773991
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 1773992
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    move v1, v2

    :goto_3
    if-ge v3, v5, :cond_3

    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->j:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    .line 1773993
    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_2

    iget v6, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->i:I

    add-int/lit8 v6, v6, -0x1

    if-ne v1, v6, :cond_2

    .line 1773994
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1773995
    invoke-virtual {v0, v7}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    move v0, v1

    .line 1773996
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_3

    .line 1773997
    :cond_2
    add-int/lit8 v0, v1, 0x1

    goto :goto_4

    .line 1773998
    :cond_3
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 1773999
    new-instance v3, Landroid/widget/PopupMenu;

    invoke-virtual {p0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-direct {v3, v0, v1}, Landroid/widget/PopupMenu;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 1774000
    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    move v1, v2

    .line 1774001
    :goto_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 1774002
    invoke-virtual {v3}, Landroid/widget/PopupMenu;->getMenu()Landroid/view/Menu;

    move-result-object v5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getId()I

    move-result v6

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v5, v2, v6, v1, v0}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 1774003
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1774004
    :cond_4
    new-instance v0, LX/BKZ;

    invoke-direct {v0, p0}, LX/BKZ;-><init>(Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;)V

    invoke-virtual {v3, v0}, Landroid/widget/PopupMenu;->setOnMenuItemClickListener(Landroid/widget/PopupMenu$OnMenuItemClickListener;)V

    .line 1774005
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/BKa;

    invoke-direct {v1, p0, v3}, LX/BKa;-><init>(Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;Landroid/widget/PopupMenu;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_2

    :cond_5
    move v0, v1

    goto/16 :goto_1
.end method

.method public final a(LX/BKc;Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 1774033
    invoke-virtual {p0, p1}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;)Landroid/view/View;

    move-result-object v0

    .line 1774034
    if-eqz v0, :cond_0

    .line 1774035
    invoke-virtual {v0, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1774036
    :cond_0
    return-void
.end method

.method public final a(LX/BKc;Z)V
    .locals 1

    .prologue
    .line 1774029
    invoke-direct {p0, p1}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->b(LX/BKc;)Lcom/facebook/fbui/glyph/GlyphView;

    move-result-object v0

    .line 1774030
    if-eqz v0, :cond_0

    .line 1774031
    invoke-virtual {v0, p2}, Lcom/facebook/fbui/glyph/GlyphView;->setActivated(Z)V

    .line 1774032
    :cond_0
    return-void
.end method

.method public final b(LX/BKc;Z)V
    .locals 1

    .prologue
    .line 1774025
    invoke-virtual {p0, p1}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;)Landroid/view/View;

    move-result-object v0

    .line 1774026
    if-eqz v0, :cond_0

    .line 1774027
    invoke-virtual {v0, p2}, Landroid/view/View;->setEnabled(Z)V

    .line 1774028
    :cond_0
    return-void
.end method

.method public final c(LX/BKc;Z)V
    .locals 2

    .prologue
    .line 1774020
    invoke-virtual {p0, p1}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a(LX/BKc;)Landroid/view/View;

    move-result-object v1

    .line 1774021
    if-eqz v1, :cond_0

    .line 1774022
    if-eqz p2, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1774023
    :cond_0
    return-void

    .line 1774024
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final onFinishInflate()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/16 v0, 0x2c

    const v1, 0x26015fa5

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 1774009
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 1774010
    const v0, 0x7f0d191e

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1774011
    const v0, 0x7f0d25fe

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1774012
    const v0, 0x7f0d00dc

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1774013
    const v0, 0x7f0d25ff

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1774014
    const v0, 0x7f0d2600

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1774015
    const v0, 0x7f0d2601

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 1774016
    const v0, 0x7f0d2602

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->g:Landroid/view/View;

    .line 1774017
    const v0, 0x7f0d2603

    invoke-virtual {p0, v0}, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->h:Landroid/view/View;

    .line 1774018
    iget-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->a:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v2, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->b:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v3, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->c:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v4, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v5, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->e:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-static {v0, v2, v3, v4, v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->j:LX/0Px;

    .line 1774019
    const/16 v0, 0x2d

    const v2, -0x2ae87829

    invoke-static {v6, v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setMaxVisibleButtonAllowed(I)V
    .locals 0

    .prologue
    .line 1774006
    if-lez p1, :cond_0

    :goto_0
    iput p1, p0, Lcom/facebook/platform/composer/footer/PlatformComposerFooterView;->i:I

    .line 1774007
    return-void

    .line 1774008
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method
