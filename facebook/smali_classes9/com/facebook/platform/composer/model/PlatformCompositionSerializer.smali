.class public Lcom/facebook/platform/composer/model/PlatformCompositionSerializer;
.super Lcom/fasterxml/jackson/databind/JsonSerializer;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/fasterxml/jackson/databind/JsonSerializer",
        "<",
        "Lcom/facebook/platform/composer/model/PlatformComposition;",
        ">;"
    }
.end annotation


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 1774519
    const-class v0, Lcom/facebook/platform/composer/model/PlatformComposition;

    new-instance v1, Lcom/facebook/platform/composer/model/PlatformCompositionSerializer;

    invoke-direct {v1}, Lcom/facebook/platform/composer/model/PlatformCompositionSerializer;-><init>()V

    invoke-static {v0, v1}, LX/2Ah;->a(Ljava/lang/Class;Lcom/fasterxml/jackson/databind/JsonSerializer;)V

    .line 1774520
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1774521
    invoke-direct {p0}, Lcom/fasterxml/jackson/databind/JsonSerializer;-><init>()V

    return-void
.end method

.method private static a(Lcom/facebook/platform/composer/model/PlatformComposition;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1774522
    if-nez p0, :cond_0

    .line 1774523
    invoke-virtual {p1}, LX/0nX;->h()V

    .line 1774524
    :cond_0
    invoke-virtual {p1}, LX/0nX;->f()V

    .line 1774525
    invoke-static {p0, p1, p2}, Lcom/facebook/platform/composer/model/PlatformCompositionSerializer;->b(Lcom/facebook/platform/composer/model/PlatformComposition;LX/0nX;LX/0my;)V

    .line 1774526
    invoke-virtual {p1}, LX/0nX;->g()V

    .line 1774527
    return-void
.end method

.method private static b(Lcom/facebook/platform/composer/model/PlatformComposition;LX/0nX;LX/0my;)V
    .locals 4

    .prologue
    .line 1774528
    const-string v0, "has_user_interacted"

    iget-boolean v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mHasUserInteracted:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1774529
    const-string v0, "user_id"

    iget-wide v2, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1774530
    const-string v0, "attachments"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAttachments:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1774531
    const-string v0, "target_album"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTargetAlbum:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774532
    const-string v0, "rating"

    iget v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mRating:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1774533
    const-string v0, "text_with_entities"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTextWithEntities:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774534
    const-string v0, "location_info"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mLocationInfo:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774535
    const-string v0, "minutiae_object"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774536
    const-string v0, "product_item_attachment"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774537
    const-string v0, "marketplace_id"

    iget-wide v2, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMarketplaceId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1774538
    const-string v0, "user_selected_tags"

    iget-boolean v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserSelectedTags:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1774539
    const-string v0, "publish_mode"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mPublishMode:LX/5Rn;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774540
    const-string v0, "schedule_time"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mScheduleTime:Ljava/lang/Long;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Long;)V

    .line 1774541
    const-string v0, "tagged_profiles"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1774542
    const-string v0, "removed_urls"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mRemovedURLs:LX/0Px;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/util/Collection;)V

    .line 1774543
    const-string v0, "date_info"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mComposerDateInfo:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774544
    const-string v0, "referenced_sticker_data"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mReferencedStickerData:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774545
    const-string v0, "share_params"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mShareParams:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774546
    const-string v0, "is_backout_draft"

    iget-boolean v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsBackoutDraft:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1774547
    const-string v0, "app_attribution"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppAttribution:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-static {p1, p2, v0, v1}, LX/2BE;->a(LX/0nX;LX/0my;Ljava/lang/String;Ljava/lang/Object;)V

    .line 1774548
    const-string v0, "app_provided_hashtag"

    iget-object v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppProvidedHashtag:Ljava/lang/String;

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/String;)V

    .line 1774549
    const-string v0, "user_deleted_app_provided_hashtag"

    iget-boolean v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserDeletedAppProvidedHashtag:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1774550
    const-string v0, "is_feed_only_post"

    iget-boolean v1, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsFeedOnlyPost:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/2BE;->a(LX/0nX;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1774551
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;LX/0nX;LX/0my;)V
    .locals 0

    .prologue
    .line 1774552
    check-cast p1, Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-static {p1, p2, p3}, Lcom/facebook/platform/composer/model/PlatformCompositionSerializer;->a(Lcom/facebook/platform/composer/model/PlatformComposition;LX/0nX;LX/0my;)V

    return-void
.end method
