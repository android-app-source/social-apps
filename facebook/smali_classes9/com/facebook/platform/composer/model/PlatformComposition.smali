.class public Lcom/facebook/platform/composer/model/PlatformComposition;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation build Lcom/facebook/common/json/AutoGenJsonDeserializer;
.end annotation

.annotation build Lcom/facebook/common/json/AutoGenJsonSerializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonDeserialize;
    using = Lcom/facebook/platform/composer/model/PlatformCompositionDeserializer;
.end annotation

.annotation runtime Lcom/fasterxml/jackson/databind/annotation/JsonSerialize;
    using = Lcom/facebook/platform/composer/model/PlatformCompositionSerializer;
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/facebook/platform/composer/model/PlatformComposition;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation
.end field

.field public final b:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0P1;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonIgnore;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation
.end field

.field public final mAppAttribution:Lcom/facebook/share/model/ComposerAppAttribution;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "app_attribution"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mAppProvidedHashtag:Ljava/lang/String;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "app_provided_hashtag"
    .end annotation
.end field

.field public final mAttachments:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "attachments"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation
.end field

.field public final mComposerDateInfo:Lcom/facebook/ipc/composer/model/ComposerDateInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "date_info"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mHasUserInteracted:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "has_user_interacted"
    .end annotation
.end field

.field public final mIsBackoutDraft:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_backout_draft"
    .end annotation
.end field

.field public final mIsFeedOnlyPost:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "is_feed_only_post"
    .end annotation
.end field

.field public final mLocationInfo:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "location_info"
    .end annotation
.end field

.field public final mMarketplaceId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "marketplace_id"
    .end annotation
.end field

.field public final mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "minutiae_object"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "product_item_attachment"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mPublishMode:LX/5Rn;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "publish_mode"
    .end annotation
.end field

.field public final mRating:I
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "rating"
    .end annotation
.end field

.field public final mReferencedStickerData:Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "referenced_sticker_data"
    .end annotation
.end field

.field public final mRemovedURLs:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "removed_urls"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final mScheduleTime:Ljava/lang/Long;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "schedule_time"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mShareParams:Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "share_params"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mTaggedProfiles:LX/0Px;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "tagged_profiles"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field public final mTargetAlbum:Lcom/facebook/graphql/model/GraphQLAlbum;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "target_album"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final mTextWithEntities:Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "text_with_entities"
    .end annotation
.end field

.field public final mUserDeletedAppProvidedHashtag:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_deleted_app_provided_hashtag"
    .end annotation
.end field

.field public final mUserId:J
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_id"
    .end annotation
.end field

.field public final mUserSelectedTags:Z
    .annotation runtime Lcom/fasterxml/jackson/annotation/JsonProperty;
        value = "user_selected_tags"
    .end annotation
.end field


# direct methods
.method public static $$getDeserializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1774472
    const-class v0, Lcom/facebook/platform/composer/model/PlatformCompositionDeserializer;

    return-object v0
.end method

.method public static $$getSerializerClass()Ljava/lang/Class;
    .locals 1
    .annotation build Lcom/facebook/proguard/annotations/DoNotStrip;
    .end annotation

    .prologue
    .line 1774404
    const-class v0, Lcom/facebook/platform/composer/model/PlatformCompositionSerializer;

    return-object v0
.end method

.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1774405
    new-instance v0, LX/BKo;

    invoke-direct {v0}, LX/BKo;-><init>()V

    sput-object v0, Lcom/facebook/platform/composer/model/PlatformComposition;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    .line 1774406
    new-instance v0, LX/BKp;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, LX/BKp;-><init>(J)V

    invoke-direct {p0, v0}, Lcom/facebook/platform/composer/model/PlatformComposition;-><init>(LX/BKp;)V

    .line 1774407
    return-void
.end method

.method public constructor <init>(LX/BKp;)V
    .locals 2

    .prologue
    .line 1774408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774409
    iget-wide v0, p1, LX/BKp;->a:J

    iput-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserId:J

    .line 1774410
    iget-object v0, p1, LX/BKp;->b:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAttachments:LX/0Px;

    .line 1774411
    iget-object v0, p1, LX/BKp;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTargetAlbum:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1774412
    iget-boolean v0, p1, LX/BKp;->d:Z

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mHasUserInteracted:Z

    .line 1774413
    iget v0, p1, LX/BKp;->e:I

    iput v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mRating:I

    .line 1774414
    iget-object v0, p1, LX/BKp;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTextWithEntities:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1774415
    iget-object v0, p1, LX/BKp;->g:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mLocationInfo:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1774416
    iget-object v0, p1, LX/BKp;->h:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1774417
    iget-object v0, p1, LX/BKp;->i:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1774418
    iget-wide v0, p1, LX/BKp;->j:J

    iput-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMarketplaceId:J

    .line 1774419
    iget-boolean v0, p1, LX/BKp;->k:Z

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserSelectedTags:Z

    .line 1774420
    iget-object v0, p1, LX/BKp;->l:LX/0Px;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    .line 1774421
    iget-object v0, p1, LX/BKp;->m:LX/5Rn;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mPublishMode:LX/5Rn;

    .line 1774422
    iget-object v0, p1, LX/BKp;->n:Ljava/lang/Long;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mScheduleTime:Ljava/lang/Long;

    .line 1774423
    iget-object v0, p1, LX/BKp;->o:LX/0Px;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mRemovedURLs:LX/0Px;

    .line 1774424
    iget-object v0, p1, LX/BKp;->p:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->a:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1774425
    iget-object v0, p1, LX/BKp;->q:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mComposerDateInfo:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1774426
    iget-object v0, p1, LX/BKp;->r:LX/0Px;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->b:LX/0Px;

    .line 1774427
    iget-object v0, p1, LX/BKp;->s:LX/0P1;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->c:LX/0P1;

    .line 1774428
    iget-object v0, p1, LX/BKp;->t:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mReferencedStickerData:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 1774429
    iget-object v0, p1, LX/BKp;->u:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mShareParams:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1774430
    iget-boolean v0, p1, LX/BKp;->v:Z

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsBackoutDraft:Z

    .line 1774431
    iget-object v0, p1, LX/BKp;->w:Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppAttribution:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1774432
    iget-object v0, p1, LX/BKp;->x:Ljava/lang/String;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppProvidedHashtag:Ljava/lang/String;

    .line 1774433
    iget-boolean v0, p1, LX/BKp;->y:Z

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserDeletedAppProvidedHashtag:Z

    .line 1774434
    iget-boolean v0, p1, LX/BKp;->z:Z

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsFeedOnlyPost:Z

    .line 1774435
    return-void

    .line 1774436
    :cond_0
    iget-object v0, p1, LX/BKp;->c:Lcom/facebook/graphql/model/GraphQLAlbum;

    goto :goto_0
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 1774437
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1774438
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserId:J

    .line 1774439
    const-class v0, Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAttachments:LX/0Px;

    .line 1774440
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLAlbum;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTargetAlbum:Lcom/facebook/graphql/model/GraphQLAlbum;

    .line 1774441
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mLocationInfo:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    .line 1774442
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserSelectedTags:Z

    .line 1774443
    const-class v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    .line 1774444
    const-class v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    .line 1774445
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMarketplaceId:J

    .line 1774446
    sget-object v0, Lcom/facebook/ipc/model/FacebookProfile;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Landroid/os/Parcelable$Creator;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    .line 1774447
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mRating:I

    .line 1774448
    invoke-static {p1}, LX/4By;->a(Landroid/os/Parcel;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTextWithEntities:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 1774449
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mHasUserInteracted:Z

    .line 1774450
    const-class v0, LX/5Rn;

    invoke-static {p1, v0}, LX/46R;->e(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/5Rn;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mPublishMode:LX/5Rn;

    .line 1774451
    invoke-static {p1}, LX/46R;->d(Landroid/os/Parcel;)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mScheduleTime:Ljava/lang/Long;

    .line 1774452
    invoke-static {p1}, LX/46R;->j(Landroid/os/Parcel;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mRemovedURLs:LX/0Px;

    .line 1774453
    const-class v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->a:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    .line 1774454
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mComposerDateInfo:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    .line 1774455
    const-class v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingLogger$LoggingParameters;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readArrayList(Ljava/lang/ClassLoader;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->b:LX/0Px;

    .line 1774456
    const-class v0, Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readHashMap(Ljava/lang/ClassLoader;)Ljava/util/HashMap;

    move-result-object v0

    invoke-static {v0}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->c:LX/0P1;

    .line 1774457
    const-class v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/model/ComposerStickerData;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mReferencedStickerData:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    .line 1774458
    const-class v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mShareParams:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    .line 1774459
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsBackoutDraft:Z

    .line 1774460
    const-class v0, Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/share/model/ComposerAppAttribution;

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppAttribution:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 1774461
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppProvidedHashtag:Ljava/lang/String;

    .line 1774462
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserDeletedAppProvidedHashtag:Z

    .line 1774463
    invoke-static {p1}, LX/46R;->a(Landroid/os/Parcel;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsFeedOnlyPost:Z

    .line 1774464
    return-void

    .line 1774465
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final A()LX/BKp;
    .locals 1

    .prologue
    .line 1774466
    new-instance v0, LX/BKp;

    invoke-direct {v0, p0}, LX/BKp;-><init>(Lcom/facebook/platform/composer/model/PlatformComposition;)V

    return-object v0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 1774467
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mHasUserInteracted:Z

    return v0
.end method

.method public final b()J
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1774469
    iget-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserId:J

    return-wide v0
.end method

.method public final c()Lcom/facebook/graphql/model/GraphQLAlbum;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774468
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTargetAlbum:Lcom/facebook/graphql/model/GraphQLAlbum;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 1774475
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTargetAlbum:Lcom/facebook/graphql/model/GraphQLAlbum;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1774474
    const/4 v0, 0x0

    return v0
.end method

.method public final e()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/composer/attachments/ComposerAttachment;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1774473
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAttachments:LX/0Px;

    return-object v0
.end method

.method public final f()Lcom/facebook/graphql/model/GraphQLTextWithEntities;
    .locals 1

    .prologue
    .line 1774401
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTextWithEntities:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1774471
    invoke-virtual {p0}, Lcom/facebook/platform/composer/model/PlatformComposition;->f()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0}, LX/8oj;->a(LX/175;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;
    .locals 1

    .prologue
    .line 1774470
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mLocationInfo:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    return-object v0
.end method

.method public final i()Lcom/facebook/composer/minutiae/model/MinutiaeObject;
    .locals 1

    .prologue
    .line 1774402
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    return-object v0
.end method

.method public final j()Lcom/facebook/ipc/composer/model/ProductItemAttachment;
    .locals 1

    .prologue
    .line 1774403
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    return-object v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 1774361
    iget-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMarketplaceId:J

    return-wide v0
.end method

.method public final l()LX/5Rn;
    .locals 1

    .prologue
    .line 1774360
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mPublishMode:LX/5Rn;

    return-object v0
.end method

.method public final m()Ljava/lang/Long;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774359
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mScheduleTime:Ljava/lang/Long;

    return-object v0
.end method

.method public final n()LX/0P1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/creativeediting/analytics/CreativeEditingUsageParams;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1774358
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->c:LX/0P1;

    return-object v0
.end method

.method public final o()Lcom/facebook/ipc/composer/model/ComposerStickerData;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774357
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mReferencedStickerData:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    return-object v0
.end method

.method public final p()Lcom/facebook/ipc/composer/intent/ComposerShareParams;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 1774356
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mShareParams:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 1774355
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserSelectedTags:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 1774354
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()LX/0Rf;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1774343
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1774344
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1774345
    iget-wide v6, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1774346
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1774347
    :cond_0
    const v0, 0x285feb

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v2

    .line 1774348
    invoke-virtual {p0}, Lcom/facebook/platform/composer/model/PlatformComposition;->f()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0, v2}, LX/8oj;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Rf;)LX/0Px;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1774349
    invoke-virtual {p0}, Lcom/facebook/platform/composer/model/PlatformComposition;->e()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_1

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/attachments/ComposerAttachment;

    .line 1774350
    invoke-virtual {v0}, Lcom/facebook/composer/attachments/ComposerAttachment;->d()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    invoke-static {v0, v2}, LX/8oj;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;LX/0Rf;)LX/0Px;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1774351
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1774352
    :cond_1
    iget-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserId:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1774353
    invoke-static {v3}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final t()LX/0Rf;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1774362
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 1774363
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1774364
    iget-wide v4, v0, Lcom/facebook/ipc/model/FacebookProfile;->mId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 1774365
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1774366
    :cond_0
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v0

    return-object v0
.end method

.method public final u()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1774367
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    return-object v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 1774368
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsBackoutDraft:Z

    return v0
.end method

.method public final w()Lcom/facebook/share/model/ComposerAppAttribution;
    .locals 1

    .prologue
    .line 1774369
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppAttribution:Lcom/facebook/share/model/ComposerAppAttribution;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 1774370
    iget-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1774371
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAttachments:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1774372
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTargetAlbum:Lcom/facebook/graphql/model/GraphQLAlbum;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1774373
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mLocationInfo:Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1774374
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserSelectedTags:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1774375
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMinutiaeObject:Lcom/facebook/composer/minutiae/model/MinutiaeObject;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1774376
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mProductItemAttachment:Lcom/facebook/ipc/composer/model/ProductItemAttachment;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1774377
    iget-wide v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mMarketplaceId:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 1774378
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTaggedProfiles:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    .line 1774379
    iget v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mRating:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1774380
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mTextWithEntities:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Parcel;Lcom/facebook/flatbuffers/Flattenable;)V

    .line 1774381
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mHasUserInteracted:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1774382
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mPublishMode:LX/5Rn;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Enum;)V

    .line 1774383
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mScheduleTime:Ljava/lang/Long;

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Ljava/lang/Long;)V

    .line 1774384
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mRemovedURLs:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    .line 1774385
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->a:Lcom/facebook/composer/lifeevent/model/ComposerLifeEventModel;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1774386
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mComposerDateInfo:Lcom/facebook/ipc/composer/model/ComposerDateInfo;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1774387
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->b:LX/0Px;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 1774388
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->c:LX/0P1;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeMap(Ljava/util/Map;)V

    .line 1774389
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mReferencedStickerData:Lcom/facebook/ipc/composer/model/ComposerStickerData;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1774390
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mShareParams:Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1774391
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsBackoutDraft:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1774392
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppAttribution:Lcom/facebook/share/model/ComposerAppAttribution;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 1774393
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppProvidedHashtag:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 1774394
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserDeletedAppProvidedHashtag:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1774395
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsFeedOnlyPost:Z

    invoke-static {p1, v0}, LX/46R;->a(Landroid/os/Parcel;Z)V

    .line 1774396
    return-void

    .line 1774397
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1774398
    iget-object v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mAppProvidedHashtag:Ljava/lang/String;

    return-object v0
.end method

.method public final y()Z
    .locals 1

    .prologue
    .line 1774399
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mUserDeletedAppProvidedHashtag:Z

    return v0
.end method

.method public final z()Z
    .locals 1

    .prologue
    .line 1774400
    iget-boolean v0, p0, Lcom/facebook/platform/composer/model/PlatformComposition;->mIsFeedOnlyPost:Z

    return v0
.end method
