.class public Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;
.super Lcom/facebook/ui/dialogs/FbDialogFragment;
.source ""


# instance fields
.field public m:Landroid/view/View$OnClickListener;

.field public n:Landroid/view/View$OnClickListener;

.field public o:Landroid/content/DialogInterface$OnDismissListener;

.field public p:Landroid/content/DialogInterface$OnCancelListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1773937
    invoke-direct {p0}, Lcom/facebook/ui/dialogs/FbDialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 1773938
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->a(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 1773939
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 1773940
    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1773941
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 1773942
    iget-object v0, p0, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->p:Landroid/content/DialogInterface$OnCancelListener;

    if-eqz v0, :cond_0

    .line 1773943
    iget-object v0, p0, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->p:Landroid/content/DialogInterface$OnCancelListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnCancelListener;->onCancel(Landroid/content/DialogInterface;)V

    .line 1773944
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x53dd0f9a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1773945
    invoke-super {p0, p1, p2, p3}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 1773946
    const v1, 0x7f030fb9

    invoke-virtual {p1, v1, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 1773947
    const v2, 0x7f0d25f7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1773948
    const v3, 0x7f0d25f8

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1773949
    const v4, 0x7f0d25f9

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 1773950
    iget-object v5, p0, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->m:Landroid/view/View$OnClickListener;

    if-eqz v5, :cond_0

    .line 1773951
    iget-object v5, p0, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1773952
    :goto_0
    new-instance v2, LX/BKY;

    invoke-direct {v2, p0}, LX/BKY;-><init>(Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1773953
    iget-object v2, p0, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->n:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_1

    .line 1773954
    iget-object v2, p0, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1773955
    :goto_1
    const v2, -0x648874cb

    invoke-static {v2, v0}, LX/02F;->f(II)V

    return-object v1

    .line 1773956
    :cond_0
    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1773957
    :cond_1
    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 1773958
    invoke-super {p0, p1}, Lcom/facebook/ui/dialogs/FbDialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1773959
    iget-object v0, p0, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->o:Landroid/content/DialogInterface$OnDismissListener;

    if-eqz v0, :cond_0

    .line 1773960
    iget-object v0, p0, Lcom/facebook/platform/composer/draftpost/DraftPostComposerCancelDialogFragment;->o:Landroid/content/DialogInterface$OnDismissListener;

    invoke-interface {v0, p1}, Landroid/content/DialogInterface$OnDismissListener;->onDismiss(Landroid/content/DialogInterface;)V

    .line 1773961
    :cond_0
    return-void
.end method
