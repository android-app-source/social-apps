.class public Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;
.super Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;
.source ""


# instance fields
.field private c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1775399
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1775400
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082496

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;->b:Ljava/lang/String;

    .line 1775401
    return-void
.end method

.method public final c()Lcom/facebook/base/fragment/FbFragment;
    .locals 2

    .prologue
    .line 1775402
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    if-nez v0, :cond_0

    .line 1775403
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1775404
    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    .line 1775405
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;->a:LX/BJU;

    .line 1775406
    iput-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->m:LX/BJU;

    .line 1775407
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    const/4 v1, 0x1

    .line 1775408
    iput-boolean v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->k:Z

    .line 1775409
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    return-object v0
.end method
