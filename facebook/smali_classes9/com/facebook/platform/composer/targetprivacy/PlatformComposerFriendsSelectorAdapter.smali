.class public Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;
.super LX/9UC;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final k:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public final c:Landroid/widget/Filter;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1775290
    const-class v0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;->k:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;LX/2Rd;LX/0ad;)V
    .locals 1
    .param p2    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 1775291
    invoke-direct {p0, p1, p2, p3, p4}, LX/9UC;-><init>(Landroid/content/Context;Landroid/database/Cursor;LX/2Rd;LX/0ad;)V

    .line 1775292
    new-instance v0, LX/BL7;

    invoke-direct {v0, p0, p1}, LX/BL7;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;->c:Landroid/widget/Filter;

    .line 1775293
    return-void
.end method


# virtual methods
.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1775294
    invoke-virtual {p0, p1, p2}, LX/9UC;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1775295
    if-nez p4, :cond_0

    .line 1775296
    iget-object v1, p0, LX/9UB;->g:Landroid/view/LayoutInflater;

    const v2, 0x7f030fc6

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 1775297
    :cond_0
    const v1, 0x7f0d1602

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1775298
    const v2, 0x7f0d2616

    invoke-virtual {p4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 1775299
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1775300
    iget-object v3, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1775301
    :goto_0
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1775302
    return-object p4

    .line 1775303
    :cond_1
    sget-object v3, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;->k:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v6, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 1775304
    if-nez p2, :cond_0

    .line 1775305
    new-instance p2, Landroid/view/View;

    iget-object v0, p0, LX/9UB;->f:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1775306
    :cond_0
    return-object p2
.end method
