.class public abstract Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:LX/BJU;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1775134
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract b()V
.end method

.method public abstract c()Lcom/facebook/base/fragment/FbFragment;
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, -0x59348400

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1775135
    const v0, 0x7f030fc7

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1775136
    invoke-virtual {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;->b()V

    .line 1775137
    const v1, 0x7f0d0a0a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    .line 1775138
    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1775139
    const v1, 0x7f0d0508

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1775140
    new-instance v3, LX/BLc;

    invoke-direct {v3, p0}, LX/BLc;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1775141
    iget-object v1, p0, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v1

    .line 1775142
    invoke-virtual {v1}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 1775143
    const v3, 0x7f0d2617

    invoke-virtual {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;->c()Lcom/facebook/base/fragment/FbFragment;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1775144
    invoke-virtual {v1}, LX/0hH;->b()I

    .line 1775145
    const/16 v1, 0x2b

    const v3, 0x37f37cd7

    invoke-static {v5, v1, v3, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v0
.end method
