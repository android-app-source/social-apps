.class public Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;
.super Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;
.source ""


# instance fields
.field private c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1775156
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .prologue
    .line 1775146
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f082495

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;->b:Ljava/lang/String;

    .line 1775147
    return-void
.end method

.method public final c()Lcom/facebook/base/fragment/FbFragment;
    .locals 2

    .prologue
    .line 1775148
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    if-nez v0, :cond_0

    .line 1775149
    iget-object v0, p0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v0, v0

    .line 1775150
    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    .line 1775151
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerProfileSearchFragment;->a:LX/BJU;

    .line 1775152
    iput-object v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->p:LX/BJU;

    .line 1775153
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    const/4 v1, 0x1

    .line 1775154
    iput-boolean v1, v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->k:Z

    .line 1775155
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendSearchFragment;->c:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    return-object v0
.end method
