.class public Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# static fields
.field private static final k:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Landroid/widget/AbsListView$OnScrollListener;

.field public final B:Landroid/text/TextWatcher;

.field public final C:Landroid/widget/AdapterView$OnItemClickListener;

.field public a:LX/8Rp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/8S9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Or;
    .annotation runtime Lcom/facebook/privacy/gating/IsFullCustomPrivacyEnabled;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Sy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Landroid/view/inputmethod/InputMethodManager;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/03V;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/2c9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public i:LX/8tB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment$DataProvider;",
            ">;"
        }
    .end annotation
.end field

.field public m:Lcom/facebook/privacy/model/SelectablePrivacyData;

.field public n:LX/BJX;

.field public o:Landroid/view/View;

.field public p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

.field public q:Landroid/view/View;

.field public r:Landroid/view/View;

.field public s:Landroid/view/View;

.field public t:Lcom/facebook/widget/listview/BetterListView;

.field private u:Z

.field public v:Z

.field public w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

.field public x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;"
        }
    .end annotation
.end field

.field public z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1775888
    const-class v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    sput-object v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->k:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1775889
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1775890
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    .line 1775891
    new-instance v0, LX/BLM;

    invoke-direct {v0, p0}, LX/BLM;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->A:Landroid/widget/AbsListView$OnScrollListener;

    .line 1775892
    new-instance v0, LX/BLQ;

    invoke-direct {v0, p0}, LX/BLQ;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->B:Landroid/text/TextWatcher;

    .line 1775893
    new-instance v0, LX/BLR;

    invoke-direct {v0, p0}, LX/BLR;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->C:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1775894
    return-void
.end method

.method public static A(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QO;
    .locals 2

    .prologue
    .line 1775895
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    sget v1, LX/BLY;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1775896
    invoke-virtual {v0}, LX/8Rn;->h()LX/8QO;

    move-result-object v0

    return-object v0
.end method

.method public static B(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QX;
    .locals 2

    .prologue
    .line 1775897
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    sget v1, LX/BLY;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1775898
    invoke-virtual {v0}, LX/8Rn;->i()LX/8QX;

    move-result-object v0

    return-object v0
.end method

.method public static C(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 3

    .prologue
    .line 1775899
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->f:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1775900
    return-void
.end method

.method private D()LX/BKO;
    .locals 1

    .prologue
    .line 1775901
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->l:Ljava/lang/ref/WeakReference;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1775902
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->l:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BKO;

    return-object v0
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;"
        }
    .end annotation

    .prologue
    .line 1775903
    new-instance v1, LX/8QP;

    invoke-direct {v1}, LX/8QP;-><init>()V

    .line 1775904
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne p1, v0, :cond_0

    .line 1775905
    const-string v0, "{\"value\":\"SELF\"}"

    invoke-virtual {v1, v0}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    .line 1775906
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1775907
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1775908
    if-eqz p2, :cond_1

    .line 1775909
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1775910
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775911
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775912
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->a(Ljava/lang/String;)LX/8QP;

    goto :goto_1

    .line 1775913
    :cond_0
    const-string v0, "{\"value\":\"ALL_FRIENDS\"}"

    invoke-virtual {v1, v0}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    goto :goto_0

    .line 1775914
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 1775915
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 1775916
    if-eqz p3, :cond_2

    .line 1775917
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1775918
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775919
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775920
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->b(Ljava/lang/String;)LX/8QP;

    goto :goto_2

    .line 1775921
    :cond_2
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    const-string v6, "custom"

    .line 1775922
    iput-object v6, v0, LX/2dc;->f:Ljava/lang/String;

    .line 1775923
    move-object v0, v0

    .line 1775924
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1775925
    new-instance v6, LX/4YK;

    invoke-direct {v6}, LX/4YK;-><init>()V

    .line 1775926
    iput-object p1, v6, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1775927
    move-object v6, v6

    .line 1775928
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1775929
    iput-object v3, v6, LX/4YK;->b:LX/0Px;

    .line 1775930
    move-object v3, v6

    .line 1775931
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    .line 1775932
    iput-object v5, v3, LX/4YK;->d:LX/0Px;

    .line 1775933
    move-object v3, v3

    .line 1775934
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v5

    .line 1775935
    iput-object v5, v3, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1775936
    move-object v3, v3

    .line 1775937
    invoke-virtual {v3}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    .line 1775938
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v5}, LX/8Rp;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, LX/8QP;->d(Ljava/lang/String;)LX/8QP;

    move-result-object v5

    invoke-virtual {v5, v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/8QP;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8QP;->a(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/8QP;->b(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    .line 1775939
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    if-ne p1, v0, :cond_3

    .line 1775940
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->c(LX/0Px;)LX/8QP;

    .line 1775941
    :goto_3
    invoke-virtual {v1}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    return-object v0

    .line 1775942
    :cond_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-static {v0, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->c(LX/0Px;)LX/8QP;

    goto :goto_3
.end method

.method private a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;"
        }
    .end annotation

    .prologue
    .line 1775943
    new-instance v0, LX/8QP;

    invoke-direct {v0}, LX/8QP;-><init>()V

    const-string v1, "{\"value\":\"ALL_FRIENDS\"}"

    invoke-virtual {v0, v1}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    move-result-object v1

    .line 1775944
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1775945
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1775946
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1775947
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775948
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1775949
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->b(Ljava/lang/String;)LX/8QP;

    goto :goto_0

    .line 1775950
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    const-string v4, "friends_except_acquaintances"

    .line 1775951
    iput-object v4, v0, LX/2dc;->f:Ljava/lang/String;

    .line 1775952
    move-object v0, v0

    .line 1775953
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1775954
    new-instance v4, LX/4YK;

    invoke-direct {v4}, LX/4YK;-><init>()V

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1775955
    iput-object v5, v4, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1775956
    move-object v4, v4

    .line 1775957
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1775958
    iput-object v3, v4, LX/4YK;->d:LX/0Px;

    .line 1775959
    move-object v3, v4

    .line 1775960
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v4

    .line 1775961
    iput-object v4, v3, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1775962
    move-object v3, v3

    .line 1775963
    invoke-virtual {v3}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    .line 1775964
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, p1}, LX/8Rp;->a(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/8QP;->d(Ljava/lang/String;)LX/8QP;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/8QP;

    move-result-object v0

    .line 1775965
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1775966
    invoke-virtual {v0, v1}, LX/8QP;->a(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->b(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->FRIENDS_OF_TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->c(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Landroid/text/Layout;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 1775967
    if-eqz p1, :cond_0

    .line 1775968
    invoke-virtual {p1}, Landroid/text/Layout;->getLineCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 1775969
    if-ltz v0, :cond_0

    invoke-virtual {p1, v0}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v0

    if-lez v0, :cond_0

    .line 1775970
    const/4 p1, 0x0

    .line 1775971
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    .line 1775972
    const/16 v1, 0x30

    invoke-virtual {v0, v1, p1, p1}, Landroid/widget/Toast;->setGravity(III)V

    .line 1775973
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1775974
    :cond_0
    return-void
.end method

.method public static a$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V
    .locals 7

    .prologue
    .line 1775975
    iget-object v1, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1775976
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v0

    .line 1775977
    new-instance v3, LX/BLW;

    invoke-direct {v3, p0, p1}, LX/BLW;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1775978
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1775979
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1775980
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    .line 1775981
    :goto_0
    move-object v6, v0

    .line 1775982
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1775983
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    .line 1775984
    :goto_1
    move-object v5, v0

    .line 1775985
    :goto_2
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual/range {v0 .. v6}, LX/8Rp;->a(Lcom/facebook/privacy/model/PrivacyOptionsResult;Lcom/facebook/graphql/model/GraphQLPrivacyOption;LX/0Or;Landroid/content/res/Resources;Ljava/util/List;Ljava/util/List;)LX/622;

    move-result-object v0

    .line 1775986
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    sget v2, LX/BLY;->a:I

    invoke-virtual {v1, v2, v0}, LX/8tB;->a(ILX/621;)V

    .line 1775987
    return-void

    .line 1775988
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1775989
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    .line 1775990
    :goto_3
    move-object v5, v0

    .line 1775991
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_10

    .line 1775992
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    .line 1775993
    :goto_4
    move-object v6, v0

    .line 1775994
    goto :goto_2

    .line 1775995
    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_2

    .line 1775996
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1775997
    if-nez v0, :cond_3

    .line 1775998
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1775999
    goto :goto_0

    .line 1776000
    :cond_3
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776001
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v4, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v0

    .line 1776002
    if-eqz v0, :cond_4

    .line 1776003
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776004
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1776005
    :cond_4
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1776006
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v4, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v4

    .line 1776007
    if-eqz v4, :cond_5

    .line 1776008
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    goto :goto_0

    .line 1776009
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1776010
    goto :goto_0

    .line 1776011
    :cond_6
    if-eqz p1, :cond_7

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_7

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_7

    .line 1776012
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776013
    if-nez v0, :cond_8

    .line 1776014
    :cond_7
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1776015
    goto :goto_1

    .line 1776016
    :cond_8
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776017
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v4, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v0

    .line 1776018
    if-eqz v0, :cond_9

    .line 1776019
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776020
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    goto/16 :goto_1

    .line 1776021
    :cond_9
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1776022
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v4, v0}, LX/8Rp;->b(LX/1oS;)Z

    move-result v4

    .line 1776023
    if-eqz v4, :cond_a

    .line 1776024
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    goto/16 :goto_1

    .line 1776025
    :cond_a
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1776026
    goto/16 :goto_1

    .line 1776027
    :cond_b
    if-eqz p1, :cond_c

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_c

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_c

    .line 1776028
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776029
    if-nez v0, :cond_d

    .line 1776030
    :cond_c
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1776031
    goto/16 :goto_3

    .line 1776032
    :cond_d
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776033
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v4, v0}, LX/8Rp;->c(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1776034
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776035
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    goto/16 :goto_3

    .line 1776036
    :cond_e
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1776037
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v4, v0}, LX/8Rp;->c(LX/1oS;)Z

    move-result v4

    if-eqz v4, :cond_f

    .line 1776038
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    goto/16 :goto_3

    .line 1776039
    :cond_f
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1776040
    goto/16 :goto_3

    .line 1776041
    :cond_10
    if-eqz p1, :cond_11

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v0, :cond_11

    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    if-eqz v0, :cond_11

    .line 1776042
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776043
    if-nez v0, :cond_12

    .line 1776044
    :cond_11
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1776045
    goto/16 :goto_4

    .line 1776046
    :cond_12
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776047
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v4, v0}, LX/8Rp;->d(LX/1oS;)Z

    move-result v0

    .line 1776048
    if-eqz v0, :cond_13

    .line 1776049
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v0

    .line 1776050
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    goto/16 :goto_4

    .line 1776051
    :cond_13
    iget-object v0, p1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v0, v0, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1776052
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v4, v0}, LX/8Rp;->d(LX/1oS;)Z

    move-result v4

    .line 1776053
    if-eqz v4, :cond_14

    .line 1776054
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    goto/16 :goto_4

    .line 1776055
    :cond_14
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 1776056
    goto/16 :goto_4
.end method

.method public static a$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1776057
    iput-object p1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    .line 1776058
    iput-object p2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    .line 1776059
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    invoke-direct {p0, v1, v2, v3}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776060
    return-void
.end method

.method private b(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)",
            "Lcom/facebook/graphql/model/GraphQLPrivacyOption;"
        }
    .end annotation

    .prologue
    .line 1776061
    new-instance v0, LX/8QP;

    invoke-direct {v0}, LX/8QP;-><init>()V

    const-string v1, "{\"value\":\"SELF\"}"

    invoke-virtual {v0, v1}, LX/8QP;->c(Ljava/lang/String;)LX/8QP;

    move-result-object v1

    .line 1776062
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 1776063
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 1776064
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1776065
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1776066
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 1776067
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/8QP;->a(Ljava/lang/String;)LX/8QP;

    goto :goto_0

    .line 1776068
    :cond_0
    new-instance v0, LX/2dc;

    invoke-direct {v0}, LX/2dc;-><init>()V

    const-string v4, "custom"

    .line 1776069
    iput-object v4, v0, LX/2dc;->f:Ljava/lang/String;

    .line 1776070
    move-object v0, v0

    .line 1776071
    invoke-virtual {v0}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    .line 1776072
    new-instance v4, LX/4YK;

    invoke-direct {v4}, LX/4YK;-><init>()V

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1776073
    iput-object v5, v4, LX/4YK;->c:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    .line 1776074
    move-object v4, v4

    .line 1776075
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 1776076
    iput-object v3, v4, LX/4YK;->b:LX/0Px;

    .line 1776077
    move-object v3, v4

    .line 1776078
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object v4

    .line 1776079
    iput-object v4, v3, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1776080
    move-object v3, v3

    .line 1776081
    invoke-virtual {v3}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    .line 1776082
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, p1}, LX/8Rp;->b(Landroid/content/res/Resources;Ljava/util/List;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, LX/8QP;->d(Ljava/lang/String;)LX/8QP;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/8QP;

    move-result-object v0

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->a(LX/0Px;)LX/8QP;

    move-result-object v0

    .line 1776083
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 1776084
    invoke-virtual {v0, v1}, LX/8QP;->b(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QP;->c(LX/0Px;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;
    .locals 1

    .prologue
    .line 1776085
    new-instance v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-direct {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;-><init>()V

    .line 1776086
    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1776087
    return-object v0
.end method

.method public static b(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1776088
    invoke-virtual {p0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokenSpans()[LX/8uk;

    move-result-object v0

    check-cast v0, [LX/8ul;

    .line 1776089
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1776090
    array-length v3, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    .line 1776091
    iget-object p0, v4, LX/8uk;->f:LX/8QK;

    move-object v4, p0

    .line 1776092
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1776093
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1776094
    :cond_0
    return-object v2
.end method

.method public static b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;LX/8QL;)V
    .locals 2

    .prologue
    .line 1776095
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Treating non tag expansion token as a tag expansion token"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 1776096
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelection(I)V

    .line 1776097
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1776098
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1776099
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->c()V

    .line 1776100
    return-void

    .line 1776101
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/8QL;)Z
    .locals 2

    .prologue
    .line 1776102
    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->PRIVACY:LX/8vA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->FULL_CUSTOM:LX/8vA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/8QL;->a:LX/8vA;

    sget-object v1, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8QL;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1776103
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_1

    .line 1776104
    :cond_0
    :goto_0
    return-void

    .line 1776105
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1776106
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_0

    .line 1776107
    :cond_2
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1776108
    iget-object v1, v0, LX/8QL;->a:LX/8vA;

    sget-object v2, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v1, v2, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    .line 1776109
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1776110
    :cond_3
    move-object v0, v0

    .line 1776111
    sget-object v1, LX/BLP;->a:[I

    iget-object v2, v0, LX/8QL;->a:LX/8vA;

    invoke-virtual {v2}, LX/8vA;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1776112
    sget-object v1, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->k:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unexpected selected option token of type %s"

    iget-object v0, v0, LX/8QL;->a:LX/8vA;

    invoke-virtual {v0}, LX/8vA;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v0

    invoke-virtual {v0}, LX/0VK;->g()LX/0VG;

    move-result-object v0

    .line 1776113
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->g:LX/03V;

    invoke-virtual {v1, v0}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 1776114
    :pswitch_0
    new-instance v1, LX/8QV;

    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v1, v2}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    check-cast v0, LX/8QM;

    .line 1776115
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v2, v2, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    invoke-virtual {v0}, LX/8QM;->n()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->a(I)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    .line 1776116
    invoke-static {v2}, LX/2cA;->h(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    invoke-static {v3}, LX/4YK;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/4YK;

    move-result-object v3

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    move-result-object p1

    .line 1776117
    iput-object p1, v3, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1776118
    move-object v3, v3

    .line 1776119
    invoke-virtual {v3}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v3

    .line 1776120
    invoke-static {v2}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v2

    invoke-virtual {v2}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v2

    move-object v0, v2

    .line 1776121
    invoke-virtual {v1, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto/16 :goto_0

    .line 1776122
    :pswitch_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1776123
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;->SELF:Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;

    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    invoke-direct {p0, v1, v2, v3}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyBaseState;Ljava/util/List;Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto/16 :goto_0

    .line 1776124
    :pswitch_2
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1776125
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto/16 :goto_0

    .line 1776126
    :pswitch_3
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 1776127
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static e(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1776128
    iput-object p1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    .line 1776129
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776130
    return-void
.end method

.method public static e$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 3

    .prologue
    .line 1775583
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775584
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775585
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1775586
    invoke-static {v0}, LX/2cA;->h(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    invoke-static {v0}, LX/4YK;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/4YK;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1775587
    iput-object v1, v0, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1775588
    move-object v0, v0

    .line 1775589
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    .line 1775590
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775591
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1775592
    invoke-static {v1}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1775593
    new-instance v1, LX/8QV;

    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v1, v2}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v2, 0x1

    .line 1775594
    iput-boolean v2, v1, LX/8QV;->c:Z

    .line 1775595
    move-object v1, v1

    .line 1775596
    invoke-virtual {v1, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775597
    return-void
.end method

.method public static f(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1775885
    iput-object p1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    .line 1775886
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-direct {p0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Ljava/util/List;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775887
    return-void
.end method

.method public static k(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 3

    .prologue
    .line 1776131
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->D()LX/BKO;

    move-result-object v0

    .line 1776132
    iget-object v1, v0, LX/BKO;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/BKO;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v1, :cond_4

    .line 1776133
    iget-object v1, v0, LX/BKO;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->d:Lcom/facebook/composer/privacy/model/ComposerPrivacyData;

    iget-object v1, v1, Lcom/facebook/composer/privacy/model/ComposerPrivacyData;->b:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776134
    :goto_0
    move-object v0, v1

    .line 1776135
    invoke-static {p0, v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    .line 1776136
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-boolean v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    iget-boolean v2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776137
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1776138
    iget-object v2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v2, v2

    .line 1776139
    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v1, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v1, v1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    iget-object v2, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    iget-object v2, v2, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1776140
    :goto_1
    return-void

    .line 1776141
    :cond_0
    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776142
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776143
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1776144
    invoke-virtual {v0, v1}, LX/8Rp;->c(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1776145
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776146
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1776147
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->e(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;)V

    .line 1776148
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776149
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1776150
    invoke-virtual {v0, v1}, LX/8Rp;->d(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1776151
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776152
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1776153
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->f(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;)V

    .line 1776154
    :cond_2
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776155
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1776156
    invoke-virtual {v0, v1}, LX/8Rp;->b(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1776157
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776158
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1776159
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->j()LX/0Px;

    move-result-object v0

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1776160
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1776161
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->x_()LX/0Px;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;Ljava/util/List;)V

    .line 1776162
    :cond_3
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->l(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    goto :goto_1

    :cond_4
    iget-object v1, v0, LX/BKO;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->ah:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto/16 :goto_0
.end method

.method public static l(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 4

    .prologue
    .line 1775559
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_1

    .line 1775560
    :cond_0
    :goto_0
    return-void

    .line 1775561
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775562
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v1

    .line 1775563
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/SelectablePrivacyData;->c()I

    move-result v2

    .line 1775564
    if-eqz v1, :cond_0

    .line 1775565
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b()V

    .line 1775566
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v0, v1}, LX/8Rp;->b(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1775567
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QN;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1775568
    :goto_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    .line 1775569
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1775570
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1775571
    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c(LX/8QL;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1775572
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->n(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775573
    :cond_2
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    goto :goto_0

    .line 1775574
    :cond_3
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v0, v1}, LX/8Rp;->c(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1775575
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->A(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QO;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_1

    .line 1775576
    :cond_4
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v0, v1}, LX/8Rp;->d(LX/1oS;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1775577
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->B(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QX;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_1

    .line 1775578
    :cond_5
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    sget v3, LX/BLY;->a:I

    invoke-virtual {v0, v3}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1775579
    invoke-virtual {v0, v2}, LX/8Rn;->a(I)LX/8QM;

    move-result-object v0

    .line 1775580
    if-nez v0, :cond_6

    .line 1775581
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v0, v1, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v0

    .line 1775582
    :cond_6
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    goto :goto_1
.end method

.method public static m(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 3

    .prologue
    .line 1775598
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QY;

    move-result-object v0

    .line 1775599
    if-eqz v0, :cond_0

    .line 1775600
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 1775601
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1775602
    :cond_0
    return-void
.end method

.method public static n(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1775603
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-nez v0, :cond_1

    .line 1775604
    :cond_0
    :goto_0
    return-void

    .line 1775605
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v0}, Lcom/facebook/widget/listview/BetterListView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    .line 1775606
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    .line 1775607
    :cond_2
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775608
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1775609
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1775610
    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c(LX/8QL;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1775611
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    sget v4, LX/BLY;->a:I

    invoke-virtual {v1, v4}, LX/8tB;->g(I)LX/621;

    move-result-object v1

    check-cast v1, LX/8Rn;

    .line 1775612
    invoke-virtual {v1}, LX/8Rn;->j()V

    .line 1775613
    iget-object v4, v0, LX/8QL;->a:LX/8vA;

    sget-object v5, LX/8vA;->FULL_CUSTOM:LX/8vA;

    if-ne v4, v5, :cond_5

    .line 1775614
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QN;

    move-result-object v0

    .line 1775615
    :goto_1
    iget-boolean v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->u:Z

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v4}, Lcom/facebook/privacy/model/SelectablePrivacyData;->f()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1775616
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775617
    iget-object v5, v4, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v4, v5

    .line 1775618
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->l()LX/0Px;

    move-result-object v4

    .line 1775619
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-le v4, v2, :cond_8

    .line 1775620
    :goto_2
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    iget-object v5, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v5}, Lcom/facebook/privacy/model/SelectablePrivacyData;->g()Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;

    move-result-object v5

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v3, v2}, LX/8Rp;->a(Lcom/facebook/graphql/enums/GraphQLPrivacyOptionTagExpansionType;Landroid/content/res/Resources;ZZ)LX/8QY;

    move-result-object v3

    .line 1775621
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v4}, Lcom/facebook/widget/listview/BetterListView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3

    .line 1775622
    invoke-virtual {v1}, LX/622;->e()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 1775623
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v3, v0}, LX/8Rn;->a(LX/8QY;I)V

    .line 1775624
    :cond_3
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    const v1, 0x2c871daa

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1775625
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775626
    iget-boolean v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v0, v1

    .line 1775627
    if-eqz v0, :cond_4

    if-nez v2, :cond_0

    .line 1775628
    :cond_4
    invoke-static {p0, v3}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;LX/8QL;)V

    goto/16 :goto_0

    .line 1775629
    :cond_5
    iget-object v4, v0, LX/8QL;->a:LX/8vA;

    sget-object v5, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    if-ne v4, v5, :cond_6

    .line 1775630
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->A(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QO;

    move-result-object v0

    goto :goto_1

    .line 1775631
    :cond_6
    iget-object v0, v0, LX/8QL;->a:LX/8vA;

    sget-object v4, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    if-ne v0, v4, :cond_7

    .line 1775632
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->B(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QX;

    move-result-object v0

    goto :goto_1

    .line 1775633
    :cond_7
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-virtual {v0}, Lcom/facebook/privacy/model/SelectablePrivacyData;->c()I

    move-result v0

    invoke-virtual {v1, v0}, LX/8Rn;->a(I)LX/8QM;

    move-result-object v0

    goto :goto_1

    :cond_8
    move v2, v3

    .line 1775634
    goto :goto_2
.end method

.method public static o(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QY;
    .locals 4

    .prologue
    .line 1775635
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 1775636
    iget-object v2, v0, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v2, v3, :cond_0

    .line 1775637
    check-cast v0, LX/8QY;

    .line 1775638
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 1

    .prologue
    .line 1775639
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775640
    iget-object p0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, p0

    .line 1775641
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 1775642
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    .line 1775643
    :cond_0
    :goto_0
    return-void

    .line 1775644
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1775645
    :cond_2
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;

    .line 1775646
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    const v3, 0xe198c7c

    if-ne v2, v3, :cond_0

    .line 1775647
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, LX/8QR;->SOME_FRIENDS:LX/8QR;

    invoke-virtual {v3}, LX/8QR;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyAudienceMember;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1775648
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1775649
    iget-object v4, v3, Lcom/facebook/privacy/model/PrivacyOptionsResult;->friendListPrivacyOptions:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_0

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1775650
    invoke-static {v0}, LX/2cA;->a(LX/1oR;)Lcom/facebook/privacy/model/PrivacyParameter;

    move-result-object v6

    .line 1775651
    if-eqz v6, :cond_3

    iget-object v6, v6, Lcom/facebook/privacy/model/PrivacyParameter;->allow:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1775652
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v3, v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v0

    .line 1775653
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b()V

    .line 1775654
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1775655
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1775656
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1775657
    invoke-static {p0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;)V

    .line 1775658
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->n(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775659
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e()V

    .line 1775660
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-static {p0, v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    goto/16 :goto_0

    .line 1775661
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public static s(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V
    .locals 2

    .prologue
    .line 1775662
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    if-eqz v0, :cond_0

    .line 1775663
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    .line 1775664
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1775665
    :cond_1
    :goto_0
    return-void

    .line 1775666
    :cond_2
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->D()LX/BKO;

    move-result-object v0

    .line 1775667
    iget-object v1, v0, LX/BKO;->a:Lcom/facebook/platform/composer/composer/PlatformComposerFragment;

    iget-object v1, v1, Lcom/facebook/platform/composer/composer/PlatformComposerFragment;->I:LX/BKm;

    iget-object v1, v1, LX/BKm;->c:Lcom/facebook/platform/composer/model/PlatformComposition;

    invoke-virtual {v1}, Lcom/facebook/platform/composer/model/PlatformComposition;->s()LX/0Rf;

    move-result-object v1

    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 1775668
    iget-boolean v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->u:Z

    if-eq v0, v1, :cond_1

    .line 1775669
    iput-boolean v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->u:Z

    .line 1775670
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->n(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775671
    iget-boolean v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->u:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_1

    .line 1775672
    new-instance v0, LX/8QV;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v0, v1}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v1, 0x1

    .line 1775673
    iput-boolean v1, v0, LX/8QV;->c:Z

    .line 1775674
    move-object v0, v0

    .line 1775675
    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static t(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;
    .locals 1

    .prologue
    .line 1775676
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775677
    iget-boolean p0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->b:Z

    move v0, p0

    .line 1775678
    if-eqz v0, :cond_0

    .line 1775679
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->TAGGEES:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1775680
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    goto :goto_0
.end method

.method private x()LX/8QM;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1775681
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1775682
    :goto_0
    return-object v0

    .line 1775683
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1775684
    iget-object v4, v3, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1775685
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v6, v7, :cond_2

    .line 1775686
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v3, v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v0

    goto :goto_0

    .line 1775687
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1775688
    goto :goto_0
.end method

.method private y()LX/8QM;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1775689
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v0, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1775690
    :goto_0
    return-object v0

    .line 1775691
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    iget-object v3, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 1775692
    iget-object v4, v3, Lcom/facebook/privacy/model/PrivacyOptionsResult;->basicPrivacyOptions:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_3

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 1775693
    invoke-static {v0}, LX/2cA;->a(LX/1oV;)Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;->ONLY_ME:Lcom/facebook/graphql/enums/GraphQLPrivacyOptionType;

    if-ne v6, v7, :cond_2

    .line 1775694
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    invoke-virtual {v3, v0}, Lcom/facebook/privacy/model/PrivacyOptionsResult;->b(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/8Rp;->a(LX/1oT;I)LX/8QM;

    move-result-object v0

    goto :goto_0

    .line 1775695
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1775696
    goto :goto_0
.end method

.method public static z(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)LX/8QN;
    .locals 2

    .prologue
    .line 1775697
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    sget v1, LX/BLY;->a:I

    invoke-virtual {v0, v1}, LX/8tB;->g(I)LX/621;

    move-result-object v0

    check-cast v0, LX/8Rn;

    .line 1775698
    invoke-virtual {v0}, LX/8Rn;->g()LX/8QN;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/8QL;Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)V
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/16 v4, 0x8

    .line 1775699
    invoke-static {p2}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v2

    .line 1775700
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->FULL_CUSTOM:LX/8vA;

    if-ne v0, v3, :cond_6

    .line 1775701
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x()LX/8QM;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1775702
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x()LX/8QM;

    move-result-object p1

    .line 1775703
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 1775704
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v0

    .line 1775705
    const v3, 0x7f0d2608

    invoke-virtual {v0, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-nez v3, :cond_f

    .line 1775706
    new-instance v3, Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-direct {v3}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;-><init>()V

    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    .line 1775707
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    .line 1775708
    const v4, 0x7f0d2608

    iget-object v5, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-virtual {v3, v4, v5}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1775709
    invoke-virtual {v3}, LX/0hH;->b()I

    .line 1775710
    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 1775711
    :goto_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v3, LX/BLC;

    invoke-direct {v3, p0}, LX/BLC;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775712
    iput-object v3, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->t:LX/8Qe;

    .line 1775713
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v3, LX/BLD;

    invoke-direct {v3, p0}, LX/BLD;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775714
    iput-object v3, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->u:LX/0RV;

    .line 1775715
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v3, LX/BLE;

    invoke-direct {v3, p0}, LX/BLE;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775716
    iput-object v3, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->w:LX/8Qe;

    .line 1775717
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v3, LX/BLF;

    invoke-direct {v3, p0}, LX/BLF;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775718
    iput-object v3, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->x:LX/0RV;

    .line 1775719
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    new-instance v3, LX/BLG;

    invoke-direct {v3, p0}, LX/BLG;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775720
    iput-object v3, v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->y:LX/0RV;

    .line 1775721
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-virtual {v0}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->b()V

    .line 1775722
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    const v3, 0x7f0d2608

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q:Landroid/view/View;

    .line 1775723
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1775724
    :cond_2
    :goto_1
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-eq v0, v3, :cond_3

    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->PRIVACY:LX/8vA;

    if-eq v0, v3, :cond_4

    :cond_3
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->FULL_CUSTOM:LX/8vA;

    if-eq v0, v3, :cond_4

    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    if-eq v0, v3, :cond_4

    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    if-ne v0, v3, :cond_5

    .line 1775725
    :cond_4
    invoke-virtual {p2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->b()V

    .line 1775726
    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 1775727
    :cond_5
    const/4 v0, 0x0

    .line 1775728
    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    .line 1775729
    invoke-virtual {p2, p1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 1775730
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1775731
    :goto_2
    invoke-static {p0, v2}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;Ljava/util/List;)V

    .line 1775732
    iget-object v1, p1, LX/8QL;->a:LX/8vA;

    sget-object v2, LX/8vA;->TAG_EXPANSION:LX/8vA;

    if-ne v1, v2, :cond_e

    .line 1775733
    if-eqz v0, :cond_d

    .line 1775734
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->e$redex0(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775735
    :goto_3
    invoke-virtual {p2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e()V

    .line 1775736
    return-void

    .line 1775737
    :cond_6
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->FRIENDS_EXCEPT:LX/8vA;

    if-ne v0, v3, :cond_9

    .line 1775738
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x()LX/8QM;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 1775739
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->x()LX/8QM;

    move-result-object p1

    .line 1775740
    :cond_7
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->r:Landroid/view/View;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->r:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 1775741
    :cond_8
    const/4 v6, 0x0

    .line 1775742
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    .line 1775743
    const v0, 0x7f0d2609

    invoke-virtual {v3, v0}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_10

    .line 1775744
    invoke-static {v6}, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;->a(Z)Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    move-result-object v0

    .line 1775745
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v4

    invoke-virtual {v4}, LX/0gc;->a()LX/0hH;

    move-result-object v4

    .line 1775746
    const v5, 0x7f0d2609

    invoke-virtual {v4, v5, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1775747
    invoke-virtual {v4}, LX/0hH;->b()I

    .line 1775748
    invoke-virtual {v3}, LX/0gc;->b()Z

    .line 1775749
    :goto_4
    new-instance v3, LX/BLH;

    invoke-direct {v3, p0}, LX/BLH;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(LX/8Qe;)V

    .line 1775750
    new-instance v3, LX/BLI;

    invoke-direct {v3, p0}, LX/BLI;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775751
    iput-object v3, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    .line 1775752
    new-instance v3, LX/BLJ;

    invoke-direct {v3, p0}, LX/BLJ;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775753
    iput-object v3, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->x:LX/0RV;

    .line 1775754
    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->c()V

    .line 1775755
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    const v3, 0x7f0d2609

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->r:Landroid/view/View;

    .line 1775756
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->r:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1775757
    goto/16 :goto_1

    .line 1775758
    :cond_9
    iget-object v0, p1, LX/8QL;->a:LX/8vA;

    sget-object v3, LX/8vA;->SPECIFIC_FRIENDS:LX/8vA;

    if-ne v0, v3, :cond_2

    .line 1775759
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y()LX/8QM;

    move-result-object v0

    if-eqz v0, :cond_a

    .line 1775760
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y()LX/8QM;

    move-result-object p1

    .line 1775761
    :cond_a
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s:Landroid/view/View;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 1775762
    :cond_b
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    .line 1775763
    const v0, 0x7f0d260a

    invoke-virtual {v3, v0}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_11

    .line 1775764
    new-instance v0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    invoke-direct {v0}, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;-><init>()V

    .line 1775765
    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v4

    invoke-virtual {v4}, LX/0gc;->a()LX/0hH;

    move-result-object v4

    .line 1775766
    const v5, 0x7f0d260a

    invoke-virtual {v4, v5, v0}, LX/0hH;->a(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 1775767
    invoke-virtual {v4}, LX/0hH;->b()I

    .line 1775768
    invoke-virtual {v3}, LX/0gc;->b()Z

    .line 1775769
    :goto_5
    new-instance v3, LX/BLK;

    invoke-direct {v3, p0}, LX/BLK;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    invoke-virtual {v0, v3}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->a(LX/8Qe;)V

    .line 1775770
    new-instance v3, LX/BLL;

    invoke-direct {v3, p0}, LX/BLL;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775771
    iput-object v3, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->w:LX/0RV;

    .line 1775772
    new-instance v3, LX/BLN;

    invoke-direct {v3, p0}, LX/BLN;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775773
    iput-object v3, v0, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->x:LX/0RV;

    .line 1775774
    invoke-virtual {v0}, Lcom/facebook/privacy/selector/AbstractCustomPrivacyTypeaheadFragment;->c()V

    .line 1775775
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    const v3, 0x7f0d260a

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s:Landroid/view/View;

    .line 1775776
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1775777
    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->h:LX/2c9;

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_6
    invoke-virtual {v3, v0}, LX/2c9;->a(I)V

    .line 1775778
    goto/16 :goto_1

    .line 1775779
    :cond_c
    invoke-virtual {p2, p1, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;Z)V

    .line 1775780
    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move v0, v1

    .line 1775781
    goto/16 :goto_2

    .line 1775782
    :cond_d
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775783
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775784
    iget-object v1, v0, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v0, v1

    .line 1775785
    invoke-static {v0}, LX/2cA;->h(LX/1oS;)Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    invoke-static {v0}, LX/4YK;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/4YK;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;->UNSPECIFIED:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1775786
    iput-object v1, v0, LX/4YK;->e:Lcom/facebook/graphql/enums/GraphQLPrivacyTagExpansionState;

    .line 1775787
    move-object v0, v0

    .line 1775788
    invoke-virtual {v0}, LX/4YK;->a()Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;

    move-result-object v0

    .line 1775789
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775790
    iget-object v2, v1, Lcom/facebook/privacy/model/SelectablePrivacyData;->d:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 1775791
    invoke-static {v1}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QP;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/8QP;->a(Lcom/facebook/graphql/model/GraphQLPrivacyRowInput;)LX/8QP;

    move-result-object v0

    invoke-virtual {v0}, LX/8QP;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v0

    .line 1775792
    new-instance v1, LX/8QV;

    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    invoke-direct {v1, v2}, LX/8QV;-><init>(Lcom/facebook/privacy/model/SelectablePrivacyData;)V

    const/4 v2, 0x0

    .line 1775793
    iput-boolean v2, v1, LX/8QV;->c:Z

    .line 1775794
    move-object v1, v1

    .line 1775795
    invoke-virtual {v1, v0}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 1775796
    invoke-static {p0, p1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;LX/8QL;)V

    .line 1775797
    goto/16 :goto_3

    .line 1775798
    :cond_e
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->n(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    goto/16 :goto_3

    .line 1775799
    :cond_f
    const v3, 0x7f0d2608

    invoke-virtual {v0, v3}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    goto/16 :goto_0

    .line 1775800
    :cond_10
    const v0, 0x7f0d2609

    invoke-virtual {v3, v0}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/FriendsExceptTypeaheadFragment;

    goto/16 :goto_4

    .line 1775801
    :cond_11
    const v0, 0x7f0d260a

    invoke-virtual {v3, v0}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/privacy/selector/SpecificFriendsTypeaheadFragment;

    goto/16 :goto_5

    .line 1775802
    :cond_12
    const/4 v0, -0x1

    goto :goto_6
.end method

.method public final a(LX/BKO;)V
    .locals 2

    .prologue
    .line 1775803
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->l:Ljava/lang/ref/WeakReference;

    .line 1775804
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    .line 1775805
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1775806
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;

    invoke-static {v0}, LX/8Rp;->b(LX/0QB;)LX/8Rp;

    move-result-object v3

    check-cast v3, LX/8Rp;

    invoke-static {v0}, LX/8S9;->c(LX/0QB;)LX/8S9;

    move-result-object v4

    check-cast v4, LX/8S9;

    const/16 v5, 0x34e

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v6

    check-cast v6, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v7

    check-cast v7, LX/0Sy;

    invoke-static {v0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v8

    check-cast v8, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v9

    check-cast v9, LX/03V;

    invoke-static {v0}, LX/2c9;->b(LX/0QB;)LX/2c9;

    move-result-object v10

    check-cast v10, LX/2c9;

    invoke-static {v0}, LX/8tB;->b(LX/0QB;)LX/8tB;

    move-result-object p1

    check-cast p1, LX/8tB;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v3, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->a:LX/8Rp;

    iput-object v4, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b:LX/8S9;

    iput-object v5, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->c:LX/0Or;

    iput-object v6, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->d:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    iput-object v7, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->e:LX/0Sy;

    iput-object v8, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->f:Landroid/view/inputmethod/InputMethodManager;

    iput-object v9, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->g:LX/03V;

    iput-object v10, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->h:LX/2c9;

    iput-object p1, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    iput-object v0, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->j:LX/0if;

    .line 1775807
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->d:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-virtual {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a()V

    .line 1775808
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v0, 0x0

    .line 1775809
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 1775810
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->w:Lcom/facebook/privacy/selector/CustomPrivacyFragment;

    invoke-virtual {v1}, Lcom/facebook/privacy/selector/CustomPrivacyFragment;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1775811
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q()V

    .line 1775812
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1775813
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->C(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775814
    :cond_0
    :goto_0
    return v0

    .line 1775815
    :cond_1
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->r:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->r:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    .line 1775816
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->r:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1775817
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->C(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    goto :goto_0

    .line 1775818
    :cond_2
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s:Landroid/view/View;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 1775819
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->q()V

    .line 1775820
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->s:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1775821
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->C(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775822
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->h:LX/2c9;

    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->y:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, LX/2c9;->b(I)V

    goto :goto_0

    .line 1775823
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c()Lcom/facebook/privacy/model/SelectablePrivacyData;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1775824
    invoke-direct {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1775825
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->k(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    :cond_0
    move v1, v2

    .line 1775826
    :goto_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    invoke-virtual {v0}, LX/8tB;->c()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 1775827
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    invoke-virtual {v0, v1}, LX/8tB;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 1775828
    invoke-interface {v0, v2}, LX/621;->a(Z)V

    .line 1775829
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1775830
    :cond_1
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    const v1, 0x9643691

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1775831
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a()V

    .line 1775832
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->clearComposingText()V

    .line 1775833
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->e()V

    .line 1775834
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-static {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b(Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    .line 1775835
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->m:Lcom/facebook/privacy/model/SelectablePrivacyData;

    return-object v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x2

    const/16 v0, 0x2a

    const v1, 0xd6472ec

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1775836
    const v1, 0x7f030fc3

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    .line 1775837
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    .line 1775838
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->b:LX/8S9;

    .line 1775839
    new-instance p1, LX/BLZ;

    invoke-direct {p1}, LX/BLZ;-><init>()V

    .line 1775840
    new-instance p2, LX/BLV;

    invoke-direct {p2, p0}, LX/BLV;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775841
    iput-object p2, p1, LX/BLZ;->a:LX/8Rf;

    .line 1775842
    move-object p1, p1

    .line 1775843
    invoke-virtual {v2, v3, p1}, LX/8tB;->a(LX/8RK;LX/8RE;)V

    .line 1775844
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    new-instance v3, LX/623;

    invoke-direct {v3}, LX/623;-><init>()V

    new-instance p1, LX/623;

    invoke-direct {p1}, LX/623;-><init>()V

    invoke-static {v3, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/8tB;->a(Ljava/util/List;)V

    .line 1775845
    const v2, 0x7f0d04af

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/listview/BetterListView;

    iput-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    .line 1775846
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1775847
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->A:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1775848
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->C:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1775849
    const v2, 0x7f0d25f1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1775850
    new-instance v3, LX/BLS;

    invoke-direct {v3, p0}, LX/BLS;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1775851
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    .line 1775852
    const v2, 0x7f0d260c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iput-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1775853
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->B:Landroid/text/TextWatcher;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1775854
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v3, LX/8ut;->NO_DROPDOWN:LX/8ut;

    .line 1775855
    iput-object v3, v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->f:LX/8ut;

    .line 1775856
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    sget-object v3, LX/8uy;->PLAIN_TEXT:LX/8uy;

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setTextMode(LX/8uy;)V

    .line 1775857
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const p1, 0x7f0a00d2

    invoke-virtual {v3, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 1775858
    iput v3, v2, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->p:I

    .line 1775859
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setLongClickable(Z)V

    .line 1775860
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-boolean v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->v:Z

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setFocusable(Z)V

    .line 1775861
    iget-boolean v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->v:Z

    if-nez v2, :cond_0

    .line 1775862
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/BLT;

    invoke-direct {v3, p0}, LX/BLT;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1775863
    :cond_0
    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    new-instance v3, LX/BLU;

    invoke-direct {v3, p0}, LX/BLU;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1775864
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    const/16 v2, 0x2b

    const v3, -0x33c12b2c    # -5.0025296E7f

    invoke-static {v4, v2, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-object v1
.end method

.method public final onDestroyView()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/16 v0, 0x2a

    const v1, 0x281fdf3e

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1775865
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 1775866
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1775867
    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    .line 1775868
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->B:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1775869
    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    .line 1775870
    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->i:LX/8tB;

    .line 1775871
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onDestroyView()V

    .line 1775872
    const/16 v1, 0x2b

    const v2, -0x75be0805

    invoke-static {v4, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, -0x4ab3d7a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1775873
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1775874
    iget-boolean v1, p0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v1, v1

    .line 1775875
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 1775876
    :cond_0
    :goto_0
    const/16 v1, 0x2b

    const v2, 0x2a55270f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 1775877
    :cond_1
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->o:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1775878
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->t:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1}, Lcom/facebook/widget/listview/BetterListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/BLX;

    invoke-direct {v2, p0}, LX/BLX;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1775879
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->k(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775880
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->z:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1775881
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->n(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775882
    :cond_2
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->l(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    .line 1775883
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    iget-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->p:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->setSelection(I)V

    .line 1775884
    invoke-static {p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;->C(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerPrivacyFragment;)V

    goto :goto_0
.end method
