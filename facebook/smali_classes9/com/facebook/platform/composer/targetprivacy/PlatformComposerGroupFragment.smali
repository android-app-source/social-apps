.class public Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

.field public b:Lcom/facebook/widget/listview/BetterListView;

.field public c:LX/BL2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0kL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/resources/ui/FbEditText;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Landroid/view/View;

.field public j:LX/BJX;

.field public k:Z

.field private final l:Landroid/widget/AdapterView$OnItemClickListener;

.field public m:LX/BJU;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1775397
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1775398
    new-instance v0, LX/BL8;

    invoke-direct {v0, p0}, LX/BL8;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->l:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public static b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;
    .locals 1

    .prologue
    .line 1775342
    new-instance v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    invoke-direct {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;-><init>()V

    .line 1775343
    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1775344
    return-object v0
.end method

.method public static b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1775384
    if-eqz p1, :cond_0

    .line 1775385
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1775386
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1775387
    :goto_0
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 1775388
    if-eqz p1, :cond_1

    move v2, v1

    .line 1775389
    :goto_1
    if-eqz p1, :cond_2

    .line 1775390
    :goto_2
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->i:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1775391
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1775392
    return-void

    .line 1775393
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1775394
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v2, v0

    .line 1775395
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1775396
    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1775381
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1775382
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;

    new-instance v3, LX/BL2;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-direct {v3, v2}, LX/BL2;-><init>(LX/0tX;)V

    move-object v2, v3

    check-cast v2, LX/BL2;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object p1

    check-cast p1, LX/0kL;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    iput-object v2, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->c:LX/BL2;

    iput-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->d:LX/1Ck;

    iput-object p1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->e:LX/0kL;

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->f:LX/0if;

    .line 1775383
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, 0x16b1d0da

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1775359
    const v0, 0x7f030fc5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1775360
    const v1, 0x7f0d2614

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1775361
    const v1, 0x7f0d2615

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    .line 1775362
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    new-instance v4, LX/BL9;

    invoke-direct {v4, p0}, LX/BL9;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;)V

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1775363
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    iget-boolean v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->k:Z

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbEditText;->setFocusable(Z)V

    .line 1775364
    iget-boolean v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->k:Z

    if-eqz v1, :cond_1

    .line 1775365
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 1775366
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    new-instance v3, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment$3;

    invoke-direct {v3, p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment$3;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v3, v4, v5}, Lcom/facebook/resources/ui/FbEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1775367
    :goto_0
    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1775368
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/listview/BetterListView;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    .line 1775369
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 1775370
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->l:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1775371
    new-instance v1, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v1, v3}, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    .line 1775372
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->a:Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1775373
    const v1, 0x7f0d0d65

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1775374
    const v1, 0x7f0d0d66

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->i:Landroid/view/View;

    .line 1775375
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_0

    .line 1775376
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f08248e

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1775377
    :cond_0
    const v1, 0x6f5c8947

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-object v0

    .line 1775378
    :cond_1
    new-instance v1, LX/BLA;

    invoke-direct {v1, p0}, LX/BLA;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;)V

    .line 1775379
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v4, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1775380
    invoke-virtual {v3, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x1972f449

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1775356
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1775357
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->d:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1775358
    const/16 v1, 0x2b

    const v2, -0x567fc99c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/16 v0, 0x2a

    const v1, 0x7480f29e

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1775345
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1775346
    const/4 v1, 0x1

    invoke-static {p0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;Z)V

    .line 1775347
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->d:LX/1Ck;

    const-string v2, "fetchGroups"

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;->c:LX/BL2;

    .line 1775348
    invoke-static {}, LX/9Tt;->a()LX/9Ts;

    move-result-object v4

    .line 1775349
    const-string v6, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v7

    invoke-virtual {v4, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 1775350
    iget-object v6, v3, LX/BL2;->a:LX/0tX;

    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    invoke-virtual {v6, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    .line 1775351
    new-instance v6, LX/BL0;

    invoke-direct {v6, v3}, LX/BL0;-><init>(LX/BL2;)V

    .line 1775352
    sget-object v7, LX/131;->INSTANCE:LX/131;

    move-object v7, v7

    .line 1775353
    invoke-static {v4, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v3, v4

    .line 1775354
    new-instance v4, LX/BLB;

    invoke-direct {v4, p0}, LX/BLB;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerGroupFragment;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 1775355
    const/16 v1, 0x2b

    const v2, -0x590d4a3

    invoke-static {v5, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
