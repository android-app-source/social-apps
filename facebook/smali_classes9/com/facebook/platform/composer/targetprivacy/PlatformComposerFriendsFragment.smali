.class public Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;
.super Lcom/facebook/base/fragment/FbFragment;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/listview/BetterListView;

.field public b:LX/1Ck;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/2Rd;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:Lcom/facebook/resources/ui/FbEditText;

.field public h:Lcom/facebook/resources/ui/FbTextView;

.field public i:Landroid/view/View;

.field public j:LX/BJX;

.field public k:Z

.field public l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

.field private m:LX/BL6;

.field public n:Ljava/lang/String;

.field private final o:Landroid/widget/AdapterView$OnItemClickListener;

.field public p:LX/BJU;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1775271
    invoke-direct {p0}, Lcom/facebook/base/fragment/FbFragment;-><init>()V

    .line 1775272
    new-instance v0, LX/BL3;

    invoke-direct {v0, p0}, LX/BL3;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->o:Landroid/widget/AdapterView$OnItemClickListener;

    .line 1775273
    return-void
.end method

.method public static b(Landroid/os/Bundle;)Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;
    .locals 1

    .prologue
    .line 1775268
    new-instance v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    invoke-direct {v0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;-><init>()V

    .line 1775269
    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 1775270
    return-object v0
.end method

.method public static b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 1775255
    if-eqz p1, :cond_0

    .line 1775256
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1775257
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1775258
    :goto_0
    const/16 v0, 0x8

    const/4 v1, 0x0

    .line 1775259
    if-eqz p1, :cond_1

    move v2, v1

    .line 1775260
    :goto_1
    if-eqz p1, :cond_2

    .line 1775261
    :goto_2
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->i:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1775262
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1775263
    return-void

    .line 1775264
    :cond_0
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 1775265
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v2, v0

    .line 1775266
    goto :goto_1

    :cond_2
    move v0, v1

    .line 1775267
    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 1775249
    invoke-super {p0, p1}, Lcom/facebook/base/fragment/FbFragment;->a(Landroid/os/Bundle;)V

    .line 1775250
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;->a(LX/0QB;)Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    move-result-object v4

    check-cast v4, Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    invoke-static {v0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v5

    check-cast v5, LX/0if;

    invoke-static {v0}, LX/2Rd;->b(LX/0QB;)LX/2Rd;

    move-result-object p1

    check-cast p1, LX/2Rd;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    iput-object v3, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->b:LX/1Ck;

    iput-object v4, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->c:Lcom/facebook/contacts/background/AddressBookPeriodicRunner;

    iput-object v5, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->d:LX/0if;

    iput-object p1, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->e:LX/2Rd;

    iput-object v0, v2, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->f:LX/0ad;

    .line 1775251
    new-instance v0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->e:LX/2Rd;

    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->f:LX/0ad;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;LX/2Rd;LX/0ad;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    .line 1775252
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    .line 1775253
    new-instance v1, LX/BL6;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, LX/BL6;-><init>(Landroid/content/Context;Ljava/lang/ref/WeakReference;)V

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->m:LX/BL6;

    .line 1775254
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x2a

    const v2, -0x7b745f7d

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 1775227
    const v0, 0x7f030fc5

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 1775228
    const v1, 0x7f0d2614

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1775229
    const v1, 0x7f0d2615

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    .line 1775230
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->n:Ljava/lang/String;

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1775231
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    new-instance v4, LX/BL4;

    invoke-direct {v4, p0}, LX/BL4;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;)V

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 1775232
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    iget-boolean v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->k:Z

    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbEditText;->setFocusable(Z)V

    .line 1775233
    iget-boolean v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->k:Z

    if-eqz v1, :cond_1

    .line 1775234
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbEditText;->requestFocus()Z

    .line 1775235
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    new-instance v3, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment$3;

    invoke-direct {v3, p0}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment$3;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v3, v4, v5}, Lcom/facebook/resources/ui/FbEditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1775236
    :goto_0
    const v1, 0x1020004

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1775237
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/listview/BetterListView;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    .line 1775238
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setEmptyView(Landroid/view/View;)V

    .line 1775239
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->o:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 1775240
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->a:Lcom/facebook/widget/listview/BetterListView;

    iget-object v3, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    invoke-virtual {v1, v3}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 1775241
    const v1, 0x7f0d0d65

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbTextView;

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    .line 1775242
    const v1, 0x7f0d0d66

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->i:Landroid/view/View;

    .line 1775243
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v1, :cond_0

    .line 1775244
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->h:Lcom/facebook/resources/ui/FbTextView;

    const v3, 0x7f082490

    invoke-virtual {v1, v3}, Lcom/facebook/resources/ui/FbTextView;->setText(I)V

    .line 1775245
    :cond_0
    const v1, 0x1b11f894

    invoke-static {v1, v2}, LX/02F;->f(II)V

    return-object v0

    .line 1775246
    :cond_1
    new-instance v1, LX/BL5;

    invoke-direct {v1, p0}, LX/BL5;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;)V

    .line 1775247
    iget-object v4, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->g:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v4, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1775248
    invoke-virtual {v3, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2a

    const v1, 0x409c6cee

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 1775224
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onPause()V

    .line 1775225
    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 1775226
    const/16 v1, 0x2b

    const v2, 0x3ec67d8b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onResume()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v0, 0x2a

    const v3, 0x1ff7a591

    invoke-static {v9, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 1775217
    invoke-super {p0}, Lcom/facebook/base/fragment/FbFragment;->onResume()V

    .line 1775218
    invoke-static {p0, v1}, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->b(Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;Z)V

    .line 1775219
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->l:Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsSelectorAdapter;

    .line 1775220
    iget-object v3, v0, LX/9UA;->d:Landroid/database/Cursor;

    move-object v0, v3

    .line 1775221
    if-nez v0, :cond_0

    .line 1775222
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformComposerFriendsFragment;->m:LX/BL6;

    sget-object v3, LX/0P0;->g:Landroid/net/Uri;

    sget-object v4, LX/9UW;->a:[Ljava/lang/String;

    const-string v5, "display_name IS NOT NULL AND LENGTH(display_name) > 0"

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, LX/BL6;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 1775223
    :cond_0
    const/16 v0, 0x2b

    const v1, 0x66ec9851

    invoke-static {v9, v0, v1, v8}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
