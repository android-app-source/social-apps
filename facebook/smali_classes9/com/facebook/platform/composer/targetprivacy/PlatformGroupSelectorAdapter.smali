.class public Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;
.super LX/9UL;
.source ""

# interfaces
.implements Landroid/widget/Filterable;
.implements Lcom/facebook/common/callercontext/CallerContextable;


# static fields
.field private static final g:Lcom/facebook/common/callercontext/CallerContext;


# instance fields
.field public h:Ljava/lang/String;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/BLj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1776373
    const-class v0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;

    invoke-static {v0}, Lcom/facebook/common/callercontext/CallerContext;->a(Ljava/lang/Class;)Lcom/facebook/common/callercontext/CallerContext;

    move-result-object v0

    sput-object v0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->g:Lcom/facebook/common/callercontext/CallerContext;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 1776374
    invoke-direct {p0, p1}, LX/9UL;-><init>(Landroid/content/Context;)V

    .line 1776375
    new-instance v0, LX/BLj;

    invoke-direct {v0, p0}, LX/BLj;-><init>(Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;)V

    iput-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->j:LX/BLj;

    .line 1776376
    return-void
.end method


# virtual methods
.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1776377
    invoke-virtual {p0, p1, p2}, LX/9UL;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookProfile;

    .line 1776378
    if-nez p4, :cond_0

    .line 1776379
    iget-object v1, p0, LX/9UL;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f030fc6

    invoke-virtual {v1, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 1776380
    :cond_0
    const v1, 0x7f0d1602

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 1776381
    const v2, 0x7f0d2616

    invoke-virtual {p4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/resources/ui/FbTextView;

    .line 1776382
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/CharSequence;

    const/4 v4, 0x0

    iget-object v5, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v3}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1776383
    iget-object v3, v0, Lcom/facebook/ipc/model/FacebookProfile;->mImageUrl:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v3, v4}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 1776384
    :goto_0
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookProfile;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 1776385
    return-object p4

    .line 1776386
    :cond_1
    sget-object v3, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->g:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v6, v3}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/model/FacebookProfile;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1776387
    iput-object p1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->i:Ljava/util/List;

    .line 1776388
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->j:LX/BLj;

    iget-object v1, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/BLj;->filter(Ljava/lang/CharSequence;)V

    .line 1776389
    const v0, 0x46493081

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 1776390
    return-void
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 1776391
    iget-object v0, p0, Lcom/facebook/platform/composer/targetprivacy/PlatformGroupSelectorAdapter;->j:LX/BLj;

    return-object v0
.end method
